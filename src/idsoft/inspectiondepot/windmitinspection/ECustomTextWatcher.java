package idsoft.inspectiondepot.windmitinspection;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class ECustomTextWatcher implements TextWatcher {
	private EditText mEditText;

	public ECustomTextWatcher(EditText e) { 
        mEditText = e;
    }
	
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub
		   if (mEditText.getText().toString().startsWith(" "))
	        {
	            // Not allowed
	        	mEditText.setText("");
	        }
		   if (mEditText.getText().toString().startsWith("0"))
	        {
	            // Not allowed
	        	mEditText.setText("");
	        }
	}

}