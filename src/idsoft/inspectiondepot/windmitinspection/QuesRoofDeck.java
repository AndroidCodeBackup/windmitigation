package idsoft.inspectiondepot.windmitinspection;

import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class QuesRoofDeck extends Activity {
	TextView txroofdeckheading,prevmitidata,roofdeck_type;
	RadioButton rdioA, rdioB, rdioC, rdioD, rdioE, rdioF, rdioG;
	ListView list;
	String commentsfill, InspectionType, status, rdiochk, comm, homeId,
			othertext = "", updatecnt="0",commdescrip = "",conchkbox,
			identity,inspectortypeid, helpcontent,roofdeckcomment,roofdeckvalueprev, roofdeckothrtxtprev,descrip, comm2, chkstatus = "true";
	EditText comments, txtother;
	Intent iInspectionList;
	String tmp="";
	int value, Count,viewimage = 1, commentsch,optionid;
	Button saveclose;
	private static final int visibility = 0;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	String[] arrcomment1;
	boolean load_comment=true;
	CheckBox temp_st;
	View v1;
	CheckBox[] cb;
	View vv;
	AlertDialog alertDialog;	
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}
		db.getPHinformation(cf.Homeid);
		setContentView(R.layout.roofdeck);
		db.getInspectorId();
		db.getPHinformation(cf.Homeid);
		cf.getDeviceDimensions();
		db.changeimage(cf.Homeid);
		
		/** menu **/
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(QuesRoofDeck.this, 2, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);/** Questions submenu **/
		sublayout.addView(new MyOnclickListener(QuesRoofDeck.this,23, cf, 1));
		
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		txroofdeckheading = (TextView) findViewById(R.id.txtheadingroofdeck);
		txroofdeckheading.setText(Html.fromHtml("<font color=red> * "
				+ "</font>What is the " + "<u>" + "weakest" + "</u>"
				+ " form of roof deck attachment?"));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
				
		db.getQuesOriginal(cf.Homeid);
		if(!db.rdoriddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.rdoriddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}
		
		
		this.rdioA = (RadioButton) this.findViewById(R.id.rdio1);
		this.rdioA.setOnClickListener(OnClickListener);
		this.rdioB = (RadioButton) this.findViewById(R.id.rdio2);
		this.rdioB.setOnClickListener(OnClickListener);
		this.rdioC = (RadioButton) this.findViewById(R.id.rdio3);
		this.rdioC.setOnClickListener(OnClickListener);
		this.rdioD = (RadioButton) this.findViewById(R.id.rdio4);
		this.rdioD.setOnClickListener(OnClickListener);
		this.rdioE = (RadioButton) this.findViewById(R.id.rdio5);
		this.rdioE.setOnClickListener(OnClickListener);
		this.rdioF = (RadioButton) this.findViewById(R.id.rdio6);
		this.rdioF.setOnClickListener(OnClickListener);
		this.rdioG = (RadioButton) this.findViewById(R.id.rdio7);
		this.rdioG.setOnClickListener(OnClickListener);

		this.txtother = (EditText) this.findViewById(R.id.txtroofdeckother);
		
				
		comments = (EditText)findViewById(R.id.txtcomments);
		roofdeck_type=(TextView) findViewById(R.id.SH_TV_ED);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,roofdeck_type));
		
		setdata();
	
	}
	
	private void setdata() {
		// TODO Auto-generated method stub
		try {
			Cursor cur = db.wind_db.rawQuery("select * from "
					+ db.Questions + " where SRID='" + cf.Homeid
					+ "'", null);
			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {
				roofdeckvalueprev = cur.getString(cur
						.getColumnIndex("RoofDeckValue"));
				roofdeckothrtxtprev = db.decode(cur.getString(cur
						.getColumnIndex("RoofDeckOtherText")));

				if (roofdeckvalueprev.equals("1")) {
					rdioA.setChecked(true);
					rdiochk = "1";
				} else if (roofdeckvalueprev.equals("2")) {
					rdioB.setChecked(true);
					rdiochk = "2";
				} else if (roofdeckvalueprev.equals("3")) {
					rdioC.setChecked(true);
					rdiochk = "3";
				} else if (roofdeckvalueprev.equals("4")) {
					rdioD.setChecked(true);
					rdiochk = "4";
				} else if (roofdeckvalueprev.equals("5")) {
					rdioE.setChecked(true);
					rdiochk = "5";
					txtother.setText(roofdeckothrtxtprev);
				} else if (roofdeckvalueprev.equals("6")) {
					rdioF.setChecked(true);
					rdiochk = "6";
				} else if (roofdeckvalueprev.equals("7")) {
					rdioG.setChecked(true);
					rdiochk = "7";
				} else {

				}
			}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in retrieving QUES data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}

		try {
			Cursor cur = db.wind_db.rawQuery("select * from " + db.QuestionsComments	+ " where SRID='" + cf.Homeid + "'", null);
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				if (cur != null) {
					roofdeckcomment = db.decode(cur.getString(cur
							.getColumnIndex("RoofDeckComment")));
					
					comments.setText(roofdeckcomment);
				}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in retrieving roofdeck comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.rdio1:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 3 Roof Deck Attachment, where mean lift is less than that of B and C Selections.";
				comments.setText(commentsfill);
				rdiochk = "1";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				viewimage = 1;
				break;
			case R.id.rdio2:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "2";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(true);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				viewimage = 1;

				break;
			case R.id.rdio3:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "3";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(true);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				viewimage = 1;

				break;
			case R.id.rdio4:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "4";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(true);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				viewimage = 1;

				break;
			case R.id.rdio5:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "5";
				rdioA.setChecked(false);
				txtother.setEnabled(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(true);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				viewimage = 1;
				break;
			case R.id.rdio6:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 3 Roof Deck Attachment.";
				comments.setText(commentsfill);
				rdiochk = "6";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(true);
				rdioG.setChecked(false);
				viewimage = 1;
				break;
			case R.id.rdio7:
				commentsfill = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				comments.setText(commentsfill);
				rdiochk = "7";
				txtother.setText("");
				txtother.setEnabled(false);
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				rdioD.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(true);
				viewimage = 1;
				break;
			}
		}
	};

	public void clicker(View v) {

		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			RD_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				System.out.println("tesload");
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_ques", "3");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
						
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Deck");	
			}			
			break;
		case R.id.txthelpcontentoptionA:
			cf.alerttitle="A - Mean Uplift less than B and C";
		    cf.alertcontent="Plywood/oriented-strand board (OSB) roof sheathing attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by staples or 6d nails spaced at 6 inches along the edge and 12 inches in the field OR batten decking supporting wood shakes or wood shingles OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that has an equivalent mean uplift less than that required for options B or C below.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionB:
			cf.alerttitle="B - 103 PSF";
			cf.alertcontent="Plywood/OSB roof sheathing, with a minimum thickness of 7/16 inch, attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by 8d common nails spaced a maximum of 12 inches in the field OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that is shown to have an equivalent or greater resistance than 8d nails spaced a maximum of 12 inches in the field or has a mean uplift resistance of at least 103 psf.";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionC:
			cf.alerttitle="C - 182 PSF";
			cf.alertcontent="Plywood/OSB roof sheathing, with a minimum thickness of 7/16 inch, attached to the roof truss/rafter (spaced a maximum of 24 inches o.c.) by 8d common nails spaced a maximum of 6 inches in the field OR dimensional lumber/tongue-and-groove decking with a minimum of two nails per board (or one nail per board if each board is equal to or less than 6 inches in width) OR any system of screws, nails, adhesives, other deck fastening system or truss/rafter spacing that is shown to have an equivalent or greater resistance than 8d common nails spaced a maximum of 6 inches in the field or has a mean uplift resistance of at least 182 psf";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionD:
			cf.alerttitle="D - Concrete";
			cf.alertcontent="Reinforced concrete roof deck";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionE:
			cf.alerttitle="E - other";
		    cf.alertcontent="Other.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionF:
			cf.alerttitle="F - Unknown";
		    cf.alertcontent="Unknown or unidentified.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionG:
			cf.alerttitle="G - No Attic";
		    cf.alertcontent="No attic access.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:
			comm = comments.getText().toString();

			if ((rdioA.isChecked() == true) || (rdioB.isChecked() == true)
					|| (rdioC.isChecked() == true)
					|| (rdioD.isChecked() == true)
					|| (rdioE.isChecked() == true)
					|| (rdioF.isChecked() == true)
					|| (rdioG.isChecked() == true)) {
				if (rdioE.isChecked() == true) {
					othertext = txtother.getText().toString();
					if (othertext.trim().equals("")) {
						cf.ShowToast("Please enter the Other text for Roof Deck Attachment");
						txtother.requestFocus();
						chkstatus = "false";
					} else {
						chkstatus = "true";
					}
				}

				if (comm.trim().equals("")) {
					cf.ShowToast("Please enter the Comments for Roof Deck");
					comments.requestFocus();
					updatecnt = "0";
				} else {
					updatecnt = "1";
					
					if (chkstatus.equals("true") && updatecnt.equals("1")) {

						try {
							Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
									+ db.Questions + " WHERE SRID='"
									+ cf.Homeid + "'", null);System.out.println("iii"+c2.getCount());
							int rws = c2.getCount();
							if (rws == 0) {
								db.wind_db.execSQL("INSERT INTO "
										+ db.Questions 
										+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
										+ " VALUES ('" + cf.Homeid
										+ "','','','','" + 0 + "','" + 0
										+ "','','','','','" + 0 + "','"
										+ rdiochk + "','"
										+ db.encode(othertext)
										+ "','" + 0 + "','" + 0 + "','" + 0
										+ "','" + 0 + "','" + 0 + "','','" + 0
										+ "','" + 0 + "','" + 0 + "','','" + 0
										+ "','','" + 0 + "','" + 0
										+ "','','','','','','','','" + 0
										+ "','" + 0 + "','" + 0 + "','" + 0
										+ "','" + 0 + "','" + 0 + "','" + 0
										+ "','" + 0 + "','','" + 0 + "','" + 0
										+ "','','" + 0 + "','','','" + cd
										+ "','" + 0 + "')");
							} else {
								System.out.println("iiielse");
								db.wind_db.execSQL("UPDATE " + db.Questions
										+ " SET RoofDeckValue='" + rdiochk
										+ "',RoofDeckOtherText='"
										+ db.encode(othertext)
										+ "'" + " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");
							}

						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invalid character");
							System.out.println("roof deco="+e.getMessage());
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in saving roofdeck values because of invalid input on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

						}
						try {

							db.getInspectorId();
							Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
									+db.QuestionsComments + " WHERE SRID='"
									+ cf.Homeid + "'", null);
							int rws = c2.getCount();
							if (rws == 0) {
								db.wind_db.execSQL("INSERT INTO "
										+ db.QuestionsComments
										+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
										+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','"+ db.encode(comments.getText().toString())+ "','','','','','','','"+ cf.datewithtime + "')");
						

							} else {
								db.wind_db.execSQL("UPDATE "
										+ db.QuestionsComments
										+ " SET RoofDeckComment='"
										+ db.encode(comments
												.getText().toString())
										+ "',CreatedOn ='"
										+ md + "'" + " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");
							}
						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invalid character");
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in saving roofdeck comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

						}
						db.getInspectorId();

						try {
							Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "
									+ db.SubmitCheckTable + " WHERE Srid='"
									+ cf.Homeid + "'", null);
							int subchkrws = c3.getCount();
							if (subchkrws == 0) {
								db.wind_db.execSQL("INSERT INTO "
										+ db.SubmitCheckTable
										+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
										+ "VALUES ('" + db.Insp_id + "','"
										+ cf.Homeid
										+ "',0,0,0,1,0,0,0,0,0,0,0,0,0,0,0)");
							} else {
								db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
										+ " SET fld_roofdeck='1' WHERE Srid ='"
										+ cf.Homeid + "' and InspectorId='"
										+ db.Insp_id+ "'");
							}
						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invlaid character");
							//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofDeck.this +" problem in inserting or updating roofdeck values on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

						}

					}
				}
			} else {
				cf.ShowToast("Please select the Roof Deck Attachment");

			}
			if (updatecnt.equals("1") && chkstatus.equals("true")) {
				
				// roofdecktick.setVisibility(visibility);
				cf.ShowToast("Roof Deck has been saved successfully");
				iInspectionList = new Intent(QuesRoofDeck.this, QuesRoofWall.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("status", cf.status);
				startActivity(iInspectionList);
				finish();
			}

			break;
		}
	}
	private void RD_suboption() {
		// TODO Auto-generated method stub
		if(rdioA.isChecked())	{		tmp="1";}
		else if(rdioB.isChecked()) {	tmp="2";}
		else if(rdioC.isChecked()) {	tmp="3";}
		else if(rdioD.isChecked()) {	tmp="4";}
		else if(rdioE.isChecked()) {	tmp="5";}
		else if(rdioF.isChecked()) {	tmp="6";}
		else if(rdioG.isChecked()) {	tmp="7";}
		else{tmp="";}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesRoofDeck.this, QuesRoofCover.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		//switch (resultCode) {
	
		  if(requestCode==cf.loadcomment_code)
			{
				load_comment=true;
				if(resultCode==RESULT_OK)
				{
					String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
					((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
				}
			}
		/*	case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;*/

	   }

}
