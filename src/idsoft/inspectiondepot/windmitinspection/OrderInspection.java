package idsoft.inspectiondepot.windmitinspection;


import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

public class OrderInspection extends Activity {
	Button placeorder, home;
	String redcolor = "<font color=red> * </font>", ImageName;
	CheckBox cb[], checkbox[];
	EditText et[];
	TimePickerDialog timepicker;
	static final int TIME_DIALOG_ID = 999;
	ImageView img[];View v1;
	int i, j, Cnt,usrcheck, n, width, height,noofbuild,ii,keyDelfee,keyDeldis,val_handler,eval_handler;
	boolean rgcheck = false,zip2identifier = true, rgcheck2 = false, call_county2 = true, oncreate,countysetselection=false,importph=false;
	SoapObject chkaddservice,order_result,validate_result;
	String insptypeid = "28", statevalue = "false",zipidentifier,state,county,city,stateid,countyid,feeformat="";
	String strinspectionbasesq_feet = "0", strinspectionbase_fee = "0",
			strinspectionadditionalsq_feet = "0",
			strinspectionaddtional_fee = "0", strbuildingbasesq_feet = "0",
			strbuildingbase_fee = "0", strbuildingaddsq_feet = "0",
			strbuildingadd_fee = "0", strsquarefootagevalue = "0",
			strtotalvalue = "0", stradditionalinspectionvalue = "0",
			stradditionalbuildingvalue = "0", strdiscountvalue = "0";
	float inspectionbasesq_feet, inspectionbase_fee,
			inspectionadditionalsq_feet, inspectionaddtional_fee,
			buildingbasesq_feet, buildingbase_fee, buildingaddsq_feet,
			buildingadd_fee, squarefootagevalue, totalvalue,
			additionalinspectionvalue, additionalbuildingvalue = 0,
			discountvalue,inspectionbase_fee2,discountvalue2,value=0;
	// lastnametxt.setText(Html.fromHtml(cf.redcolor+" "+"Last Name "));
	Spinner spinnerstate, spinnercounty,  spinnerstate2,
			spinneraddi, spinnercounty2, spinneryear, spinnerbuildintype;
	LinearLayout llgeneralinfo, llcreateanaccount, llspinner,
			llcalculatefees, llotheryear,
			llnoofbuildings, llotherbuildingtype;
	ImageView plus1, plus2, minus1, minus2, descriptionimage;
	String[] arrayid, array_Insp_ID,
			array_InspName, array_Type_ID, arraystateid, arraystatename,
			arraycountyid, arraycountyname, arraycompanyid, arraycompanyname,
			arraystateid2, arr_bType, arraystatename2, arraycountyid2, arraycountyname2,
			arrayagencyname,arrayagencyid,arrayagentname,arrayagentid,arrayrealestatecompanyname,
			arrayrealestatecompanyid,arrayrealtorname,arrayrealtorid;
	String yearbuilt[] = { "--Select--", "Other", "2013", "2012", "2011",
			"2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003",
			"2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995",
			"1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987",
			"1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979",
			"1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971",
			"1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963",
			"1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955",
			"1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947",
			"1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939",
			"1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931",
			"1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923",
			"1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915",
			"1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907",
			"1906", "1905", "1904", "1903", "1902", "1901", "1900" };
	TextView description,totalfee;
	CommonFunctions cf;
	int show_handler;
	String 	strpreferredtime, strinspaddress1, strinspaddress2, strstate,
			strcounty, strcity = "", strzip = "", strmailaddress1 = "",
			strmailaddress2 = "", strstate2, strcounty2, strcity2 = "",
			strzip2 = "", strselect = "--Select--", 
			strcompanyname,stryear, strtime = "", strcountyid,
			strcountyid2, strstateid, strstateid2, strcompanyid = "0",
			strbuildingname,strinspectioncategoryname, strinspectioncategoryid = "0",
			strinsptypename, str_building_edittext_value = "",
			strpolicyno = "", strnoofstories, strnoofbuildings, strdate,
			strfirstname, strlastname, strmail, strphone, currentdate;
	CheckBox cbaddresscheck;
	EditText etinspaddress1, etinspaddress2, etcity, etzip, etmailaddress1,etinspfees,etinspdiscount,
			etmailaddress2, etcity2, etzip2, ettotalfees, etotherbuildingtype,
			etinspectionfees, etsquarefootage,etcompanyname,
			etdate, etnoofstories, etnoofbuildings, etfirstname,
			etlastname, etmail, etphone, etpolicyno, etotheryear,ettime;
	EditText[] dynamic_etsquarefootage,dynamic_etnoofstories;
	ArrayAdapter<String> stateadapter, countyadapter, countyadapter2;
	String inspector_id,inspectorname,companyid,companyname;
	DatePickerDialog datePicker;
	int keyDel;
	int id_len;
	DataBaseHelper1 dbh1;
	DatabaseFunctions db;
	WebserviceFunctions wb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.orderinspection);
		cf = new CommonFunctions(this);
		db = new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		cf.setTouchListener(findViewById(R.id.widget54));

		final Calendar c = Calendar.getInstance();
		final int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
		System.out.println("The current date is" + currentdate);
		db.getInspectorId();
		
		Cursor cur=db.wind_db.rawQuery("select * from "+db.inspectorlogin + " where Ins_Id='"+db.Insp_id+"'", null);System.out.println("select * from "+db.inspectorlogin + " where Ins_Id='"+db.Insp_id+"'");
		if(cur.getCount()>0)
		{
			cur.moveToFirst();
			companyid=db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyId")));
			companyname=db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyName")));System.out.println("companyname"+companyname);
		}
		db.getInspectorId();
		inspectorname=db.Insp_firstname+" "+db.Insp_lastname;

		home = (Button) findViewById(R.id.orderinspection_home);
		placeorder = (Button) findViewById(R.id.orderinsp_btnfreeinspectionquotes);
		spinnerstate = (Spinner) findViewById(R.id.orderinspection_spinnerstate);
		spinnercounty = (Spinner) findViewById(R.id.orderinspection_spinnercounty);
		spinnerstate2 = (Spinner) findViewById(R.id.orderinspection_spinnerstate2);
		spinnercounty2 = (Spinner) findViewById(R.id.orderinspection_spinnercounty2);
		etcompanyname = (EditText) findViewById(R.id.orderinspection_preferredimc);
		etcompanyname.setText(companyname);
		spinneryear = (Spinner) findViewById(R.id.orderinspection_spinneryear);
		//spinnerbuildintype = (Spinner) findViewById(R.id.orderinspection_buildingtype);
		/*spinneragency = (Spinner) findViewById(R.id.orderinspection_spinneragency);
		spinneragent = (Spinner) findViewById(R.id.orderinspection_spinneragent);
		spinnerrealestatecompany = (Spinner) findViewById(R.id.orderinspection_spinnerrealestatecompany);
		spinnerrealtor = (Spinner) findViewById(R.id.orderinspection_spinnerrealtor);
		rgtime = (RadioGroup) findViewById(R.id.orderinsp_radiogrouptime);
		llagent = (LinearLayout) findViewById(R.id.orderinspection_llagent);
		llrealtor = (LinearLayout) findViewById(R.id.orderinspection_llrealtor);*/
		etinspfees = (EditText) findViewById(R.id.orderinsp_etinspfee);
		etinspdiscount = (EditText) findViewById(R.id.orderinsp_etinspdiscount);
		llgeneralinfo = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutgeneralinfoborder);
		llcreateanaccount = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutcreateanaccountborder);
		llspinner = (LinearLayout) findViewById(R.id.llspinner);
		llcalculatefees = (LinearLayout) findViewById(R.id.orderinsp_llcalculatefees);
		llotherbuildingtype = (LinearLayout) findViewById(R.id.orderinspection_llothetbuildingtype);
		llnoofbuildings = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutnoofbuildings);
		llotheryear = (LinearLayout) findViewById(R.id.orderinspection_llotheryear);
		plus1 = (ImageView) findViewById(R.id.orderinspection_plus1);
		plus2 = (ImageView) findViewById(R.id.orderinspection_plus2);
		minus1 = (ImageView) findViewById(R.id.orderinspection_minus1);
		minus2 = (ImageView) findViewById(R.id.orderinspection_minus2);
		descriptionimage = (ImageView) findViewById(R.id.orderinspection_descriptionimage);
		description = (TextView) findViewById(R.id.orderinspection_descriptiontext);
		cbaddresscheck = (CheckBox) findViewById(R.id.orderinspection_addresscheck);
		etinspaddress1 = (EditText) findViewById(R.id.orderinspection_etinspectionaddress1);
		etinspaddress2 = (EditText) findViewById(R.id.orderinspection_etinspectionaddress2);
		etcity = (EditText) findViewById(R.id.orderinspection_etcity);
		etzip = (EditText) findViewById(R.id.orderinspection_etzip);
		etmailaddress1 = (EditText) findViewById(R.id.orderinspection_etmailingaddress1);
		etmailaddress2 = (EditText) findViewById(R.id.orderinspection_etmailingaddress2);
		etcity2 = (EditText) findViewById(R.id.orderinspection_etcity2);
		etzip2 = (EditText) findViewById(R.id.orderinspection_etzip2);
		ettotalfees = (EditText) findViewById(R.id.orderinsp_ettotalfees);
		//etinspectionfees = (EditText) findViewById(R.id.orderinsp_etinspectionfees);
		//etdiscount = (EditText) findViewById(R.id.orderinsp_etdiscount);
		ettotalfees = (EditText) findViewById(R.id.orderinsp_ettotalfees);
		etsquarefootage = (EditText) findViewById(R.id.orderinsp_etsquarefootage);
		//etcouponcode = (EditText) findViewById(R.id.orderinspection_couponcode);
		etdate = (EditText) findViewById(R.id.orderinsp_etinspdate);
		ettime = (EditText) findViewById(R.id.orderinsp_etinsptime);
		etnoofstories = (EditText) findViewById(R.id.orderinspection_etnoofstories);
		//etnoofbuildings = (EditText) findViewById(R.id.orderinspection_noofbuildings);
		//etotherbuildingtype = (EditText) findViewById(R.id.orderinspection_etotherbuildingtype);
		etfirstname = (EditText) findViewById(R.id.orderinspection_etfirstanme);
		etlastname = (EditText) findViewById(R.id.orderinspection_etlastname);
		etmail = (EditText) findViewById(R.id.orderinspection_etmail);
		etphone = (EditText) findViewById(R.id.orderinspection_etphone);
		etpolicyno = (EditText) findViewById(R.id.orderinspection_policynumber);
		etotheryear = (EditText) findViewById(R.id.orderinspection_etotheryear);
		totalfee  = (TextView)findViewById(R.id.orderinsp_totalinspfee);
		
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		width = displayMetrics.widthPixels;
		height = displayMetrics.heightPixels;

		statevalue = LoadState();

		if (statevalue == "true") {
			stateadapter = new ArrayAdapter<String>(OrderInspection.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);

			stateadapter = new ArrayAdapter<String>(OrderInspection.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate2.setAdapter(stateadapter);
		}

		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				int stateid = spinnerstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(strstateid);
					spinnercounty.setEnabled(true);
					
				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							OrderInspection.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnerstate2.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				call_county2 = true;
				return false;
			}
		});

		spinnerstate2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate2 = spinnerstate2.getSelectedItem().toString();
				int stateid = spinnerstate2.getSelectedItemPosition();
				strstateid2 = arraystateid[stateid];
				if (call_county2 == true) {
					if (!strstate2.equals("--Select--")) {
						LoadCounty2(strstateid2);
						spinnercounty2.setEnabled(true);

					} else {
						// spinnercounty2.setAdapter(null);
						spinnercounty2.setEnabled(false);
						arraycountyname2 = new String[0];
						countyadapter2 = new ArrayAdapter<String>(
								OrderInspection.this,
								android.R.layout.simple_spinner_item,
								arraycountyname2);
						countyadapter2
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spinnercounty2.setAdapter(countyadapter2);
					}
				} else {
					spinnercounty2.setEnabled(true);
					spinnercounty2.setSelection(spinnercounty
							.getSelectedItemPosition());
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				int countyid = spinnercounty.getSelectedItemPosition();
				strcountyid = arraycountyid[countyid];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty2 = spinnercounty2.getSelectedItem().toString();
				int countyid = spinnercounty2.getSelectedItemPosition();
				strcountyid2 = arraycountyid2[countyid];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		
		/*arraycompanyname=new String[1];
		arraycompanyid=new String[1];
		arraycompanyname[0]=companyname;
		arraycompanyid[0]=companyid;
		
		ArrayAdapter<String> imcadapter = new ArrayAdapter<String>(
				OrderInspection.this,
				android.R.layout.simple_spinner_item,
				arraycompanyname);
		imcadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerimc.setAdapter(imcadapter);
		spinnerimc.setEnabled(false);*/

		/*spinnerimc.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcompanyname = spinnerimc.getSelectedItem().toString();
				int companyid = spinnerimc.getSelectedItemPosition();
				strcompanyid = arraycompanyid[companyid];
				
				LoadInspectionTypeDescription(insptypeid, strcompanyid);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});*/
		
		
		spinneryear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stryear = spinneryear.getSelectedItem().toString();
				if (stryear.equals("Other")) {
					llotheryear.setVisibility(View.VISIBLE);
				} else {
					llotheryear.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		ArrayAdapter<String> yearadapter = new ArrayAdapter<String>(
				OrderInspection.this, android.R.layout.simple_spinner_item,
				yearbuilt);
		yearadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinneryear.setAdapter(yearadapter);

		cbaddresscheck
				.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
		/*etcouponcode
				.addTextChangedListener(new TextWatcher1(etcouponcode));*/
		etpolicyno.addTextChangedListener(new TextWatcher1(etpolicyno));
		etnoofstories.addTextChangedListener(new TextWatcher1(
				etnoofstories));
		etfirstname.addTextChangedListener(new TextWatcher1(etfirstname));
		etlastname.addTextChangedListener(new TextWatcher1(etlastname));
		etmail.addTextChangedListener(new TextWatcher1(etmail));
		etinspaddress1.addTextChangedListener(new TextWatcher1(
				etinspaddress1));
		etinspaddress2.addTextChangedListener(new TextWatcher1(
				etinspaddress2));
		etcity.addTextChangedListener(new TextWatcher1(etcity));
		etmailaddress1.addTextChangedListener(new TextWatcher1(
				etmailaddress1));
		etmailaddress2.addTextChangedListener(new TextWatcher1(
				etmailaddress2));
		etcity2.addTextChangedListener(new TextWatcher1(etcity2));
/*		etotherbuildingtype.addTextChangedListener(new CustomTextWatcher(
				etotherbuildingtype));*/

		etotheryear.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (etotheryear.getText().toString().trim().matches("^0")) {
					etotheryear.setText("");
				}
				if (!etotheryear.getText().toString().trim().equals("")) {
					try{
					if (Integer.parseInt((etotheryear.getText().toString()
							.trim())) > year1) {
						etotheryear.setText("");
						cf.ShowToast("Please enter a valid year");
						
					}
					}catch (Exception e) {
						// TODO: handle exception
					}
				}
				if (Arrays.asList(yearbuilt).contains(
						etotheryear.getText().toString().trim())) {
					etotheryear.setText("");
					cf.ShowToast("Please enter a year that is not in the year list");
					
				}

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});
		
		etphone.setLongClickable(false);

		etphone.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etphone.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etphone.setText("");
		        }
				if (etphone.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					etphone.setText("");
	            }

				etphone.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if (keyCode == KeyEvent.KEYCODE_DEL)
						{
							keyDel = 1;
							System.out.println("Inside delete");
							
							String str = etphone.getText().toString();
							int len=str.length();
							
							if(len==9)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==5)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==1)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							
							return false;
						}
						else
						{
							keyDel = 0;
							return false;
						}
					}
				});

				if (keyDel == 0) {
					String a = "";
					String str = etphone.getText().toString();
					String replaced = str.replaceAll(Pattern.quote("("), "");
					replaced = replaced.replaceAll(Pattern.quote("-"), "");
					replaced = replaced.replaceAll(Pattern.quote(")"), "");
					char[] id_char = replaced.toCharArray();
					id_len = replaced.length();
					System.out.println("The length is "+id_len);
					for (int i = 0; i < id_len; i++) {
						if (i == 0) {
							a = "(" + id_char[i];
						} else if (i == 2) {
							a += id_char[i] + ")";
						} else if (i == 5) {
							a += id_char[i] + "-";
						} else
							a += id_char[i];
							keyDel = 0;
					}
					etphone.removeTextChangedListener(this);
					etphone.setText(a);
					if (before > 0)
						etphone.setSelection(start);
					else
						etphone.setSelection(a.length());
					etphone.addTextChangedListener(this);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		etsquarefootage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etsquarefootage.getText().toString().startsWith(" ")) {
					// Not allowed
					etsquarefootage.setText("");
				}
				if (etsquarefootage.getText().toString().trim().matches("^0")) {
					// Not allowed
					etsquarefootage.setText("");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		etotheryear.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etotheryear.getText().toString().trim().length() >= 1) {
						if (etotheryear.getText().toString().trim().length() < 4) {
							etotheryear.setText("");
							cf.ShowToast("Please enter a valid year");
							
						}
					}
				}
			}
		});

		etmail.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etmail.getText().toString().trim().length() >= 1) {
						if (!cf.Email_Validation(etmail.getText().toString()
								.trim())) {
							etmail.setText("");
							cf.ShowToast("Please enter a valid Email");
						}
					}
				}
			}
		});
		
		/*etcouponcode.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etcouponcode.getText().toString().trim().length() >= 1) {
						Check_Couponcode();
					}
				}
			}
		});*/
		
		etzip.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etzip.getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					zipidentifier="zip1";
					Load_State_County_City(etzip);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		etzip2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etzip2.getText().toString().trim().length() == 5) {
					spinnerstate2.setSelection(0);
					zipidentifier = "zip2";
					if(zip2identifier)
					{
						Load_State_County_City(etzip2);
					}
					
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		
       ((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside");
				Intent insp_info = new Intent(OrderInspection.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);System.out.println("ends");
			}
		});
	
       etinspfees.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (etinspfees.getText().toString().matches("0"))
		        {
		            // Not allowed
					etinspfees.setText("");
		        }
				if (etinspfees.getText().toString().startsWith(" "))
		        {
		            // Not allowed
					etinspfees.setText("");
		        }
				if (etinspfees.getText().toString().matches("0"))
		        {
		            // Not allowed
					etinspfees.setText("");
		        }
				   
						if(!etinspdiscount.getText().toString().trim().equals(""))
						{
							try{
							if(Integer.parseInt(etinspdiscount.getText().toString())>Integer.parseInt(etinspfees.getText().toString()))
							{
								cf.ShowToast("Please enter Discount less than the Inspection Fee");
								cf.hidekeyboard(etinspdiscount);
								
							}
							}catch (Exception e) {
								// TODO: handle exception
							}
						}
					
			
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
       
       etinspdiscount.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (etinspdiscount.getText().toString().matches("0"))
		        {
		            // Not allowed
					etinspdiscount.setText("");
		        }
				if (etinspdiscount.getText().toString().startsWith(" "))
		        {
		            // Not allowed
					etinspdiscount.setText("");
		        }
				if (etinspdiscount.getText().toString().matches("0"))
		        {
		            // Not allowed
					etinspdiscount.setText("");
		        }
			    if(etinspfees.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter Inspection Fee");
					etinspfees.requestFocus();
					cf.hidekeyboard(etinspdiscount);
				}
				else 
				{
					if(!etinspdiscount.getText().toString().trim().equals(""))
					{
						
						try{
						if(Integer.parseInt(etinspdiscount.getText().toString())>Integer.parseInt(etinspfees.getText().toString()))
						{
							cf.ShowToast("Please enter Discount less than the Inspection Fee");
							cf.hidekeyboard(etinspdiscount);
							
						}
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
					
				}
			 
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.orderinspection_plus1:
			System.out.println("plus1");
			llgeneralinfo.setVisibility(View.VISIBLE);
			plus1.setVisibility(View.GONE);
			minus1.setVisibility(View.VISIBLE);
			break;

		case R.id.orderinspection_minus1:
			System.out.println("minus1");
			llgeneralinfo.setVisibility(View.GONE);
			plus1.setVisibility(View.VISIBLE);
			minus1.setVisibility(View.GONE);
			break;

		case R.id.orderinspection_plus2:
			System.out.println("plus2");
			llcreateanaccount.setVisibility(View.VISIBLE);
			plus2.setVisibility(View.GONE);
			minus2.setVisibility(View.VISIBLE);
			break;

		case R.id.orderinspection_minus2:
			System.out.println("minus2");
			llcreateanaccount.setVisibility(View.GONE);
			plus2.setVisibility(View.VISIBLE);
			minus2.setVisibility(View.GONE);
			break;

		case R.id.orderinspection_home:
			Intent inthome = new Intent(OrderInspection.this, HomeScreen.class);
			startActivity(inthome);
			finish();
			break;

		case R.id.orderinsp_btncalculatefees:
					inspectionbasesq_feet = 0;
					inspectionbase_fee = 0;
					inspectionadditionalsq_feet = 0;
					inspectionaddtional_fee = 0;
					buildingbasesq_feet = 0;
					buildingbase_fee = 0;
					buildingaddsq_feet = 0;
					buildingadd_fee = 0;
					discountvalue2 = 0;
					Calculate_Fees();

			break;

		case R.id.orderinsp_btnfreeinspectionquotes:
				Check_Validation();
			break;

		case R.id.orderinsp_getdate:
			showDialogDate(etdate);
			break;
			
		case R.id.orderinsp_gettime:
			showDialog(TIME_DIALOG_ID);
			break;
		}
	}
	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {

			int hour = selectedHour;
			int minute = selectedMinute;
			StringBuilder sb = new StringBuilder();
			String hr="",min="";
			if (hour >= 12) {
				hour=hour-12;
				if(hour<10)
				{
					hr="0"+hour;
				}
				else
				{
					hr=String.valueOf(hour);
				}
				if(minute<10)
				{
					min="0"+minute;
				}
				else
				{
					min=String.valueOf(minute);
				}
				sb.append(hr).append(":").append(min).append(" PM");
			} else {
				if(hour<10)
				{
					hr="0"+hour;
				}
				else
				{
					hr=String.valueOf(hour);
				}
				if(minute<10)
				{
					min="0"+minute;
				}
				else
				{
					min=String.valueOf(minute);
				}
				sb.append(hr).append(":").append(min).append(" AM");
			}

			ettime.setText(sb);
			strtime = ettime.getText().toString();
			
			//updating time picker to current time
			final Calendar c = Calendar.getInstance();
			timepicker.updateTime(c.get(Calendar.HOUR_OF_DAY), 
					 c.get(Calendar.MINUTE));
		}
	};
	
	@Override
	protected Dialog onCreateDialog(int id) {

	switch (id) {
	case TIME_DIALOG_ID:
		final Calendar c1 = Calendar.getInstance();
		int hour = c1.get(Calendar.HOUR_OF_DAY);
		int minute = c1.get(Calendar.MINUTE);
		timepicker= new TimePickerDialog(this, timePickerListener, hour, minute,
				false);
		return timepicker;
	}
	return null;
	}
	private void Check_Validation() {
		boolean mailvalidation, phonevalidation;

		if (spinneryear.getSelectedItem().toString().equals("Other")) {
			stryear = etotheryear.getText().toString().trim();
		} else {
			stryear = spinneryear.getSelectedItem().toString();
		}
		System.out.println("The year is " + stryear);

		if (etmail.getText().toString().trim().equals("")) {
			mailvalidation = true;
		} else {
			if (cf.Email_Validation(etmail.getText().toString().trim())) {
				mailvalidation = true;
			} else {
				mailvalidation = false;
			}
		}

		if (etphone.getText().toString().trim().equals("")) {
			phonevalidation = true;
		} else {
			if (etphone.getText().toString().trim().length() < 13) {
				phonevalidation = false;
			} else {
				phonevalidation = true;
			}
		}
		
		
		boolean boolnoofstories = true;
		if (etnoofstories.getText().toString().trim().equals("")) {
			boolnoofstories = false;
		} else {
			boolnoofstories = true;
		}		
		
		boolean boolsquarefootage=true;
			if(etsquarefootage.getText().toString().trim().equals(""))
			{
				boolsquarefootage=false;
			}
			else
			{
				try{
				int val=Integer.parseInt(etsquarefootage.getText().toString().trim());
				if(val<399||val>100000)
				{
					boolsquarefootage=false;
				}
				else
				{
					boolsquarefootage=true;
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
			
			boolean booldiscount = true;
			if (!etinspdiscount.getText().toString().trim().equals("") && !etinspfees.getText().toString().trim().equals("")) 
			{
				System.out.println("inside not null");
				try{
				if(Integer.parseInt(etinspdiscount.getText().toString())>Integer.parseInt(etinspfees.getText().toString()))
				{
					System.out.println("booldiscount");	
					booldiscount=false;
				}
				else
				{
					booldiscount=true;
				}
				}
				catch (Exception e) {
					// TODO: handle exception
				}
			} 
			else
			{
				booldiscount = true;
			}	
			System.out.println("booldiscount"+booldiscount);
		
		strcompanyname = etcompanyname.getText().toString();
		if (boolsquarefootage) {
			if (!spinneryear.getSelectedItem().toString().equals("--Select--")) {
				if (!stryear.equals("")) {
					if (boolnoofstories) {
						//if(boolnoofbuildings){
							if (!strcompanyname.equals("--Select--")) {
								if (!etdate.getText().toString().trim()
										.equals("")) {
									if (!strtime.equals("")) {
										if (!etinspfees.getText().toString()
												.trim().equals("")) {
											if(booldiscount){
											/*if (!etinspdiscount.getText().toString()
													.trim().equals("")) {*/
											if (!etfirstname.getText()
													.toString().trim()
													.equals("")) {
												if (!etlastname.getText()
														.toString().trim()
														.equals("")) {
													if (mailvalidation) {
														if (phonevalidation) {
															if (!etinspaddress1
																	.getText()
																	.toString()
																	.trim()
																	.equals("")) {
																
																		
																			if (!etzip
																					.getText()
																					.toString()
																					.trim()
																					.equals("")) {
																				if ((etzip
																						.getText()
																						.toString()
																						.length() == 5)) {
																					if ((!etzip
																							.getText()
																							.toString()
																							.contains(
																									"00000"))) {
																						if (!etcity
																								.getText()
																								.toString()
																								.trim()
																								.equals("")) {
																							if (!strstate
																									.equals("--Select--")) {
																								if (!strcounty
																										.equals("--Select--")) {
																						if (!etmailaddress1
																								.getText()
																								.toString()
																								.trim()
																								.equals("")) {
																							
																									
																										if (!etzip2
																												.getText()
																												.toString()
																												.trim()
																												.equals("")) {
																											if ((etzip2
																													.getText()
																													.toString()
																													.length() == 5)) {
																												if ((!etzip2
																														.getText()
																														.toString()
																														.contains(
																																"00000"))) {
																													if (!etcity2
																															.getText()
																															.toString()
																															.trim()
																															.equals("")) {
																														if (!strstate2
																																.equals("--Select--")) {
																															if (!strcounty2
																																	.equals("--Select--")) {
																																	Place_Order();
																													
																															} else {
																																cf.ShowToast("Please select County under Mailing Address");
																																llcreateanaccount
																																		.setVisibility(View.VISIBLE);
																																plus2.setVisibility(View.GONE);
																																minus2.setVisibility(View.VISIBLE);
																															}
																														} else {
																															cf.ShowToast("Please select State under Mailing Address");
																															llcreateanaccount
																																	.setVisibility(View.VISIBLE);
																															plus2.setVisibility(View.GONE);
																															minus2.setVisibility(View.VISIBLE);

																														}
																															} else {
																																cf.ShowToast("Please enter City under Mailing Address");
																																etcity2.requestFocus();
																																etcity2.setText("");
																																llcreateanaccount
																																		.setVisibility(View.VISIBLE);
																																plus2.setVisibility(View.GONE);
																																minus2.setVisibility(View.VISIBLE);
																															}
																												} else {
																													cf.ShowToast("Please enter a valid Zip under Mailing Address");
																													etzip2.requestFocus();
																													etzip2.setText("");
																													llcreateanaccount
																															.setVisibility(View.VISIBLE);
																													plus2.setVisibility(View.GONE);
																													minus2.setVisibility(View.VISIBLE);
																												}
																											} else {
																												cf.ShowToast("Please enter valid Zip under Mailing Address");
																												etzip2.requestFocus();
																												etzip2.setText("");
																												llcreateanaccount
																														.setVisibility(View.VISIBLE);
																												plus2.setVisibility(View.GONE);
																												minus2.setVisibility(View.VISIBLE);
																											}
																										} else {
																											cf.ShowToast("Please enter Zip under Mailing Address");
																											etzip2.requestFocus();
																											etzip2.setText("");
																											llcreateanaccount
																													.setVisibility(View.VISIBLE);
																											plus2.setVisibility(View.GONE);
																											minus2.setVisibility(View.VISIBLE);
																										}
																									
																								
																						} else {
																							cf.ShowToast("Please enter Mailing Address #1");
																							etmailaddress1
																									.requestFocus();
																							etmailaddress1
																									.setText("");
																							llcreateanaccount
																									.setVisibility(View.VISIBLE);
																							plus2.setVisibility(View.GONE);
																							minus2.setVisibility(View.VISIBLE);
																						}
																						// }
																								} else {
																									cf.ShowToast("Please select County under Inspection Address");
																									llcreateanaccount
																											.setVisibility(View.VISIBLE);
																									plus2.setVisibility(View.GONE);
																									minus2.setVisibility(View.VISIBLE);
																								}
																							} else {
																								cf.ShowToast("Please select State under Inspection Address");
																								llcreateanaccount
																										.setVisibility(View.VISIBLE);
																								plus2.setVisibility(View.GONE);
																								minus2.setVisibility(View.VISIBLE);
																							}
																								} else {
																									cf.ShowToast("Please enter City under Inspection Address");
																									etcity.requestFocus();
																									etcity.setText("");
																									llcreateanaccount
																											.setVisibility(View.VISIBLE);
																									plus2.setVisibility(View.GONE);
																									minus2.setVisibility(View.VISIBLE);
																								}
																					} else {
																						cf.ShowToast("Please enter a valid Zip under Inspection Address");
																						etzip.requestFocus();
																						etzip.setText("");
																						llcreateanaccount
																								.setVisibility(View.VISIBLE);
																						plus2.setVisibility(View.GONE);
																						minus2.setVisibility(View.VISIBLE);
																					}
																				} else {
																					cf.ShowToast("Please enter valid Zip");
																					etzip.requestFocus();
																					etzip.setText("");
																					llcreateanaccount
																							.setVisibility(View.VISIBLE);
																					plus2.setVisibility(View.GONE);
																					minus2.setVisibility(View.VISIBLE);
																				}
																			} else {
																				cf.ShowToast("Please enter Zip under Inspection Address");
																				etzip.requestFocus();
																				etzip.setText("");
																				llcreateanaccount
																						.setVisibility(View.VISIBLE);
																				plus2.setVisibility(View.GONE);
																				minus2.setVisibility(View.VISIBLE);
																			}
																		
																	
															} else {
																cf.ShowToast("Please enter Inspection Address1");
																etinspaddress1
																		.requestFocus();
																etinspaddress1
																		.setText("");
																llcreateanaccount
																		.setVisibility(View.VISIBLE);
																plus2.setVisibility(View.GONE);
																minus2.setVisibility(View.VISIBLE);
															}
														} else {
															cf.ShowToast("Please enter valid Phone");
															etphone.requestFocus();
															etphone.setText("");
															llcreateanaccount
																	.setVisibility(View.VISIBLE);
															plus2.setVisibility(View.GONE);
															minus2.setVisibility(View.VISIBLE);
														}
													} else {
														cf.ShowToast("Please enter valid Email");
														etmail.requestFocus();
														etmail.setText("");
														llcreateanaccount
																.setVisibility(View.VISIBLE);
														plus2.setVisibility(View.GONE);
														minus2.setVisibility(View.VISIBLE);
													}
												} else {
													cf.ShowToast("Please enter Last Name");
													etlastname.requestFocus();
													etlastname.setText("");
													llcreateanaccount
															.setVisibility(View.VISIBLE);
													plus2.setVisibility(View.GONE);
													minus2.setVisibility(View.VISIBLE);
												}
											} else {
												cf.ShowToast("Please enter First Name");
												etfirstname.requestFocus();
												etfirstname.setText("");
												llcreateanaccount
														.setVisibility(View.VISIBLE);
												plus2.setVisibility(View.GONE);
												minus2.setVisibility(View.VISIBLE);
											}
										}
										else
										{
											cf.ShowToast("Please enter Discount less than Inspection Fee");
											etinspdiscount.requestFocus();
										}
										} else {
											cf.ShowToast("Please enter Inspection Fee");
											etinspfees.requestFocus();
											/*etinspectionfees.requestFocus();
											etinspectionfees.setText("");
											llgeneralinfo
													.setVisibility(View.VISIBLE);
											plus1.setVisibility(View.GONE);
											minus1.setVisibility(View.VISIBLE);*/
										}
									} else {
										cf.ShowToast("Please select Inspection Time");
										llgeneralinfo
												.setVisibility(View.VISIBLE);
										plus1.setVisibility(View.GONE);
										minus1.setVisibility(View.VISIBLE);
									}
								} else {
									cf.ShowToast("Please select Inspection Date");
									llgeneralinfo.setVisibility(View.VISIBLE);
									plus1.setVisibility(View.GONE);
									minus1.setVisibility(View.VISIBLE);
									etdate.requestFocus();
									etdate.setText("");
								}
							} else {
								cf.ShowToast("Please select Preferred IMC");
								llgeneralinfo.setVisibility(View.VISIBLE);
								plus1.setVisibility(View.GONE);
								minus1.setVisibility(View.VISIBLE);
							}
						/*} else {
							cf.ShowToast("Please enter No Of Buildings", 0);
							etnoofbuildings.requestFocus();
							etnoofbuildings.setText("");
							llgeneralinfo.setVisibility(View.VISIBLE);
							plus1.setVisibility(View.GONE);
							minus1.setVisibility(View.VISIBLE);
						}*/
					} else {
						cf.ShowToast("Please enter No of Stories");
						etnoofstories.requestFocus();
						etnoofstories.setText("");
						llgeneralinfo.setVisibility(View.VISIBLE);
						plus1.setVisibility(View.GONE);
						minus1.setVisibility(View.VISIBLE);
					}
				} else {
					cf.ShowToast("Please enter the Year of Construction");
					etotheryear.requestFocus();
					etotheryear.setText("");
					llgeneralinfo.setVisibility(View.VISIBLE);
					plus1.setVisibility(View.GONE);
					minus1.setVisibility(View.VISIBLE);
				}
			} else {
				cf.ShowToast("Please select Year of Construction");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
		} else {
			
			if(etsquarefootage.getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter Square Footage");
				etsquarefootage.requestFocus();
				etsquarefootage.setText("");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			else
			{
				cf.ShowToast("Please enter the Square Footage greater than 399 and less than 1,00,000");
				etsquarefootage.requestFocus();
				etsquarefootage.setText("");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			
		}
	}

	private void Place_Order() {
		if (cbaddresscheck.isChecked()) {
			strstateid2 = strstateid;
			strcountyid2 = strcountyid;
			strcounty2 = strcounty;
		}
		if (spinneryear.getSelectedItem().toString().equals("Other")) {
			stryear = etotheryear.getText().toString();
		} else {
			stryear = spinneryear.getSelectedItem().toString();
		}
		//strcouponcode = etcouponcode.getText().toString();
		strsquarefootagevalue = etsquarefootage.getText().toString();
		strpolicyno = etpolicyno.getText().toString();
		strnoofstories = etnoofstories.getText().toString();
		strdate = etdate.getText().toString().trim();
		strfirstname = etfirstname.getText().toString();
		strlastname = etlastname.getText().toString();
		strmail = etmail.getText().toString();
		strphone = etphone.getText().toString();
		strcity = etcity.getText().toString();
		strzip = etzip.getText().toString();
		strcity2 = etcity2.getText().toString();
		strzip2 = etzip2.getText().toString();
		strinspaddress1 = etinspaddress1.getText().toString();
		strinspaddress2 = etinspaddress2.getText().toString();
		strmailaddress1 = etmailaddress1.getText().toString();
		strmailaddress2 = etmailaddress2.getText().toString();

		if (wb.isInternetOn() == true) {
			cf.show_ProgressDialog("Ordering Inspection...");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(wb.NAMESPACE,
								"OrderInspection");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("CouponID", "");
						request.addProperty("InspectorId", db.Insp_id);//Integer.parseInt(AgentId)
						request.addProperty("InspectorName", inspectorname);//AgentName
						request.addProperty("Mailing_Address1", strmailaddress1);
						request.addProperty("Mailing_Address2", strmailaddress2);
						try{
						request.addProperty("Mailing_StateId",Integer.parseInt(strstateid2));
						}catch (Exception e) {
							// TODO: handle exception
						}
						request.addProperty("Mailing_County", strcounty2);
						request.addProperty("Mailing_Zip", strzip2);
						request.addProperty("Mailing_City", strcity2);
						request.addProperty("Insp_Address1", strinspaddress1);
						request.addProperty("CategoryID", 0);
						request.addProperty("Insp_Address2", strinspaddress2);
						try{
						request.addProperty("Insp_StateId",
								Integer.parseInt(strstateid));
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						request.addProperty("Insp_County", strcounty);
						request.addProperty("Insp_Zip", strzip);
						request.addProperty("Insp_City", strcity);
						request.addProperty("Firstname", strfirstname);
						request.addProperty("Lastname", strlastname);
						request.addProperty("Email", strmail);
						request.addProperty("Phone", strphone);
						request.addProperty("MainInspTypeID",Integer.parseInt(insptypeid));
						request.addProperty("AdditionalInspTypes","");//stradditionalinspectionid
						request.addProperty("AdditionalBuildingTypes","");//stradditionalbuildingid
						request.addProperty("AddBuildindSqFootage",	"");//str_building_edittext_value
						request.addProperty("NoOfBuildings",0);
						request.addProperty("PolicyNumber", strpolicyno);
						request.addProperty("CarrierID", 0);//Integer.parseInt(strcarrierid)
					/*	request.addProperty("AgentID",Integer.parseInt(stragentid));//Integer.parseInt(Agent_Id2)
						request.addProperty("AgencyID", Integer.parseInt(stragencyid));//Integer.parseInt(AgencyId)
						request.addProperty("Re_AgentID",Integer.parseInt(strrealtorid));//Integer.parseInt(Agent_Id2)
						request.addProperty("Re_AgencyID", Integer.parseInt(strrealestatecompanyid));//Integer.parseInt(AgencyId)
						*/request.addProperty("sqFootage",
								Long.parseLong(strsquarefootagevalue));
						request.addProperty("YearConstruction",
								Integer.parseInt(stryear));
						request.addProperty("NoOfStories",
								Long.parseLong(strnoofstories));
//						request.addProperty("PreInspID", "");//Integer.parseInt(strinspectorid)
//						request.addProperty("PreCompanyID",
//								Integer.parseInt(strcompanyid));
						request.addProperty("PreDateInsp", strdate);
						request.addProperty("PreTime", strtime);
						request.addProperty("Inspectionfee", etinspfees.getText().toString());
						
						if(etinspdiscount.getText().toString().equals(""))
						{
							request.addProperty("Discount", "0");
						}
						else
						{
							request.addProperty("Discount", etinspdiscount.getText().toString());	
						}
						
						envelope.setOutputSoapObject(request);
						System.out.println("OrderInspection request is "
								+ request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								wb.URL);
						System.out.println("Before http call");
						androidHttpTransport.call(wb.NAMESPACE
								+ "OrderInspection", envelope);
						order_result = (SoapObject) envelope.getResponse();
						System.out.println("OrderInspection result is"
								+ order_result);

						show_handler = 5;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 5) {
							show_handler = 0;
							String Status = String.valueOf(order_result.getProperty("Status"));
							String SRID = String.valueOf(order_result.getProperty("SRID"));							
							if (Status.equals("1") && !SRID.equals("anyType{}")) {
								
							System.out.println("import policyholder");
                         	ImportPlaceorderdetails(SRID);							

							} else {
								cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							}
						}
					}				
				};
			}.start();
		} else {
			cf.ShowToast("Internet connection not available");
		}
	}
	private void ImportPlaceorderdetails(final String SRID) {
		// TODO Auto-generated method stub
		
		 if(wb.isInternetOn()==true) {
			 System.out.println("came insode import");
				cf.show_ProgressDialog("Processing...");
				new Thread() {
					public void run() {
						try {
							
									ImportPolicyHolder(SRID);
									if(importph==true)
									{
										usrcheck = 1;
										handler3.sendEmptyMessage(0);
									}
									else
									{
										usrcheck = 3;
										handler3.sendEmptyMessage(0);
									}
								

						} catch (SocketTimeoutException s) {
							usrcheck = 4;
							handler3.sendEmptyMessage(0);
						} catch (NetworkErrorException n) {

							usrcheck = 3;
							handler3.sendEmptyMessage(0);
						} catch (IOException io) {
							usrcheck = 3;
							handler3.sendEmptyMessage(0);
						} catch (XmlPullParserException x) {
							usrcheck = 3;
							handler3.sendEmptyMessage(0);
						}

						catch (Exception e) {
							
						}
						
					}
					private Handler handler3 = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (usrcheck == 3) {
								usrcheck = 0;
								cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

							} else if (usrcheck == 4) {
								usrcheck = 0;
								cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

							} else if (usrcheck == 2) {
								usrcheck = 0;
								//cf.ShowToast(status_userid, 1);
							}else if (usrcheck == 1) {
								cf.ShowToast("Order Placed Successfully");
								
								Intent in =new Intent(OrderInspection.this,PolicyholderInfo.class);
								in.putExtra("homeid", SRID);
								in.putExtra("status", "Scheduled");
								startActivity(in);
								finish();
								usrcheck = 0;
								
								
							}
						}
					};
				}.start();
			} else {
				cf.ShowToast("Internet Connection is not available");
			}
	}

	protected void ImportPolicyHolder(String SRID) throws NetworkErrorException,IOException, XmlPullParserException, SocketTimeoutException{
		// TODO Auto-generated method stub
		String  buildingsize="", email, cperson, insurancecompanyname,  homeid = "", firstname, Orderid,
				lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, status, substatus,
				companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname,  yearbuilt, nstories, 
				inspectiondate, inspectioncomment, inspectiontypeid,inspectiontime,
				MailingAddress,MailingAddress2,Mailingcity,MailingState,MailingCounty,Mailingzip, inspfee, inspdiscount,adrschk;

		db.CreateTable(2);
		try{
			SoapObject request = new SoapObject(wb.NAMESPACE, "UPDATEMOBILEDBWITHSRID");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;			
			request.addProperty("SRID", SRID);
			envelope.setOutputSoapObject(request);System.out.println("UPDATEMOBILEDBWITHSRID"+request);
			
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);	System.out.println("wb.url");
			androidHttpTransport.call(wb.NAMESPACE+"UPDATEMOBILEDBWITHSRID", envelope);System.out.println("wb.test");
			SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("test.url");
			System.out.println("result1"+result1);
	
			homeid = String.valueOf(result1.getProperty("SRID"));
			firstname = String.valueOf(result1.getProperty("FirstName"));
			lastname = String.valueOf(result1.getProperty("LastName"));
			middlename = (String.valueOf(result1.getProperty("MiddleName")).equals(" "))?"":String.valueOf(result1.getProperty("MiddleName"));
			addr1 = String.valueOf(result1.getProperty("Address1"));
			addr2 = (String.valueOf(result1.getProperty("Address2")).equals(" "))?"":String.valueOf(result1.getProperty("Address2"));
			city = String.valueOf(result1.getProperty("City"));
			state= String.valueOf(result1.getProperty("State"));
			country = String.valueOf(result1.getProperty("Country"));
			zip = String.valueOf(result1.getProperty("Zip"));
			homephone = (String.valueOf(result1.getProperty("HomePhone")).equals(" "))?"":String.valueOf(result1.getProperty("HomePhone"));
			cellphone = (String.valueOf(result1.getProperty("CellPhone")).equals(" "))?"":String.valueOf(result1.getProperty("CellPhone"));
			workphone = (String.valueOf(result1.getProperty("WorkPhone")).equals(" "))?"":String.valueOf(result1.getProperty("WorkPhone"));
			email = (String.valueOf(result1.getProperty("Email")).equals(" "))?"":String.valueOf(result1.getProperty("Email"));			
			cperson = (String.valueOf(result1.getProperty("ContactPerson")).equals(" "))?"":String.valueOf(result1.getProperty("ContactPerson"));
			ownerpolicyno = (String.valueOf(result1.getProperty("OwnerPolicyNo")).equals(" "))?"":String.valueOf(result1.getProperty("OwnerPolicyNo"));
			status = String.valueOf(result1.getProperty("Status"));
			substatus = String.valueOf(result1.getProperty("Substatus"));
			companyid = String.valueOf(result1.getProperty("CompanyId"));
			inspectorid = String.valueOf(result1.getProperty("InspectorId"));			
			yearbuilt = String.valueOf(result1.getProperty("YearBuilt"));
			nstories = String.valueOf(result1.getProperty("Nstories"));
			inspectiontypeid = String.valueOf(result1.getProperty("InspectionTypeId"));
			inspectiondate= String.valueOf(result1.getProperty("Inspectiondate"));
	        inspectiontime= String.valueOf(result1.getProperty("InspectionTime"));
	        inspectioncomment= (String.valueOf(result1.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals("N/A"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals(" "))?"":String.valueOf(result1.getProperty("InspectionComment"));
	        insurancecompanyname = (String.valueOf(result1.getProperty("InsuranceCompany")).equals(" "))?"":String.valueOf(result1.getProperty("InsuranceCompany"));
			buildingsize = String.valueOf(result1.getProperty("BuildingSize"));
			
			MailingAddress= (String.valueOf(result1.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingAddress"));
			MailingAddress2 = (String.valueOf(result1.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals("N/A"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals(" "))?"":String.valueOf(result1.getProperty("MailingAddress2"));
			Mailingcity = (String.valueOf(result1.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(result1.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(result1.getProperty("Mailingcity"));
			MailingState = (String.valueOf(result1.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingState"));
			MailingCounty = (String.valueOf(result1.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingCounty"));				
			Mailingzip = (String.valueOf(result1.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(result1.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(result1.getProperty("Mailingzip"));
			
			inspfee = String.valueOf(result1.getProperty("Inspectionfee"));
			inspdiscount =String.valueOf(result1.getProperty("Discount"));
			Orderid=String.valueOf(result1.getProperty("Orderid"));

			if(status.equals("30") || status.equals("40"))
			{
				status= "40";
			}
			
			db.wind_db.execSQL("INSERT INTO "
								+ db.policyholder
								+ " (PH_SRID,PH_FirstName,PH_LastName,PH_MiddleName,PH_Address1,PH_Address2,PH_City,PH_State,PH_County,PH_Zip,YearBuilt,PH_HomePhone," +
								"PH_WorkPhone,PH_CellPhone,PH_ContactPerson ,PH_Policyno,PH_Email,PH_NOOFSTORIES," +
								"PH_Status,PH_SubStatus,PH_InspectorId," +
								"Schedule_Comments,PH_InspectionTypeId,InspectionDate,InspectionTime,InsuranceCompanyname,chkbx," +
								"BuildingSize,Fee,Discount,Orderid)"
								+ " VALUES ('" + homeid + "','"+ db.encode(firstname) + "','"+ db.encode(lastname) + "','"+ db.encode(middlename) + "'," +
										"'"+db.encode(addr1)+"','"+db.encode(addr2)+"','"+db.encode(city)+"','"+db.encode(state)+"','"+db.encode(county)+"'," +
										"'"+zip+"','"+yearbuilt+"','"+db.encode(homephone)+"','"+db.encode(workphone)+"','"+db.encode(cellphone)+"','"+db.encode(cperson)+"'," +
										"'"+db.encode(ownerpolicyno)+"','"+db.encode(email)+"','"+nstories+"'," +
										"'"+status+"','"+substatus+"','"+inspectorid+"'," +
										"'"+db.encode(inspectioncomment)+"','"+inspectiontypeid+"','"+db.encode(inspectiondate)+"','"+db.encode(inspectiontime)+"','"+db.encode(insurancecompanyname)+"'," +
										"'0','"+buildingsize+"','"+db.encode(inspfee)+"','"+db.encode(inspdiscount)+"','"+Orderid+"')");
	
			db.CreateTable(23);
			if(db.encode(addr1.trim()).equals(db.encode(MailingAddress.trim())) && db.encode(addr2.trim()).equals(db.encode(MailingAddress2.trim()))&&
					db.encode(city.trim()).equals(db.encode(Mailingcity.trim())) && db.encode(state.trim()).equals(db.encode(MailingState.trim())) &&
					db.encode(county.trim()).equals(db.encode(MailingCounty.trim())) && zip.trim().equals(Mailingzip.trim()))
			{
				adrschk ="1";
			}
			else
			{
				adrschk ="0";
			}
			db.wind_db.execSQL("INSERT INTO "
								+ db.MailingPolicyHolder
								+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
								+ "VALUES ('"+homeid+"','"+inspectorid+"','"+adrschk+"','"+db.encode(MailingAddress)+"','"
							    + db.encode(MailingAddress2)+"','"+db.encode(Mailingcity)+"','"
							    +Mailingzip+"','"+db.encode(MailingState)+"','"
							    + db.encode(MailingCounty)+"')");
			importph=true;
		} catch (Exception e)
		{
			importph = false;
		    System.out.println("eepolic="+e.getMessage());
		}
			
	}
	
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(OrderInspection.this,
				new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}

	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				System.out.println("current date " + date1);
				System.out.println("selected date " + date2);
				if (date2.compareTo(date1) > 0) {
					System.out.println("inside date if");
					cf.ShowToast("Please select Current and Past Date");
					v.setText("");
				} else {
					System.out.println("inside date else");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	
	private void Calculate_Fees() {
		//noofbuild = Integer.parseInt(etnoofbuildings.getText().toString());
		if (etsquarefootage.getText().toString().trim().equals("")) {
			cf.ShowToast("Please enter Square Footage");
			etsquarefootage.requestFocus();
		} else {

			llcalculatefees.setVisibility(View.VISIBLE);

			squarefootagevalue = Float.parseFloat(etsquarefootage.getText()
					.toString());
			inspectionbasesq_feet = Float.parseFloat(strinspectionbasesq_feet);
			inspectionbase_fee = Float.parseFloat(strinspectionbase_fee);
			inspectionadditionalsq_feet = Float
					.parseFloat(strinspectionadditionalsq_feet);
			inspectionaddtional_fee = Float
					.parseFloat(strinspectionaddtional_fee);
			if (squarefootagevalue <= inspectionbasesq_feet) {
				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
			//	Calculate_discount();

			} else {
				if ((squarefootagevalue - inspectionbasesq_feet) <= inspectionadditionalsq_feet) {
					inspectionbase_fee = inspectionbase_fee
							+ inspectionaddtional_fee;

				} else if ((squarefootagevalue - inspectionbasesq_feet) > inspectionadditionalsq_feet) {
					inspectionbase_fee = inspectionbase_fee
							+ (((squarefootagevalue - inspectionbasesq_feet) / inspectionadditionalsq_feet) * inspectionaddtional_fee);

				}
				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
				System.out.println("inspection base fees is "+inspectionbase_fee);
				//Calculate_discount();
			}
		}
	}
	
	/*private void Display_Fees() {
		totalvalue = Float.parseFloat(etinspectionfees.getText().toString())
				- Float.parseFloat(etdiscount.getText().toString());
		System.out.println("The total value is " + totalvalue);
		 ettotalfees.setText(String.valueOf(totalvalue));
	}*/

	/*private void Calculate_discount() {
		System.out.println("Inside calculate discount");
		
		int companyid = spinnerimc.getSelectedItemPosition();
		String strcompanyid = arraycompanyid[companyid];

		strcouponcode = etcouponcode.getText().toString();

		System.out.println("The inspetion fees is "
				+ etinspectionfees.getText().toString());

		//LoadDiscount(strcouponcode, etinspectionfees.getText().toString(),strcompanyid, insptypeid);
	}*/

	public void LoadDiscount(final String couponcode,
			final String inspectionfee, final String companyid,
			final String inspectionid) {
		System.out.println("Inside load discount");
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>"
					+ "Calculating Discount..."
					+ " Please wait.</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = wb.Calling_WS_LoadInspectionType_Discount(
								couponcode, inspectionfee, companyid,
								inspectionid, "LoadInspectionType_Discount");
						System.out
								.println("response LoadInspectionType_Discount"
										+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("here is a problem on your application. Please contact Paperless administrator");
							
						} else if (show_handler == 5) {
							show_handler = 0;

							strdiscountvalue = String.valueOf(chklogin
									.getProperty("Discount"));
							discountvalue2 += Float.parseFloat(strdiscountvalue);
							//etdiscount.setText(String.valueOf(strdiscountvalue));
							//Display_Fees();

						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
			
		}
	}

	class myCheckBoxChnageClicker implements CheckBox.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub

			if (isChecked) {
				
				strinspaddress1 = etinspaddress1.getText().toString();
				strinspaddress2 = etinspaddress2.getText().toString();
				strcity = etcity.getText().toString();
				strzip = etzip.getText().toString();
				if (!strinspaddress1.equals("")) {
					if (!strstate.equals("--Select--")) {
						if (!strcounty.equals("--Select--")
								&& !strcounty.equals("")) {
							if (!strcity.equals("")) {
								if (!strzip.equals("")) {
									if ((strzip.length() == 5)) {

										call_county2 = false;
										zip2identifier = false;

										etmailaddress1.setText(strinspaddress1);
										etmailaddress2.setText(strinspaddress2);
										etcity2.setText(strcity);
										etzip2.setText(strzip);

										arraystateid2 = arraystateid;
										arraystatename2 = arraystatename;
										arraycountyid2 = arraycountyid;
										arraycountyname2 = arraycountyname;

										spinnerstate2.setAdapter(stateadapter);
										spinnercounty2
												.setAdapter(countyadapter);
										spinnerstate2.setSelection(spinnerstate
												.getSelectedItemPosition());

									} else {
										cf.ShowToast("Please enter valid Zip");
										etzip.requestFocus();
										cbaddresscheck.setChecked(false);
									}
								} else {
									cf.ShowToast("Please enter Zip");
									etzip.requestFocus();
									cbaddresscheck.setChecked(false);
								}
							} else {
								cf.ShowToast("Please enter City");
								etcity.requestFocus();
								cbaddresscheck.setChecked(false);
							}
						} else {
							cf.ShowToast("Please select County");
							cbaddresscheck.setChecked(false);
						}
					} else {
						cf.ShowToast("Please select State");
						cbaddresscheck.setChecked(false);
					}
				} else {
					cf.ShowToast("Please enter Inspection Address #1");
					etinspaddress1.requestFocus();
					cbaddresscheck.setChecked(false);
				}
			} else {
				zip2identifier = true;
				etmailaddress1.setText("");
				etmailaddress2.setText("");
				etcity2.setText("");
				etzip2.setText("");
				spinnerstate2.setSelection(stateadapter.getPosition(strselect));

				call_county2 = true;
			}

		}

	}


	public void LoadInspectionTypeDescription(final String id,
			final String companyid) {

		System.out.println("Inside Load Inspection Types");
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing. Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = wb.Calling_WS_LoadInspectionTypeDescription(
								id, strcompanyid, "LoadInspectionTypeDetails");
						System.out
								.println("response LoadInspectionTypedescriptionDetails"
										+ chklogin);
						// LoadInspectionTypeDescription(chklogin);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							
						} else if (show_handler == 5) {
							show_handler = 0;

							if (chklogin.getPropertyCount() < 1) {
								System.out.println("Inside if");
								cf.ShowToast("Company not in list");
							} else {
								System.out.println("Inside else");
								LoadInspectionTypeDescription(chklogin);
							}
						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
			
		}
	}

	public void LoadInspectionTypeDescription(SoapObject chklogin) {

		SoapObject objInsert = (SoapObject) chklogin.getProperty(0);

		String Description = String.valueOf(objInsert
				.getProperty("Description"));
		ImageName = String.valueOf(objInsert.getProperty("ImageName"));
		strinspectionbasesq_feet = String.valueOf(objInsert
				.getProperty("Base_Sqft"));
		strinspectionbase_fee = String.valueOf(objInsert
				.getProperty("Base_Fee"));
		System.out.println("inspectionbase_fee is " + strinspectionbase_fee);
		strinspectionadditionalsq_feet = String.valueOf(objInsert
				.getProperty("Additional_Sqft"));
		strinspectionaddtional_fee = String.valueOf(objInsert
				.getProperty("Additional_Fee"));

		description.setText(Description);
		System.out.println("The Image Name is " + ImageName);

		// Drawable drawable = LoadImageFromWebOperations(ImageName);
		// descriptionimage.setImageDrawable(drawable);

		new DownloadImageTask(descriptionimage).execute(ImageName);

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog mDialog;
		private ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected void onPreExecute() {

			// mDialog =
			// ProgressDialog.show(ChartActivity.this,"Please wait...",
			// "Retrieving data ...", true);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", "image download error");
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			// set image of your imageview
			bmImage.setImageBitmap(result);
			// close
			// mDialog.dismiss();
		}
	}


	public String LoadState() {
		try {
			dbh1 = new DataBaseHelper1(OrderInspection.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = db.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = db.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(OrderInspection.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + db.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = db.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = db.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(OrderInspection.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}

	public void LoadCounty2(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(OrderInspection.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + db.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid2 = new String[rows + 1];
			arraycountyname2 = new String[rows + 1];
			arraycountyid2[0] = "--Select--";
			arraycountyname2[0] = "--Select--";
			System.out.println("LoadCounty2 count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = db.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = db.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid2[i] = id;
					arraycountyname2[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData2();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData2() {
		countyadapter2 = new ArrayAdapter<String>(OrderInspection.this,
				android.R.layout.simple_spinner_item, arraycountyname2);
		countyadapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty2.setAdapter(countyadapter2);
		
		if(countysetselection)
		{
			spinnercounty2.setSelection(countyadapter2.getPosition(county));
			countysetselection=false;
		}
		
	}

	
	/*private void Check_Couponcode()
	{
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing. Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				String chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = wb.Calling_WS_CheckCouponcode(etcouponcode.getText().toString().trim(),"CHECKCOUPONID");
						System.out.println("response CHECKCOUPONID" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network.", 0);
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator.", 0);

						} else if (show_handler == 5) {
							show_handler = 0;
							if(chklogin1.toLowerCase().equals("false"))
							{
								etcouponcode.setText("");
								etcouponcode.requestFocus();
								cf.ShowToast("Coupon code is not valid", 0);
							}
						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available", 0);

		}
	}
	*/
	private void Load_State_County_City(final EditText et)
	{

		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing. Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = wb
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							
						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								cf.ShowToast("Please enter a valid Zip");
								cf.hidekeyboard(((EditText)findViewById(R.id.orderinspection_etzip)));
								
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
		}
	
	}
	
	private void Load_State_County_City(SoapObject objInsert)
	{
		    cf.hidekeyboard(((EditText)findViewById(R.id.orderinspection_etzip)));
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
			if(zipidentifier.equals("zip1"))
			{
				spinnerstate.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity.setText(city);
			}
			else if(zipidentifier.equals("zip2"))
			{
				spinnerstate2.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity2.setText(city);
			}
			
	}
	
	
    private Drawable LoadImageFromWebOperations(String url) {
		// TODO Auto-generated method stub
		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (Exception e) {
			System.out.println("Exc=" + e);
			return null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent inthome = new Intent(OrderInspection.this, HomeScreen.class);
		inthome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(inthome);
		finish();
	}
}
