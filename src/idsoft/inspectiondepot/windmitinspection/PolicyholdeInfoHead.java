package idsoft.inspectiondepot.windmitinspection;



import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

public class PolicyholdeInfoHead extends Activity implements AnimationListener {
String homeid,insp_id,table_name;
CommonFunctions cf;
DatabaseFunctions db;
boolean b=false;
public Animation myFadeInAnimation;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle b = getIntent().getExtras();
		setContentView(R.layout.infohead);
		String type="";
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		if(b!=null)
		{
			homeid=b.getString("homeid");
			System.out.println("homeid"+homeid);
			type=b.getString("Type");
			insp_id=db.Insp_id;
			
			if(type==null)
			{
				finish();
			}
		}
		System.out.println(" the policy holde"+type);
		if(type.equals("Policyholder"))
		{
			
			try
			{
			Show_ph_info();
			}
			catch (Exception e) {
				// TODO: handle exception
				finish();
			}
		}
		else if(type.equals("Inspector"))
		{     
			try
			{
				Show_insp_info();
			}
			catch (Exception e) {
				// TODO: handle exception
				finish();
			}
		}
		
		db.getInspectorId();
	    TranslateAnimation trans=new TranslateAnimation(310,0,0,0); 
	       trans.setDuration(500);
	       trans.setFillAfter(true);
	       ((ScrollView) findViewById(R.id.anim_con)).setAnimation(trans);
	       trans.start();
	    
	}
	private void Show_ph_info() {
		// TODO Auto-generated method stub
		((TableLayout) findViewById(R.id.ph_info_tbl)).setVisibility(View.VISIBLE);
		
		Cursor c=db.wind_db.rawQuery(" Select PH_FirstName||' '||PH_LastName as name,PH_Address1||' '|| PH_Address2 as address,PH_City,PH_County,PH_State,PH_HomePhone,PH_Policyno FROM "+db.policyholder+" Where PH_SRID='"+homeid+"' and PH_InspectorId='"+insp_id+"' limit 1 ", null);
		System.out.println("c.getCount()"+c.getCount());
		if(c.getCount()>0)
		{ c.moveToFirst();
			if(c.getString(c.getColumnIndex("name"))!=null)
				((TextView) findViewById(R.id.ph_name)).setText(db.decode(c.getString(c.getColumnIndex("name"))).trim());
			if(c.getString(c.getColumnIndex("address"))!=null)
				((TextView) findViewById(R.id.ph_address)).setText(db.decode(c.getString(c.getColumnIndex("address"))).trim());
			if(c.getString(c.getColumnIndex("PH_City"))!=null)
				((TextView) findViewById(R.id.ph_city)).setText(db.decode(c.getString(c.getColumnIndex("PH_City"))).trim());
			if(c.getString(c.getColumnIndex("PH_County"))!=null)
				((TextView) findViewById(R.id.ph_county)).setText(db.decode(c.getString(c.getColumnIndex("PH_County"))).trim());
			if(c.getString(c.getColumnIndex("PH_State"))!=null)
				((TextView) findViewById(R.id.ph_state)).setText(db.decode(c.getString(c.getColumnIndex("PH_State"))).trim());
			if(c.getString(c.getColumnIndex("PH_HomePhone"))!=null)
				((TextView) findViewById(R.id.ph_ph_no)).setText(db.decode(c.getString(c.getColumnIndex("PH_HomePhone"))).trim());
			if(c.getString(c.getColumnIndex("PH_Policyno"))!=null)
				((TextView) findViewById(R.id.ph_p_no)).setText(db.decode(c.getString(c.getColumnIndex("PH_Policyno"))).trim());
			
		}

	}
	private void Show_insp_info() {     
		// TODO Auto-generated method stub
		((TableLayout) findViewById(R.id.insp_info_tbl)).setVisibility(View.VISIBLE);
		
		
		Cursor c=db.wind_db.rawQuery(" Select Ins_FirstName||'  '||Ins_LastName	 as name,Ins_Address1 as address,Ins_CompanyName,Ins_CompanyId,Ins_Email FROM "+db.inspectorlogin+" Where Ins_Id='"+insp_id+"' limit 1 ", null);
		if(c.getCount()>0)
		{ c.moveToFirst();
			if(c.getString(c.getColumnIndex("name"))!=null)
				((TextView) findViewById(R.id.insp_name)).setText(db.decode(c.getString(c.getColumnIndex("name"))).trim());
			if(c.getString(c.getColumnIndex("address"))!=null)
				((TextView) findViewById(R.id.insp_address)).setText(db.decode(c.getString(c.getColumnIndex("address"))).trim());
			
			if(c.getString(c.getColumnIndex("Ins_CompanyName"))!=null)
				((TextView) findViewById(R.id.insp_company)).setText(db.decode(c.getString(c.getColumnIndex("Ins_CompanyName"))).trim());
			if(c.getString(c.getColumnIndex("Ins_Email"))!=null)
				((TextView) findViewById(R.id.insp_email)).setText(db.decode(c.getString(c.getColumnIndex("Ins_Email"))).trim());
			/*if(c.getString(c.getColumnIndex("Ins_CompanyId"))!=null)
				((TextView) findViewById(R.id.insp_company_id)).setText(db.decode(c.getString(c.getColumnIndex("Ins_CompanyId"))).trim());*/
			
			
			try {
				System.out.println("d"+this.getFilesDir());
				File outputFile = new File(this.getFilesDir()+"/"+"Headshot"+db.Insp_id.toString()+".jpg");
				System.out.println("outputFile="+outputFile);
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				System.out.println("bitmap = "+bitmap);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				((ImageView) findViewById(R.id.insp_photo)).setImageDrawable(bmd);
				
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the value in the s=  ffds  ff"+e.getMessage());
			}
			
			
			try {
				System.out.println("d"+this.getFilesDir());
				File outputFile1 = new File(this.getFilesDir()+"/"+"CompanyLogo"+db.Insp_id.toString()+".jpg");
				System.out.println("outputFile1="+outputFile1);
				BitmapFactory.Options o1 = new BitmapFactory.Options();
				o1.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile1),
						null, o1);
				final int REQUIRED_SIZE = 200;
				int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
				int scale1 = 1;
				while (true) {
					if (width_tmp1 / 2 < REQUIRED_SIZE
							|| height_tmp1 / 2 < REQUIRED_SIZE)
						break;
					width_tmp1 /= 2;
					height_tmp1 /= 2;
					scale1 *= 2;
				}
				BitmapFactory.Options o3 = new BitmapFactory.Options();
				o3.inSampleSize = scale1;
				Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
						outputFile1), null, o3);
				System.out.println("bitmap1 = "+bitmap1);
				BitmapDrawable bmd1 = new BitmapDrawable(bitmap1);
				((ImageView) findViewById(R.id.comp_photo)).setImageDrawable(bmd1);
				
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the ssue in company logi ff"+e.getMessage());
			}
			
			//File f =new File(+insp_id+Ext);
			
		}
	}
	
	public void clicker(View v)
   {
		finish();
	}
	
	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		
			//((RelativeLayout) findViewById(R.id.ph_head_scr)).setBackgroundColor(getResources().getColor(R.color.transpahrrant));
		
	}
	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
			//((RelativeLayout) findViewById(R.id.ph_head_scr)).setBackgroundColor(getResources().getColor(R.color.transpahrrant));
		
	}
	
	
		  
}
