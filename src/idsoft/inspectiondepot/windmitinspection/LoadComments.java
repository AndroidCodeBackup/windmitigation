/*
   ************************************************************
   * Project: ALL RISK REPORT
   * Module : Submit.java
   * Creation History:
      Created By : Rakki s on 11/22/2012
   * Modification History:
      Last Modified By : Gowri on 12/15/2012
   ************************************************************  
*/

package idsoft.inspectiondepot.windmitinspection;

import java.util.ArrayList;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoadComments extends Activity {
CommonFunctions cf;
String insp_name="",insp_type="",insp_ques="",insp_opt="";
String[] s={"-Select-","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1","test1"};
int fromx=0,fromy=0;
int inti=0,animat=0;
String selectedtag="";
int lenth=0,max_length=500;
DatabaseFunctions db;
Bundle b;
LinearLayout com_li_fill;
RelativeLayout com_li;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load_comments);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		db.getInspectorId();
		cf.getDeviceDimensions();
	
		com_li_fill=(LinearLayout) findViewById(R.id.com_com_li);
		com_li=(RelativeLayout) findViewById(R.id.contentwrp);
		b=getIntent().getExtras();
		if(b!=null)
		{
			insp_ques=b.getString("insp_ques");
			insp_opt=b.getString("insp_opt");
			System.out.println("insp_name="+insp_ques+"\n"+insp_opt);
			fromx=b.getInt("xfrom");
			fromy=b.getInt("yfrom");
			lenth=b.getInt("length");
			max_length=b.getInt("max_length");
			
		}
	/*System.out.println("the quesry = WHERE CL_inspid='"+cf.Insp_id+"' and CL_inspectionName='"+cf.encode(insp_name)+"' " +
				"and CL_type='"+cf.encode(insp_type)+"' and CL_question='"+cf.encode(insp_ques)+"' and CL_option='"+cf.encode(insp_opt)+"' AND CL_Active='true' order by CL_Mdate");*/	
	
		db.CreateTable(5);
		try
		{
			Cursor c =	db.SelectTablefunction(db.StandardComments, " WHERE SC_InspectorId='"+db.Insp_id+"' and SC_QuesID='"+db.encode(insp_ques)+"' " +
					"and SC_OptionID='"+db.encode(insp_opt)+"' AND SC_Status='0'" );
			System.out.println("ccc=)"+c.getCount());
			s= new String[c.getCount()];
			if(c.getCount()>0)
			{int j=0;
			c.moveToFirst();
				do
				{
					s[j]=db.decode(c.getString(c.getColumnIndex("SC_Description")));
					j++;
				}while(c.moveToNext());
				TextView tv;
				LinearLayout li;
				CheckedTextView ch;
				LinearLayout tbl= (LinearLayout) findViewById(R.id.com_lis);
				tbl.removeAllViews();
				for(int i=0;i<s.length;i++)
				{
					
					li=new LinearLayout(this);
					li.setGravity(Gravity.CENTER_VERTICAL);
					//li.setOnClickListener(new liClick(i,0));
					li.setOrientation(LinearLayout.HORIZONTAL);
					li.setPadding(10, 5, 10, 5);
					 tv= new TextView(this);
					// tv.setText((i+1)+". ");
					 tv.setTextColor(Color.BLACK);
					 li.addView(tv,20,LayoutParams.WRAP_CONTENT);
					 
					 LinearLayout li_tv= new LinearLayout(this);
					 
					/* tv= new TextView(this);
					 tv.setText(s[i]);
					 tv.setWidth(0);
					 
					 tv.setTextColor(Color.BLACK);
					 li_tv.addView(tv,cf.wd-380,LayoutParams.WRAP_CONTENT);
					 li.addView(li_tv,cf.wd-380,LayoutParams.WRAP_CONTENT);*/
					 
					 ch= new CheckedTextView(this,null,R.attr.checked_text);
					 ch.setTag("checkbox_"+i);
					 ch.setText(s[i]);
					 ch.setTextColor(Color.BLACK);
					 ch.setOnClickListener(new liClick(i,1));
					 ch.setGravity(Gravity.CENTER_VERTICAL);
					 li.addView(ch,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					 tbl.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					 if(i<s.length-1)
					 {
						 View v=new View(this);
						 v.setBackgroundColor(getResources().getColor(R.color.sub_menu));
						 tbl.addView(v,LayoutParams.FILL_PARENT,1);
					 }
					 RelativeLayout.LayoutParams li_p=new RelativeLayout.LayoutParams(cf.wd-300,LayoutParams.FILL_PARENT);
					 ((LinearLayout) findViewById(R.id.com_com_li)).setLayoutParams(li_p);
					
				}
			}else
			{
				finish();
				cf.ShowToast("Comments not available");
				
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
	
	
		
	}
	class liClick implements android.view.View.OnClickListener
	{
		int id,type;
		liClick(int id,int type)
		{
			this.id=id;
			this.type=type;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			/*System.out.println("there iss isses "+v.getTag()+"the type "+type);
			if(type==0)
			{
				if(((CheckedTextView) v.findViewWithTag("checkbox_"+id)).isChecked())
				{
					((CheckedTextView) v.findViewWithTag("checkbox_"+id)).setChecked(false);
					selectedtag.replace("checkbox_"+id+"~", "");
					
				}
				else
				{
					((CheckedTextView) v.findViewWithTag("checkbox_"+id)).setChecked(true);
					selectedtag="checkbox_"+id+"~";
				}
				
				
			}
			else
			{*/
			
			
				if(!((CheckedTextView) v).isChecked())
				{
					if(lenth>=max_length)
					{
						cf.ShowToast("You have exceeded the maximum limit");
						
					}
					else
					{
						selectedtag+="checkbox_"+id+"~";
						((CheckedTextView) v).setChecked(true);
						lenth=lenth+s[id].length();
					}
				}
				else
				{
					
					selectedtag=selectedtag.replace("checkbox_"+id+"~", "");
					lenth=lenth-s[id].length();
					((CheckedTextView) v).setChecked(false);
				}
			
			//}
			
			System.out.println("the elected are "+selectedtag);
				/*Intent in = new Intent();
				b.putString("Comments",s[id]);
				in.putExtras(b);
				setResult(RESULT_OK, in);*/
			
		//	Scaleoutanimation(fromx,fromy);
				
		
		}
		
	}
	
	public void clicker(View v)
	{
		if(v.getId()==R.id.load_com_close)
		{
			if(animat==0)
			{
				Intent in = new Intent();
				b.putString("Comments","");
				in.putExtras(b);
				setResult(RESULT_CANCELED, in);
				Scaleoutanimation(fromx,fromy);
				//cf.ShowToast("you have not  selcted any comments", 0);
			}
		}
		else if(v.getId()==R.id.ok)
		{
			if(selectedtag.length()>1)
			{
				selectedtag=selectedtag.substring(0,selectedtag.length()-1);
				String[] sel=selectedtag.split("~");
				String comts="";
				for(int i=0;i<sel.length;i++)
				{
					
					sel[i]=sel[i].replace("checkbox_", "");
					if(comts.equals(""))
					{
						comts+=s[Integer.parseInt(sel[i])];
					}
					else
					{
						comts+=" "+s[Integer.parseInt(sel[i])];
					}
				}
				Intent in = new Intent();
				b.putString("Comments",comts);
				in.putExtras(b);
				setResult(RESULT_OK, in);
			
				Scaleoutanimation(fromx,fromy);
			}
			else
			{
				cf.ShowToast("Please select atleast one comment");
			}
		}
		else
		{
			
		}
	}
	public void Scaleinanimation(float xfrom, float yfrom) {
		AnimationSet an= new AnimationSet(false);
        ScaleAnimation scale = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f,com_li_fill.getWidth()/2,com_li_fill.getHeight()/2);
       scale.setDuration(500);
       an.addAnimation(scale);
        TranslateAnimation trans=new TranslateAnimation(TranslateAnimation.ABSOLUTE,xfrom-(com_li_fill.getWidth()/2), TranslateAnimation.ABSOLUTE,0,
    		   TranslateAnimation.ABSOLUTE,yfrom-(com_li_fill.getHeight()/2),TranslateAnimation.ABSOLUTE,0); 
       trans.setDuration(500);
       trans.setStartOffset(10);
       an.setFillAfter(true);
       an.addAnimation(trans);
       an.setAnimationListener(new animat_lis(1));
       com_li_fill.startAnimation(an);
    }
	public void Scaleoutanimation(float xfrom, float yfrom) {
	      
		
		
	       AnimationSet an= new AnimationSet(false);
	       ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f,com_li_fill.getWidth()/2,com_li_fill.getHeight()/2);
	       scale.setDuration(500);
	       an.addAnimation(scale);
	       TranslateAnimation trans=new TranslateAnimation(TranslateAnimation.ABSOLUTE,0, TranslateAnimation.ABSOLUTE,xfrom-(com_li_fill.getWidth()/2),
	    		   TranslateAnimation.ABSOLUTE,0,TranslateAnimation.ABSOLUTE,yfrom-(com_li_fill.getHeight()/2)); 
	       trans.setDuration(500);
	       trans.setStartOffset(10);
	       an.setFillAfter(true);
	       an.addAnimation(trans);
	       com_li_fill.startAnimation(an);
	       an.setAnimationListener(new animat_lis(0));
	       
	    }
		
	
	public void onWindowFocusChanged(boolean hasFocus)
	{
		if(inti==0)
		{
			Scaleinanimation(fromx,fromy);
		 inti=1;
		}
	}
	class animat_lis implements AnimationListener
	{int type;
		animat_lis(int type)
		{
			this.type=type;
		}
		
		@Override
		public void onAnimationStart(Animation animation) {
			// TODO Auto-generated method stub
			animat=1;
		}
		
		@Override
		public void onAnimationRepeat(Animation animation) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onAnimationEnd(Animation animation) {
			// TODO Auto-generated method stub
			animat=0;
			if(type==0)
			{
				finish();
			}
		}

		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			
			/*if(animat==0)
			{
				Intent in = new Intent();
				in.putExtra("Comments","");
				setResult(RESULT_CANCELED, in);
				Scaleoutanimation(fromx,fromy);
				//LinearLayout scr=(LinearLayout) findViewById(R.id.cont);
		    	//View v1 = scr.getRootView();
		        
				//cf.ShowToast("you have not  selcted any comments", 0);
			}
			*/
			return true;

		}
		return super.onKeyDown(keyCode, event);
	}
	/* private void splitImage(Bitmap bitmap, int chunkNumbers) { 
		 Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
		 ((RelativeLayout) findViewById(R.id.spli_img)).setVisibility(View.VISIBLE);
	       com_li.setVisibility(View.GONE);
		
		 	ImageView[] im=new ImageView[9];
			
	        im[0]=(ImageView) findViewById(R.id.split1);
	        im[1]=(ImageView) findViewById(R.id.split2);
	        im[2]=(ImageView) findViewById(R.id.split3);
	        im[3]=(ImageView) findViewById(R.id.split4);
	        im[4]=(ImageView) findViewById(R.id.split5);
	        im[5]=(ImageView) findViewById(R.id.split6);
	        im[6]=(ImageView) findViewById(R.id.split7);
	        im[7]=(ImageView) findViewById(R.id.split8);
	        im[8]=(ImageView) findViewById(R.id.split9);
	        
	        //For the number of rows and columns of the grid to be displayed
	        int rows,cols;
	        
	        //For height and width of the small image chunks 
	        int chunkHeight,chunkWidth;
	  
	        //To store all the small image chunks in bitmap format in this list 
	        ArrayList<Bitmap> chunkedImages = new ArrayList<Bitmap>(chunkNumbers);
	  
	        //Getting the scaled bitmap of the source image
	         
	  
	        rows = cols = (int) Math.sqrt(chunkNumbers);
	        chunkHeight = bitmap.getHeight()/rows;
	        chunkWidth = bitmap.getWidth()/cols;
	       
	        //xCoord and yCoord are the pixel positions of the image chunks
	        int yCoord = 0;
	        for(int x=0; x<rows; x++){
	            int xCoord = 0;
	            for(int y=0; y<cols; y++){
	                chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));
	                xCoord += chunkWidth;
	                
	            }
	            yCoord += chunkHeight;
	        }

	         Now the chunkedImages has all the small image chunks in the form of Bitmap class. 
	         * You can do what ever you want with this chunkedImages as per your requirement.
	         * I pass it to a new Activity to show all small chunks in a grid for demo.
	         * You can get the source code of this activity from my Google Drive Account.
	         
	 
	        //Start a new activity to show these chunks into a grid 
	        ScaleAnimation sc =new ScaleAnimation(1f, 0f, 1f, 0f,chunkWidth/2,chunkHeight/2);
	        sc.setDuration(500);
	       for(int i =0;i<chunkNumbers;i++)
	       {
	    	   Drawable drawable = new BitmapDrawable(chunkedImages.get(i));
	    	   im[i].setBackgroundDrawable(drawable);
	    	   im[i].setAnimation(sc);
	       }
	       
	       sc.setFillAfter(true);
	       sc.start();
	       sc.setAnimationListener(new animat_lis(0));
	       
	       
	    }*/
}