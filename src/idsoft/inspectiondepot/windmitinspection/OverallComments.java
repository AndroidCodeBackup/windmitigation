package idsoft.inspectiondepot.windmitinspection;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OverallComments extends Activity {
	EditText comments;
	Button saveclose;
	String overallcomm;
	TextView txtoverallheading;
	TextView over_type1;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	
	TextWatcher watcher;
	ImageView img1, img2, img3, img4, img6, img7, img8, img9, buildcode,
			roofcover, roofdeck, roofwall, roofgeometry, swr, openprotection,
			wallconstruction;;
	TableLayout exceedslimit1;View v1;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db = new DatabaseFunctions(this);
		wb = new WebserviceFunctions(this);
		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		db.getInspectorId();

		Bundle extras = getIntent().getExtras();
		   if (extras != null) {
				cf.getExtras(extras);				
		   }
		   System.out.println("OverallComments="+cf.status);
		setContentView(R.layout.overallcomments);
		db.getInspectorId();
		cf.getDeviceDimensions();

		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);		  
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(OverallComments.this, 5, cf, 0));
	
		
		declaration();
		setvalue();		
	}
	
	private void declaration() {
		// TODO Auto-generated method stub
		this.txtoverallheading = (TextView) this.findViewById(R.id.txtoverallheading);
		txtoverallheading
				.setText(Html
						.fromHtml("Note: This comment box should be used to describe any other information relating to your inspection that did not directly affect the mitigation features reported. "));
		int maxwidth = (cf.wd)-50;
		txtoverallheading.setMaxWidth(maxwidth);
		
		over_type1=(TextView) findViewById(R.id.overall_text);			
		comments = (EditText) this.findViewById(R.id.txtoverallcomments);
		comments.addTextChangedListener(new TextWatchLimit(comments,1000,over_type1));
	}

	private void setvalue() {
		// TODO Auto-generated method stub
		db.getQuesComments(cf.Homeid);
		comments.setText(db.overallcomments);
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			overallcomm = comments.getText().toString();
			if (overallcomm.trim().equals(""))
			{
				cf.ShowToast("Please enter the Overall Comments");
			} 
			else
			{
					try 
					{
						Cursor c2 = db.SelectTablefunction(db.QuestionsComments, "where SRID='"+ cf.Homeid + "'");
						if (c2.getCount() == 0) {
							db.wind_db.execSQL("INSERT INTO "
									+ db.QuestionsComments
									+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','','','','','','"+db.encode(overallcomm)+"','"+ cf.datewithtime + "')");
						} else
						{
							db.wind_db.execSQL("UPDATE " + db.QuestionsComments
									+ " SET InsOverAllComments='"+db.encode(overallcomm)+ "',CreatedOn ='" + cf.datewithtime + "'" + " WHERE SRID ='" + cf.Homeid.toString() + "'");
						}
					
					
				
					Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "+ db.SubmitCheckTable + " WHERE Srid='"+ cf.Homeid + "'", null);
					if (c3.getCount() == 0)
					{
						db.wind_db.execSQL("INSERT INTO "
								+ db.SubmitCheckTable
								+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
								+ "VALUES ('" + db.Insp_id + "','"
								+ cf.Homeid
								+ "',0,0,0,0,0,0,0,0,0,0,0,0,0,1,0)");
					} 
					else
					{
						db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
								+ " SET fld_overall=1 WHERE Srid ='"
								+ cf.Homeid + "' and InspectorId='"
								+ db.Insp_id + "'");
					}

				
						cf.ShowToast("OverAll Comments has been saved successfully");
						Intent genimg = new Intent(OverallComments.this,
								GeneralHazInfo.class);
						genimg.putExtra("homeid", cf.Homeid);
						genimg.putExtra("status", cf.status);
						startActivity(genimg);
						finish();
				

				} catch (Exception e) {
					System.out.println("erroe "+e.getMessage());
					cf.ShowToast("There is a problem in saving your data due to invalid character");
				}
			}
			break;
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(OverallComments.this,
					FeedbackDocument.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	
}
