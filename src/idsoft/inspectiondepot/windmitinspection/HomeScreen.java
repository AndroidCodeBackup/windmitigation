package idsoft.inspectiondepot.windmitinspection;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class HomeScreen extends Activity {
	CommonFunctions cf;
	private ProgressDialog pd;
	byte[] raw;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	public static int RUNNING = 1;
	int messsage,delay = 40,typeBar,mState,val_handler,eval_handler;
	int maxBarValue = 0; // Maximum value of horizontal progress bar
	public boolean value;
	ProgressThread progThread;
	String newcode,newversion,apk_uri="",url,username,password,userid;
	int usercheck,vcode,total,show_handler;
	String versionname, newversionname="", newversioncode,dbversionname;
	AlertDialog alertDialog;
	Dialog dialog1;
	RelativeLayout rl;
	View v1;
	ScaleAnimation zoom;
	AnimationSet animSet;
	ProgressDialog progDialog;
	ImageView checkforupdates,min,exp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		setContentView(R.layout.home);
		progDialog = new ProgressDialog(this);
        db.getInspectorId();
		
        checkforupdates = ((ImageView)findViewById(R.id.checkforupdates));
        
		zoom = new ScaleAnimation((float)1.00,(float)0.90, (float)1.00,(float)0.90,(float) 100,(float) 20); 
		animSet = new AnimationSet(false);
		zoom.setRepeatMode(Animation.REVERSE);
		zoom.setRepeatCount(Animation.INFINITE);
		animSet.addAnimation(zoom);
		animSet.setDuration(300);
		AlphaAnimation alpha = new AlphaAnimation(1, (float)0.9);
        alpha.setDuration(300); // Make animation instant
        alpha.setRepeatMode(Animation.REVERSE);
        alpha.setRepeatCount(Animation.INFINITE);
        animSet.addAnimation(alpha);
        db.CreateTable(0);
       /* System.out.println("animation flag "+WindMitInspection.animationflag);
        if(WindMitInspection.animationflag)
        {
        	check_for_updates();
        	WindMitInspection.animationflag=false;
        	check_for_animation();
        }
        */
        db.getInspectorId();
    	((TextView) findViewById(R.id.insp_name)).setText(db.Insp_firstname+" "+db.Insp_lastname);
		check_for_animation();
		
		
		((TextView)findViewById(R.id.editinspector)).setText(Html.fromHtml("<u>Edit Inspector Profile</u>"));
		((TextView)findViewById(R.id.editinspector)).setOnClickListener((OnClickListener) new clicker1());
		
       ((ImageView)findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(HomeScreen.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);System.out.println("ends");
				//finish();
			}
		});
       
       try {
			System.out.println("d"+this.getFilesDir());
			File outputFile = new File(this.getFilesDir()+"/"+"Headshot"+db.Insp_id.toString()+".jpg");
			System.out.println("outputFile="+outputFile);
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(outputFile),
					null, o);
			final int REQUIRED_SIZE = 200;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
					outputFile), null, o2);
			System.out.println("bitmap = "+bitmap);
			BitmapDrawable bmd = new BitmapDrawable(bitmap);
			((ImageView) findViewById(R.id.inspectorphoto)).setImageDrawable(bmd);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("the value in the s=  ffds  ff"+e.getMessage());
		}

	}
	class clicker1 implements OnClickListener {
		public void onClick(View v) {
			
			Intent in =new Intent(HomeScreen.this,Registration.class);
			in.putExtra("inspid",Integer.parseInt(db.Insp_id));
			startActivity(in);
			finish();
		}
	}
	private void check_for_animation() {
		// TODO Auto-generated method stub
		/** Start animating for the  Check for updates**/
		
			Cursor c12 = db.SelectTablefunction(db.B1802version, " ");   
			c12.moveToFirst();
		
			if (c12.getCount() >= 1) {
				if (c12.getInt(1)>getcurrentversioncode()) {
					try
					{
				    	checkforupdates.setAnimation(animSet);
							animSet.start();
					}
					catch(Exception e)
					{
						
					}
				
				} 
			}
			/** End animating for the  Check foru pdates**/
	}
	public void Clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.startinsp:
				Intent in =new Intent(this,Dashboard.class);
				startActivity(in);
				finish();
			break;
			
			case R.id.placeorder:
				/*if (wb.isInternetOn() == true) {
					cf.show_ProgressDialog("Validating...");
					new Thread() {
						public void run() {
							Looper.prepare();
							
							try
							{
								SoapObject request = new SoapObject(wb.NAMESPACE,"TOTALINSPECTIONCOUNT");
								SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
								envelope.dotNet = true;
								request.addProperty("InspectorID", db.Insp_id);
								
								envelope.setOutputSoapObject(request);
								System.out.println("TOTALINSPECTIONCOUNT request is "+ request);
								HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
								System.out.println("Before http call");
								androidHttpTransport.call(wb.NAMESPACE+ "TOTALINSPECTIONCOUNT", envelope);
							
								Object result= (Object)envelope.getResponse();
								System.out.println("TOTALINSPECTIONCOUNT result is"+ result);
								
								if(result.toString().equals("true"))
								{
									val_handler = 1;
									handler.sendEmptyMessage(0);
								}
								else
								{
									val_handler = 2;
									handler.sendEmptyMessage(0);
								}

								System.out.println("val_handler="+val_handler);
								
							}
							catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								val_handler = 3;
								handler.sendEmptyMessage(0);
							} catch (XmlPullParserException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								val_handler = 3;
								handler.sendEmptyMessage(0);
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
								val_handler = 4;
								handler.sendEmptyMessage(0);
							}
						}
						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
								cf.pd.dismiss();
								if (val_handler == 3) {
									val_handler = 0;
									cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
									
								} else if (val_handler == 4) {
									val_handler = 0;
									cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

								} else if (val_handler == 1) {
									System.out.println("inside");
									val_handler = 0;
									Intent intentorder=new Intent(HomeScreen.this,OrderInspection.class);
									startActivity(intentorder);
									finish();
								}
								else if (val_handler == 2) {
									val_handler = 0;
									
									final Dialog dialog1 = new Dialog(HomeScreen.this,android.R.style.Theme_Translucent_NoTitleBar);
									dialog1.getWindow().setContentView(R.layout.alert);
									
									RelativeLayout li=(RelativeLayout)dialog1.findViewById(R.id.helprow);
									li.setVisibility(View.VISIBLE);
									TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
									txttitle.setText("Demonstration Expired");
									
									LinearLayout lin=(LinearLayout)dialog1.findViewById(R.id.camera);
									lin.setVisibility(v1.VISIBLE);
									
									TextView txt = (TextView) dialog1.findViewById(R.id.txtquestio);
									txt.setText("You have now used up free inspection report allowances. \nTo purchase more or extend your demonstration please click the YES button below ");
									((EditText) dialog1.findViewById(R.id.ed_values)).setVisibility(View.GONE);
									
									Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
									btn_helpclose.setOnClickListener(new OnClickListener()
									{
									public void onClick(View arg0) {
											// TODO Auto-generated method stub
											dialog1.dismiss();
											//finish();
										}
										
									});

									Button btnclear = (Button) dialog1.findViewById(R.id.clear);
									btnclear.setText("No");								
									
									
									Button btn_ok = (Button) dialog1.findViewById(R.id.save);
									btn_ok.setText("Yes");

							
									btnclear.setOnClickListener(new OnClickListener()
									{
										public void onClick(View arg0)
										{
											dialog1.dismiss();
										}
									});


									btn_ok.setOnClickListener(new OnClickListener()
									{
									public void onClick(View arg0) {
											// TODO Auto-generated method stub
										 dialog1.dismiss();
										    final Dialog dialog2 = new Dialog(HomeScreen.this,android.R.style.Theme_Translucent_NoTitleBar);
										    dialog2.getWindow().setContentView(R.layout.alert);
											
											RelativeLayout li=(RelativeLayout)dialog2.findViewById(R.id.helprow);
											li.setVisibility(View.VISIBLE);
											TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
											txttitle.setText("Increase number of inspections allowed");
											
											LinearLayout lin=(LinearLayout)dialog2.findViewById(R.id.camera);
											lin.setVisibility(v1.VISIBLE);
											
											TextView txt = (TextView) dialog2.findViewById(R.id.txtquestio);
											//txt.setText("To increase the number of additional inspections you can complete, simply enter the number you need in the field below. If you are using these inspections for your actual clients, MAKE SURE YOU UPLOAD your logo, contact information, etc. correctly within your registration area. Once uploaded this information will be used on your inspection reports.");
											txt.setText("Please confirm how many inspections you would like to add to your system.  Once submitted this will  be reviewed and/or approved by our administrators. ");
											((EditText) dialog2.findViewById(R.id.ed_values)).setVisibility(View.VISIBLE);
											
											final EditText etextend = ((EditText) dialog2.findViewById(R.id.ed_values));
											etextend.setMaxWidth(10);
											etextend.setInputType(InputType.TYPE_CLASS_NUMBER);
											etextend.setFilters(new InputFilter[] { new InputFilter.LengthFilter(1) });
											etextend.addTextChangedListener(new TextWatcher() {
												
												@Override
												public void onTextChanged(CharSequence s, int start, int before, int count) {
													// TODO Auto-generated method stub
													if(etextend.getText().toString().equals(" "))
													{
														etextend.setText("");
													}
													if(etextend.getText().toString().equals("0"))
													{
														etextend.setText("");
													}
													if(!etextend.getText().toString().trim().equals(""))
													{
														if(Integer.parseInt(etextend.getText().toString().trim())>5)
														{
															etextend.setText("");
															cf.ShowToast("Please enter the number of inspections less than or equal to 5");
														}
													}
												}
												
												@Override
												public void beforeTextChanged(CharSequence s, int start, int count,
														int after) {
													// TODO Auto-generated method stub
													
												}
												
												@Override
												public void afterTextChanged(Editable s) {
													// TODO Auto-generated method stub
													
												}
											});
											
											Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
											btn_helpclose.setOnClickListener(new OnClickListener()
											{
											public void onClick(View arg0) {
													// TODO Auto-generated method stub
												dialog2.dismiss();
													//finish();
												}
												
											});

											Button btnclear = (Button) dialog2.findViewById(R.id.clear);
											btnclear.setVisibility(v1.GONE);							
											
											
											Button btn_ok = (Button) dialog2.findViewById(R.id.save);
											btn_ok.setText("Save");
									

											btn_ok.setOnClickListener(new OnClickListener()
											{
											public void onClick(View arg0) {
													// TODO Auto-generated method stub
												if (wb.isInternetOn() == true) {
													cf.show_ProgressDialog("Increasing number of inspections...");
													new Thread() {
														public void run() {
															Looper.prepare();
															try
															{
																SoapObject request = new SoapObject(wb.NAMESPACE,"EXPORTINSPECTIONVALUE");
																SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
																envelope.dotNet = true;
																request.addProperty("InspectorID", db.Insp_id);
																request.addProperty("Totalinspection", etextend.getText().toString());
																
																envelope.setOutputSoapObject(request);
																System.out.println("EXPORTINSPECTIONVALUE request is "
																		+ request);
																HttpTransportSE androidHttpTransport = new HttpTransportSE(
																		wb.URL);
																System.out.println("Before http call");
																androidHttpTransport.call(wb.NAMESPACE+ "EXPORTINSPECTIONVALUE", envelope);
															
																Object result= (Object)envelope.getResponse();
																System.out.println("EXPORTINSPECTIONVALUEResult result is"+ result);
																
																if(result.toString().equals("true"))
																{
																	eval_handler = 1;
																	handler.sendEmptyMessage(0);
																}
																else
																{
																	eval_handler = 2;
																	handler.sendEmptyMessage(0);
																}
															}
															catch (IOException e) {
																// TODO Auto-generated catch block
																e.printStackTrace();
																eval_handler = 3;
																handler.sendEmptyMessage(0);
															} catch (XmlPullParserException e) {
																// TODO Auto-generated catch block
																e.printStackTrace();
																eval_handler = 3;
																handler.sendEmptyMessage(0);
															}
															catch (Exception e) {
																// TODO: handle exception
																e.printStackTrace();
																eval_handler = 3;
																handler.sendEmptyMessage(0);
															}
														}
														private Handler handler = new Handler() {
															@Override
															public void handleMessage(Message msg) {
																cf.pd.dismiss();
																if (eval_handler == 3) {
																	eval_handler = 0;
																	cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
																	
																} else if (eval_handler == 4) {
																	eval_handler = 0;
																	cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

																} else if (eval_handler == 1) {
																	eval_handler = 0;
																	Intent intentorder=new Intent(HomeScreen.this,OrderInspection.class);
																	startActivity(intentorder);
																	finish();
																	Intent intent = getIntent();
																	overridePendingTransition(0, 0);
																	intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
																	finish();
																	overridePendingTransition(0, 0);
																	startActivity(intent);
																}else if (eval_handler == 2) {
																	eval_handler = 0;
																	cf.ShowToast("You can't able to extend your demo inspection");
																}
															}
														};
													}.start();
												}
												else
												{
													cf.ShowToast("Internet connection not available");
												}
												}
											
											});
											
											dialog2.setCancelable(false);
											dialog2.show();

										}
									
									});
									
									dialog1.setCancelable(false);
									dialog1.show();

									
								}
							}

						
						};
				
					}.start();
				}
				else
				{
					cf.ShowToast("Internet connection not available");
				}*/
				Intent intentorder=new Intent(HomeScreen.this,OrderInspection.class);
				startActivity(intentorder);
				finish();
			break;			
			case R.id.importfiles:				
				if(wb.isInternetOn()==true) 
				 {
					total = 0;
					showDialog(1);
					start_import();					
				}
				else 
				{
					usercheck = 1;
					handler.sendEmptyMessage(0);
				}
			break;
			case R.id.exportfiles:
				try
				{
					db.CreateTable(2);
					Cursor cur2 = db.wind_db.rawQuery("select * from " + db.policyholder + " where PH_Status='2' and PH_InspectorId='"+db.Insp_id+"'", null);
					int count2 = cur2.getCount();
					if (count2 == 0) {
						cf.ShowToast("No records available");
					} else {
						cur2.moveToFirst();
						Intent submitint = new Intent(HomeScreen.this,Export.class);
						submitint.putExtra("type", "export");
						submitint.putExtra("classidentifier", "HomeScreen");
						startActivity(submitint);
						finish();
					}
					cur2.close();
				}catch (Exception e) {
					// TODO: handle exception
				}
			break;
			case R.id.checkforupdates:				
				if(animSet.hasStarted())
				{
					Update_Alert_Market();
				}
				else
				{
					check_for_updates();
				}				
			break;
			case R.id.toaddcomments:
				startActivity(new Intent(this,AddComments.class));
				finish();
				break;
			case R.id.logout:
				try
				{
					db.wind_db.execSQL(" UPDATE "+db.inspectorlogin+" SET Ins_Flag1='0' where Ins_Flag1='1'");
					
					Intent submitint = new Intent(HomeScreen.this,WindMitInspection.class);
					submitint.putExtra("status", "logout");
					startActivity(submitint);
					finish();
				}
				catch(Exception e)
				{
					System.out.println("hoeme screenl lo"+e.getMessage());
				}
				
			break;
			case R.id.emailreport:
				db.CreateTable(2);
				Cursor cur2 = db.wind_db.rawQuery("select * from " + db.policyholder + " where PH_InspectorId='"+db.Insp_id+"'", null);
				int count2 = cur2.getCount();
				if (count2 == 0) {
					cf.ShowToast("No records available, Please click IMPORT INSPECTIONS to import data");
				} else {
					cur2.moveToFirst();
					Intent intent_emailreport = new Intent(HomeScreen.this,EmailList.class);
					startActivity(intent_emailreport);
					finish();
				}
				cur2.close();
				break;
			
			case R.id.resetpwd:
			reset_password();
			break;
		}
	}
	 private void start_import() {
			// TODO Auto-generated method stub
			 new Thread() {
				public void run() {

					try {
						boolean versioncode = getversioncodefromweb();
						if (versioncode == true || versioncode == false) 
						{
							 total = 15;
						  		 try
								 {
					  			 
						  			 SoapObject policy_holder = wb.Calling_service(db.Insp_id,"UpdateMobileDB");			
						  			 System.out.println("UpdateMobileDB"+policy_holder);
						  			 if (policy_holder.equals("anyType{}")) 
						  			 {
										cf.ShowToast("Server is busy. Please try again later");
										finish();
						  			 } 
						  			 else 
						  			 {
										GetPolicyholderInfo(policy_holder);
										total=40;
										Get1802CommentsInfo();
										total = 65;
										GetOriginalQuesMitigationInfo();
										total = 100;	
										usercheck = 4;
						  			 }
								}
								catch (SocketTimeoutException s) {
									System.out.println("s "+s.getMessage());
									    usercheck = 2;
										total = 100;
										
								 } catch (NetworkErrorException n) {
									 System.out.println("n "+n.getMessage());
									    usercheck = 2;
									    total = 100;
									   
								 } catch (IOException io) {
									 System.out.println("io "+io.getMessage());
									    usercheck = 2;
									    total = 100;
									    
								 } catch (XmlPullParserException x) {
									 System.out.println("x "+x.getMessage());
									 	usercheck = 2;
									 	total = 100;
									    
								 }
			               	     catch (Exception e) {
			               	    	 System.out.println("e "+e.getMessage());
			               	    	    usercheck = 3;
										total = 100;									
								}
						  	 }
						   else 
						    {
						    	total = 100;
						    }						 }
						 catch (Exception e) {
							 System.out.println("commeonexcep"+e.getMessage());
							 usercheck = 2;
							 	total = 100;
								//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ HomeScreen.this +" "+" in the stage of(catch) importing  at "+" "+cf.datewithtime+" "+"in apk"+" "+cf.apkrc);
								
						}
					}
			 }
			 .start();
		}
	public void GetOriginalQuesMitigationInfo() throws NetworkErrorException,IOException, XmlPullParserException, SocketTimeoutException
	{
		String bc_original, rc_original, rd_original, rw_original, rg_original, swr_original, op_original, wc_original,homeid;
		try
		{
			db.CreateTable(6);
			SoapObject request = new SoapObject(wb.NAMESPACE, "LoadOriginalDataText");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;				
			
			request.addProperty("InspectorID",db.Insp_id);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			androidHttpTransport.call(wb.NAMESPACE+"LoadOriginalDataText", envelope);
			SoapObject obj = (SoapObject) envelope.getResponse();System.out.println("result1"+obj);
			int Cnt;
			
			 if(String.valueOf(obj).equals("null") || String.valueOf(obj).equals(null) || String.valueOf(obj).equals("anytype{}"))
			 {
					Cnt=0;total = 100;
			 }
			 else
			 {
				 	Cnt = obj.getPropertyCount();		
					for (int i = 0; i < Cnt; i++) 
					{
							SoapObject result1 = (SoapObject) obj.getProperty(i);
							homeid = String.valueOf(result1.getProperty("SRID"));System.out.println("SRID");
							bc_original = String.valueOf(result1.getProperty("BuildCodeOriginal"));
							rc_original = String.valueOf(result1.getProperty("RoofCoverOriginal"));
							rd_original = String.valueOf(result1.getProperty("RoofDeckOriginal"));
							rw_original = String.valueOf(result1.getProperty("RoofWallOriginalValue"));
							rg_original = String.valueOf(result1.getProperty("RoofGeoOriginalValue"));
							swr_original = String.valueOf(result1.getProperty("SWROriginalValue"));
							op_original = String.valueOf(result1.getProperty("OpenProtectOriginalValue"));
							wc_original = String.valueOf(result1.getProperty("WallConstrOriginal"));

							Cursor c2 = db.SelectTablefunction(db.OriginalMit, " where ORI_SRID='"+ homeid + "'");
						    if (c2.getCount() == 0) 
						    {
						    		db.wind_db.execSQL("INSERT INTO "
										+ db.OriginalMit
										+ " (ORI_SRID,ORI_BUILDCODE,ORI_ROOFCOVER,ORI_ROOFDECK,ORI_ROOFWALL,ORI_ROOFGEO,ORI_SWR,ORI_OPENPROT,ORI_WALLCONS)"
										+ " VALUES ('" + homeid + "','"
										+ db.encode(bc_original) + "','"
										+ db.encode(rc_original) + "','"
										+ db.encode(rd_original) + "','"
										+ db.encode(rw_original) + "','"
										+ db.encode(rg_original) + "','"
										+ db.encode(swr_original) + "','"
										+ db.encode(op_original) + "','"
										+ db.encode(wc_original) + "')");
							}
						} 
			}
		
		} 
		catch (Exception e)
		{
			System.out.println("Oringal"+e.getMessage());
		}
	}
	public void GetPolicyholderInfo(SoapObject obj) throws NetworkErrorException,IOException, XmlPullParserException, SocketTimeoutException
	 {
		 String substatus ="0",Orderid,buildingsize="", email, cperson, IsInspected = "0", insurancecompanyname, IsUploaded = "0", homeid = "", firstname, 
					lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, status, 
					companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname, scheduleddate, yearbuilt, nstories, inspectionstarttime, 
					inspectionendtime, inspectioncomment, inspectiontypeid,licenceexpirydate="",scheduledcreateddate,assigneddate,adrschk,
							MailingAddress,MailingAddress2,Mailingcity,MailingState,MailingCounty,Mailingzip,inspfee,inspdiscount,inspectiondate,inspectiontime;

			
		 int Cnt;
		 db.CreateTable(2);
		 if(String.valueOf(obj).equals("null") || String.valueOf(obj).equals(null) || String.valueOf(obj).equals("anytype{}"))
		 {
				Cnt=0;total = 40;
		 }
		 else
		 {
			 
			 Cnt = obj.getPropertyCount();
			 System.out.println("policy_holder result1");
			for (int i = 0; i < Cnt; i++) 
			{
				SoapObject result1 = (SoapObject) obj.getProperty(i);	 System.out.println(" result1"+result1);
				try 
				{
					homeid = String.valueOf(result1.getProperty("SRID"));
					firstname = String.valueOf(result1.getProperty("FirstName")).trim();
					lastname = String.valueOf(result1.getProperty("LastName")).trim();
					middlename = String.valueOf(result1.getProperty("MiddleName")).trim();
					addr1 = String.valueOf(result1.getProperty("Address1")).trim();
					addr2 = String.valueOf(result1.getProperty("Address2")).trim();
					city = String.valueOf(result1.getProperty("City")).trim();
					state= String.valueOf(result1.getProperty("State")).trim();
					country = String.valueOf(result1.getProperty("Country")).trim();
					zip = String.valueOf(result1.getProperty("Zip"));
					homephone = String.valueOf(result1.getProperty("HomePhone")).trim();
					cellphone = String.valueOf(result1.getProperty("CellPhone")).trim();
					workphone = String.valueOf(result1.getProperty("WorkPhone")).trim();
					email = String.valueOf(result1.getProperty("Email")).trim();			
					cperson = String.valueOf(result1.getProperty("ContactPerson")).trim();
					ownerpolicyno = String.valueOf(result1.getProperty("OwnerPolicyNo")).trim();
					status = String.valueOf(result1.getProperty("Status")).trim();
					substatus = String.valueOf(result1.getProperty("Substatus")).trim();
					companyid = String.valueOf(result1.getProperty("CompanyId")).trim();
					inspectorid = String.valueOf(result1.getProperty("InspectorId"));			
					yearbuilt = String.valueOf(result1.getProperty("YearBuilt")).trim();
					nstories = String.valueOf(result1.getProperty("Nstories")).trim();					
					Orderid= String.valueOf(result1.getProperty("Orderid")).trim();
					inspectiontypeid = String.valueOf(result1.getProperty("InspectionTypeId"));
					inspectiondate= String.valueOf(result1.getProperty("Inspectiondate")).trim();
			        inspectiontime= String.valueOf(result1.getProperty("Inspectiontime")).trim();
			        inspectioncomment= (String.valueOf(result1.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(result1.getProperty("InspectionComment")).trim();
			        insurancecompanyname = String.valueOf(result1.getProperty("InsuranceCompany")).trim();
					buildingsize = String.valueOf(result1.getProperty("BuildingSize"));
					
					MailingAddress= (String.valueOf(result1.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingAddress")).trim();
					MailingAddress2 = (String.valueOf(result1.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingAddress2")).trim();
					Mailingcity = (String.valueOf(result1.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(result1.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(result1.getProperty("Mailingcity")).trim();
					MailingState = (String.valueOf(result1.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingState")).trim();
					MailingCounty = (String.valueOf(result1.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingCounty")).trim();				
					Mailingzip = (String.valueOf(result1.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(result1.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(result1.getProperty("Mailingzip"));
					
					inspfee = String.valueOf(result1.getProperty("Inspectionfee"));
					inspdiscount = String.valueOf(result1.getProperty("Discount"));
					
					if(status.equals("30") || status.equals("40"))
					{
						status="40";
					}
					
					db.CreateTable(23);
					Cursor c1 = db.SelectTablefunction(db.policyholder," where PH_InspectorId='" + db.encode(db.Insp_id) + "' and PH_SRID='"+homeid+"'");
					System.out.println("polic="+c1.getCount());
						if(c1.getCount() == 0)
						 {


							db.wind_db.execSQL("INSERT INTO "
									+ db.policyholder
									+ " (PH_SRID,PH_FirstName,PH_LastName,PH_MiddleName,PH_Address1,PH_Address2,PH_City,PH_State,PH_County,PH_Zip,YearBuilt,PH_HomePhone," +
									"PH_WorkPhone,PH_CellPhone,PH_ContactPerson ,PH_Policyno,PH_Email,PH_NOOFSTORIES," +
									"PH_Status,PH_SubStatus,PH_InspectorId," +
									"Schedule_Comments,PH_InspectionTypeId,InspectionDate,InspectionTime,InsuranceCompanyname,chkbx," +
									"BuildingSize,Fee,Discount,Orderid)"
									+ " VALUES ('" + homeid + "','"+ db.encode(firstname) + "','"+ db.encode(lastname) + "','"+ db.encode(middlename) + "'," +
											"'"+db.encode(addr1)+"','"+db.encode(addr2)+"','"+db.encode(city)+"','"+db.encode(state)+"','"+db.encode(country)+"'," +
											"'"+zip+"','"+yearbuilt+"','"+db.encode(homephone)+"','"+db.encode(workphone)+"','"+db.encode(cellphone)+"','"+db.encode(cperson)+"'," +
											"'"+db.encode(ownerpolicyno)+"','"+db.encode(email)+"','"+nstories+"'," +
											"'"+status+"','"+substatus+"','"+inspectorid+"'," +
											"'"+db.encode(inspectioncomment)+"','"+inspectiontypeid+"','"+db.encode(inspectiondate)+"','"+db.encode(inspectiontime)+"','"+db.encode(insurancecompanyname)+"'," +
											"'0','"+buildingsize+"','"+db.encode(inspfee)+"','"+db.encode(inspdiscount)+"','"+Orderid+"')");
						}		
						else
						{

								db.wind_db.execSQL("UPDATE " + db.policyholder
									+ " SET PH_FirstName='"+ db.encode(firstname)+ "',PH_LastName='"+ db.encode(lastname)+ "',PH_MiddleName='"+db.encode(middlename)+"',PH_Address1='"+ db.encode(addr1) + "'," +
											"PH_Address2='"+db.encode(addr2)+"',PH_City='"+ db.encode(city) + "',PH_Zip='"+ zip + "',PH_State='"+ db.encode(state) + "',PH_County='"+ db.encode(country)+ "'," +
											"YearBuilt='" + yearbuilt+ "',PH_ContactPerson='"+ db.encode(cperson)+ "',PH_HomePhone='" + db.encode(homephone)+ "',PH_WorkPhone='" + db.encode(workphone)+ "'," +
											"PH_CellPhone='" + db.encode(cellphone)+ "',PH_Policyno='"+ db.encode(ownerpolicyno) + "',PH_NOOFSTORIES='"+ nstories + "',PH_Email='" + db.encode(email)+ "'," +
											"chkbx='" + 0 + "',PH_InspectorId='"+inspectorid+"',PH_Status='"+status+"',PH_SubStatus='"+substatus+"',Schedule_Comments='"+db.encode(inspectioncomment)+"',InspectionDate='"+db.encode(inspectiondate)+"',InspectionTime='"+db.encode(inspectiontime)+"',Fee='"+db.encode(inspfee)+"',Discount='"+db.encode(inspdiscount)+"',Orderid='"+Orderid+"' WHERE PH_SRID ='" + homeid+ "'");
						}
							
						db.CreateTable(23);
						if(db.encode(addr1.trim()).equals(db.encode(MailingAddress.trim())) && db.encode(addr2.trim()).equals(db.encode(MailingAddress2.trim())) &&
								db.encode(city.trim()).equals(db.encode(Mailingcity.trim())) && db.encode(state.trim()).equals(db.encode(MailingState.trim())) &&
								db.encode(country.trim()).equals(db.encode(MailingCounty.trim())) && zip.trim().equals(Mailingzip.trim()))
						{
							adrschk = "1";
						}
						else
						{
							adrschk = "0";
						}
				        Cursor c21 = db.SelectTablefunction(db.MailingPolicyHolder," where ML_PH_InspectorId='" + db.encode(db.Insp_id) + "' and ML_PH_SRID='"+homeid+"'");
						if(c21.getCount() >= 1)
						{						
							db.wind_db.execSQL("UPDATE " + db.MailingPolicyHolder
									+ " SET ML_PH_Address1='" + db.encode(MailingAddress)
									+ "',ML_PH_Address2='"
									+ db.encode(MailingAddress2)
									+ "',ML='"
									+ adrschk+"',ML_PH_City='"
									+ db.encode(Mailingcity)
									+ "',ML_PH_Zip='"
									+ db.encode(Mailingzip) + "',ML_PH_State='"
									+ db.encode(MailingState) + "',ML_PH_County='"
									+ db.encode(MailingCounty) + "' WHERE ML_PH_SRID ='" +homeid+ "'");
						}
						else
						{
							db.wind_db.execSQL("INSERT INTO "
									+ db.MailingPolicyHolder
									+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
									+ "VALUES ('"+homeid+"','"+inspectorid+"','0','"+db.encode(MailingAddress)+"','"
								    + db.encode(MailingAddress2)+"','"+db.encode(Mailingcity)+"','"
								    +Mailingzip+"','"+db.encode(MailingState)+"','"
								    + db.encode(MailingCounty)+"')");
							
						}
				}
				catch(Exception e)
				{
					System.out.println("Exception"+e.getMessage());
				}
			}
			}
	 }
	 private void Get1802CommentsInfo() throws NetworkErrorException, IOException,
		XmlPullParserException, SocketTimeoutException {
	// TODO Auto-generated method stub
				db.CreateTable(5);
				String quesID, optionID, Comments;
				SoapObject request = new SoapObject(wb.NAMESPACE, "LoadCommandlibraryFor1802");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;				
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

				androidHttpTransport.call(wb.NAMESPACE+"LoadCommandlibraryFor1802", envelope);
				SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("result1"+result1);
				int Cnt;
				
				 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
				 {
						Cnt=0;total = 65;
				 }
				 else
				 {
					 	Cnt = result1.getPropertyCount();
						/*double Totaltmp = 0.0;
						int countchk = Cnt;
						Totaltmp = (25.00 / (double) countchk);
						double temptot = Totaltmp;
						int temptotal = total;*/
						for (int i = 0; i < Cnt; i++) 
						{
							
							try 
							{
								
								SoapObject obj = (SoapObject) result1.getProperty(i);
								quesID = String.valueOf(obj.getProperty("questId"));
								optionID = String.valueOf(obj.getProperty("OptionId"));
								Comments = String.valueOf(obj.getProperty("Command"));
								
								Cursor c2 = db.SelectTablefunction(db.StandardComments,
										" where SC_QuesID='" + quesID + "' and SC_OptionID='"
												+ optionID + "'  and SC_Description='" + db.encode(Comments) + "' and SC_InspectorId='"+db.Insp_id+"'");
								if (c2.getCount() == 0) 
								{
									
										db.wind_db.execSQL("INSERT INTO "
												+ db.StandardComments
												+ " (SC_InspectorId,SC_QuesID,SC_OptionID,SC_Description,SC_Status)"
												+ " VALUES ('" + db.Insp_id + "','" + quesID+ "','" + optionID + "','"+ db.encode(Comments)+ "','1')");
								}
							} 
							catch (Exception e)
							{
								System.out.println("cException " + e.getMessage());
							}
							
								/*if (total <= 66) {
	
									total = temptotal + (int) (Totaltmp);
								} else {
									total = 65;
								}
								Totaltmp += temptot;*/

						}
				 }

	 }

	private void reset_password() {
		// TODO Auto-generated method stub

		dialog1 = new Dialog(HomeScreen.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.setContentView(R.layout.resetpassword);
		dialog1.setCancelable(false);
		dialog1.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		final EditText oldpassword;
		final EditText newpassword;
		final EditText confirmpassword;
		Button submit, cancel;
		oldpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_oldpassword);
		newpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_newpassword);
		confirmpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_confirmpassword);

		
		oldpassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(oldpassword.getText().toString().equals(" "))
				{
					oldpassword.setText("");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		newpassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(newpassword.getText().toString().equals(" "))
				{
					newpassword.setText("");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		confirmpassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(confirmpassword.getText().toString().equals(" "))
				{
					confirmpassword.setText("");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		ImageView clo = (ImageView) dialog1
				.findViewById(R.id.resetpassword_close);
		submit = (Button) dialog1.findViewById(R.id.resetpassword_submit);
		cancel = (Button) dialog1.findViewById(R.id.resetpassword_cancel);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (oldpassword.getText().toString().equals("")) {
					cf.ShowToast("Please enter Old Password");
					oldpassword.requestFocus();
				} else {

					if (newpassword.getText().toString().equals("")) {
						cf.ShowToast("Please enter New Password");
						newpassword.requestFocus();
					} else {
						
						boolean upperFound = false;
						boolean specialcharacterFound = false;
						boolean numberFound = false;
						
						//Checks for edittext contains a Digit/Number
						String numberchecker=newpassword.getText().toString();
						if(numberchecker.matches(".*\\d.*")){
						    numberFound = true;
						} else{
						    numberFound = false;
						}
						
						//Checks for edittext contains a Special Character
						Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
						Matcher m = p.matcher(newpassword.getText().toString());
						boolean b = m.find();

						if (b)
						{
							specialcharacterFound=true;
						}
						else
						{
							specialcharacterFound=false;
						}
						
						//Checks for edittext contains a Upper Case Character
						String aString = newpassword.getText().toString();
						for (char c : aString.toCharArray()) {
						    if (Character.isUpperCase(c)) {
						        upperFound = true;
						       break;
						    }
						}
						
						if(numberFound&&specialcharacterFound&&upperFound)
						{
							if (confirmpassword.getText().toString().equals("")) {
								cf.ShowToast("Please enter Confirm Password");
								confirmpassword.requestFocus();
							} else {
								if (newpassword
										.getText()
										.toString()
										.equals(confirmpassword.getText()
												.toString())) {
									db.CreateTable(1);
									Cursor cur = db.wind_db.rawQuery("select * from "+ db.inspectorlogin + " where  Ins_Flag1=1", null);
									cur.moveToFirst();
									if (cur.getCount() >= 1) {
										username = db.decode(cur.getString(cur
												.getColumnIndex("Ins_UserName")));
										password = db.decode(cur.getString(cur
												.getColumnIndex("Ins_Password")));
									}
									cur.close();
                                    System.out.println("pass="+password+" "+oldpassword.getText()
											.toString());
									if (!password.equals(oldpassword.getText()
											.toString())) {
										cf.ShowToast("Please enter correct Old Password");
										cf.hidekeyboard(oldpassword);
										oldpassword.setText("");
										newpassword.setText("");
										confirmpassword.setText("");
										oldpassword.requestFocus();
									} else {
										cf.hidekeyboard(oldpassword);
										call_reset_password_webservice(confirmpassword
												.getText().toString());
									}

								} else {
									cf.ShowToast("New Password and Confirm Password do not match");
									cf.hidekeyboard(newpassword);
									newpassword.setText("");
									confirmpassword.setText("");
									newpassword.requestFocus();
								}

							}
						}
						else
						{
							cf.ShowToast("Please enter a valid password");
							cf.hidekeyboard(newpassword);
							newpassword.setText("");
							newpassword.requestFocus();
						}
						
						
					}

				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.dismiss();

			}
		});
		clo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
		}
		});

		dialog1.show();
	
	}
	protected void call_reset_password_webservice(final String newpass) {
		// TODO Auto-generated method stub
		if (wb.isInternetOn() == true) {
			cf.show_ProgressDialog("Resetting Password..");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						System.out.println("The newpass is "+newpass);
						String chkauth = Calling_WS_ChangePassword(username,
								password, newpass, "CHANGEPASSWORD");
						System.out.println("The Result is " + chkauth);
						if (chkauth.equals("true")) 
						{
							show_handler = 5;
							handler.sendEmptyMessage(0);

						} else /* USERAUTHENTICATION IS FALSE */
						{
							show_handler = 1;
							handler.sendEmptyMessage(0);

						}
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("The error is " + e.getMessage());
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private String Calling_WS_ChangePassword(String username,
						String password, String newpass, String string) throws SocketException,
						IOException, NetworkErrorException, TimeoutException,
						XmlPullParserException, Exception {
					// TODO Auto-generated method stub
					SoapObject request = new SoapObject(wb.NAMESPACE, string);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("Username", username);
					request.addProperty("Currentpwd", password);
					request.addProperty("Newpwd", newpass);
					System.out.println("Request is " + request);
					envelope.setOutputSoapObject(request);

					HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
					androidHttpTransport.call(wb.NAMESPACE + string, envelope);
					String result = envelope.getResponse().toString();System.out.println("reerere"+result);
					return result;
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						// dialog1.dismiss();
						if (show_handler == 1) {
							show_handler = 0;
							cf.ShowToast("Invalid Password");
							
						} else if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 5) {
							db.CreateTable(1);
							db.wind_db.execSQL("delete from " + db.inspectorlogin);
							
							dialog1.dismiss();
							

							final Dialog dialog2 = new Dialog(
									HomeScreen.this,
									android.R.style.Theme_Translucent_NoTitleBar);
							dialog2.setContentView(R.layout.loginagain);
							dialog2.setCancelable(false);
							TextView ok = (TextView) dialog2
									.findViewById(R.id.redirect);
							ok.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									dialog2.dismiss();
									
									db.CreateTable(1);
									db.wind_db.execSQL("delete from " + db.inspectorlogin);
									
									Intent intent = new Intent(HomeScreen.this,
											WindMitInspection.class);
									startActivity(intent);
									
									finish();
								}

							
							});
							dialog2.show();
						}
					}
				};
			}.start();
		} else {
			cf.ShowToast("Internet connection not available");

		}

	}
	private void check_for_updates() {
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Checking for Updates..."
					+ " Please wait.</font></b>";
			final ProgressDialog pd = ProgressDialog.show(HomeScreen.this, "",
					Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
//				boolean value;

				public void run() {
					Looper.prepare();
					try {
						vcode = getcurrentversioncode();
						System.out.println("loopvocde"+vcode);
						value = getversioncodefromweb();
						System.out.println("loopvalue"+value);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					}catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {

					@Override
					public void handleMessage(Message msg) {

						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("here is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 5) {
							show_handler = 0;

							System.out.println("value is " + value);
							if (value == true) {
								// No update available
								cf.ShowToast("No update available");

							} else {
								System.out.println("Inside else");
								// update available
//								Update_Alert();
								Update_Alert_Market();
								System.out.println("the url is "+url);
							}

						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");

		}
	}

	public boolean getversioncodefromweb() throws NetworkErrorException,SocketTimeoutException, IOException, XmlPullParserException,Exception {
		SoapObject request = new SoapObject(wb.NAMESPACE,"GETVERSIONINFORMATION");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE + "GETVERSIONINFORMATION",envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("The result for version information is " + result);
		SoapObject obj = (SoapObject) result.getProperty(0);
		newversioncode = String.valueOf(obj.getProperty("VersionCode"));
		System.out.println("newversioncode"+newversioncode);
		newversionname = String.valueOf(obj.getProperty("VersionName"));
		if (!"null".equals(newversioncode) && null != newversioncode && !newversioncode.equals("")) 
		{			
			if (Integer.toString(vcode).equals(newversioncode)) 
			{
				return true;
			} 
			else 
			{
				try 
				{
					db.CreateTable(0);
					Cursor c12 = db.wind_db.rawQuery("select * from " + db.B1802version,null);
					if (c12.getCount() < 1) 
					{						
							db.wind_db.execSQL("INSERT INTO " + db.B1802version
									+ "(VersionCode,VersionName,ModifiedDate)"
									+ " VALUES ('" + newversioncode + "','"
									+ newversionname + "','"+cf.datewithtime+"')");
					}
					else 
					{						
							db.wind_db.execSQL("UPDATE " + db.B1802version
									+ " set VersionCode='" + newversioncode
									+ "',VersionName='" + newversionname
									+ "',ModifiedDate='"+cf.datewithtime+"'");
					}
					return false;					
				} 
				catch (Exception e) 
				{					
					return true;
				}
			}
		}
		else
		{return true;
			
		}
		 
	}
	
	private void Update_Alert_Market() {
		Cursor c12 = db.SelectTablefunction(db.B1802version, " ");  
		if (c12.getCount() >= 1) {
			c12.moveToFirst();	
			dbversionname = c12.getString(c12.getColumnIndex("VersionName"));
		}
		
		alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
		alertDialog
				.setMessage("There is a New Version ( "
						+ dbversionname
						+ " ) of B1 1802 Inspection available. Update now to get the Latest Features and Improvements. ");
		alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(
								Intent.ACTION_VIEW,
								Uri.parse("https://play.google.com/store/apps/details?id=idsoft.inspectiondepot.windmitinspection")));
					}
				});
		alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});

		alertDialog.show();
	}
	

	 
	 private int getcurrentversioncode() {
			// TODO Auto-generated method stub
		 int vcode=0;
			try {
				vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
               System.out.println("vcode"+vcode);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return vcode;
		}
	
	 final Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				 if(usercheck==1)
				 {
					 usercheck = 0;
					 cf.ShowToast("Internet connection not available");
					 startActivity(new Intent(HomeScreen.this, HomeScreen.class));
					 finish();
				 }
				 else if(usercheck==2)
				 {
					 usercheck = 0;
					 cf.ShowToast("There is a problem on your network, so please try again later with better network");
					 startActivity(new Intent(HomeScreen.this, HomeScreen.class));
					 finish();
				 }
				 else if(usercheck==3)
				 {
					 usercheck = 0;
					 cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
					 startActivity(new Intent(HomeScreen.this, HomeScreen.class));
					 finish();
				 }
				 else if(usercheck==4)
				 {
					 usercheck=0;
					 progDialog.dismiss();
					 
					 final Dialog dialog1 = new Dialog(HomeScreen.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog1.getWindow().setContentView(R.layout.alert);
						RelativeLayout li=(RelativeLayout)dialog1.findViewById(R.id.helprow);
						li.setVisibility(View.VISIBLE);
						LinearLayout lin=(LinearLayout)dialog1.findViewById(R.id.camera);
						lin.setVisibility(View.VISIBLE);
						TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
						txttitle.setText("Import");
						TextView txt = (TextView) dialog1.findViewById(R.id.txtquestio);
						txt.setText("Import successfully completed");
						((EditText) dialog1.findViewById(R.id.ed_values)).setVisibility(View.GONE);
						
						Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
						btn_helpclose.setOnClickListener(new OnClickListener()
						{
						public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								startActivity(new Intent(HomeScreen.this,Dashboard.class));
								finish();
							}
							
						});

						Button btnclear = (Button) dialog1.findViewById(R.id.clear);
						btnclear.setVisibility(View.GONE);								
						
						Button btn_ok = (Button) dialog1.findViewById(R.id.save);
						btn_ok.setText("OK");
						btn_ok.setVisibility(View.VISIBLE);
						btn_ok.setOnClickListener(new OnClickListener()
						{
						public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								startActivity(new Intent(HomeScreen.this,Dashboard.class));
								finish();
							}
							
						});
						
						dialog1.setCancelable(false);
						dialog1.show();
				 }
				
			}

		};
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case 1:
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setMax(100);	
				String source = "<font color=#FFFFFF>Importing please wait..."	+ "</font>";
		        progDialog.setMessage(Html.fromHtml(source));	
				progDialog.setCancelable(false);	
				progThread = new ProgressThread(handler1);	
				progThread.start();	
				return progDialog;
			default:
				return null;
			}
		}
		
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	final Handler handler1 = new Handler() {
		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			int total = msg.getData().getInt("total");
			progDialog.setProgress(total);
			//progDialog.setProgressStyle(R.style.progtext);
			
			
			if (total == 100) {
				progDialog.setCancelable(true);
				removeDialog(1);
				handler.sendEmptyMessage(0);
				progThread.setState(ProgressThread.DONE);
		 	}

		}
	};
	private class ProgressThread extends Thread {

		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;
		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;
			while (mState == RUNNING) {
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", total);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}
		public void setState(int state) {
			mState = state;
		}

	}
}