package idsoft.inspectiondepot.windmitinspection;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AddComments extends Activity {
	CommonFunctions cf;
	Spinner sp1,sp2;
	EditText ed;
	ArrayAdapter ad1,ad2;
	CheckBox chk[]=new CheckBox[2];
	LinearLayout li_insp;
	TableLayout tbl;
	DatabaseFunctions db;
	String s1_val,s2_val;
	String tmp[];String resmsg="";
	String questions[] = {"--Select--","Building Code","Roof Cover","Roof Deck Attachment","Roof to Wall Attachment","Roof Geometry","Secondary Water Resistance(SWR)","Opening Protection", "Wall Construction(Addendum)" };
	String b1_q1[] = { "-Select-", "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
	String b1_q2[] = { "-Select-","Meets FBC(PD -03/2/02) or Original (after 2004)","Meets SFBC (PD 09/01/1994) or Post 1997","One or more does not meet A or B", "None Meet A or B" };
	String b1_q3[] = { "-Select-", "Mean Uplift less than B and C","103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic" };
	String b1_q4[] = { "-Select-", "Toe Nail", "Clips", "Single Wraps","Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
	String b1_q5[] = { "-Select-", "Hip Roof", "Flat Roof","Other Roof Type" };
	String b1_q6[] = { "-Select-", "SWR", "No SWR", "Unknown" };
	String b1_q7[] = { "-Select-", "FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected" };
	String b1_q8[] = { "-Select-", "Wall Construction Comments" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addcomments);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		
		Declaration();
		db.CreateTable(5);
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		sp1=(Spinner) findViewById(R.id.addcomment_SPquestype);System.out.println("Sdfsdfs");
		sp2=(Spinner) findViewById(R.id.addcomment_SPquesoption);
		ed=(EditText) findViewById(R.id.addcomment_EDcomment);
		tbl=(TableLayout) findViewById(R.id.addcomment_LLlistingcomments);
		//li_insp=(LinearLayout) findViewById(R.id.addcomment_LLinsptype);
		ed.setOnTouchListener(new TouchFoucs(ed));
		ed.addTextChangedListener(new  textWatchLimits(ed, 500,((TextView)findViewById(R.id.addcomment_TVcommetslimit))));
		ad1 = new ArrayAdapter(this, android.R.layout.simple_spinner_item,questions);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp1.setAdapter(ad1);
		
		
		sp1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
				if(sp1.getSelectedItemPosition()!=0)
				{
						tmp=sets4value(sp1.getSelectedItem().toString());
		
					ad2 = new ArrayAdapter(AddComments.this,android.R.layout.simple_spinner_item, tmp);
					ad2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					sp2.setAdapter(ad2);
				
					sp2.setSelection(0);
					((TableLayout) findViewById(R.id.addcomment_LLlistingcomments)).setVisibility(View.VISIBLE);
				}
				else
				{
					try
					{
						sp2.setSelection(0);
					}catch (Exception e) {
						// TODO: handle exception
						
					}
					((TableLayout) findViewById(R.id.addcomment_LLlistingcomments)).setVisibility(View.GONE);
					//((Button) findViewById(R.id.addcomments)).setVisibility(View.GONE);
					//((TableRow) findViewById(R.id.lib_comm_inspO_rw)).setVisibility(View.GONE);
				
				}
				try
				{
				//showSavedComments();
					view_comments();
				}catch (Exception e) {
					// TODO: handle exception
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
        ((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside");
				Intent insp_info = new Intent(AddComments.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);System.out.println("ends");
			}
		});
	}
	private void showSavedComments() {
		// TODO Auto-generated method stub
		int s1_val=sp1.getSelectedItemPosition();
		int s2_val=sp2.getSelectedItemPosition();

		Cursor c=db.SelectTablefunction(db.StandardComments, " Where SC_InspectorId='"+db.Insp_id+"' and SC_QuesID='"+s1_val+"' " +
				"and SC_OptionID='"+s2_val+"' and SC_Description='"+db.encode(ed.getText().toString())+"'");
		if(c.getCount()>0)
		{
			
			tbl.removeAllViews();
			TableRow tbl= new TableRow(this);
			TableLayout.LayoutParams lp= new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			tbl.setBackgroundColor(getResources().getColor(R.color.white));
			TextView tv;
			View v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tv = new TextView(this);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setBackgroundColor(Color.BLACK);
			tv.setText("No");
			tv.setTextColor(Color.WHITE);
			tv.setPadding(0, 10, 0, 10);
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tbl.addView(v,1,LayoutParams.FILL_PARENT);
			
			tv = new TextView(this);
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setPadding(0, 10, 0, 10);
			tv.setText("Active");
			tv.setTextColor(Color.WHITE);
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tbl.addView(v,1,LayoutParams.FILL_PARENT);
			
			tv = new TextView(this);
			tv.setTextColor(Color.WHITE);
			tv.setPadding(10, 10, 0, 10);
			tv.setText("Comments");
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.BLACK);
			tbl.addView(v,1,LayoutParams.FILL_PARENT);
			
			tv = new TextView(this);
			tv.setText("Edit/Delete");
			tv.setGravity(Gravity.CENTER_HORIZONTAL);
			tv.setPadding(0, 10, 0, 10);
			tv.setTextColor(Color.WHITE);
			tbl.addView(tv,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			tbl.addView(tbl,lp);
			
			c.moveToFirst();
			for(int i=1;i<=c.getCount();i++,c.moveToNext())
			{
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,LayoutParams.FILL_PARENT,1);
				tbl= new TableRow(this);
				tbl.setId(c.getInt(c.getColumnIndex("_id")));
				String status=c.getString(c.getColumnIndex("SC_Status"));
				String cmt=db.decode(c.getString(c.getColumnIndex("SC_Description")));
				v.setBackgroundColor(Color.BLACK);
				tv = new TextView(this);
				tv.setGravity(Gravity.CENTER_HORIZONTAL);
				tv.setText(i+"");
				tbl.addView(tv,50,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,1,LayoutParams.FILL_PARENT);
				
				LinearLayout chli=new LinearLayout(this);
				CheckBox ch = new CheckBox(this);
				ch.setOnClickListener(new list_listener(tbl.getId()));
				chli.setGravity(Gravity.CENTER_HORIZONTAL);
				chli.addView(ch,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				chli.setPadding(0, 10, 10, 10);
				if(status.equals("true"))
				{
					ch.setChecked(true);
				}
			
				ch.setTag("Active");
				tbl.addView(chli,100,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,1,LayoutParams.FILL_PARENT);
				
				tv = new TextView(this);
				tv.setText(cmt);
				tv.setPadding(10, 10, 0, 10);
				tbl.addView(tv,500,LayoutParams.WRAP_CONTENT);
				v=new View(this);
				v.setBackgroundColor(Color.BLACK);
				tbl.addView(v,1,LayoutParams.FILL_PARENT);
				
				ImageView im1 =new ImageView(this,null,R.attr.Roof_RCN_Tit_img);
				ImageView im2 =new ImageView(this,null,R.attr.Roof_RCN_opt_img);
				im1.setTag("Edit");
				im2.setTag("Delete");
				im1.setVisibility(View.VISIBLE);
				im2.setVisibility(View.VISIBLE);
				im1.setPadding(10, 10, 10, 10);
				im1.setOnClickListener(new list_listener(tbl.getId()));
				im2.setOnClickListener(new list_listener(tbl.getId()));
				im2.setPadding(10, 10, 10, 10);
				LinearLayout li=new LinearLayout(this);
				li.addView(im1,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				li.addView(im2,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				tbl.addView(li,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
				
				tbl.addView(tbl,lp);
			}
			//findViewById(R.id.breadcum).setVisibility(View.VISIBLE);
		}
		else
		{
			tbl.removeAllViews();
			//findViewById(R.id.breadcum).setVisibility(View.GONE);
			//cf.ShowToast(" Sorry no records found for this option", 0);
			
		}
	}
	class  list_listener implements OnClickListener{ 
		int id;
		list_listener(int id)
		{
			this.id=id;
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v.getTag().equals("Active"))
			{
				final CheckBox chk= (CheckBox) v;
				String msg="";
				String Status="false";
				if(chk.isChecked())
				{
					
					msg="Are you sure, Do you want to Activate the selected comments?";
					Status="true";
					resmsg="Activated successfully.";
				}
				else
				{
					msg="Are you sure, Do you want to De-activate the selected comments?";
					Status="false";
					resmsg="De-activated successfully.";
				}
				final String sta=Status;
				AlertDialog.Builder b =new AlertDialog.Builder(AddComments.this);
				b.setTitle("Confirmation");
				b.setMessage(msg);
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						db.wind_db.execSQL(" UPDATE "+db.StandardComments+" Set SC_Status='"+sta+"' WHERE SID="+id);
						
						cf.ShowToast(resmsg);
						
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(sta.equals("true"))
						{
							chk.setChecked(false);
						}else
						{
							chk.setChecked(true);
						}
					}
				});
				AlertDialog al=b.create();al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();
			}
			else if(v.getTag().equals("Edit"))
			{
				edit_comments(id);
			}
		else if(v.getTag().equals("Delete"))
			{
				
				AlertDialog.Builder b =new AlertDialog.Builder(AddComments.this);
				b.setTitle("Confirmation");
				b.setMessage("Are you sure, Do you want to delete the selected comment?");
				b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						db.wind_db.execSQL(" Delete from "+db.StandardComments+"  WHERE SID="+id);
						//clear_all();
						/*if(!sp2.getSelectedItem().toString().equals(cf.getResourcesvalue(R.string.b11802_8)))
						{
							
						}*/
						cf.ShowToast("Deleted successfully");
						//showSavedComments();
						view_comments();
					}
				});
				b.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					
					}
				});   
				AlertDialog al=b.create();
				al.setIcon(R.drawable.alertmsg);
				al.setCancelable(false);
				al.show();    
			}
		}
	};
	public void edit_comments(int id) {
		// TODO Auto-generated method stub
		Cursor c=db.SelectTablefunction(db.StandardComments, " WHERE SID="+id);
		c.moveToFirst();
		if(c.getCount()>0)
		{
		 String s1_val,s2_val,s3_val,s4_val,comm_val;
		
		 s1_val=db.decode(c.getString(c.getColumnIndex("SC_QuesID")));
		 s2_val=db.decode(c.getString(c.getColumnIndex("SC_OptionID")));
		 comm_val=db.decode(c.getString(c.getColumnIndex("SC_Description")));
		 
		 if(!comm_val.equals(""))
		 { int pos=0;
			
			 ed.setText(comm_val);
		 }
		 ((View) findViewById(R.id.addcomments)).setTag(id);
		 ((Button) findViewById(R.id.addcomments)).setText("Update Comments");
		
		}
	}
	public String[] sets4value(String s)
	{
			String b1_q1[] = { "-Select-", "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
			String b1_q2[] = { "-Select-","Meets FBC(PD -03/2/02) or Original (after 2004)","Meets SFBC (PD 09/01/1994) or Post 1997","One or more does not meet A or B", "None Meet A or B" };
			String b1_q3[] = { "-Select-", "Mean Uplift less than B and C","103 PSF", "182 PSF", "Concrete", "Other", "Unknown", "No Attic" };
			String b1_q4[] = { "-Select-", "Toe Nail", "Clips", "Single Wraps","Double Wraps", "Structural", "Other", "Unknown", "No Attic" };
			String b1_q5[] = { "-Select-", "Hip Roof", "Flat Roof","Other Roof Type" };
			String b1_q6[] = { "-Select-", "SWR", "No SWR", "Unknown" };
			String b1_q7[] = { "-Select-", "FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected" };
			String b1_q8[] = { "-Select-", "Wall Construction Comments" };
			
			int pos=sp1.getSelectedItemPosition();System.out.println("pos="+pos);
				switch(pos)
				{ 
					case 0:
						//tmp=s4;
					break;
					case 1:
						tmp=b1_q1;
					break;
					case 2:
						tmp=b1_q2;
					break;
					case 3:
						tmp=b1_q3;
					break;
					case 4:
						tmp=b1_q4;
					break;
					case 5:
						tmp=b1_q5;
					break;
					case 6:
						tmp=b1_q6;
					break;
					case 7:
						tmp=b1_q7;
					break;
					case 8:
						tmp=b1_q8;
					break;
				}
				return tmp;
	
		}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			cf.gohome(); 
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
			case R.id.hme:
				cf.gohome();
			break;
			case R.id.clearcomments:
				clearall();
			break;
			case R.id.viewcomments:
				if(sp1.getSelectedItemPosition()!=0)
				{
					if(sp2.getSelectedItemPosition()!=0)
					{
						view_comments();
					}
					else
					{
						cf.ShowToast("Please select Sub option for Question Type");
					}
				}
				else
				{
					cf.ShowToast("Please select Question Type");
				}
			break;
			case R.id.addcomments:
				save_comments();
			break;
		default:
			break;
		}
	}
	private void save_comments() {
		// TODO Auto-generated method stub
		System.out.println("save comment");
		if(sp1.getSelectedItemPosition()!=0)
		{
			
			if(sp2.getSelectedItemPosition()!=0)
			{
				if(!ed.getText().toString().trim().equals(""))
				{
					if(!((Button) findViewById(R.id.addcomments)).getText().equals("Update"))
					{
					
							Cursor c=	db.SelectTablefunction(db.StandardComments, " WHERE SC_InspectorId='"+db.Insp_id+"' and SC_Description='"+db.encode(ed.getText().toString().trim())+"' and SC_QuesID='"+sp1.getSelectedItemPosition()+"'");
							if(c.getCount()>0)
							{
								cf.ShowToast(" This Comment already available");
								return;
							}
					
					
						db.wind_db.execSQL(" INSERT INTO "+db.StandardComments+" (SC_InspectorId,SC_QuesID,SC_OptionID,SC_Description,SC_Status) VALUES " +
								"('"+db.Insp_id+"','"+sp1.getSelectedItemPosition()+"','"+sp2.getSelectedItemPosition()+"','"+db.encode(ed.getText().toString().trim())+"','1')");
					}
					else
					{
						db.wind_db.execSQL(" UPDATE  "+db.StandardComments+" SET SC_QuesID='"+sp1.getSelectedItemPosition()+"',SC_Description='"+db.encode(ed.getText().toString().trim())+"' WHERE SId='"+((TextView) findViewById(R.id.edit_id)).getText().toString()+"'");
						((TextView) findViewById(R.id.edit_id)).setText("");
						//li_insp.setVisibility(View.VISIBLE);
						((Button) findViewById(R.id.addcomments)).setText("Save/Add more comments");
						//((Button) findViewById(R.id.addcomment_BTview)).setVisibility(View.VISIBLE);
					}
					//clearall();
					System.out.println("save ed");
					ed.setText("");
					cf.ShowToast("Comments saved succesfully");
					view_comments();
				}
				else
				{
					cf.ShowToast("Please enter Comments");
				}
			}
			else
			{
				cf.ShowToast("Please enter Questions option");
			}
		}
		else
		{
			cf.ShowToast("Please select Questions");
		}
	}
	private void view_comments() {
		// TODO Auto-generated method stub
		System.out.println("view_comments");
		TableRow tbl_rw;
		View  v;
		Cursor c=null;;
		tbl.removeAllViews();
		String insp_typ="";
		
		c =db.SelectTablefunction(db.StandardComments, " WHERE SC_InspectorId='"+db.Insp_id+"' and SC_QuesID='"+sp1.getSelectedItemPosition()+"'  and SC_OptionID='"+sp2.getSelectedItemPosition()+"'");
		System.out.println("dsd"+c.getCount());
		if(c!=null)
		{
			if(c.getCount()>0)
			{
				System.out.println("dsd>>");
				((TextView) findViewById(R.id.no_comments)).setVisibility(View.GONE);System.out.println("no_comments");
				
				TextView TV_breadcum=new TextView(this);
				TV_breadcum.setText(Html.fromHtml("<b> Question Type</b>:&nbsp;"+sp1.getSelectedItem().toString()));
				TV_breadcum.setTextSize(16);
				TV_breadcum.setPadding(0, 10, 0, 10);
				TV_breadcum.setGravity(Gravity.CENTER);
				tbl.addView(TV_breadcum,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				tbl_rw=new TableRow(this);
			
			//tbl_rw.setBackgroundColor(getResources().getColor(R.color.sub_tabs));
			//tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenborderwithsubtabbackgroundcolor));
				
			tbl_rw.setBackgroundColor(Color.BLACK);
				
			TextView tv_no=new TextView(this,null,R.attr.textview_200);
			tv_no.setMinHeight(40);
			//tv_no.setGravity(Gravity.CENTER_VERTICAL);
			tv_no.setText("No");
			tv_no.setPadding(10, 2, 0,2);
			tbl_rw.addView(tv_no,50,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.parseColor("#323232"));System.out.println("outline");
			
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_area=new TextView(this,null,R.attr.textview_200);
			tv_area.setText("Type");
			tv_area.setVisibility(View.GONE);//**only retail record present so we no need show this**//* 
			tbl_rw.addView(tv_area,150,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setVisibility(View.GONE);//**only retail record present so we no need show this**//*
			v.setBackgroundColor(Color.parseColor("#323232"));
			
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
			TextView tv_sparea=new TextView(this,null,R.attr.textview_200);
			tv_sparea.setText("Comments");
			tv_sparea.setPadding(10, 2, 0,2);
			tbl_rw.addView(tv_sparea,550,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.parseColor("#323232"));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);System.out.println("ftrtrt");
			
			TextView tv_status=new TextView(this,null,R.attr.textview_200);
			tv_status.setText("Active Status");
			tv_status.setPadding(10, 2, 2,2);
			tbl_rw.addView(tv_status,100,LayoutParams.WRAP_CONTENT);
			v=new View(this);
			v.setBackgroundColor(Color.parseColor("#323232"));
			tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);System.out.println("fill");
			
			
			TextView tv_Edit=new TextView(this,null,R.attr.textview_200);
			tv_Edit.setText("Edit/Delete");
			tv_Edit.setPadding(20, 2, 2,2);
			tbl_rw.addView(tv_Edit,150,LayoutParams.WRAP_CONTENT);
			tbl.addView(tbl_rw);
			c.moveToFirst();System.out.println("moveToFirst");
		
	for(int i =0;i<c.getCount();i++,c.moveToNext())
	{System.out.println("getCount");
		String comments,type = "",status;
		int id=c.getInt(c.getColumnIndex("SID"));
		comments=db.decode(c.getString(c.getColumnIndex("SC_Description")));
		
		status=c.getString(c.getColumnIndex("SC_Status"));System.out.println("status");
		tbl_rw=new TableRow(this);
		TextView tv_no1=new TextView(this,null,R.attr.textview_200);
		tv_no1.setText((i+1)+"");
		tv_no1.setTextColor(Color.BLACK);
		tv_no1.setPadding(10, 2, 0,2);
		tbl_rw.addView(tv_no1);
		
		v=new View(this);System.out.println("virew");
		v.setBackgroundColor(Color.parseColor("#323232"));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		
		TextView tv_area1=new TextView(this,null,R.attr.textview_200);
		tv_area1.setText(type);
		tv_area1.setPadding(10,2, 0,2);
		tv_area1.setVisibility(View.GONE);//**only retail record present so we no need show this**//*
		tbl_rw.addView(tv_area1,150,LayoutParams.WRAP_CONTENT);
		
		v=new View(this);
		v.setVisibility(View.GONE);//**only retail record present so we no need show this**//*
		v.setBackgroundColor(Color.parseColor("#323232"));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		
		TextView tv_sparea1=new TextView(this,null,R.attr.textview_200);
		tv_sparea1.setText(comments);
		tv_sparea1.setTextColor(Color.BLACK);
		tbl_rw.addView(tv_sparea1,550,LayoutParams.WRAP_CONTENT);
		
		v=new View(this);
		v.setBackgroundColor(Color.parseColor("#323232"));		
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);System.out.println("tttt");
		
		LinearLayout li1=new LinearLayout(this);
		CheckBox chk =new CheckBox(this);
		chk.setOnClickListener(new clickerlistenr(id,status));
		chk.setTag(id);//set the id here
		if(status.equals("0"))
		{
		chk.setChecked(true);
		}
		else
		{
		chk.setChecked(false);
		}
		li1.addView(chk);
		li1.setGravity(Gravity.CENTER_HORIZONTAL);
		tbl_rw.addView(li1,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		v=new View(this);
		v.setBackgroundColor(Color.parseColor("#323232"));
		tbl_rw.addView(v,2,LayoutParams.FILL_PARENT);
		
		LinearLayout li=new LinearLayout(this);
		ImageView im =new ImageView(this);
		im.setBackgroundDrawable(getResources().getDrawable(R.drawable.roofedit));
		ImageView im1 =new ImageView(this);
		im1.setBackgroundDrawable(getResources().getDrawable(R.drawable.roofdelete));
		li.addView(im);
		li.addView(im1);
		im.setOnClickListener(new edit_clicker(id,comments));
		im1.setOnClickListener(new del_clickerlistenr(id));
		li.setGravity(Gravity.CENTER_HORIZONTAL);
		tbl_rw.addView(li);
		li.setPadding(5, 10, 5, 10);
		tbl_rw.setBackgroundDrawable(getResources().getDrawable(R.drawable.commentborderwithoutcurve));
		tbl.addView(tbl_rw);
		}
			}
			else
			{
				((TextView) findViewById(R.id.no_comments)).setVisibility(View.VISIBLE);
			}
			
		}
	}
	class clickerlistenr implements OnClickListener
	{
		int id;
		String status;
		String sta="Activate";
		String sta_val="true";
		
		public clickerlistenr(int id, String status) {
			// TODO Auto-generated constructor stub
			this.id=id;
			this.status=status;
		}

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			System.out.println("status"+status);
			if(status.equals("1"))
			{
				sta="Activate";
				sta_val="0";
			}
			else
			{
				sta="De-Activate";
				sta_val="1";
			}
			AlertDialog.Builder b =new AlertDialog.Builder(AddComments.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to change the status to "+ sta+"?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						db.wind_db.execSQL(" UPDATE  "+db.StandardComments+" SET SC_Status='"+sta_val+"' WHERE SID='"+id+"'");
						
						status=sta_val;
						cf.ShowToast("Status changed successfully");
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					if(status.equals("0"))
						((CheckBox)v).setChecked(true);
					else
						((CheckBox)v).setChecked(false);
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show(); 
		
		}
		
	}
	class del_clickerlistenr implements OnClickListener
	{
		int id;
		
		
		public del_clickerlistenr(int id) {
			// TODO Auto-generated constructor stub
			this.id=id;
		
		}

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			
			AlertDialog.Builder b =new AlertDialog.Builder(AddComments.this);
			b.setTitle("Confirmation");
			b.setMessage("Do you want to delete the selected comments?");
			b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					try
					{
						db.wind_db.execSQL(" DELETE FROM  "+db.StandardComments+"  WHERE SID='"+id+"'");
						cf.ShowToast("Selected comment deleted successfully");
						view_comments();
						
						
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("the exeption in delete"+e.getMessage());
					}
					
					
					
				}
			});
			b.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}
			});   
			AlertDialog al=b.create();
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show(); 
		
		}
		
	}
	class edit_clicker implements OnClickListener
	{
		int id;
		String comments;
		edit_clicker(int id,String comment)
		{
			this.id=id;
			comments=comment;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			((TextView) findViewById(R.id.edit_id)).setText(id+"");
			ed.setText(comments);
			//li_insp.setVisibility(View.GONE);
			((Button) findViewById(R.id.addcomments)).setText("Update");
			//((Button) findViewById(R.id.addcomment_BTview)).setVisibility(View.GONE);
		}
		
	}
	private void clearall() {
		// TODO Auto-generated method stub
		sp1.setSelection(0);
		ed.setText("");
		tbl.removeAllViews();
		((TextView) findViewById(R.id.edit_id)).setText("");
		((Button) findViewById(R.id.addcomments)).setText("Save/Add more comments");
	}

}
