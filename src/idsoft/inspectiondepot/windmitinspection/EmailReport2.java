package idsoft.inspectiondepot.windmitinspection;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmailReport2 extends Activity {
	CheckBox ckinspector, ckcustomer, ckother, ckattachreport;
	EditText etinspector, etcustomer, etother, etto, etsubject, etbody;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wf;
	String inspectorid, inspectorname, inspectoremail, srid, chklogin, path,
			to, classidentifier, mailid, ownersname, policynumber,status,companyid,companyname;
	boolean report;
	int show_handler;
	LinearLayout llreport;
	TextView tvpercentage;
	String cntrws,companyphone="",companyemail="";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email_report2);
		cf = new CommonFunctions(this);
		db = new DatabaseFunctions(this);
		wf = new WebserviceFunctions(this);
		
		cf.setTouchListener(findViewById(R.id.widget54));
		ckinspector = (CheckBox) findViewById(R.id.e_report2_ckinspectormailid);
		ckcustomer = (CheckBox) findViewById(R.id.e_report2_ckcustomermailid);
		ckother = (CheckBox) findViewById(R.id.e_report2_ckothermailid);
		ckattachreport = (CheckBox) findViewById(R.id.e_report2_ckattachreport);
		etinspector = (EditText) findViewById(R.id.e_report2_etinspectormail);
		etcustomer = (EditText) findViewById(R.id.e_report2_etcustomermail);
		etother = (EditText) findViewById(R.id.e_report2_etothermail);
		etto = (EditText) findViewById(R.id.e_report2_etto);
		etsubject = (EditText) findViewById(R.id.e_report2_etsubject);
		etbody = (EditText) findViewById(R.id.e_report2_etbody);
		llreport = (LinearLayout) findViewById(R.id.e_report2_llreport);
		tvpercentage = (TextView) findViewById(R.id.e_report2_tvpercentage);

		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		policynumber = bundle.getString("policynumber");System.out.println("polic-"+policynumber);
		status = bundle.getString("status");
		mailid = bundle.getString("mailid");
		classidentifier = bundle.getString("classidentifier");
		ownersname = bundle.getString("ownersname");
		cntrws = bundle.getString("count");
		
		if(status.equals("40")||status.equals("30"))
		{
			status = "Scheduled";
		}
		else if(status.equals("1"))
		{
			status = "Inspection Completed";
		}
		else if(status.equals("90"))
		{
			status = "Cancelled";
		}
		else if(status.equals("2"))
		{
			status = "Download Reports";
		}
		
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside");
				Intent insp_info = new Intent(EmailReport2.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		
		db.CreateTable(1);
		db.CreateTable(2);
		db.getInspectorId();
		db.getPHinformation(policynumber);
		
		Cursor cur=db.wind_db.rawQuery("select * from "+db.inspectorlogin + " where Ins_Id='"+db.Insp_id+"'", null);
		System.out.println("count="+cur.getCount());
		if(cur.getCount()>0)
		{
			cur.moveToFirst();
			companyid=db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyId")));
			companyname=db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyName")));
			companyphone=db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyPhone")));
			companyemail=db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyEmail")));

		}
		
		etbody.setText("Thank you for choosing \""+companyname+"\" for your inspection needs. Attached is your completed \"Wind Mitigation Inspection\" If you have any questions please email or contact me on the following information. (Phone : "+companyphone +" or Email : "+companyemail+")");
		System.out.println("db.ph_lastname"+db.ph_lastname);
		etsubject.setText("Re: Wind Mitigation Inspection Report - "+db.ph_firstname +" "+db.ph_lastname);
		
		
		Call_Pdf_Available();

		
		
		etinspector.setText(db.Insp_email);
		etcustomer.setText(mailid);

		cf.Device_Information();

		ckinspector.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String inspectormailid = etinspector.getText().toString();
				if (isChecked) {
					if (cf.Email_Validation(inspectormailid) == true)
					{
						etto.append(inspectormailid + ",");
						etinspector.setEnabled(false);
					//	etinspector.setBackgroundColor(getResources().getColor(R.color.disables));
					} else {
						cf.ShowToast("Inspector Mail Id is in invalid format");
						// etagent.setText("");
						etinspector.requestFocus();
						ckinspector.setChecked(false);
					}
				} else {
					String agency = etto.getText().toString();
					String inspector = etinspector.getText().toString();
					if (agency.contains(inspector + ",")) {
						agency = agency.replace(inspector + ",", "");
						etto.setText(agency);
						etinspector.setEnabled(true);
					//	etinspector.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});

		ckcustomer.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String customermailid = etcustomer.getText().toString();
				if (isChecked) {
					if (cf.Email_Validation(customermailid) == true){
						etto.append(customermailid + ",");
						etcustomer.setEnabled(false);
						//etcustomer.setBackgroundColor(getResources().getColor(R.color.disables));
					} else {
						cf.ShowToast("Customer Mail Id is in invalid format");
						// etcustomer.setText("");
						etcustomer.requestFocus();
						ckcustomer.setChecked(false);
					}
				} else {
					String agency = etto.getText().toString();
					String customer = etcustomer.getText().toString();
					if (agency.contains(customer + ",")) {
						agency = agency.replace(customer + ",", "");
						etto.setText(agency);
						etcustomer.setEnabled(true);
						//etcustomer.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});

		ckother.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					if (etother.getText().toString().trim().equals("")) {
						cf.ShowToast("Please enter Mail Id");
						// etother.setText("");
						etother.requestFocus();
						ckother.setChecked(false);
						
					} else {
						String othermailid = etother.getText().toString();
						if (othermailid.contains(",")) {
							String[] splitvalue = othermailid.split(",");
							String valid = "";
							for (int i = 0; i < splitvalue.length; i++) {
								if (cf.Email_Validation(splitvalue[i]) == true){
									valid += "true";
								} else {
									valid += "false";
								}
							}
							if (valid.contains("false")) {
								cf.ShowToast("Mail Id is in invalid format");
								// etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
								
							} else {
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
								//etother.setBackgroundColor(getResources().getColor(R.color.disables));
							}
						} else {
							
							if (cf.Email_Validation(etother.getText().toString()) == true) {
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
								//etother.setBackgroundColor(getResources().getColor(R.color.disables));
								
							} else {
								cf.ShowToast("Mail Id is in invalid format");
								// etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
								
							}
						}
					}
				} else {
					String agency = etto.getText().toString();
					String other = etother.getText().toString();
					if (agency.contains(other + ",")) {
						agency = agency.replace(other + ",", "");
						etto.setText(agency);
						etother.setEnabled(true);
						//etother.setBackgroundColor(getResources().getColor(R.color.nomal));
					}
				}

			}
		});

		etinspector.addTextChangedListener(new TextWatcher1(etinspector));
		etcustomer.addTextChangedListener(new TextWatcher1(etcustomer));
		etother.addTextChangedListener(new TextWatcher1(etother));
		// etbody.addTextChangedListener(new TextWatcher1(etbody));
		etsubject.addTextChangedListener(new TextWatcher1(etsubject));

		etbody.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etbody.getText().toString().startsWith(" ")) {
					// Not allowed
					etbody.setText("");
				}
				int length = etbody.getText().toString().length();
				if (length == 0) {
					tvpercentage.setText("0%");
				} else {
					length = length / 3;
					tvpercentage.setText(String.valueOf(length) + "%");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.e_report2_btnsend:
			Check_Validation();
			break;

		case R.id.e_report2_btncancel:
			System.out.println("classiden="+classidentifier);
			if(classidentifier.equals("EmailReport"))
			{
				Intent intenthome = new Intent(EmailReport2.this, EmailList.class);
				intenthome.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intenthome);
				finish();
			}
			else if(classidentifier.equals("Export"))
			{
				Intent intenthome = new Intent(EmailReport2.this, Export.class);
				intenthome.putExtra("type", "emailreport");
				intenthome.putExtra("classidentifier", classidentifier);
				startActivity(intenthome);
				finish();
			}
			else if(classidentifier.equals("DashBoard"))
			{
				Intent intentreportready=new Intent(EmailReport2.this,View_pdf.class);
				intentreportready.putExtra("type", "emailreport");
				intentreportready.putExtra("classidentifier", classidentifier);
				intentreportready.putExtra("total_record",cntrws);
				intentreportready.putExtra("current", 1);
				
				startActivity(intentreportready);
				finish();
			}
			break;
		case R.id.e_report2_btnviewreport:
			String[] filenamesplit = path.split("/");
			final String filename = filenamesplit[filenamesplit.length - 1];
			System.out.println("The File Name is " + filename);
			File sdDir = new File(Environment.getExternalStorageDirectory()
					.getPath());
			File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
					+ filename);
			if (file.exists()) {
				View_Pdf_File(filename);
			} else {
				if (wf.isInternetOn() == true) {
					cf.show_ProgressDialog("Downloading... Please wait.");
					new Thread() {
						public void run() {
							Looper.prepare();
							try {
								String extStorageDirectory = Environment
										.getExternalStorageDirectory()
										.toString();
								File folder = new File(extStorageDirectory,
										"DownloadedPdfFile");
								folder.mkdir();
								File file = new File(folder, filename);
								try {
									file.createNewFile();
									Downloader.DownloadFile(path, file);
								} catch (IOException e1) {
									e1.printStackTrace();
								}

								show_handler = 2;
								handler.sendEmptyMessage(0);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("The error is "
										+ e.getMessage());
								e.printStackTrace();
								show_handler = 1;
								handler.sendEmptyMessage(0);

							}
						}

						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
								cf.pd.dismiss();
								// dialog1.dismiss();
								if (show_handler == 1) {
									show_handler = 0;
									cf.ShowToast(
											"There is a problem on your application. Please contact Paperless administrator");

								} else if (show_handler == 2) {
									show_handler = 0;

									View_Pdf_File(filename);

								}
							}
						};
					}.start();
				} else {
					cf.ShowToast("Internet connection not available");

				}
			}
			break;

		case R.id.e_report2_toclear:
			ckinspector.setChecked(false);
			ckcustomer.setChecked(false);
			ckother.setChecked(false);
			etto.setText("");
			break;

		case R.id.e_report2_home:
			cf.gohome();
			break;

		}
	}

	private void View_Pdf_File(String filename) {
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/" + filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try {
			startActivity(intent);
		} catch (ActivityNotFoundException e) {
			// Toast.makeText(VehicleInspection.this,
			// "No Application Available to View PDF",
			// Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(EmailReport2.this, ViewPdfFile.class);
			intentview.putExtra("path", path);
			startActivity(intentview);
			// finish();
		}
	}

	private void Check_Validation() {
		if (!etto.getText().toString().trim().equals("")) {
			if (!etsubject.getText().toString().trim().equals("")) {
				if (!etbody.getText().toString().equals("")) {
					Send_Mail();
				} else {
					cf.ShowToast("Please enter Body");
				}
			} else {
				cf.ShowToast("Please enter Subject");
			}
		} else {
			cf.ShowToast("Please select Mail Id");
		}
	}

	private void Send_Mail() {
		to = etto.getText().toString();
		to = to.substring(0, to.length() - 1);
		if (!ckattachreport.isChecked()) {
			path = "";
		}

		if (wf.isInternetOn() == true) {
			cf.show_ProgressDialog("Sending Mail... ");
			new Thread() {
				String chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = wf.Calling_WS_EmailReport(policynumber,db.Insp_id, to,
								etsubject.getText().toString(), etbody
										.getText().toString(), path,
								"REPORTSREADYMAIL");
						System.out.println("response EmailReport" + chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast(
									"There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast(
									"There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 5) {
							show_handler = 0;

							if (chklogin.toLowerCase().equals("true")) {
								Intent intenthome = new Intent(
										EmailReport2.this, HomeScreen.class);
								startActivity(intenthome);
								finish();
								cf.ShowToast("Email sent successfully");
							} else {
								Intent intenthome = new Intent(
										EmailReport2.this, HomeScreen.class);
								startActivity(intenthome);
								finish();
								cf.ShowToast(
										"There is a problem on your application. Please contact Paperless administrator");
							}
						}
					}
				};
			}.start();
		} else {
			cf.ShowToast("Internet connection not available");
		}
	}

	private void Call_Pdf_Available() {
		
		if (wf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>"
					+ "Processing... Please wait" + "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(EmailReport2.this,
					"", Html.fromHtml(source), true);
			new Thread() 
			{
				public void run() {

					Looper.prepare();
					try {
						chklogin = wf.Calling_WS_ViewCustomerPDF(policynumber,
								"ViewCustomerPDF");
						System.out.println("response ViewCustomerPDF"
								+ chklogin);

						if (!chklogin.toLowerCase().equals("false")) {

							report = true;
							path = chklogin;
						} else {
							report = false;
							path = "N/A";
						}

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast(
									"There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast(
									"There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 5) {
							show_handler = 0;
							if (report == true) {
								llreport.setVisibility(View.VISIBLE);
							} else {
								llreport.setVisibility(View.GONE);
							}

						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		System.out.println("classidentifier="+classidentifier);
		if(classidentifier.equals("EmailReport"))
		{
			Intent intenthome = new Intent(EmailReport2.this, EmailList.class);
			//intenthome.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intenthome);
			finish();
		}
		else if(classidentifier.equals("DashBoard"))
		{
			Intent intentreportready=new Intent(EmailReport2.this,View_pdf.class);
			intentreportready.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intentreportready.putExtra("type", "emailreport");
			intentreportready.putExtra("classidentifier", classidentifier);
			intentreportready.putExtra("total_record", cntrws);
			intentreportready.putExtra("current", 1);			
			startActivity(intentreportready);
			finish();
		}
		else if(classidentifier.equals("Export")||classidentifier.equals("HomeScreen"))
		{
			Intent intenthome = new Intent(EmailReport2.this, Export.class);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intenthome.putExtra("type", "emailreport");
			intenthome.putExtra("classidentifier", classidentifier);
			startActivity(intenthome);
			finish();
		}
	}

}
