package idsoft.inspectiondepot.windmitinspection;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AnalogClock;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
   
public class FeedbackDocument extends Activity {
	CommonFunctions cf;
	WebserviceFunctions wb;
	DatabaseFunctions db;
	String InspectionType="",homeid="",status="";
	int value,Count;
	Spinner sp[]=new Spinner[3];
	EditText ed_others[]=new EditText[3],comments;
	LinearLayout sup_l,off_l;
	String[] array_who={"--Select--","Owner","Representative","Agent","Other"},
	array_doc={"--Select--","Acknowledgement Form","CSE Form","OIR 1802 Form","Paper Signup Sheet","Other Information","Roof Permit","Sketch","Building Permit","Property Appraisal Information","Field Inspection Report","Field Notes"};
    RadioGroup rg[] = new RadioGroup[3]; 
    private boolean load_comment=true;
    Validation va;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db = new DatabaseFunctions(this);
		wb = new WebserviceFunctions(this);
	
		setContentView(R.layout.feedbackdocument);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}
		System.out.println("feedbc="+cf.status);
			cf.getDeviceDimensions();
			db.CreateTable(10);
			db.CreateTable(11);
		  /** menu **/
			LinearLayout layout = (LinearLayout) findViewById(R.id.header);
	        layout.addView(new MyOnclickListener(FeedbackDocument.this,4,cf,0));
	        va =new Validation(cf);
	        declaration();
	        show_saved_data();
		
	}
	private void declaration() {
		// TODO Auto-generated method stub
		sp[0]=(Spinner) findViewById(R.id.feedback_SPwhowas);
		sp[1]=(Spinner) findViewById(R.id.feedback_SPsupplement);
		sp[2]=(Spinner) findViewById(R.id.feedback_SPoffice);
		ed_others[0]=(EditText) findViewById(R.id.feedback_EDwhowas_other);
		ed_others[1]=(EditText) findViewById(R.id.feedback_EDsupplement_other);
		ed_others[2]=(EditText) findViewById(R.id.feedback_EDoffice_other);
		comments=(EditText) findViewById(R.id.feedback_EDcomment);
		ArrayAdapter ad =new ArrayAdapter(this,android.R.layout.simple_spinner_item,array_who);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter ad1 =new ArrayAdapter(this,android.R.layout.simple_spinner_item,array_doc);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp[0].setAdapter(ad);
		sp[1].setAdapter(ad1);
		sp[2].setAdapter(ad1);
		sp[0].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[0].getSelectedItem().toString().equals("Other"))
				{
					ed_others[0].setVisibility(View.VISIBLE);
					ed_others[0].setTag("required");
				}
				else
				{
					ed_others[0].setText("");
					ed_others[0].setVisibility(View.GONE);
					ed_others[0].setTag("");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp[1].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[1].getSelectedItem().toString().equals("Other Information"))
				{
					ed_others[1].setVisibility(View.VISIBLE);
				}
				else
				{
					ed_others[1].setText("");
					ed_others[1].setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		sp[2].setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp[2].getSelectedItem().toString().equals("Other Information"))
				{
					ed_others[2].setVisibility(View.VISIBLE);
				}
				else
				{
					ed_others[2].setText("");
					ed_others[2].setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		comments.setOnTouchListener(new TouchFoucs(comments));
		comments.addTextChangedListener(new TextWatchLimit(comments, 500, ((TextView) findViewById(R.id.feedback_TVcommentslimit))));
		cf.setTouchListener(findViewById(R.id.content)); 
		sup_l=(LinearLayout)findViewById(R.id.feedback_LLsupplement_dy_list);
		off_l=(LinearLayout)findViewById(R.id.feedback_LLoffice_dy_list);
		rg[0]=(RadioGroup) findViewById(R.id.feedback_RGcustomerservice);
		rg[1]=(RadioGroup) findViewById(R.id.feedback_Insowner);
		rg[2]=(RadioGroup) findViewById(R.id.feedback_Manowner);
		
		Cursor c1=db.SelectTablefunction(db.FeedBackInfo, " WHERE FD_SRID='"+cf.Homeid+"'");
		if(c1.getCount()>0)
		{
			c1.moveToFirst();
			String csc,who,who_other,insupaperwork,manuinfo,comments;
			csc=db.decode(c1.getString(c1.getColumnIndex("FD_CSC")));
			who=db.decode(c1.getString(c1.getColumnIndex("FD_whowas")));
			who_other=db.decode(c1.getString(c1.getColumnIndex("FD_whowas_other")));
			insupaperwork=db.decode(c1.getString(c1.getColumnIndex("FD_insupaperwork")));
			manuinfo=db.decode(c1.getString(c1.getColumnIndex("FD_manuinfo")));
			comments=db.decode(c1.getString(c1.getColumnIndex("FD_comments")));
			cf.setvaluerd(rg[0], csc);
			cf.setvaluerd(rg[1], insupaperwork);
			cf.setvaluerd(rg[2], manuinfo);
			sp[0].setSelection(ad.getPosition(who));
			ed_others[0].setText(who_other);
			this.comments.setText(comments);
			
		}
		
		show_saved_data();
	}
	

	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.feedback_BTsupplement_browse:
			if(sp[1].getSelectedItemPosition()!=0)
			{
				if(!sp[1].getSelectedItem().toString().trim().equals("Other Information") || !ed_others[1].getText().toString().trim().equals(""))
				{
					Intent reptoit1 = new Intent(this,Select_phots.class);
					reptoit1.putExtra("Selectedvalue",""); /**Send the already selected image **/
					reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
					reptoit1.putExtra("Db_count",0); /**SendDatabase count ofr the image order**/
					reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
					reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
					reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
					reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
					startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
				//	finish();
				}
				else
				{
					cf.ShowToast("Please enter the Other information for the supplemental document");
				}
				
			}
			else
			{
				cf.ShowToast("Please select supplemental document title");
			}
		break;
		case R.id.feedback_BToffice_browse:
			if(sp[2].getSelectedItemPosition()!=0)
			{
				if(!sp[2].getSelectedItem().toString().trim().equals("Other Information") || !ed_others[2].getText().toString().trim().equals(""))
				{
					Intent reptoit1 = new Intent(this,Select_phots.class);
					reptoit1.putExtra("Selectedvalue",""); /**Send the already selected image **/
					reptoit1.putExtra("office_use","false"); /**Send the is for office use **/
					reptoit1.putExtra("Db_count",0); /**SendDatabase count ofr the image order**/
					reptoit1.putExtra("Maximumcount", 0);/**Total count of image in the database **/
					reptoit1.putExtra("Total_Maximumcount", 0); /***Total count of image we need to accept**/
					reptoit1.putExtra("unlimeted", "true"); /***Total count of image we need to accept**/
					reptoit1.putExtra("PDF", "true"); /***Total count of image we need to accept**/
					startActivityForResult(reptoit1,122); /** Call the Select image page in the idma application  image ***/
					//finish();
				}
				else
				{
					cf.ShowToast("Please enter the Other information for the office document");
				}
				
			}
			else
			{
				cf.ShowToast("Please select office document title");
			}
		break;
		case R.id.hme:
			cf.gohome();
		break;
		case R.id.clear:
			sp[0].setSelection(0);
			rg[0].clearCheck();
			rg[1].clearCheck();
			comments.setText("");
		break;
		case R.id.save:
			save_values();
		break;
		default:
			break;
		}
		
	}
	private void save_values() {
		// TODO Auto-generated method stub
		if(va.validate(rg[0], "Customer service evaluation form completed and signed by owner / representative"))
		{
			 if(va.validate(sp[0], "Who was present at inspection?",array_who[0]))
			 {
				if(va.validate_other(ed_others[0], "Who was present at inspection?", "Other"))
				{
					if(va.validate(rg[1], "Did Homeowner have Insurance Paperwork available?"))
					{
						if(va.validate(rg[2], "Did Homeowner have Manufacture Information available?"))
						{
							
						/*if(!comments.getText().toString().trim().equals(""))
						{*/
						/*	Cursor c=db.SelectTablefunction(db.FeedBackDocument, " WHERE FD_D_SRID='"+cf.Homeid+"' and FD_D_type='0'");
							if(c.getCount()>0)
							{*/
								Cursor c1=db.SelectTablefunction(db.FeedBackInfo, " WHERE FD_SRID='"+cf.Homeid+"'");
								String who,who_other,csc,insupaperwork,manuinfo,comments;
							
								
								who=db.encode(sp[0].getSelectedItem().toString().trim());
								who_other=db.encode(ed_others[0].getText().toString().trim());
								
								csc=db.encode(((RadioButton) rg[0].findViewById((rg[0].getCheckedRadioButtonId()))).getText().toString().trim());
								insupaperwork=db.encode(((RadioButton) rg[1].findViewById((rg[1].getCheckedRadioButtonId()))).getText().toString().trim());
								manuinfo=db.encode(((RadioButton) rg[2].findViewById((rg[2].getCheckedRadioButtonId()))).getText().toString().trim());
								comments=db.encode(this.comments.getText().toString().trim());
								if(c1.getCount()>0)
								{
									db.wind_db.execSQL("UPDATE "+db.FeedBackInfo+" SET FD_CSC='"+csc+"',FD_whowas='"+who+"',FD_whowas_other='"+who_other+"',FD_insupaperwork='"+insupaperwork+"',FD_manuinfo='"+manuinfo+"',FD_comments='"+comments+"' WHERE FD_SRID='"+cf.Homeid+"'");
								}
								else
								{
									db.wind_db.execSQL(" INSERT INTO "+db.FeedBackInfo+" (FD_InspectorId,FD_SRID,FD_CSC,FD_whowas,FD_whowas_other,FD_insupaperwork,FD_manuinfo,FD_comments) VALUES" +
											" ('"+db.Insp_id+"','"+cf.Homeid+"','"+csc+"','"+who+"','"+who_other+"','"+insupaperwork+"','"+manuinfo+"','"+comments+"')");
								}
								cf.ShowToast("Feedback information saved successfully ");
								Intent in =new Intent(FeedbackDocument.this,OverallComments.class);System.out.println("cf.hom"+cf.Homeid);
								in.putExtra("homeid", cf.Homeid);
								in.putExtra("status", cf.status);
								startActivity(in);
								finish();
							/*}
							else
							{
								cf.ShowToast("Please add feedback document for Supplemental Documents ");
								
							}*/
						//}
						/*else
						{
							cf.ShowToast("Please enter Inspector Feedback Comments - For Office Use Only ");
						}*/
						}
					}	
				}
			}
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==RESULT_OK && requestCode!=cf.loadcomment_code)
		{ int type=0; 
			String title="";
			String title_other="";
			if(requestCode==121)
			{
				type=0;
				System.out.println("sp11="+sp[1].getSelectedItem().toString());
				if(sp[1].getSelectedItem().toString().trim().equals("Other Information"))
				{
					title=db.encode(ed_others[1].getText().toString().trim());	
				}
				else
				{
					title=db.encode(sp[1].getSelectedItem().toString().trim());
				}
				sp[1].setSelection(0);
			}
			else if(requestCode==122)
			{
				type=1;
				System.out.println("sp22="+sp[2].getSelectedItem().toString());
				if(sp[2].getSelectedItem().toString().trim().equals("Other Information"))
				{
					title=db.encode(ed_others[2].getText().toString().trim());	
				}
				else
				{
					title=db.encode(sp[2].getSelectedItem().toString().trim());
				}
				sp[2].setSelection(0);
			}
			  String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
				for(int i=0;i<value.length;i++ )
				{
					System.out.println(" INSERT INTO "+db.FeedBackDocument+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
							"('"+db.Insp_id+"','"+cf.Homeid+"','"+title+"','','"+db.encode(value[i])+"','"+type+"') ");
					
					db.wind_db.execSQL(" INSERT INTO "+db.FeedBackDocument+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
							"('"+db.Insp_id+"','"+cf.Homeid+"','"+title+"','','"+db.encode(value[i])+"','"+type+"') ");
				}
				show_saved_data();
		}
		 if(requestCode==cf.loadcomment_code)
			{
					load_comment=true;
					if(resultCode==RESULT_OK)
					{
						comments.setText((comments.getText().toString()+" "+data.getExtras().getString("Comments")).trim());
						
					}
					/*else if(resultCode==RESULT_CANCELED)
					{
						cf.ShowToast("You have canceled  the comments selction ",0);
					}*/
			}		
		
	}
	private void show_saved_data() {
		// TODO Auto-generated method stub
	
		Cursor c =db.SelectTablefunction(db.FeedBackDocument, " WHERE FD_D_SRID='"+cf.Homeid+"'");
		
		sup_l.removeAllViews();
		off_l.removeAllViews();
		findViewById(R.id.feedback_LLsupplement_dy_main).setVisibility(View.GONE);
		findViewById(R.id.feedback_LLoffice_dy_main).setVisibility(View.GONE);
		if(c.getCount()>0)
		{
			
			c.moveToFirst();
			do{
				String path,doc_tit,doc_tit_other;
				int id,type;
				path=db.decode(c.getString(c.getColumnIndex("FD_D_path")));
				doc_tit=db.decode(c.getString(c.getColumnIndex("FD_D_doctit")));
				id=c.getInt(c.getColumnIndex("FD_D_Id"));
				type=c.getInt(c.getColumnIndex("FD_D_type"));
				LinearLayout li=new LinearLayout(this);
				ImageView im =new ImageView(this);
				im.setImageDrawable(getResources().getDrawable(R.drawable.allfilesicon));
				li.addView(im);
				TextView tv =new TextView(this);
				tv.setTag(id);
				tv.setText(doc_tit);
				li.addView(tv,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				li.setPadding(0, 5, 10, 5);
				li.setOnClickListener(new my_dyclicker(id,path,doc_tit));
				li.setGravity(Gravity.CENTER_VERTICAL);
				System.out.println("type="+type);
				if(type==0)
				{
					findViewById(R.id.feedback_LLsupplement_dy_main).setVisibility(View.VISIBLE);
						sup_l.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
						View v=new View(this);
						v.setBackgroundColor(getResources().getColor(R.color.main_menu));
						sup_l.addView(v,LayoutParams.FILL_PARENT,2);
					
				}
				else if(type==1)
				{
					findViewById(R.id.feedback_LLoffice_dy_main).setVisibility(View.VISIBLE);
					off_l.addView(li,LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
					View v=new View(this);
					v.setBackgroundColor(getResources().getColor(R.color.main_menu));
					off_l.addView(v,LayoutParams.FILL_PARENT,2);
					
					
				}
				else
				{
					cf.ShowToast("Comes in the wrong place");
					
				}
			}while (c.moveToNext());
		}
			
	}
	class my_dyclicker implements OnClickListener
	{
		int doc_id;
		String path;
		String title;
		 int currnet_rotated;
		 Bitmap rotated_b;
		 View v2;

		my_dyclicker(int id,String path,String title)
		{
			this.doc_id=id;
			this.path=path;
			this.title=title;
			
			
		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			 final Dialog dialog = new Dialog(FeedbackDocument.this);
	            dialog.getWindow().setContentView(R.layout.maindialog);
	            dialog.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+title+"</font>"));//.pri
	            
	            dialog.setCancelable(true);
	            final ImageView img = (ImageView) dialog.findViewById(R.id.ImageView01);
	            EditText ed1 = (EditText) dialog.findViewById(R.id.TextView01);
	            ed1.setVisibility(v2.GONE);img.setVisibility(v2.GONE);
	            
	            Button button_close = (Button) dialog.findViewById(R.id.Button01);
	    		button_close.setText("Close");//button_close.setVisibility(v2.GONE);
	    		
	    		
	            Button button_d = (Button) dialog.findViewById(R.id.Button03);
	    		button_d.setText("Delete");
	    		button_d.setVisibility(v2.VISIBLE);
	    		
	    		final Button button_view = (Button) dialog.findViewById(R.id.Button02);
	    		button_view.setText("View");
	    		button_view.setVisibility(v2.VISIBLE);
	    		
	    		
	    		final Button button_saveimage= (Button) dialog.findViewById(R.id.Button05);
	    		button_saveimage.setVisibility(v2.GONE);
	    		final LinearLayout linrotimage = (LinearLayout) dialog.findViewById(R.id.linrotation);
	    		linrotimage.setVisibility(v2.GONE);
	    		
	    		final Button rotateleft= (Button) dialog.findViewById(R.id.rotateleft);
	    		rotateleft.setVisibility(v2.GONE);
	    		final Button rotateright= (Button) dialog.findViewById(R.id.rotateright);
	    		rotateright.setVisibility(v2.GONE);
	     		
	    		button_d.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            
		            AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackDocument.this);
		   			builder.setMessage("Are you sure? Do you want to delete the selected document?")
		   				.setTitle("Confirmation")
		   				.setIcon(R.drawable.alertmsg)
	   			       .setCancelable(false)
	   			       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			        	 db.wind_db.execSQL("Delete From " +  db.FeedBackDocument + "  WHERE FD_D_Id ='" + doc_id + "'");
	   			        	cf.ShowToast("Document has been deleted sucessfully");
	   					   show_saved_data();
	   			           }
	   			       })
	   			       .setNegativeButton("No", new DialogInterface.OnClickListener() {
	   			           public void onClick(DialogInterface dialog, int id) {
	   			                dialog.cancel();
	   			           }
	   			       });
	   			 builder.show();
	            
	   			 dialog.cancel(); 
            }
		           	
		  	 });
	    		
	    		
	    		
	    		button_view.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            currnet_rotated=0;
		            	if(path.endsWith(".pdf"))
					     {
		            		img.setVisibility(v2.GONE);
		            		String tempstr ;
		            		if(path.contains("file://"))
		    		    	{
		    		    		 tempstr = path.replace("file://","");		    		
		    		    	}
		            		else
		            		{
		            			tempstr = path;
		            		}
		            		 File file = new File(tempstr);
						 
			                 if (file.exists()) {
			                	Uri path = Uri.fromFile(file);
			                    Intent intent = new Intent(Intent.ACTION_VIEW);
			                    intent.setDataAndType(path, "application/pdf");
			                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			  
			                    try {
			                        startActivity(intent);
			                       // finish();
			                    } 
			                    catch (ActivityNotFoundException e) {
			                    	cf.ShowToast("No application available to view PDF");
			                       
			                    }
			               
			                }
					     }
		            	else  
		            	{
		            		Bitmap bitmap2=cf.ShrinkBitmap(path,250,250);
		            		BitmapDrawable bmd2 = new BitmapDrawable(bitmap2); 
	    					img.setImageDrawable(bmd2);
	    					img.setVisibility(v2.VISIBLE);
	    					linrotimage.setVisibility(v2.VISIBLE);
	    					rotateleft.setVisibility(v2.VISIBLE);
	    					rotateright.setVisibility(v2.VISIBLE);
				            button_view.setVisibility(v2.GONE);
				            button_saveimage.setVisibility(v2.VISIBLE);
				           
		            		
		            	}
		            }
	    		});
	    		rotateleft.setOnClickListener(new OnClickListener() {  			
	    			
					public void onClick(View v) {

	    				// TODO Auto-generated method stub
	    			
	    				System.gc();
	    				currnet_rotated-=90;
	    				if(currnet_rotated<0)
	    				{
	    					currnet_rotated=270;
	    				}

	    				
	    				Bitmap myImg;
	    				try {
	    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
	    					Matrix matrix =new Matrix();
	    					matrix.reset();
	    					//matrix.setRotate(currnet_rotated);
	    					
	    					matrix.postRotate(currnet_rotated);
	    					
	    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
	    					        matrix, true);
	    					 
	    					 img.setImageBitmap(rotated_b);

	    				} catch (FileNotFoundException e) {
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    				catch (Exception e) {
	    					
	    				}
	    				catch (OutOfMemoryError e) {
	    					
	    					System.gc();
	    					try {
	    						myImg=null;
	    						System.gc();
	    						Matrix matrix =new Matrix();
	    						matrix.reset();
	    						//matrix.setRotate(currnet_rotated);
	    						matrix.postRotate(currnet_rotated);
	    						myImg= cf.ShrinkBitmap(path, 800, 800);
	    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
	    						System.gc();
	    						img.setImageBitmap(rotated_b); 

	    					} catch (Exception e1) {
	    						// TODO Auto-generated catch block
	    						e1.printStackTrace();
	    					}
	    					 catch (OutOfMemoryError e1) {
	    							// TODO Auto-generated catch block
	    						 cf.ShowToast("You cannot rotate this image. Image size is too large");
	    					}
	    				}

	    			
	    			}
	    		});
	    		rotateright.setOnClickListener(new OnClickListener() {
	    		    public void onClick(View v) {
	    		    	
	    		    	currnet_rotated+=90;
	    				if(currnet_rotated>=360)
	    				{
	    					currnet_rotated=0;
	    				}
	    				
	    				Bitmap myImg;
	    				try {
	    					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
	    					Matrix matrix =new Matrix();
	    					matrix.reset();
	    					//matrix.setRotate(currnet_rotated);
	    					
	    					matrix.postRotate(currnet_rotated);
	    					
	    					 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
	    					        matrix, true);
	    					 
	    					 img.setImageBitmap(rotated_b);  

	    				} catch (FileNotFoundException e) {
	    					//.println("FileNotFoundException "+e.getMessage()); 
	    					// TODO Auto-generated catch block
	    					e.printStackTrace();
	    				}
	    				catch(Exception e){}
	    				catch (OutOfMemoryError e) {
	    					//.println("comes in to out ot mem exception");
	    					System.gc();
	    					try {
	    						myImg=null;
	    						System.gc();
	    						Matrix matrix =new Matrix();
	    						matrix.reset();
	    						//matrix.setRotate(currnet_rotated);
	    						matrix.postRotate(currnet_rotated);
	    						myImg= cf.ShrinkBitmap(path, 800, 800);
	    						rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
	    						System.gc();
	    						img.setImageBitmap(rotated_b); 

	    					} catch (Exception e1) {
	    						// TODO Auto-generated catch block
	    						e1.printStackTrace();
	    					}
	    					 catch (OutOfMemoryError e1) {
	    							// TODO Auto-generated catch block
	    						 cf.ShowToast("You cannot rotate this image. Image size is too large");
	    					}
	    				}

	    		    }
	    		});
	    		button_saveimage.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						
						if(currnet_rotated>0)
						{ 

							try
							{
								/**Create the new image with the rotation **/
						
								String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
								 ContentValues values = new ContentValues();
								  values.put(MediaStore.Images.Media.ORIENTATION, 0);
								  FeedbackDocument.this.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
								
								if(current!=null)
								{
								String path_new=cf.getPath(Uri.parse(current));
								File fout = new File(path);
								fout.delete();
								/** delete the selected image **/
								File fin = new File(path_new);
								/** move the newly created image in the slected image pathe ***/
								fin.renameTo(new File(path));
								cf.ShowToast("Saved successfully");dialog.cancel();
								show_saved_data();
								
								
							}
							} catch(Exception e)
							{
								//.println("Error occure while rotate the image "+e.getMessage());
							}
							
						}
						else
						{
							cf.ShowToast("Saved successfully");
							dialog.cancel();
							show_saved_data();
						}
						
					}
				});
	    		button_close.setOnClickListener(new OnClickListener() {
		            public void onClick(View v) {
		            	dialog.cancel();
		            }
	    		});
	    		dialog.show();
			
		}
		
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(FeedbackDocument.this, Signature.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
