package idsoft.inspectiondepot.windmitinspection;



import java.io.File;

import org.w3c.dom.Text;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.sax.Element;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class Photos extends Activity {
	CommonFunctions cf;
	String homeid,InspectionType,status,elevname;
	int value,Count;
public Uri CapturedImageURI;
Spinner sp_elev;
TextView no_uploaded;
LinearLayout upload_img;
DatabaseFunctions db;
WebserviceFunctions wb;
Button save;
View v1;

String saved_val[];
private int maximumindb,maximumallowed=8;
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.photos);
		cf = new CommonFunctions(this);
		db= new DatabaseFunctions(this);
		wb= new WebserviceFunctions(this);


		Bundle bunQ1extras1 = getIntent().getExtras();
		if (bunQ1extras1 != null) {
			cf.Homeid = homeid = bunQ1extras1.getString("homeid");
			cf.InspectionType = InspectionType = bunQ1extras1
					.getString("InspectionType");
			cf.status = status = bunQ1extras1.getString("status");
			cf.value = value = bunQ1extras1.getInt("keyName");
			cf.Count = Count = bunQ1extras1.getInt("Count");

		}
		System.out.println("Phoh"+cf.status+"status="+status);
		setContentView(R.layout.photos); 		
		cf.getDeviceDimensions();				        
		
		
		db.CreateTable(13);
		db.CreateTable(25);
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(Photos.this, 3, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(Photos.this, 18,cf, 1));

		declaration();
		show_savedvalue();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		sp_elev=(Spinner) findViewById(R.id.PH_elev_sp);
		ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,cf.elev);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_elev.setAdapter(ad);
		sp_elev.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				show_savedvalue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		no_uploaded=(TextView) findViewById(R.id.photo_noofuploads);
		upload_img=(LinearLayout) findViewById(R.id.uploaded_img);
		
		save=(Button)findViewById(R.id.save);
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				db.CreateTable(12);
				Cursor c = db.SelectTablefunction(db.Photos, " WHERE IM_SRID='"+cf.Homeid+"'");
				if(c.getCount()>=1)
				{
					cf.ShowToast("Photos saved successfully");
					Intent int1 = new Intent(Photos.this, FeedbackDocument.class);
					int1.putExtra("homeid", cf.Homeid);
					int1.putExtra("status", cf.status);
					startActivity(int1);
					finish();
				}
				else
				{
					cf.ShowToast("Please add atleast one image");
				}
			}
		});
	}
	public void clicker(View v)
	{
		System.out.println("ssps"+sp_elev.getSelectedItemPosition());
		switch(v.getId())
		{
		case R.id.home:
			cf.gohome();
			break;
		case R.id.browsetxt:
			if(sp_elev.getSelectedItemPosition()!=0){
				Cursor c1 = db.SelectTablefunction(db.Photos, " WHERE IM_SRID='"+cf.Homeid+"' and IM_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
				maximumindb =c1.getCount();
				if(maximumindb<8)
				{
					Intent reptoit1 = new Intent(this,Select_phots.class);
					Bundle b=new Bundle();
					reptoit1.putExtra("Selectedvalue", saved_val);System.out.println("tets"+saved_val); /**Send the already selected image **/
					reptoit1.putExtra("Maximumcount", maximumindb);System.out.println("maximumindb"+maximumindb);/**Total count of image in the database **/
					reptoit1.putExtra("Total_Maximumcount", 8); /***Total count of image we need to accept**/
					startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
				}
				else
				{
					cf.ShowToast("You are allowed to upload only 8 Images for "+sp_elev.getSelectedItem().toString());	
				}
			}
			else
			{
				cf.ShowToast("Please select elevation type ");
			}
		break;
		case R.id.browsecamera:
			if(sp_elev.getSelectedItemPosition()!=0){
				Cursor c1 = db.SelectTablefunction(db.Photos, " WHERE IM_SRID='"+cf.Homeid+"' and IM_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
				maximumindb =c1.getCount();
				if(maximumindb<8)
				{
					String fileName = "temp.jpg";
					ContentValues values = new ContentValues();
					values.put(MediaStore.Images.Media.TITLE, fileName);
					CapturedImageURI = getContentResolver().insert(
							MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
					startActivityForResult(intent, 111);
				}
				else
				{
					cf.ShowToast("You are allowed to upload only 8 Images for "+sp_elev.getSelectedItem().toString());	
				}
			}
			else
			{
				cf.ShowToast("Please select elevation type ");
			}

		break;
		}
	
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK)
		{
			System.out.println("resultcodeOK");
			if (requestCode == 111) {System.out.println("requestCode"+requestCode);
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(CapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String capturedImageFilePath = cursor
						.getString(column_index_data);
				String selectedImagePath = capturedImageFilePath;System.out.println("display_taken_image");
				display_taken_image(selectedImagePath);
				
	
			}
			else if(requestCode == 121)
			{
				/** we get the result from the Idma page for this elevation **/
			  String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
				for(int i=0;i<value.length;i++ )
				{
					
		 					db.wind_db.execSQL("INSERT INTO "
	 							+ db.Photos
	 							+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
	 							+ " VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+sp_elev.getSelectedItemPosition()+ "','"+ db.encode(value[i]) + "','"
	 							+ db.encode(sp_elev.getSelectedItem().toString() + " "+(maximumindb+i+1))+ "','" + (maximumindb+1+i) + "')");
		 					
				}
				cf.ShowToast(sp_elev.getSelectedItem().toString()+ " Image saved successfully");
				show_savedvalue();
				/** we get the result from the Idma page for this elevation Ends **/	
			}
			else if(requestCode == 112)
			{
				/** we get the result from the Idma page for this elevation **/
			  String value=	data.getExtras().getString("Path"); 
			  String Caption=	data.getExtras().getString("Caption"); 
			  
			  getelevname(sp_elev.getSelectedItemPosition());
				  
			  
							db.wind_db.execSQL("INSERT INTO "
	 							+ db.Photos
	 							+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
	 							+ " VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+sp_elev.getSelectedItemPosition()+ "','"+ db.encode(value) + "','"
	 							+ db.encode(Caption)+ "','" + (maximumindb+1) + "')");
		 					
		
				cf.ShowToast(sp_elev.getSelectedItem().toString()+ " Image saved successfully");
				show_savedvalue();
			  
				/** we get the result from the Idma page for this elevation Ends **/	
			}
			else if(requestCode == 122)
			{
				
				/** we get the result from the Idma page for this elevation **/
			  String value=	data.getExtras().getString("Path"); 
			  String Caption=	data.getExtras().getString("Caption"); 
			  String  id=	data.getExtras().getString("id"); 
			  boolean dele=data.getExtras().getBoolean("Delete_data");
			  
			  if(!dele)
			  {
							db.wind_db.execSQL(" UPDATE  "
	 							+ db.Photos
	 							+ " SET IM_path='"+db.encode(value)+"',IM_Description='"+db.encode(Caption)+"' "
	 							+ "  WHERE WSID='"+id+"'");
		 					
		
				cf.ShowToast("Image saved successfully");
				
				/** we get the result from the Idma page for this elevation Ends **/	
			  }
			  else
			  {
				  db.wind_db.execSQL(" DELETE FROM "+db.Photos+" WHERE WSID='"+id+"'");
			  }
			  show_savedvalue();
			}
		}
	}
	private void display_taken_image(String selectedImagePath) {
		// TODO Auto-generated method stub
		System.out.println("Edit_photos");
		Intent in = new  Intent(this,Edit_photos.class);
		in.putExtra("Path",selectedImagePath);
		in.putExtra("Caption",sp_elev.getSelectedItem().toString()+ " "+(maximumindb+1));
		in.putExtra("saved_val",saved_val);
		in.putExtra("delete","false");
		startActivityForResult(in, 112);
	}
	private void show_savedvalue() {
		// TODO Auto-generated method stub
		
		Cursor c = db.SelectTablefunction(db.Photos, " WHERE IM_SRID='"+cf.Homeid+"' and IM_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
		maximumindb =c.getCount();
		no_uploaded.setText("No of image uploaded/Remaining image to be upload :"+maximumindb+"/"+(maximumallowed-maximumindb));
		if(c.getCount()>0)
		{
			c.moveToFirst();
			saved_val=new String[c.getCount()];
			upload_img.removeAllViews();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String path,img_order,description;
				path=db.decode(c.getString(c.getColumnIndex("IM_path")));
				saved_val[i]=db.decode(c.getString(c.getColumnIndex("IM_path")));
				description=db.decode(c.getString(c.getColumnIndex("IM_Description")));
				img_order=db.decode(c.getString(c.getColumnIndex("IM_ImageOrder")));
				
				LinearLayout li=new LinearLayout(this);
				li.setGravity(Gravity.CENTER_VERTICAL);
				TextView tv= new TextView(this,null,R.attr.textview_200);
				tv.setText(description);
				tv.setTextColor(Color.BLACK);
				li.addView(tv,200,LayoutParams.WRAP_CONTENT);
				ImageView im =new ImageView(this,null,R.attr.photos_imageview);
				im.setTag(c.getString(c.getColumnIndex("WSID")));
				File f =new File(path);
				if(f.exists())
				{
					Bitmap b =cf.ShrinkBitmap(path, 100, 100);
					if(b!=null)
					{
						im.setImageBitmap(b);
					}
				}
				im.setOnClickListener(new im_clicker(path,c.getString(c.getColumnIndex("WSID"))));
				li.addView(im,100,100);
				Spinner sp =new Spinner(this);
				getelevname(sp_elev.getSelectedItemPosition());
				
				Cursor cap=db.SelectTablefunction(db.PhotoCaption, " WHERE IM_C_InspectorId='"+db.Insp_id+"' AND IM_C_Elevation_Name='"+elevname+"' and IM_C_Elevation='"+img_order+"'");
				String caption[];
				System.out.println("cap="+cap.getCount());
				if(cap.getCount()>0)
				{
					cap.moveToFirst();
					caption=new String[cap.getCount()];
					cap.moveToFirst();
					for(int j=0;j<cap.getCount();j++,cap.moveToNext())
					{
						caption[j]=db.decode(cap.getString(cap.getColumnIndex("IM_C_caption")));
					}
					
				}
				else
				{
					caption=new String[2];
					caption[0]="--Select--";
					caption[1]="ADD PHOTO CAPTION";
				}
				ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item, caption);
				ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sp.setAdapter(ad);System.out.println("test");
				
				sp.setOnItemSelectedListener(new sp_onclicker(c.getString(c.getColumnIndex("WSID")),img_order,sp,tv));
				sp.setPadding(20, 0, 0, 0);
				li.addView(sp,220,LayoutParams.WRAP_CONTENT);
				upload_img.addView(li,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			}
		}
		else
		{
			upload_img.removeAllViews();
			TextView tv= new TextView(this);
			tv.setText("No records found");
			upload_img.addView(tv,150,LayoutParams.WRAP_CONTENT);
			saved_val=null;
		}
			
	}
	private void getelevname(int pos) {
		// TODO Auto-generated method stub
		if(pos==1)
		{
			elevname = "FE";
		}
		else if(pos==2)
		{
			elevname = "RE";
		}
		else if(pos==3)
		{
			elevname = "BE";
		}
		else if(pos==4)
		{
			elevname = "LE";
		}
		else if(pos==5)
		{
			elevname = "AE";
		}
		else if(pos==6)
		{
			elevname = "Additional Photo";
		}
	}
	class im_clicker implements OnClickListener
	{
		String path,id;
			public im_clicker(String path, String string) {
			// TODO Auto-generated constructor stub
				this.path=path;
				this.id=string;
		}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor c =db.SelectTablefunction(db.Photos," WHERE WSID='"+id+"'");
				c.moveToFirst();
				Intent in = new  Intent(Photos.this,Edit_photos.class);
				in.putExtra("Path",path);
				in.putExtra("Caption",db.decode(c.getString(c.getColumnIndex("IM_Description"))));
				in.putExtra("saved_val",saved_val);
				in.putExtra("id", id);
				startActivityForResult(in, 122);
				//finish();
 				
			}
		}
	class sp_onclicker implements OnItemSelectedListener
	{
		String id,img_order;
		Spinner sp;
		TextView tv;
			public sp_onclicker(String id,String  img_order,Spinner sp,TextView tv) {
			// TODO Auto-generated constructor stub
				System.out.println("dsd");
				this.img_order=img_order;System.out.println("11");
				this.id=id;System.out.println("22");
				this.sp=sp;System.out.println("233d");
				this.tv=tv;System.out.println("444");
		}

			

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(sp.getSelectedItem().toString().trim().equals("ADD PHOTO CAPTION"))
				{
					final Dialog dialog1 = new Dialog(Photos.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					Button close=((Button)dialog1.findViewById(R.id.helpclose));
					LinearLayout l1=((LinearLayout)dialog1.findViewById(R.id.camera));
					l1.setVisibility(v1.VISIBLE);
					((TextView)dialog1.findViewById(R.id.txthelp)).setText("Add caption");
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								getelevname(sp_elev.getSelectedItemPosition());
								db.wind_db.execSQL(" INSERT INTO "+db.PhotoCaption+" (IM_C_InspectorId,IM_C_caption,IM_C_Elevation,IM_C_Elevation_Name) VALUES " +
										"('"+db.Insp_id+"','"+db.encode(ed.getText().toString().trim())+"','"+img_order+"','"+elevname+"')");
								//add_caption(ed.getText().toString().trim(),img_order);
								
								cf.ShowToast("Photo Caption added successfully");
								show_savedvalue();
								dialog1.dismiss();
							}
							else
							{
								cf.ShowToast("Please enter caption ");
							}
						}

						
					});
					dialog1.show();				
					}
				else if(!sp.getSelectedItem().toString().trim().equals("--Select--"))
				{
					final Dialog dialog1 = new Dialog(Photos.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					final LinearLayout li1=((LinearLayout)dialog1.findViewById(R.id.maintable));
					final LinearLayout li2=((LinearLayout)dialog1.findViewById(R.id.edit_Caption_li));
					li1.setVisibility(View.GONE);
					li2.setVisibility(View.VISIBLE);
					//final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button select=((Button)dialog1.findViewById(R.id.select));
					Button edit=((Button)dialog1.findViewById(R.id.edit));
					Button delete1=((Button)dialog1.findViewById(R.id.del));
					Button close1=((Button)dialog1.findViewById(R.id.helpclose5));
					((TextView)dialog1.findViewById(R.id.txtquestio1)).setText(sp.getSelectedItem().toString().toString());
					
					close1.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							sp.setSelection(0);
							dialog1.dismiss();
						}
					});
					edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							li1.setVisibility(View.VISIBLE);
							li2.setVisibility(View.GONE);
						}
					});
					delete1.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder b =new AlertDialog.Builder(Photos.this);
							b.setTitle("Confirmation");
							b.setMessage("Do you want to delete the selected Caption?");
							b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									try
									{
										db.wind_db.execSQL(" DELETE  FROM "+db.PhotoCaption+" WHERE IM_C_InspectorId='"+db.Insp_id+"' " +
												"and IM_C_Elevation='"+img_order+"' and IM_C_caption='"+db.encode(sp.getSelectedItem().toString())+"'");
										show_savedvalue();
										dialog1.dismiss();
										
									}catch (Exception e) {
										// TODO: handle exception
										System.out.println("the exeption in delete"+e.getMessage());
									}
									
									cf.ShowToast("The selected caption has been deleted successfully");
									
								}
							});
							b.setNegativeButton("No", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
								}
							});   
							AlertDialog al=b.create();
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show(); 
						}
					});
					select.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
								db.wind_db.execSQL(" UPDATE "+db.Photos+" SET IM_Description='"+db.encode(sp.getSelectedItem().toString().trim())+"'" +
										" WHERE WSID='"+id+"'");
								///add_caption(ed.getText().toString().trim(),img_order);
								tv.setText(sp.getSelectedItem().toString().trim());
								sp.setSelection(0);
								dialog1.dismiss();
						}

						
					});
					dialog1.show();
					
					//***For edit layout **//
					final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					ed.setText(sp.getSelectedItem().toString().trim());
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					Button close=((Button)dialog1.findViewById(R.id.helpclose));
					LinearLayout l1=((LinearLayout)dialog1.findViewById(R.id.camera));
					l1.setVisibility(v1.VISIBLE);
					((TextView)dialog1.findViewById(R.id.txthelp)).setText("Edit caption");
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							sp.setSelection(0);
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								
								getelevname(sp_elev.getSelectedItemPosition());
								db.wind_db.execSQL(" UPDATE "+db.PhotoCaption+" SET IM_C_caption='"+db.encode(ed.getText().toString().trim())+"' " +
										"WHERE  IM_C_InspectorId='"+db.Insp_id+"' and IM_C_Elevation='"+img_order+"' and IM_C_Elevation_Name='"+elevname+"' AND IM_C_caption='"+db.encode(sp.getSelectedItem().toString().trim())+"'");
								///add_caption(ed.getText().toString().trim(),img_order);
								show_savedvalue();
								dialog1.dismiss();
							}
							else
							{
								cf.ShowToast("Please enter caption ");
							}
						}

						
					});
					dialog1.show();	
					
					//***For edit layout ends **//*
				
				}
			}
			private void add_caption(String trim, String img_order) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(Photos.this,
					Signature.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}