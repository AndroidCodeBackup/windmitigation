package idsoft.inspectiondepot.windmitinspection;

import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class QuesOpenProt extends Activity {
	String chkstatus[] = { "true", "true", "true", "true", "true" };
	String commentsfill, descrip, comm2, Windowentrydoorsval, Garagedoorsval,
			Skylightsval, Glassblocksval, Entrydoorsval, NGOGarageDoorsval;
	String InspectionType, status, homeId, rdiochk, suboption = "0", updatecnt,
			identity, comm, roofopenprotcomment, levelchart = "";
	String inspectortypeid, helpcontent,tmp="";
    boolean load_comment = true;
	TextView  txtheadingopenprotect, option1, option2,helptxt,open_TV_type;
	Intent iInspectionList;
	int value, Count;
	ListView list;
	Button saveclose;
	EditText comments;
	String buildcommadmin1, buildcommadmin2, buildcommadmin3;
	String roofopenprottypeprev, roofopenprotvalueprev,
			roofopenprotsubvalueprev, roofopenprotlevelchartprev,
			roofwondentrydoorvalprev, roofgaragedoorsvalueprev,
			roofgoskylightsvalueprev, roofNGOglassblocksvalueprev,
			roofNGOentrydoorsvalueprev, roofNGOgaragedoorvalueprev;

	private static final int visibility = 0;
	int inc;
	TextView prevmitidata;
	public String count_value;
	String conchkbox, commdescrip = "", miamelink, fbclink;
	String[] arrcomment1;
	int viewimage = 1, optionid;
	AlertDialog alertDialog;

	TextWatcher watcher;
	View v1;

	LinearLayout  lnrlytsuboptionA, lnrlytsuboptionB,openprot_parrant,openprot_type1,
			lnrlytsuboptionC, lnrlytsuboptionN;
	CheckBox[] cb;
	CheckBox temp_st;
	RadioButton rdioquestion1, rdioquestion1answer1, rdioquestion1answer2,
			rdioquestion1answer3;
	RadioButton rdioquestion2, rdioquestion2answer1, rdioquestion2answer2,
			rdioquestion2answer3;
	RadioButton rdioquestion3, rdioquestion3answer1, rdioquestion3answer2,
			rdioquestion3answer3;
	RadioButton rdioquestion4, rdioquestion4answer1, rdioquestion4answer2,
			rdioquestion4answer3, rdioquestion5;
	RadioButton rdiocheckNA1, rdiocheckNA2, rdiocheckNA3, rdiocheckNA4,
			rdiocheckNA5, rdiocheckNA6;
	RadioButton rdiocheckA1, rdiocheckA2, rdiocheckA3, rdiocheckA4,
			rdiocheckA5, rdiocheckA6;
	RadioButton rdiocheckB1, rdiocheckB2, rdiocheckB3, rdiocheckB4,
			rdiocheckB5, rdiocheckB6;
	RadioButton rdiocheckC1, rdiocheckC2, rdiocheckC3, rdiocheckC4,
			rdiocheckC5, rdiocheckC6;
	RadioButton rdiocheckD1, rdiocheckD2, rdiocheckD3, rdiocheckD4,
			rdiocheckD5, rdiocheckD6;
	RadioButton rdiocheckN11, rdiocheckN12, rdiocheckN13, rdiocheckN14,
			rdiocheckN15, rdiocheckN16;
	RadioButton rdiocheckN21, rdiocheckN22, rdiocheckN23, rdiocheckN24,
			rdiocheckN25, rdiocheckN26;
	RadioButton rdiocheckX1, rdiocheckX2, rdiocheckX3, rdiocheckX4,
			rdiocheckX5, rdiocheckX6;
	Map<String, String[]> autoselection = new LinkedHashMap<String, String[]>();
	String auto_status[] = { "flase", "flase", "flase", "flase", "flase",
			"flase", "flase", "false" };
	android.text.format.DateFormat df;
	CharSequence cd, md;
	int commentsch;
	String b1_q7[] = {"FBC Plywood","All Glazed Openings 4 � 8 lb Large Missile lb.","All Glazed Openings 4 - 8 lb Large Missile","Opening Protection Not Verified", "None / Some Not Protected" };
	TextView miameweblinkopen, fbcweblinkopen;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);

		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);

		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}
		setContentView(R.layout.openprotect);
		db.getInspectorId();
		db.getPHinformation(cf.Homeid);//cf.get_density_frm_db();
        db.changeimage(cf.Homeid);
		cf.getDeviceDimensions();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(QuesOpenProt.this, 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(QuesOpenProt.this, 27,
				cf, 1));
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdioquestion1answer1.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A1 for non glazed openings, meeting the large missile impact 9lb rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs and documentation.";
				} else if (rdioquestion1answer2.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				} else if (rdioquestion1answer3.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. One or more non glazed openings did not meet level A, large 9 lb missile impact rating as identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				} else if (rdioquestion2answer1.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B1 for non glazed openings, meeting the large 4-8lb  missile impact rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs.";
				} else if (rdioquestion2answer2.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				} else if (rdioquestion2answer3.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection  B for glazed openings and B3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. One or more non glazed opening did not meet level B large missile, 4-8lb, impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
				}

				else if (rdioquestion3answer1.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels. Refer to attached Photographs.";
				} else if (rdioquestion3answer2.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				} else if (rdioquestion3answer3.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection  C for glazed openings and C3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as protected with wood structural panels that are compliant to the Florida Building Code. One or more non glazed opening did not meet level A,B or C impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
				}

				else if (rdioquestion4answer1.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that the opening protection was present to all openings that could not be verified as meeting the impact requirements of selection A or B. Refer to attached Photographs.";
				} else if (rdioquestion4answer2.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that we were unable to verify the opening protection as meeting large missile impact rating as outline in selection A and B, apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				} else if (rdioquestion4answer3.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection  N for glazed openings and N3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that some openings were not protected. Refer to attached photographs.";
				} else if (rdioquestion5.isChecked()) {
					cf.alertcontent = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
				} else {
					cf.alertcontent = "To see help comments, please select Opening Protection options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
				
			}
		});
		
		txtheadingopenprotect = (TextView) findViewById(R.id.txtheadingopenprotect);
		txtheadingopenprotect
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>What is the "
								+ "<u>"
								+ "weakest"
								+ "</u>"
								+ " form of wind borne debris protection installed on the structure? First, use the table to determine the weakest form of protection for each category of opening. Second, (a) check one answer below (A, B, C, N, or X) based upon the lowest protection level for ALL Glazed openings and (b) check the protection level for all Non-Glazed openings (.1, .2, or .3) as applicable. "));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);

		miameweblinkopen = (TextView) this
				.findViewById(R.id.miamedadeprodapprlink);
		fbcweblinkopen = (TextView) this.findViewById(R.id.fbcprodapprlink);

		miamelink = "<a href=''>" + "Miame-Dade Product Approval" + "</a>";
		miameweblinkopen.setText(Html.fromHtml(miamelink));

		fbclink = "<a href=''>" + "FBC Product Approval" + "</a>";
		fbcweblinkopen.setText(Html.fromHtml(fbclink));

		miameweblinkopen.setOnClickListener((OnClickListener) new clicker1());
		fbcweblinkopen.setOnClickListener((OnClickListener) new clicker1());
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			inc = 1;
		} else {
			inc = 2;
		}

		
		lnrlytsuboptionA = (LinearLayout) findViewById(R.id.lnrlayoutsuboptionA);
		lnrlytsuboptionB = (LinearLayout) findViewById(R.id.lnrlayoutsuboptionB);
		lnrlytsuboptionC = (LinearLayout) findViewById(R.id.lnrlayoutsuboptionC);
		lnrlytsuboptionN = (LinearLayout) findViewById(R.id.lnrlayoutsuboptionN);

		
		db.getQuesOriginal(cf.Homeid);
		if(!db.oporiddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.oporiddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}
		
		
		option1 = (TextView) findViewById(R.id.optiontxt1);
		option2 = (TextView) findViewById(R.id.optiontxt2);
		option1.setText(Html
				.fromHtml("1. Miami-Dade County PA 201, 202, and 203 "
						+ "<br/>"
						+ "2. Florida Building Code Testing Application Standard (TAS) 201, 202,and 203 "
						+ "<br/>"
						+ "3. American Society for Testing and Materials (ASTM) E 1886 andASTM E 1996"
						+ "<br/>"
						+ "4. Southern Standards Technical Document (SSTD) 12"
						+ "<br/>"
						+ "5. For Skylights Only: ASTM E 1886 andASTM E 1996 "
						+ "<br/>" + "6. For Garage Doors Only: ANSI/DASMA 115 "));
		option2.setText(Html
				.fromHtml("1. ASTM E 1886 andASTM E 1996 (Large Missile � 4.5 lb.) "
						+ "<br/>"
						+ "2. SSTD 12 (Large Missile � 4 lb. to 8 lb.) "
						+ "<br/>"
						+ "3. For Skylights Only: ASTM E 1886 and ASTM E 1996 (Large Missile - 2 to 4.5 lb.) "
						+ "<br/>"));

		this.rdiocheckNA1 = (RadioButton) this.findViewById(R.id.NAans1);
		this.rdiocheckNA1.setOnClickListener(OnClickListener);
		this.rdiocheckNA2 = (RadioButton) this.findViewById(R.id.NAans2);
		this.rdiocheckNA2.setOnClickListener(OnClickListener);
		this.rdiocheckNA3 = (RadioButton) this.findViewById(R.id.NAans3);
		this.rdiocheckNA3.setOnClickListener(OnClickListener);
		this.rdiocheckNA4 = (RadioButton) this.findViewById(R.id.NAans4);
		this.rdiocheckNA4.setOnClickListener(OnClickListener);
		this.rdiocheckNA5 = (RadioButton) this.findViewById(R.id.NAans5);
		this.rdiocheckNA5.setOnClickListener(OnClickListener);
		this.rdiocheckNA6 = (RadioButton) this.findViewById(R.id.NAans6);
		this.rdiocheckNA6.setOnClickListener(OnClickListener);
		this.rdiocheckA1 = (RadioButton) this.findViewById(R.id.rdioAans1);
		this.rdiocheckA1.setOnClickListener(OnClickListener);
		this.rdiocheckA2 = (RadioButton) this.findViewById(R.id.rdioAans2);
		this.rdiocheckA2.setOnClickListener(OnClickListener);
		this.rdiocheckA3 = (RadioButton) this.findViewById(R.id.rdioAans3);
		this.rdiocheckA3.setOnClickListener(OnClickListener);
		this.rdiocheckA4 = (RadioButton) this.findViewById(R.id.rdioAans4);
		this.rdiocheckA4.setOnClickListener(OnClickListener);
		this.rdiocheckA5 = (RadioButton) this.findViewById(R.id.rdioAans5);
		this.rdiocheckA5.setOnClickListener(OnClickListener);
		this.rdiocheckA6 = (RadioButton) this.findViewById(R.id.rdioAans6);
		this.rdiocheckA6.setOnClickListener(OnClickListener);
		this.rdiocheckB1 = (RadioButton) this.findViewById(R.id.rdioBans1);
		this.rdiocheckB1.setOnClickListener(OnClickListener);
		this.rdiocheckB2 = (RadioButton) this.findViewById(R.id.rdioBans2);
		this.rdiocheckB2.setOnClickListener(OnClickListener);
		this.rdiocheckB3 = (RadioButton) this.findViewById(R.id.rdioBans3);
		this.rdiocheckB3.setOnClickListener(OnClickListener);
		this.rdiocheckB4 = (RadioButton) this.findViewById(R.id.rdioBans4);
		this.rdiocheckB4.setOnClickListener(OnClickListener);
		this.rdiocheckB5 = (RadioButton) this.findViewById(R.id.rdioBans5);
		this.rdiocheckB5.setOnClickListener(OnClickListener);
		this.rdiocheckB6 = (RadioButton) this.findViewById(R.id.rdioBans6);
		this.rdiocheckB6.setOnClickListener(OnClickListener);

		this.rdiocheckC1 = (RadioButton) this.findViewById(R.id.rdioCans1);
		this.rdiocheckC1.setOnClickListener(OnClickListener);
		this.rdiocheckC2 = (RadioButton) this.findViewById(R.id.rdioCans2);
		this.rdiocheckC2.setOnClickListener(OnClickListener);
		this.rdiocheckC3 = (RadioButton) this.findViewById(R.id.rdioCans3);
		this.rdiocheckC3.setOnClickListener(OnClickListener);
		this.rdiocheckC4 = (RadioButton) this.findViewById(R.id.rdioCans4);
		this.rdiocheckC4.setOnClickListener(OnClickListener);
		this.rdiocheckC5 = (RadioButton) this.findViewById(R.id.rdioCans5);
		this.rdiocheckC5.setOnClickListener(OnClickListener);
		this.rdiocheckC6 = (RadioButton) this.findViewById(R.id.rdioCans6);
		this.rdiocheckC6.setOnClickListener(OnClickListener);
		this.rdiocheckD1 = (RadioButton) this.findViewById(R.id.rdioDans1);
		this.rdiocheckD1.setOnClickListener(OnClickListener);
		this.rdiocheckD2 = (RadioButton) this.findViewById(R.id.rdioDans2);
		this.rdiocheckD2.setOnClickListener(OnClickListener);
		this.rdiocheckD3 = (RadioButton) this.findViewById(R.id.rdioDans3);
		this.rdiocheckD3.setOnClickListener(OnClickListener);
		this.rdiocheckD4 = (RadioButton) this.findViewById(R.id.rdioDans4);
		this.rdiocheckD4.setOnClickListener(OnClickListener);
		this.rdiocheckD5 = (RadioButton) this.findViewById(R.id.rdioDans5);
		this.rdiocheckD5.setOnClickListener(OnClickListener);
		this.rdiocheckD6 = (RadioButton) this.findViewById(R.id.rdioDans6);
		this.rdiocheckD6.setOnClickListener(OnClickListener);

		this.rdiocheckN11 = (RadioButton) this.findViewById(R.id.rdioN1ans1);
		rdiocheckN11.setOnClickListener(OnClickListener);
		this.rdiocheckN12 = (RadioButton) this.findViewById(R.id.rdioN1ans2);
		rdiocheckN12.setOnClickListener(OnClickListener);
		this.rdiocheckN13 = (RadioButton) this.findViewById(R.id.rdioN1ans3);
		rdiocheckN13.setOnClickListener(OnClickListener);
		this.rdiocheckN14 = (RadioButton) this.findViewById(R.id.rdioN1ans4);
		rdiocheckN14.setOnClickListener(OnClickListener);
		this.rdiocheckN15 = (RadioButton) this.findViewById(R.id.rdioN1ans5);
		rdiocheckN15.setOnClickListener(OnClickListener);
		this.rdiocheckN16 = (RadioButton) this.findViewById(R.id.rdioN1ans6);
		rdiocheckN16.setOnClickListener(OnClickListener);

		this.rdiocheckN21 = (RadioButton) this.findViewById(R.id.rdioN2ans1);
		rdiocheckN21.setOnClickListener(OnClickListener);
		this.rdiocheckN22 = (RadioButton) this.findViewById(R.id.rdioN2ans2);
		rdiocheckN22.setOnClickListener(OnClickListener);
		this.rdiocheckN23 = (RadioButton) this.findViewById(R.id.rdioN2ans3);
		rdiocheckN23.setOnClickListener(OnClickListener);
		this.rdiocheckN24 = (RadioButton) this.findViewById(R.id.rdioN2ans4);
		rdiocheckN24.setOnClickListener(OnClickListener);
		this.rdiocheckN25 = (RadioButton) this.findViewById(R.id.rdioN2ans5);
		rdiocheckN25.setOnClickListener(OnClickListener);
		this.rdiocheckN26 = (RadioButton) this.findViewById(R.id.rdioN2ans6);
		rdiocheckN26.setOnClickListener(OnClickListener);

		this.rdiocheckX1 = (RadioButton) this.findViewById(R.id.rdioXans1);
		rdiocheckX1.setOnClickListener(OnClickListener);
		this.rdiocheckX2 = (RadioButton) this.findViewById(R.id.rdioXans2);
		rdiocheckX2.setOnClickListener(OnClickListener);
		this.rdiocheckX3 = (RadioButton) this.findViewById(R.id.rdioXans3);
		rdiocheckX3.setOnClickListener(OnClickListener);
		this.rdiocheckX4 = (RadioButton) this.findViewById(R.id.rdioXans4);
		rdiocheckX4.setOnClickListener(OnClickListener);
		this.rdiocheckX5 = (RadioButton) this.findViewById(R.id.rdioXans5);
		rdiocheckX5.setOnClickListener(OnClickListener);
		this.rdiocheckX6 = (RadioButton) this.findViewById(R.id.rdioXans6);
		rdiocheckX6.setOnClickListener(OnClickListener);

		this.rdioquestion1 = (RadioButton) this.findViewById(R.id.rdioques1);
		this.rdioquestion1.setOnClickListener(OnClickListener);
		this.rdioquestion1answer1 = (RadioButton) this
				.findViewById(R.id.rdioques1ans1);
		this.rdioquestion1answer1.setOnClickListener(OnClickListener);
		this.rdioquestion1answer2 = (RadioButton) this
				.findViewById(R.id.rdioques1ans2);
		this.rdioquestion1answer2.setOnClickListener(OnClickListener);
		this.rdioquestion1answer3 = (RadioButton) this
				.findViewById(R.id.rdioques1ans3);
		this.rdioquestion1answer3.setOnClickListener(OnClickListener);

		this.rdioquestion2 = (RadioButton) this.findViewById(R.id.rdioques2);
		this.rdioquestion2.setOnClickListener(OnClickListener);
		this.rdioquestion2answer1 = (RadioButton) this
				.findViewById(R.id.rdioques2ans1);
		this.rdioquestion2answer1.setOnClickListener(OnClickListener);
		this.rdioquestion2answer2 = (RadioButton) this
				.findViewById(R.id.rdioques2ans2);
		this.rdioquestion2answer2.setOnClickListener(OnClickListener);
		this.rdioquestion2answer3 = (RadioButton) this
				.findViewById(R.id.rdioques2ans3);
		this.rdioquestion2answer3.setOnClickListener(OnClickListener);

		this.rdioquestion3 = (RadioButton) this.findViewById(R.id.rdioques3);
		this.rdioquestion3.setOnClickListener(OnClickListener);

		this.rdioquestion3answer1 = (RadioButton) this
				.findViewById(R.id.rdioques3ans1);
		this.rdioquestion3answer1.setOnClickListener(OnClickListener);
		this.rdioquestion3answer2 = (RadioButton) this
				.findViewById(R.id.rdioques3ans2);
		this.rdioquestion3answer2.setOnClickListener(OnClickListener);
		this.rdioquestion3answer3 = (RadioButton) this
				.findViewById(R.id.rdioques3ans3);
		this.rdioquestion3answer3.setOnClickListener(OnClickListener);

		this.rdioquestion4 = (RadioButton) this.findViewById(R.id.rdioques4);
		this.rdioquestion4.setOnClickListener(OnClickListener);

		this.rdioquestion4answer1 = (RadioButton) this
				.findViewById(R.id.rdioques4ans1);
		this.rdioquestion4answer1.setOnClickListener(OnClickListener);
		this.rdioquestion4answer2 = (RadioButton) this
				.findViewById(R.id.rdioques4ans2);
		this.rdioquestion4answer2.setOnClickListener(OnClickListener);
		this.rdioquestion4answer3 = (RadioButton) this
				.findViewById(R.id.rdioques4ans3);
		this.rdioquestion4answer3.setOnClickListener(OnClickListener);

		this.rdioquestion5 = (RadioButton) this.findViewById(R.id.rdioques5);
		this.rdioquestion5.setOnClickListener(OnClickListener);

		
		comments = (EditText) this.findViewById(R.id.txtcomments);
		open_TV_type = (TextView) findViewById(R.id.SH_TV_ED);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,open_TV_type));
		
		autoselection.put("answer1", auto_status);
		autoselection.put("answer2", auto_status);
		autoselection.put("answer3", auto_status);
		autoselection.put("answer4", auto_status);
		autoselection.put("answer5", auto_status);
		autoselection.put("answer6", auto_status);
		// initialize the map Ends here
		saveclose = (Button) findViewById(R.id.savenext);
		getcountname();
		
		setdata();

		
		saveclose.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				comm = comments.getText().toString();

				if (rdiocheckNA1.isChecked() == true
						|| rdiocheckA1.isChecked() == true
						|| rdiocheckB1.isChecked() == true
						|| rdiocheckC1.isChecked() == true
						|| rdiocheckD1.isChecked() == true
						|| rdiocheckN11.isChecked() == true
						|| rdiocheckN21.isChecked() == true
						|| rdiocheckX1.isChecked() == true) {
					if (rdiocheckNA2.isChecked() == true
							|| rdiocheckA2.isChecked() == true
							|| rdiocheckB2.isChecked() == true
							|| rdiocheckC2.isChecked() == true
							|| rdiocheckD2.isChecked() == true
							|| rdiocheckN12.isChecked() == true
							|| rdiocheckN22.isChecked() == true
							|| rdiocheckX2.isChecked() == true) {
						if (rdiocheckNA3.isChecked() == true
								|| rdiocheckA3.isChecked() == true
								|| rdiocheckB3.isChecked() == true
								|| rdiocheckC3.isChecked() == true
								|| rdiocheckD3.isChecked() == true
								|| rdiocheckN13.isChecked() == true
								|| rdiocheckN23.isChecked() == true
								|| rdiocheckX3.isChecked() == true) {
							if (rdiocheckNA4.isChecked() == true
									|| rdiocheckA4.isChecked() == true
									|| rdiocheckB4.isChecked() == true
									|| rdiocheckC4.isChecked() == true
									|| rdiocheckD4.isChecked() == true
									|| rdiocheckN14.isChecked() == true
									|| rdiocheckN24.isChecked() == true
									|| rdiocheckX4.isChecked() == true) {
								if (rdiocheckNA5.isChecked() == true
										|| rdiocheckA5.isChecked() == true
										|| rdiocheckB5.isChecked() == true
										|| rdiocheckC5.isChecked() == true
										|| rdiocheckD5.isChecked() == true
										|| rdiocheckN15.isChecked() == true
										|| rdiocheckN25.isChecked() == true
										|| rdiocheckX5.isChecked() == true) {
									if (rdiocheckNA6.isChecked() == true
											|| rdiocheckA6.isChecked() == true
											|| rdiocheckB6.isChecked() == true
											|| rdiocheckC6.isChecked() == true
											|| rdiocheckD6.isChecked() == true
											|| rdiocheckN16.isChecked() == true
											|| rdiocheckN26.isChecked() == true
											|| rdiocheckX6.isChecked() == true) {
										if (rdioquestion1.isChecked() == true
												|| rdioquestion2.isChecked() == true
												|| rdioquestion3.isChecked() == true
												|| rdioquestion4.isChecked() == true
												|| rdioquestion5.isChecked() == true) {
											if (rdioquestion1.isChecked() == true) {
												if (rdioquestion1answer1
														.isChecked() == false
														&& rdioquestion1answer2
																.isChecked() == false
														&& rdioquestion1answer3
																.isChecked() == false) {
													cf.ShowToast("Please select option for All Glazed Openings - Large Missile 9 lb");
													chkstatus[0] = "false";
												} else {
													chkstatus[0] = "true";
												}
											} else if (rdioquestion2
													.isChecked() == true) {
												if (rdioquestion2answer1
														.isChecked() == false
														&& rdioquestion2answer2
																.isChecked() == false
														&& rdioquestion2answer3
																.isChecked() == false) {
													cf.ShowToast("Please select option for All Glazed Openings 4 - 8 lb Large Missile");
													chkstatus[1] = "false";
												} else {
													chkstatus[1] = "true";
												}
											} else if (rdioquestion3
													.isChecked() == true) {
												if (rdioquestion3answer1
														.isChecked() == false
														&& rdioquestion3answer2
																.isChecked() == false
														&& rdioquestion3answer3
																.isChecked() == false) {
													cf.ShowToast("Please select option for FBC Plywood");
													chkstatus[2] = "false";
												} else {
													chkstatus[2] = "true";
												}
											} else if (rdioquestion4
													.isChecked() == true) {
												if (rdioquestion4answer1
														.isChecked() == false
														&& rdioquestion4answer2
																.isChecked() == false
														&& rdioquestion4answer3
																.isChecked() == false) {
													cf.ShowToast("Please select option for Opening Protection Not Verified");
													chkstatus[3] = "false";
												} else {
													chkstatus[3] = "true";
												}
											}

											if (comm.trim().equals("")) {
												cf.ShowToast("Please enter comments for Opening Protection");

												chkstatus[4] = "false";
											} else {
												chkstatus[4] = "true";
											}
											if (chkstatus[0] == "true"
													&& chkstatus[1] == "true"
													&& chkstatus[2] == "true"
													&& chkstatus[3] == "true"
													&& chkstatus[4] == "true") {
												try {
													Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "+ db.Questions+ " WHERE SRID='"+ cf.Homeid+ "'",null);
													
													if (c2.getCount() == 0) {
														db.wind_db.execSQL("INSERT INTO "
																+ db.Questions
																+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
																+ " VALUES ('"
																+ cf.Homeid
																+ "','','','"
																+ 0
																+ "','','"
																+ 0
																+ "','','','','','','"
																+ 0
																+ "','','"
																+ 0
																+ "','','','','','','"
																+ 0
																+ "','','','','"
																+ 0
																+ "','','"
																+ rdiochk
																+ "','"
																+ suboption
																+ "','','"
																+ Windowentrydoorsval
																+ "','"
																+ Garagedoorsval
																+ "','"
																+ Skylightsval
																+ "','"
																+ Glassblocksval
																+ "','"
																+ Entrydoorsval
																+ "','"
																+ NGOGarageDoorsval
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','"
																+ 0
																+ "','','"
																+ 0
																+ "','"
																+ 0
																+ "','','"
																+ 0
																+ "','','','"
																+ cd
																+ "','"
																+ 0 + "')");

													} else {

														if (suboption.equals(null) || suboption.equals(""))
														{
															suboption = "0";
														}

														db.wind_db.execSQL("UPDATE "
																+ db.Questions
																+ " SET OpenProtectType ='',OpenProtectValue='"
																+ rdiochk
																+ "',OpenProtectSubValue='"
																+ suboption
																+ "',GOWindEntryDoorsValue='"
																+ Windowentrydoorsval
																+ "',GOGarageDoorsValue='"
																+ Garagedoorsval
																+ "',GOSkylightsValue='"
																+ Skylightsval
																+ "',NGOGlassBlockValue='"
																+ Glassblocksval
																+ "',NGOEntryDoorsValue='"
																+ Entrydoorsval
																+ "',NGOGarageDoorsValue='"
																+ NGOGarageDoorsval
																+ "',OpenProtectLevelChart=''"
																+ " WHERE SRID ='"
																+ cf.Homeid
																		.toString()
																+ "'");
													}
													
												

													Cursor c4 = db.wind_db.rawQuery("SELECT * FROM "+ db.QuestionsComments+ " WHERE SRID='"+ cf.Homeid+ "'",null);
													if (c4.getCount() == 0) {
														db.wind_db.execSQL("INSERT INTO "
																+ db.QuestionsComments
																+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
																+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','','','','"+ db.encode(comments.getText().toString())+ "','','','"+ cf.datewithtime + "')");
												
													} else {

														db.wind_db.execSQL("UPDATE "
																+ db.QuestionsComments
																+ " SET OpeningProtectionComment='"
																+ db.encode(comments
																		.getText()
																		.toString())
																+ "',CreatedOn ='"
																+ md
																+ "'"
																+ " WHERE SRID ='"
																+ cf.Homeid
																		.toString()
																+ "'");
													}
													db.getInspectorId();

													Cursor c3 =db.wind_db.rawQuery("SELECT * FROM "+ db.SubmitCheckTable+ " WHERE Srid='"+ cf.Homeid+ "'",null);
													if (c3.getCount() == 0) {
														db.wind_db.execSQL("INSERT INTO "
																+ db.SubmitCheckTable
																+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
																+ "VALUES ('"
																+ db.Insp_id
																+ "','"
																+ cf.Homeid
																+ "',0,0,0,0,0,0,0,1,0,0,0,0,0,0,0)");
													} else {
														db.wind_db.execSQL("UPDATE "
																+ db.SubmitCheckTable
																+ " SET fld_open='1' WHERE Srid ='"
																+ cf.Homeid
																+ "' and InspectorId='"
																+ db.Insp_id
																+ "'");
													}
													updatecnt = "1";
													
												} 
												catch (Exception e) {
													
													updatecnt = "0";
													System.out
															.println("inside catch"+e.getMessage());
													cf.ShowToast("There is a problem in saving your data due to invlaid character");
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OpenProtect.this +" problem in inserting or updating openprotection comments datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);


												}
												
												
												// opentick.setVisibility(visibility);
											}

										} else {
											cf.ShowToast("Please select Opening Protection");

										}

									} else {
										cf.ShowToast("Please select Garage Doors for Non - Glazed Openings");

									}
								} else {
									cf.ShowToast("Please select Entry Doors for Non - Glazed Openings");

								}
							} else {
								cf.ShowToast("Please select Glass Block for Non - Glazed Openings");

							}
						} else {
							cf.ShowToast("Please select Skylights for Glazed Openings");

						}
					} else {
						cf.ShowToast("Please select Garage Doors for Glazed Openings");

					}
				} else {
					cf.ShowToast("Please select Windows or Entry Doors for Glazed Openings");

				}
				System.out.println("updatecnt"+updatecnt);

				if (updatecnt == "1") {
					cf.ShowToast("Opening Protection saved successfully");
					iInspectionList = new Intent(QuesOpenProt.this, QuesWallCons.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList.putExtra("status", cf.status);
					startActivity(iInspectionList);
					finish();
				
				}
			}
		});

	}
	private void setdata() {
		// TODO Auto-generated method stub
		try {
			Cursor cur = db.wind_db.rawQuery("select * from "
					+ db.Questions + " where SRID='" + cf.Homeid
					+ "'", null);
			
			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {
				roofopenprotvalueprev = cur.getString(cur
						.getColumnIndex("OpenProtectValue"));
				roofopenprotsubvalueprev = cur.getString(cur
						.getColumnIndex("OpenProtectSubValue"));
				roofwondentrydoorvalprev = cur.getString(cur
						.getColumnIndex("GOWindEntryDoorsValue"));
				roofgaragedoorsvalueprev = cur.getString(cur
						.getColumnIndex("GOGarageDoorsValue"));
				roofgoskylightsvalueprev = cur.getString(cur
						.getColumnIndex("GOSkylightsValue"));
				roofNGOglassblocksvalueprev = cur.getString(cur
						.getColumnIndex("NGOGlassBlockValue"));
				roofNGOentrydoorsvalueprev = cur.getString(cur
						.getColumnIndex("NGOEntryDoorsValue"));
				roofNGOgaragedoorvalueprev = cur.getString(cur
						.getColumnIndex("NGOGarageDoorsValue"));

				if (roofwondentrydoorvalprev.equals("NA")) {
					open_product_autoselct("answer1", 0);
					rdiocheckNA1.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("A")) {
					open_product_autoselct("answer1", 1);
					rdiocheckA1.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("B")) {
					open_product_autoselct("answer1", 2);
					rdiocheckB1.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("C")) {
					open_product_autoselct("answer1", 3);
					rdiocheckC1.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("D")) {
					open_product_autoselct("answer1", 4);
					rdiocheckD1.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("N")) {
					open_product_autoselct("answer1", 5);
					rdiocheckN11.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("NSUB1")) {
					open_product_autoselct("answer1", 6);
					rdiocheckN21.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else if (roofwondentrydoorvalprev.equals("X")) {
					open_product_autoselct("answer1", 7);
					rdiocheckX1.setChecked(true);
					Windowentrydoorsval = roofwondentrydoorvalprev;
				} else {
				}

				if (roofgaragedoorsvalueprev.equals("NA")) {
					open_product_autoselct("answer2", 0);
					rdiocheckNA2.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else if (roofgaragedoorsvalueprev.equals("A")) {
					open_product_autoselct("answer2", 1);
					rdiocheckA2.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else if (roofgaragedoorsvalueprev.equals("B")) {
					open_product_autoselct("answer2", 2);
					rdiocheckB2.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else if (roofgaragedoorsvalueprev.equals("C")) {
					open_product_autoselct("answer2", 3);
					rdiocheckC2.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else if (roofgaragedoorsvalueprev.equals("D")) {
					open_product_autoselct("answer2", 4);
					rdiocheckD2.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else if (roofgaragedoorsvalueprev.equals("N")) {
					open_product_autoselct("answer2", 5);
					rdiocheckN12.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else if (roofgaragedoorsvalueprev.equals("NSUB2")) {
					open_product_autoselct("answer2", 6);
					rdiocheckN22.setChecked(true);
				} else if (roofgaragedoorsvalueprev.equals("X")) {
					open_product_autoselct("answer2", 7);
					rdiocheckX2.setChecked(true);
					Garagedoorsval = roofgaragedoorsvalueprev;
				} else {
				}

				if (roofgoskylightsvalueprev.equals("NA")) {
					open_product_autoselct("answer3", 0);
					rdiocheckNA3.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("A")) {
					open_product_autoselct("answer3", 1);
					rdiocheckA3.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("B")) {
					open_product_autoselct("answer3", 2);
					rdiocheckB3.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("C")) {
					open_product_autoselct("answer3", 3);
					rdiocheckC3.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("D")) {
					open_product_autoselct("answer3", 4);
					rdiocheckD3.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("N")) {
					open_product_autoselct("answer3", 5);
					rdiocheckN13.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("NSUB3")) {
					open_product_autoselct("answer3", 6);
					rdiocheckN23.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else if (roofgoskylightsvalueprev.equals("X")) {
					open_product_autoselct("answer3", 7);
					rdiocheckX3.setChecked(true);
					Skylightsval = roofgoskylightsvalueprev;
				} else {
				}

				if (roofNGOglassblocksvalueprev.equals("NA")) {
					open_product_autoselct("answer4", 0);
					rdiocheckNA4.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("A")) {
					open_product_autoselct("answer4", 1);
					rdiocheckA4.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("B")) {
					open_product_autoselct("answer4", 2);
					rdiocheckB4.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("C")) {
					open_product_autoselct("answer4", 3);
					rdiocheckC4.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("D")) {
					open_product_autoselct("answer4", 4);
					rdiocheckD4.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("N")) {
					open_product_autoselct("answer4", 5);
					rdiocheckN14.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("NSUB4")) {
					open_product_autoselct("answer4", 6);
					rdiocheckN24.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else if (roofNGOglassblocksvalueprev.equals("X")) {
					open_product_autoselct("answer4", 7);
					rdiocheckX4.setChecked(true);
					Glassblocksval = roofNGOglassblocksvalueprev;
				} else {
				}

				if (roofNGOentrydoorsvalueprev.equals("NA")) {
					open_product_autoselct("answer5", 0);
					rdiocheckNA5.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("A")) {
					open_product_autoselct("answer5", 1);
					rdiocheckA5.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("B")) {
					open_product_autoselct("answer5", 2);
					rdiocheckB5.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("C")) {
					open_product_autoselct("answer5", 3);
					rdiocheckC5.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("D")) {
					open_product_autoselct("answer5", 4);
					rdiocheckD5.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("N")) {
					open_product_autoselct("answer5", 5);
					rdiocheckN15.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("NSUB6")) {
					open_product_autoselct("answer5", 6);
					rdiocheckN25.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else if (roofNGOentrydoorsvalueprev.equals("X")) {
					open_product_autoselct("answer5", 7);
					rdiocheckX5.setChecked(true);
					Entrydoorsval = roofNGOentrydoorsvalueprev;
				} else {
				}

				if (roofNGOgaragedoorvalueprev.equals("NA")) {
					open_product_autoselct("answer6", 0);
					rdiocheckNA6.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("A")) {
					open_product_autoselct("answer6", 1);
					rdiocheckA6.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("B")) {
					open_product_autoselct("answer6", 2);
					rdiocheckB6.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("C")) {
					open_product_autoselct("answer6", 3);
					rdiocheckC6.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("D")) {
					open_product_autoselct("answer6", 4);
					rdiocheckD6.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("N")) {
					open_product_autoselct("answer6", 5);
					rdiocheckN16.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("NSUB5")) {
					open_product_autoselct("answer6", 6);
					rdiocheckN26.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else if (roofNGOgaragedoorvalueprev.equals("X")) {
					open_product_autoselct("answer6", 7);
					rdiocheckX6.setChecked(true);
					NGOGarageDoorsval = roofNGOgaragedoorvalueprev;
				} else {
				}

				if (roofopenprotvalueprev.equals("1")) {
					rdiochk = roofopenprotvalueprev;
					rdioquestion1.setChecked(true);

					lnrlytsuboptionA.setVisibility(v1.VISIBLE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.GONE);

					if (roofopenprotsubvalueprev.equals("1")) {
						rdioquestion1answer1.setChecked(true);
						suboption = roofopenprotsubvalueprev;
					} else if (roofopenprotsubvalueprev.equals("2")) {
						rdioquestion1answer2.setChecked(true);

						suboption = roofopenprotsubvalueprev;
					} else if (roofopenprotsubvalueprev.equals("3")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion1answer3.setChecked(true);
					} else {

					}
				} else if (roofopenprotvalueprev.equals("2")) {
					rdiochk = roofopenprotvalueprev;
					rdioquestion2.setChecked(true);

					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.VISIBLE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.GONE);

					if (roofopenprotsubvalueprev.equals("1")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion2answer1.setChecked(true);
					} else if (roofopenprotsubvalueprev.equals("2")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion2answer2.setChecked(true);
					} else if (roofopenprotsubvalueprev.equals("3")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion2answer3.setChecked(true);
					} else {

					}
				} else if (roofopenprotvalueprev.equals("3")) {
					rdiochk = roofopenprotvalueprev;
					rdioquestion3.setChecked(true);
					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.VISIBLE);
					lnrlytsuboptionN.setVisibility(v1.GONE);
					if (roofopenprotsubvalueprev.equals("1")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion3answer1.setChecked(true);
					} else if (roofopenprotsubvalueprev.equals("2")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion3answer2.setChecked(true);
					} else if (roofopenprotsubvalueprev.equals("3")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion3answer3.setChecked(true);
					} else {

					}
				} else if (roofopenprotvalueprev.equals("4")) {
					rdiochk = roofopenprotvalueprev;
					rdioquestion4.setChecked(true);
					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.VISIBLE);
					if (roofopenprotsubvalueprev.equals("1")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion4answer1.setChecked(true);
					} else if (roofopenprotsubvalueprev.equals("2")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion4answer2.setChecked(true);
					} else if (roofopenprotsubvalueprev.equals("3")) {
						suboption = roofopenprotsubvalueprev;
						rdioquestion4answer3.setChecked(true);
					} else {

					}
				} else if (roofopenprotvalueprev.equals("5")) {
					rdioquestion5.setChecked(true);
					rdiochk = roofopenprotvalueprev;
				}
				rdiochk = roofopenprotvalueprev;
			}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OpenProtect.this +" problem in fetching openproectection data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}

		try {
			Cursor cur = db.wind_db.rawQuery("select * from " + db.QuestionsComments
					+ " where SRID='" + cf.Homeid + "'", null);
			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {
				roofopenprotcomment = db.decode(cur.getString(cur
						.getColumnIndex("OpeningProtectionComment")));
				comments.setText(roofopenprotcomment);
			}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OpenProtect.this +" problem in fetching openproectection comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}
	class Touch_Listener implements OnTouchListener
	{
		   
		   Touch_Listener()
			{
							
			}
		    public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
		    	
				
		    		cf.setFocus(comments);
				
		
				return false;
		    }
	}
	
	
	class clicker1 implements OnClickListener {
		public void onClick(View v) {
			if (v == miameweblinkopen) {
				if (inc == 1) {
					iInspectionList = new Intent(QuesOpenProt.this,
							WeblinkOpen.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList
							.putExtra("weburl",
									"http://www.miamidade.gov/building/pc-search_app.asp");
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("page", "open");
					startActivity(iInspectionList);
					finish();
				} else {

				}
			} else if (v == fbcweblinkopen) {
				if (inc == 1) {
					iInspectionList = new Intent(QuesOpenProt.this,
							WeblinkOpen.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList
							.putExtra("weburl",
									"http://www.floridabuilding.org/pr/pr_app_srch.aspx");
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.putExtra("page", "open");
					startActivity(iInspectionList);
					finish();

				} else {

				}
			}
		}
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {
		public void onClick(View v) {
			String s;
			switch (v.getId()) {
			case R.id.rdioques1:
				rdiochk = "1";viewimage=1;
				lnrlytsuboptionA.setVisibility(v1.VISIBLE);
				lnrlytsuboptionB.setVisibility(v1.GONE);
				lnrlytsuboptionC.setVisibility(v1.GONE);
				lnrlytsuboptionN.setVisibility(v1.GONE);
				rdioquestion1.setChecked(true);
				rdioquestion2.setChecked(false);
				rdioquestion3.setChecked(false);
				rdioquestion4.setChecked(false);
				rdioquestion5.setChecked(false);
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
				break;
			case R.id.rdioques2:
				rdiochk = "2";viewimage=1;
				lnrlytsuboptionA.setVisibility(v1.GONE);
				lnrlytsuboptionB.setVisibility(v1.VISIBLE);
				lnrlytsuboptionC.setVisibility(v1.GONE);
				lnrlytsuboptionN.setVisibility(v1.GONE);
				rdioquestion1.setChecked(false);
				rdioquestion2.setChecked(true);
				rdioquestion3.setChecked(false);
				rdioquestion4.setChecked(false);
				rdioquestion5.setChecked(false);
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
				break;

			case R.id.rdioques3:
				rdiochk = "3";viewimage=1;
				lnrlytsuboptionA.setVisibility(v1.GONE);
				lnrlytsuboptionB.setVisibility(v1.GONE);
				lnrlytsuboptionC.setVisibility(v1.VISIBLE);
				lnrlytsuboptionN.setVisibility(v1.GONE);
				rdioquestion1.setChecked(false);
				rdioquestion2.setChecked(false);
				rdioquestion3.setChecked(true);
				rdioquestion4.setChecked(false);
				rdioquestion5.setChecked(false);
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
				break;

			case R.id.rdioques4:
				rdiochk = "4";viewimage=1;
				lnrlytsuboptionA.setVisibility(v1.GONE);
				lnrlytsuboptionB.setVisibility(v1.GONE);
				lnrlytsuboptionC.setVisibility(v1.GONE);
				lnrlytsuboptionN.setVisibility(v1.VISIBLE);
				rdioquestion1.setChecked(false);
				rdioquestion2.setChecked(false);
				rdioquestion3.setChecked(false);
				rdioquestion4.setChecked(true);
				rdioquestion5.setChecked(false);
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
				break;

			case R.id.rdioques5:
				viewimage=1;
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
				comments.setText(commentsfill);
				rdiochk = "5";
				lnrlytsuboptionA.setVisibility(v1.GONE);
				lnrlytsuboptionB.setVisibility(v1.GONE);
				lnrlytsuboptionC.setVisibility(v1.GONE);
				lnrlytsuboptionN.setVisibility(v1.GONE);
				rdioquestion1.setChecked(false);
				rdioquestion2.setChecked(false);
				rdioquestion3.setChecked(false);
				rdioquestion4.setChecked(false);
				rdioquestion5.setChecked(true);
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques1ans1:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A1 for non glazed openings, meeting the large missile impact 9lb rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs and documentation.";
				comments.setText(commentsfill);
				suboption = "1";
				rdioquestion1answer1.setChecked(true);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques1ans2:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "2";
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(true);
				rdioquestion1answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques1ans3:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. One or more non glazed openings did not meet level A, large 9 lb missile impact rating as identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "3";
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(true);
				viewimage = 1;
				break;
			case R.id.rdioques2ans1:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B1 for non glazed openings, meeting the large 4-8lb  missile impact rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "1";
				rdioquestion2answer1.setChecked(true);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques2ans2:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "2";
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(true);
				rdioquestion2answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques2ans3:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  B for glazed openings and B3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. One or more non glazed opening did not meet level B large missile, 4-8lb, impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "3";
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(true);
				viewimage = 1;
				break;

			case R.id.rdioques3ans1:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels. Refer to attached Photographs.";
				comments.setText(commentsfill);
				suboption = "1";
				rdioquestion3answer1.setChecked(true);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques3ans2:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "2";
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(true);
				rdioquestion3answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques3ans3:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  C for glazed openings and C3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as protected with wood structural panels that are compliant to the Florida Building Code. One or more non glazed opening did not meet level A,B or C impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "3";
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(true);
				viewimage = 1;
				break;

			case R.id.rdioques4ans1:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that the opening protection was present to all openings that could not be verified as meeting the impact requirements of selection A or B. Refer to attached Photographs.";
				comments.setText(commentsfill);
				suboption = "1";
				rdioquestion4answer1.setChecked(true);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioques4ans2:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that we were unable to verify the opening protection as meeting large missile impact rating as outline in selection A and B, apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "2";
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(true);
				rdioquestion4answer3.setChecked(false);
			    viewimage = 1;
				break;

			case R.id.rdioques4ans3:
				commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  N for glazed openings and N3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that some openings were not protected. Refer to attached photographs.";
				comments.setText(commentsfill);
				suboption = "3";
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(true);
				viewimage = 1;
				break;

			case R.id.NAans1:
				Windowentrydoorsval = "NA";
				s = "answer1";

				poen_product_autoselct(s, 1);
				rdiocheckNA1.setChecked(true);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(false); // comments.setFocusable(false);

				break;

			case R.id.NAans2:
				s = "answer2";
				poen_product_autoselct(s, 1);
				Garagedoorsval = "NA";
				rdiocheckNA2.setChecked(true);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.NAans3:
				s = "answer3";
				poen_product_autoselct(s, 1);
				Skylightsval = "NA";
				rdiocheckNA3.setChecked(true);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.NAans4:
				s = "answer4";
				poen_product_autoselct(s, 1);
				Glassblocksval = "NA";
				rdiocheckNA4.setChecked(true);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.NAans5:
				s = "answer5";
				poen_product_autoselct(s, 1);
				Entrydoorsval = "NA";
				rdiocheckNA5.setChecked(true);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.NAans6:

				s = "answer6";
				poen_product_autoselct(s, 1);
				NGOGarageDoorsval = "NA";
				rdiocheckNA6.setChecked(true);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioAans1:
				s = "answer1";
				poen_product_autoselct(s, 2);
				Windowentrydoorsval = "A";
				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(true);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioAans2:
				s = "answer2";
				poen_product_autoselct(s, 2);
				Garagedoorsval = "A";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(true);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioAans3:
				s = "answer3";
				poen_product_autoselct(s, 2);
				Skylightsval = "A";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(true);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioAans4:
				s = "answer4";
				poen_product_autoselct(s, 2);
				Glassblocksval = "A";
				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(true);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioAans5:
				s = "answer5";
				poen_product_autoselct(s, 2);
				Entrydoorsval = "A";
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(true);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioAans6:
				s = "answer6";
				poen_product_autoselct(s, 2);
				NGOGarageDoorsval = "A";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(true);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioBans1:
				s = "answer1";
				poen_product_autoselct(s, 3);
				Windowentrydoorsval = "B";
				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(true);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioBans2:
				s = "answer2";
				poen_product_autoselct(s, 3);
				Garagedoorsval = "B";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(true);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioBans3:
				s = "answer3";
				poen_product_autoselct(s, 3);
				Skylightsval = "B";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(true);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioBans4:
				s = "answer4";
				poen_product_autoselct(s, 3);
				Glassblocksval = "B";
				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(true);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioBans5:
				s = "answer5";
				poen_product_autoselct(s, 3);
				Entrydoorsval = "B";
				;
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(true);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioBans6:
				s = "answer6";
				poen_product_autoselct(s, 3);
				NGOGarageDoorsval = "B";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(true);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioCans1:
				s = "answer1";
				poen_product_autoselct(s, 4);
				Windowentrydoorsval = "C";
				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(true);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioCans2:
				s = "answer2";
				poen_product_autoselct(s, 4);
				Garagedoorsval = "C";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(true);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioCans3:
				s = "answer3";
				poen_product_autoselct(s, 4);
				Skylightsval = "C";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(true);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioCans4:
				s = "answer4";
				poen_product_autoselct(s, 4);
				Glassblocksval = "C";
				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(true);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioCans5:
				s = "answer5";
				poen_product_autoselct(s, 4);
				Entrydoorsval = "C";
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(true);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioCans6:
				s = "answer6";
				poen_product_autoselct(s, 4);
				NGOGarageDoorsval = "C";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(true);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioDans1:
				s = "answer1";
				poen_product_autoselct(s, 5);
				Windowentrydoorsval = "D";
				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(true);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioDans2:
				s = "answer2";
				poen_product_autoselct(s, 5);
				Garagedoorsval = "D";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(true);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioDans3:
				s = "answer3";
				poen_product_autoselct(s, 5);
				Skylightsval = "D";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(true);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioDans4:
				s = "answer4";
				poen_product_autoselct(s, 5);
				Glassblocksval = "D";
				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(true);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioDans5:
				s = "answer5";
				poen_product_autoselct(s, 5);
				Entrydoorsval = "D";
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(true);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioDans6:
				s = "answer6";
				poen_product_autoselct(s, 5);
				NGOGarageDoorsval = "D";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(true);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioN1ans1:
				s = "answer1";
				poen_product_autoselct(s, 6);
				Windowentrydoorsval = "N";
				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(true);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN1ans2:
				s = "answer2";
				poen_product_autoselct(s, 6);
				Garagedoorsval = "N";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(true);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN1ans3:
				s = "answer3";
				poen_product_autoselct(s, 6);
				Skylightsval = "N";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(true);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN1ans4:
				s = "answer4";
				poen_product_autoselct(s, 6);
				Glassblocksval = "N";
				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(true);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN1ans5:
				s = "answer5";
				poen_product_autoselct(s, 6);
				Entrydoorsval = "N";
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(true);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN1ans6:
				s = "answer6";
				poen_product_autoselct(s, 6);
				NGOGarageDoorsval = "N";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(true);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioN2ans1:
				s = "answer1";
				poen_product_autoselct(s, 7);
				Windowentrydoorsval = "NSUB1";
				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(true);
				rdiocheckX1.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN2ans2:
				s = "answer2";
				poen_product_autoselct(s, 7);
				Garagedoorsval = "NSUB2";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(true);
				rdiocheckX2.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN2ans3:
				s = "answer3";
				poen_product_autoselct(s, 7);
				Skylightsval = "NSUB3";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(true);
				rdiocheckX3.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN2ans4:
				s = "answer4";
				poen_product_autoselct(s, 7);
				Glassblocksval = "NSUB4";

				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(true);
				rdiocheckX4.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN2ans5:
				s = "answer5";
				poen_product_autoselct(s, 7);
				Entrydoorsval = "NSUB6";
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(true);
				rdiocheckX5.setChecked(false);// comments.setFocusable(false);

				break;
			case R.id.rdioN2ans6:
				s = "answer6";
				poen_product_autoselct(s, 7);
				NGOGarageDoorsval = "NSUB5";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(true);
				rdiocheckX6.setChecked(false);// comments.setFocusable(false);

				break;

			case R.id.rdioXans1:
				s = "answer1";
				poen_product_autoselct(s, 8);
				Windowentrydoorsval = "X";

				rdiocheckNA1.setChecked(false);
				rdiocheckA1.setChecked(false);
				rdiocheckB1.setChecked(false);
				rdiocheckC1.setChecked(false);
				rdiocheckD1.setChecked(false);
				rdiocheckN11.setChecked(false);
				rdiocheckN21.setChecked(false);
				rdiocheckX1.setChecked(true);// comments.setFocusable(false);

				break;
			case R.id.rdioXans2:
				s = "answer2";
				poen_product_autoselct(s, 8);
				Garagedoorsval = "X";
				rdiocheckNA2.setChecked(false);
				rdiocheckA2.setChecked(false);
				rdiocheckB2.setChecked(false);
				rdiocheckC2.setChecked(false);
				rdiocheckD2.setChecked(false);
				rdiocheckN12.setChecked(false);
				rdiocheckN22.setChecked(false);
				rdiocheckX2.setChecked(true);// comments.setFocusable(false);

				break;
			case R.id.rdioXans3:
				s = "answer3";
				poen_product_autoselct(s, 8);
				Skylightsval = "X";
				rdiocheckNA3.setChecked(false);
				rdiocheckA3.setChecked(false);
				rdiocheckB3.setChecked(false);
				rdiocheckC3.setChecked(false);
				rdiocheckD3.setChecked(false);
				rdiocheckN13.setChecked(false);
				rdiocheckN23.setChecked(false);
				rdiocheckX3.setChecked(true);// comments.setFocusable(false);

				break;
			case R.id.rdioXans4:
				s = "answer4";
				poen_product_autoselct(s, 8);
				Glassblocksval = "X";
				rdiocheckNA4.setChecked(false);
				rdiocheckA4.setChecked(false);
				rdiocheckB4.setChecked(false);
				rdiocheckC4.setChecked(false);
				rdiocheckD4.setChecked(false);
				rdiocheckN14.setChecked(false);
				rdiocheckN24.setChecked(false);
				rdiocheckX4.setChecked(true); // comments.setFocusable(false);

				break;
			case R.id.rdioXans5:
				s = "answer5";
				poen_product_autoselct(s, 8);
				Entrydoorsval = "X";
				rdiocheckNA5.setChecked(false);
				rdiocheckA5.setChecked(false);
				rdiocheckB5.setChecked(false);
				rdiocheckC5.setChecked(false);
				rdiocheckD5.setChecked(false);
				rdiocheckN15.setChecked(false);
				rdiocheckN25.setChecked(false);
				rdiocheckX5.setChecked(true); // comments.setFocusable(false);

				break;
			case R.id.rdioXans6:
				s = "answer6";
				poen_product_autoselct(s, 8);
				NGOGarageDoorsval = "X";
				rdiocheckNA6.setChecked(false);
				rdiocheckA6.setChecked(false);
				rdiocheckB6.setChecked(false);
				rdiocheckC6.setChecked(false);
				rdiocheckD6.setChecked(false);
				rdiocheckN16.setChecked(false);
				rdiocheckN26.setChecked(false);
				rdiocheckX6.setChecked(true);// comments.setFocusable(false);

				break;
			}
		}
	};

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			OP_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				if(load_comment)
				{
					load_comment=false;
					int loc1[] = new int[2];
					v.getLocationOnScreen(loc1);
					Intent in = cf.loadComments(tmp,loc1);
					in.putExtra("insp_ques", "7");
					in.putExtra("length", len);
					in.putExtra("max_length", 500);
					startActivityForResult(in, cf.loadcomment_code);
					
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Opening Protection");	
			}			
			break;
		
			
		 case R.id.txthelpcontentoptionA: 
			  cf.alerttitle="A � All Glazed Openings � Large Missile 9 lb.";
			  cf.alertcontent="Exterior openings cyclic pressure and 9 lbs. large missile (4.5 lbs. for skylights only): All glazed openings are protected at a minimum, with impact-resistant coverings or products listed as wind-borne debris protection devices in the product approval system of the State of Florida or Miami-Dade County AND meet the requirements of one of the following for �Cyclic Pressure and Large Missile Impact� (Level A in the table above).";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		 case R.id.txthelpcontentoptionB: 
			 cf.alerttitle="All Glazed Openings 4 � 8 lb Large Missile";
			 cf.alertcontent="Exterior opening protection � cyclic pressure and 4-8 lbs. large missile (2-4.5 lbs. for skylights only):All glazed openings are protected, at a minimum, with impact-resistant coverings or products listed as wind-borne debris protection devices in the product approval system of the State of Florida or Miami-Dade County AND meet the requirements of one of the following for �Cyclic Pressure and Large Missile Impact� (Level B in the table above)"; cf.showhelp(cf.alerttitle,cf.alertcontent);
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			 break;
		 case R.id.txthelpcontentoptionC: 
			 cf.alerttitle="C � FBC Plywood";
			 cf.alertcontent="Exterior opening protection � wood structural panels meeting FBC: All glazed openings are covered with plywood/OSB meeting the requirements of Table 1609.1.2 of the FBC 2007 (Level C in the table above).";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			 break;
		 case R.id.txthelpcontentoptionD: 
			 cf.alerttitle="N � Opening Protection Not Verified";
			 cf.alertcontent="Exterior opening protection (unverified shutter systems with no documentation):All glazed openings are protected with protective coverings not meeting the requirements of answer A, B or C OR systems that appear to meet answer A or B with no documentation of compliance (Level N in the table above).";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			 break;
		 case R.id.txthelpcontentoptionE: 
			cf.alerttitle="X � None / Some Not Protected";
			cf.alertcontent="None or some glazed openings:One or more glazed openings classified as Level X in the table above";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			  
		case R.id.hme:
			cf.gohome();
			break;

	
		}
	}

	private void OP_suboption() {
		// TODO Auto-generated method stub
		if(rdioquestion1.isChecked())	{		tmp="1";}
		else if(rdioquestion2.isChecked()) {	tmp="2";}
		else if(rdioquestion3.isChecked()) {	tmp="3";}
		else if(rdioquestion4.isChecked()) {	tmp="4";}
		else if(rdioquestion5.isChecked()) {	tmp="5";}
	    else{tmp="";}
	}
	// this is initializing the form
	private void open_product_autoselct(String s, int i) {
		// TODO Auto-generated method stub
		String temp2[] = { "false", "false", "false", "false", "false",
				"false", "false", "false" };
		for (int j = 0; j < temp2.length; j++) {
			if (j == i) {
				temp2[j] = "true";
			} else {
				temp2[j] = "false";
			}
		}
		autoselection.put(s, temp2);

	}

	protected void poen_product_autoselct(String s, int i) {
		// TODO Auto-generated method stub
		try {
			String temp2[] = { "false", "false", "false", "false", "false",
					"false", "false", "false" };
			for (int j = 0; j < temp2.length; j++) {
				if (j == i - 1) {
					temp2[j] = "true";
				} else {
					temp2[j] = "false";
				}
			}
			autoselection.put(s, temp2);
			String answer_1[] = autoselection.get("answer1");
			String answer_2[] = autoselection.get("answer2");
			String answer_3[] = autoselection.get("answer3");
			String answer_4[] = autoselection.get("answer4");
			String answer_5[] = autoselection.get("answer5");
			String answer_6[] = autoselection.get("answer6");

			if ((answer_1[0] == "true" || answer_1[1] == "true"
					|| answer_1[2] == "true" || answer_1[3] == "true"
					|| answer_1[4] == "true" || answer_1[5] == "true"
					|| answer_1[6] == "true" || answer_1[7] == "true")
					&& (answer_2[0] == "true" || answer_2[1] == "true"
							|| answer_2[2] == "true" || answer_2[3] == "true"
							|| answer_2[4] == "true" || answer_2[5] == "true"
							|| answer_2[6] == "true" || answer_2[7] == "true")
					&& (answer_3[0] == "true" || answer_3[1] == "true"
							|| answer_3[2] == "true" || answer_3[3] == "true"
							|| answer_3[4] == "true" || answer_3[5] == "true"
							|| answer_3[6] == "true" || answer_3[7] == "true")
					&& (answer_4[0] == "true" || answer_4[1] == "true"
							|| answer_4[2] == "true" || answer_4[3] == "true"
							|| answer_4[4] == "true" || answer_4[5] == "true"
							|| answer_4[6] == "true" || answer_4[7] == "true")) {
				if ((answer_4[7] == "true")
						&& ((count_value.trim().equals("miami-dade")) || (count_value
								.trim().equals("broward")))) {
					rdiochk = "5";
					suboption = "0";
					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.GONE);
					rdioquestion1.setChecked(false);
					rdioquestion2.setChecked(false);
					rdioquestion3.setChecked(false);
					rdioquestion4.setChecked(false);
					rdioquestion5.setChecked(true);
					rdioquestion1answer1.setChecked(false);
					rdioquestion1answer2.setChecked(false);
					rdioquestion1answer3.setChecked(false);
					rdioquestion2answer1.setChecked(false);
					rdioquestion2answer2.setChecked(false);
					rdioquestion2answer3.setChecked(false);
					rdioquestion3answer1.setChecked(false);
					rdioquestion3answer2.setChecked(false);
					rdioquestion3answer3.setChecked(false);
					rdioquestion4answer1.setChecked(false);
					rdioquestion4answer2.setChecked(false);
					rdioquestion4answer3.setChecked(false);
					commentsfill = "The opening protection to this home was verified as meeting the requirements of selection X for glazed, for question 7  of the OIR B1 -1802, Opening Protection. This means that some or all openings did not have verifiable opening protection, meeting the impact requirements of selection A or B. Refer to attached Photographs.";
					comments.setText(commentsfill);
					comments.setText("");

				} else if ((answer_1[5] == "true" || answer_2[5] == "true"
						|| answer_3[5] == "true" || answer_4[5] == "true"
						|| answer_1[6] == "true" || answer_2[6] == "true"
						|| answer_3[6] == "true" || answer_4[6] == "true")) {
					rdiochk = "4";
					rdioquestion1.setChecked(false);
					rdioquestion2.setChecked(false);
					rdioquestion3.setChecked(false);
					rdioquestion4.setChecked(true);
					rdioquestion5.setChecked(false);

					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.VISIBLE);
					if (answer_5[7] == "true" && answer_6[7] == "true") {
						rdiochk = "4";
						suboption = "3";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.VISIBLE);
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(true);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  N for glazed openings and N3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that some openings were not protected. Refer to attached photographs.";
						comments.setText(commentsfill);
						comments.setText("");
						// exceedslimit1.setVisibility(v1.GONE);lincomments.setVisibility(v1.GONE);viewimage
						// =1;

					} else if (answer_5[4] == "true" && answer_6[4] == "true") {
						rdiochk = "4";
						suboption = "2";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.VISIBLE);
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(true);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that we were unable to verify the opening protection as meeting large missile impact rating as outline in selection A and B, apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						comments.setText(commentsfill);
						comments.setText("");
						// exceedslimit1.setVisibility(v1.GONE);lincomments.setVisibility(v1.GONE);viewimage
						// =1;

					} else if ((answer_5[0] == "true" || answer_5[1] == "true"
							|| answer_5[2] == "true" || answer_5[3] == "true"
							|| answer_5[5] == "true" || answer_5[6] == "true")
							&& (answer_6[0] == "true" || answer_6[1] == "true"
									|| answer_6[2] == "true"
									|| answer_6[3] == "true"
									|| answer_6[5] == "true" || answer_6[6] == "true")) {
						rdiochk = "4";
						suboption = "1";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.VISIBLE);

						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(true);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection N for glazed openings and N1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that the opening protection was present to all openings that could not be verified as meeting the impact requirements of selection A or B. Refer to attached Photographs.";
						comments.setText(commentsfill);
						comments.setText("");
						// exceedslimit1.setVisibility(v1.GONE);lincomments.setVisibility(v1.GONE);viewimage
						// =1;
					} else {
						comments.setText("");
						// exceedslimit1.setVisibility(v1.GONE);lincomments.setVisibility(v1.GONE);viewimage
						// =1;
						// rdiochk="";
						suboption = "";
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
					}
				} else if (answer_1[3] == "true" || answer_2[3] == "true"
						|| answer_3[3] == "true" || answer_4[3] == "true") {
					rdiochk = "3";
					rdioquestion1.setChecked(false);
					rdioquestion2.setChecked(false);
					rdioquestion3.setChecked(true);
					rdioquestion4.setChecked(false);
					rdioquestion5.setChecked(false);
					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.VISIBLE);
					lnrlytsuboptionN.setVisibility(v1.GONE);

					if ((answer_5[6] == "true" || answer_5[7] == "true" || answer_5[5] == "true")
							&& (answer_6[6] == "true" || answer_6[5] == "true" || answer_6[7] == "true")) {
						rdiochk = "3";
						suboption = "3";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.VISIBLE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(true);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  C for glazed openings and C3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as protected with wood structural panels that are compliant to the Florida Building Code. One or more non glazed opening did not meet level A,B or C impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
						comments.setText(commentsfill);
					} else if ((answer_5[4] == "true" && answer_6[4] == "true")) {
						rdiochk = "3";
						suboption = "2";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.VISIBLE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(true);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels apart from the non glazed garage door. This door opening was verified as wind only rated and not impact rated, and identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						comments.setText(commentsfill);

					} else if ((answer_5[1] == "true" || answer_5[2] == "true"
							|| answer_5[3] == "true" || answer_5[0] == "true")
							&& (answer_6[1] == "true" || answer_6[0] == "true"
									|| answer_6[2] == "true" || answer_6[3] == "true")) {
						rdiochk = "3";
						suboption = "1";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.VISIBLE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(true);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection C for glazed openings and C1 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection. This means that all openings to this home were verified as meeting the Florida Building Code requirements for wood structural panels. Refer to attached Photographs.";
						comments.setText(commentsfill);
					} else {
						comments.setText("");

						// rdiochk="";
						suboption = "";
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
					}
				} else if ((answer_1[2] == "true" || answer_2[2] == "true"
						|| answer_3[2] == "true" || answer_4[2] == "true")) {
					rdiochk = "2";
					rdioquestion1.setChecked(false);
					rdioquestion2.setChecked(true);
					rdioquestion3.setChecked(false);
					rdioquestion4.setChecked(false);
					rdioquestion5.setChecked(false);
					lnrlytsuboptionA.setVisibility(v1.GONE);
					lnrlytsuboptionB.setVisibility(v1.VISIBLE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.GONE);

					if ((answer_5[6] == "true" || answer_5[7] == "true"
							|| answer_5[5] == "true" || answer_5[3] == "true")
							&& (answer_6[3] == "true" || answer_6[6] == "true"
									|| answer_6[5] == "true" || answer_6[7] == "true")) {
						rdiochk = "2";
						suboption = "3";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.VISIBLE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(true);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection  B for glazed openings and B3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. One or more non glazed opening did not meet level B large missile, 4-8lb, impact rating as identified on the Opening Protection Level Chart. Refer to attached photographs.";
						comments.setText(commentsfill);

					} else if ((answer_5[4] == "true" && answer_6[4] == "true")) {
						rdiochk = "2";
						suboption = "2";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.VISIBLE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.GONE);
						rdioquestion1.setChecked(false);
						rdioquestion2.setChecked(true);
						rdioquestion3.setChecked(false);
						rdioquestion4.setChecked(false);
						rdioquestion5.setChecked(false);
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(true);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as 4-8lb missile impact rating. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						comments.setText(commentsfill);

					} else if ((answer_5[1] == "true" || answer_5[2] == "true" || answer_5[0] == "true")
							&& (answer_6[0] == "true" || answer_6[1] == "true" || answer_6[2] == "true")) {
						rdiochk = "2";
						suboption = "1";
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(true);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection B for glazed openings and B1 for non glazed openings, meeting the large 4-8lb  missile impact rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs.";
						comments.setText(commentsfill);
					} else {
						comments.setText("");

						// rdiochk="";
						suboption = "";
						lnrlytsuboptionA.setVisibility(v1.GONE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.GONE);
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
					}
				} else if ((answer_1[1] == "true" || answer_2[1] == "true"
						|| answer_3[1] == "true" || answer_4[1] == "true")
						|| (answer_1[0] == "true" || answer_2[0] == "true"
								|| answer_3[0] == "true" || answer_4[0] == "true")) {
					rdioquestion1.setChecked(true);
					rdioquestion2.setChecked(false);
					rdioquestion3.setChecked(false);
					rdioquestion4.setChecked(false);
					rdioquestion5.setChecked(false);
					lnrlytsuboptionA.setVisibility(v1.VISIBLE);
					lnrlytsuboptionB.setVisibility(v1.GONE);
					lnrlytsuboptionC.setVisibility(v1.GONE);
					lnrlytsuboptionN.setVisibility(v1.GONE);

					rdiochk = "1";
					if ((answer_5[6] == "true" || answer_5[2] == "true"
							|| answer_5[7] == "true" || answer_5[5] == "true" || answer_5[3] == "true")
							&& (answer_6[2] == "true" || answer_6[3] == "true"
									|| answer_6[6] == "true"
									|| answer_6[5] == "true" || answer_6[7] == "true")) {
						rdiochk = "1";
						suboption = "3";
						lnrlytsuboptionA.setVisibility(v1.VISIBLE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(true);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A3 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. One or more non glazed openings did not meet level A, large 9 lb missile impact rating as identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						comments.setText(commentsfill);

					} else if ((answer_5[4] == "true" && answer_6[4] == "true")) {
						rdiochk = "1";
						suboption = "2";
						lnrlytsuboptionA.setVisibility(v1.VISIBLE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1.setChecked(true);
						rdioquestion2.setChecked(false);
						rdioquestion3.setChecked(false);
						rdioquestion4.setChecked(false);
						rdioquestion5.setChecked(false);
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(true);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A2 for non glazed openings, for question 7  of the OIR B1 -1802, Opening Protection.  This means that all glazed openings were verified as impact rated. A non glazed garage door opening was however verified as wind only rated and not impact rated, being identified on the Opening Protection Level Chart as level D. Refer to attached photographs.";
						comments.setText(commentsfill);

					} else if ((answer_5[1] == "true" || answer_5[0] == "true")
							&& (answer_6[1] == "true" || answer_6[0] == "true")) {
						rdiochk = "1";
						suboption = "1";
						lnrlytsuboptionA.setVisibility(v1.VISIBLE);
						lnrlytsuboptionB.setVisibility(v1.GONE);
						lnrlytsuboptionC.setVisibility(v1.GONE);
						lnrlytsuboptionN.setVisibility(v1.GONE);

						rdioquestion1.setChecked(true);
						rdioquestion2.setChecked(false);
						rdioquestion3.setChecked(false);
						rdioquestion4.setChecked(false);
						rdioquestion5.setChecked(false);
						rdioquestion1answer1.setChecked(true);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
						commentsfill = "The opening protection to this home was verified as meeting the requirements of selection A for glazed openings and A1 for non glazed openings, meeting the large missile impact 9lb rating, for question 7  of the OIR B1 -1802, Opening Protection. Refer to attached photographs and documentation.";
						comments.setText(commentsfill);
					} else {
						comments.setText("");
						suboption = "";
						rdioquestion1answer1.setChecked(false);
						rdioquestion1answer2.setChecked(false);
						rdioquestion1answer3.setChecked(false);
						rdioquestion2answer1.setChecked(false);
						rdioquestion2answer2.setChecked(false);
						rdioquestion2answer3.setChecked(false);
						rdioquestion3answer1.setChecked(false);
						rdioquestion3answer2.setChecked(false);
						rdioquestion3answer3.setChecked(false);
						rdioquestion4answer1.setChecked(false);
						rdioquestion4answer2.setChecked(false);
						rdioquestion4answer3.setChecked(false);
					}
				} else {
					comments.setText("");

					rdiochk = "";
					suboption = "";

					rdioquestion1.setChecked(false);
					rdioquestion2.setChecked(false);
					rdioquestion3.setChecked(false);
					rdioquestion4.setChecked(false);
					rdioquestion5.setChecked(false);
					rdioquestion1answer1.setChecked(false);
					rdioquestion1answer2.setChecked(false);
					rdioquestion1answer3.setChecked(false);
					rdioquestion2answer1.setChecked(false);
					rdioquestion2answer2.setChecked(false);
					rdioquestion2answer3.setChecked(false);
					rdioquestion3answer1.setChecked(false);
					rdioquestion3answer2.setChecked(false);
					rdioquestion3answer3.setChecked(false);
					rdioquestion4answer1.setChecked(false);
					rdioquestion4answer2.setChecked(false);
					rdioquestion4answer3.setChecked(false);
				}
			} else {
				comments.setText("");

				rdiochk = "";
				suboption = "";
				lnrlytsuboptionA.setVisibility(v1.GONE);
				lnrlytsuboptionB.setVisibility(v1.GONE);
				lnrlytsuboptionC.setVisibility(v1.GONE);
				lnrlytsuboptionN.setVisibility(v1.GONE);

				rdioquestion1.setChecked(false);
				rdioquestion2.setChecked(false);
				rdioquestion3.setChecked(false);
				rdioquestion4.setChecked(false);
				rdioquestion5.setChecked(false);
				rdioquestion1answer1.setChecked(false);
				rdioquestion1answer2.setChecked(false);
				rdioquestion1answer3.setChecked(false);
				rdioquestion2answer1.setChecked(false);
				rdioquestion2answer2.setChecked(false);
				rdioquestion2answer3.setChecked(false);
				rdioquestion3answer1.setChecked(false);
				rdioquestion3answer2.setChecked(false);
				rdioquestion3answer3.setChecked(false);
				rdioquestion4answer1.setChecked(false);
				rdioquestion4answer2.setChecked(false);
				rdioquestion4answer3.setChecked(false);
			}

		} catch (Exception e) {
			System.out.println("error in the click even " + e.getMessage());
			
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OpenProtect.this +" problem in autoselecting openprotection values on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}

	}

	private void getcountname() {
		try {
			Cursor cur = db.wind_db.rawQuery(
					"select * from Retail_inpgeneralinfo where s_SRID='"
							+ cf.Homeid + "'", null);

			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {

				count_value = cur.getString(cur.getColumnIndex("s_County")).toLowerCase();

			}
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ OpenProtect.this +" problem in retrieving county name on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesOpenProt.this, QuesSWR.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
				((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
			}
		}
		/*switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}
*/
	}
	
}
