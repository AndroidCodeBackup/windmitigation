package idsoft.inspectiondepot.windmitinspection;

import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class QuesWallCons extends Activity {
	View v1;
	String[] rowlength,arrcomment1,array_spiner;
	CheckBox[] cb;
	boolean load_comment = true;
	CheckBox temp_st;
	private CheckBox chkAnswer1, chkAnswer2, chkAnswer3, chkAnswer4,
			chkAnswer5;
	String tmp="",per1 = "0", per2 = "0", per3 = "0", per4 = "0", per5 = "0",updatecnt,woodframeval = "0", reinmasonryval = "0", unreinmasonryval = "0",
			pouredconcreteval = "0", otherwallval = "0", comm,InspectionType, status, homeId,
			otherwallconstxt, wallconstothertxt = "",inspectortypeid,commdescrip="",conchkbox, comm2, descrip,dupcomm;
	TextView txtheadingwallconstruction,WC_type,helptxt,prevmitidata;
	Spinner spn1, spn2, spn3, spn4, spn5;
	int per11 = 0, per12 = 0, per13 = 0, per14 = 0, per15 = 0, total,
			commentsch,totp = 0,optionid,value, Count,viewimage = 1;
	Button savenext;
	ArrayAdapter adapter;
	Intent iInspectionList;
	ListView list;
	EditText comments, othertext;
	private static final int visibility = 0;
	AlertDialog alertDialog;
	CommonFunctions cf;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	TextWatcher watcher;
	DatabaseFunctions db;
	WebserviceFunctions wb;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);

		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);

		}
		setContentView(R.layout.wallconstruction);
		db.getInspectorId();
		db.getPHinformation(cf.Homeid);
		cf.getDeviceDimensions();
		db.changeimage(cf.Homeid);
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(QuesWallCons.this, 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(QuesWallCons.this, 28,cf, 1));
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
			  cf.alertcontent="To see help comments, Please select Wall Construction options.";
				  cf.showhelp("HELP",cf.alertcontent);
			
			}
		});
		
		txtheadingwallconstruction = (TextView) findViewById(R.id.txtheadingwallconstruction);
		txtheadingwallconstruction
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>Check all wall construction types for exterior walls of the structure and percentages for each:"
								+ "<br/><font color=red> Note : All of these must equal to 100%"
								+ "</font>"));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);

		db.getQuesOriginal(cf.Homeid);
		if(!db.oporiddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.oporiddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}
		
		
		array_spiner = new String[11];
		array_spiner[0] = "Select";
		array_spiner[1] = "10";
		array_spiner[2] = "20";
		array_spiner[3] = "30";
		array_spiner[4] = "40";
		array_spiner[5] = "50";
		array_spiner[6] = "60";
		array_spiner[7] = "70";
		array_spiner[8] = "80";
		array_spiner[9] = "90";
		array_spiner[10] = "100";

		try {
			this.chkAnswer1 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer1);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer4 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer4);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer2 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer2);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer3 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer3);
		} catch (Exception e) {
		}
		try {
			this.chkAnswer5 = (CheckBox) this
					.findViewById(R.id.chkquestion7answer5);
		} catch (Exception e) {
		}

		spn1 = (Spinner) this.findViewById(R.id.spranswer1);
		spn4 = (Spinner) this.findViewById(R.id.spranswer4);
		spn2 = (Spinner) this.findViewById(R.id.spranswer2);
		spn5 = (Spinner) this.findViewById(R.id.spranswer5);
		spn3 = (Spinner) this.findViewById(R.id.spranswer3);

		spn1.setEnabled(false);
		spn2.setEnabled(false);
		spn3.setEnabled(false);
		spn4.setEnabled(false);
		spn5.setEnabled(false);

		adapter = new ArrayAdapter(QuesWallCons.this, android.R.layout.simple_spinner_item,	array_spiner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn1.setAdapter(adapter);

		adapter = new ArrayAdapter(QuesWallCons.this, android.R.layout.simple_spinner_item,	array_spiner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn4.setAdapter(adapter);

		adapter = new ArrayAdapter(QuesWallCons.this, android.R.layout.simple_spinner_item,array_spiner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn2.setAdapter(adapter);

		adapter = new ArrayAdapter(QuesWallCons.this, android.R.layout.simple_spinner_item,array_spiner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn5.setAdapter(adapter);

		adapter = new ArrayAdapter(QuesWallCons.this, android.R.layout.simple_spinner_item,array_spiner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn3.setAdapter(adapter);

		chkAnswer1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					woodframeval = "1";
					spn1.setEnabled(true);
					viewimage=1;
				} else {
					spn1.setSelection(0);
					spn1.setEnabled(false);
					woodframeval = "0";

				}
			}
		});
		chkAnswer2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					reinmasonryval = "1";
					spn2.setEnabled(true);
					viewimage=1;

				} else {
					spn2.setSelection(0);
					spn2.setEnabled(false);
					reinmasonryval = "0";
				}
			}
		});
		chkAnswer3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					otherwallval = "1";
					othertext.setEnabled(true);
					spn3.setEnabled(true);
					viewimage=1;

				} else {
					spn3.setSelection(0);
					spn3.setEnabled(false);
					othertext.setEnabled(false);
					othertext.setText("");
					otherwallval = "0";
				}
			}
		});
		chkAnswer4.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					unreinmasonryval = "1";
					spn4.setEnabled(true);
					viewimage=1;

				} else {
					spn4.setSelection(0);
					spn4.setEnabled(false);
					unreinmasonryval = "0";
				}
			}
		});
		chkAnswer5.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					pouredconcreteval = "1";

					spn5.setEnabled(true);
					viewimage=1;
				} else {
					spn5.setSelection(0);
					spn5.setEnabled(false);

					pouredconcreteval = "0";
				}
			}
		});
		spn1.setOnItemSelectedListener(new MyOnItemSelectedListener1());
		spn2.setOnItemSelectedListener(new MyOnItemSelectedListener2());
		spn3.setOnItemSelectedListener(new MyOnItemSelectedListener3());
		spn4.setOnItemSelectedListener(new MyOnItemSelectedListener4());
		spn5.setOnItemSelectedListener(new MyOnItemSelectedListener5());

		comments = (EditText) this.findViewById(R.id.txtcomments);
		WC_type=(TextView) findViewById(R.id.SH_TV_ED);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,WC_type));
		
		othertext = (EditText) this.findViewById(R.id.othertxtwallconst);

		try {
			Cursor c2 = db.wind_db
					.rawQuery(
							"SELECT WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,"
									+ "PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer"
									+ " FROM "
									+ db.Questions
									+ " WHERE SRID='" + cf.Homeid + "'", null);
			if (c2.getCount() == 0) {
				
			} else {
				c2.moveToFirst();
				if (c2 != null) {
					int tmp = 0;
					String WoodFrameValue, WoodFramePer, ReinMasonryValue, ReinMasonryPer, UnReinMasonryValue, UnReinMasonryPer, PouredConcrete, PouredConcretePer, OtherWal, OtherWallPer;
					WoodFrameValue = c2.getString(c2
							.getColumnIndex("WoodFrameValue"));
					ReinMasonryValue = c2.getString(c2
							.getColumnIndex("ReinMasonryValue"));
					UnReinMasonryValue = c2.getString(c2
							.getColumnIndex("UnReinMasonryValue"));
					PouredConcrete = c2.getString(c2
							.getColumnIndex("PouredConcrete"));
					OtherWal = db.decode(c2.getString(c2
							.getColumnIndex("OtherWal")));
					if (WoodFrameValue.equals("1")) {
						tmp = 0;
						woodframeval = "1";
						this.chkAnswer1.setChecked(true);
						spn1.setEnabled(true);
						per1 = WoodFramePer = c2.getString(c2
								.getColumnIndex("WoodFramePer"));
						if (!WoodFramePer.equals("")) {
							per11 = tmp = Integer.parseInt(WoodFramePer);
							tmp = tmp / 10;
						}
						spn1.setSelection(tmp);
					}
					if (ReinMasonryValue.equals("1")) {
						tmp = 0;
						reinmasonryval = "1";
						this.chkAnswer2.setChecked(true);
						spn2.setEnabled(true);
						per2 = ReinMasonryPer = c2.getString(c2
								.getColumnIndex("ReinMasonryPer"));
						if (!ReinMasonryPer.equals("")) {
							per12 = tmp = Integer.parseInt(ReinMasonryPer);
							tmp = tmp / 10;
						}
						spn2.setSelection(tmp);
					}
					if (UnReinMasonryValue.equals("1")) {
						tmp = 0;
						unreinmasonryval = "1";
						this.chkAnswer4.setChecked(true);
						spn4.setEnabled(true);
						per3 = UnReinMasonryPer = c2.getString(c2
								.getColumnIndex("UnReinMasonryPer"));
						if (!UnReinMasonryPer.equals("")) {
							per13 = tmp = Integer.parseInt(UnReinMasonryPer);
							tmp = tmp / 10;
						}
						spn4.setSelection(tmp);
					}
					if (PouredConcrete.equals("1")) {
						tmp = 0;
						pouredconcreteval = "1";
						this.chkAnswer5.setChecked(true);
						spn5.setEnabled(true);
						per4 = PouredConcretePer = c2.getString(c2
								.getColumnIndex("PouredConcretePer"));
						if (!PouredConcretePer.equals("")) {
							per15 = tmp = Integer.parseInt(PouredConcretePer);
							tmp = tmp / 10;
						}
						spn5.setSelection(tmp);
					}
					if (OtherWal.equals("1")) {
						tmp = 0;
						otherwallval = "1";
						this.chkAnswer3.setChecked(true);
						spn3.setEnabled(true);
						per5 = OtherWallPer = c2.getString(c2
								.getColumnIndex("OtherWallPer"));
						if (!OtherWallPer.equals("")) {
							per13 = tmp = Integer.parseInt(OtherWallPer);
							tmp = tmp / 10;
						}
						otherwallconstxt = db.decode(c2.getString(c2
								.getColumnIndex("OtherWallTitle")));
						othertext.setText(otherwallconstxt);
						spn3.setSelection(tmp);

					}
				}

			}
		} catch (Exception e) {
			
			//cf.Error_LogFile_Creation(e.getMessage()+ " at "+ WallConstruction.this +" problem in retrieving wallconstruction information on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}
		try {
			Cursor cur = db.wind_db.rawQuery("select * from " + db.QuestionsComments
					+ " where SRID='" + cf.Homeid + "'", null);
			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {
				String wallconstructcomment = db.decode(cur
						.getString(cur
								.getColumnIndex("WallConstructionComment")));
				
				comments.setText(wallconstructcomment);
			}
			}
		} catch (Exception e) {
			
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" in retrieving wallconstruction comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
		}

		this.savenext = (Button) this.findViewById(R.id.savenext);
		
		
	}


	public class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			per1 = parent.getItemAtPosition(pos).toString();
			if (per1.equals("")) {
				per1 = "0";
			}
			try {
				per11 = Integer.parseInt(per1);
			} catch (Exception e) {
				
				per11 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in first spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}

		}

		public void onNothingSelected(AdapterView parent) {

			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			per2 = parent.getItemAtPosition(pos).toString();
			if (per1.equals("")) {
				per1 = "0";
			}
			try {
				per12 = Integer.parseInt(per2);
			} catch (Exception e) {
				
				per12 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in second spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {

			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			per3 = parent.getItemAtPosition(pos).toString();
			if (per3.equals("")) {
				per3 = "0";
			}
			try {
				per15 = Integer.parseInt(per3);
			} catch (Exception e) {
				per15 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in third spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			per4 = parent.getItemAtPosition(pos).toString();
			if (per4.equals("")) {
				per4 = "0";
			}
			try {
				per13 = Integer.parseInt(per4);
			} catch (Exception e) {
				per13 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in fourth spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener5 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			per5 = parent.getItemAtPosition(pos).toString();
			if (per5.equals("")) {
				per5 = "0";
			}
			try {
				per14 = Integer.parseInt(per5);
			} catch (Exception e) {
				per14 = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ WallConstruction.this +" conversion of data types in fifth spinner on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
				
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		
		case R.id.loadcomments:
			/***Call for the comments***/
			WC_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				System.out.println("tesload");
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_ques", "8");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
						
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Building Code");	
			}			
			break;
		
		case R.id.txthelpcontentoptionA:
			  cf.alerttitle="Wood Frame";
			  cf.alertcontent="Wood Frame";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.txthelpcontentoptionB:
			  cf.alerttitle="Un-Reinforced Masonry";
			  cf.alertcontent="Un-Reinforced Masonry";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionC:
			  cf.alerttitle="Reinforced Masonry";
			  cf.alertcontent="Reinforced Masonry";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionD:
			  cf.alerttitle="Poured concrete";
			  cf.alertcontent="Poured concrete";
			cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionE:
			  cf.alerttitle="Other";
			  cf.alertcontent="Other";
			 cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;

		case R.id.hme:
			cf.gohome();
			break;
		case R.id.savenext:
			boolean chkglobal = false;
			comm = comments.getText().toString();
			totp = per11 + per12 + per13 + per14 + per15;
			if(chkAnswer1.isChecked() || chkAnswer2.isChecked() || chkAnswer3.isChecked() || chkAnswer4.isChecked() || chkAnswer5.isChecked())
			{	
			if ((chkAnswer1.isChecked() == true) && (chkglobal == false)) {
				if (per11 == 0) {
					cf.ShowToast("Please select wood frame percentage");
					chkglobal = true;
				}
			}
			if ((chkAnswer2.isChecked() == true) && (chkglobal == false)) {
				if (per12 == 0) {
					cf.ShowToast("Please select reinforced masonry percentage");
					chkglobal = true;

				}
			}
			if ((chkAnswer3.isChecked() == true) && (chkglobal == false)) {
				if (per15 == 0) {
					cf.ShowToast("Please select other percentage");
					chkglobal = true;

				}
			}
			if ((chkAnswer4.isChecked() == true) && (chkglobal == false)) {
				if (per13 == 0) {
					cf.ShowToast("Please select unreinforced masonry percentage");
					chkglobal = true;

				}
			}
			if ((chkAnswer5.isChecked() == true) && (chkglobal == false)) {
				if (per14 == 0) {
					cf.ShowToast("Please select poured concrete percentage");
					chkglobal = true;
				}
			}
			/*if ((comm.trim().equals("") && (chkglobal == false))) {
				cf.ShowToast("Please enter the comments for Wall Construction Type");
				chkglobal = true;

			} else*/ 
			if ((totp == 0) && (chkglobal == false)) {
				cf.ShowToast("Percentage should sum to 100");
				chkglobal = true;

			} else if (((totp > 100) || (totp < 100)) && (chkglobal == false)) {
				cf.ShowToast("Percentage should sum to 100");
				chkglobal = true;

			} else if ((totp == 100) && (chkglobal == false)) {
				if (otherwallval.equals("1")) {
					String s = othertext.getText().toString().trim();

					if (s.equals("")) {
						cf.ShowToast("Enter the text for Other Wall Construction Type");
						chkglobal = true;

					} else {
						try {
							otherwallconstxt = othertext.getText().toString();
							Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
									+ db.Questions + " WHERE SRID='"
									+ cf.Homeid + "'", null);
							int rws = c2.getCount();
							if (rws == 0) {
								db.wind_db.execSQL("INSERT INTO "
										+ db.Questions
										+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
										+ "VALUES ('"
										+ cf.Homeid
										+ "','','','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','','','','','"
										+ 0
										+ "','"
										+ 0
										+ "','','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','','"
										+ 0
										+ "','"
										+ 0
										+ "','"
										+ 0
										+ "','','"
										+ 0
										+ "','','"
										+ 0
										+ "','"
										+ 0
										+ "','','','','','','','','"
										+ woodframeval
										+ "','"
										+ per11
										+ "','"
										+ reinmasonryval
										+ "','"
										+ per12
										+ "','"
										+ unreinmasonryval
										+ "','"
										+ per13
										+ "','"
										+ pouredconcreteval
										+ "','"
										+ per14
										+ "','"
										+ db.encode(otherwallconstxt)
										+ "','" + otherwallval + "','" + per15
										+ "','','" + 0 + "','','','" + cd
										+ "','" + 0 + "')");

							} else {
								db.wind_db.execSQL("UPDATE "
										+ db.Questions
										+ " SET WoodFrameValue='"
										+ woodframeval
										+ "',WoodFramePer='"
										+ per11
										+ "',ReinMasonryValue='"
										+ reinmasonryval
										+ "',ReinMasonryPer='"
										+ per12
										+ "',UnReinMasonryValue='"
										+ unreinmasonryval
										+ "',UnReinMasonryPer='"
										+ per13
										+ "',PouredConcrete='"
										+ pouredconcreteval
										+ "',PouredConcretePer='"
										+ per14
										+ "',OtherWallTitle='"
										+ db.encode(otherwallconstxt)
										+ "',OtherWal='" + otherwallval
										+ "',OtherWallPer='" + per15 + "'"
										+ " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");

							}

							Cursor c5 = db.wind_db.rawQuery("SELECT * FROM "
									+ db.QuestionsComments + " WHERE SRID='"
									+ cf.Homeid + "'", null);
						
							if (c5.getCount() == 0) {

								db.wind_db.execSQL("INSERT INTO "
										+ db.QuestionsComments
										+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
										+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','','','','','"+ db.encode(comments.getText().toString())+ "','','"+ cf.datewithtime + "')");
						

							} else {

								db.wind_db.execSQL("UPDATE "
										+ db.QuestionsComments
										+ " SET WallConstructionComment='"
										+ db.encode(comments
												.getText().toString())
										+ "',CreatedOn ='"
										+ md + "'" + " WHERE SRID ='"
										+ cf.Homeid.toString() + "'");
							}

							db.getInspectorId();

							Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "
									+ db.SubmitCheckTable + " WHERE Srid='"
									+ cf.Homeid + "'", null);
							if (c3.getCount() == 0) {
								db.wind_db.execSQL("INSERT INTO "
										+ db.SubmitCheckTable
										+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
										+ "VALUES ('" + db.Insp_id + "','"
										+ cf.Homeid
										+ "',0,0,0,0,0,0,0,0,1,0,0,0,0,0,0)");
							} else {
								db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
										+ " SET fld_wall='1' WHERE Srid ='"
										+ cf.Homeid + "' and InspectorId='"
										+ db.Insp_id + "'");
							}

							updatecnt = "1";
							
							// walltick.setVisibility(visibility);
						} catch (Exception e) {

							updatecnt = "0";
							cf.ShowToast("There is a problem in saving your data due to invalid character");
							
							//String errormessage ="inserting or updating wallconstrcution table";
							//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
							
						}

						if (updatecnt == "1") {
							cf.ShowToast("Wall Construction details has been saved successfully");
							Intent intimg1 = new Intent(QuesWallCons.this,
									Signature.class);
							intimg1.putExtra("homeid", cf.Homeid);
							intimg1.putExtra("status", cf.status);
							startActivity(intimg1);
							finish();
						}
					}
				} else {
					try {
						otherwallconstxt = othertext.getText().toString();
						Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
								+ db.Questions + " WHERE SRID='"
								+ cf.Homeid + "'", null);
						if (c2.getCount() == 0) {
							db.wind_db.execSQL("INSERT INTO "
									+ db.Questions
									+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
									+ "VALUES ('" + cf.Homeid + "','','','" + 0
									+ "','" + 0 + "','" + 0 + "','','','','','"
									+ 0 + "','" + 0 + "','','" + 0 + "','" + 0
									+ "','" + 0 + "','" + 0 + "','" + 0
									+ "','','" + 0 + "','" + 0 + "','" + 0
									+ "','','" + 0 + "','','" + 0 + "','" + 0
									+ "','','','','','','','','" + woodframeval
									+ "','" + per11 + "','" + reinmasonryval
									+ "','" + per12 + "','" + unreinmasonryval
									+ "','" + per13 + "','" + pouredconcreteval
									+ "','" + per14 + "','" + otherwallconstxt
									+ "','"
									+ db.encode(otherwallval)
									+ "','" + per15 + "','','" + 0
									+ "','','','" + cd + "','" + 0 + "')");

						} else {
							db.wind_db.execSQL("UPDATE " + db.Questions
									+ " SET WoodFrameValue='" + woodframeval
									+ "',WoodFramePer='" + per11
									+ "',ReinMasonryValue='" + reinmasonryval
									+ "',ReinMasonryPer='" + per12
									+ "',UnReinMasonryValue='"
									+ unreinmasonryval + "',UnReinMasonryPer='"
									+ per13 + "',PouredConcrete='"
									+ pouredconcreteval
									+ "',PouredConcretePer='" + per14
									+ "',OtherWallTitle='"
									+ db.encode(otherwallconstxt)
									+ "',OtherWal='" + otherwallval
									+ "',OtherWallPer='" + per15 + "'"
									+ " WHERE SRID ='" + cf.Homeid.toString()
									+ "'");

						}
					} catch (Exception e) {
						//String errormessage ="Problem in inserting or updating wallconstruction value";
						//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}

					try {

						Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
								+ db.QuestionsComments + " WHERE SRID='" + cf.Homeid
								+ "'", null);
						if (c2.getCount() == 0) {

							
							db.wind_db.execSQL("INSERT INTO "
									+ db.QuestionsComments
									+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','','','','','"+ db.encode(comments.getText().toString())+ "','','"+ cf.datewithtime + "')");
					
						} else {
							db.wind_db.execSQL("UPDATE "
									+ db.QuestionsComments
									+ " SET WallConstructionComment='"
									+ db.encode(comm)
									+ "',CreatedOn ='"
									+ md + "'" + " WHERE SRID ='"
									+ cf.Homeid.toString() + "'");
						}
					} catch (Exception e) {
						//String errormessage ="Problem in inserting or updating wallconstruction comments";
						//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}
					db.getInspectorId();
					try {
						Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "
								+ db.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						if (c3.getCount() == 0) {
							db.wind_db.execSQL("INSERT INTO "
									+ db.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + db.Insp_id + "','"
									+ cf.Homeid
									+ "',0,0,0,0,0,0,0,0,1,0,0,0,0,0,0)");
						} else {
							db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
									+ " SET fld_wall='1' WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ db.Insp_id + "'");
						}
					} catch (Exception e) {
						
						//String errormessage ="Problem in inserting or updating SubmitCheck data";
						//cf.Error_LogFile_Creation(errormessage+" at "+ WallConstruction.this +" on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					}
					updatecnt = "1";
					if (updatecnt == "1") {
						cf.ShowToast("Wall Construction details has been saved successfully");
						Intent intimg1 = new Intent(QuesWallCons.this,Signature.class);
						intimg1.putExtra("homeid", cf.Homeid);
						intimg1.putExtra("status", cf.status);
						startActivity(intimg1);
						finish();
					}
				}

			}
		}
		else
		{cf.ShowToast("Please select atleast one wall construction type.");
			
		}
			break;

		}
	}

	private void WC_suboption() {
		// TODO Auto-generated method stub
		if(chkAnswer1.isChecked())	{		tmp="1";}
		else if(chkAnswer2.isChecked()) {	tmp="2";}
		else if(chkAnswer3.isChecked()) {	tmp="3";}
		else if(chkAnswer4.isChecked()) {	tmp="4";}
		else if(chkAnswer5.isChecked()) {	tmp="5";}
		else{tmp="";}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesWallCons.this, QuesOpenProt.class);
			//intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
				((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
			}
		}
		/*switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}*/

	}
	
}