package idsoft.inspectiondepot.windmitinspection;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MyOnclickListener extends LinearLayout {
	private static final int visibility = 0;
	View v1;
	Context context = null;
	ImageView takephoto,img1, img2, img3, img4, img6, img7, img8, img9, 
			buildtick, roofcovertick, roofdecktick, roofwalltick, roofgeotick,
			swrtick, opentick, walltick,inspinfo,policyinfo;
	Button b,poicyholdimg,agntimg, callimg, schimg, addimg,addiservice,buildcode, roofcover, roofdeck,genhazinfo,genhazimg,
	roofwall, roofgeometry, swr, openprotection, wallconstruction,signatureimg,elevationimg;
	Context activityname;
	CommonFunctions cf;
	public DatabaseFunctions db;
	int app, sub;
	ImageView btn_home;
	RelativeLayout phrel, qrel, imgrel,signrel,genhazrel;
	View V;private static final int SELECT_PICTURE = 0;
	public static int count =0;

	public MyOnclickListener(Context context, int appname, CommonFunctions cf,
			int sub) {
		super(context);
		db = new DatabaseFunctions(context);
		this.context = context;
		this.app = appname;
		this.cf = cf;
		this.sub = sub;
		create();
	}

	public MyOnclickListener(Context context, AttributeSet attrs, int appname,
			CommonFunctions cf, int sub) {
		super(context, attrs);
		this.context = context;
		this.app = appname;
		this.cf = cf;
		this.sub = sub;
		create();
	}

	private void create() {
		System.out.println("ap="+app+"sub="+sub);
		LayoutInflater layoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		/** submenu **/
		
		if (this.sub == 1) {
			/** PH SUB MENU **/
			if (this.app == 11 || this.app == 12 || this.app == 13
					|| this.app == 14 || this.app == 15 || this.app == 16) {
				
				layoutInflater.inflate(R.layout.phsubmenu, this, true);
				phrel = (RelativeLayout) findViewById(R.id.relativeLayout4);
				poicyholdimg = (Button) findViewById(R.id.policyhold);
				agntimg = (Button) findViewById(R.id.agnt);
				callimg = (Button) findViewById(R.id.call);
				schimg = (Button) findViewById(R.id.sch);
				
				
				
				poicyholdimg.setOnClickListener(new clicker());
				agntimg.setOnClickListener(new clicker());
				callimg.setOnClickListener(new clicker());
				schimg.setOnClickListener(new clicker());
				
			}
			/** QUESTIONS SUB MENU **/
			else if (this.app == 21 || this.app == 22 || this.app == 23
					|| this.app == 24 || this.app == 25 || this.app == 26
					|| this.app == 27 || this.app == 28) {
				layoutInflater.inflate(R.layout.quessubmenu, this, true);
				qrel = (RelativeLayout) findViewById(R.id.quest);
				buildcode = (Button) findViewById(R.id.build);				
				
				
				buildtick = (ImageView) findViewById(R.id.buildovr);
				roofcover = (Button) findViewById(R.id.roofcover);
				roofcovertick = (ImageView) findViewById(R.id.roofcoverovr);
				roofdeck = (Button) findViewById(R.id.roofdeck);
				roofwall = (Button) findViewById(R.id.roofwall);
				roofgeometry = (Button) findViewById(R.id.roofgeo);
				swr = (Button) findViewById(R.id.swr);
				openprotection = (Button) findViewById(R.id.openpro);
				wallconstruction = (Button) findViewById(R.id.wall);
				roofdecktick = (ImageView) findViewById(R.id.roofdeckovr);
				roofwalltick = (ImageView) findViewById(R.id.roofwallovr);
				roofgeotick = (ImageView) findViewById(R.id.roofgeoovr);
				swrtick = (ImageView) findViewById(R.id.swrovr);
				opentick = (ImageView) findViewById(R.id.openproovr);
				walltick = (ImageView) findViewById(R.id.wallovr);
				db.changeimage(cf.Homeid);
				
			
				if (!db.buildcodevalue.equals("0")) 
				{
					buildtick.setVisibility(visibility);
				}				
				if (!db.roofdeckvalueprev.equals("0")) 
				{
					roofdecktick.setVisibility(visibility);
				}
				if (!db.rooftowallvalueprev.equals("0")) 
				{					
					roofwalltick.setVisibility(visibility);
				}
				if (!db.roofgeovalue.equals("0")) 
				{
					roofgeotick.setVisibility(visibility);
				}
				if (!db.roofswrvalue.equals("0")) 
				{
					swrtick.setVisibility(visibility);
				}
				if (!db.openprovalue.equals("0")) 
				{
					opentick.setVisibility(visibility);
				}
				if (!db.woodframeper.equals("0") || !db.reper.equals("0")
						|| !db.unreper.equals("0") || !db.pcnper.equals("0")
						|| !db.otrper.equals("0")) {
					walltick.setVisibility(visibility);
				}
				if (db.chkroofcover == 0) {
					roofcovertick.setVisibility(V.GONE);
				} else {
					roofcovertick.setVisibility(visibility);
				}
				
				buildcode.setOnClickListener(new clicker());
				roofcover.setOnClickListener(new clicker());
				roofdeck.setOnClickListener(new clicker());
				roofwall.setOnClickListener(new clicker());
				roofgeometry.setOnClickListener(new clicker());
				swr.setOnClickListener(new clicker());
				openprotection.setOnClickListener(new clicker());
				wallconstruction.setOnClickListener(new clicker());
			}
			if (this.app == 17 || this.app == 18)
			{
				
				layoutInflater.inflate(R.layout.photossubmenu, this, true);
				signrel = (RelativeLayout) findViewById(R.id.relativeLayout4);
				signatureimg = (Button) findViewById(R.id.signature);
				elevationimg = (Button) findViewById(R.id.elevation);
				
				
				signatureimg.setOnClickListener(new clicker());
				elevationimg.setOnClickListener(new clicker());
				
			}
			if (this.app == 29 || this.app == 30)
			{
				
				layoutInflater.inflate(R.layout.geninfosubmenu, this, true);
				genhazrel = (RelativeLayout) findViewById(R.id.relativeLayout4);
				genhazinfo = (Button) findViewById(R.id.genhazinfo);
				genhazimg = (Button) findViewById(R.id.genhazimages);
				
				
				genhazinfo.setOnClickListener(new clicker());
				genhazimg.setOnClickListener(new clicker());
				
			}

		}
		/** menu **/
		else {
			layoutInflater.inflate(R.layout.menuclick, this, true);
			img1 = (ImageView) findViewById(R.id.img01);
			img2 = (ImageView) findViewById(R.id.img02);
			img3 = (ImageView) findViewById(R.id.img03);
			img4 = (ImageView) findViewById(R.id.img04);
			img6 = (ImageView) findViewById(R.id.img06);
			img7 = (ImageView) findViewById(R.id.img07);
			img8 = (ImageView) findViewById(R.id.img08);
			img9 = (ImageView) findViewById(R.id.img09);
			btn_home = (ImageView) findViewById(R.id.home);
			
			takephoto = (ImageView) findViewById(R.id.head_take_image);
			inspinfo = (ImageView) findViewById(R.id.head_insp_info);
			policyinfo = (ImageView) findViewById(R.id.head_policy_info);

			img1.setOnClickListener(new clicker());
			img2.setOnClickListener(new clicker());
			img3.setOnClickListener(new clicker());
			img4.setOnClickListener(new clicker());
			img6.setOnClickListener(new clicker());
			img7.setOnClickListener(new clicker());
			img8.setOnClickListener(new clicker());
			img9.setOnClickListener(new clicker());
			btn_home.setOnClickListener(new clicker());
			takephoto.setOnClickListener(new clicker());
			inspinfo.setOnClickListener(new clicker());
			policyinfo.setOnClickListener(new clicker());
			
		}

		switch (this.app) {

		case 1:
			img1.setBackgroundResource(R.drawable.policyholderovr);
			break;
		case 2:
			img2.setBackgroundResource(R.drawable.questionsovr);
			break;
		case 3:
			img3.setBackgroundResource(R.drawable.imagesovr);
			takephoto.setVisibility(v1.GONE);
			break;
		case 4:
			img4.setBackgroundResource(R.drawable.feedbackovr);
			takephoto.setVisibility(v1.GONE);
			break;
		case 5:
			img6.setBackgroundResource(R.drawable.overallcommentsovr);
			break;
		case 6:
			img7.setBackgroundResource(R.drawable.generalconditionovr);
			takephoto.setVisibility(v1.GONE);
			break;
		case 7:
			img8.setBackgroundResource(R.drawable.mapovr);
			takephoto.setVisibility(v1.GONE);
			
			break;
		case 8:
			img9.setBackgroundResource(R.drawable.submitovr);
			takephoto.setVisibility(v1.GONE);
			break;
		case 11:
			poicyholdimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 12:
			agntimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 13:
			callimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 14:
			schimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		
		case 21:
			buildcode.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 22:
			roofcover.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 23:
			roofdeck.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 24:
			roofwall.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 25:
			roofgeometry.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 26:
			swr.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 27:
			openprotection.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 28:
			wallconstruction.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 17:
			signatureimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 18:
			elevationimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 29:
			genhazinfo.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;
		case 30:
			genhazimg.setBackgroundResource(R.drawable.submenusrepeatovr);
			break;

		}
	}

	class clicker implements OnClickListener {

		public void onClick(View v) {
			// TODO Auto-generated method stub

			switch (v.getId()) {
			case R.id.img01:
				Intent i = new Intent(context, PolicyholderInfo.class);
				i.putExtra("homeid", cf.Homeid);
				i.putExtra("status", cf.status);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
				((Activity)context).finish();
				break;
			case R.id.img02:
					Intent intque = new Intent(context, QuesBuildCode.class);
					intque.putExtra("homeid", cf.Homeid);
					intque.putExtra("status", cf.status);
					intque.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intque);
					((Activity)context).finish();
			break;
			case R.id.img03:
				
					Intent intimg = new Intent(context, Signature.class);
					intimg.putExtra("homeid", cf.Homeid);
					intimg.putExtra("status", cf.status);
					intimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intimg);
					((Activity)context).finish();
					break;
			case R.id.img04:
			
					Intent iInspectionList = new Intent(context,FeedbackDocument.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList.putExtra("status", cf.status);
					iInspectionList.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(iInspectionList);
					((Activity)context).finish();
					break;
			case R.id.img06:
				
					Intent fb = new Intent(context, OverallComments.class);
					fb.putExtra("homeid", cf.Homeid);
					fb.putExtra("status", cf.status);
					fb.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(fb);
					((Activity)context).finish();
				break;
			case R.id.img07:
				
					Intent genimg = new Intent(context, GeneralHazInfo.class);
					genimg.putExtra("homeid", cf.Homeid);
					genimg.putExtra("status", cf.status);
					genimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(genimg);
					((Activity)context).finish();
			break;
			case R.id.img08:
			
					Intent mapimg = new Intent(context, Maps.class);
					mapimg.putExtra("homeid", cf.Homeid);
					mapimg.putExtra("status", cf.status);
					mapimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(mapimg);
					((Activity)context).finish();
				break;
			case R.id.img09:
					Intent subimg = new Intent(context, Submit.class);
					subimg.putExtra("homeid", cf.Homeid);
					subimg.putExtra("status", cf.status);
					
					subimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(subimg);
					((Activity)context).finish();
								break;
		
			case R.id.policyhold:
				Intent ii = new Intent(context,PolicyholderInfo.class);
				ii.putExtra("homeid", cf.Homeid);
				ii.putExtra("status", cf.status);
				ii.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(ii);
				((Activity)context).finish();
				break;
		case R.id.build:
				Intent bcint = new Intent(context, QuesBuildCode.class);
				bcint.putExtra("homeid", cf.Homeid);
				bcint.putExtra("status", cf.status);
				bcint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(bcint);
				((Activity)context).finish();
				break;
			case R.id.roofcover:
				Intent rcint = new Intent(context, QuesRoofCover.class);
				rcint.putExtra("homeid", cf.Homeid);
				rcint.putExtra("status", cf.status);
				rcint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(rcint);
				((Activity)context).finish();
				break;
			case R.id.roofdeck:
				Intent rdint = new Intent(context, QuesRoofDeck.class);
				rdint.putExtra("homeid", cf.Homeid);
				rdint.putExtra("status", cf.status);
				rdint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(rdint);
				((Activity)context).finish();
				break;
			case R.id.roofwall:
				Intent rwint = new Intent(context, QuesRoofWall.class);
				rwint.putExtra("homeid", cf.Homeid);
				rwint.putExtra("status", cf.status);
				rwint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(rwint);
				((Activity)context).finish();
				break;
			case R.id.roofgeo:
				Intent rgint = new Intent(context, QuesRoofGeometry.class);
				rgint.putExtra("homeid", cf.Homeid);
				rgint.putExtra("status", cf.status);
				rgint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(rgint);
				((Activity)context).finish();
				break;
			case R.id.swr:
				Intent sint = new Intent(context, QuesSWR.class);
				sint.putExtra("homeid", cf.Homeid);
				sint.putExtra("status", cf.status);
				sint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(sint);
				((Activity)context).finish();
				break;
			case R.id.openpro:
				Intent oint = new Intent(context, QuesOpenProt.class);
				oint.putExtra("homeid", cf.Homeid);
				oint.putExtra("status", cf.status);
				oint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(oint);
				((Activity)context).finish();
				break;
			case R.id.wall:
				Intent wint = new Intent(context, QuesWallCons.class);
				wint.putExtra("homeid", cf.Homeid);
				wint.putExtra("status", cf.status);
				wint.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(wint);
				((Activity)context).finish();
				break;
			case R.id.signature:
				Intent intimg1 = new Intent(context, Signature.class);
				intimg1.putExtra("homeid", cf.Homeid);
				intimg1.putExtra("status", cf.status);
				intimg1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intimg1);
				((Activity)context).finish();
				break;
				
			case R.id.elevation:
				Intent photoimg = new Intent(context, Photos.class);
				photoimg.putExtra("homeid", cf.Homeid);
				photoimg.putExtra("status", cf.status);
				photoimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(photoimg);
				((Activity)context).finish();
				break;
			case R.id.genhazinfo:
				Intent genhzdinfo = new Intent(context, GeneralHazInfo.class);
				genhzdinfo.putExtra("homeid", cf.Homeid);
				genhzdinfo.putExtra("status", cf.status);
				genhzdinfo.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(genhzdinfo);
				((Activity)context).finish();
				break;	
			case R.id.genhazimages:
				Intent genhzdimg = new Intent(context, GeneralHazImage.class);
				genhzdimg.putExtra("homeid", cf.Homeid);
				genhzdimg.putExtra("status", cf.status);
				genhzdimg.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(genhzdimg);
				((Activity)context).finish();
				break;
			case R.id.head_take_image:
				Intent cam_info = new Intent(context,CameraImage.class);
				cam_info.putExtra("homeid", cf.Homeid);
				cam_info.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(cam_info);
				//((Activity)context).finish();
				break;
			case R.id.head_insp_info:
				db.getInspectorId();
				Intent insp_info = new Intent(context,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("homeid", cf.Homeid);
				insp_info.putExtra("insp_id", db.Insp_id);
				insp_info.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(insp_info);
				//((Activity)context).finish();
				break;
			case R.id.head_policy_info:
				db.getInspectorId();
				Intent policy_info = new Intent(context,PolicyholdeInfoHead.class);
				policy_info.putExtra("Type", "Policyholder");
				policy_info.putExtra("homeid", cf.Homeid);
				policy_info.putExtra("insp_id", db.Insp_id);
				policy_info.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(policy_info);
				//((Activity)context).finish();
				break;
			case R.id.home:		
				
				cf.gohome();				
				break;
			}
		}
	}
}
