package idsoft.inspectiondepot.windmitinspection;

import idsoft.inspectiondepot.windmitinspection.OrderInspection.mDateSetListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

public class QuesBuildCode extends Activity {
	String appdateprev, InspectionType, status,inspectortypeid, helpcontent,commdescrip = "",commentsfill,compchk = null, 
			yearofhome, country, conchkbox, comm2,newyearbuilt, descrip,homeId, yearbuilt,
			rdiochk, permitDate, comm, updatecnt, identity, buildingcomment;	
	int spinnerPosition, yearbuiltprev, buildcodevalueprev,value, Count, chkrws, optionid,viewimage = 1;
	Button saveclose, getdate1, getdate2;
	View v1;
	String b1_q1[] = { "Meets 2001 FBC", "Meets SFBC -94","Unknown - Does Not Meet" };
	EditText permitDate1, permitDate2, comments;
	Spinner yearbuilt1, yearbuilt2;boolean load_comment=true;
	TextView nocommentsdisp, txtheading,prevmitidata,helptxt,buildcode_TV_type1;
	RadioButton rdobutton1, rdobutton2, rdounknown;
	ImageView Vimage;	int yhme = 0;
	ArrayAdapter adapter,adapter2;
	RelativeLayout tblcommentschange;
	LinearLayout lincomments;
	CheckBox[] cb;
	TextView buildcode_type1;
	CheckBox temp_st;	
	String[] arrcomment1;
	int commentsch;
	AlertDialog alertDialog;
	protected static final int DATE_DIALOG_ID = 0;
	private static final int visibility = 0;
	private TableLayout tbllayout8, exceedslimit1;
	ListView list;
	String yeara[] = { "Select", "2001", "2002", "2003", "2004", "2005",
			"2006", "2007", "2008", "2009", "2010", "2011", "2012","2013" };
	
	String yearb[] = { "Select", "1994", "1995", "1996", "1997", "1998", "1999", "2000",
			"2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008",
			"2009", "2010", "2011", "2012","2013" };
	String tmp="";
	Map<String, CheckBox> map1 = new LinkedHashMap<String, CheckBox>();	
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		db.CreateTable(2);
		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		
			Bundle bunhomeId = getIntent().getExtras();
			if (bunhomeId != null) {
				cf.getExtras(bunhomeId);
			}System.out.println("Status=buildingcode"+cf.status);
			setContentView(R.layout.buildcode);
			db.getInspectorId();
			db.getPHinformation(cf.Homeid);
			db.changeimage(cf.Homeid);
			cf.getDeviceDimensions(); 
			/** menu **/
			LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
			layout.addView(new MyOnclickListener(QuesBuildCode.this, 2, cf, 0));		
			layout.setMinimumWidth(cf.wd);
			
			LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);/** Questions submenu **/
			sublayout.addView(new MyOnclickListener(QuesBuildCode.this,21, cf, 1));
			
			Declaration();
		
			setvalue();
	}
	private void setvalue() {
		// TODO Auto-generated method stub
		try
		{
				db.getPHinformation(cf.Homeid);
				if (!db.yearbuilt.equals("")) 
				{
					try {
						yhme = Integer.parseInt(db.yearbuilt);
					} catch (Exception e) {
						System.out.println("yhmeerror"+e.getMessage());
						yhme = 0;
					}
				}System.out.println("ph_county"+db.ph_county);
				if (db.ph_county.toLowerCase().equals("miami-dade") || db.ph_county.toLowerCase().equals("broward")) 
				{
					Cursor c3 = db.SelectTablefunction(db.Questions," where SRID='" + cf.Homeid + "'");
					if (c3.getCount() == 0) 
					{
						rdiochk = "2";
						rdobutton2.setChecked(true);
						getdate2.setEnabled(true);
						yearbuilt2.setEnabled(true);
						spinnerPosition = adapter2.getPosition(db.yearbuilt);
						yearbuilt2.setSelection(spinnerPosition);
						commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
						comments.setText(commentsfill);
					} 
					else 
					{
						c3.moveToFirst();
							if (c3.getString(c3.getColumnIndex("BuildingCodeValue")).equals("0")) 
							{
								rdiochk = "2";
								rdobutton2.setChecked(true);getdate2.setEnabled(true);
								yearbuilt2.setEnabled(true);
								spinnerPosition = adapter2.getPosition(db.yearbuilt);
								yearbuilt2.setSelection(spinnerPosition);
								commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
								comments.setText(commentsfill);
							}
					}
		
				}
				else
				{
					Cursor c3 = db.SelectTablefunction(db.Questions," where SRID='" + cf.Homeid + "'");
					System.out.println("Cunt"+c3.getCount());
					if (c3.getCount() == 0) 
					{
						if(yhme>=1994)
						{
							rdiochk = "1";
							rdobutton1.setChecked(true);
							getdate1.setEnabled(true);
							yearbuilt1.setEnabled(true);
							spinnerPosition = adapter.getPosition(db.yearbuilt);
							yearbuilt1.setSelection(spinnerPosition);
							commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.";
							comments.setText(commentsfill);
						}
					} 
					else 
					{						
						c3.moveToFirst();
						System.out.println("yhme"+yhme);
							if (c3.getString(c3.getColumnIndex("BuildingCodeValue")).equals("0") && yhme>=1994) 
							{
								rdiochk = "1";
								rdobutton1.setChecked(true);
								getdate1.setEnabled(true);
								yearbuilt1.setEnabled(true);
								spinnerPosition = adapter.getPosition(db.yearbuilt);
								yearbuilt1.setSelection(spinnerPosition);
								commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.";
								comments.setText(commentsfill);
							}
					}					
				}
				
			
					if (yhme < 1994 && yhme != 0) {
						rdiochk = "3";
						rdobutton2.setChecked(false);
						rdounknown.setChecked(true);
						commentsfill = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
						comments.setText(commentsfill);
					}
			
				    
					Cursor c2 = db.wind_db.rawQuery("SELECT BuildingCodeYearBuilt,BuildingCodeValue,BuildingCodePerApplnDate FROM "+ db.Questions + "" +
							" WHERE SRID='"+ cf.Homeid + "'", null);
					fetchdata();
					
					db.getQuesComments(cf.Homeid);
					if(!db.bccomments.equals(""))
					{
						comments.setText(db.bccomments);
					}
		} 
		catch (Exception e) 
		{

		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null)
				{	cf.alertcontent = "To see help comments, please select Building Code options.";}
				else if (rdiochk.equals("1") || rdobutton1.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.";
				} else if (rdiochk.equals("2") || rdobutton2.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.";
				} else if (rdiochk.equals("3") || rdounknown.isChecked()) {
					cf.alertcontent = "This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.";
				} else {
					cf.alertcontent = "To see help comments, please select Building Code options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
		txtheading = (TextView) findViewById(R.id.txtquestion1heading);
		txtheading.setText(Html.fromHtml("<font color=red> * "
								+ "</font>Was the structure built in compliance with the Florida Building Code (FBC 2001 or later) OR for homes located in the HVHZ (Miami-Dade or Broward counties), South Florida Building Code (SFBC-94)? "));
		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
	
		rdobutton1 = (RadioButton) this.findViewById(R.id.rdobtn1);this.rdobutton1.setOnClickListener(OnClickListener);
		rdobutton2 = (RadioButton) this.findViewById(R.id.rdobtn2);this.rdobutton2.setOnClickListener(OnClickListener);
		rdounknown = (RadioButton) this.findViewById(R.id.rdounknown);this.rdounknown.setOnClickListener(OnClickListener);
		
		yearbuilt1 = (Spinner) this.findViewById(R.id.txtyrbuilt1);
		adapter = new ArrayAdapter(QuesBuildCode.this,android.R.layout.simple_spinner_item, yeara);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearbuilt1.setAdapter(adapter);
		yearbuilt1.setEnabled(false);

		yearbuilt2 = (Spinner) this.findViewById(R.id.txtyrbuilt2);
		adapter2 = new ArrayAdapter(QuesBuildCode.this,android.R.layout.simple_spinner_item, yearb);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearbuilt2.setAdapter(adapter2);
		yearbuilt2.setEnabled(false);

		permitDate1 = (EditText) this.findViewById(R.id.txtpermitDate1);
		permitDate2 = (EditText) this.findViewById(R.id.txtpermitDate2);
		getdate1 = (Button) findViewById(R.id.Calcicon1);
		getdate2 = (Button) findViewById(R.id.Calcicon2);
		
		buildcode_type1=(TextView) findViewById(R.id.SH_TV_ED);

		comments = (EditText) this.findViewById(R.id.txtcomments);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,buildcode_type1));
		saveclose = (Button) findViewById(R.id.savenext);

		
		db.getQuesOriginal(cf.Homeid);
		if(!db.bcoriddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.bcoriddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}

	}
			
	private void fetchdata() {
		// TODO Auto-generated method stub
		System.out.println("fetchdata");
		try {
			Cursor cur = db.SelectTablefunction(db.Questions,
					"where SRID='" + cf.Homeid + "'");
			System.out.println("cur"+cur.getCount());
			if(cur.getCount()>0)
			{
			cur.moveToFirst();
			if (cur != null) {
								
				yearbuiltprev = cur.getInt(cur
						.getColumnIndex("BuildingCodeYearBuilt"));
				buildcodevalueprev = cur.getInt(cur
						.getColumnIndex("BuildingCodeValue"));
				appdateprev = db.decode(cur.getString(cur
						.getColumnIndex("BuildingCodePerApplnDate")));
				newyearbuilt = db.decode(cur.getString(cur
						.getColumnIndex("BuildingCodeYearBuilt")));
				System.out.println("buildcodevalueprevinside"+buildcodevalueprev);
				if (buildcodevalueprev == 1) {
					rdiochk = "1";
					yearbuilt2.setEnabled(false);
					rdounknown.setChecked(false);
					rdobutton1.setChecked(true);
					yearbuilt1.setEnabled(true);
					getdate1.setEnabled(true);
					getdate2.setEnabled(false);
					System.out.println("newyearbuilt="+newyearbuilt);
					
					/*int sel = 0;
					if (yearbuiltprev >= 2001
							&& yearbuiltprev <= cf.mYear) {
						for (int k = 1, i = 2001; i <= yearbuiltprev; i++, k++) {
                                sel = k;
						}
					}*/
					
					System.out.println("yearbuiltprev="+yearbuiltprev);
					yearbuilt1.setSelection(adapter.getPosition(newyearbuilt));
					yearbuilt = newyearbuilt;
					permitDate = appdateprev;
					permitDate1.setText("" + appdateprev);
				} else if (buildcodevalueprev == 2) {
					rdiochk = "2";
					rdobutton2.setChecked(true);
					yearbuilt2.setEnabled(true);
					rdounknown.setChecked(false);
					yearbuilt1.setEnabled(false);
					getdate2.setEnabled(true);
					getdate1.setEnabled(false);
					int sel = 0;
					
					/*if (yearbuiltprev >= 1994 && yearbuiltprev <= cf.mYear) {
						for (int k = 1, i = 1994; i <= yearbuiltprev; i++, k++) {

							sel = k;

						}
					}*/
					yearbuilt2.setSelection(adapter2.getPosition(newyearbuilt));
					permitDate2.setText("" + appdateprev);
					yearbuilt = newyearbuilt;
					permitDate = appdateprev;

				} else if (buildcodevalueprev == 3) {
					
					
					rdobutton1.setChecked(false);
					rdobutton2.setChecked(false);
					yearbuilt2.setEnabled(false);
					yearbuilt1.setEnabled(false);
					getdate1.setEnabled(false);getdate2.setEnabled(false);
					rdiochk = "3";
					rdounknown.setChecked(true);
				}
			}
			}
		} catch (Exception e) {
			System.out.println("eee"+e.getMessage());
	  }
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() 
{
		public void onClick(View v) {
			try {
				switch (v.getId()) {
				case R.id.rdobtn1:
					rdiochk="1";
					comments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection A, Question 1 Building Code.");
					showhideoption();
					rdobutton1.setChecked(true);
					permitDate1.setEnabled(true);permitDate1.setCursorVisible(false);
					yearbuilt1.setEnabled(true);yearbuilt1.requestFocus();
					((Button)findViewById(R.id.Calcicon1)).setEnabled(true);					
					break;

				case R.id.rdobtn2:
					rdiochk="2";
					comments.setText("This home was verified as meeting the requirements of the OIR B1- 1802, Selection B, Question 1, South Florida Building Code, 1994.");
					showhideoption();
					rdobutton2.setChecked(true);
					permitDate2.setEnabled(true);permitDate2.setCursorVisible(false);
					yearbuilt2.setEnabled(true);yearbuilt2.requestFocus();
					((Button)findViewById(R.id.Calcicon2)).setEnabled(true);					
					break;

				case R.id.rdounknown:					
					rdiochk="3";
					comments.setText("This home was verified as NOT meeting the requirements of the OIR B1-1802, Question 1, selection A or B.");
					showhideoption();
					rdounknown.setChecked(true);
					break;
				}
			} catch (Exception e) {
				System.out.println("erro in on click " + e);
			}
		}
	};
	public void showhideoption() /** TO HIDE CONTROLS WHEN CLICKED ON RADIO OPTION**/
	{
		rdobutton1.setChecked(false);
		rdobutton2.setChecked(false);
		rdounknown.setChecked(false);
		permitDate1.setEnabled(false);permitDate1.setText("");
		permitDate2.setEnabled(false);permitDate2.setText("");
		yearbuilt1.setEnabled(false);yearbuilt1.setSelection(0);
		yearbuilt2.setEnabled(false);yearbuilt2.setSelection(0);
		((Button)findViewById(R.id.Calcicon1)).setEnabled(false);
		((Button)findViewById(R.id.Calcicon2)).setEnabled(false);
	}


	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			BC_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				System.out.println("tesload");
					if(load_comment)
					{
						load_comment=false;
						int loc1[] = new int[2];
						v.getLocationOnScreen(loc1);
						Intent in = cf.loadComments(tmp,loc1);
						in.putExtra("insp_ques", "1");
						in.putExtra("length", len);
						in.putExtra("max_length", 500);
						startActivityForResult(in, cf.loadcomment_code);
					
					}
			}
			else
			{
				cf.ShowToast("Please select the option for Building Code.");	
			}			
			break;
		case R.id.Calcicon1:
			showDialogDate(permitDate1);
			System.out.println("ccccaalicon");
			/*cf.getCalender();
			showDialog(DATE_DIALOG_ID);System.out.println("showDialog");*/
			break;
			
		case R.id.Calcicon2:
			showDialogDate(permitDate2);
			/*cf.getCalender();
			showDialog(DATE_DIALOG_ID);*/
			break;	
		
		case R.id.txthelpcontentoptionA:
			
			cf.alerttitle="A - Meets 2001 FBC";
		    cf.alertcontent="Built in compliance with the FBC: year built. For homes built in 2002 or 2003, provide a permit application with a date after 3/1/2002: building permit application date (mm/dd/yyyy).";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionB:
			cf.alerttitle="B - Meets SFBC-94";
		    cf.alertcontent="For the HVHZ Only: Built in compliance with the SFBC-94: year built. For homes built in 1994, 1995 or 1996, provide a permit application with a date after 9/1/1994: buildings permit application date (mm/dd/yyyy).";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.txthelpcontentoptionC:
			cf.alerttitle="C - Unknown - Does Not Meet";
		    cf.alertcontent="Unknown or does not meet the requirements of answer A or B.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:
			comm = comments.getText().toString();
			if (rdobutton1.isChecked()) {
				yearbuilt = yearbuilt1.getSelectedItem().toString();
				permitDate = permitDate1.getText().toString();
			} else if (rdobutton2.isChecked()) {
				yearbuilt = yearbuilt2.getSelectedItem().toString();
				permitDate = permitDate2.getText().toString();
			} else if (rdounknown.isChecked()) {
				yearbuilt = "";
				permitDate = "";
			} else {
				cf.ShowToast("Please select the Building Code Option");
			}
			if (rdobutton1.isChecked() == true
					|| rdobutton2.isChecked() == true) {
				if (yearbuilt.equals("") || yearbuilt.equals("Select")) {
					cf.ShowToast("Please select Year Built");

					if (rdobutton1.isChecked()) {
						yearbuilt1.setFocusableInTouchMode(true);
						yearbuilt1.requestFocus();
					} else if (rdobutton2.isChecked()) {
						yearbuilt2.setFocusableInTouchMode(true);
						yearbuilt2.requestFocus();
					}
				} else if (comm.trim().equals("")) {
					cf.ShowToast("Please enter the comments for Building Code");

					comments.requestFocus();
				} else {

					if (yearvalidation(yearbuilt) == "true") {

						if ((yearbuilt.equals("2002") || yearbuilt
								.equals("2003"))
								|| ((yearbuilt.equals("1994")
										|| yearbuilt.equals("1995") || yearbuilt
											.equals("1996")))) {
							if (permitDate.equals("")) {
								cf.ShowToast("Please select the Permit Application Date");

								if (rdobutton1.isChecked()) {
									permitDate1.setFocusableInTouchMode(true);
									permitDate1.requestFocus();
								} else if (rdobutton2.isChecked()) {
									permitDate2.setFocusableInTouchMode(true);
									permitDate2.requestFocus();
								}
							} else {
								if (checkforpermitdateistoday(permitDate) == "true") {
									if (compareyearbuiltandpermidate(yearbuilt,
											permitDate) == "true") {
										insertdata();
									}

									else if (checkforpermitdateistoday(permitDate) == "false") {
										if (rdobutton1.isChecked()) {
											if (yearbuilt.equals("2002")
													|| yearbuilt.equals("2003")) {
												cf.ShowToast("Please enter Permit Application Date after(3/1/2002)");

											} else {
												cf.ShowToast("The Permit Application Date should not be greater than Year Built");

											}
											permitDate1.setText("");
											permitDate1.requestFocus();
										} else if (rdobutton2.isChecked()) {
											if (yearbuilt.equals("1994")
													|| yearbuilt.equals("1995")
													|| yearbuilt.equals("1996")) {
												cf.ShowToast("Please enter Permit Application Date after(9/1/1994)");

											} else {
												cf.ShowToast("The Permit Application Date should not be greater than Year Built");

											}
											permitDate2.setText("");
											permitDate2.requestFocus();
										}
									}

								} else {
									cf.ShowToast("The Permit Application Date should not be greater than Today's Date");

									if (rdobutton1.isChecked()) {
										permitDate1.setText("");
										permitDate1.requestFocus();
									} else if (rdobutton2.isChecked()) {
										permitDate2.setText("");
										permitDate2.requestFocus();
									}
								}
							}
						} else {
							if (permitDate.equals("")) {
								insertdata();
							} else {
								if (checkforpermitdateistoday(permitDate) == "true") {
									if (checkpermitdategraterthanbuild(
											yearbuilt, permitDate) == true) {
										if (compareyearbuiltandpermidate(
												yearbuilt, permitDate) == "true") {
											insertdata();
										}
									} else {
										if (rdobutton1.isChecked()) {
											permitDate1.setText("");
											permitDate1.requestFocus();
										} else if (rdobutton2.isChecked()) {
											permitDate2.setText("");
											permitDate2.requestFocus();
										}
									}
								} else {
									cf.ShowToast("The Permit Application Date should not be greater than Today's Date");
									if (rdobutton1.isChecked()) {
										permitDate1.setText("");
										permitDate1.requestFocus();
									} else if (rdobutton2.isChecked()) {
										permitDate2.setText("");
										permitDate2.requestFocus();
									}
								}
							}
						}

					} else {
						if (rdobutton1.isChecked()) {
							yearbuilt1.setSelection(0);
							yearbuilt1.requestFocus();
							cf.ShowToast("Please select the Year Built greater than 2000");

						} else if (rdobutton2.isChecked()) {
							yearbuilt2.setSelection(0);
							yearbuilt2.requestFocus();
							cf.ShowToast("Please select the Year Built greater than 1994");

						}
					}

				}
			} else if (rdounknown.isChecked() == true) {
				if (comm.equals("")) {
					cf.ShowToast("Please enter the comments for Building Code");
					comments.requestFocus();
				} else {
					insertdata();
				}
			}
			break;

		}
	}
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(QuesBuildCode.this,new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());
		}
	}
	private void BC_suboption() {
		// TODO Auto-generated method stub
		if(rdobutton1.isChecked())	{		tmp="1";}
		else if(rdobutton2.isChecked()) {	tmp="2";}
		else if(rdounknown.isChecked()) {	tmp="3";}
		else{tmp="";}
	}
	private boolean checkpermitdategraterthanbuild(String yearbuilt3,
			String permitDate3) {
		// TODO Auto-generated method stub
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt3 = yearbuilt3.trim();
		int yr = Integer.parseInt(yearbuilt3);
		if (yr < j) {
			cf.ShowToast("The Permit Application Date should not be greater than the Year Built");
			return false;
		} else {
			return true;
		}
	}

	private String compareyearbuiltandpermidate(String yearbuilt4,
			String permitDate4) {
		// TODO Auto-generated method stub
		int i1 = permitDate4.indexOf("/");
		String result = permitDate4.substring(0, i1);
		int i2 = permitDate4.lastIndexOf("/");
		String result1 = permitDate4.substring(i1 + 1, i2);
		String result2 = permitDate4.substring(i2 + 1);
		result2 = result2.trim();
		int j = Integer.parseInt(result2);
		final int j1 = Integer.parseInt(result1);
		final int j2 = Integer.parseInt(result);
		yearbuilt4 = yearbuilt4.trim();
		int yr = Integer.parseInt(yearbuilt4);
		if (rdobutton1.isChecked() == true) {

			if (yearbuilt4.equals("2002") || yearbuilt4.equals("2003")) {
				if ((j >= 2002) && (j2 >= 3 || j > 2002)
						&& (j1 >= 2 || j > 2002 || j2 > 3)) {
					if (j < 2002) {
						compchk = "true";
					} else if (j == yr || j + 1 == yr) {
						compchk = "false";
						AlertDialog.Builder builder = new AlertDialog.Builder(
								QuesBuildCode.this);
						builder.setTitle("Select")
								.setMessage(
										"The Year Built and Permit Application Date are outside of typical values,Please confirm this is correct?")
								.setCancelable(false)
								.setPositiveButton("Ok",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												if (j2 >= 3) {
													if (j1 > 1) {

														chkupdate();

													} else {

														chkupdate1();

													}
												} else {

													chkupdate1();

												}

											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												permitDate1.setText("");
											}

											private void chkupdate() {
												// TODO Auto-generated method
												// stub
												try

												{
													insertdata();
												} catch (Exception e) {
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												}
											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate1();
											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												permitDate1.setText("");
												permitDate1.requestFocus();
											}
										});

						builder.show();

					} else if (j > yr) {
						compchk = "false";
						cf.ShowToast("The Permit Application Date Should not be greater than the Year Built");
						permitDate1.setText("");
						permitDate1.requestFocus();
					}

				} else {
					compchk = "false";
					cf.ShowToast("The Permit Application Date should be greater than(03/01/2002) for the Year Built(2002 or 2003).");
					permitDate1.setText("");
					permitDate1.requestFocus();

				}
			} else {
				if (j + 1 == yr || j == yr) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							QuesBuildCode.this);
					builder.setTitle("Select")
							.setMessage(
									"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate();
										}

										private void chkupdate() {
											// TODO Auto-generated method stub
											compchk = "true";
											try

											{
												insertdata();
											} catch (Exception e) {
												//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocode data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												
											}
										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate1();
										}

										private void chkupdate1() {
											// TODO Auto-generated method stub
											compchk = "false";
											permitDate1.setText("");
											permitDate1.requestFocus();
										}
									});

					builder.show();

				} else if (j < yr) {
					compchk = "true";
				} else if (j > yr) {

					compchk = "false";
					cf.ShowToast("The Permit Application Date should not be greater than the Year Built.");
					permitDate1.setText("");
					permitDate1.requestFocus();
				}

			}

		} else if (rdobutton2.isChecked() == true) {
			if (yearbuilt4.equals("1994") || yearbuilt4.equals("1995")
					|| yearbuilt4.equals("1996")) {
				if (j <= yr) {
					if ((j >= 1994) && (j2 >= 9 || j > 1994)
							&& (j1 > 1 || j > 1994 || j2 > 9)) {
						if (j == yr || j + 1 == yr) {
							compchk = "false";
							AlertDialog.Builder builder = new AlertDialog.Builder(
									QuesBuildCode.this);
							builder.setTitle("Select")
									.setMessage(
											"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
									.setCancelable(false)
									.setPositiveButton(
											"Ok",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													chkupdate();
												}

												private void chkupdate() {
													// TODO Auto-generated
													// method stub
													compchk = "true";
													try

													{
														insertdata();
													} catch (Exception e) {
														//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
														
													}
												}
											})
									.setNegativeButton(
											"Cancel",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													chkupdate1();
												}

												private void chkupdate1() {
													// TODO Auto-generated
													// method stub
													compchk = "false";
													permitDate2.setText("");
													permitDate2.requestFocus();
												}
											});

							builder.show();
						} else {
							compchk = "true";
						}
					} else {
						cf.ShowToast("The Permit Application Date should be greater than(09/01/1994) for the Year Built(1994 to 1996)");
						permitDate2.setText("");
						permitDate2.requestFocus();
						compchk = "false";
					}
				} else if (j > yr) {
					cf.ShowToast("The Permit Application Date should not be greater than the Year Built");
					permitDate2.setText("");
					permitDate2.requestFocus();
					compchk = "false";
				} else if (j == yr || j + 1 == yr) {
					compchk = "false";
					AlertDialog.Builder builder = new AlertDialog.Builder(
							QuesBuildCode.this);
					builder.setTitle("Select")
							.setMessage(
									"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
							.setCancelable(false)
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate();
										}

										private void chkupdate() {
											// TODO Auto-generated method stub
											compchk = "true";
											try

											{
												insertdata();
											} catch (Exception e) {
												//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in inserting or updating buildocde datas on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
												
											}
										}
									})
							.setNegativeButton("Cancel",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											chkupdate1();

										}

										private void chkupdate1() {
											// TODO Auto-generated method stub
											compchk = "false";
											permitDate2.setText("");
											permitDate2.requestFocus();
										}
									});

					builder.show();

				} else {
					cf.ShowToast("The Permit Application Date should be greater than(09/01/1994) for the Year Built(1994 to 1996)");
					permitDate2.setText("");
					permitDate2.requestFocus();
					compchk = "false";
				}
			} else {
				if (j <= yr) {
					if (j == yr || j + 1 == yr) {
						compchk = "false";
						AlertDialog.Builder builder = new AlertDialog.Builder(
								QuesBuildCode.this);
						builder.setTitle("Select")
								.setMessage(
										"The Year Built and Permit Application Date are outside of Typical values, Please confirm this is correct?.")
								.setCancelable(false)
								.setPositiveButton("Ok",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate();
											}

											private void chkupdate() {
												// TODO Auto-generated method
												// stub
												compchk = "true";
												try

												{
													insertdata();
												} catch (Exception e) {
													//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ BuildCode.this +" in inserting or updating buildocde data on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
													
												}
											}
										})
								.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												chkupdate1();
											}

											private void chkupdate1() {
												// TODO Auto-generated method
												// stub
												compchk = "false";
												permitDate2.setText("");
												permitDate2.requestFocus();
											}
										});

						builder.show();

					} else {
						compchk = "true";
					}
				} else {
					cf.ShowToast("The Permit Application Date should not be greater than the Year Built");
					compchk = "false";
				}
			}
		}
		return compchk;
	}

	private String checkforpermitdateistoday(String permitDate3) {
		String chkdate = null;
		int i1 = permitDate3.indexOf("/");
		String result = permitDate3.substring(0, i1);
		int i2 = permitDate3.lastIndexOf("/");
		String result1 = permitDate3.substring(i1 + 1, i2);
		String result2 = permitDate3.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;
		if (j < thsyr) {
			chkdate = "true";
		} else if (j == thsyr) {
			if (j1 <= curmnth && j2 <= curdate) {
				chkdate = "true";
			} else {
				chkdate = "false";
			}
		}

		return chkdate;

	}

	private String yearvalidation(String yearbuilt3) {
		// TODO Auto-generated method stub
		cf.getCalender();
		int year = Integer.parseInt(yearbuilt3);
		String chk = null;
		if (rdobutton1.isChecked() == true) {
			if (year > 2000 && year <= cf.mYear) {
				chk = "true";
			} else {
				chk = "false";
			}
		} else if (rdobutton2.isChecked() == true) {
			if (year > 1993 && year <= cf.mYear) {
				chk = "true";
			} else {
				chk = "false";
			}
		}
		return chk;
	}

	private void insertdata() {
		// TODO Auto-generated method stub
		try {
			Cursor c2 = db.SelectTablefunction(db.Questions,
					"where SRID='" + cf.Homeid + "'");
			
			System.out.println("yearbuilt "+yearbuilt+ " permitDate "+permitDate+ " rdiochk "+rdiochk);
			if (c2.getCount() == 0) {
				db.wind_db.execSQL("INSERT INTO "
						+ db.Questions
						+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
						+ "VALUES ('" + cf.Homeid + "','" + yearbuilt + "','"
						+ permitDate + "','" + rdiochk + "','" + 0 + "','" + 0
						+ "','','','','','" + 0 + "','" + 0 + "','','" + 0
						+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
						+ "','','" + 0 + "','" + 0 + "','','','','','','','','"
						+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
						+ "','" + 0 + "','','" + 0 + "','','','" + cf.datewithtime + "','"
						+ 0 + "')");
			} else {
				db.wind_db.execSQL("UPDATE " + db.Questions
						+ " SET BuildingCodeYearBuilt='" + yearbuilt
						+ "',BuildingCodePerApplnDate ='" + permitDate
						+ "',BuildingCodeValue='" + rdiochk + "',ModifyDate='"
						+ cf.datewithtime + "' WHERE SRID ='" + cf.Homeid.toString() + "'");
			}

			c2 = db.SelectTablefunction(db.QuestionsComments, "where SRID='"
					+ cf.Homeid + "'");
			if (c2.getCount() == 0) {
				db.wind_db.execSQL("INSERT INTO "
						+ db.QuestionsComments
						+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
						+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','"+ db.encode(comments.getText().toString())+ "','','','','','','','','','"+ cf.datewithtime + "')");
			} else
			{
				db.wind_db.execSQL("UPDATE " + db.QuestionsComments
						+ " SET BuildingCodeComment='"+db.encode(comments.getText().toString())+ "',CreatedOn ='" + cf.datewithtime + "'" + " WHERE SRID ='" + cf.Homeid.toString() + "'");
			}
			Cursor c3 = db.SelectTablefunction(db.SubmitCheckTable, "where Srid='" + cf.Homeid + "'");
			if (c3.getCount() == 0) {
				db.wind_db.execSQL("INSERT INTO "
						+ db.SubmitCheckTable
						+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
						+ "VALUES ('" + db.Insp_id + "','" + cf.Homeid
						+ "',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0)");
			} else {
				db.wind_db.execSQL("UPDATE " +  db.SubmitCheckTable
						+ " SET fld_builcode='1' WHERE Srid ='" + cf.Homeid
						+ "' and InspectorId='" + db.Insp_id + "'");
			}
			updatecnt = "1";
		} catch (Exception e) {
			updatecnt = "0";
			cf.ShowToast("There is a problem in saving your data due to invalid character");
		}

		if (updatecnt == "1") {
			cf.ShowToast("Building Code Details saved successfully");

			Intent iInspectionList = new Intent(QuesBuildCode.this, QuesRoofCover.class);
			iInspectionList.putExtra("homeid", cf.Homeid);
			iInspectionList.putExtra("status", cf.status);
			
			startActivity(iInspectionList);
			finish();
		}
	}
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesBuildCode.this, PolicyholderInfo.class);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	 /**on activity result for the load comments**/
	 @Override
	 		protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	 		try
	 		{
	 			if(requestCode==cf.loadcomment_code)
	 			{
	 				load_comment=true;
	 				if(resultCode==RESULT_OK)
	 				{
	 					String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
	 					((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
	 				}
	 			}
	 		}
	 		catch (Exception e) {
	 			// TODO: handle exception
	 			System.out.println("the erro was "+e.getMessage());
	 		}
	 	}/**on activity result for the load comments ends**/
}