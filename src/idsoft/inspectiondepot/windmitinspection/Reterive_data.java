package idsoft.inspectiondepot.windmitinspection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;




public class Reterive_data extends Activity {
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	private int vcode,usercheck,mState,delay=40,RUNNING=1;
	double total;
	private ProgressDialog progDialog;
	String progress_text="Data";
	PowerManager.WakeLock wl=null;
	View v1;
	public ProgressThread progThread;
	String FILENAME1="",FILENAME="",path="",path1="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 progDialog = new ProgressDialog(this);
		 wb=new WebserviceFunctions(this);
		 db=new DatabaseFunctions(this);
		 cf=new CommonFunctions(this);
		 total = 0;
		showDialog(1);
		total=5;
		Bundle extras = getIntent().getExtras();
		   if (extras != null) {
			   cf.Homeid=extras.getString("homeid");		
		   }System.out.println("Homm="+cf.Homeid);
		start_import();
	}
	private void start_import() {
		// TODO Auto-generated method 
		 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
		 wl.acquire();
		 new Thread() {
				public void run() {
					System.out.println("comes in the correct way"+cf.Homeid);
					try {
					
					total=5;
					 progress_text="Policy Holder details.";
					 getpolicyholder_info();
					 total=15;					
					 progress_text="Questions.";
					 GetQuestions();
					 total=30;
					progress_text="Image.";
					 Getimage();
					 total=60;
					  progress_text="Feedback information.";
					 Getfeedback();
					 total=75;
					 progress_text="General information.";
					 GetGCH();
					usercheck = 4;
					 total = 100;
					 handler.sendEmptyMessage(0);
					 
				}
				catch (SocketTimeoutException s) {
					System.out.println("s "+s.getMessage());
					    usercheck = 2;
					    total = 100;
					    handler.sendEmptyMessage(0);
						
				 } catch (NetworkErrorException n) {
					 System.out.println("n "+n.getMessage());
					    usercheck = 2;
					    total = 100;
					    handler.sendEmptyMessage(0);
					   
				 } catch (IOException io) {
					 System.out.println("io "+io.getMessage());
					    usercheck = 2;
					    total = 100;
					    handler.sendEmptyMessage(0);
					    
				 } catch (XmlPullParserException x) {
					 System.out.println("x "+x.getMessage());
					 	usercheck = 2;
					 	total = 100;
					 	handler.sendEmptyMessage(0);
					    
				 }
					 catch (Exception e) {
						 System.out.println("e "+e.getMessage());
						 	usercheck = 2;
						 	total = 100;
						 	handler.sendEmptyMessage(0);
						 System.out.println("commeonexcep"+e.getMessage());
					}
				}

				

				

				
		 }
		 .start();
	}
	 
	 protected void GetGCH() {
		// TODO Auto-generated method stub
		 String insuredothertxt="",observothertxt="",occupancyothertxt="",InsuredIs,occupancytype,observtype,notdet,
				 occflg,stroccup,vacflg,strvacant,balconypres,permitconf,bsize,addistru,otherloca,loc1,loc2,loc3,loc4,obs1,obs2,obs3,obs4,obs5,
				 strpf1,strpf2,strpf3,strpf4,strpp1,strpp2,strpp3,strpp4,strpp5,strpp6,strpp7,strhz1, strhz2, strhz3, strhz4, strhz5, 
				 strhz6, strhz7, strhz8,gencomments,
					strhz9, strhz10, strhz11, strhz12, strhz13, strhz14, strhz15,
					strhz16, strhz17, strhz18, strhz19, strhz20, strhz21, strhz22,
					strhz23, strhz24, strhz25;
			try{
				SoapObject request = new SoapObject(wb.NAMESPACE, "ImportGenHzdData");
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
				envelope.dotNet = true;
				request.addProperty("InspectorID", db.Insp_id);
				request.addProperty("Srid", cf.Homeid);
				envelope.setOutputSoapObject(request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);System.out.println("requestGCH"+request);

				androidHttpTransport.call(wb.NAMESPACE+"ImportGenHzdData", envelope);
				SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("resultGCH"+result1);
				int Cnt;
				db.CreateTable(15);
				 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
				 {
						Cnt=0;total = 15;
				 }
				 else
				 {
					 InsuredIs  =String.valueOf(result1.getProperty("InsuredIs"));
					 insuredothertxt = String.valueOf(result1.getProperty("OtherInsured"));
					 occupancytype = String.valueOf(result1.getProperty("OccupancyType"));
					 occupancyothertxt = String.valueOf(result1.getProperty("OtherOccupancy"));
					 observtype = String.valueOf(result1.getProperty("ObservationType"));
					 observothertxt  = String.valueOf(result1.getProperty("OtherObservation"));
					 occflg = String.valueOf(result1.getProperty("Occupied"));
					 stroccup  = String.valueOf(result1.getProperty("OccupiedPercent"));
					 vacflg = String.valueOf(result1.getProperty("Vacant"));
					 strvacant = String.valueOf(result1.getProperty("VacantPercent"));
				     notdet = String.valueOf(result1.getProperty("NotDetermined"));
				     permitconf  = String.valueOf(result1.getProperty("PermitConfirmed"));
				     bsize = String.valueOf(result1.getProperty("BuildingSize"));
				     balconypres = String.valueOf(result1.getProperty("BalconyPresent"));

				     addistru= String.valueOf(result1.getProperty("AdditionalStructure"));
				     otherloca= String.valueOf(result1.getProperty("OtherLocation"));
				     loc1 = String.valueOf(result1.getProperty("Location1"));
				     loc2 = String.valueOf(result1.getProperty("Location2"));
				     loc3 = String.valueOf(result1.getProperty("Location3"));
				     loc4 = String.valueOf(result1.getProperty("Location4"));
				      
				     obs1  = String.valueOf(result1.getProperty("Observation1"));
				     obs2= String.valueOf(result1.getProperty("Observation2"));
				     obs3  = String.valueOf(result1.getProperty("Observation3"));
				     obs4  = String.valueOf(result1.getProperty("Observation4"));
				     obs5 = String.valueOf(result1.getProperty("Observation5"));

				     strpf1 = String.valueOf(result1.getProperty("PerimeterPoolFence"));
				     strpf2= String.valueOf(result1.getProperty("PoolFenceDisrepair"));
				     strpf3= String.valueOf(result1.getProperty("SelfLatch"));
				     strpf4= String.valueOf(result1.getProperty("ProfesInstall"));
				      
				     strpp1= String.valueOf(result1.getProperty("PoolPresent"));
				     strpp2 = String.valueOf(result1.getProperty("HotTubPresnt"));
				     strpp3= String.valueOf(result1.getProperty("EmptyGroundPool"));
				     strpp4= String.valueOf(result1.getProperty("PoolSlide"));
				     strpp5= String.valueOf(result1.getProperty("DivingBoard"));
				     strpp6= String.valueOf(result1.getProperty("PerimeterPoolEncl"));
				     strpp7 = String.valueOf(result1.getProperty("PoolEnclosure"));
				     
				     
				     strhz1  =   String.valueOf(result1.getProperty("Vicious"));
				      strhz2  =  String.valueOf(result1.getProperty("LiveStock"));
				      strhz3  =  String.valueOf(result1.getProperty("OverHanging"));
				      strhz4  =  String.valueOf(result1.getProperty("Trampoline"));
				      strhz5  =  String.valueOf(result1.getProperty("SkateBoard"));
				      strhz6  =  String.valueOf(result1.getProperty("bicycle"));
				      strhz7  =  String.valueOf(result1.getProperty("TripHazardDesc"));
				      strhz8  =  String.valueOf(result1.getProperty("TripHazardNoted"));
				      strhz9  =  String.valueOf(result1.getProperty("UnsafeStairway"));
				      strhz10  =  String.valueOf(result1.getProperty("PorchAndDeck"));
				      strhz11  =  String.valueOf(result1.getProperty("NonStdConstruction"));
				      strhz12  =  String.valueOf(result1.getProperty("OutDoorAppliances"));
				      strhz13  =  String.valueOf(result1.getProperty("OpenFoundation"));
				      strhz14  =  String.valueOf(result1.getProperty("WoodShingled"));
				      strhz15  =  String.valueOf(result1.getProperty("ExcessDebris"));
				      strhz16  =  String.valueOf(result1.getProperty("BusinessPremises"));
				      strhz17  =  String.valueOf(result1.getProperty("GeneralDisrepair"));
				      strhz18  =  String.valueOf(result1.getProperty("PropertyDamage"));
				      strhz19  =  String.valueOf(result1.getProperty("StructurePartial"));
				      strhz20  =  String.valueOf(result1.getProperty("InOperative"));
				      strhz21  =  String.valueOf(result1.getProperty("RecentDrywall"));
				      strhz22  =  String.valueOf(result1.getProperty("ChineseDrywall"));
				      strhz23  =  String.valueOf(result1.getProperty("ConfirmDrywall"));
				      strhz24  =  String.valueOf(result1.getProperty("NonSecurity"));
				      strhz25  =  String.valueOf(result1.getProperty("NonSmoke"));
				      gencomments  =  String.valueOf(result1.getProperty("Comments"));

				     
					 Cursor c1 = db.wind_db.rawQuery("SELECT * FROM " + db.GeneralHazInfo + " WHERE Srid='" + cf.Homeid + "'", null);
						if(c1.getCount()==0)
							{
							db.wind_db.execSQL("INSERT INTO "
									+ db.GeneralHazInfo
									+ " (Srid,InsuredIs,InsuredOther,OccupancyType,OcccupOther,ObservationType,ObservOther,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
									+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
									+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
									+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
									+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
									+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
									+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
									+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
									+ " VALUES ('"
									+ cf.Homeid
									+ "','"
									+ InsuredIs
									+ "','"+db.encode(insuredothertxt)+"','"
									+ occupancytype
									+ "','"+db.encode(occupancyothertxt)+"','"
									+ observtype
									+ "','"+db.encode(observothertxt)+"','"
									+ stroccup
									+ "','"
									+ occflg
									+ "','"
									+ vacflg
									+ "','"
									+ strvacant
									+ "','"
									+ notdet
									+ "','"
									+ permitconf
									+ "','"
									+ bsize
									+ "','"
									+ balconypres
									+ "','"
									+ addistru
									+ "','"
									+ db.encode(otherloca)
									+ "',"
									+ "'"+loc1+"','"+loc2+"','"+loc3+"','"+loc4+"',"
									+ "'"+obs1+"','"+obs2+"','"+obs3+"','"+obs4+"','"+obs5+"','"+strpf1+"','"+strpf2+"','"+strpf3+"','"+strpf4+"','"+strpp1+"','"+strpp2+"','"+strpp3+"','"+strpp4+"','"+strpp5+"','"+strpp6+"','"+strpp7+"','"+strhz1 +"','"+strhz2+"','"+strhz3+"','"+strhz4+"','"+strhz5+"','"+strhz6 +"','"+strhz7+"','"+strhz8+"','"+strhz9+"','"+strhz10+"','"+strhz11 +"','"+strhz12+"','"+strhz13+"','"+strhz14+"','"+strhz15+"','"+strhz16 +"','"+strhz17+"','"+strhz18+"','"+strhz19+"','"+strhz20+"','"+strhz21 +"','"+strhz22+"','"+strhz23+"','"+strhz24+"','"+strhz25+"','"+db.encode(gencomments)+"')");
							
							} 
							else
							{
								db.wind_db.execSQL("UPDATE "
										+ db.GeneralHazInfo
										+ " SET InsuredIs='" + InsuredIs
										+ "',InsuredOther='" + db.encode(insuredothertxt)
										+ "',OccupancyType='" + occupancytype
										+ "',OcccupOther='" + db.encode(occupancyothertxt)
										+ "',ObservationType='" + observtype 
										+ "',ObservOther='" + db.encode(observothertxt)
										+ "'," + "OccupiedPercent='"
										+ stroccup + "',OccupiedFlag='"
										+ occflg + "',Vacant='" + vacflg
										+ "',VacantPercent='" + strvacant
										+ "',NotDetermined='" + notdet
										+ "',PermitConfirmed='" + permitconf  
										+ "',BuildingSize='" + bsize
										+ "'," + "BalconyPresent='"
										+ balconypres 
										+ "',AdditionalStructure='"
										+ addistru + "',OtherLocation='"
										+ db.encode(otherloca)+ "',Location1='" + loc1 + "',Location2='"
										+ loc2 + "',Location3='" + loc3
										+ "',Location4='" + loc4 + "', Observation1='" + obs1
							+ "',Observation2='" + obs2 + "',"
							+ "Observation3='" + obs3 + "',Observation4='"
							+ obs4 + "',Observation5='" + obs5
							+ "',PerimeterPoolFence='" + strpf1
							+ "',PoolFenceDisrepair='" + strpf4 + "',"
							+ "SelfLatch='" + strpf2 + "',ProfesInstall='"
							+ strpf3 + "',PoolPresent='" + strpp1
							+ "',HotTubPresnt='" + strpp2
							+ "',EmptyGroundPool='" + strpp3 + "',"
							+ "PoolSlide='" + strpp4
							+ "',DivingBoardPoolSlide='" + strpp5
							+ "',PerimeterPoolEncl='" + strpp6
							+ "',PoolEnclosure='" + strpp7 + "',Vicious='" + strhz1 + "',LiveStock='"
							+ strhz2 + "',OverHanging='" + strhz3
							+ "',Trampoline='" + strhz4 + "',SkateBoard='"
							+ strhz5 + "'," + "bicycle='" + strhz6
							+ "',TripHazardDesc='"
							+ db.encode(strhz8)
							+ "',TripHazardNoted='" + strhz7
							+ "',UnsafeStairway='" + strhz9
							+ "',PorchAndDeck='" + strhz10 + "',"
							+ "NonStdConstruction='" + strhz11
							+ "',OutDoorAppliances='" + strhz12
							+ "',OpenFoundation='" + strhz13
							+ "',WoodShingled='" + strhz14 + "',"
							+ "ExcessDebris='" + strhz15
							+ "',BusinessPremises='" + strhz16
							+ "',GeneralDisrepair='" + strhz17
							+ "',PropertyDamage='" + strhz18 + "',"
							+ "StructurePartial='" + strhz19
							+ "',InOperative='" + strhz20 + "',RecentDrywall='"
							+ strhz21 + "',ChineseDrywall='" + strhz22 + "',"
							+ "ConfirmDrywall='" + strhz23 + "',NonSecurity='"
							+ strhz24 + "',NonSmoke='" + strhz25
							+ "',Comments='"+db.encode(gencomments)+"' WHERE Srid ='" + cf.Homeid+ "'");
							}
						GeneralHazImages();
				 }
			}
			catch(Exception e)
			{
				System.out.println("commemts = "+e.getMessage());
			}
	}
	public void GeneralHazImages()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException{
		// TODO Auto-generated method stub
		db.CreateTable(14);
		SoapObject request = new SoapObject(wb.NAMESPACE,"ImportGenHzdImages");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("SRID",cf.Homeid);
		request.addProperty("InspectorID",db.Insp_id);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE+"ImportGenHzdImages",envelope);
		SoapObject temp_result = (SoapObject) envelope.getResponse();System.out.println("temp_resultt "+temp_result);
		if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
		{
			total = 60;
		}
		else
		{
			try{
				File f=new File(Environment.getExternalStorageDirectory()+"/imported_image");
				if(!f.exists())
				{
					f.mkdir();
				}
				Double count_p=25.00/Double.parseDouble(temp_result.getPropertyCount()+"");
				db.wind_db.execSQL(" DELETE FROM "+db.GeneralHazDoc+" WHERE GCH_SRID='"+cf.Homeid+"'");
				for(int i=0;i<temp_result.getPropertyCount();i++)
				{
					progress_text=" Image "+(i+1)+" of "+temp_result.getPropertyCount()+" images.";
					
					SoapObject na=(SoapObject) temp_result.getProperty(i);
					int elevation_no=0;
					String Elevation=db.convert_null(na.getProperty("Elevation").toString());
					
					
					String Path=db.convert_null(na.getProperty("ImageName").toString());
					String Description=db.encode(db.convert_null(na.getProperty("Description").toString()));
					String ImageOrder=db.encode(db.convert_null(na.getProperty("ImageOrder").toString()));
					
					URL ulrn = new URL(Path);
					HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
					InputStream is = con.getInputStream();
				    
					Bitmap bmp = BitmapFactory.decodeStream(is);
				    ByteArrayOutputStream baos = new ByteArrayOutputStream();
				    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
				    byte[] b = baos.toByteArray();
				    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
				    byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);
				    FILENAME= cf.Homeid+Elevation+ImageOrder+Path.substring(Path.lastIndexOf("."));
				    try
					{
						 
						System.out.println("FILENAME"+FILENAME);
						
						File f1=new File(f,FILENAME);
						if(f1.exists())
						{
							f1.delete();
						}
						FileOutputStream fos = new FileOutputStream(f1);
						fos.write(decode3);
						fos.close();	
					}
					catch (IOException e){
						System.out.println("we found isseus in the webservice "+e.getMessage());
					}
						db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazDoc
							+ " (GCH_InspectorId,GCH_SRID,GCH_Elevation,GCH_path,GCH_Description,GCH_ImageOrder)"
							+ " VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+Elevation+ "','"+ db.encode(Environment.getExternalStorageDirectory()+"/imported_image/"+FILENAME) + "','"
							+ Description+ "','" + ImageOrder + "')");
						
						total+=count_p;
					
			}
			}
			catch(Exception e)
			{
				System.out.println("he issues is gendoc"+e.getMessage());
			
			}
		}
	}
	protected void GetQuestions() {
		// TODO Auto-generated method stub
		String bcyearbuilt,bcdate,bcvalue,SRID,rdvalue,rdothertext,rwvalue,rwsubvalue,toenailvalue,clipsvalue,singlewallvalue,doublewallvalue,rwothertext,
		rgvalue,rgothertext,rglength,rgtotalarea,swrvalue,openprottype,opvalue,opsubvalue,Windowentrydoorsval,Garagedoorsval,Skylightsval,
		Glassblocksval,Entrydoorsval,NGOGarageDoorsval,woodframeval,woodframeper,reinmasonryval,unreinmasonryval,unreinmasonryper,pouredconcreteval,
		pouredconcreteper,otherwalltxt,otherwallval,otherwallper,overallcomments,gcvalue,reinmasonryper;
		try{
			SoapObject request = new SoapObject(wb.NAMESPACE, "ImportQuestions");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID", db.Insp_id);
			request.addProperty("SRID", cf.Homeid);
			envelope.setOutputSoapObject(request);System.out.println("nnnn"+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			androidHttpTransport.call(wb.NAMESPACE+"ImportQuestions", envelope);
			SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("resultquestions"+result1);
			int Cnt;
			db.CreateTable(7);
			 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
			 {
					Cnt=0;total = 30;
			 }
			 else
			 {			
				 System.out.println("inside qusest elase");
				 SRID = String.valueOf(result1.getProperty("SRID"));
				 bcyearbuilt = String.valueOf(result1.getProperty("BuildingCodeYearBuilt"));
				 bcdate = String.valueOf(result1.getProperty("BuildingCodePerApplnDate"));
				 bcvalue = String.valueOf(result1.getProperty("BuildingCodeValue"));
				 
				 rdvalue = String.valueOf(result1.getProperty("RoofDeckValue"));
				 rdothertext= String.valueOf(result1.getProperty("RoofDeckOtherText")).equals("anyType{}")?"":String.valueOf(result1.getProperty("RoofDeckOtherText"));
				 
				 rwvalue = String.valueOf(result1.getProperty("RooftoWallValue"));
				 toenailvalue = String.valueOf(result1.getProperty("RooftoWallToeNailMinValue"));
				 clipsvalue = String.valueOf(result1.getProperty("RooftoWallClipsMinValue"));
				 singlewallvalue = String.valueOf(result1.getProperty("RooftoWallSingleMinValue"));
				 doublewallvalue = String.valueOf(result1.getProperty("RooftoWallDoubleMinValue"));
				 rwsubvalue = String.valueOf(result1.getProperty("RooftoWallSubValue"));
				 rwothertext= String.valueOf(result1.getProperty("RooftoWallOtherText")).equals("anyType{}")?"":String.valueOf(result1.getProperty("RooftoWallOtherText"));
				 
				 rgvalue = String.valueOf(result1.getProperty("RoofGeoValue"));
				 rgothertext= String.valueOf(result1.getProperty("RoofGeoOtherText")).equals("anyType{}")?"":String.valueOf(result1.getProperty("RoofGeoOtherText"));
				 rglength = String.valueOf(result1.getProperty("RoofGeoLength"));
				 rgtotalarea = String.valueOf(result1.getProperty("RoofGeoTotalArea"));
				 
				 swrvalue = String.valueOf(result1.getProperty("SWRValue"));
				 
				 opvalue = String.valueOf(result1.getProperty("OpenProtectValue"));
				 opsubvalue = String.valueOf(result1.getProperty("OpenProtectSubValue"));
				 
				 
				 Windowentrydoorsval = String.valueOf(result1.getProperty("GOWindEntryDoorsValue"));
				 Garagedoorsval = String.valueOf(result1.getProperty("GOGarageDoorsValue"));
				 Skylightsval = String.valueOf(result1.getProperty("GOSkylightsValue"));
				 Glassblocksval = String.valueOf(result1.getProperty("NGOGlassBlockValue"));
				 Entrydoorsval = String.valueOf(result1.getProperty("NGOEntryDoorsValue"));
				 NGOGarageDoorsval = String.valueOf(result1.getProperty("NGOGarageDoorsValue"));
				 
				 woodframeval = String.valueOf(result1.getProperty("WoodFrameValue"));
				 woodframeper = String.valueOf(result1.getProperty("WoodFramePer"));
				 
				 reinmasonryval = String.valueOf(result1.getProperty("ReinMasonryValue"));
				 reinmasonryper = String.valueOf(result1.getProperty("ReinMasonryPer"));
				 
				 unreinmasonryval = String.valueOf(result1.getProperty("UnReinMasonryValue"));
				 unreinmasonryper = String.valueOf(result1.getProperty("UnReinMasonryPer"));
				 
				 pouredconcreteval = String.valueOf(result1.getProperty("PouredConcrete"));
				 pouredconcreteper = String.valueOf(result1.getProperty("PouredConcretePer"));
				 
				 otherwalltxt= String.valueOf(result1.getProperty("OtherWallTitle")).equals("anyType{}")?"":String.valueOf(result1.getProperty("OtherWallTitle"));
				 otherwallval = String.valueOf(result1.getProperty("OtherWal"));
				 otherwallper = String.valueOf(result1.getProperty("OtherWallPer"));
				 
				 gcvalue = String.valueOf(result1.getProperty("GeneralHazardsInclude"));
				 
				 Cursor c =db.SelectTablefunction(db.Questions, " WHERE SRID='"+cf.Homeid+"'");
					if(c.getCount()>0)
					{
						db.wind_db.execSQL("UPDATE " + db.Questions
								+ " SET BuildingCodeYearBuilt='" + bcyearbuilt
								+ "',BuildingCodePerApplnDate ='" + bcdate
								+ "',BuildingCodeValue='" + bcvalue + "',RoofDeckValue='" + rdvalue+ "',RoofDeckOtherText='"+ db.encode(rdothertext)+ "',RooftoWallValue ='" + rwvalue
									+ "',RooftoWallSubValue='" + rwsubvalue
									+ "',RooftoWallClipsMinValue='"
									+ clipsvalue
									+ "',RooftoWallSingleMinValue='"
									+ singlewallvalue
									+ "',RooftoWallDoubleMinValue='"
									+ doublewallvalue
									+ "',RooftoWallOtherText='"
									+ db.encode(rwothertext) + "',RoofGeoValue='" + rgvalue
								+ "',RoofGeoLength ='" + rglength
								+ "',RoofGeoTotalArea='" + rgtotalarea
								+ "',RoofGeoOtherText='"
								+ db.encode(rgothertext) + "',SWRValue='" + swrvalue + "',OpenProtectValue='"
																+ opvalue
																+ "',OpenProtectSubValue='"
																+ opsubvalue
																+ "',GOWindEntryDoorsValue='"
																+ Windowentrydoorsval
																+ "',GOGarageDoorsValue='"
																+ Garagedoorsval
																+ "',GOSkylightsValue='"
																+ Skylightsval
																+ "',NGOGlassBlockValue='"
																+ Glassblocksval
																+ "',NGOEntryDoorsValue='"
																+ Entrydoorsval
																+ "',NGOGarageDoorsValue='"
																+ NGOGarageDoorsval
																+ "',WoodFrameValue='"
										+ woodframeval
										+ "',WoodFramePer='"
										+ woodframeper 
										+ "',ReinMasonryValue='"
										+ reinmasonryval
										+ "',ReinMasonryPer='"+reinmasonryper +"',UnReinMasonryValue='"
										+ unreinmasonryval
										+ "',UnReinMasonryPer='"
										+ unreinmasonryper 
										+ "',PouredConcrete='"
										+ pouredconcreteval
										+ "',PouredConcretePer='"
										+ pouredconcreteper 
										+ "',OtherWallTitle='"
										+ db.encode(otherwalltxt)
										+ "',OtherWal='" + otherwallval
										+ "',OtherWallPer='" + otherwallper  + "' WHERE SRID ='" + cf.Homeid.toString() + "'");
						
						System.out.println("UPDATE " + db.Questions
								+ " SET BuildingCodeYearBuilt='" + bcyearbuilt
								+ "',BuildingCodePerApplnDate ='" + bcdate
								+ "',BuildingCodeValue='" + bcvalue + "',RoofDeckValue='" + rdvalue+ "',RoofDeckOtherText='"+ db.encode(rdothertext)+ "',RooftoWallValue ='" + rwvalue
									+ "',RooftoWallSubValue='" + rwsubvalue
									+ "',RooftoWallClipsMinValue='"
									+ clipsvalue
									+ "',RooftoWallSingleMinValue='"
									+ singlewallvalue
									+ "',RooftoWallDoubleMinValue='"
									+ doublewallvalue
									+ "',RooftoWallOtherText='"
									+ db.encode(rwothertext) + "',RoofGeoValue='" + rgvalue
								+ "',RoofGeoLength ='" + rglength
								+ "',RoofGeoTotalArea='" + rgtotalarea
								+ "',RoofGeoOtherText='"
								+ db.encode(rgothertext) + "',SWRValue='" + swrvalue + "',OpenProtectValue='"
																+ opvalue
																+ "',OpenProtectSubValue='"
																+ opsubvalue
																+ "',GOWindEntryDoorsValue='"
																+ Windowentrydoorsval
																+ "',GOGarageDoorsValue='"
																+ Garagedoorsval
																+ "',GOSkylightsValue='"
																+ Skylightsval
																+ "',NGOGlassBlockValue='"
																+ Glassblocksval
																+ "',NGOEntryDoorsValue='"
																+ Entrydoorsval
																+ "',NGOGarageDoorsValue='"
																+ NGOGarageDoorsval
																+ "',WoodFrameValue='"
										+ woodframeval
										+ "',WoodFramePer='"
										+ woodframeper 
										+ "',ReinMasonryValue='"
										+ reinmasonryval
										+ "',ReinMasonryPer='"+reinmasonryper +"',UnReinMasonryValue='"
										+ unreinmasonryval
										+ "',UnReinMasonryPer='"
										+ unreinmasonryper 
										+ "',PouredConcrete='"
										+ pouredconcreteval
										+ "',PouredConcretePer='"
										+ pouredconcreteper 
										+ "',OtherWallTitle='"
										+ db.encode(otherwalltxt)
										+ "',OtherWal='" + otherwallval
										+ "',OtherWallPer='" + otherwallper  + "' WHERE SRID ='" + cf.Homeid.toString() + "'");
						
					}
					else
					{
						db.wind_db.execSQL("INSERT INTO "
								+ db.Questions
								+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue," +
								"RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide," +
								"RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue," +
								"RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue," +
								"OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue," +
								"GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue," +
								"ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal," +
								"OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
								+ "VALUES ('" + cf.Homeid + "','" + bcyearbuilt + "','"+ bcdate + "','" + bcvalue + "','" + 0 + "','" + 0
								+ "','','','','','" + 0 + "','" + rdvalue + "','"+rdothertext+"','" + rwvalue
								+ "','" + rwsubvalue + "','" + clipsvalue + "','" + singlewallvalue + "','" + doublewallvalue+ "','"+rwothertext+"'," +
								"'" + rgvalue + "','" + rglength + "','" + rgtotalarea + "','"+rgothertext+"','" + swrvalue
								+ "','','" + opvalue + "','" + opsubvalue + "','','"+Windowentrydoorsval+"','"+Garagedoorsval+"','"+Skylightsval+"','"+Glassblocksval+"'," +
								"'"+Entrydoorsval+"','"+NGOGarageDoorsval+"','"
								+ woodframeval + "','" + woodframeper  + "','" + reinmasonryval + "','" + reinmasonryper  + "','" + unreinmasonryval
								+ "','" + unreinmasonryper   + "','" + pouredconcreteval + "','" + pouredconcreteper  + "','"+otherwalltxt+"','" + otherwallval
								+ "','" + otherwallper  + "','','" + 0 + "','','','" + cf.datewithtime + "','"
								+ gcvalue + "')");
					}
					//total=25;
					GetRoofCover();
			 }
		}
		catch(Exception e)
		{
			System.out.println("catch in questions"+e.getMessage());
		}
	}
	private void GetRoofCover()throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		String permitdateasphalt = "",yearvalasp = "", yearvalconc = "", yearvalmetal = "",
				yearvalbuiltup = "", yearvalmemb = "", yearvalother = "",
				prodapprovalasphalt = "", yearasphalt = "",RoofCoverValue_f="",Srid="",
				permitdateconcrete = "", prodapprovalconcrete = "",
				yearconcrete = "", permitdatemetal = "", prodapprovalmetal = "",
				yearmetal = "", permitdatebuiltup = "", prodapprovalbuiltup = "",
				yearbuiltup = "", permitdatemembrane = "",predominant="0",
				prodapprovalmembrane = "", yearmembrane = "", permitdateother = "",othercovertext="",
				prodapprovalother = "", yearother = "",yearinfo1 = "0", yearinfo2 = "0", yearinfo3 = "0", yearinfo4 = "0",
				yearinfo5 = "0", yearinfo6 = "0";
		try{
			SoapObject request = new SoapObject(wb.NAMESPACE, "ImportRoofCoverType");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID", db.Insp_id);
			request.addProperty("SRID", cf.Homeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);System.out.println("requestroof"+request);

			androidHttpTransport.call(wb.NAMESPACE+"ImportRoofCoverType", envelope);
			SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("resultroofocver"+result1);
			int Cnt;
			db.CreateTable(12);
			 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
			 {
					Cnt=0;total = 15;
			 }
			 else
			 {
				 Srid  =String.valueOf(result1.getProperty("SRID"));
				 RoofCoverValue_f = String.valueOf(result1.getProperty("RoofCoverType"));
				 permitdateasphalt = String.valueOf(result1.getProperty("PermitApplnDate1")).trim();
				 permitdateconcrete = String.valueOf(result1.getProperty("PermitApplnDate2")).trim();
				 permitdatemetal = String.valueOf(result1.getProperty("PermitApplnDate3")).trim();
				 permitdatebuiltup = String.valueOf(result1.getProperty("PermitApplnDate4")).trim();
				 permitdatemembrane = String.valueOf(result1.getProperty("PermitApplnDate5")).trim();
				 permitdateother = String.valueOf(result1.getProperty("PermitApplnDate6")).trim();
				 othercovertext = String.valueOf(result1.getProperty("RoofCoverTypeOther")).trim();
				 prodapprovalasphalt = String.valueOf(result1.getProperty("ProdApproval1")).trim();
				 prodapprovalconcrete = String.valueOf(result1.getProperty("ProdApproval2")).trim();
				 prodapprovalmetal = String.valueOf(result1.getProperty("ProdApproval3")).trim();
				 prodapprovalbuiltup = String.valueOf(result1.getProperty("ProdApproval4")).trim();
				 prodapprovalmembrane = String.valueOf(result1.getProperty("ProdApproval5")).trim();
				 prodapprovalother = String.valueOf(result1.getProperty("ProdApproval6")).trim();
				 
				 yearvalasp = String.valueOf(result1.getProperty("InstallYear1")).trim();
				 yearvalconc = String.valueOf(result1.getProperty("InstallYear2")).trim();
				 yearvalmetal = String.valueOf(result1.getProperty("InstallYear3")).trim();
				 yearvalbuiltup = String.valueOf(result1.getProperty("InstallYear4")).trim();
				 yearvalmemb = String.valueOf(result1.getProperty("InstallYear5")).trim();
				 yearvalother = String.valueOf(result1.getProperty("InstallYear6")).trim();
				 
				 yearinfo1 = String.valueOf(result1.getProperty("NoInfo1"));
				 yearinfo2 = String.valueOf(result1.getProperty("NoInfo2"));
				 yearinfo3 = String.valueOf(result1.getProperty("NoInfo3"));
				 yearinfo4 = String.valueOf(result1.getProperty("NoInfo4"));
				 yearinfo5 = String.valueOf(result1.getProperty("NoInfo5"));
				 yearinfo6 = String.valueOf(result1.getProperty("NoInfo6"));
				 predominant = String.valueOf(result1.getProperty("RoofPreDominant"));
					
					Cursor c2 = db.wind_db.rawQuery("SELECT * FROM " + db.QuestionsRoofCover
							+ " WHERE SRID='" + cf.Homeid + "'", null);
					if (c2.getCount() == 0) 
						{
						System.out.println("insert roof cover");
						db.wind_db.execSQL("INSERT INTO "
								+ db.QuestionsRoofCover
								+ " (SRID,RoofCoverType,RoofCoverValue,RoofPreDominant,PermitApplnDate1,PermitApplnDate2,PermitApplnDate3,PermitApplnDate4,PermitApplnDate5,PermitApplnDate6,RoofCoverTypeOther,ProdApproval1,ProdApproval2,ProdApproval3,ProdApproval4,ProdApproval5,ProdApproval6,InstallYear1,InstallYear2,InstallYear3,InstallYear4,InstallYear5,InstallYear6,NoInfo1,NoInfo2,NoInfo3,NoInfo4,NoInfo5,NoInfo6)"
								+ "VALUES ('" + cf.Homeid + "','"
								+ db.encode(RoofCoverValue_f) + "','','"
								+ predominant + "','"
								+ db.encode(permitdateasphalt) + "','"
								+ db.encode(permitdateconcrete)
								+ "','" + db.encode(permitdatemetal)
								+ "','" + db.encode(permitdatebuiltup)
								+ "','"
								+ db.encode(permitdatemembrane)
								+ "','" + db.encode(permitdateother)
								+ "','" + db.encode(othercovertext)
								+ "','"
								+ db.encode(prodapprovalasphalt)
								+ "','"
								+ db.encode(prodapprovalconcrete)
								+ "','" + db.encode(prodapprovalmetal)
								+ "','"
								+ db.encode(prodapprovalbuiltup)
								+ "','"
								+ db.encode(prodapprovalmembrane)
								+ "','" + db.encode(prodapprovalother)
								+ "','" + db.encode(yearvalasp)
								+ "','" + db.encode(yearvalconc)
								+ "','" + db.encode(yearvalmetal)
								+ "','" + db.encode(yearvalbuiltup)
								+ "','" + db.encode(yearvalmemb)
								+ "','" + db.encode(yearvalother)
								+ "','" + yearinfo1 + "','" + yearinfo2 + "','"
								+ yearinfo3 + "','" + yearinfo4 + "','" + yearinfo5
								+ "','" + yearinfo6 + "')");
						} 
						else
						{
							System.out.println("UPDATE " + db.QuestionsRoofCover
									+ " SET RoofCoverType='" + db.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='',RoofPreDominant='" + predominant
									+ "',PermitApplnDate1='" + db.encode(permitdateasphalt)
									+ "',ProdApproval1 ='"
									+ db.encode(prodapprovalasphalt)
									+ "',InstallYear1='" + yearvalasp
									+ "',NoInfo1='" + yearinfo1 + "',PermitApplnDate2='" + db.encode(permitdateconcrete)
								+ "',ProdApproval2 ='"
								+ db.encode(prodapprovalconcrete)
								+ "',InstallYear2='" + yearvalconc
								+ "',NoInfo2='" + yearinfo2 + "',PermitApplnDate3='" + db.encode(permitdatemetal)
								+ "',ProdApproval3 ='"
								+ db.encode(prodapprovalmetal)
								+ "',InstallYear3='" + yearvalmetal
								+ "',NoInfo3='" + yearinfo3 + "',PermitApplnDate4='" + db.encode(permitdatebuiltup)
								+ "',ProdApproval4 ='"
								+ db.encode(prodapprovalbuiltup)
								+ "',InstallYear4='" + yearvalbuiltup
								+ "',NoInfo4='" + yearinfo4 + "',PermitApplnDate5='" + db.encode(permitdatemembrane)
								+ "',ProdApproval5='"
								+ db.encode(prodapprovalmembrane)
								+ "',InstallYear5='" + yearvalmemb
								+ "',NoInfo5='" + yearinfo5 + "',PermitApplnDate6='" + db.encode(permitdateother)
								+ "',ProdApproval6='"
								+ db.encode(prodapprovalother)
								+ "',InstallYear6='" + yearvalother
								+ "',NoInfo6='" + yearinfo6
								+ "',RoofCoverTypeOther='"
								+ db.encode(othercovertext) + "' WHERE SRID ='" + cf.Homeid.toString() + "'");
							
							
							db.wind_db.execSQL("UPDATE " + db.QuestionsRoofCover
									+ " SET RoofCoverType='" + db.encode(RoofCoverValue_f)
									+ "',RoofCoverValue='',RoofPreDominant='" + predominant
									+ "',PermitApplnDate1='" + db.encode(permitdateasphalt)
									+ "',ProdApproval1 ='"
									+ db.encode(prodapprovalasphalt)
									+ "',InstallYear1='" + yearvalasp
									+ "',NoInfo1='" + yearinfo1 + "',PermitApplnDate2='" + db.encode(permitdateconcrete)
								+ "',ProdApproval2 ='"
								+ db.encode(prodapprovalconcrete)
								+ "',InstallYear2='" + yearvalconc
								+ "',NoInfo2='" + yearinfo2 + "',PermitApplnDate3='" + db.encode(permitdatemetal)
								+ "',ProdApproval3 ='"
								+ db.encode(prodapprovalmetal)
								+ "',InstallYear3='" + yearvalmetal
								+ "',NoInfo3='" + yearinfo3 + "',PermitApplnDate4='" + db.encode(permitdatebuiltup)
								+ "',ProdApproval4 ='"
								+ db.encode(prodapprovalbuiltup)
								+ "',InstallYear4='" + yearvalbuiltup
								+ "',NoInfo4='" + yearinfo4 + "',PermitApplnDate5='" + db.encode(permitdatemembrane)
								+ "',ProdApproval5='"
								+ db.encode(prodapprovalmembrane)
								+ "',InstallYear5='" + yearvalmemb
								+ "',NoInfo5='" + yearinfo5 + "',PermitApplnDate6='" + db.encode(permitdateother)
								+ "',ProdApproval6='"
								+ db.encode(prodapprovalother)
								+ "',InstallYear6='" + yearvalother
								+ "',NoInfo6='" + yearinfo6
								+ "',RoofCoverTypeOther='"
								+ db.encode(othercovertext) + "' WHERE SRID ='" + cf.Homeid.toString() + "'");
						}
					GetComments();
			 }
		}
		catch(Exception e)
		{
			System.out.println("commemts = "+e.getMessage());
		}
		
	}
	private void GetComments()throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException {
		// TODO Auto-generated method stub
		String Srid,bccomments,rccomments,rdcomments,rwcomments,rgcomments,swrcomments,opcomments,wccomments,occomments;
		try{
			SoapObject request = new SoapObject(wb.NAMESPACE, "ImportComments");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID", db.Insp_id);
			request.addProperty("Srid", cf.Homeid);
			envelope.setOutputSoapObject(request);System.out.println("request"+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			androidHttpTransport.call(wb.NAMESPACE+"ImportComments", envelope);
			SoapObject resultcomm = (SoapObject) envelope.getResponse();System.out.println("resultcomme"+resultcomm);
			int Cnt;
			db.CreateTable(8);
			 if(String.valueOf(resultcomm).equals("null") || String.valueOf(resultcomm).equals(null) || String.valueOf(resultcomm).equals("anytype{}"))
			 {
					Cnt=0;total = 15;
			 }
			 else
			 {
				 Srid  =String.valueOf(resultcomm.getProperty("Srid"));
				 bccomments = String.valueOf(resultcomm.getProperty("BuildingCodeComment"));
				 rccomments = String.valueOf(resultcomm.getProperty("RoofCoverComment"));
				 rdcomments = String.valueOf(resultcomm.getProperty("RoofDeckComment"));
				 rwcomments = String.valueOf(resultcomm.getProperty("RoofWallComment"));
				 rgcomments = String.valueOf(resultcomm.getProperty("RoofGeometryComment"));
				 swrcomments = String.valueOf(resultcomm.getProperty("SecondaryWaterComment"));
				 opcomments = String.valueOf(resultcomm.getProperty("OpeningProtectionComment"));				 
				 wccomments= String.valueOf(resultcomm.getProperty("WallConstructionComment")).equals("anyType{}")?"":String.valueOf(resultcomm.getProperty("WallConstructionComment"));
					
				 occomments = String.valueOf(resultcomm.getProperty("InsOverAllComments"));
					
					Cursor c2 = db.wind_db.rawQuery("SELECT * FROM " + db.QuestionsComments
							+ " WHERE SRID='" + cf.Homeid + "'", null);System.out.println("c2 roof comm"+c2.getCount());
					if (c2.getCount() == 0) 
						{
						System.out.println("INSERT INTO "
								+ db.QuestionsComments
								+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
								+ " VALUES ('"+ cf.Homeid+ "','28','"+db.encode(bccomments)+"','"+db.encode(rccomments)+"','"+db.encode(rdcomments)+"','"+db.encode(rwcomments)+"','"+ db.encode(rgcomments)+ "','"+ db.encode(swrcomments)+ "','"+ db.encode(opcomments)+ "','"+ db.encode(wccomments)+ "','"+ db.encode(occomments)+ "','"+ cf.datewithtime + "')");
							db.wind_db.execSQL("INSERT INTO "
									+ db.QuestionsComments
									+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('"+ cf.Homeid+ "','28','"+db.encode(bccomments)+"','"+db.encode(rccomments)+"','"+db.encode(rdcomments)+"','"+db.encode(rwcomments)+"','"+ db.encode(rgcomments)+ "','"+ db.encode(swrcomments)+ "','"+ db.encode(opcomments)+ "','"+ db.encode(wccomments)+ "','"+ db.encode(occomments)+ "','"+ cf.datewithtime + "')");
						} 
						else
						{
							db.wind_db.execSQL("UPDATE " + db.QuestionsComments
									+ " SET BuildingCodeComment='"+ db.encode(bccomments)+"'," +
											"RoofCoverComment='"+ db.encode(rccomments)+ "',RoofDeckComment='"+ db.encode(rdcomments)+ "'," +
											"RoofWallComment='"+ db.encode(rwcomments)+ "',RoofGeometryComment='"+ db.encode(rgcomments)+ "'," +
											"SecondaryWaterComment='"+ db.encode(swrcomments)+ "',OpeningProtectionComment='"+ db.encode(opcomments)+ "'," +
											"WallConstructionComment='"+ db.encode(wccomments)+ "',InsOverAllComments='"+ db.encode(occomments)+ "'," +
											"RoofCoverComment='"+ db.encode(rccomments)+ "' WHERE SRID ='"+ cf.Homeid.toString() + "'");
						}
					System.out.println("comeplted comments");
			 }
		}
		catch(Exception e)
		{
			System.out.println("commemts = "+e.getMessage());
		}
		
	}
	private void getpolicyholder_info()throws NetworkErrorException,SocketTimeoutException, XmlPullParserException, IOException {
		 
		 
		String  substatus="0",buildingsize="", email, cperson, insurancecompanyname,  homeid = "", firstname, 
				lastname, middlename, addr1, addr2, city, state, country, zip, homephone, cellphone, workphone, ownerpolicyno, status, 
				companyid, inspectorid, wId, cId, inspectorfirstname, inspectorlastname,  yearbuilt, nstories, 
				inspectiondate, inspectioncomment, inspectiontypeid,inspectiontime,description2,description1,
				MailingAddress,MailingAddress2,Mailingcity,MailingState,MailingCounty,Mailingzip, inspfee, inspdiscount;
			
			try{
			SoapObject request = new SoapObject(wb.NAMESPACE, "UPDATEMOBILEDBWITHSRID");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID", cf.Homeid);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);

			androidHttpTransport.call(wb.NAMESPACE+"UPDATEMOBILEDBWITHSRID", envelope);
			SoapObject result1 = (SoapObject) envelope.getResponse();System.out.println("resultpolict"+result1);
			int Cnt;
			db.CreateTable(2);
			 if(String.valueOf(result1).equals("null") || String.valueOf(result1).equals(null) || String.valueOf(result1).equals("anytype{}"))
			 {
					Cnt=0;total = 15;
			 }
			 else
			 {
				 
				 File f=new File(Environment.getExternalStorageDirectory()+"/imported_image");
					if(!f.exists())
					{
						f.mkdir();
					}
					
					String Path=db.convert_null(result1.getProperty("Signature1").toString());
					
					if(!Path.equals("N/A"))
				    {
						URL ulrn = new URL(Path.replace(" ", "%20"));
						HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						InputStream is = con.getInputStream();
					    
						Bitmap bmp = BitmapFactory.decodeStream(is);
					    ByteArrayOutputStream baos = new ByteArrayOutputStream();
					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
					    byte[] b = baos.toByteArray();
					    String homeownersign = Base64.encodeToString(b, Base64.DEFAULT);
					    byte[] decode3 = Base64.decode(homeownersign.toString(), 0);
					    FILENAME= cf.Homeid+"HomeOwner"+Path.substring(Path.lastIndexOf("."));System.out.println("FILENAME"+FILENAME);
					    try
						{
							File f1=new File(f,FILENAME);
							if(f1.exists())
							{
								f1.delete();
							}
							System.out.println("comes correctly22 inside homeowner");
							FileOutputStream fos = new FileOutputStream(f1);System.out.println("fop");
							fos.write(decode3);System.out.println("decode");
							fos.close();	System.out.println("close");
						}
						catch (IOException e){
							System.out.println("we found isseus homwowners signature image webservice "+e.getMessage());
						}
				    }
					else
					{
						FILENAME="";
					}
				    System.out.println("path");
				    
				    String Path2=db.convert_null(result1.getProperty("Signature2").toString());
				    if(!Path2.equals("N/A"))
				    {
				    	URL ulrn1 = new URL(Path2.replace(" ", "%20"));
						HttpURLConnection con1 = (HttpURLConnection)ulrn1.openConnection();
						InputStream is1 = con1.getInputStream();
					    
						Bitmap bmp1 = BitmapFactory.decodeStream(is1);
					    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
					    bmp1.compress(Bitmap.CompressFormat.PNG, 100, baos1); //bm is the bitmap object
					    byte[] b1 = baos1.toByteArray();
					    String paperworksign = Base64.encodeToString(b1, Base64.DEFAULT);
					    byte[] decode4 = Base64.decode(paperworksign.toString(), 0);
					    FILENAME1= cf.Homeid+"PaperWork"+Path2.substring(Path2.lastIndexOf("."));System.out.println("FILENAME"+FILENAME1);
					    try
						{
							File f2=new File(f,FILENAME1);
							if(f2.exists())
							{
								f2.delete();
							}
							System.out.println("comes correctly22");
							FileOutputStream fos1 = new FileOutputStream(f2);
							fos1.write(decode4);
							fos1.close();	
						}
						catch (IOException e){
							System.out.println("we found isseus paperwork signature image webservice "+e.getMessage());
						}
				    }
				    else
				    {
				    	FILENAME1="";
				    }

					homeid = String.valueOf(result1.getProperty("SRID"));
					firstname = String.valueOf(result1.getProperty("FirstName"));
					lastname = String.valueOf(result1.getProperty("LastName"));
					middlename= (String.valueOf(result1.getProperty("MiddleName")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MiddleName")).equals("NA"))?"":(String.valueOf(result1.getProperty("MiddleName")).equals("N/A"))?"":String.valueOf(result1.getProperty("MiddleName"));
					addr1 = String.valueOf(result1.getProperty("Address1")).trim();
					addr2= (String.valueOf(result1.getProperty("Address2")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Address2")).equals("NA"))?"":(String.valueOf(result1.getProperty("Address2")).equals("N/A"))?"":String.valueOf(result1.getProperty("Address2"));
					addr2 = String.valueOf(result1.getProperty("Address2")).trim();
					city = String.valueOf(result1.getProperty("City")).trim();
					state= String.valueOf(result1.getProperty("State")).trim();
					country = String.valueOf(result1.getProperty("Country")).trim();
					zip = String.valueOf(result1.getProperty("Zip"));
					homephone = String.valueOf(result1.getProperty("HomePhone")).trim();
					cellphone = String.valueOf(result1.getProperty("CellPhone")).trim();
					workphone = String.valueOf(result1.getProperty("WorkPhone")).trim();
					email = String.valueOf(result1.getProperty("Email")).trim();			
					cperson = String.valueOf(result1.getProperty("ContactPerson")).trim();
					ownerpolicyno = String.valueOf(result1.getProperty("OwnerPolicyNo")).trim();
					status = String.valueOf(result1.getProperty("Status"));
					substatus = String.valueOf(result1.getProperty("Substatus")).trim();
					companyid = String.valueOf(result1.getProperty("CompanyId"));
					inspectorid = String.valueOf(result1.getProperty("InspectorId"));			
					yearbuilt = String.valueOf(result1.getProperty("YearBuilt")).trim();
					nstories = String.valueOf(result1.getProperty("Nstories"));
					inspectiontypeid = String.valueOf(result1.getProperty("InspectionTypeId"));
					inspectiondate= String.valueOf(result1.getProperty("Inspectiondate")).trim();
			        inspectiontime= String.valueOf(result1.getProperty("InspectionTime")).trim();
			        inspectioncomment= (String.valueOf(result1.getProperty("InspectionComment")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals("NA"))?"":(String.valueOf(result1.getProperty("InspectionComment")).equals("N/A"))?"":String.valueOf(result1.getProperty("InspectionComment"));
			        insurancecompanyname = String.valueOf(result1.getProperty("InsuranceCompany"));
					buildingsize = String.valueOf(result1.getProperty("BuildingSize"));
					
					description1 = String.valueOf(result1.getProperty("Description1"));
					description2 = String.valueOf(result1.getProperty("Description2"));
					
					MailingAddress= (String.valueOf(result1.getProperty("MailingAddress")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingAddress")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingAddress")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingAddress"));
					MailingAddress2 = (String.valueOf(result1.getProperty("MailingAddress2")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingAddress2")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingAddress2"));
					Mailingcity = (String.valueOf(result1.getProperty("Mailingcity")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Mailingcity")).equals("NA"))?"":(String.valueOf(result1.getProperty("Mailingcity")).equals("N/A"))?"":String.valueOf(result1.getProperty("Mailingcity"));
					MailingState = (String.valueOf(result1.getProperty("MailingState")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingState")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingState")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingState"));
					MailingCounty = (String.valueOf(result1.getProperty("MailingCounty")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("MailingCounty")).equals("NA"))?"":(String.valueOf(result1.getProperty("MailingCounty")).equals("N/A"))?"":String.valueOf(result1.getProperty("MailingCounty"));				
					Mailingzip = (String.valueOf(result1.getProperty("Mailingzip")).equals("anyType{}"))?"":(String.valueOf(result1.getProperty("Mailingzip")).equals("NA"))?"":(String.valueOf(result1.getProperty("Mailingzip")).equals("N/A"))?"":String.valueOf(result1.getProperty("Mailingzip"));
					
					inspfee = String.valueOf(result1.getProperty("Inspectionfee")).trim();
					inspdiscount = String.valueOf(result1.getProperty("Discount")).trim();
					
					if(status.equals("30") || status.equals("40")){
						status = "40";
					}
					
					if(FILENAME.equals(""))
					{
						path = "";
					}
					else 
					{
						path= db.encode(Environment.getExternalStorageDirectory()+"/imported_image/"+FILENAME);
					}
					
					
					if(FILENAME1.equals(""))
					{
						path1 = "";
					}
					else 
					{
						path1= db.encode(Environment.getExternalStorageDirectory()+"/imported_image/"+FILENAME1);
					}
					
					db.CreateTable(23);
					Cursor c1 = db.SelectTablefunction(db.policyholder," where PH_InspectorId='" + db.encode(db.Insp_id) + "' and PH_SRID='"+cf.Homeid+"'");
						if(c1.getCount() == 0)
						 {
							db.wind_db.execSQL("INSERT INTO "
									+ db.policyholder
									+ " (PH_SRID,PH_FirstName,PH_LastName,PH_MiddleName,PH_Address1,PH_Address2,PH_City,PH_State,PH_County,PH_Zip,YearBuilt,PH_HomePhone," +
									"PH_WorkPhone,PH_CellPhone,PH_ContactPerson ,PH_Policyno,PH_Email,PH_NOOFSTORIES," +
									"PH_Status,PH_SubStatus,PH_InspectorId," +
									"Schedule_Comments,PH_InspectionTypeId,InspectionDate,InspectionTime,InsuranceCompanyname,chkbx," +
									"BuildingSize,Fee,Discount,fld_homewonercaption,fld_paperworkcaption,fld_homeownersign,fld_paperworksign)"
									+ " VALUES ('" + homeid + "','"+ db.encode(firstname) + "','"+ db.encode(lastname) + "','"+ db.encode(middlename) + "'," +
											"'"+db.encode(addr1)+"','"+db.encode(addr2)+"','"+db.encode(city)+"','"+db.encode(state)+"','"+db.encode(country)+"'," +
											"'"+zip+"','"+yearbuilt+"','"+db.encode(homephone)+"','"+db.encode(workphone)+"','"+db.encode(cellphone)+"','"+db.encode(cperson)+"'," +
											"'"+db.encode(ownerpolicyno)+"','"+db.encode(email)+"','"+nstories+"'," +
											"'"+status+"','"+substatus+"','"+inspectorid+"'," +
											"'"+db.encode(inspectioncomment)+"','"+inspectiontypeid+"','"+db.encode(inspectiondate)+"','"+db.encode(inspectiontime)+"','"+db.encode(insurancecompanyname)+"'," +
											"'0','"+buildingsize+"','"+db.encode(inspfee)+"','"+db.encode(inspdiscount)+"','"+db.encode(description1)+"','"+db.encode(description2)+"',,'"+ path + "','"+ path1+ "')");

							System.out.println("INSERT INTO "
									+ db.policyholder
									+ " (PH_SRID,PH_FirstName,PH_LastName,PH_MiddleName,PH_Address1,PH_Address2,PH_City,PH_State,PH_County,PH_Zip,YearBuilt,PH_HomePhone," +
									"PH_WorkPhone,PH_CellPhone,PH_ContactPerson ,PH_Policyno,PH_Email,PH_NOOFSTORIES," +
									"PH_Status,PH_SubStatus,PH_InspectorId," +
									"Schedule_Comments,PH_InspectionTypeId,InspectionDate,InspectionTime,InsuranceCompanyname,chkbx," +
									"BuildingSize,Fee,Discount,fld_homewonercaption,fld_paperworkcaption,fld_homeownersign,fld_paperworksign)"
									+ " VALUES ('" + homeid + "','"+ db.encode(firstname) + "','"+ db.encode(lastname) + "','"+ db.encode(middlename) + "'," +
											"'"+db.encode(addr1)+"','"+db.encode(addr2)+"','"+db.encode(city)+"','"+db.encode(state)+"','"+db.encode(country)+"'," +
											"'"+zip+"','"+yearbuilt+"','"+db.encode(homephone)+"','"+db.encode(workphone)+"','"+db.encode(cellphone)+"','"+db.encode(cperson)+"'," +
											"'"+db.encode(ownerpolicyno)+"','"+db.encode(email)+"','"+nstories+"'," +
											"'"+status+"','"+substatus+"','"+inspectorid+"'," +
											"'"+db.encode(inspectioncomment)+"','"+inspectiontypeid+"','"+db.encode(inspectiondate)+"','"+db.encode(inspectiontime)+"','"+db.encode(insurancecompanyname)+"'," +
											"'0','"+buildingsize+"','"+db.encode(inspfee)+"','"+db.encode(inspdiscount)+"','"+db.encode(description1)+"','"+db.encode(description2)+"','"+ path + "','"+ path1+ "')");
							
							db.wind_db.execSQL("INSERT INTO "
									+ db.MailingPolicyHolder
									+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
									+ "VALUES ('"+homeid+"','"+inspectorid+"','0','"+db.encode(MailingAddress)+"','"
								    + db.encode(MailingAddress2)+"','"+db.encode(Mailingcity)+"','"
								    +Mailingzip+"','"+db.encode(MailingState)+"','"
								    + db.encode(MailingCounty)+"')");
		
						 }		
						else
						{
							System.out.println("UPDATE " + db.policyholder
									+ " SET PH_FirstName='"+ db.encode(firstname)+ "',PH_LastName='"+ db.encode(lastname)+ "',PH_Address1='"+ db.encode(addr1) + "',PH_Address2='"+ db.encode(addr2) + "'," +
											"PH_City='"+ db.encode(city) + "',PH_Zip='"+ zip + "',PH_State='"+ db.encode(state) + "',PH_County='"+ db.encode(country)+ "'," +
											"YearBuilt='" + yearbuilt+ "',PH_Status='"+status+"',PH_SubStatus='"+substatus+"',PH_ContactPerson='"+ db.encode(cperson)+ "',PH_HomePhone='" + db.encode(homephone)+ "',PH_WorkPhone='" + db.encode(workphone)+ "'," +
											"PH_CellPhone='" + db.encode(cellphone)+ "',PH_Policyno='"+ db.encode(ownerpolicyno) + "',PH_NOOFSTORIES='"+ nstories + "',PH_Email='" + db.encode(email)+ "'," +
											"chkbx='" + 0 + "',Schedule_Comments='"+db.encode(inspectioncomment)+"',InspectionDate='"+db.encode(inspectiondate)+"',InspectionTime='"+db.encode(inspectiontime)+"',Fee='"+db.encode(inspfee)+"',Discount='"+db.encode(inspdiscount)+"',fld_homeownersign='"+ path + "',fld_paperworksign='"+ path1 + "' WHERE PH_SRID ='" + cf.Homeid+ "'");
							db.wind_db.execSQL("UPDATE " + db.policyholder
									+ " SET PH_FirstName='"+ db.encode(firstname)+ "',PH_LastName='"+ db.encode(lastname)+ "',PH_Address1='"+ db.encode(addr1) + "',PH_Address2='"+ db.encode(addr2) + "'," +
											"PH_City='"+ db.encode(city) + "',PH_Zip='"+ zip + "',PH_State='"+ db.encode(state) + "',PH_County='"+ db.encode(country)+ "'," +
											"YearBuilt='" + yearbuilt+ "',PH_Status='"+status+"',PH_SubStatus='"+substatus+"',PH_ContactPerson='"+ db.encode(cperson)+ "',PH_HomePhone='" + db.encode(homephone)+ "',PH_WorkPhone='" + db.encode(workphone)+ "'," +
											"PH_CellPhone='" + db.encode(cellphone)+ "',PH_Policyno='"+ db.encode(ownerpolicyno) + "',PH_NOOFSTORIES='"+ nstories + "',PH_Email='" + db.encode(email)+ "'," +
											"chkbx='" + 0 + "',Schedule_Comments='"+db.encode(inspectioncomment)+"',InspectionDate='"+db.encode(inspectiondate)+"',InspectionTime='"+db.encode(inspectiontime)+"',Fee='"+db.encode(inspfee)+"',Discount='"+db.encode(inspdiscount)+"',fld_homewonercaption='"+db.encode(description1)+"',fld_paperworkcaption='"+db.encode(description2)+"',fld_homeownersign='"+ path + "',fld_paperworksign='"+ path1 + "' WHERE PH_SRID ='" + cf.Homeid+ "'");

						
							db.wind_db.execSQL("UPDATE " + db.MailingPolicyHolder
									+ " SET ML_PH_Address1='" + db.encode(MailingAddress)
									+ "',ML_PH_Address2='"
									+ db.encode(MailingAddress2)
									+ "',ML_PH_City='"
									+ db.encode(Mailingcity)
									+ "',ML_PH_Zip='"
									+ db.encode(Mailingzip) + "',ML_PH_State='"
									+ db.encode(MailingState) + "',ML_PH_County='"
									+ db.encode(MailingCounty) + "' WHERE ML_PH_SRID ='" +cf.Homeid+ "'");
						
						}
									
							       
										
							
			 }				 
		} catch (Exception e)
		{
			System.out.println("eepolic="+e.getMessage());
		}
		 
	 }
	 
	 public void Getimage()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException  
	 {
		 String Elevation="";
		 db.CreateTable(13);
			SoapObject request = new SoapObject(wb.NAMESPACE,"ImportWSI1802ElevationPicture");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("SRID",cf.Homeid);
			request.addProperty("InspectorID",db.Insp_id);
			System.out.println("the findin info request "+request);
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportWSI1802ElevationPicture",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();System.out.println("temp_resultt "+temp_result);
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 60;
			}
			else
			{
				try{
					File f=new File(Environment.getExternalStorageDirectory()+"/imported_image");
					if(!f.exists())
					{
						f.mkdir();
					}
					System.out.println("count_p "+f);
					Double count_p=25.00/Double.parseDouble(temp_result.getPropertyCount()+"");
					db.wind_db.execSQL(" DELETE FROM "+db.Photos+" WHERE IM_SRID='"+cf.Homeid+"'");
					for(int i=0;i<temp_result.getPropertyCount();i++)
					{
						progress_text=" Image "+(i+1)+" of "+temp_result.getPropertyCount()+" images.";
						System.out.println("progress_text"+progress_text);
						
						SoapObject na=(SoapObject) temp_result.getProperty(i);
						int elevation_no=0;
						Elevation=db.convert_null(na.getProperty("Elevation").toString());
						String Path=db.convert_null(na.getProperty("ImageName").toString());
						String Description=db.encode(db.convert_null(na.getProperty("Description").toString()));
						String ImageOrder=db.encode(db.convert_null(na.getProperty("ImageOrder").toString()));System.out.println("Path"+Path);
						
						
						//need to change this in webservice ( image url to byte array )starts //
						URL ulrn = new URL(Path.replace(" ", "%20"));
						HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						InputStream is = con.getInputStream();
					    
						Bitmap bmp = BitmapFactory.decodeStream(is);
					    ByteArrayOutputStream baos = new ByteArrayOutputStream();
					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
					    byte[] b = baos.toByteArray();
					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
					    byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);
					    
					    //ends
						//String b =na.getProperty("imagebyte").toString();
						
					    //byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);
					    
					    
					    
					    String FILENAME= cf.Homeid+Elevation+ImageOrder+Path.substring(Path.lastIndexOf("."));System.out.println("FILENAME"+FILENAME);
					    try
						{
							File f1=new File(f,FILENAME);
							if(f1.exists())
							{
								f1.delete();
							}
							System.out.println("comes correctly22");
							FileOutputStream fos = new FileOutputStream(f1);
							fos.write(decode3);
							fos.close();	
						}
						catch (IOException e){
							System.out.println("we found isseus in the webservice "+e.getMessage());
						}
					    
					    if(Elevation.equals("8"))
						{
					    	Elevation="6";
						}
					    
					    if(!Elevation.equals("7"))
					    {
							db.wind_db.execSQL("INSERT INTO "
									+ db.Photos
									+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
									+ " VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+Elevation+ "','"+ db.encode(Environment.getExternalStorageDirectory()+"/imported_image/"+FILENAME) + "','"
									+ Description+ "','" + ImageOrder + "')");
					    }
							total+=count_p;
						
				}
				}
				catch(Exception e)
				{
					System.out.println("he issues photso is "+e.getMessage());
				
				}
			}
		 
	 }
	 protected void Getfeedback()throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException {
			// TODO Auto-generated method stub
		 String manufinfo="",csc="",insppaerwork="";
	        SoapObject request = new SoapObject(wb.NAMESPACE,"ImportFeedbackInformation");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",db.Insp_id);
			request.addProperty("SRID",cf.Homeid);
			
			envelope.setOutputSoapObject(request);System.out.println("request"+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportFeedbackInformation",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();System.out.println("ImportFeedbackInformation"+temp_result);
			db.CreateTable(10);
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					String CustomerServiceEvaluation=db.encode(db.convert_null(temp_result.getProperty("IsCusServiceCompltd").toString()));
					String WhoPresent=db.encode(db.convert_null(temp_result.getProperty("PresentatInspection").toString()));
					String WhoPresent_Other=db.encode(db.convert_null(temp_result.getProperty("OtherPresent").toString()));
					String InspPaerwork=db.encode(db.convert_null(temp_result.getProperty("IsInspectionPaperAvbl").toString()));
					String Manufinfo =db.encode(db.convert_null(temp_result.getProperty("IsManufacturerInfo").toString()));
					String comments= String.valueOf(temp_result.getProperty("FeedbackComments")).equals("anyType{}")?"":String.valueOf(temp_result.getProperty("FeedbackComments"));
					
					
					if(CustomerServiceEvaluation.equals("1"))
					{
						csc ="Yes";
					}
					else if(CustomerServiceEvaluation.equals("2"))
					{
						csc ="No";
					}
					
					
					if(InspPaerwork.equals("1"))
					{
						insppaerwork ="Yes";
					}
					else if(InspPaerwork.equals("2"))
					{
						insppaerwork ="No";
					}
					else if(InspPaerwork.equals("3"))
					{
						insppaerwork ="Not Applicable";
					}
					
					if(Manufinfo.equals("1"))
					{
						manufinfo ="Yes";
					}
					else if(Manufinfo.equals("2"))
					{
						manufinfo ="No";
					}
					else if(Manufinfo.equals("3"))
					{
						manufinfo ="Not Applicable";
					}
					
					
					
					Cursor c1=db.SelectTablefunction(db.FeedBackInfo, " WHERE FD_SRID='"+cf.Homeid+"'");System.out.println("c1="+c1.getCount());
					if(c1.getCount()>0)
					{
						db.wind_db.execSQL("UPDATE "+db.FeedBackInfo+" SET FD_CSC='"+db.encode(csc)+"',FD_whowas='"+WhoPresent+"'," +
								"FD_whowas_other='"+WhoPresent_Other+"',FD_insupaperwork='"+db.encode(insppaerwork)+"'," +
										"FD_manuinfo='"+db.encode(manufinfo)+"',FD_comments='"+comments+"' WHERE FD_SRID='"+cf.Homeid+"'");
					}
					else
					{
						db.wind_db.execSQL(" INSERT INTO "+db.FeedBackInfo+" (FD_InspectorId,FD_SRID,FD_CSC,FD_whowas,FD_whowas_other,FD_insupaperwork,FD_manuinfo,FD_comments)" +
								" VALUES ('"+db.Insp_id+"','"+cf.Homeid+"','"+db.encode(csc)+"','"+WhoPresent+"','"+WhoPresent_Other+"','"+db.encode(insppaerwork)+"','"+db.encode(manufinfo)+"','"+comments+"')");
					}
					}
					catch(Exception e)
					{
						System.out.println("he issues feedbackinso is "+e.getMessage());
					
					}

					total=80;
					Getfeedbackdocument();
				
			}
		}
	 private void Getfeedbackdocument() throws SocketException,IOException,FileNotFoundException,NetworkErrorException,TimeoutException, XmlPullParserException{
		// TODO Auto-generated method stub
		 SoapObject request = new SoapObject(wb.NAMESPACE,"ImportFeedbackDocument");System.out.println("");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("InspectorID",db.Insp_id);
			request.addProperty("SRID",cf.Homeid);
			
			envelope.setOutputSoapObject(request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
			androidHttpTransport.call(wb.NAMESPACE+"ImportFeedbackDocument",envelope);
			SoapObject temp_result = (SoapObject) envelope.getResponse();System.out.println("refeedbc"+temp_result);
			db.CreateTable(11);
			if(String.valueOf(temp_result).equals("null") || String.valueOf(temp_result).equals(null) || String.valueOf(temp_result).equals("anytype{}"))
			{
				total = 30;
			}
			else
			{
				try{
					File f=new File(Environment.getExternalStorageDirectory()+"/imported_feedback");
					
					if(!f.exists())
					{
						f.mkdir();
					}
					try
					{
						db.wind_db.execSQL("DELETE FROM  "+db.FeedBackDocument+ " WHERE  FD_D_SRID='"+cf.Homeid+"' " );
					}
					catch(Exception e)
					{
						System.out.println("fddeleted"+e.getMessage());
					}
					Double count_p=20.00/Double.parseDouble(temp_result.getPropertyCount()+"");
				
					for(int i=0;i<temp_result.getPropertyCount();i++)
					{
						progress_text=" Feedback Documents "+(i+1)+" of "+temp_result.getPropertyCount()+" Documents.";
					
						SoapObject na=(SoapObject) temp_result.getProperty(i);
						
						int elevation_no=0;
						 
						String title=db.convert_null(na.getProperty("DocumentTitle").toString());System.out.println("title"+title);
						String ImageOrder=db.encode(db.convert_null(na.getProperty("ImageOrder").toString()));
						String Path=db.convert_null(na.getProperty("FileName").toString());System.out.println("Path"+Path);
						String type=db.encode(db.convert_null(na.getProperty("IsOfficeUse").toString()));System.out.println("IsOfficeUse"+type);
						type=(type.equals("true"))? "1":"0";System.out.println("type"+type);
						URL ulrn = new URL(Path.replace(" ", "%20"));System.out.println("ulrn"+ulrn);
						HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						InputStream is = con.getInputStream();System.out.println("iss");
					   
						Bitmap bmp = BitmapFactory.decodeStream(is);System.out.println("ByteArrayOutputStream");
					    ByteArrayOutputStream baos = new ByteArrayOutputStream();
					    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
					    byte[] b = baos.toByteArray();System.out.println("compress");
					    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
					    byte[] decode3 = Base64.decode(headshot_insp.toString(), 0);System.out.println("decode3"+decode3);
					    String FILENAME= cf.Homeid+title+ImageOrder+Path.substring(Path.lastIndexOf("."));System.out.println("Filenamefed"+FILENAME);
					   
					    try
						{
							 
							
							
							File f1=new File(f,FILENAME);
							if(f1.exists())
							{
								f1.delete();
							}
							
							FileOutputStream fos = new FileOutputStream(f1);
							fos.write(decode3);
							fos.close();	
						}
						catch (IOException e){
							System.out.println("we found isseus in the webservice "+e.getMessage());
						}
						
						db.wind_db.execSQL(" INSERT INTO "+db.FeedBackDocument+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
								"('"+db.Insp_id+"','"+cf.Homeid+"','"+title+"','','"+db.encode(Environment.getExternalStorageDirectory()+"/imported_feedback/"+FILENAME)+"','"+type+"') ");
						
						System.out.println(" INSERT INTO "+db.FeedBackDocument+" (FD_D_InspectorId,FD_D_SRID,FD_D_doctit,FD_D_doctit_other,FD_D_path,FD_D_type) VALUES " +
								"('"+db.Insp_id+"','"+cf.Homeid+"','"+title+"','','"+db.encode(Environment.getExternalStorageDirectory()+"/imported_feedback/"+FILENAME)+"','"+type+"') ");
						total+=count_p;
						
				}
				}
				catch(Exception e)
				{
					System.out.println("he issues feeddoc is "+e.getMessage());
				
				}
			}
	}

	 
	 @Override
		protected Dialog onCreateDialog(int id) {
					switch (id) {
					case 1:
						progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
						progDialog.setMax(100);	
						progDialog.setMessage(Html.fromHtml("Please be patient as we import your data and don't lock your screen it may affect your import.<br> Current importing Status : "+progress_text));	
						progDialog.setCancelable(false);	
						progThread = new ProgressThread(handler1);	
						progThread.start();	
						return progDialog;
					default:
						return null;
					}
				}
	 
		class ProgressThread extends Thread {


		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;

		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			mState = RUNNING;
			
			while (mState == RUNNING) {
				try {
					
					// Control speed of update (but precision of delay not
					// guaranteed)
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					//Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", (int)total);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}

		public void setState(int state) {
			mState = state;
		}

	}
		 	final Handler handler1 = new Handler() {
				public void handleMessage(Message msg) {
					// Get the current value of the variable total from the message data
					// and update the progress bar.
					
					int total = msg.getData().getInt("total");
					progDialog.setProgress(total);
					progDialog.setMessage(Html.fromHtml("Please be patient as we import your data and don't lock your screen it may affect your import.<br> Current importing Status : "+progress_text));
					if (total == 100) {
						progDialog.setCancelable(true);
						dismissDialog(1);
						progThread.setState(ProgressThread.DONE);
				 	}

				}
			};
			 final Handler handler = new Handler() {

					public void handleMessage(Message msg) {
						System.out.println("usercheck"+usercheck);
						 if(usercheck==1)
						 {
							 usercheck = 0;
							 cf.ShowToast("Internet connection not available");
							 finish();
						 }
						 else if(usercheck==2)
						 {
							 usercheck = 0;
							 cf.ShowToast("There is a problem on your network, so please try again later with better network");
							 finish();
						 }
						 else if(usercheck==3)
						 {
							 usercheck = 0;
							 cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							 finish();
						 }
						 else if(usercheck==4)
						 {
							 usercheck=0;
							 progDialog.dismiss();
							 
							 final Dialog dialog1 = new Dialog(Reterive_data.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alert);
								RelativeLayout li=(RelativeLayout)dialog1.findViewById(R.id.helprow);
								li.setVisibility(View.VISIBLE);
								
								LinearLayout lin=(LinearLayout)dialog1.findViewById(R.id.camera);
								lin.setVisibility(v1.VISIBLE);
								
								TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
								txttitle.setText("Import");System.out.println("sdfdsdsfsdf");
								TextView txt = (TextView) dialog1.findViewById(R.id.txtquestio);
								txt.setText("Retrieving data successfully completed");
								((EditText) dialog1.findViewById(R.id.ed_values)).setVisibility(View.GONE);
								
								
								
								Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
								btn_helpclose.setOnClickListener(new OnClickListener()
								{
								public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										//finish();
									}
									
								});

								Button btnclear = (Button) dialog1.findViewById(R.id.clear);
								btnclear.setVisibility(View.GONE);								
								
								Button btn_ok = (Button) dialog1.findViewById(R.id.save);
								btn_ok.setText("OK");
								btn_ok.setVisibility(View.VISIBLE);
								btn_ok.setOnClickListener(new OnClickListener()
								{
								public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										
										db.wind_db.execSQL("UPDATE " + db.policyholder + " SET PH_Status=1 WHERE PH_SRID ='"+ cf.Homeid + "' and PH_InspectorId = '"+ db.Insp_id + "'");
										
										Intent in =new Intent(Reterive_data.this,PolicyholderInfo.class);System.out.println("cf.hom"+cf.Homeid);
										in.putExtra("homeid", cf.Homeid);
										in.putExtra("status", "Inspection Completed");
										startActivity(in);
										//finish();
									}
									
								});
								
								dialog1.setCancelable(false);
								dialog1.show();
						
						 }
						if(wl!=null)
							{
							wl.release();
							wl=null;
							}
					}

				};


}
