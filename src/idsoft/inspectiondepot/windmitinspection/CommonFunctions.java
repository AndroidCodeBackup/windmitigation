package idsoft.inspectiondepot.windmitinspection;


import java.io.DataOutputStream;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;



public class CommonFunctions
{
	public static final int loadcomment_code = 14,currnet_rotated=0;
	int chkroofcover;
	Context  con; 
	public Dialog dialog1;
	public Toast toast;
	public String colorname="",selectedhomeid="",deviceId,model,manuf,devversion,apiLevel,Homeid,Identity, InspectionType,status,
			picpath="";
	public String elev[]={"--Select--","Front Elevation","Right Elevation","Back Elevation","Left Elevation","Attic Elevation","Additional Elevation"};
	public String elevations[]={"--Select--","Front Elevation","Right Elevation","Back Elevation","Left Elevation","Attic Elevation","Additional Elevation","Exterior and Grounds","Interior","Additional Images"};
	
	
	public int wd,ht,identityval=0,mYear,mMonth,mDay,typeidentity=1,windmitinstypeval,k1=0,mDayofWeek,selmDayofWeek,hours,minutes,amorpm,seconds,ipAddress,
	Count,value;
	public  ProgressDialog pd;	
	public String datewithtime,alertcontent,alerttitle,newphone="",checkInspectorAvail="false",loadinsp_q="",elevname="";
	public Uri mCapturedImageURI;
	View v1;
	
	public CommonFunctions(Context cont)
	{
		con=cont;
		DateFormat df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
		
	}
	public void setRcvalue(TextView releasecode2) {
		// TODO Auto-generated method stub
		/*System.out.println("URL="+URL+"RC="+rcstr);
		if(URL.contains("72.15.221.153"))
		{
			releasecode2.setText(rcstr.replace("U", "L"));
		}
		else
		{
				releasecode2.setText(rcstr);
		}*/
	}
	public void ShowToast(String s) {
		
	/*	switch(i)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}*/
		 toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(R.id.text);
		tv.setTextColor(Color.parseColor("#000000")); 
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(s);
		//tv.setTextSize(Typeface.BOLD);
		
		//toast.setGravity(Gravity.BOTTOM, 10, 80);
		toast.setView(layout);
		fireLongToast();


	}
	private void fireLongToast() {

	    Thread t = new Thread() {
	        public void run() {
	            int count = 0;
	            try {
	                while (true && count < 10) {
	                    toast.show();
	                    sleep(3);
	                    count++;

	                    // do some logic that breaks out of the while loop
	                }
	            } catch (Exception e) {
	               
	            }
	        }
	    };
	    t.start();
	}
	public void gohome() {
		// TODO Auto-generated method stub
		this.con.startActivity(new Intent(this.con,HomeScreen.class));
		((Activity) this.con).finish();
	}
	public void getCalender() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
			mYear = c.get(Calendar.YEAR);System.out.println("yearss"+mYear);
			mMonth = c.get(Calendar.MONTH);
			mDay = c.get(Calendar.DAY_OF_MONTH);System.out.println("mDay"+mDay);
			
			mDayofWeek = c.get(Calendar.DAY_OF_WEEK);
			hours = c.get(Calendar.HOUR);
			minutes = c.get(Calendar.MINUTE);
			seconds = c.get(Calendar.SECOND);
			amorpm = c.get(Calendar.AM_PM);
	
	}
	public void showDialogDate(EditText edt) {
			// TODO Auto-generated method stub
		    getCalender();
			Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR);
	        int mMonth = c.get(Calendar.MONTH);
	        int mDay = c.get(Calendar.DAY_OF_MONTH);
	        DatePickerDialog dialog= new DatePickerDialog(con, new mDateSetListener(edt),
	                   mYear, mMonth, mDay);
	        dialog.show();
		}
	public class mDateSetListener implements DatePickerDialog.OnDateSetListener
	{
		EditText v;
		mDateSetListener(EditText v)
		{
			this.v=v;
		}
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			getCalender();
			mYear = year;
			mMonth = monthOfYear+1;
			mDay = dayOfMonth;
			selmDayofWeek = mDayofWeek ;
			
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(pad(mMonth)).append("/").append(pad(mDay)).append("/")
					.append(mYear).append(" "));
		}
		
	}
	public String pad(int c) {
		// TODO Auto-generated method stub		
		return c>=10 ? ""+c : "0"+c;   
	}
	public void getDeviceDimensions() {
		// TODO Auto-generated method stub
		    DisplayMetrics metrics = new DisplayMetrics();
			((Activity) this.con).getWindowManager().getDefaultDisplay().getMetrics(metrics);

			wd = metrics.widthPixels;
			ht = metrics.heightPixels;
	}
	
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = ((Activity) con).managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	public 	Bitmap ShrinkBitmap(String file, int width, int height) { try {
		BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
		bmpFactoryOptions.inJustDecodeBounds = true;
		Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
	
		int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
				/ (float) height);
		int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
				/ (float) width);
	
		if (heightRatio > 1 || widthRatio > 1) {
			if (heightRatio > widthRatio) {
				bmpFactoryOptions.inSampleSize = heightRatio;
			} else {
				bmpFactoryOptions.inSampleSize = widthRatio;
			}
		}
	
		bmpFactoryOptions.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
		return bitmap;
	} catch (Exception e) {
		return null;
	}
	
	}
	public void show_ProgressDialog(String string) {
		// TODO Auto-generated method stub
		String source = "<b><font color=#00FF33>"+string+"Please wait...</font></b>";
		pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
	}	
	public boolean Email_Validation(String string) {
		// TODO Auto-generated method stub
		
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(string);

		return matcher.matches();
	}
	public void hidekeyboard()	
	{
		InputMethodManager imm = (InputMethodManager)con.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);	    
	}
	public void showselectedimage(final String capturedImageFilePath) {
		
		/*currnet_rotated=0;
		 System.gc();
		// TODO Auto-generated method stub
		picpath=capturedImageFilePath;
		BitmapFactory.Options o = new BitmapFactory.Options();
	 		o.inJustDecodeBounds = true;
		
	 		dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			
			*//** get the help and update relative layou and set the visbilitys for the respective relative layout *//*
			LinearLayout Re=(LinearLayout) dialog1.findViewById(R.id.maintable);
			Re.setVisibility(View.GONE);
			LinearLayout Reup=(LinearLayout) dialog1.findViewById(R.id.updateimage);
			Reup.setVisibility(View.GONE);
			LinearLayout camerapic=(LinearLayout) dialog1.findViewById(R.id.cameraimage);
			camerapic.setVisibility(View.VISIBLE);
			tvcamhelp = (TextView)dialog1.findViewById(R.id.camtxthelp);
			tvcamhelp.setText("Save Picture");
			tvcamhelp.setTextSize(TypedValue.COMPLEX_UNIT_PX,14);
			spinvw = (RelativeLayout)dialog1.findViewById(R.id.cam_photo_update);
			capvw = (RelativeLayout)dialog1.findViewById(R.id.camaddcaption);
			
			*//** get the help and update relative layou and set the visbilitys for the respective relative layout *//*
			
			final ImageView upd_img = (ImageView) dialog1.findViewById(R.id.cameraimg);
			btn_helpclose = (Button) dialog1.findViewById(R.id.camhelpclose);
			btn_camcaptclos  = (Button) dialog1.findViewById(R.id.camcapthelpclose);
			Button btn_save = (Button) dialog1.findViewById(R.id.camsave);
			//Button btn_cancl= (Button) dialog1.findViewById(R.id.camdelete);
			Button rotatecamleft = (Button) dialog1.findViewById(R.id.rotatecamimgleft);
			Button rotatecamright = (Button) dialog1.findViewById(R.id.rotatecamright);
			elevspin = (Spinner) dialog1.findViewById(R.id.cameraelev);
			cameratxt = (TextView)dialog1.findViewById(R.id.captxt);
			captionspin = (Spinner) dialog1.findViewById(R.id.cameracaption);
			 adapter2 = new ArrayAdapter(this.con,android.R.layout.simple_spinner_item, elevnames);
			 
 			adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
 			elevspin.setAdapter(adapter2);
 			elevspin.setOnItemSelectedListener(new MyOnItemSelectedListener());
			try {
	 				BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath),
	 						null, o);
	 				final int REQUIRED_SIZE = 400;
		 			int width_tmp = o.outWidth, height_tmp = o.outHeight;
		 			int scale = 1;
		 			while (true) {
		 				if (width_tmp / 2 < REQUIRED_SIZE
		 						|| height_tmp / 2 < REQUIRED_SIZE)
		 					break;
		 				width_tmp /= 2;
		 				height_tmp /= 2;
		 				scale *= 2;
		 				BitmapFactory.Options o2 = new BitmapFactory.Options();
			 			o2.inSampleSize = scale;
			 			Bitmap bitmap = null;
			 	    	bitmap = BitmapFactory.decodeStream(new FileInputStream(
			 	    		capturedImageFilePath), null, o2);
			 	       BitmapDrawable bmd = new BitmapDrawable(bitmap);
		 			   upd_img.setImageDrawable(bmd);
		 			}
	 		
	 		} catch (FileNotFoundException e) {
	 			System.out.println("Issues "+e.getMessage());
	 		}
			
			btn_save.setOnClickListener(new OnClickListener() {


	            public void onClick(View v) {

	            	System.out.println("elevspin.getSelectedItem().toString "+elevspin.getSelectedItem().toString());System.out.println("SSSSSSSSS "+spinelevval);
	            	if(!elevspin.getSelectedItem().toString().equals("Select Elevation"))
	            	{
	            		if(elevspin.getSelectedItem().toString().equals("Front Elevation")) {		maxindb=8;elev_name="FE "; }
	            		else if(elevspin.getSelectedItem().toString().equals("Right Elevation")) {	maxindb=8;	elev_name="RE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Back Elevation")){	maxindb=8;	elev_name="BE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Left Elevation"))	{ 	maxindb=8;	elev_name="LE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Attic Elevation")) { 	maxindb=8;	elev_name="AE ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Additional Elevation")){	elev_name="Additional Photo "; maxindb=20;}
	            		else if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds"))	{ 	maxindb=8;	elev_name="Exterior and Grounds ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Interior")) { 	maxindb=8;	elev_name="Interior ";}
	            		else if(elevspin.getSelectedItem().toString().equals("Additional Images")){	elev_name="Additional "; maxindb=8;}
	            		
	            		String[] bits = picpath.split("/");
						String	picname = bits[bits.length - 1];
						System.out.println("getSelectedItem "+captionspin.getSelectedItem().toString());
						System.out.println("elevspin "+elevspin.getSelectedItem().toString());
						if(captionspin.getSelectedItem().toString().equals("Select")|| captionspin.getSelectedItem().toString().equals("SELECT") || captionspin.getSelectedItem().toString().equals("ADD PHOTO CAPTION"))
	            		{
							ShowToast("Please add Photo Caption.", 0);
							
	            		}
	            		else
	            		{		
	            			
	            			if(elevspin.getSelectedItem().toString().equals("Front Elevation") || elevspin.getSelectedItem().toString().equals("Right Elevation") 
	            					|| elevspin.getSelectedItem().toString().equals("Back Elevation") ||  elevspin.getSelectedItem().toString().equals("Left Elevation")
	            					|| elevspin.getSelectedItem().toString().equals("Additional Elevation") ||  elevspin.getSelectedItem().toString().equals("Attic Elevation"))
	            			
	            			{	
			            				int j;
			            				Cursor c11 = db.rawQuery("SELECT * FROM " + ImageTable
				        	 				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval
				        	 				+ "' and Delflag=0 order by CreatedOn DESC", null);
						        	 		int imgrws = c11.getCount();
						        	 		
						        	 		if (imgrws == 0) {
						        	 			j = imgrws + 1;
						        	 		} else {
						        	 			j = imgrws + 1;
					
						        	 		}
					        	 			if (imgrws > maxindb) 
					        	 			{								
					        	 				ShowToast toast = new ShowToast(con,"You are allowed to select only "+maxindb+" Images per Elevation.");
					        	 				elevspin.setSelection(0);
					        	 			}
					        	 			else 
					        	 			{
													db.execSQL("INSERT INTO "
															+ ImageTable
															+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
															+ " VALUES ('"+ Homeid+ "','"+ quesid+ "','"+ spinelevval+ "','"+ convertsinglequotes(picpath)+ "','"
															+ convertsinglequotes(newspin) + "','"+ convertsinglequotes(picname)+ "','" + datewithtime + "','" + datewithtime + "','"+ j + "','" + 0 + "')");
					        	 			}  
	            			}
	            			else
	            			{
	            				if(elevspin.getSelectedItem().toString().equals("Exterior and Grounds")){spinelevval=1;}
	            				else if(elevspin.getSelectedItem().toString().equals("Interior")){spinelevval=2;}
	            				else if(elevspin.getSelectedItem().toString().equals("Additional Images")){spinelevval=3;}
	            				
	            				System.out.println("saving camera images");
	            				int j;
	            				
	            				Cursor c11 = db.rawQuery("SELECT * FROM " + HazardImageTable
		        	 				+ " WHERE SrID='" + Homeid + "' and Elevation='" + spinelevval
		        	 				+ "' and Delflag=0 order by CreatedOn DESC", null);
				        	 		int imgrws = c11.getCount();
				        	 		
				        	 		if (imgrws == 0) {
				        	 			j = imgrws + 1;
				        	 		} else {
				        	 			j = imgrws + 1;
			
				        	 		}
			        	 			if (imgrws==8) 
			        	 			{								
			        	 				ShowToast toast = new ShowToast(con,"You are allowed to select only "+maxindb+" Images per Elevation.");
			        	 				elevspin.setSelection(0);
			        	 			}          
			        	 			else 
			        	 			{
										
											db.execSQL("INSERT INTO "
													+ HazardImageTable
													+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
													+ " VALUES ('"+ Homeid+ "','"+ quesid+ "','"+ spinelevval+ "','"+ convertsinglequotes(picpath)+ "','"
													+ convertsinglequotes(newspin) + "','"+ convertsinglequotes(picname)+ "','" + datewithtime + "','" + datewithtime + "','"+ j + "','" + 0 + "')");
			        	 			
											System.out.println("saving camera images"+"INSERT INTO "
													+ HazardImageTable
													+ " (SrID,Ques_Id,Elevation,ImageName,Description,Nameext,CreatedOn,ModifiedOn,ImageOrder,Delflag)"
													+ " VALUES ('"+ Homeid+ "','"+ quesid+ "','"+ spinelevval+ "','"+ convertsinglequotes(picpath)+ "','"
													+ convertsinglequotes(newspin) + "','"+ convertsinglequotes(picname)+ "','" + datewithtime + "','" + datewithtime + "','"+ j + "','" + 0 + "')");
			        	 			}   
	            			}
			            			
	            			
	            			*//**Save the rotated value in to the external stroage place **//*
							if(currnet_rotated>0)
							{ 

								try
								{
									*//**Create the new image with the rotation **//*
									String current=MediaStore.Images.Media.insertImage(con.getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
									
									 ContentValues values = new ContentValues();
									  values.put(MediaStore.Images.Media.ORIENTATION, 0);
									  con.getContentResolver().update(Uri.parse(current), values, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
									
									if(current!=null)
									{
									String path=getPath(Uri.parse(current));
									File fout = new File(capturedImageFilePath);
									fout.delete();
									*//** delete the selected image **//*
									File fin = new File(path);
									*//** move the newly created image in the slected image pathe ***//*
									fin.renameTo(new File(capturedImageFilePath));
									
									
								}
								} catch(Exception e)
								{
									System.out.println("Error occure while rotate the image "+e.getMessage());
								}
								
							}
						
						*//**Save the rotated value in to the external stroage place ends **//*	
							
							ShowToast("\"Taken photo successfully saved into your "+elevspin.getSelectedItem().toString()+" . \"",1);
							dialog1.dismiss();  
	            			
	            			
	            		}
	            }
	            	else
	            	{
	            		ShowToast("Please select Elevation type.", 0);
	            	}
	            }
			});
			
			btn_helpclose.setOnClickListener(new OnClickListener() {
	            public void onClick(View v) {
	            	dialog1.dismiss();
	            }
			});
			rotatecamright.setOnClickListener(new OnClickListener() {  
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					System.gc();
					currnet_rotated+=90;
					if(currnet_rotated>=360)
					{
						currnet_rotated=0;
					}
					
					Bitmap myImg;
					try {
						myImg = BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath));
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
					
						matrix.postRotate(currnet_rotated);
						
						 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
						        matrix, true);
						 System.gc();
						 upd_img.setImageBitmap(rotated_b);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						
					}
					catch (OutOfMemoryError e) {
						System.gc();
						try {
							myImg=null;
							System.gc();
							Matrix matrix =new Matrix();
							matrix.reset();
							//matrix.setRotate(currnet_rotated);
							matrix.postRotate(currnet_rotated);
							myImg= ShrinkBitmap(capturedImageFilePath, 800, 800);
							rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
							System.gc();
							upd_img.setImageBitmap(rotated_b); 

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 catch (OutOfMemoryError e1) {
								// TODO Auto-generated catch block
							ShowToast("You can not rotate this image. Image size is too large.",1);
						}
					}

				}
			});
			rotatecamleft.setOnClickListener(new OnClickListener() {  
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					System.gc();
					 currnet_rotated-=90;
						if(currnet_rotated<0)
						{
							currnet_rotated=270;
						}	
					Bitmap myImg;
					try {
						myImg = BitmapFactory.decodeStream(new FileInputStream(capturedImageFilePath));
						Matrix matrix =new Matrix();
						matrix.reset();
						//matrix.setRotate(currnet_rotated);
					
						matrix.postRotate(currnet_rotated);
						
						 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
						        matrix, true);
						 System.gc();
						 upd_img.setImageBitmap(rotated_b);

					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					catch (Exception e) {
						
					}
					catch (OutOfMemoryError e) {
						System.gc();
						try {
							myImg=null;
							System.gc();
							Matrix matrix =new Matrix();
							matrix.reset();
							//matrix.setRotate(currnet_rotated);
							matrix.postRotate(currnet_rotated);
							myImg= ShrinkBitmap(capturedImageFilePath, 800, 800);
							rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(), matrix, true);
							System.gc();
							upd_img.setImageBitmap(rotated_b); 

						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 catch (OutOfMemoryError e1) {
								// TODO Auto-generated catch block
							ShowToast("You can not rotate this image. Image size is too large.",1);
						}
					}

				}
			});
			dialog1.setCancelable(false);
			dialog1.show();*/
				
			
	}
	public void setFocus(EditText ed) {
		// TODO Auto-generated method stub
		ed.setFocusableInTouchMode(true);
		ed.requestFocus();
		ed.setSelection(ed.getText().length()); //For set the cursor at the end of the selection
	}
	public boolean checkfortodaysdate(String string) {
		// TODO Auto-generated method stub
		int i1 = string.indexOf("/");
		String result = string.substring(0, i1);
		int i2 = string.lastIndexOf("/");
		String result1 = string.substring(i1 + 1, i2);
		String result2 = string.substring(i2 + 1);
		result2 = result2.trim();
		int j1 = Integer.parseInt(result);
		int j2 = Integer.parseInt(result1);
		int j = Integer.parseInt(result2);
		final Calendar c = Calendar.getInstance();
		int thsyr = c.get(Calendar.YEAR);
		int curmnth = c.get(Calendar.MONTH);
		int curdate = c.get(Calendar.DAY_OF_MONTH);
		int day = c.get(Calendar.DAY_OF_WEEK);
		curmnth = curmnth + 1;

		if (j > thsyr || (j1 > curmnth && j >= thsyr)
				|| (j2 >= curdate && j1 >= curmnth && j >= thsyr)) {
			return true;
		} else {
			return false;
		}
	}
	public void showhelp(String alerttitle,String alertcontent) {
		// TODO Auto-generated method stub
		
		final Dialog dialog = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.getWindow().setContentView(R.layout.alert);
		
		LinearLayout l1 = (LinearLayout) dialog.findViewById(R.id.helpalert);
		l1.setVisibility(v1.VISIBLE);
		TextView txt = (TextView) l1.findViewById(R.id.txtid);
		TextView helptxt = (TextView) dialog.findViewById(R.id.txthelp);
		
		txt.setText(Html.fromHtml(alertcontent));
		helptxt.setText(Html.fromHtml(alerttitle));
		Button btn_helpclose = (Button) dialog.findViewById(R.id.helpclose);
		btn_helpclose.setOnClickListener(new OnClickListener()
		{
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
			
		});
		LinearLayout l2 = (LinearLayout) dialog.findViewById(R.id.camera);
		l2.setVisibility(v1.GONE);
		dialog.setCancelable(false);
		dialog.show();
		
	}

	public void setTouchListener(View v) {
		// TODO Auto-generated method stub
		if(v instanceof ViewGroup)
		{
			ViewGroup vg=(ViewGroup)v;
			for(int i=0;i<vg.getChildCount();i++)
			{
				if((vg.getChildAt(i)) instanceof ViewGroup)
				{
					setTouchListener(vg.getChildAt(i));
				}
				else if((vg.getChildAt(i)) instanceof EditText)
				{
					EditText ed=(EditText)(vg.getChildAt(i));
					ed.setOnTouchListener(new TouchFoucs(ed));
					
				}
			}
		}
	}
	public void Device_Information()
	{
		 
		deviceId = Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		ipAddress = wifiInfo.getIpAddress();
	}

	public String PhoneNo_validation(String decode) {
		// TODO Auto-generated method stub
		if (decode.length() == 13) {
			StringBuilder sVowelBuilder = new StringBuilder(decode);
			sVowelBuilder.insert(0, "(");
			sVowelBuilder.insert(4, ")");
			sVowelBuilder.insert(8, "-");
			newphone = sVowelBuilder.toString();
			System.out.println("fdsfdsfyes");
			return "Yes";
            
		} else {
			System.out.println("fdsfdsfno");
			return "No";
		}
	}
	 public void Error_LogFile_Creation(String strerrorlog2)
	    {
	        try {
	 			
	 			 DataOutputStream out = new DataOutputStream(this.con.openFileOutput("errorlogfile.txt", this.con.MODE_APPEND));
	 			 out.writeUTF(strerrorlog2);
	 			 
	    	 }
	    	 catch(Exception e)
	    	 {
	    		 
	    	 }
	    }
	 public Intent loadComments(String string,int loc[]) {
			// TODO Auto-generated method stub
			System.out.println("loadinsp_n= "+string);
			Intent in =new Intent(this.con,LoadComments.class);
		 	in.putExtra("insp_ques", loadinsp_q);
			in.putExtra("insp_opt", string);
			in.putExtra("xfrom", loc[0]+10);
			in.putExtra("yfrom", loc[1]+10);
		   return in;
		}	 
	 public void setvaluerd(RadioGroup radioGroup, String string) {
			// TODO Auto-generated method stub
			for(int i=0;i<radioGroup.getChildCount();i++)
			{
				if(radioGroup.getChildAt(i) instanceof RadioButton)
				{
					if(((RadioButton)radioGroup.getChildAt(i)).getText().toString().trim().equals(string))
					{
						((RadioButton)radioGroup.getChildAt(i)).setChecked(true);
					}
				}
			}
		}
	 public void hidekeyboard(EditText editText){
	        InputMethodManager imm = (InputMethodManager)con.getSystemService(Context.INPUT_METHOD_SERVICE);
	        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
	    }
	 public void getElevationname(int pos) {
			// TODO Auto-generated method stub
			if(pos==1)
			{
				elevname = "FE";
			}
			else if(pos==2)
			{
				elevname = "RE";
			}
			else if(pos==3)
			{
				elevname = "BE";
			}
			else if(pos==4)
			{
				elevname = "LE";
			}
			else if(pos==5)
			{
				elevname = "AE";
			}
			else if(pos==6)
			{
				elevname = "Additional Photo";
			}
			else if(pos==7)
			{
				elevname = "EXT";
			}
			else if(pos==8)
			{
				elevname = "INT";
			}
			else if(pos==9)
			{
				elevname = "AI";
			}

		}
	public void showalert(String alerttitle2, String alertcontent2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(this.con,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.getWindow().setContentView(R.layout.alert);
		TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);System.out.println("txthelp");
		txttitle.setText(alerttitle2);
		TextView txt = (TextView) dialog1.findViewById(R.id.txtid);System.out.println("dfdf");
		txt.setText(alertcontent2);
		Button btn_helpclose = (Button) dialog1.findViewById(R.id.helpclose);System.out.println("helpclose");
		btn_helpclose.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
			}
			
		});
		Button btn_ok = (Button) dialog1.findViewById(R.id.ok);
		btn_ok.setVisibility(v1.VISIBLE);
		btn_ok.setOnClickListener(new OnClickListener()
		{
		public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
				con.startActivity(new Intent(con,Dashboard.class));
				
			}
			
		});
		
		dialog1.setCancelable(false);
		dialog1.show();
	}
	 public void getExtras(Bundle extras) {
			// TODO Auto-generated method stub
			Homeid =  extras.getString("homeid");
			status = extras.getString("status");
		    
		}
}