package idsoft.inspectiondepot.windmitinspection;

import android.graphics.Color;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class textWatchLimits implements TextWatcher
{
	EditText ed;
	int length; 
	TextView textView;
	
	public textWatchLimits(EditText ed, int max_length, TextView showing_textview)
	{
		this.ed=ed;
		this.length=max_length;
		this.textView=showing_textview;
	}

	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub
		int string_len=ed.getText().toString().length();
		if(string_len>0)
		{
			int percent=(int)((string_len)*(100.00/Double.parseDouble(String.valueOf(length))));
		    
		    if(string_len==length)
		    {
		    	textView.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
		    	textView.setTextColor(Color.WHITE);
		    	
		    }
		    else
		    {
		    	textView.setText("   "+percent+" %");
		    	textView.setTextColor(Color.parseColor("#E77D00"));
		    }
		}
		else
		{
			textView.setText("0%");
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before,
			int count) {
		// TODO Auto-generated method stub
		
	}
	
}
     