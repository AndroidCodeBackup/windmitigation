package idsoft.inspectiondepot.windmitinspection;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import java.util.Locale;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Export extends Activity {
	ScrollView sv;Button deleteallinspections;
	String alerttitle="",path,SRID;
	LinearLayout dynamic,llheader,llfooter;
	EditText etpage;
	TextView tvshowingresults,tvnoofpages;
	Button btngotopage;
	ProgressDialog pd;
	ProgressDialog pd2;
	String countarr[],data[],datasend[],ownersname[],email[],srid[];
	int show_handler3,show_handler,tot;
	String colomnname="",classidentifier="";
	String[] homelist = new String[100];
	String[] pdfpath;	
	int value = 0, Count = 0, cnt, columnvalue, rws = 0;
	Cursor cur;View v1;
	String statusofdata, InspectionType, status = "", keyword, statuschk,
			query, strtype, strtit,dta = " ",s = "";
	private Button homepage, search, search_clear_txt;
	EditText search_text;
	TextView welcome, title,t;
	TextView tvstatus[];
	Button deletebtn[];
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	SoapObject resultrr;
	View v;
	Map<String, TextView> map1 = new LinkedHashMap<String, TextView>();
	int propertycount, i, j, total, remainder, totalrecords;
	int start = 0, end = 3, pagestart = 0, pageend = 10, pagenumber = 0;
	String identifier = "start";
	Cursor cursor;


	public void onCreate(Bundle bundle) {

		super.onCreate(bundle);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		
		Bundle b =getIntent().getExtras();
		if(b!=null)
		{
			status=b.getString("type");
			classidentifier=b.getString("classidentifier");
		}
		setContentView(R.layout.exporteddata);
		
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside");
				Intent insp_info = new Intent(Export.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		db.CreateTable(26);
		
		db.getInspectorId();
		declaration();
		RRInspectionsList();
	}

	private void RRInspectionsList() {
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing..."
					+ " </font></b>";
			final ProgressDialog pd = ProgressDialog.show(Export.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(wb.NAMESPACE,"ExportReportsready");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorId",Integer.parseInt(db.Insp_id));
						envelope.setOutputSoapObject(request);
						System.out.println("ExportReportsready request is "+request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
						androidHttpTransport.call(wb.NAMESPACE+"ExportReportsready",envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						System.out.println("ExportReportsready result is "+result);
						LoadExportReportsready(result);
						System.out.println("response ExportReportsready"
								+ result);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {

						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 5) {
							show_handler = 0;
							
							db.CreateTable(26);
							Cursor cur=db.wind_db.rawQuery("select * from "+db.ReportsReady +" Order by created_Date desc", null);
							int count=cur.getCount();System.out.println("counnt"+count);
							if(count>=1)
							{
								//t.setText("No of Records : "+count);
								dynamic.setVisibility(View.VISIBLE);
								llheader.setVisibility(View.VISIBLE);
								llfooter.setVisibility(View.VISIBLE);
								DynamicList(cur);
								
							}
							else
							{
								//t.setText("No of Records : 0");
								dynamic.setVisibility(View.GONE);		
								llheader.setVisibility(View.GONE);
								llfooter.setVisibility(View.GONE);		
							}
						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
			
		}
	}
	private void LoadExportReportsready(SoapObject result)
	{
		db.CreateTable(24);
	 	db.wind_db.execSQL("delete from "+db.ReportsReady);
		int count;
		if(String.valueOf(result).equals("null") || String.valueOf(result).equals(null) || String.valueOf(result).equals("anytype{}"))
		 {
				count=0;
				total = 100;
		  }
		 else
		 {
			 count = result.getPropertyCount();
			 for (int i = 0; i < count; i++) {
					SoapObject obj = (SoapObject) result.getProperty(i);
					String srid = String.valueOf(obj.getProperty("SRID"));
					String firstname = String.valueOf(obj.getProperty("FirstName"));
					String lastname = String.valueOf(obj.getProperty("LastName"));
					String address1 = String.valueOf(obj.getProperty("Address1"));
					String address2 = String.valueOf(obj.getProperty("Address2"));
					String state = String.valueOf(obj.getProperty("State"));
					String county = String.valueOf(obj.getProperty("Country"));
					String city = String.valueOf(obj.getProperty("City"));
					String zip = String.valueOf(obj.getProperty("Zip"));
					String email = String.valueOf(obj.getProperty("Email"));
					String status = String.valueOf(obj.getProperty("Status"));
					String policynumber = String.valueOf(obj.getProperty("OwnerPolicyNo"));
					String pdfpath = String.valueOf(obj.getProperty("Pdfpath"));
					String create_date =String.valueOf(obj.getProperty("AcceptedDate"));
					String Inspecteddate = String.valueOf(obj.getProperty("Inspecteddate"));					

					db.wind_db.execSQL("insert into "
							+ db.ReportsReady
							+ " (srid,firstname,lastname,address1,address2,state,county,city,zip,policyno,email,status,pdfpath,inspectiondate,created_Date) values('"
							+ db.encode(srid) + "','"
							+ db.encode(firstname) + "','"
							+ db.encode(lastname) + "','"
							+ db.encode(address1) + "','"
							+ db.encode(address2) + "','"
							+ db.encode(state) + "','"
							+ db.encode(county) + "','"
							+ db.encode(city) + "','"
							+ db.encode(zip) + "','"
							+ db.encode(policynumber) + "','"
							+ db.encode(email) + "','"
							+ db.encode(status) + "','" + db.encode(pdfpath)
							+ "','"+db.encode(Inspecteddate)+"','"+create_date+"');");
					
			 }
		 }
	}
	private void declaration() {
		// TODO Auto-generated method stub
		

		tvshowingresults = (TextView) findViewById(R.id.export_tvshowingresults);
		tvnoofpages = (TextView) findViewById(R.id.export_tvnoofpages);
		llheader = (LinearLayout) findViewById(R.id.export_llheader);
		llfooter = (LinearLayout) findViewById(R.id.export_llfooter);
		etpage = (EditText) findViewById(R.id.export_etgotopage);
		btngotopage = (Button) findViewById(R.id.export_buttongotopage);
		
		etpage.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etpage.getText().toString().startsWith(" "))
		        {
		            // Not allowed
					etpage.setText("");
		        }
				if (etpage.getText().toString().trim().matches("^0") )
		        {
		            // Not allowed
					etpage.setText("");
		        }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		btngotopage.setOnClickListener(new OnClickListener() {
		
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cf.hidekeyboard(etpage);
				String value = etpage.getText().toString().trim();
				if (!value.equals("")) {
					int val = Integer.parseInt(value);
					if (Integer.parseInt(value) <= total
							&& Integer.parseInt(value) != 0) {
						if (val >= pagestart && val <= pageend) {
							Go_To_Page(value);
						} else {
							if (val > pageend) {
								for (int i = 0; i < total; i++) {
									pagestart = pagestart + 1;
									pageend = pageend + 1;
									boolean check = Check_Goto_Condition();
									if (check == true) {
										Go_To_Page(value);
										break;
									}
								}
							} else if (val < pagestart) {
								System.out
										.println("inside page start condition");
								for (int i = 0; i < total; i++) {
									pagestart = pagestart - 1;
									pageend = pageend - 1;
									boolean check = Check_Goto_Condition();
									if (check == true) {
										Go_To_Page(value);
										break;
									}
								}
							}
						}
					} else {
						cf.ShowToast("Out of page");
					}
				} else {
					cf.ShowToast("Please enter a value");
				}
		
			}
		});



		TextView note  = (TextView) findViewById(R.id.note);
		//t  = (TextView) findViewById(R.id.tvrows);
		String headernote="FIRST NAME LAST NAME | ADDRESS1 | CITY | STATE | COUNTY | ZIP | INSPECTION DATE | POLICY NO | EMAIL";
		note.setText(headernote);
		
		dynamic = (LinearLayout) findViewById(R.id.linlayoutdyn);
		
	}
	private boolean Check_Goto_Condition() {
		String value = etpage.getText().toString().trim();
		int val = Integer.parseInt(value);
		if (val > pagestart && val <= pageend) {
			return true;
		} else {
			return false;
		}
	}

	private void Go_To_Page(String value) {
		if (Integer.parseInt(value) <= total && Integer.parseInt(value) != 0) {
			end = Integer.parseInt(value) * 3;
			start = end - 3;
			display_record(start, end);
			pagenumber = Integer.parseInt(value) - 1;
			if (pagenumber == total) {
				pagenumber = pagenumber - 1;
			}
			System.out.println("the page from text view is " + pagenumber);
			identifier = "gotobtn";
			Show_Page_Numbers(pagestart, pageend);
		} else {
			cf.ShowToast("Out of page");
		}
	}

	
	private void DynamicList(Cursor cur) {
		cursor=cur;
		
		totalrecords = cur.getCount();
		if(totalrecords<3)
		{
			total=1;
		}
		else
		{
			total = totalrecords / 3;
			remainder = totalrecords % 3;
			if (remainder != 0) {
				total = total + 1;
			}
		}
		
		
		System.out.println("total/remainder is "+total);

		ownersname = new String[totalrecords];
		data = new String[totalrecords];
		datasend = new String[totalrecords];
		pdfpath = new String[totalrecords];
		email = new String[totalrecords];
		srid = new String[totalrecords];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {

				SRID = db.decode(cur.getString(cur
						.getColumnIndex("srid")));
				srid[i]=SRID;
				String FirstName = db.decode(cur.getString(cur
						.getColumnIndex("firstname")));
				data[i] = " " + FirstName + " ";
				ownersname[i] = FirstName + " ";
				String LastName = db.decode(cur.getString(cur
						.getColumnIndex("lastname")));
				data[i] += LastName + " | ";
				ownersname[i] += " " + FirstName;
				String Address1 = db.decode(cur.getString(cur
						.getColumnIndex("address1")));
				data[i] += Address1 + " | ";
				
				String City = db.decode(cur.getString(cur
						.getColumnIndex("city")));
				data[i] += City + " | ";
				String State = db.decode(cur.getString(cur
						.getColumnIndex("state")));
				data[i] += State + " | ";
				String Country = db.decode(cur.getString(cur
						.getColumnIndex("county")));
				data[i] += Country + " | ";
				String Zip = db
						.decode(cur.getString(cur.getColumnIndex("zip")));
				data[i] += Zip + " | ";
				String inspectiondate = db
						.decode(cur.getString(cur.getColumnIndex("inspectiondate")));
				data[i] += inspectiondate + " | ";
				String OwnerPolicyNo = db.decode(cur.getString(cur
						.getColumnIndex("policyno")));
				data[i] += OwnerPolicyNo + " | ";
				datasend[i] = OwnerPolicyNo + ":";
				
				String Email = db.decode(cur.getString(cur
						.getColumnIndex("email")));
				data[i] += Email;
				email[i]=Email;
				
				String Status = db.decode(cur.getString(cur
						.getColumnIndex("status")));
				datasend[i] += Status;
				String COMMpdf = db.decode(cur.getString(cur
						.getColumnIndex("pdfpath")));
				pdfpath[i] = COMMpdf;

				System.out.println("The COMMpdf path is " + COMMpdf);

				System.out.println("The Datas is " + data[i]+"srid[i]"+srid[i]);
				i++;
			} while (cur.moveToNext());

		}
		
		display_record(start, end);
		Show_Page_Numbers(pagestart, pageend);


	}
	private void Show_Page_Numbers(int ps,int pe)
	{
		llfooter.removeAllViews();
		
		System.out.println("the page number from starting layout is "+pagenumber);
		
		if(identifier.equals("next"))
		{
			pagenumber=pagenumber+1;
			if(pagenumber==total)
			{
				pagenumber=pagenumber-1;
			}
		}
		else if(identifier.equals("previous"))
		{
			pagenumber=pagenumber-1;
		}
		
		if(pe>total)
		{
			pe=total;
		}
		final TextView[] tvpageno=new TextView[totalrecords];
		
		Button btnprevious=new Button(this);
		btnprevious.setLayoutParams(new LayoutParams(100,ViewGroup.LayoutParams.WRAP_CONTENT));
		btnprevious.setText("Previous");
		btnprevious.setBackgroundResource(R.drawable.mybutton);
		//btnprevious.setTextColor(0xff535861);
		btnprevious.setTextColor(Color.WHITE);
		llfooter.addView(btnprevious);
		
		btnprevious.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(start!=0)
				{
					dynamic.removeAllViews();
					start=start-3;
					end=end-3;
					System.out.println("the start is "+start);
					System.out.println("the end is "+end);
					if(start<0)
					{
						start=0;
					}
					dynamic.removeAllViews();
					display_record(start, end);
					
					if(pagenumber==pagestart)
					{
						pagestart=pagestart-1;
						pageend=pageend-1;
					}
					identifier="previous";
					System.out.println("the page no is "+pagenumber);
					llfooter.removeAllViews();
					Show_Page_Numbers(pagestart,pageend);
				}
			}
		});
		
		LinearLayout.LayoutParams tvparams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
		tvparams.setMargins(10, 0, 10, 0);
		
		for(j=ps;j<pe;j++)
		{
			
			if(j<total)
			{
				tvpageno[j]=new TextView(this);
				tvpageno[j].setLayoutParams(tvparams);
				tvpageno[j].setText(String.valueOf(j+1));
				tvpageno[j].setTag(j+1);
				//tvpageno[j].setTextColor(0xff535861);
				tvpageno[j].setTextColor(Color.WHITE);
				tvpageno[j].setTextSize(16);
				llfooter.addView(tvpageno[j]);
				
				tvpageno[j].setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String value=v.getTag().toString();
						
						if(Integer.parseInt(value)<=total&&Integer.parseInt(value)!=0)
						{
							end=Integer.parseInt(value)*3;
							start=end-3;
							display_record(start,end);
							int val=Integer.parseInt(value)-1;
							pagenumber=val;
							System.out.println("the page from text view is "+pagenumber);
							identifier="textview";
							Show_Page_Numbers(pagestart,pageend);
						}
					}
				});
				
			}
		}
		
		System.out.println("ps is "+ps);
		System.out.println("pe is "+pe);
		System.out.println("the pagenumber is "+pagenumber);
		
		tvpageno[pagenumber].setTypeface(null, Typeface.BOLD);
		tvpageno[pagenumber].setTextColor(Color.parseColor("#C35817"));
		
		
		Button btnnext=new Button(this);
		btnnext.setLayoutParams(new LayoutParams(100,ViewGroup.LayoutParams.WRAP_CONTENT));
		btnnext.setText("Next");
		btnnext.setBackgroundResource(R.drawable.mybutton);
		//btnnext.setTextColor(0xff535861);
		btnnext.setTextColor(Color.WHITE);
		llfooter.addView(btnnext);
		
		btnnext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(end<totalrecords)
				{
					start=end;
					end=end+3;
					System.out.println("the start is "+start);
					System.out.println("the end is "+end);
					System.out.println("the totalrecords is "+totalrecords);
					dynamic.removeAllViews();
					display_record(start,end);
					
					System.out.println("the pageend is "+pageend);
					System.out.println("the pagenumber is "+pagenumber);
					if(pagenumber+1==pageend)
					{
						pagestart=pagestart+1;
						pageend=pageend+1;
						llfooter.removeAllViews();
						identifier="next";
						Show_Page_Numbers(pagestart,pageend);
					}
					else
					{
						llfooter.removeAllViews();
						identifier="next";
						Show_Page_Numbers(pagestart,pageend);
					}
				}
				
			}
		});
	}
	
	private void display_record(int start, int end) {

		dynamic.removeAllViews();
		ScrollView sv = new ScrollView(this);
		dynamic.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

//		Cursor cur = dbh.db
//				.rawQuery("select * from " + dbh.HomeBuyerInspection, null);
		System.out.println("total record is "+cursor.getCount());
		int rows = end;
		
		if(end>cursor.getCount())
		{
			end=cursor.getCount();
		}
		
		tvshowingresults.setText("Showing record "+(start+1)+" to "+end+" of "+totalrecords);
		
		
		tvnoofpages.setText("Total no of pages : "+total);
		System.out.println("the rows is "+rows);

		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] pter = new Button[rows];
		final Button[] sendmail = new Button[rows];
		LinearLayout[] rl = new LinearLayout[rows];
		RelativeLayout[] rlparent = new RelativeLayout[rows];

		for (int i = start; i <end; i++) {
			
			LinearLayout.LayoutParams llparams;
			LinearLayout.LayoutParams lltxtparams;

			llparams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT);
			llparams.setMargins(20, 0, 20, 0);

			rl[i] = new LinearLayout(this);
			l1.addView(rl[i], llparams);

			lltxtparams = new LinearLayout.LayoutParams(560,ViewGroup.LayoutParams.WRAP_CONTENT);
			lltxtparams.setMargins(20, 30, 20, 30);

			tvstatus[i] = new TextView(this);
			tvstatus[i].setLayoutParams(lltxtparams);
			tvstatus[i].setId(1);
			tvstatus[i].setText(data[i]);
			tvstatus[i].setTextColor(Color.WHITE);
			tvstatus[i].setTextSize(14);
			rl[i].addView(tvstatus[i]);

			LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
					110, ViewGroup.LayoutParams.WRAP_CONTENT);
			downloadparams1.setMargins(0, 0, 20, 0);
			downloadparams1.gravity = Gravity.CENTER_VERTICAL;

			LinearLayout.LayoutParams sendmailparams = new LinearLayout.LayoutParams(
					110, ViewGroup.LayoutParams.WRAP_CONTENT);
			sendmailparams.setMargins(110, 0, 20, 0);
			sendmailparams.gravity = Gravity.CENTER_VERTICAL;

			sendmail[i] = new Button(this);
			sendmail[i].setLayoutParams(sendmailparams);
			sendmail[i].setId(2);
			sendmail[i].setText("Send Mail");
			sendmail[i].setBackgroundResource(R.drawable.mybutton);
			sendmail[i].setTextColor(0xffffffff);
			sendmail[i].setTextSize(14);
			//sendmail[i].setTypeface(null, Typeface.BOLD);
			sendmail[i].setTag(srid[i]);
			rl[i].addView(sendmail[i]);

			view[i] = new Button(this);
			view[i].setLayoutParams(downloadparams1);
			view[i].setId(2);
			view[i].setText("View PDF");
			view[i].setBackgroundResource(R.drawable.buttonstyle);
			view[i].setTextColor(0xffffffff);
			view[i].setTextSize(14);
			//view[i].setTypeface(null, Typeface.BOLD);
			view[i].setTag(i);
			rl[i].addView(view[i]);
			

			pter[i] = new Button(this);
			pter[i].setLayoutParams(downloadparams1);
			pter[i].setId(2);
			pter[i].setText("Email Report");
			pter[i].setBackgroundResource(R.drawable.buttonstyle);
			pter[i].setTextColor(0xffffffff);
			pter[i].setTextSize(14);
			//pter[i].setTypeface(null, Typeface.BOLD);
			pter[i].setTag(i + ":" + ownersname[i] + ":"+srid[i]);
			rl[i].addView(pter[i]);

			Button edit = new Button(this);
			edit.setLayoutParams(downloadparams1);
			edit.setText("Regenerate");
			edit.setTextColor(0xffffffff);
			edit.setBackgroundResource(R.drawable.buttonstyle);
			edit.setTextSize(14);
			edit.setTypeface(null, Typeface.BOLD);
			edit.setTag(srid[i]);
			rl[i].addView(edit);
			
			if (pdfpath[i].equals("N/A")) {
				sendmail[i].setVisibility(View.VISIBLE);
				view[i].setVisibility(View.GONE);
				pter[i].setVisibility(View.GONE);
			} else {
				sendmail[i].setVisibility(View.GONE);
				view[i].setVisibility(View.VISIBLE);
				pter[i].setVisibility(View.VISIBLE);

			}
			
				edit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(final View v) {
					// TODO Auto-generated method stub
					if(wb.isInternetOn())
					{
						Cursor c =db.SelectTablefunction(db.ReportsReady, " WHERE srid='"+v.getTag().toString()+"'");
						c.moveToFirst();
						
						path = db.decode(c.getString(c.getColumnIndex("pdfpath")));
						String[] filenamesplit = path.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						File sdDir = new File(Environment
								.getExternalStorageDirectory().getPath());
						File file = new File(sdDir.getPath()
								+ "/DownloadedPdfFile/" + filename);
					
						//cf.go_back(Import.class);
						if(file.exists())
						{
							file.delete();
						}
						/*Intent in = new Intent(Export.this,Reterive_data.class);System.out.println("cc="+v.getTag().toString());
						in.putExtra("SRID", v.getTag().toString());
						startActivityForResult(in, 22);
						finish();*/
						
					}
					else
					{
						cf.ShowToast("Please enable internet connection");
					}
						
					
					
					 final Dialog dialog1 = new Dialog(Export.this,android.R.style.Theme_Translucent_NoTitleBar);
						dialog1.getWindow().setContentView(R.layout.alert);
						//dialog1.findViewById(R.id.maintable).setVisibility(View.GONE);
						RelativeLayout li=(RelativeLayout)dialog1.findViewById(R.id.helprow);
						li.setVisibility(View.VISIBLE);
						TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
						txttitle.setText("Edit Report");
						
						LinearLayout lin=(LinearLayout)dialog1.findViewById(R.id.camera);
						lin.setVisibility(v1.VISIBLE);
						
						TextView txt = (TextView) dialog1.findViewById(R.id.txtquestio);
						txt.setText("Do you want to edit and re-generate the selected report?");
						((EditText) dialog1.findViewById(R.id.ed_values)).setVisibility(View.GONE);
						
						Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
						btn_helpclose.setOnClickListener(new OnClickListener()
						{
						public void onClick(View arg0) {
								// TODO Auto-generated method stub
								dialog1.dismiss();
								//finish();
							}
							
						});

						Button btnclear = (Button) dialog1.findViewById(R.id.clear);
						btnclear.setText("No");								
						
						
						Button btn_ok = (Button) dialog1.findViewById(R.id.save);
						btn_ok.setText("Yes");
				
						btnclear.setOnClickListener(new OnClickListener()
						{
							public void onClick(View arg0)
							{
								dialog1.dismiss();
							}
						});


						btn_ok.setOnClickListener(new OnClickListener()
						{
						public void onClick(View arg0) {
								// TODO Auto-generated method stub
							if(wb.isInternetOn())
							{
								dialog1.dismiss();
								String SRID=v.getTag().toString();System.out.println("DSfdsf"+SRID);
							
								Cursor c =db.SelectTablefunction(db.ReportsReady, " WHERE srid='"+SRID+"'");
								c.moveToFirst();
								
								path = db.decode(c.getString(c.getColumnIndex("pdfpath")));
								String[] filenamesplit = path.split("/");
								final String filename = filenamesplit[filenamesplit.length - 1];
								File sdDir = new File(Environment
										.getExternalStorageDirectory().getPath());
								File file = new File(sdDir.getPath()
										+ "/DownloadedPdfFile/" + filename);
								System.out.println(" the path"+sdDir.getPath()
										+ "/DownloadedPdfFile/" + filename);
								//cf.go_back(Import.class);
								if(file.exists())
								{
									file.delete();
								}
								Intent in = new Intent(Export.this,Reterive_data.class);
								in.putExtra("homeid", SRID);System.out.println("dsdd"+SRID);
								startActivityForResult(in, 22);
								//finish();
								
							}
							else
							{
								cf.ShowToast("Please enable internet connection then try import");
							}
							}
						
						});
						
						dialog1.setCancelable(false);
						dialog1.show();
						
				}
				});

			view[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Button b = (Button) v;
					String buttonvalue = v.getTag().toString();
					System.out.println("buttonvalue is" + buttonvalue);
					int s = Integer.parseInt(buttonvalue);
					path = pdfpath[s];
					String[] filenamesplit = path.split("/");
					final String filename = filenamesplit[filenamesplit.length - 1];
					System.out.println("The File Name is " + filename);
					File sdDir = new File(Environment
							.getExternalStorageDirectory().getPath());
					File file = new File(sdDir.getPath()
							+ "/DownloadedPdfFile/" + filename);

					if (file.exists()) {
						View_Pdf_File(filename,path);
					} else {
						if (wb.isInternetOn() == true) {
							cf.show_ProgressDialog("Downloading... ");
							new Thread() {
								public void run() {
									Looper.prepare();
									try {
										String extStorageDirectory = Environment
												.getExternalStorageDirectory()
												.toString();
										File folder = new File(
												extStorageDirectory,
												"DownloadedPdfFile");
										folder.mkdir();
										File file = new File(folder,
												filename);
										try {
											file.createNewFile();
											Downloader.DownloadFile(path,
													file);
										} catch (IOException e1) {
											e1.printStackTrace();
										}

										show_handler = 2;
										handler.sendEmptyMessage(0);

									} catch (Exception e) {
										// TODO Auto-generated catch block
										System.out.println("The error is "
												+ e.getMessage());
										e.printStackTrace();
										show_handler = 1;
										handler.sendEmptyMessage(0);

									}
								}

								private Handler handler = new Handler() {
									@Override
									public void handleMessage(Message msg) {
										cf.pd.dismiss();
										// dialog1.dismiss();
										if (show_handler == 1) {
											show_handler = 0;
											cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

										} else if (show_handler == 2) {
											show_handler = 0;

											View_Pdf_File(filename,path);

										}
									}
								};
							}.start();
						} else {
							cf.ShowToast("Internet connection not available");

						}
					}
				}
			});

			
			pter[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String buttonvalue = v.getTag().toString();System.out.println("button="+buttonvalue);
					String[] arrayvalue = buttonvalue.split(":");
					String value = arrayvalue[0];
					String name = arrayvalue[1];
					String homid = arrayvalue[2];

					String ivalue = value;
					int ival = Integer.parseInt(ivalue);
					String ivalsplit = datasend[ival];
					String emailaddress=email[ival];
					String homeid= srid[ival];
					String[] arrayivalsplit = ivalsplit.split(":");
					String pn = arrayivalsplit[0];System.out.println("SDdas"+pn);
					String status = arrayivalsplit[1];

					int s = Integer.parseInt(value);
					path = pdfpath[s];
					String[] filenamesplit = path.split("/");
					String filename = filenamesplit[filenamesplit.length - 1];

					Intent intent = new Intent(Export.this,
							EmailReport2.class);System.out.println("homeid"+homeid);
					intent.putExtra("policynumber", homeid);
					intent.putExtra("status", status);
					intent.putExtra("mailid", emailaddress);
					intent.putExtra("classidentifier", "Export");
					intent.putExtra("ownersname", ownersname[ival]);
					startActivity(intent);
					finish();

				}
			});

			if (i % 2 == 0) {
				 rl[i].setBackgroundColor(Color.parseColor("#1871A0"));
							/*"#6E6E6E"));*/
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#106896"));
							/*"#2E2E2E"));*/
				}
			
		}
	}

	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
			startActivity(Intent.createChooser(intent, "Select PDF option..."));
        } 
        catch (ActivityNotFoundException e) {
			Intent intentview = new Intent(Export.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			finish();
        }
	}
	public void clicker(View v) {
		switch (v.getId()) 
		{
		case R.id.home:
			cf.gohome();
			break;
		
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			cf.gohome();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}