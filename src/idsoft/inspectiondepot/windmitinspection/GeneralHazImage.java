package idsoft.inspectiondepot.windmitinspection;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class GeneralHazImage extends Activity  {
	private static final int SELECT_PICTURE = 0;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	private static final String TAG = null;
	public Uri CapturedImageURI;
	private static final int visibility = 0;ProgressDialog pd1;
	Map<String, String[]> elevationcaption_map = new LinkedHashMap<String, String[]>();
	String j,homeid, picname, InspectionType, status, sgnpath, pathdesc,finaltext="",pathofimage="",repidofselbtn1,imgnum,phtodesc,
			selectedImagePath = "empty", capturedImageFilePath, updstr, paht,
			strdesc, name = "Exterior and Grounds", str;
	int value, maxLength=99,Count, alreadyExist=0,delimagepos,rws, chk, cvrtstr1,selid, t, f, quesid, elev = 1, isexist,backclick=0,selectedcount=0;;
	Uri mCapturedImageURI;	Bitmap rotated_b;
	int currnet_rotated=0;int globalvar=0;
	Spinner spnelevation[];	
	CheckBox chkboxchild[];        
	String saved_val[];
	String selectedtestcaption[] =null;
	ImageView thumbviewimgchild[],imgviewparent[],elevationimage[];
	TextView tvstatusparent[],tvstatuschild[];
	String[] arrimgord,pieces1=null,pieces2=null,pieces3=null;
	ListAdapter adapter;
	LinearLayout lnrlayout,lnrlayoutparent,lnrlayoutchild;
	TextView txthdr;
	File externamstoragepath;
	TextView imagepath,titleheader,textviewvalue_parent,textviewvalue_child,childtitleheader;
	Button backfolderwise,cancelfolderwise,selectfolderwise,selectfullimage,cancelfullimage;
	File listFile[];
	String[] items = { "gallery", "camera" };
	public String elevation[]={"--Select--","Exterior and Grounds","Interior","Additional Images"};
	
	EditText eddesc1,input, firsttxt, edbrowse, ed1;
	Button btnbrwse, btnupd, updat, del,save;
	TextView tvstatus[];
	ProgressDialog pd;
	int ImageOrder = 0, id1;
	static String[] arrpath;
	static String[] arrpathdesc;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	ImageView fullimageview;
	CheckBox cb1;
	View v1;
	String elevname="";
	private Cursor cursor;
	private int columnIndex;
	private int count;
	Dialog dialog,dialogfullimage;
	private Bitmap[] thumbnails;
	private boolean[] thumbnailsselection;
	private String[] arrPath;
	UploadRestriction upr = new UploadRestriction();
	public  ScrollView scr2=null;
	File[] Currentfile_list=null;
	public int limit_start=0; 
	protected RelativeLayout Rec_count_lin=null;
	protected TextView Total_Rec_cnt=null;
	protected TextView showing_rec_cnt;
	public LinearLayout sdcardImages;
	public String patharr[] = new String[8];
	int maxclick, maxlevel = 0,maximumindb=0;
	ImageView img1, img2, img3, img4, img6, img7, img8, img9, fstimg,  exttick, intetick, additick;
	Button extimg,inteimg, addiimg;
	public String[] et;
	public Bitmap[] thumbnails1;
	public int[] imageorder;
	public int count1;
	RelativeLayout Lin_Pre_nex=null;
	ImageView prev=null,next=null;
	private File images=null;
	protected int[] imageid;
	private String[] arrPath1;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	Spinner sp_elev;
	TextView no_uploaded;
LinearLayout upload_img;
int maximumallowed=8;
	Map<String, TextView> mapfolder = new LinkedHashMap<String, TextView>();
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		db = new DatabaseFunctions(this);
		wb = new WebserviceFunctions(this);

		/** Creating hazard image table **/
		db.CreateTable(14);
		db.CreateTable(18);
		
		cf.getDeviceDimensions();
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}
		setContentView(R.layout.generalimage);
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(GeneralHazImage.this, 6, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(GeneralHazImage.this, 30,cf, 1));
		
		Declaration();
		show_savedvalue();
		
		save=(Button)findViewById(R.id.save);
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				db.CreateTable(12);
				Cursor c = db.SelectTablefunction(db.GeneralHazDoc, " WHERE GCH_SRID='"+cf.Homeid+"'");
				if(c.getCount()>=1)
				{
					cf.ShowToast("General Hazard Image saved successfully");
					Intent int1 = new Intent(GeneralHazImage.this, Maps.class);
					int1.putExtra("homeid", cf.Homeid);
					int1.putExtra("status", cf.status);
					startActivity(int1);
					finish();
				}
				else
				{
					cf.ShowToast("Please add atleast one image");
				}
			}
		});
	}

	private void Declaration() {
		// TODO Auto-generated method stub
		
		sp_elev=(Spinner) findViewById(R.id.PH_elev_sp); 
		ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,elevation);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_elev.setAdapter(ad);
		
		sp_elev.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				show_savedvalue();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		no_uploaded=(TextView) findViewById(R.id.photo_noofuploads);
		upload_img=(LinearLayout) findViewById(R.id.uploaded_img);
	}

	protected void show_savedvalue() {
		// TODO Auto-generated method stub
		
		Cursor c = db.SelectTablefunction(db.GeneralHazDoc, " WHERE GCH_SRID='"+cf.Homeid+"' and GCH_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
		System.out.println("CC"+c.getCount());
		maximumindb =c.getCount();
		no_uploaded.setText("No of image uploaded/Remaining image to be upload :"+maximumindb+"/"+(maximumallowed-maximumindb));
		if(c.getCount()>0)
		{
			c.moveToFirst();
			saved_val=new String[c.getCount()];
			upload_img.removeAllViews();
			for(int i=0;i<c.getCount();i++,c.moveToNext())
			{
				String path,img_order,description;
				path=db.decode(c.getString(c.getColumnIndex("GCH_path")));
				saved_val[i]=db.decode(c.getString(c.getColumnIndex("GCH_path")));
				description=db.decode(c.getString(c.getColumnIndex("GCH_Description")));
				img_order=db.decode(c.getString(c.getColumnIndex("GCH_ImageOrder")));
				
				LinearLayout li=new LinearLayout(this);
				li.setGravity(Gravity.CENTER_VERTICAL);
				TextView tv= new TextView(this,null,R.attr.textview_200);
				tv.setText(description);
				tv.setTextColor(Color.BLACK);
				li.addView(tv,200,LayoutParams.WRAP_CONTENT);
				ImageView im =new ImageView(this,null,R.attr.photos_imageview);
				im.setTag(c.getString(c.getColumnIndex("GID")));
				File f =new File(path);
				if(f.exists())
				{
					Bitmap b =cf.ShrinkBitmap(path, 100, 100);
					if(b!=null)
					{
						im.setImageBitmap(b);
					}
				}
				im.setOnClickListener(new im_clicker(path,c.getString(c.getColumnIndex("GID"))));
				li.addView(im,100,100);
				Spinner sp =new Spinner(this);
				getelevname(sp_elev.getSelectedItemPosition());
				
				Cursor cap=db.SelectTablefunction(db.PhotoCaption, " WHERE IM_C_InspectorId='"+db.Insp_id+"' AND IM_C_Elevation_Name='"+elevname+"' and IM_C_Elevation='"+img_order+"'");
				String caption[];
				System.out.println("cap="+cap.getCount());
				if(cap.getCount()>0)
				{
					
					cap.moveToFirst();
					caption=new String[cap.getCount()+2];
					cap.moveToFirst();
					caption[0]="SELECT";
					caption[1]="ADD PHOTO CAPTION";
					for(int j=2;j<cap.getCount()+2;j++,cap.moveToNext())
					{
						caption[j]=db.decode(cap.getString(cap.getColumnIndex("IM_C_caption")));
					}
				}
				else
				{
					caption=new String[2];
					caption[0]="SELECT";
					caption[1]="ADD PHOTO CAPTION";
				}
				ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item, caption);
				ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				sp.setAdapter(ad);System.out.println("test");
				
				sp.setOnItemSelectedListener(new sp_onclicker(c.getString(c.getColumnIndex("GID")),img_order,sp,tv));
				sp.setPadding(20, 0, 0, 0);
				li.addView(sp,220,LayoutParams.WRAP_CONTENT);
				upload_img.addView(li,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			}
		}
		else
		{
			upload_img.removeAllViews();
			TextView tv= new TextView(this);
			tv.setText("No record founds");
			upload_img.addView(tv,150,LayoutParams.WRAP_CONTENT);
			saved_val=null;
		}
	}
	class im_clicker implements OnClickListener
	{
		String path,id;
			public im_clicker(String path, String string) {
			// TODO Auto-generated constructor stub
				this.path=path;
				this.id=string;
		}

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Cursor c =db.SelectTablefunction(db.GeneralHazDoc," WHERE GID='"+id+"'");
				c.moveToFirst();
				Intent in = new  Intent(GeneralHazImage.this,Edit_photos.class);
				in.putExtra("Path",path);
				in.putExtra("Caption",db.decode(c.getString(c.getColumnIndex("GCH_Description"))));
				in.putExtra("saved_val",saved_val);
				in.putExtra("id", id);
				startActivityForResult(in, 122);
				
			}
		}
	class sp_onclicker implements OnItemSelectedListener
	{
		String id,img_order;
		Spinner sp;
		TextView tv;
			public sp_onclicker(String id,String  img_order,Spinner sp,TextView tv) {
			// TODO Auto-generated constructor stub
				System.out.println("dsd");
				this.img_order=img_order;System.out.println("11");
				this.id=id;System.out.println("22");
				this.sp=sp;System.out.println("233d");
				this.tv=tv;System.out.println("444");
		}

			

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				System.out.println("sss="+sp.getSelectedItem().toString());
				if(sp.getSelectedItem().toString().trim().equals("ADD PHOTO CAPTION"))
				{
					final Dialog dialog1 = new Dialog(GeneralHazImage.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					Button close=((Button)dialog1.findViewById(R.id.helpclose));
					((TextView)dialog1.findViewById(R.id.txthelp)).setText("Add caption");
					LinearLayout l1=((LinearLayout)dialog1.findViewById(R.id.camera));
					l1.setVisibility(v1.VISIBLE);
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								getelevname(sp_elev.getSelectedItemPosition());
								db.wind_db.execSQL(" INSERT INTO "+db.PhotoCaption+" (IM_C_InspectorId,IM_C_caption,IM_C_Elevation,IM_C_Elevation_Name) VALUES " +
										"('"+db.Insp_id+"','"+db.encode(ed.getText().toString().trim())+"','"+img_order+"','"+elevname+"')");
								//add_caption(ed.getText().toString().trim(),img_order);
								
								cf.ShowToast("Photo Caption added successfully");
								show_savedvalue();
								dialog1.dismiss();
							}
							else
							{
								cf.ShowToast("Please enter caption ");
							}
						}

						
					});
					dialog1.show();				
					}
				else if(!sp.getSelectedItem().toString().trim().equals("SELECT"))
				{
					final Dialog dialog1 = new Dialog(GeneralHazImage.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					final LinearLayout li1=((LinearLayout)dialog1.findViewById(R.id.maintable));
					final LinearLayout li2=((LinearLayout)dialog1.findViewById(R.id.edit_Caption_li));
					li1.setVisibility(View.GONE);
					li2.setVisibility(View.VISIBLE);
					//final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button select=((Button)dialog1.findViewById(R.id.select));
					Button edit=((Button)dialog1.findViewById(R.id.edit));
					Button delete1=((Button)dialog1.findViewById(R.id.del));
					Button close1=((Button)dialog1.findViewById(R.id.helpclose5));
					((TextView)dialog1.findViewById(R.id.txtquestio1)).setText(sp.getSelectedItem().toString().toString());
					
					close1.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							System.out.println("inside close 1");
							sp.setSelection(0);System.out.println("sssspppp");
							dialog1.dismiss();System.out.println("dsfs");
						}
					});
					edit.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							li1.setVisibility(View.VISIBLE);
							li2.setVisibility(View.GONE);
						}
					});
					delete1.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							AlertDialog.Builder b =new AlertDialog.Builder(GeneralHazImage.this);
							b.setTitle("Confirmation");
							b.setMessage("Do you want to delete the selected Caption?");
							b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									try
									{
										db.wind_db.execSQL(" DELETE  FROM "+db.PhotoCaption+" WHERE IM_C_InspectorId='"+db.Insp_id+"' " +
												"and IM_C_Elevation='"+img_order+"' and IM_C_caption='"+db.encode(sp.getSelectedItem().toString().trim())+"'");
										
										show_savedvalue();
										dialog1.dismiss();
										
									}catch (Exception e) {
										// TODO: handle exception
										System.out.println("the exeption in delete"+e.getMessage());
									}
									
									cf.ShowToast("The selected caption has been deleted successfully.");
									
								}
							});
							b.setNegativeButton("No", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									
								}
							});   
							AlertDialog al=b.create();
							al.setIcon(R.drawable.alertmsg);
							al.setCancelable(false);
							al.show(); 
						}
					});
					select.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
								db.wind_db.execSQL(" UPDATE "+db.GeneralHazDoc+" SET GCH_Description='"+db.encode(sp.getSelectedItem().toString().trim())+"'" +
										" WHERE GID='"+id+"'");
								///add_caption(ed.getText().toString().trim(),img_order);
								tv.setText(sp.getSelectedItem().toString().trim());
								sp.setSelection(0);
								dialog1.dismiss();
						}
					});
					dialog1.show();
					
					//***For edit layout **//
					final EditText ed=((EditText)dialog1.findViewById(R.id.ed_values));
					ed.setText(sp.getSelectedItem().toString().trim());
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					Button close=((Button)dialog1.findViewById(R.id.helpclose));
					LinearLayout l1=((LinearLayout)dialog1.findViewById(R.id.camera));
					l1.setVisibility(v1.VISIBLE);
					((TextView)dialog1.findViewById(R.id.txthelp)).setText("Edit caption");
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							sp.setSelection(0);
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								
								getelevname(sp_elev.getSelectedItemPosition());
								db.wind_db.execSQL(" UPDATE "+db.PhotoCaption+" SET IM_C_caption='"+db.encode(ed.getText().toString().trim())+"' " +
										"WHERE  IM_C_InspectorId='"+db.Insp_id+"' and IM_C_Elevation='"+img_order+"' and IM_C_Elevation_Name='"+elevname+"' AND IM_C_caption='"+db.encode(sp.getSelectedItem().toString().trim())+"'");
								///add_caption(ed.getText().toString().trim(),img_order);
								show_savedvalue();
								dialog1.dismiss();
							}
							else
							{
								cf.ShowToast("Please enter caption ");
							}
						}

						
					});
					dialog1.show();	
					
					//***For edit layout ends **//*
				
				}
			}
			private void add_caption(String trim, String img_order) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		}
	private void getelevname(int pos) {
		// TODO Auto-generated method stub
		if(pos==1)
		{
			elevname = "EXT";
		}
		else if(pos==2)
		{
			elevname = "INT";
		}
		else if(pos==3)
		{
			elevname = "AI";
		}
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.home:
			cf.gohome();
			break;
		case R.id.browsetxt:
			if(sp_elev.getSelectedItemPosition()!=0){
				Cursor c1 = db.SelectTablefunction(db.GeneralHazDoc, " WHERE GCH_SRID='"+cf.Homeid+"' and GCH_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
				maximumindb =c1.getCount();
				if(maximumindb<8)
				{
					Intent reptoit1 = new Intent(this,Select_phots.class);
	 				Bundle b=new Bundle();
	 				reptoit1.putExtra("Selectedvalue", saved_val); /**Send the already selected image **/
	 				reptoit1.putExtra("Maximumcount", maximumindb);/**Total count of image in the database **/
	 				reptoit1.putExtra("Total_Maximumcount", 8); /***Total count of image we need to accept**/
	 				//reptoit1.setClassName("com.idinspection","com.idinspection.Select_phots");
	 				startActivityForResult(reptoit1,121); /** Call the Select image page in the idma application  image ***/
				}
				else
				{
					cf.ShowToast("You are allowed to upload only 8 Images for "+sp_elev.getSelectedItem().toString());	
				}
			}
			else
			{
				cf.ShowToast("Please select elevation type ");
			}
		break;
		case R.id.browsecamera:
			if(sp_elev.getSelectedItemPosition()!=0){
				Cursor c1 = db.SelectTablefunction(db.GeneralHazDoc, " WHERE GCH_SRID='"+cf.Homeid+"' and GCH_Elevation='"+sp_elev.getSelectedItemPosition()+"'");
				maximumindb =c1.getCount();
				if(maximumindb<8)
				{
					String fileName = "temp.jpg";
					ContentValues values = new ContentValues();
					values.put(MediaStore.Images.Media.TITLE, fileName);
					CapturedImageURI = getContentResolver().insert(
							MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
					startActivityForResult(intent, 111);
				}
				else
				{
					cf.ShowToast("You are allowed to upload only 8 Images for "+sp_elev.getSelectedItem().toString());	
				}
			}
			else
			{
				cf.ShowToast("Please select elevation type ");
			}

		break;
		}
	
		
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK)
		{
			if (requestCode == 111) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(CapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String capturedImageFilePath = cursor
						.getString(column_index_data);
				String selectedImagePath = capturedImageFilePath;
				display_taken_image(selectedImagePath);
				
	
			}
			else if(requestCode == 121)
			{
				/** we get the result from the Idma page for this elevation **/
			  String[] value=	data.getExtras().getStringArray("Selected_array"); /**We pass the array of tje value from the IDAM Select page **/
				for(int i=0;i<value.length;i++ )
				{
					db.wind_db.execSQL("INSERT INTO "
						+ db.GeneralHazDoc
						+ " (GCH_InspectorId,GCH_SRID,GCH_Elevation,GCH_path,GCH_Description,GCH_ImageOrder)"
						+ " VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+sp_elev.getSelectedItemPosition()+ "','"+ db.encode(value[i]) + "','"
						+ db.encode(sp_elev.getSelectedItem().toString() + (maximumindb+i+1))+ "','" + (maximumindb+1+i) + "')");
		 					
				}
				cf.ShowToast(sp_elev.getSelectedItem().toString() +" Image saved successfully");
				show_savedvalue();
				/** we get the result from the Idma page for this elevation Ends **/	
			}
			else if(requestCode == 112)
			{
				/** we get the result from the Idma page for this elevation **/
			  String value=	data.getExtras().getString("Path"); 
			  String Caption=	data.getExtras().getString("Caption"); 
			  
			  db.wind_db.execSQL("INSERT INTO "
						+ db.GeneralHazDoc
						+ " (GCH_InspectorId,GCH_SRID,GCH_Elevation,GCH_path,GCH_Description,GCH_ImageOrder)"
						+ " VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+sp_elev.getSelectedItemPosition()+ "','"+ db.encode(value) + "','"
						+ db.encode(Caption)+ "','" + (maximumindb+1) + "')");
		 					
		
				cf.ShowToast(sp_elev.getSelectedItem().toString() +" Image saved successfully");
				show_savedvalue();
			  
				/** we get the result from the Idma page for this elevation Ends **/	
			}
			else if(requestCode == 122)
			{
			
				/** we get the result from the Idma page for this elevation **/
			  String value=	data.getExtras().getString("Path"); 
			  String Caption=	data.getExtras().getString("Caption"); 
			  String  id=	data.getExtras().getString("id"); 
			  boolean dele=data.getExtras().getBoolean("Delete_data");
			  if(!dele)
			  {
							db.wind_db.execSQL(" UPDATE  "
	 							+ db.GeneralHazDoc
	 							+ " SET GCH_path='"+db.encode(value)+"',GCH_Description='"+db.encode(Caption)+"' "
	 							+ "  WHERE GID='"+id+"'");
		 					
		
				cf.ShowToast("Image saved successfully");
				
				/** we get the result from the Idma page for this elevation Ends **/	
			  }
			  else
			  {
				  db.wind_db.execSQL(" DELETE FROM "+db.GeneralHazDoc+" WHERE GID='"+id+"'");
				  cf.ShowToast("Deleted successfully");
			  }
			  show_savedvalue();
			}
		}
	}
	private void display_taken_image(String selectedImagePath) {
		// TODO Auto-generated method stub
		Intent in = new  Intent(this,Edit_photos.class);
		in.putExtra("Path",selectedImagePath);
		in.putExtra("Caption",sp_elev.getSelectedItem().toString()+(maximumindb+1));
		in.putExtra("saved_val",saved_val);
		in.putExtra("delete","false");
		startActivityForResult(in, 112);
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(GeneralHazImage.this, GeneralHazInfo.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}