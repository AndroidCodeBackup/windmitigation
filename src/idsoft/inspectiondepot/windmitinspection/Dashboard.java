package idsoft.inspectiondepot.windmitinspection;

import android.app.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class Dashboard extends Activity {
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	int schedule=0,completed=0,reports=0,total=0,cancelled=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);System.out.println("onCreate");
		setContentView(R.layout.dash);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		db.getInspectorId();
		setdashcount();
		((TextView)findViewById(R.id.welcomename)).setText(db.Insp_firstname+ " "+db.Insp_lastname);
		
        ((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(Dashboard.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
	}

	private void setdashcount() {
		// TODO Auto-generated method stub
		try
		{
			db.CreateTable(2);
			Cursor cur = db.SelectTablefunction(db.policyholder," where PH_InspectorId='" + db.Insp_id + "'");System.out.println("policyholder="+cur.getCount());
			if(cur.getCount()>0)
			{
				cur.moveToFirst();
				do {
					
					String PH_Status=db.decode(cur.getString(cur.getColumnIndex("PH_Status")));System.out.println("D="+cur.getCount());
					String PH_SubStatus=db.decode(cur.getString(cur.getColumnIndex("PH_SubStatus")));System.out.println("D="+cur.getCount());
					
					if ((PH_Status.equals("40") && PH_SubStatus.equals("0"))||PH_Status.equals("30")) {
						schedule++;
					}
					if (PH_Status.equals("1") && PH_SubStatus.equals("0")) {
						completed++;
					}
					if((PH_Status.equals("2") && PH_SubStatus.equals("0"))
							|| (PH_Status.equals("5") && PH_SubStatus.equals("0"))
							|| (PH_Status.equals("2") && PH_SubStatus.equals("121"))
							|| (PH_Status.equals("2") && PH_SubStatus.equals("122"))
							|| (PH_Status.equals("2") && PH_SubStatus.equals("123"))
							|| (PH_Status.equals("5") && PH_SubStatus.equals("151"))
							|| (PH_Status.equals("5") && PH_SubStatus.equals("153"))
							|| (PH_Status.equals("5") && PH_SubStatus.equals("152"))
							|| (PH_Status.equals("70") && PH_SubStatus.equals("71"))
							|| (PH_Status.equals("70"))
							|| (PH_Status.equals("140"))) 
					{
						reports++;
					}
					if (PH_Status.equals("90") && PH_SubStatus.equals("0")) {
						cancelled++;
					}
					
					
				}while(cur.moveToNext());
					
			}cur.close();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		total = schedule + completed +cancelled+reports;
		
		((TextView) findViewById(R.id.scheduled)).setText(String.valueOf(schedule));
		((TextView) findViewById(R.id.completed)).setText(String.valueOf(completed));
		((TextView) findViewById(R.id.download)).setText(String.valueOf(reports));
		((TextView) findViewById(R.id.cancelled)).setText(String.valueOf(cancelled));
		((TextView) findViewById(R.id.total)).setText(String.valueOf(total));
		
		((TextView) findViewById(R.id.scheduled)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MoveTo_HomeOwner(v,schedule,"Scheduled",1);
			}
		});
		
       ((TextView) findViewById(R.id.completed)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MoveTo_HomeOwner(v,completed,"Inspection Completed",1);
			}
		});
       
       ((TextView) findViewById(R.id.cancelled)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MoveTo_HomeOwner(v,cancelled,"Cancelled",1);
			}
		});

     ((TextView) findViewById(R.id.download)).setOnClickListener(new OnClickListener() {
	
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			MoveTo_HomeOwner(v,reports,"Download Reports",2);
		}
      });
	}

	
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.home:
				cf.gohome();
			break;
			
		}
	}

	private void MoveTo_HomeOwner(View v,int count, String status, int i) {
		// TODO Auto-generated method stub
		if(!((TextView) v).getText().toString().trim().equals("0"))
		{
			if(i==1)
			{
				Intent intent = new Intent(Dashboard.this,HomwOwnerListing.class);
				intent.putExtra("status", status);
				startActivity(intent);
				finish();
			}
			else
			{
				Intent intentreportready=new Intent(Dashboard.this,View_pdf.class);
				intentreportready.putExtra("type", "emailreport");
				intentreportready.putExtra("classidentifier", "DashBoard");
				intentreportready.putExtra("total_record", Integer.parseInt(((TextView)v).getText().toString()));
				intentreportready.putExtra("current", 1);
				
				startActivity(intentreportready);
				finish();
			}
		}
		else 
		{
			cf.ShowToast("No record found for " +status);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent in = new Intent(this,HomeScreen.class);
			in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(in);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}