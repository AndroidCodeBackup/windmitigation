package idsoft.inspectiondepot.windmitinspection;


import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class QuesRoofWall extends Activity {

	private static final int visibility = 0;
	String inspectortypeid, helpcontent;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	CheckBox temp_st;
	boolean load_comment=true;
	String commdescrip = "",tmp="";
	ListView list;
	TextWatcher watcher;
	int viewimage = 1,commentsch;
	String buildcommadmin1, buildcommadmin2, buildcommadmin3;
	TextView txroofwallheading,helptxt;
	Button saveclose;
	String rooftowallvalueprev, rooftowallsubvalueprev, rooftowallothrtxtprev,
			rooftowallclipsminvalueprev, rooftowallsingleminvalueprev,
			rooftowalldoubleminvalueprev;
	RadioButton rdioToenail, rdiotoenailoption1, rdiotoenailoption2, rdioClips,
			rdioclipoption1, rdioclipoption2, rdioSinglewraps,
			rdiosinglewrapoption1, rdioDoubleWraps, rdiodoublewrapoption1,
			rdiodoublewrapoption2, rdioE, rdioF, rdioG, rdioH;
	String commentsfill, InspectionType, status, rdiochk, comm, homeId,
			suboption = "0", othertext = "", updatecnt, identity,
			roofwallcomment;
	EditText comments, txtroofwallother;
	Intent iInspectionList;
	int value, Count, roofclipssingleminvalue;
	String descrip, comm2, conchkbox, status1, status2, status3, buildcomment1,
			buildcomment2, buildcomment3, builcodeadmin;
	String[] arrcomment1;
	CheckBox rdiotoenailoption3, rdiotoenailoption4;
	int clipsminvalue1, clipsminvalue2, roofclipsminvalue, clipdoubleminvalue1,
			clipsingleminvalue1, clipsingleminvalue2, roofclipsdoubleminvalue,
			clipdoubleminvalue2;
	CheckBox chkclipsmincond1, chkclipsmincond2, chksingleclipsmincond1,
			chksingleclipsmincond2, chkdoubleclipsmincond1,
			chkdoubleclipsmincond2;
	public LinearLayout ln[] = new LinearLayout[4];
	int optionid;
	TextView roofwall_type;
	public boolean che_status = true;
	CheckBox[] cb;
	View vv;
	int minvalue1, minvalue2;
	View v1;
	AlertDialog alertDialog;
	TextView prevmitidata;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);

		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);

		}
		setContentView(R.layout.roofwall);
		db.getInspectorId();
		db.getPHinformation(cf.Homeid);
		cf.getDeviceDimensions();
		db.changeimage(cf.Homeid);
		/** menu **/
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(QuesRoofWall.this, 2, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		
		
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(QuesRoofWall.this, 24,cf, 1));
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null)
				{
					cf.alertcontent = "To see help comments, please select Roof Wall options.";
				}
				else if (rdiochk.equals("1") || rdioToenail.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A Toe Nail, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("2") || rdioClips.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B Clips, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("3") || rdioSinglewraps.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C single wraps, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("4") || rdioDoubleWraps.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D double wraps, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("5") || rdioE.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, structural connection, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("6") || rdioF.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 4 Roof to Wall Attachment.";
				} else if (rdiochk.equals("7") || rdioG.isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home.";
				} else if (rdiochk.equals("8") || rdioH.isChecked()) {
					cf.alertcontent = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				} else {
					cf.alertcontent = "To see help comments, please select Roof Wall options.";
				}
				cf.showhelp("HELP",cf.alertcontent);


			}
		});
		txroofwallheading = (TextView) findViewById(R.id.txtheadingroofwall);
		txroofwallheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>What is the "
								+ "<u>"
								+ "WEAKEST"
								+ "</u>"
								+ " roof to wall connection? (Do not include attachment of hip/valley jacks within 5 feet of the inside or outside corner of the roof in determination of WEAKEST type) "));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		
		db.getQuesOriginal(cf.Homeid);
		if(!db.rworiddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.rworiddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}
		
		this.rdioToenail = (RadioButton) this.findViewById(R.id.toenail);
		this.rdioToenail.setOnClickListener(OnClickListener);
		this.rdiotoenailoption1 = (RadioButton) this
				.findViewById(R.id.toenailoption1);
		this.rdiotoenailoption1.setOnClickListener(OnClickListener);
		this.rdiotoenailoption2 = (RadioButton) this
				.findViewById(R.id.toenailoption2);
		this.rdiotoenailoption2.setOnClickListener(OnClickListener);

		this.chkclipsmincond1 = (CheckBox) this.findViewById(R.id.chkclipsmin1);
		this.chkclipsmincond2 = (CheckBox) this.findViewById(R.id.chkclipsmin2);

		this.chksingleclipsmincond1 = (CheckBox) this
				.findViewById(R.id.chksinglewrapsmincond1);
		this.chksingleclipsmincond2 = (CheckBox) this
				.findViewById(R.id.chksinglewrapsmincond2);

		this.chkdoubleclipsmincond1 = (CheckBox) this
				.findViewById(R.id.chkdoublewrapmincond1);
		this.chkdoubleclipsmincond2 = (CheckBox) this
				.findViewById(R.id.chkdoublewrapmincond2);
		

		ln[0] = (LinearLayout) findViewById(R.id.linLayoutA);
		ln[1] = (LinearLayout) findViewById(R.id.linLayoutB);
		ln[2] = (LinearLayout) findViewById(R.id.linLayoutC);
		ln[3] = (LinearLayout) findViewById(R.id.linLayoutD);

		// start to create object for the lenear layout

		chkdoubleclipsmincond1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					clipdoubleminvalue1 = 1;
				} else {
					clipdoubleminvalue1 = 0;
				}
			}
		});
		chkdoubleclipsmincond2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					clipdoubleminvalue2 = 1;
				} else {
					clipdoubleminvalue2 = 0;
				}
			}
		});

		this.rdioClips = (RadioButton) this.findViewById(R.id.clips);
		this.rdioClips.setOnClickListener(OnClickListener);
		this.rdioclipoption1 = (RadioButton) this
				.findViewById(R.id.clipsoption1);
		this.rdioclipoption1.setOnClickListener(OnClickListener);
		this.rdioclipoption2 = (RadioButton) this
				.findViewById(R.id.clipsoption2);
		this.rdioclipoption2.setOnClickListener(OnClickListener);

		this.rdioSinglewraps = (RadioButton) this
				.findViewById(R.id.rdiosinglewraps);
		this.rdioSinglewraps.setOnClickListener(OnClickListener);
		this.rdiosinglewrapoption1 = (RadioButton) this
				.findViewById(R.id.singlewrapsoption1);
		this.rdiosinglewrapoption1.setOnClickListener(OnClickListener);

		this.rdioDoubleWraps = (RadioButton) this
				.findViewById(R.id.rdiodoublewraps);
		this.rdioDoubleWraps.setOnClickListener(OnClickListener);
		this.rdiodoublewrapoption1 = (RadioButton) this
				.findViewById(R.id.doublewrapsoption1);
		this.rdiodoublewrapoption1.setOnClickListener(OnClickListener);
		this.rdiodoublewrapoption2 = (RadioButton) this
				.findViewById(R.id.doublewrapsoption2);
		this.rdiodoublewrapoption2.setOnClickListener(OnClickListener);

		this.rdioE = (RadioButton) this.findViewById(R.id.rdioE);
		this.rdioE.setOnClickListener(OnClickListener);

		this.rdioF = (RadioButton) this.findViewById(R.id.rdioF);
		this.rdioF.setOnClickListener(OnClickListener);

		this.rdioG = (RadioButton) this.findViewById(R.id.rdioG);
		this.rdioG.setOnClickListener(OnClickListener);

		this.rdioH = (RadioButton) this.findViewById(R.id.rdioH);
		this.rdioH.setOnClickListener(OnClickListener);
		
		comments = (EditText) this.findViewById(R.id.txtcomments);
		roofwall_type=(TextView) findViewById(R.id.SH_TV_ED);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,roofwall_type));
		
		txtroofwallother = (EditText) this.findViewById(R.id.txtroofwallother);
		saveclose = (Button) findViewById(R.id.savenext);
		
        setdata();
  
	}
	
	
	private void setdata() {
		// TODO Auto-generated method stub
		try {
			Cursor cur = db.wind_db.rawQuery("select * from "
					+ db.Questions + " where SRID='" + cf.Homeid
					+ "'", null);
			cur.moveToFirst();
			if (cur != null) {
				rooftowallvalueprev = cur.getString(cur
						.getColumnIndex("RooftoWallValue"));
				rooftowallsubvalueprev = cur.getString(cur
						.getColumnIndex("RooftoWallSubValue"));
				rooftowallothrtxtprev = db.decode(cur
						.getString(cur
								.getColumnIndex("RooftoWallOtherText")));
				rooftowallclipsminvalueprev = cur.getString(cur
						.getColumnIndex("RooftoWallClipsMinValue"));
				rooftowallsingleminvalueprev = cur.getString(cur
						.getColumnIndex("RooftoWallSingleMinValue"));
				rooftowalldoubleminvalueprev = cur.getString(cur
						.getColumnIndex("RooftoWallDoubleMinValue"));

				if (rooftowallvalueprev.equals("1")) {
					rdiochk = "1";
					ln[0].setVisibility(View.VISIBLE);
					rdioToenail.setChecked(true);
					if (rooftowallsubvalueprev.equals("1")) {
						rdiotoenailoption1.setChecked(true);
					} else if (rooftowallsubvalueprev.equals("2")) {
						rdiotoenailoption2.setChecked(true);
					} else {
					}
					suboption = rooftowallsubvalueprev;

				} else if (rooftowallvalueprev.equals("2")) {
					rdiochk = "2";
					ln[1].setVisibility(View.VISIBLE);
					rdioClips.setChecked(true);
					chkclipsmincond1.setChecked(true);
					chkclipsmincond2.setChecked(true);

					if (rooftowallsubvalueprev.equals("1")) {
						rdioclipoption1.setChecked(true);
					} else if (rooftowallsubvalueprev.equals("2")) {
						rdioclipoption2.setChecked(true);
					}

					else {
					}
					suboption = rooftowallsubvalueprev;
				} else if (rooftowallvalueprev.equals("3")) {
					rdiochk = "3";
					suboption = "1";
					ln[2].setVisibility(View.VISIBLE);
					rdioSinglewraps.setChecked(true);
					chksingleclipsmincond1.setChecked(true);
					chksingleclipsmincond2.setChecked(true);
					rdiosinglewrapoption1.setChecked(true);

				}

				else if (rooftowallvalueprev.equals("4")) {
					rdiochk = "4";
					ln[3].setVisibility(View.VISIBLE);
					rdioDoubleWraps.setChecked(true);
					chkdoubleclipsmincond1.setChecked(true);
					chkdoubleclipsmincond2.setChecked(true);

					if (rooftowallsubvalueprev.equals("1")) {
						rdiodoublewrapoption1.setChecked(true);
					} else if (rooftowallsubvalueprev.equals("2")) {
						rdiodoublewrapoption2.setChecked(true);
					}

					else {
					}
					suboption = rooftowallsubvalueprev;
				}

				else if (rooftowallvalueprev.equals("5")) {
					rdioE.setChecked(true);
					rdiochk = "5";
				} else if (rooftowallvalueprev.equals("6")) {
					rdioF.setChecked(true);
					txtroofwallother.setText(rooftowallothrtxtprev);
					rdiochk = "6";
				} else if (rooftowallvalueprev.equals("7")) {
					rdioG.setChecked(true);
					rdiochk = "7";
				} else if (rooftowallvalueprev.equals("8")) {
					rdioH.setChecked(true);
					rdiochk = "8";
				} else {
				}
			}
		} catch (Exception e) {
			System.out.println("e" + e);
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in retrieving data and setting roofwall value on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}

		try {
			Cursor cur = db.wind_db.rawQuery("select * from " + db.QuestionsComments
					+ " where SRID='" + cf.Homeid + "'", null);
			cur.moveToFirst();
			if (cur != null) {
				roofwallcomment = db.decode(cur.getString(cur
						.getColumnIndex("RoofWallComment")));
				
				comments.setText(roofwallcomment);
			}
		} catch (Exception e) {
			System.out.println("e" + e);
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in retrieving roofwall comments on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

		}
	}


	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.toenail:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection A Toe Nail, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "1";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(true);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);
				viewimage = 1;
				break;

			case R.id.clips:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection B Clips, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "2";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(true);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);

				// chkclipsmincond1.setChecked(true);chkclipsmincond2.setChecked(true);
				chksingleclipsmincond1.setChecked(false);
				chksingleclipsmincond2.setChecked(false);
				chkdoubleclipsmincond1.setChecked(false);
				chkdoubleclipsmincond2.setChecked(false);
				viewimage = 1;

				break;

			case R.id.rdiosinglewraps:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection C single wraps, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "3";
				suboption = "1";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(true);
				rdiosinglewrapoption1.setChecked(true);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);

				chkclipsmincond1.setChecked(false);
				chkclipsmincond2.setChecked(false);
				// chksingleclipsmincond1.setChecked(true);chksingleclipsmincond2.setChecked(true);
				chkdoubleclipsmincond1.setChecked(false);
				chkdoubleclipsmincond2.setChecked(false);
				viewimage = 1;

				break;

			case R.id.rdiodoublewraps:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection D double wraps, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "4";
				set_visible(); // call the set visisble for the linear layout
				txtroofwallother.setEnabled(false);
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(true);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);

				chkclipsmincond1.setChecked(false);
				chkclipsmincond2.setChecked(false);
				chksingleclipsmincond1.setChecked(false);
				chksingleclipsmincond2.setChecked(false);
				viewimage = 1;
				
				break;

			case R.id.rdioE:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection E, structural connection, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);

				rdiochk = "5";
				set_invisible();
				txtroofwallother.setEnabled(false);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(true);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioF:
				commentsfill = "This home was verified as meeting the requirements of the OIR B1- 1802, Selection F, Question 4 Roof to Wall Attachment.";
				comments.setText(commentsfill);
				rdiochk = "6";
				set_invisible();
				txtroofwallother.setEnabled(true);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				othertext = txtroofwallother.getText().toString();
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(true);
				rdioG.setChecked(false);
				rdioH.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdioG:
				commentsfill = "We were unable to verify the weakest form or type of roof wall connection to this home.";
				comments.setText(commentsfill);
				rdiochk = "7";
				set_invisible();
				txtroofwallother.setEnabled(false);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(true);
				rdioH.setChecked(false);
				viewimage = 1;
				break;
			case R.id.rdioH:
				commentsfill = "We were unable to verify the weakest form or type of roof wall connection to this home as a result of no attic access.";
				comments.setText(commentsfill);
				rdiochk = "8";
				set_invisible();
				txtroofwallother.setEnabled(false);
				roofclipsminvalue = 0;
				roofclipssingleminvalue = 0;
				roofclipsdoubleminvalue = 0;
				rdioToenail.setChecked(false);
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				rdioClips.setChecked(false);
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(false);
				rdioSinglewraps.setChecked(false);
				rdiosinglewrapoption1.setChecked(false);
				rdioDoubleWraps.setChecked(false);
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(false);
				rdioE.setChecked(false);
				rdioF.setChecked(false);
				rdioG.setChecked(false);
				rdioH.setChecked(true);
				viewimage = 1;
				break;

			case R.id.toenailoption1:
				suboption = "1";
				rdiotoenailoption1.setChecked(true);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				break;
			case R.id.toenailoption2:
				suboption = "2";
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(true);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(false);
				break;
			case R.id.chkclipsmin1:
				// suboption="3";
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(true);rdiotoenailoption4.setChecked(false);
				break;
			case R.id.chkclipsmin2:
				// suboption="4";
				rdiotoenailoption1.setChecked(false);
				rdiotoenailoption2.setChecked(false);// rdiotoenailoption3.setChecked(false);rdiotoenailoption4.setChecked(true);
				break;
			case R.id.clipsoption1:
				suboption = "1";
				rdioclipoption1.setChecked(true);
				rdioclipoption2.setChecked(false);
				break;

			case R.id.clipsoption2:
				suboption = "2";
				rdioclipoption1.setChecked(false);
				rdioclipoption2.setChecked(true);
				break;
			case R.id.singlewrapsoption1:
				suboption = "1";
				rdiosinglewrapoption1.setChecked(true);
				break;
			case R.id.doublewrapsoption1:
				suboption = "1";
				rdiodoublewrapoption1.setChecked(true);
				rdiodoublewrapoption2.setChecked(false);
				break;

			case R.id.doublewrapsoption2:
				suboption = "2";
				rdiodoublewrapoption1.setChecked(false);
				rdiodoublewrapoption2.setChecked(true);
				break;

			}
		}
	};

	public void set_invisible() {

		for (int i = 0; i <= 3; i++) {
			ln[i].setVisibility(View.GONE);
		}
	}

	public void set_visible() {
		// function for set cisible once the radio button clicked
		for (int i = 0; i <= 3; i++) {
			if ((i + 1) != (Integer.parseInt(rdiochk))) {
				ln[i].setVisibility(View.GONE);
			} else {
				ln[i].setVisibility(View.VISIBLE);
			}
		}
		// function for set cisible once the radio button clicked
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			RW_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				if(load_comment)
				{
					load_comment=false;
					int loc1[] = new int[2];
					v.getLocationOnScreen(loc1);
					Intent in = cf.loadComments(tmp,loc1);
					in.putExtra("insp_ques", "4");
					in.putExtra("length", len);
					in.putExtra("max_length", 500);
					startActivityForResult(in, cf.loadcomment_code);
					
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Wall");	
			}			
			break;
		  case R.id.txthelpcontentoptionA: 
			  cf.alerttitle="A - Toe Nail";
			  cf.alertcontent="Rafter/truss anchored to top plate of wall using nails driven at an angle through the rafter/truss and attached to the top plate of the wall.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionB: 
			  cf.alerttitle="B - Clips";
			  cf.alertcontent="Metal attachments on every rafter/truss that are nailed to one side (or both sides in the case of a diamond type clip) of the rafter/truss and attached to the top plate of the wall frame or embedded in the bond beam.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionC: 
			  cf.alerttitle="C - Single Wraps";
			  cf.alertcontent="Metal Straps must be secured to every rafter/truss with a minimum of 3 nails, wrapping over and securing to the opposite side of the rafter/truss with a minimum of 1 nail. The Strap must be attached to the top plate of the wall frame or embedded in the bond beam in at least one place.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionD: 
			  cf.alerttitle="D - Double Wraps";
			  cf.alertcontent="Both Metal Straps must be secured to every rafter/truss with a minimum of 3 nails, wrapping over and securing to the opposite side of the rafter/truss with a minimum of 1 nail. Each Strap must be attached to the top plate of the wall frame or embedded in the bond beam in at least one place.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.txthelpcontentoptione: 
			  cf.alerttitle="E - Structural";
			  cf.alertcontent="Anchor bolts, structurally connected or reinforced concrete roof.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		  case R.id.txthelpcontentoptionF: 
			  cf.alerttitle="F - other";
			  cf.alertcontent="Other.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.txthelpcontentoptionG: 
			  cf.alerttitle="G - Unknown";
			  cf.alertcontent="Unknown or unidentified.";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
		  case R.id.txthelpcontentoptionH: 
			  cf.alerttitle="H - No Attic";
			  cf.alertcontent="No Attic Access";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			  break;
			  
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:
			comm = comments.getText().toString();
			String title_error = "";

			if (rdiochk == "1" || rdiochk == "2" || rdiochk == "3"
					|| rdiochk == "4") {
				switch (Integer.parseInt(rdiochk)) {
				case 1:
					if (rdiotoenailoption1.isChecked()
							|| rdiotoenailoption2.isChecked()) {
						che_status = true;

					} else {
						che_status = false;
						title_error = "Please select option for Toenail";
					}

					break;
				case 2:
					if (chkclipsmincond1.isChecked()
							&& chkclipsmincond2.isChecked()
							&& (rdioclipoption1.isChecked() || rdioclipoption2
									.isChecked())) {
						che_status = true;
						roofclipsminvalue = 2;
						roofclipssingleminvalue = 0;
						roofclipsdoubleminvalue = 0;
					} else if (chkclipsmincond1.isChecked()
							^ chkclipsmincond2.isChecked()) {
						che_status = false;
						title_error = "Please select minimum option for Clips";

					} else if (!(rdioclipoption1.isChecked() || rdioclipoption2
							.isChecked())) {
						che_status = false;
						title_error = "Please select option for Clips";
					} else {
						che_status = false;
						title_error = "Please select minimum option for Single Wraps  ";
					}

					break;
				case 3:
					if (chksingleclipsmincond1.isChecked()
							&& chksingleclipsmincond2.isChecked()
							&& rdiosinglewrapoption1.isChecked()) {
						che_status = true;
						roofclipsminvalue = 0;
						roofclipssingleminvalue = 2;
						roofclipsdoubleminvalue = 0;
					} else if (chksingleclipsmincond1.isChecked()
							^ chksingleclipsmincond2.isChecked()) {
						che_status = false;
						title_error = "Please select minimum option for Single Wraps";

					} else if (!rdiosinglewrapoption1.isChecked()) {
						che_status = false;
						title_error = "Please select option for Single Wraps";
					} else {
						che_status = false;
						title_error = "Please select minimum option for Single Wraps  ";
					}

					break;
				case 4:
					if (chkdoubleclipsmincond1.isChecked()
							&& chkdoubleclipsmincond2.isChecked()
							&& (rdiodoublewrapoption1.isChecked() || rdiodoublewrapoption2
									.isChecked())) {
						che_status = true;
						roofclipsminvalue = 0;
						roofclipssingleminvalue = 0;
						roofclipsdoubleminvalue = 2;
					} else if (chkdoubleclipsmincond1.isChecked()
							^ chkdoubleclipsmincond2.isChecked()) {
						che_status = false;
						title_error = "Please select minimum option for Double Wraps";

					} else if (!(rdiodoublewrapoption1.isChecked() || rdiodoublewrapoption2
							.isChecked())) {
						che_status = false;
						title_error = "Please select option for Double Wraps";
					} else {
						che_status = false;
						title_error = "Please select minimum option for Double Wraps";
					}

					break;
				}
			}
			if (rdioToenail.isChecked() == true
					|| rdioClips.isChecked() == true
					|| rdioSinglewraps.isChecked() == true
					|| rdioDoubleWraps.isChecked() == true
					|| rdioE.isChecked() == true || rdioF.isChecked() == true
					|| rdioG.isChecked() == true || rdioH.isChecked() == true) {

				if (rdioF.isChecked() == true) {
					othertext = txtroofwallother.getText().toString();
					if (othertext.trim().equals("")) {
						che_status = false;
						title_error = "Please enter the Other text for Roof Wall.";
					} else {
						che_status = true;
					}
				}
				if (comm.trim().equals("")) {
					cf.ShowToast("Please enter the Comments for Roof Wall");
				} else if (!che_status) {
					cf.ShowToast(title_error);
					title_error = "";
					che_status = true;
				} else {
					try {
						Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
								+ db.Questions + " WHERE SRID='"
								+ cf.Homeid + "'", null);
						int rws = c2.getCount();
						if (rws == 0) {

							db.wind_db.execSQL("INSERT INTO "
									+ db.Questions
									+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
									+ "VALUES ('" + cf.Homeid + "','','','" + 0
									+ "','" + 0 + "','" + 0 + "','','','','','"
									+ 0 + "','" + 0 + "','','" + rdiochk
									+ "','" + suboption + "','"
									+ roofclipsminvalue + "','"
									+ roofclipssingleminvalue + "','"
									+ roofclipsdoubleminvalue + "','"
									+ db.encode(othertext) + "','"
									+ 0 + "','" + 0 + "','" + 0 + "','','" + 0
									+ "','','" + 0 + "','" + 0
									+ "','','','','','','','','" + 0 + "','"
									+ 0 + "','" + 0 + "','" + 0 + "','" + 0
									+ "','" + 0 + "','" + 0 + "','" + 0
									+ "','','" + 0 + "','" + 0 + "','','" + 0
									+ "','','','" + cd + "','" + 0 + "')");

						} else {

							db.wind_db.execSQL("UPDATE " + db.Questions
									+ " SET RooftoWallValue ='" + rdiochk
									+ "',RooftoWallSubValue='" + suboption
									+ "',RooftoWallClipsMinValue='"
									+ roofclipsminvalue
									+ "',RooftoWallSingleMinValue='"
									+ roofclipssingleminvalue
									+ "',RooftoWallDoubleMinValue='"
									+ roofclipsdoubleminvalue
									+ "',RooftoWallOtherText='"
									+ db.encode(othertext) + "'"
									+ " WHERE SRID ='" + cf.Homeid.toString()
									+ "'");
						}
						updatecnt = "1";

						Cursor c5 = db.wind_db.rawQuery("SELECT * FROM "
								+ db.QuestionsComments + " WHERE SRID='" + cf.Homeid
								+ "'", null);
						int rws5 = c5.getCount();
						if (rws5 == 0) {
							db.wind_db.execSQL("INSERT INTO "
									+ db.QuestionsComments
									+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
									+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','"+ db.encode(comments.getText().toString())+ "','','','','','','"+ cf.datewithtime + "')");
					
						} else {
							db.wind_db.execSQL("UPDATE " + db.QuestionsComments
									+ " SET RoofWallComment='"
									+ db.encode(comm)
									+ "',CreatedOn ='"
									+ md + "'" + " WHERE SRID ='"
									+ cf.Homeid.toString() + "'");
						}
						db.getInspectorId();

						Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "
								+ db.SubmitCheckTable + " WHERE Srid='"
								+ cf.Homeid + "'", null);
						int subchkrws = c3.getCount();
						if (subchkrws == 0) {
							db.wind_db.execSQL("INSERT INTO "
									+ db.SubmitCheckTable
									+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
									+ "VALUES ('" + db.Insp_id + "','"
									+ cf.Homeid
									+ "',0,0,0,0,1,0,0,0,0,0,0,0,0,0,0)");
						} else {
							db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
									+ " SET fld_roofwall='1' WHERE Srid ='"
									+ cf.Homeid + "' and InspectorId='"
									+ db.Insp_id + "'");

						}
						updatecnt = "1";
						
						// roofwalltick.setVisibility(visibility);
					} catch (Exception e) {

						updatecnt = "0";
						cf.ShowToast("There is a problem in saving your data due to invalid character");
						//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofWall.this +" problem in inserting or updating roofwall comments on arrow buttton click on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

					}

				}
			} else {
				cf.ShowToast("Please select the Roof Wall attachment");

			}
			if (updatecnt == "1") {
				cf.ShowToast("Roof Wall has been saved successfully");
				iInspectionList = new Intent(QuesRoofWall.this, QuesRoofGeometry.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("status", cf.status);
				startActivity(iInspectionList);
				finish();
			}

			break;

		}

	}


	private void RW_suboption() {
		// TODO Auto-generated method stub
		if(rdioToenail.isChecked())	{		tmp="1";}
		else if(rdioClips.isChecked()) {	tmp="2";}
		else if(rdioSinglewraps.isChecked()) {	tmp="3";}
		else if(rdioDoubleWraps.isChecked()) {	tmp="4";}
		else if(rdioE.isChecked()) {	tmp="5";}
		else if(rdioF.isChecked()) {	tmp="6";}
		else if(rdioG.isChecked()) {	tmp="7";}
		else if(rdioH.isChecked()) {	tmp="8";}
		else{tmp="";}
	}


	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesRoofWall.this, QuesRoofDeck.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
				((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
			}
		}
		/*switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}*/

	}
	
}