package idsoft.inspectiondepot.windmitinspection;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Submit extends Activity {
	DatabaseFunctions db;
	WebserviceFunctions wb;
	String homeid, word,overallcomm="",elevationname="",inspectioncount;
	RadioButton accept, decline;
	ProgressDialog pd1;
	EditText etsetword, etgetword;
	String Exportbartext="";
	ProgressDialog progDialog;
	SoapObject inspcount_result;
	int val_handler;
	int delay = 40; // Milliseconds of delay in the update loop
	Resources res;
	int RUNNING = 1;
	private String  Error_tracker="";
	int handler_verif=0;
	double total; // Determines type progress bar: 0 = spinner, 1 = horizontal
	private String[] myString;
	TextView tv1, tv2;
	int mState;
	int typeBar = 1;
	View v1;
	PowerManager.WakeLock wl=null;
	int maxBarValue = 0,show_handler; // Maximum value of horizontal progress bar
	CommonFunctions cf;
	boolean completed=false;
	String  showstr="",erromsg="",fe="",be="",le="",re="",ae="",roof="",off="",sup="",addiphot="",homesig="",papersign="",geexter="",ininter="",additional="";
	private static final Random rgenerator = new Random();

	boolean ex_function[] ={false,false,false,false,false,false};
	
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf=new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
	     if (extras != null) {
			cf.getExtras(extras);				
	     }
		System.out.println("Status=submit"+cf.status);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		setContentView(R.layout.submit);
		
		db.CreateTable(8);
		db.CreateTable(10);
		db.CreateTable(11);
		db.CreateTable(13);
		db.CreateTable(14);
		db.CreateTable(15);
		
		db.getInspectorId();
		cf.getDeviceDimensions();
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);
		layout.setMinimumWidth(cf.wd);
        layout.addView(new MyOnclickListener(Submit.this,8,cf,0));
        
        declaration();
	}
	private void declaration() {
		// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			
			 myString = getResources().getStringArray(R.array.myArray);
			 etsetword = (EditText) findViewById(R.id.wrdedt);
			 etgetword = (EditText) findViewById(R.id.wrdedt1);
			 word = myString[rgenerator.nextInt(myString.length)];
			 accept = (RadioButton) this.findViewById(R.id.accept);
				decline = (RadioButton) this.findViewById(R.id.decline);
			etsetword.setText(word);
			if (word.contains(" ")) {
				word = word.replace(" ", "");
			}
		   ((TextView)findViewById(R.id.tv1)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ db.Insp_firstname +" "+db.Insp_lastname
							+ "</font></b><font color=black> have conducted this inspection in accordance with the required standards and processes and confirm that I am properly licensed to conduct and sign off on the same inspection. I have made every attempt to collect the data required, have spoken to the Policyholder and/or Agent of record and have conducted the necessary research requirements to ensure permitting, replacement cost valuations or other information are properly completed as part of this inspection process."
							+ "</font>"));
			((TextView)findViewById(R.id.tv2)).setText(Html
					.fromHtml("<font color=black>I, "
							+ "</font>"
							+ "<b><font color=#bddb00>"
							+ db.Insp_firstname +" "+db.Insp_lastname
							+ "</font></b><font color=black> also understand that failure to properly conduct this inspection and submit the verifiable inspection data is grounds for termination and considered possible insurance fraud."
							+ "</font>"));
		
		
	}
	public OnClickListener onClickAnswer1 = new OnClickListener() {
		public void onClick(View arg0) {
			accept.setSelected(true);
			decline.setChecked(false);
		}
	};
	public OnClickListener onClickAnswer2 = new OnClickListener() {
		public void onClick(View arg0) {
			accept.setSelected(false);
			decline.setChecked(true);
		}
	};

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.submit:
			if (accept.isChecked() == true) {
				
				if (etgetword.getText().toString().equals("")) {

					cf.ShowToast("Please enter Word Verification.");
					etgetword.requestFocus();
				} else {

					if (etgetword.getText().toString()
							.equals(word.trim().toString())) {
						validation();
						
						
					} else {
						cf.ShowToast("Please enter valid Word Verification(case sensitive).");
						etgetword.setText("");
						etgetword.requestFocus();
					}
				}

			} else {
				cf.ShowToast("Please select Accept Radio Button.");

			}
			
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.S_refresh:			
			word = myString[rgenerator.nextInt(myString.length)];			
			etsetword.setText(word);
			if (word.contains(" ")) {
				word = word.replace(" ", "");
			}			
			break;
		}
	}
	private void validation() {
		// TODO Auto-generated method stub
		
		Cursor qc = db.SelectTablefunction(db.QuestionsComments, " where SRID='" + cf.Homeid + "'");
			if(qc.getCount()>0)
			{
				qc.moveToFirst();
				if((!db.decode(qc.getString(qc.getColumnIndex("BuildingCodeComment"))).equals(""))
						&& (!db.decode(qc.getString(qc.getColumnIndex("RoofCoverComment"))).equals(""))
						&& (!db.decode(qc.getString(qc.getColumnIndex("RoofDeckComment"))).equals(""))
						&& (!db.decode(qc.getString(qc.getColumnIndex("SecondaryWaterComment"))).equals(""))
						&& (!db.decode(qc.getString(qc.getColumnIndex("RoofWallComment"))).equals(""))
						&& (!db.decode(qc.getString(qc.getColumnIndex("RoofGeometryComment"))).equals(""))
						&& (!db.decode(qc.getString(qc.getColumnIndex("OpeningProtectionComment"))).equals("")))
				{				
					completed=true;
				}
				else
				{
					completed=false;
				}
			}
			Cursor qc1 = db.SelectTablefunction(db.Questions, " where SRID='" + cf.Homeid + "'");
			if(qc1.getCount()>0)
			{
				qc1.moveToFirst();
				if(qc1.getString(qc1.getColumnIndex("WoodFramePer")).equals("0") && qc1.getString(qc1.getColumnIndex("ReinMasonryPer")).equals("0") && 
						qc1.getString(qc1.getColumnIndex("UnReinMasonryPer")).equals("0") && 
						qc1.getString(qc1.getColumnIndex("PouredConcretePer")).equals("0") && qc1.getString(qc1.getColumnIndex("OtherWallPer")).equals("0"))
				{
					completed=false;
				}
				else
				{
					completed=true;
				}
			}
			else
			{
				completed=false;	
			}
			
			 Cursor imgcur = db.SelectTablefunction(db.Photos, " where IM_SRID='" +cf.Homeid + "' and IM_Elevation='1'");
			 Cursor	fdinfocur = db.SelectTablefunction(db.FeedBackInfo, " where FD_SRID='" + cf.Homeid + "'");
			// Cursor	fddocucur = db.SelectTablefunction(db.FeedBackDocument, " where FD_D_SRID='" + cf.Homeid + "'");
			 Cursor overcur =  db.SelectTablefunction(db.QuestionsComments, " WHERE SRID='" + cf.Homeid+ "'");
			 if(overcur.getCount()>0)
			 {
				 overcur.moveToFirst();
				 overallcomm = db.decode(overcur.getString(overcur.getColumnIndex("InsOverAllComments")));
			 }
			 
				
			 if(qc.getCount()==0 || completed==false)
			 {
				 cf.ShowToast("Please Fill B1-1802 Question Section");
			 }
			 else if(imgcur.getCount()==0)
			 { 
				 cf.ShowToast("Please add atleast one Front Elevation image in images Section");
			 }
			 else if(fdinfocur.getCount()==0 )
			 {
				 cf.ShowToast("Please Fill Feedback Section");
			 }
			 else if(overallcomm.equals(""))
			 {
				 cf.ShowToast("Please enter the Overall Comments");
			 }
			 else
			 {
				db.wind_db.execSQL("UPDATE " + db.policyholder + " SET PH_Status=1 WHERE PH_SRID ='"+ cf.Homeid + "' and PH_InspectorId = '"+ db.Insp_id + "'");
					//cf.ShowToast("Saved sucessfully");		
				//startexpot(cf.Homeid,ex_function);
				InspectionCountCheck();
					
			 }			
	}
	
	
	
	protected void InspectionCountCheck() {

		// TODO Auto-generated method stub
		if (wb.isInternetOn() == true) {			
			cf.show_ProgressDialog("Submitting...");
			new Thread() {
				public void run() {
					Looper.prepare();
					
					try
					{
						String Chk_Inspector=wb.IsCurrentInspector(db.Insp_id,cf.Homeid,"IsCurrentInspector");
						if(Chk_Inspector.equals("true"))
						{  
								SoapObject request = new SoapObject(wb.NAMESPACE,"TOTALINSPECTIONCOUNT");
								SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
								envelope.dotNet = true;
								request.addProperty("InspectorID", db.Insp_id);
								
								envelope.setOutputSoapObject(request);
								System.out.println("TOTALINSPECTIONCOUNT request is "+ request);
								HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
								System.out.println("Before http call");
								androidHttpTransport.call(wb.NAMESPACE+ "TOTALINSPECTIONCOUNT", envelope);
							
								inspcount_result = (SoapObject) envelope.getResponse();
								
								System.out.println("TOTALINSPECTIONCOUNT result is"+ inspcount_result);
								
								String Status = String.valueOf(inspcount_result.getProperty("Status"));
								inspectioncount = String.valueOf(inspcount_result.getProperty("Inspcount"));
								
								
								
								if(Status.toString().equals("true"))
								{
									val_handler = 1;									
									handler.sendEmptyMessage(0);									
								}
								else
								{
										SoapObject add_property =wb.export_header("GetInspectionStatus");
										add_property.addProperty("InspectorID",db.Insp_id);
										add_property.addProperty("SRID",cf.Homeid);
									
										String result=wb.export_footer(wb.envelope, add_property, "GetInspectionStatus");
										System.out.println("he status"+result);
										if(result.trim().equals("40") ||result.trim().equals("30"))
										{
											val_handler = 2;
											handler.sendEmptyMessage(0);	
										}
										else
										{
											val_handler = 1;
											handler.sendEmptyMessage(0);
										}
										
								}
		
								System.out.println("val_handler="+val_handler);
						}
						else
						{
							val_handler=6;
							handler.sendEmptyMessage(0);
						}
						
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						val_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						val_handler = 3;
						handler.sendEmptyMessage(0);
					}catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
						val_handler = 4;
						handler.sendEmptyMessage(0);
					}
				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (val_handler == 3) {
							val_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (val_handler == 4) {
							val_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (val_handler == 1) {
							val_handler = 0;
							Intent pay_intent = new Intent(Submit.this,ExportData.class);
							pay_intent.putExtra("homeid", cf.Homeid);
							startActivity(pay_intent);
						}
						else if (val_handler == 2) 
						{
							System.out.println("inspectioncount"+inspectioncount);
							
							if(inspectioncount.equals("10"))
							{
								//cf.ShowToast("You have now used up free inspection report allowances. You can't generate reports anymore.");
								final Dialog dialog1 = new Dialog(Submit.this,android.R.style.Theme_Translucent_NoTitleBar);
								dialog1.getWindow().setContentView(R.layout.alert);
								
								RelativeLayout li=(RelativeLayout)dialog1.findViewById(R.id.helprow);
								li.setVisibility(View.VISIBLE);
								TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
								txttitle.setText("Demonstration Expired");
								
								LinearLayout lin=(LinearLayout)dialog1.findViewById(R.id.camera);
								lin.setVisibility(v1.VISIBLE);
								
								TextView txt = (TextView) dialog1.findViewById(R.id.txtquestio);
								txt.setText("You have now used up free inspection report allowances.\nTo purchase more or extend your demonstration please click the Pay Now button below.");
								((EditText) dialog1.findViewById(R.id.ed_values)).setVisibility(View.GONE);
								
								Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
								btn_helpclose.setOnClickListener(new OnClickListener()
								{
								public void onClick(View arg0) {
										// TODO Auto-generated method stub
										dialog1.dismiss();
										//finish();
									}								
								});
	
								Button btnclear = (Button) dialog1.findViewById(R.id.clear);
								btnclear.setText("Cancel");	
								btnclear.setOnClickListener(new OnClickListener()
								{
									public void onClick(View arg0)
									{
										dialog1.dismiss();
									}
								});
															
															
								Button btn_ok = (Button) dialog1.findViewById(R.id.save);
								btn_ok.setText("Pay Now");
								
								btn_ok.setOnClickListener(new OnClickListener()
								{
										public void onClick(View arg0) {
											
											dialog1.dismiss();
											Intent pay_intent = new Intent(Submit.this,MakePayment.class);
											pay_intent.putExtra("homeid", cf.Homeid);
											pay_intent.putExtra("status", cf.status);
											startActivity(pay_intent);
											//finish();
										}
								});
								
								dialog1.setCancelable(false);
								dialog1.show();
							}
							else
							{
								Intent pay_intent = new Intent(Submit.this,MakePayment.class);
								pay_intent.putExtra("homeid", cf.Homeid);
								pay_intent.putExtra("status", cf.status);
								startActivity(pay_intent);
								finish();
							}
						}
						else if (val_handler == 6) 
						{
							val_handler = 0;
							cf.ShowToast("Sorry this inspection reallocate to some other inspector");
						}
						else if (val_handler == 5) 
						{
							val_handler = 0;
							Intent pay_intent = new Intent(Submit.this,ExportData.class);
							pay_intent.putExtra("homeid", cf.Homeid);
							startActivity(pay_intent);
						}
				}

				
				};
		
			}.start();
		}
		else
		{
			cf.ShowToast("Internet connection not available");
		}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(Submit.this, Maps.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}