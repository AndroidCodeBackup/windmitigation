package idsoft.inspectiondepot.windmitinspection;


import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class QuesSWR extends Activity {
	ListView list;
	//IDSoft2013!
	private static final int visibility = 0;
	TextWatcher watcher;
	String commentsfill,tmp="", commdescrip = "",inspectortypeid, helpcontent,InspectionType, status, comm, homeId, rdiochk, yearbuilt,
			permitdate, updatecnt, identity, roofswrvalueprev, roofswrcomment,conchkbox, comm2, descrip;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	RadioButton rdioA, rdioB, rdioC;
	int value, Count,viewimage = 1,optionid,commentsch;
	Intent iInspectionList;
	TextView txtswrheading,prevmitidata,helptxt,swr_TV_type;
	EditText comments, yrbuilt1, yrbuilt2, permitdate1, permitdate2;
	Button saveclose;
	View v1;
	boolean load_comment = true;
	CheckBox[] cb;
	CheckBox temp_st;
	AlertDialog alertDialog;
	
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);

		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);

		}
		setContentView(R.layout.swr);
		db.getInspectorId();
		db.getPHinformation(cf.Homeid);
		cf.getDeviceDimensions();
		db.changeimage(cf.Homeid);
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(QuesSWR.this,2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(QuesSWR.this, 26,cf, 1));
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null){
					cf.alertcontent = "To see help comments, please select SWR options.";
				}
				else if (rdiochk.equals("1") || rdioA.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection A, �SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else if (rdiochk.equals("2") || rdioB.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection B, �No SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else if (rdiochk.equals("3") || rdioC.isChecked()) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection C, �unable to verify/unknown� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				} else {
					cf.alertcontent = "To see help comments, please select SWR options.";
				}
				cf.showhelp("HELP",cf.alertcontent);
			}
		});
		txtswrheading = (TextView) findViewById(R.id.txtswrheading);

		txtswrheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>Standard Underlayments or hot mopped felts are not SWR"));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		db.getQuesOriginal(cf.Homeid);
		if(!db.swroriddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.swroriddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}
	
		this.rdioA = (RadioButton) this.findViewById(R.id.rdio1);
		this.rdioA.setOnClickListener(OnClickListener);
		this.rdioB = (RadioButton) this.findViewById(R.id.rdio2);
		this.rdioB.setOnClickListener(OnClickListener);
		this.rdioC = (RadioButton) this.findViewById(R.id.rdio3);
		this.rdioC.setOnClickListener(OnClickListener);
		
		comments = (EditText) this.findViewById(R.id.txtcomments);
		swr_TV_type = (TextView) findViewById(R.id.SH_TV_ED);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,swr_TV_type));
		
		setdata();
		
	}

	private void setdata() {
		// TODO Auto-generated method stub
		try {
			Cursor cur = db.wind_db.rawQuery("select * from "
					+ db.Questions + " where SRID='" + cf.Homeid
					+ "'", null);
			cur.moveToFirst();
			if (cur != null) {
				roofswrvalueprev = cur.getString(cur
						.getColumnIndex("SWRValue"));

				if (roofswrvalueprev.equals("1")) {
					rdioA.setChecked(true);
					rdiochk = "1";

				} else if (roofswrvalueprev.equals("2")) {
					rdioB.setChecked(true);
					rdiochk = "2";
				} else if (roofswrvalueprev.equals("3")) {
					rdioC.setChecked(true);
					rdiochk = "3";
				} else {

				}

			}
		} catch (Exception e) {
			
		}

		try {
			Cursor cur = db.wind_db.rawQuery("select * from " + db.QuestionsComments
					+ " where SRID='" + cf.Homeid + "'", null);
			cur.moveToFirst();
			if (cur != null) {
				roofswrcomment = db.decode(cur.getString(cur
						.getColumnIndex("SecondaryWaterComment")));
				comments.setText(roofswrcomment);
				
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SWR.this +" in retrieving QUES comments table on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			
		}
	}


	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.rdio1:
				rdiochk = "1";
				commentsfill = "This home was verified as meeting the requirements of selection A, �SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				comments.setText(commentsfill);
				AlertDialog.Builder builder = new AlertDialog.Builder(QuesSWR.this);
				builder.setTitle("Confirm")
						.setMessage(
								"Please confirm this home has a SWR protection that compiles with the 1802 requirements.proper verification and documentation must be provided.")
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}

								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {

									}

								});
				builder.show();
				rdioA.setChecked(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdio2:
				commentsfill = "This home was verified as meeting the requirements of selection B, �No SWR verified� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				comments.setText(commentsfill);
				rdiochk = "2";
				rdioA.setChecked(false);
				rdioB.setChecked(true);
				rdioC.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdio3:
				commentsfill = "This home was verified as meeting the requirements of selection C, �unable to verify/unknown� of the OIR B1 -1802 Question 6 Secondary Water Resistance (SWR).";
				comments.setText(commentsfill);
				rdiochk = "3";
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(true);
				viewimage = 1;
				break;

			}
		}
	};

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			SWR_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				if(load_comment)
				{
					load_comment=false;
					int loc1[] = new int[2];
					v.getLocationOnScreen(loc1);
					Intent in = cf.loadComments(tmp,loc1);
					in.putExtra("insp_ques", "6");
					in.putExtra("length", len);
					in.putExtra("max_length", 500);
					startActivityForResult(in, cf.loadcomment_code);
					
				}
			}
			else
			{
				cf.ShowToast("Please select the option for SWR");	
			}			
			break;
	
		
		case R.id.txthelpcontentoptionA:
			  cf.alerttitle="A - SWR";
			  cf.alertcontent="SWR (also called sealed roof deck) self-adhering, polymer-modified bitumen roofing underlayment applied directly to the sheathing or foam adhesive SWR barrier (not foamed-on insulation), applied as a supplemental means to protect the dwelling from water intrusion in the event of roof covering loss";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
		case R.id.txthelpcontentoptionB:
			  cf.alerttitle="B � No SWR";
			  cf.alertcontent="No SWR";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
			
			
		case R.id.txthelpcontentoptionC:
			  cf.alerttitle="C - Unknown";
			  cf.alertcontent="Unknown or undetermined";
			  cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		case R.id.hme:
			cf.gohome();
			break;

		case R.id.savenext:

			comm = comments.getText().toString();

			if (!rdioA.isChecked() && (!rdioB.isChecked())
					&& (!rdioC.isChecked())) {
				cf.ShowToast("Please select the SWR");

			} else if (comm.trim().equals("")) {
				cf.ShowToast("Please enter the Comments for SWR");
			} else {
				try {
					Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
							+ db.Questions + " WHERE SRID='"
							+ cf.Homeid + "'", null);
					int rws = c2.getCount();
					if (rws == 0) {
						db.wind_db.execSQL("INSERT INTO "
								+ db.Questions
								+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
								+ "VALUES ('" + cf.Homeid + "','','','" + 0
								+ "','" + 0 + "','" + 0 + "','','','','','" + 0
								+ "','" + 0 + "','','" + 0 + "','" + 0 + "','"
								+ 0 + "','" + 0 + "','" + 0 + "','','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + rdiochk
								+ "','','" + 0 + "','" + 0
								+ "','','','','','','','','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
								+ "','" + 0 + "','" + 0 + "','','" + 0 + "','"
								+ 0 + "','','" + 0 + "','','','" + cd + "','"
								+ 0 + "')");
					} else {
						db.wind_db.execSQL("UPDATE " + db.Questions
								+ " SET SWRValue='" + rdiochk + "'"
								+ " WHERE SRID ='" + cf.Homeid.toString() + "'");
					}

					Cursor c5 = db.wind_db.rawQuery("SELECT * FROM "
							+ db.QuestionsComments + " WHERE SRID='" + cf.Homeid
							+ "'", null);
					int rws5 = c5.getCount();
					if (rws5 == 0) {

						db.wind_db.execSQL("INSERT INTO "
								+ db.QuestionsComments
								+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
								+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','','','"+ db.encode(comments.getText().toString())+ "','','','','"+ cf.datewithtime + "')");
				

					} else {
						db.wind_db.execSQL("UPDATE "
								+ db.QuestionsComments
								+ " SET SecondaryWaterComment='"
								+ db.encode(comments.getText()
										.toString())
								+ "',CreatedOn ='"
								+ md + "'" + " WHERE SRID ='"
								+ cf.Homeid.toString() + "'");
					}
					db.getInspectorId();
					Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "
							+ db.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
							+ "'", null);
					int subchkrws = c3.getCount();
					if (subchkrws == 0) {
						db.wind_db.execSQL("INSERT INTO "
								+ db.SubmitCheckTable
								+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
								+ "VALUES ('" + db.Insp_id + "','"
								+ cf.Homeid
								+ "',0,0,0,0,0,0,1,0,0,0,0,0,0,0,0)");
					} else {
						db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
								+ " SET fld_swr='1' WHERE Srid ='" + cf.Homeid
								+ "' and InspectorId='" + db.Insp_id + "'");
					}

					updatecnt = "1";
					
					// swrtick.setVisibility(visibility);
				} catch (Exception e) {

					updatecnt = "0";
					cf.ShowToast("There is a problem in saving your data due to invalid character");
					//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ SWR.this +" problem in inserting or updating swr table on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
					
				}

				if (updatecnt == "1") {
					cf.ShowToast("SWR details has been saved successfully");
					iInspectionList = new Intent(QuesSWR.this, QuesOpenProt.class);
					iInspectionList.putExtra("homeid", cf.Homeid);
					iInspectionList.putExtra("status", cf.status);
					startActivity(iInspectionList);
					finish();

				}

			}
			break;
		
		}
	}

	private void SWR_suboption() {
		// TODO Auto-generated method stub
		if(rdioA.isChecked())	{		tmp="1";}
		else if(rdioB.isChecked()) {	tmp="2";}
		else if(rdioC.isChecked()) {	tmp="3";}
	    else{tmp="";}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesSWR.this, QuesRoofGeometry.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
				((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
			}
		}
		/*switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}*/

	}
	
}
