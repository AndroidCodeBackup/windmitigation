package idsoft.inspectiondepot.windmitinspection;

import java.io.IOException;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class MakePayment extends Activity {
	CommonFunctions cf;
	DataBaseHelper1 dbh1;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	Cursor curpay;
	EditText edt_ccaddress,edt_cczipcode,edt_cccity,edt_ccemail,edittext_nameoncard,edittext_creditcardnum,otheryearhome;
	Spinner spin_ccstate,spin_cccounty,spinner_cardtype,spinner_expmonth,spinner_expyear,spinner_cardno;
	String statevalue = "false",strstate,strstateid,county,strcountyid,strcounty,zipidentifier,
		   state,city,stateid,countyid,str_cc_address,str_cc_zipcode,str_cc_city,str_cc_email,retstr_county,
		   str_nameoncard,str_creditcardnum,str_cardtype,str_expmonth,str_expyear,str_automode="0";
	String [] arraystateid, arraystatename,arraycountyid, arraycountyname,arrcardno;
	ArrayAdapter stateadapter,countyadapter,cardtypeadap,expmonthadap,expyearadap;
	boolean countysetselection=false;
	CheckBox chkbx_automode;
	String StatusDetails,currentdate;
	Dialog payconfirm_dialog,payconfirm_optionselect;
	View v1;
	int show_handler,twoDigit,oneDigit,fourDigit,cardtypeid,istateid,fill=0;
	String[] cardtype ={"--Select--","Visa","Master Card","American Express","Discover"};
	String[] expmonth ={"","01","02","03","04","05","06","07","08","09","10","11","12"};
	String[] expyear ={"","Other","2013","2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2025",
			"2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", 
			"2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", 
			"2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064",
			"2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", 
			"2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090",
			"2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099", "2100"};
				
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf=new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
	     if (extras != null) {
			cf.getExtras(extras);				
	     }
	     
	     db=new DatabaseFunctions(this);
	     wb=new WebserviceFunctions(this);
	     setContentView(R.layout.payment);
	     
	     edt_ccaddress = (EditText)findViewById(R.id.cc_address);
	     edt_cczipcode = (EditText)findViewById(R.id.cc_zipcode);
	     edt_cccity = (EditText)findViewById(R.id.cc_city);
	     spin_ccstate = (Spinner)findViewById(R.id.cc_state);
	     statevalue = LoadState();
	     
	     spin_cccounty = (Spinner)findViewById(R.id.cc_county);
	     spin_ccstate.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					strstate = spin_ccstate.getSelectedItem().toString();
					istateid = spin_ccstate.getSelectedItemPosition();
					strstateid = arraystateid[istateid];
					System.out.println("strstate"+strstate);
					if (!strstate.equals("--Select--")) {
						System.out.println("inside");
					
						LoadCounty(strstateid);
						spin_cccounty.setEnabled(true);
						
						
						
					} else {
						System.out.println("inside spinner state elseee");
						// spinnercounty.setAdapter(null);
						spin_cccounty.setEnabled(false);
						arraycountyname = new String[0];
						countyadapter = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item,arraycountyname);
						countyadapter
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spin_cccounty.setAdapter(countyadapter);
						
					}

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
	     spin_cccounty.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					strcounty = spin_cccounty.getSelectedItem().toString();
					int countyid = spin_cccounty.getSelectedItemPosition();
					strcountyid = arraycountyid[countyid];
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub

				}
			});
	    
	    edt_ccemail = (EditText)findViewById(R.id.cc_email);
	    edittext_nameoncard = (EditText)findViewById(R.id.edittext_nameofcard);
	    edittext_nameoncard.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(edittext_nameoncard.getText().toString().startsWith(" "))
				{
					edittext_nameoncard.setText("");
				}
				if(edittext_nameoncard.getText().toString().startsWith("0"))
				{
					edittext_nameoncard.setText("");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    spinner_cardtype = (Spinner)findViewById(R.id.spinner_cardtype);
	    cardtypeadap = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item,cardtype);
		cardtypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_cardtype.setAdapter(cardtypeadap);
		spinner_cardtype.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_cardtype = spinner_cardtype.getSelectedItem().toString();
				cardtypeid = spinner_cardtype.getSelectedItemPosition();
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});

		edittext_creditcardnum = (EditText)findViewById(R.id.edittext_creditcardnumber);
		edittext_creditcardnum.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(edittext_creditcardnum.getText().toString().trim().startsWith(" "))
				{
					edittext_creditcardnum.setText("");
				}
				if(edittext_creditcardnum.getText().toString().trim().startsWith("0"))
				{
					edittext_creditcardnum.setText("");
				}
				if(edittext_creditcardnum.getText().toString().trim().equals("0000000000000000"))
				{
					edittext_creditcardnum.setText("");
				}
			
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
 
		spinner_expmonth = (Spinner)findViewById(R.id.spin_expiration_month);
		spinner_expyear = (Spinner)findViewById(R.id.spin_expiration_year);
		
		expmonthadap = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item,expmonth);
		expmonthadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_expmonth.setAdapter(expmonthadap);
		spinner_expmonth.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_expmonth = spinner_expmonth.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		
		
		expyearadap = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item,expyear);
		expyearadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		final Calendar c = Calendar.getInstance();
		final int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
		
		otheryearhome = (EditText) this.findViewById(R.id.otheryearhome);	
		otheryearhome.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			   	    if (otheryearhome.getText().toString().startsWith(" "))
			        {
			            // Not allowed
			   	    	otheryearhome.setText("");
			        }
			   	    if(otheryearhome.getText().toString().startsWith("0"))
			   	    {
			   	    	otheryearhome.setText("");
			   	    }
			   	    
			   	   if (!otheryearhome.getText().toString().trim().equals("")) {
			   		   try{
						if (Integer.parseInt((otheryearhome.getText().toString()
								.trim())) > year1) {
							otheryearhome.setText("");
							cf.ShowToast("Please enter a valid year");
						}
			   		   }catch(Exception e)
			   		   {}
					}
					/*if (Arrays.asList(expyear).contains(
							otheryearhome.getText().toString().trim())) {
						otheryearhome.setText("");
						cf.ShowToast("Please enter a year that is not in the year list");
					}*/
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		spinner_expyear.setAdapter(expyearadap);
		spinner_expyear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				str_expyear = spinner_expyear.getSelectedItem().toString();
				if (str_expyear.equals("Other")) 
				{
					otheryearhome.setVisibility(View.VISIBLE);
				} 
				else 
				{
					otheryearhome.setVisibility(View.GONE);
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});

		chkbx_automode = (CheckBox)findViewById(R.id.autopay);

	    edt_ccaddress.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(edt_ccaddress.getText().toString().startsWith(" "))
				{
					edt_ccaddress.setText("");
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	     
	     edt_cczipcode.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					if(edt_cczipcode.getText().toString().startsWith(" "))
					{
						edt_cczipcode.setText("");fill=0;
					}
					if(edt_cczipcode.getText().toString().equals("00000"))
					{
						edt_cczipcode.setText("");fill=0;
					}
					if(edt_cczipcode.getText().toString().trim().length()==5)
					{
						
						if(fill==0){
							spin_ccstate.setSelection(0);
							zipidentifier="zip1";
						Load_State_County_City(edt_cczipcode);}
					}
					else{
						fill=0;
					}
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
	     
	     edt_cccity.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					if(edt_cccity.getText().toString().startsWith(" "))
					{
						edt_cccity.setText("");
					}
					if(edt_cccity.getText().toString().startsWith("0"))
					{
						edt_cccity.setText("");
					}
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
	     
	    
	     edt_ccemail.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					if(edt_ccemail.getText().toString().startsWith(" "))
					{
						edt_ccemail.setText("");
					}
					if(edt_ccemail.getText().toString().startsWith("0"))
					{
						edt_ccemail.setText("");
					}
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
	     
	     ((Button)findViewById(R.id.home)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cf.gohome();
			}
		});
	     
	     ((Button)findViewById(R.id.clear)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {

					// TODO Auto-generated method stub
					edt_ccaddress.setText("");
					edt_cczipcode.setText("");
					edt_cccity.setText("");
					spin_ccstate.setSelection(0);
					spin_cccounty.setSelection(0);
					edt_ccemail.setText("");
					edittext_nameoncard.setText("");
					spinner_cardtype.setSelection(0);
					edittext_creditcardnum.setText("");
					spinner_expmonth.setSelection(0);
					spinner_expyear.setSelection(0);
					chkbx_automode.setChecked(false);
					edt_ccaddress.requestFocus();
					otheryearhome.setVisibility(v1.GONE);
					otheryearhome.setText("");
				}
			});
	     
	     ((Button)findViewById(R.id.payment)).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					payment_validation();
				}
	     });
	     
	     db.CreateTable(27);
			try
			{
				db.CreateTable(27);
				Cursor cur = db.wind_db.rawQuery("select * from "+db.PaymentDetails+" where inspectorid='" + db.Insp_id + "' and automode=1", null);
				if(cur.getCount()>0)
				{
					fill=1;
					showDialog();
				}
				else
				{
					fill=0;
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			
			if (statevalue == "true") 
			 {
					stateadapter = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item, arraystatename);
					stateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spin_ccstate.setAdapter(stateadapter);
			 }
			
			
				
	}
	private void showDialog() {
		// TODO Auto-generated method stub
		final Dialog payconfirm_dialog = new Dialog(MakePayment.this,android.R.style.Theme_Translucent_NoTitleBar);
		payconfirm_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		payconfirm_dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
		payconfirm_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		payconfirm_dialog.setCancelable(false);
		payconfirm_dialog.getWindow().setContentView(R.layout.paymentalert);
				
		((Button)payconfirm_dialog.findViewById(R.id.yes)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				payconfirm_dialog.dismiss();
				try
				{
					Cursor cur = db.wind_db.rawQuery("select * from "+db.PaymentDetails+" where inspectorid='" + db.Insp_id + "' and automode=1", null);
					if(cur.getCount()==1)
					{
						curpay = db.wind_db.rawQuery("select * from "+db.PaymentDetails+" where inspectorid='" + db.Insp_id + "' and automode=1", null);
						paymentautomodesave();
					}
					else if(cur.getCount()>1)
					{	
						arrcardno = new String[cur.getCount() + 1];
						arrcardno[0] = "--Select--";	
						cur.moveToFirst();
							int i = 1;
							do 
							{
								String cardno = db.decode(cur.getString(cur.getColumnIndex("cardtype")));
								arrcardno[i] = cardno;
								i++;
								if(cur.isLast())
								{
									show_dialog();
								}
							} while (cur.moveToNext());
					}
				}
				catch(Exception e)
				{
					System.out.println("payment selec "+e.getMessage());
				}				
			}
		});
		((Button)payconfirm_dialog.findViewById(R.id.no)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				payconfirm_dialog.dismiss();
				
			}
		});
      
	   payconfirm_dialog.setCancelable(false);
       payconfirm_dialog.show();


	}
	
	private void show_dialog()
	{
		payconfirm_optionselect = new Dialog(MakePayment.this,android.R.style.Theme_Translucent_NoTitleBar);
		payconfirm_optionselect.requestWindowFeature(Window.FEATURE_NO_TITLE);
		payconfirm_optionselect.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
		payconfirm_optionselect.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		payconfirm_optionselect.setCancelable(false);
		payconfirm_optionselect.getWindow().setContentView(R.layout.paymentselect);
		
		spinner_cardno =  ((Spinner)payconfirm_optionselect.findViewById(R.id.spincardno));
		ArrayAdapter cardnoadapter = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item,arrcardno);
		cardnoadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_cardno.setAdapter(cardnoadapter);
		
	
		((Button)payconfirm_optionselect.findViewById(R.id.select)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String str_cardno = spinner_cardno.getSelectedItem().toString();
				if(str_cardno.equals("--Select--"))
				{
					cf.ShowToast("Please select Card Number");
				}
				else
				{
					curpay= db.wind_db.rawQuery("select * from "+db.PaymentDetails+" where inspectorid='" + db.Insp_id + "' and cardtype='"+db.encode(str_cardno)+"' and automode=1", null);
					paymentautomodesave();
				}
			}
		});
		
		((Button)payconfirm_optionselect.findViewById(R.id.cancel1)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				payconfirm_optionselect.dismiss();
			}
		});
		payconfirm_optionselect.setCancelable(false);
		payconfirm_optionselect.show();
	}
	
	protected void paymentautomodesave() {
		// TODO Auto-generated method stub
		try
		{
			db.CreateTable(27);
			if(curpay.getCount()>0)
			{
				curpay.moveToFirst();
				String retstr_nameoncard = db.decode(curpay.getString(curpay.getColumnIndex("name")));
				String retstr_cardtype = db.decode(curpay.getString(curpay.getColumnIndex("cardtype")));
				String retstr_creditcardnum = db.decode(curpay.getString(curpay.getColumnIndex("creditcardnumber")));
				String retstr_expirationmonth = curpay.getString(curpay.getColumnIndex("expirationmonth"));
				String retstr_expirationyear = curpay.getString(curpay.getColumnIndex("expirationyear"));
				String retstr_expirationotheryear = curpay.getString(curpay.getColumnIndex("expirationotheryear"));
				String retstr_address = db.decode(curpay.getString(curpay.getColumnIndex("address")));
				String retstr_city = db.decode(curpay.getString(curpay.getColumnIndex("city")));
				int retstr_state = curpay.getInt(curpay.getColumnIndex("state"));
				retstr_county = db.decode(curpay.getString(curpay.getColumnIndex("county")));
				String retstr_zipcode = curpay.getString(curpay.getColumnIndex("zipcode"));
				String retstr_email = db.decode(curpay.getString(curpay.getColumnIndex("email")));
				
				statevalue="true";
				edittext_nameoncard.setText(retstr_nameoncard);
				spinner_cardtype.setSelection(cardtypeadap.getPosition(retstr_cardtype));
				edittext_creditcardnum.setText(retstr_creditcardnum);
				spinner_expmonth.setSelection(expmonthadap.getPosition(retstr_expirationmonth));
				spinner_expyear.setSelection(expyearadap.getPosition(retstr_expirationyear));
				
				
				if(retstr_expirationotheryear.equals(""))
				{
					otheryearhome.setVisibility(v1.GONE);
				}
				else
				{
					otheryearhome.setVisibility(v1.VISIBLE);
					otheryearhome.setText(retstr_expirationotheryear);
				}
					
				
				edt_ccaddress.setText(retstr_address);
				edt_cccity.setText(retstr_city);
				System.out.println("retstr_county="+retstr_county+countyadapter.getPosition(retstr_county));
				spin_ccstate.setSelection(retstr_state);
				spin_cccounty.setEnabled(true);
				spin_cccounty.setSelection(countyadapter.getPosition(retstr_county));
				edt_cczipcode.setText(retstr_zipcode);
				edt_ccemail.setText(retstr_email);
				
				chkbx_automode.setChecked(true);
				payconfirm_optionselect.dismiss();
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	protected void payment_validation() {
		// TODO Auto-generated method stub
		str_cc_address = edt_ccaddress.getText().toString();
		str_cc_zipcode = edt_cczipcode.getText().toString();
		str_cc_city = edt_cccity.getText().toString();
		str_cc_email = edt_ccemail.getText().toString();
		str_nameoncard = edittext_nameoncard.getText().toString();
		str_creditcardnum = edittext_creditcardnum.getText().toString();

		
		if(str_cc_address.trim().equals(""))
		{
			cf.ShowToast("Please enter Address");
			edt_ccaddress.requestFocus();
		}
		else
		{
			if(str_cc_zipcode.trim().equals(""))
			{
				cf.ShowToast("Please enter Zipcode");
				edt_cczipcode.requestFocus();
			}
			else if(str_cc_zipcode.trim().length()<5)
			{
				cf.ShowToast("Please enter valid Zipcode");
				edt_cczipcode.requestFocus();
			}
			else
			{
				if(str_cc_city.trim().equals(""))
				{
					cf.ShowToast("Please enter City");
					edt_cccity.requestFocus();
				}
				else
				{
					if(strstate.equals("--Select--"))
					{
						cf.ShowToast("Please select State");
					}
					else
					{
						if(strcounty.equals("--Select--"))
						{
							cf.ShowToast("Please select County");
						}
						else
						{
							if(str_cc_email.trim().equals(""))
							{
								cf.ShowToast("Please enter Email");
								edt_ccemail.requestFocus();
							}
							else
							{
								if(cf.Email_Validation(str_cc_email))
								{
									if(str_nameoncard.trim().equals(""))
									{
										cf.ShowToast("Please enter Name On Card");
										edittext_nameoncard.requestFocus();
									}
									else
									{
										if(str_cardtype.equals("--Select--"))
										{
											cf.ShowToast("Please select Card Type");
										}
										else
										{
											if(str_creditcardnum.trim().equals(""))
											{
												cf.ShowToast("Please enter Credit Card Number");
												edittext_creditcardnum.requestFocus();
											}
											else
											{
												if(str_creditcardnum.length()>=4)
												{
													 twoDigit=Integer.parseInt(str_creditcardnum.substring(0, 2));
											         System.out.println("----------twoDigit--"+twoDigit);
											         fourDigit=Integer.parseInt(str_creditcardnum.substring(0, 4));
													 System.out.println("----------fourDigit--"+fourDigit);
													 oneDigit=Integer.parseInt(Character.toString(str_creditcardnum.charAt(0)));
													 System.out.println("----------oneDigit--"+oneDigit);
													 
													 if(str_cardtype.equals("Visa"))
													 {
														 check_visacreditcard_validation();
													 }
													 else if(str_cardtype.equals("Master Card"))
													 {
														 check_mastercardcreditcard_validation();
													 }
													 else if(str_cardtype.equals("American Express"))
													 {
														 check_americancreditcard_validation();
													 }
													 else if(str_cardtype.equals("Discover"))
													 {
														 check_discovercreditcard_validation();
													 }


												}
												else
												{
													cf.ShowToast("Please enter valid "+str_cardtype+" Credit Card Number");
													edittext_creditcardnum.setText("");
													edittext_creditcardnum.requestFocus();

												}

											}
										}
									}
								}
								else
								{
									cf.ShowToast("Please enter valid Email");
									edt_ccemail.requestFocus();
								}
							}
						}
					}
				}
			}
		}
		
	}
	private void check_discovercreditcard_validation() {
		// TODO Auto-generated method stub
		if(str_cardtype.equals("Discover") && (str_creditcardnum.length()==16 && (fourDigit==6011 || fourDigit==6500)))
		{
			check_expirationdate_validation();
		}
		else
		{
			cf.ShowToast("Please enter valid "+str_cardtype+" Credit Card Number");
			
		}
	}

	private void check_americancreditcard_validation() {
		// TODO Auto-generated method stub
		if(str_cardtype.equals("American Express") && (str_creditcardnum.length()==15 && (twoDigit==34 || twoDigit==37)))
		{
			check_expirationdate_validation();
		}
		else
		{
			cf.ShowToast("Please enter valid "+str_cardtype+" Credit Card Number");
	    }
	}
	
	private void check_mastercardcreditcard_validation() {
		// TODO Auto-generated method stub
		if(str_cardtype.equals("Master Card") && (str_creditcardnum.length()==16 && twoDigit>=51 && twoDigit<=55))
		{
			check_expirationdate_validation();
		}
		else
		{
			cf.ShowToast("Please enter valid "+str_cardtype+" Credit Card Number");
		}
	}

	private void check_visacreditcard_validation() {
		// TODO Auto-generated method stub
		if(str_creditcardnum.length()==13 || str_creditcardnum.length()==16 && oneDigit==4)
		{
			check_expirationdate_validation();
		}
		else
		{
			cf.ShowToast("Please enter valid "+str_cardtype+" Credit Card Number");
		}
	}


	private void check_expirationdate_validation() {
		// TODO Auto-generated method stub
		if(!str_expmonth.equals("") && !str_expyear.equals(""))
		{
			if ((!str_expyear.equals("Other"))  || (!otheryearhome.getText().toString().equals("") && 4 == otheryearhome.getText().length())) 
			{
			
				if(chkbx_automode.isChecked())
				{
					str_automode ="1";
				}
				else
				{
					str_automode ="0";
				}
				
				payconfirm_dialog = new Dialog(MakePayment.this,android.R.style.Theme_Translucent_NoTitleBar);
				payconfirm_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				payconfirm_dialog.setCancelable(false);
				payconfirm_dialog.getWindow().setContentView(R.layout.cancelalert);
				 
				((TextView)payconfirm_dialog.findViewById(R.id.cancelid)).setText("Are you sure you want to debit the amount from the card?");
				
				((Button)payconfirm_dialog.findViewById(R.id.yes)).setText("Confirm");
				((Button)payconfirm_dialog.findViewById(R.id.yes)).setOnClickListener(new OnClickListener() {
	
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						db.CreateTable(27);
						System.out.println("stateid="+istateid+"\n"+cardtypeid);
						Send_payment();
					}
				});
				((Button)payconfirm_dialog.findViewById(R.id.no)).setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						payconfirm_dialog.dismiss();
						
					}
				});
	          
				payconfirm_dialog.setCancelable(false);
	           payconfirm_dialog.show();
	           
			}
			else
			{
				if(otheryearhome.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the other text for Expiration Year");	
				}
				else if(otheryearhome.getText().length()<4)
				{
					cf.ShowToast("Please enter Expiration Year in four digits");	
				}
				otheryearhome.requestFocus();
			}
		}
		else
		{
			cf.ShowToast("Please select Expiration Date");
		}
	}
	private void Send_payment() {
		// TODO Auto-generated method stub
		cf.show_ProgressDialog("Your Transaction is being processsed...");
    	new Thread() {
			public void run(){
				Looper.prepare();
				try
				{
					db.getPHinformation(cf.Homeid);
		SoapObject request = new SoapObject(wb.NAMESPACE,"PaymentGateway_Webservice");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("SRID",cf.Homeid);
		request.addProperty("OrderId",db.orderid);
		request.addProperty("CreditCardNumber",db.decode(str_creditcardnum));
		request.addProperty("ExpirationMonth",str_expmonth);
		if(str_expyear.equals("Other"))
		{
			request.addProperty("ExpirationYear",otheryearhome.getText().toString());
		}
		else
		{
			request.addProperty("ExpirationYear",str_expyear);
		}
		request.addProperty("Amount",2);		
		request.addProperty("NameOnCard",db.decode(str_nameoncard));
		request.addProperty("CreditCardAddress",db.decode(str_cc_address));		
		request.addProperty("CreditCardCity",db.decode(str_cc_city));
		request.addProperty("CreditCardState",istateid);		
		request.addProperty("CreditCardCounty",db.decode(strcounty));
		request.addProperty("CreditCardPostalCode",str_cc_zipcode);
		request.addProperty("Email",db.decode(str_cc_email));
		request.addProperty("CardId",cardtypeid);		
		request.addProperty("CardName",db.decode(str_cardtype));
		request.addProperty("Remarks","From Android Wind Mitigation");
		
		envelope.setOutputSoapObject(request);
		System.out.println("PaymentGateway_Webservice request is "+ request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		androidHttpTransport.call(wb.NAMESPACE+ "PaymentGateway_Webservice", envelope);
	
		SoapObject paymentresult = (SoapObject) envelope.getResponse();System.out.println("paymentresult"+paymentresult);
		String StatusCode = String.valueOf(paymentresult.getProperty("StatusCode"));
		StatusDetails = String.valueOf(paymentresult.getProperty("StatusDetails"));
		
		if(StatusCode.equals("0"))
		{
			show_handler=1;
		    handler.sendEmptyMessage(0); 
		}
		else
		{
			
			show_handler=2;
			handler.sendEmptyMessage(0);
		}
	
		}catch (SocketException e) {
			// TODO Auto-generated catch block
			System.out.println("SocketException"+e.getMessage());
			show_handler=3;
			handler.sendEmptyMessage(0);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IOException"+e.getMessage());
			show_handler=3;
			handler.sendEmptyMessage(0);
			
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			System.out.println("XmlPullParserException"+e.getMessage());
			show_handler=3;
			handler.sendEmptyMessage(0);
			
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("Eee"+e.getMessage());
			show_handler=4;
			handler.sendEmptyMessage(0);
		}
				}
	    	}.start();
	}
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			cf.pd.dismiss();
			if(show_handler == 1)
			{
				show_handler=0;	
				Cursor cur = db.wind_db.rawQuery("select * from "+db.PaymentDetails+" where srid='" + cf.Homeid + "'", null);
				if(cur.getCount()>0)
				{
					db.wind_db.execSQL("UPDATE " + db.PaymentDetails
							+ " SET OrderId='"+db.orderid+"',name='" + db.encode(str_nameoncard) 
							+ "',cardtype ='" + db.encode(str_cardtype)
							+ "',creditcardnumber='" + db.encode(str_creditcardnum) + "',expirationmonth='"
							+ str_expmonth + "',expirationyear='"+str_expyear+"',expirationotheryear='"+otheryearhome.getText().toString()+"',amount='1',address='"
							+db.encode(str_cc_address)+"',city='"+db.encode(str_cc_city)+"',state='"+istateid+"',county='"
							+db.encode(strcounty)+"',zipcode='"+str_cc_zipcode+"',email='"+db.encode(str_cc_email)+"',cardid='"
							+cardtypeid+"',automode='"+str_automode+"' WHERE srid ='" + cf.Homeid + "' and inspectorid='"+db.Insp_id+"'");
				}
				else
				{
					db.wind_db.execSQL("INSERT INTO "
							+ db.PaymentDetails
							+ " (inspectorid,srid,OrderId,name,cardtype,creditcardnumber,expirationmonth,expirationyear,expirationotheryear,amount,address,city,state,county,zipcode,email,cardid,automode)"
							+ "VALUES ('"+db.Insp_id+"','" + cf.Homeid + "','"+db.orderid+"','" + db.encode(str_nameoncard) + "','"
							+ db.encode(str_cardtype) + "','" + db.encode(str_creditcardnum) + "','"+str_expmonth+"','"+str_expyear+"','"+otheryearhome.getText().toString()+"','1','"
							+db.encode(str_cc_address)+"','"+db.encode(str_cc_city)+"','"+istateid+"','"+db.encode(strcounty)+"','"+str_cc_zipcode+"','"
							+db.encode(str_cc_email)+"','"+cardtypeid+"','"+str_automode+"')");
				}
									
				
				String last4digits = str_creditcardnum.substring(str_creditcardnum.length()-4);
				
			    final Dialog dialog1 = new Dialog(MakePayment.this,android.R.style.Theme_Translucent_NoTitleBar);
				dialog1.getWindow().setContentView(R.layout.alert);
				RelativeLayout li=(RelativeLayout)dialog1.findViewById(R.id.helprow);
				li.setVisibility(View.VISIBLE);
				LinearLayout lin=(LinearLayout)dialog1.findViewById(R.id.camera);
				lin.setVisibility(View.VISIBLE);
				TextView txttitle = (TextView) li.findViewById(R.id.txthelp);
				txttitle.setText("Payment Successful");
				TextView txt = (TextView) dialog1.findViewById(R.id.txtquestio);
				txt.setText("Thank you for your payment!\nAn amount of $2 has been debited from your card number xxxx"+last4digits);
				((EditText) dialog1.findViewById(R.id.ed_values)).setVisibility(View.GONE);
				
				Button btn_helpclose = (Button) li.findViewById(R.id.helpclose);
				btn_helpclose.setOnClickListener(new OnClickListener()
				{
				public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						payconfirm_dialog.dismiss();
					}
					
				});

				Button btnclear = (Button) dialog1.findViewById(R.id.clear);
				btnclear.setVisibility(View.GONE);								
				
				Button btn_ok = (Button) dialog1.findViewById(R.id.save);
				btn_ok.setText("OK");
				btn_ok.setVisibility(View.VISIBLE);
				btn_ok.setOnClickListener(new OnClickListener()
				{
				public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog1.dismiss();
						payconfirm_dialog.dismiss();
						Intent pay_intent = new Intent(MakePayment.this,ExportData.class);
						pay_intent.putExtra("homeid", cf.Homeid);
						startActivity(pay_intent);
						
					}					
				});				
				dialog1.setCancelable(false);
				dialog1.show();
			}
			else if(show_handler == 2)
			{
				cf.ShowToast(StatusDetails);
				show_handler=0;
				payconfirm_dialog.dismiss();
			}
			else if(show_handler == 3)
			{
				cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
				show_handler=0;
			}
			else if(show_handler == 4)
			{
				cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
				show_handler=0;
			}			
		}
    };
	private void Load_State_County_City(final EditText et)
	{

		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing... "
					+ "Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(MakePayment.this,"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						System.out.println("et.getTe"+et.getText().toString());
						chklogin1 = wb.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							
						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								cf.ShowToast("Please enter a valid Zip");
								cf.hidekeyboard(((EditText)findViewById(R.id.cc_zipcode)));
								
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
		}
	
	}
	private void Load_State_County_City(SoapObject objInsert)
	{
		    cf.hidekeyboard(((EditText)findViewById(R.id.cc_zipcode)));
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
			spin_ccstate.setSelection(stateadapter.getPosition(state));
			countysetselection=true;
			edt_cccity.setText(city);
	}
	public String LoadState() {
		try {
			dbh1 = new DataBaseHelper1(MakePayment.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",null);System.out.println("statevaluecur="+cur.getCount());
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = db.decode(cur.getString(cur.getColumnIndex("stateid")));
					String Category = db.decode(cur.getString(cur.getColumnIndex("statename")));
					System.out.println("Category "+Category);
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}
	public void LoadCounty(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(MakePayment.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + db.encode(stateid) + "' order by countyname", null);System.out.println("LoadCounty"+cur.getCount());
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = db.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = db.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					System.out.println("Name= "+Name);
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}
	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(MakePayment.this,android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_cccounty.setAdapter(countyadapter);
		System.out.println("countyse"+countysetselection);
		if(countysetselection)
		{
			spin_cccounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		else
		{
			spin_cccounty.setSelection(countyadapter.getPosition(retstr_county));
		}
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(MakePayment.this, Submit.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
