package idsoft.inspectiondepot.windmitinspection;



import java.io.File;

import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class Select_phots extends Activity {

	
	private String[] pieces1;
	private File[] Currentfile_list;
	private String[] pieces3;
	private int limit_start=0;
	LinearLayout lnrlayoutparent,lnrlayoutchild;
	RelativeLayout Lin_Pre_nex,Rec_count_lin;
	Button backfolderwise,cancelfolderwise,selectfolderwise ;
	ImageView prev,next,imgviewparent[];
	TextView Total_Rec_cnt,showing_rec_cnt,titleheader,childtitleheader,tvstatusparent[],textviewvalue_parent;
	ScrollView scr2;
	private String finaltext="",selectedtestcaption[],pathofimage,array_indb[];
	int selectedcount=0,maximumindb=0,backclick=0;
	Map<String, TextView> mapfolder = new LinkedHashMap<String, TextView>();
	CommonFunctions cf;
	Dialog dialog,dialogfullimage;
	protected String repidofselbtn1;
	protected int cvrtstr1,Total_Maximumcount;
	TextView textviewvalue_child,Remaining_count;
	int chk;
	String pdf="false",unlimited="false";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.showfolderlist);
		lnrlayoutparent = (LinearLayout) findViewById(R.id.linscrollviewfolder);
		lnrlayoutchild = (LinearLayout) findViewById(R.id.linscrollviewimages);
		backfolderwise = (Button) findViewById(R.id.backfolderwise);
		cancelfolderwise = (Button) findViewById(R.id.cancelfolderwise);
		
		
		Lin_Pre_nex = (RelativeLayout) findViewById(R.id.linscrollview_Next);
		prev =(ImageView) findViewById(R.id.S_I_Prev);
		prev.setVisibility(View.GONE);
		next =(ImageView) findViewById(R.id.S_I_Next);
		scr2 =(ScrollView) findViewById(R.id.scr2);
		Rec_count_lin =(RelativeLayout) findViewById(R.id.Rec_count_lin);
		Total_Rec_cnt =(TextView) findViewById(R.id.Total_rec_cnt);
		showing_rec_cnt =(TextView) findViewById(R.id.showing_rec_cnt);
		titleheader = (TextView) findViewById(R.id.parenttitleheader);
		childtitleheader = (TextView) findViewById(R.id.child_titleheader);
		selectfolderwise = (Button) findViewById(R.id.selectfolderwise);
		//ImageView Search = (ImageView) findViewById(R.id.search);
		Remaining_count = (TextView) findViewById(R.id.Remaining_count);
	 	/*	Search.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 //final Dialog dialog1 = new Dialog(photos.this);
				*//**Search based on the folder name starts here **//*
				Search_by_folder(); 
    	       *//**Search based on the folder name Ends here**//*
    			
			}
		});
*/
        cancelfolderwise.setOnClickListener(new OnClickListener() {
			

			public void onClick(View v) {
				Intent n= new Intent();
				n.putExtra("Result", -1);
				setResult(RESULT_CANCELED,n);
				try {
					//cf.show_toast("Not allowed to access ", 1);
					finish();
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					System.out.println("issues in the final"+e.getMessage());
					e.printStackTrace();
				}
				
			}
		});
	    	backfolderwise.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				if(!titleheader.getText().toString().equals(Environment.getExternalStorageDirectory()+"/"))
				{
					backclick=1;
	 				pieces1=null;pieces3=null;limit_start=0;
	 				
	 	 				String value = titleheader.getText().toString();
	 					String[] bits = value.split("/");	 		 					
	 					String picname = bits[bits.length - 1];
	 					if(value.equals(Environment.getExternalStorageDirectory()+"/"))
	 					{
	 						finaltext="";
	 						
	 						pathofimage  = value.replace(picname+"/", "");
	 					
	 					}
	 					else
	 					{
	 						String tempstr = value.substring(0,value.length()-1);
		 					int bits1 = tempstr.lastIndexOf("/");
		 					String pathofimage1=tempstr.substring(0,bits1+1);
	 						pathofimage  = pathofimage1;
	 					//	pathofimage = value.replace(picname+"/", "");
	 					}
	 					
				titleheader.setText(pathofimage);
				finaltext=pathofimage;
				File f = new File(pathofimage);
				
				
				String[] bits1 = pathofimage.split("/");
			String picname1 = bits1[bits1.length - 1];
			childtitleheader.setText(picname1); // set the chaild title
			
				walkdir(f);
				dynamicimagesparent();
				dynamicimageschild();
						}
				else
				{

					if(titleheader.getText().toString().equals(Environment.getExternalStorageDirectory()+"/"))
					{
					final AlertDialog.Builder ab =new AlertDialog.Builder(Select_phots.this);
					ab.setTitle("Confirmation");
					ab.setMessage(Html.fromHtml("Do you want to close the file selection"));
					ab.setIcon(R.drawable.alertmsg);
					ab.setCancelable(false);
					ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent n= new Intent();
							n.putExtra("Result", -1);
							setResult(RESULT_CANCELED,n);
							try {
								//cf.show_toast("Not allowed to access ", 1);
								finish();
							} catch (Throwable e) {
								// TODO Auto-generated catch block
								System.out.println("issues in the final"+e.getMessage());
								e.printStackTrace();
							}
						}

						
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							
							
						}
					});
					AlertDialog al=ab.create();
					al.show();
					}

					//cf.show_toast("You can not go back.",1);
				}
			}
		});

	    	selectfolderwise.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(selectedcount!=0){
					/** We send the selected list of the image ***/
					Intent n= new Intent();
					Bundle b=Select_phots.this.getIntent().getExtras();
					
					b.putStringArray("Selected_array", selectedtestcaption);
					n.putExtras(b);
					setResult(RESULT_OK,n);
					finish(); 
					/** We send the selected list of the image ends ***/
				if(!"".equals(finaltext)){
					finaltext="";
				}
				//cf.show_toast("Image saved successfully", 1);
			selectedcount=0;
				selectedtestcaption=null;		
				maximumindb=0;
				pieces1=null;
				
				pieces3=null;
				if(!"".equals(finaltext)){
					finaltext="";
				}
				
			}else
			{
				cf.ShowToast("Please select atleast one image to upload");
			}
			}
		}); /**Select ends**/
        		
		Bundle b=this.getIntent().getExtras();
		
		if(b!=null)
		{
		array_indb=b.getStringArray("Selectedvalue"); 
		System.out.println("array_indb"+array_indb+"  b length"+b.keySet());
		if(array_indb!=null)
		{
			System.out.println("array_indb"+array_indb.length);
		}
		maximumindb=b.getInt("Maximumcount");
		Total_Maximumcount=b.getInt("Total_Maximumcount");
		
		pdf=b.getString("PDF");
		unlimited=b.getString("unlimeted");
		if(pdf==null)
		{
			pdf="false";	
		}
		if(unlimited==null)
		{
			unlimited="false";	
		}
		if(!unlimited.equals("true"))
		{
			Remaining_count.setText(" Saved image count :"+maximumindb+" Selected For save :0"+" Remaining :"+(Total_Maximumcount-maximumindb));
		}
		else
		{
			Remaining_count.setVisibility(View.GONE);
		}
		
		
		String t=b.getString("ok");
		if(t!=null)
		{
			if(t.equals("true"))
			{
				((Button) findViewById(R.id.selectfolderwise)).setText("Ok");
			}
			
			
		}
		
		
		cf=new CommonFunctions(this);
		File images = Environment.getExternalStorageDirectory();
		walkdir(images);
		show_imaege();
		}
		else
		{
			Intent n= new Intent();
			n.putExtra("Result", -1);
			setResult(RESULT_CANCELED,n);
			try {
				//cf.show_toast("Not allowed to access ", 1);
				finish();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				System.out.println("issues in the final"+e.getMessage());
				e.printStackTrace();
			}
		}
		
		
	}
	private void dynamicimagesparent() {
	// TODO Auto-generated method stub
		
	lnrlayoutparent.removeAllViews();
	
	//titleheader = new TextView(this);
	if(finaltext.equals("")||finaltext.equals("null"))
	{	
		titleheader.setText("/mnt/sdcard/");
		
		childtitleheader.setText("sdcard");
		
	}
	else
	{
		
		if(backclick==1)
		{
			titleheader.setText(pathofimage);backclick=0;
		}
		else
		{
			titleheader.setText(finaltext);
		}
	}
	
	if(pieces1!=null)
	{
	
		tvstatusparent = new TextView[pieces1.length];
		imgviewparent = new ImageView[pieces1.length];		
	
	for (int i = 0; i < pieces1.length; i++) {
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayoutparent.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				50, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 10;
		paramschk.topMargin = 5;
		paramschk.bottomMargin = 15;
		l2.addView(lchkbox);

		imgviewparent[i] = new ImageView(this);
		imgviewparent[i].setBackgroundResource(R.drawable.allfilesicon);
		lchkbox.addView(imgviewparent[i], paramschk);

		LinearLayout ltextbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramstext.topMargin = 5;
		paramstext.leftMargin = 15;
		l2.addView(ltextbox,paramstext);
		
		tvstatusparent[i] = new TextView(this);
		tvstatusparent[i].setText(pieces1[i]);
		tvstatusparent[i].setTextColor(Color.BLACK);
		tvstatusparent[i].setTextSize(16);
		tvstatusparent[i].setMinimumWidth(100);
		mapfolder.put("tvstatusparent" + i, tvstatusparent[i]);
		tvstatusparent[i].setTag("tvstatusparent" + i);
		imgviewparent[i].setTag("tvstatusparent" + i);
		ltextbox.addView(tvstatusparent[i], paramstext);
		
		tvstatusparent[i].setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				pieces1=null;pieces3=null;
				String gettagvaluparent = v.getTag().toString();
				String repidofselected = gettagvaluparent.replace("tvstatusparent","");
				final int inconevalueparent = Integer.parseInt(repidofselected) + 1;
				textviewvalue_parent = mapfolder.get(gettagvaluparent);
				
				subfoldersclick(inconevalueparent,textviewvalue_parent);
			}
		});
		
		imgviewparent[i].setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				pieces1=null;pieces3=null;
				String gettagvaluparent = v.getTag().toString();
				String repidofselected = gettagvaluparent.replace("tvstatusparent","");
				final int inconevalueparent = Integer.parseInt(repidofselected) + 1;
				textviewvalue_parent = mapfolder.get(gettagvaluparent);
				
				subfoldersclick(inconevalueparent,textviewvalue_parent);
			}
		});
		
	}
	}
	
	else
	{
		
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayoutparent.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 15;
		paramschk.topMargin = 20;
		l2.addView(lchkbox);
		
		
		TextView tvnorecordparent = new TextView(this);
		tvnorecordparent.setText("No Sub Folders");
		tvnorecordparent.setTextColor(Color.BLACK);
		tvnorecordparent.setMinimumWidth(200);
		lchkbox.addView(tvnorecordparent,paramschk);
	}
	
	
}
	private void subfoldersclick(int inconevalueparent,TextView textviewvalue_parent) {
	// TODO Auto-generated method stub
	String value = titleheader.getText().toString();	
	if(value=="/mnt/sdcard/")
	{
		titleheader.setText(value+textviewvalue_parent.getText().toString()+"/");
	}
	else
	{
			if ((value.endsWith(".jpg"))||(value.endsWith(".JPG"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")
					 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp")))
			{
				String[] bits = value.split("/");
				String picname = bits[bits.length - 1];
				String val = value.replace(picname, textviewvalue_parent.getText().toString());
				titleheader.setText(val);
			}
			else
			{
				titleheader.setText(value+textviewvalue_parent.getText().toString()+"/");
			}
	} 
		if ((value.endsWith(".jpg"))||(value.endsWith(".JPG"))||(value.endsWith(".jpeg"))||(value.endsWith(".png"))||(value.endsWith(".gif")
				 ||(value.endsWith(".JPEG"))||(value.endsWith(".PNG")))||(value.endsWith(".gif"))||(value.endsWith(".bmp"))){
			dialog = new Dialog(Select_phots.this);
	 		dialog.setContentView(R.layout.alertfullimage);
	 		dialog.setTitle("Select Images");
	 		dialog.setCancelable(true);
	 		ImageView fullimageview = (ImageView) dialog.findViewById(R.id.full_image);
	 		Button selectBtnfullimage = (Button) dialog.findViewById(R.id.selectBtnfullimage);
	 		Button cancelBtnfullimage = (Button) dialog.findViewById(R.id.cancelBtnfullimage);
	 	
	 		BitmapFactory.Options o2 = new BitmapFactory.Options();
			int scale = 0;
			o2.inSampleSize = scale;
			Bitmap bitmap = null; 
			try {
				bitmap = BitmapFactory.decodeStream(new FileInputStream(titleheader.getText().toString()), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				fullimageview.setImageDrawable(bmd);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cancelBtnfullimage.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					dialog.cancel();
				}
			});
			dialog.show();
		}
		else
		{
			finaltext = titleheader.getText().toString();
			File f = new File(titleheader.getText().toString());
			String[] bits = titleheader.getText().toString().split("/");
			String picname1 = bits[bits.length - 1];
			childtitleheader.setText(picname1);
			walkdir(f);   	
			dynamicimagesparent();
		}
			 dynamicimageschild();

}
	private void dynamicimageschild() {
	
	lnrlayoutchild.removeAllViews();
	if((pieces3==null))
	{
		LinearLayout l2 = new LinearLayout(this);
		l2.setOrientation(LinearLayout.HORIZONTAL);
		lnrlayoutchild.addView(l2);

		LinearLayout lchkbox = new LinearLayout(this);
		LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramschk.leftMargin = 15;
		l2.addView(lchkbox);
		
		
		TextView tvnorecordchild = new TextView(this);
		tvnorecordchild.setText("No Records Found");
		tvnorecordchild.setTextSize(16);
		tvnorecordchild.setTextColor(Color.BLACK);
		tvnorecordchild.setMinimumWidth(200);
		lchkbox.addView(tvnorecordchild,paramschk);
	}
	
	else
	{	
		
		
		
		TextView[] tvstatuschild = new TextView[pieces3.length];
		ImageView[] thumbviewimgchild = new ImageView[pieces3.length];
		final CheckBox[] chkboxchild = new CheckBox[pieces3.length];
		//imgview1 = new ImageView[pieces3.length];
		Bitmap[] thumbnails1 = new Bitmap[pieces3.length];
	
		for (int i = 0; i < pieces3.length; i++) {
		
			LinearLayout l2 = new LinearLayout(this);
			l2.setOrientation(LinearLayout.HORIZONTAL);
			lnrlayoutchild.addView(l2);
			LinearLayout lchkbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramschk = new LinearLayout.LayoutParams(
					50, ViewGroup.LayoutParams.WRAP_CONTENT);
			paramschk.leftMargin = 5;
			paramschk.topMargin = 5;
			paramschk.bottomMargin = 15;
			l2.addView(lchkbox);
			LinearLayout ltextbox = new LinearLayout(this);
			LinearLayout.LayoutParams paramstext = new LinearLayout.LayoutParams(
					300, ViewGroup.LayoutParams.WRAP_CONTENT);
			thumbviewimgchild[i] = new ImageView(this);
			chkboxchild[i] = new CheckBox(this);
			chkboxchild[i].setPadding(30, 0, 0, 0);
				lchkbox.addView(chkboxchild[i], paramschk);
				paramstext.topMargin = 5;
				paramstext.leftMargin = 15;
				l2.addView(ltextbox);
				
				
				LinearLayout ltextbox6= new LinearLayout(this);
				LinearLayout.LayoutParams paramstext6 = new LinearLayout.LayoutParams(
						75, 75);
				paramstext6.bottomMargin = 5;
				paramstext6.leftMargin = 10;
				ltextbox.addView(ltextbox6);
				System.out.println("issues start"+i+"="+pieces3[i]);
				if(pieces3[i].trim().endsWith(".pdf"))
				{
					thumbviewimgchild[i].setImageDrawable(getResources().getDrawable(R.drawable.pdficon));
				}
				else
				{
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				String j="0"; 
				try {
					BitmapFactory.decodeStream(new FileInputStream(titleheader.getText().toString()+pieces3[i]),
							null, o);
					j = "1";
				} catch (FileNotFoundException e) {
					j = "2";
				}
				if (j.equals("1")) {
					final int REQUIRED_SIZE = 100;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					// Decode with inSampleSize
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					Bitmap bitmap = null;
					try {
						bitmap = BitmapFactory.decodeStream(new FileInputStream(
								titleheader.getText().toString()+pieces3[i]), null, o2);
						
					
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					thumbviewimgchild[i].setImageDrawable(bmd);
				}	
				
				
				}
				ltextbox6.addView(thumbviewimgchild[i], paramstext6);
				LinearLayout ltextbox1= new LinearLayout(this);
				LinearLayout.LayoutParams paramstext1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				paramstext1.topMargin = 5;
				paramstext1.leftMargin = 15;
				ltextbox6.addView(ltextbox1);
				tvstatuschild[i] = new TextView(this);
				tvstatuschild[i].setText(pieces3[i]);
				tvstatuschild[i].setTextColor(Color.BLACK);
				tvstatuschild[i].setTextSize(16);
				tvstatuschild[i].setMinimumWidth(100);
				tvstatuschild[i].setTag("tvstatus" + i);
				thumbviewimgchild[i].setTag("tvstatus" + i);
				chkboxchild[i].setTag("tvstatus" + i);
				
				for(int k=0;k<selectedcount;k++)
				{
					if(selectedtestcaption[k].equals(titleheader.getText().toString()+pieces3[i]))
					{
						chkboxchild[i].setChecked(true);
					}
				}
				mapfolder.put("tvstatus" + i, tvstatuschild[i]);
				ltextbox1.addView(tvstatuschild[i], paramstext1);
				
			chkboxchild[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					String repidofselbtn = getidofselbtn.replace("tvstatus","");
					 chk = Integer.parseInt(repidofselbtn);
					final int cvrtstr = Integer.parseInt(repidofselbtn) + 1;
					 textviewvalue_child = mapfolder.get(getidofselbtn);
					boolean bu =common(titleheader.getText().toString()+textviewvalue_child.getText().toString());
				
					if (bu) {
							if(chkboxchild[chk].isChecked()==false)
							{
								selectedtestcaption=Delete_from_array(selectedtestcaption,titleheader.getText().toString()+textviewvalue_child.getText().toString());
								if(!pdf.equals("true"))
								{
									selectedcount=selectedcount-1;
									Remaining_count.setText(" Saved image count :"+maximumindb+" Selected For save :"+selectedcount+" Remaining :"+(Total_Maximumcount-(maximumindb+selectedcount)));
								}
								
							}
							else
							{
								
									Bitmap b=ShrinkBitmap(titleheader.getText().toString()+textviewvalue_child.getText().toString(), 400, 400);
								
								
								if(b!=null || textviewvalue_child.getText().toString().endsWith(".pdf"))
								{
									if((maximumindb+selectedcount)<Total_Maximumcount && !pdf.equals("true"))
									{
									
										
											
													int	isexist = check_availability(titleheader.getText().toString()+textviewvalue_child.getText().toString());
													
														if (isexist == 0) {
															
															selectedtestcaption=dynamicarraysetting(titleheader.getText().toString()+pieces3[chk],selectedtestcaption);
															
															selectedcount++;
															Remaining_count.setText(" Saved image count :"+maximumindb+" Selected For save :"+selectedcount+" Remaining :"+(Total_Maximumcount-(maximumindb+selectedcount)));
														} else {
															
															
															chkboxchild[chk].setChecked(false);
															cf.ShowToast("Already Exists! Please select a different Image");
															
														}
										}
									else if(pdf.equals("true"))
									{
										int	isexist = check_availability(titleheader.getText().toString()+textviewvalue_child.getText().toString());
										
										if (isexist == 0) {
											
											selectedtestcaption=dynamicarraysetting(titleheader.getText().toString()+pieces3[chk],selectedtestcaption);
											
											selectedcount++;
											Remaining_count.setText(" Saved image count :"+maximumindb+" Selected For save :"+selectedcount+" Remaining :"+(Total_Maximumcount-(maximumindb+selectedcount)));
										} else {
											
											
											chkboxchild[chk].setChecked(false);
											cf.ShowToast("Already Exists! Please select a different Image");
											
										}
									}
									else
										{
											chkboxchild[chk].setChecked(false);
											cf.ShowToast("You are allowed to select only "+Total_Maximumcount+" images per Elevation");
										}
								}
								else
								{
									cf.ShowToast("Unsupported format! Unable to upload");
									chkboxchild[chk].setChecked(false);
								}
							}
					}else {
						chkboxchild[chk].setChecked(false);
						cf.ShowToast("Your file size exceeds 2MB");

				}
				}

			
			
				});
				thumbviewimgchild[i].setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					String getidofselbtn = v.getTag().toString();
					repidofselbtn1= getidofselbtn.replace("tvstatus","");
					cvrtstr1 = Integer.parseInt(repidofselbtn1) + 1;
					textviewvalue_child = mapfolder.get(getidofselbtn);
					
					//boolean bu = upr.common(textviewvalue_child.getText().toString());
					//if (bu) {
					if ((titleheader.getText().toString().endsWith(".jpg"))||(titleheader.getText().toString().endsWith(".JPG"))||(titleheader.getText().toString().endsWith(".jpeg"))||(titleheader.getText().toString().endsWith(".png"))||(titleheader.getText().toString().endsWith(".gif")
			   				 ||(titleheader.getText().toString().endsWith(".JPEG"))||(titleheader.getText().toString().endsWith(".PNG")))||(titleheader.getText().toString().endsWith(".gif"))||(titleheader.getText().toString().endsWith(".bmp"))|| (titleheader.getText().toString().endsWith(".pdf"))){
						
						
						String[] bits = titleheader.getText().toString().split("/");	
						String picname = bits[bits.length - 1];
						String val = titleheader.getText().toString().replace(picname, textviewvalue_child.getText().toString());
						
						titleheader.setText(val);
					
					}
					//else if()
					else
					{
						if(titleheader.getText().toString().equals(Environment.getExternalStorageDirectory()+"/"))
						{
							titleheader.setText(titleheader.getText().toString()+textviewvalue_child.getText().toString());
						}
						else
						{
							titleheader.setText(titleheader.getText().toString()+textviewvalue_child.getText().toString());
						}
							
					}
					if(!textviewvalue_child.getText().toString().endsWith(".pdf"))
					{
						show_fullimag(v,chkboxchild);
					}
					else
					{
						show_pdfview(v,chkboxchild);
					}
				}
				
				
			});
		}
		if(scr2!=null)
		{
			scr2.scrollTo(0, 0);
		
		}
		}
	
}

	protected void show_pdfview(View v, CheckBox[] chkboxchild) {
		// TODO Auto-generated method stub
		 File file = null;
			//file = new File(URLDecoder.decode(suboffstring, "UTF-8"));
		 System.out.println("the value in the image"+titleheader.getText().toString());
			file = new File(titleheader.getText().toString());
			String value = titleheader.getText().toString();
			String[] bits = value.split("/");
			String picname = bits[bits.length - 1];
			
			titleheader.setText(value.replace(picname, ""));
             if (file.exists()) {
          	Uri path = Uri.fromFile(file);
              Intent intent1 = new Intent(Intent.ACTION_VIEW);
              intent1.setDataAndType(path, "application/pdf");
              intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

              try {
                  startActivity(intent1);
                  finish();
              } 
              catch (ActivityNotFoundException e) {
            	  cf.ShowToast("Unable to view PDF. Please install PDF Application and try again");
                 
              }
             }
	}
	protected void show_fullimag(View v,final CheckBox[] chkboxchild) {
		// TODO Auto-generated method stub
		
		
		
		dialogfullimage = new Dialog(Select_phots.this);
		dialogfullimage.setContentView(R.layout.alertfullimage);
		dialogfullimage.setTitle("Select Images");
		dialogfullimage.setCancelable(true);
		ImageView	fullimageview = (ImageView) dialogfullimage.findViewById(R.id.full_image);
 		Button selectfullimage = (Button) dialogfullimage.findViewById(R.id.selectBtnfullimage);
 		Button cancelfullimage = (Button) dialogfullimage.findViewById(R.id.cancelBtnfullimage);
 		TextView imagepath = (TextView) dialogfullimage.findViewById(R.id.imagepath);
 		imagepath.setText(titleheader.getText().toString());
 		
 		Bitmap b = ShrinkBitmap(titleheader.getText().toString(), 130, 130);
 		fullimageview.setImageBitmap(b);
 		
		
		cancelfullimage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				//pieces1=null;pieces2=null;pieces3=null;
				String value = titleheader.getText().toString();
				String[] bits = value.split("/");
				String picname = bits[bits.length - 1];
				titleheader.setText(value.replace(picname, ""));
				dialogfullimage.setCancelable(true);
				dialogfullimage.cancel();
			}
		});
		selectfullimage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				boolean bu = common(titleheader.getText().toString());
				if (bu) 
				{
					
					Bitmap b=ShrinkBitmap(titleheader.getText().toString(), 400, 400);
					
					if(b!=null)
					{
					
						if(chkboxchild[Integer.parseInt(repidofselbtn1)].isChecked())
						{
							cf.ShowToast("Image already selected");
						}
						else if((maximumindb+selectedcount)<Total_Maximumcount)
						{
							
	    						
								int	isexist = check_availability(titleheader.getText().toString());
							
								if (isexist == 0) {
									
									
										selectedtestcaption=dynamicarraysetting(titleheader.getText().toString(),selectedtestcaption );
										selectedcount++;
										chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(true);
										Remaining_count.setText("Saved image count :"+maximumindb+" Selected For save :"+selectedcount+" Remaining :"+(Total_Maximumcount-(maximumindb+selectedcount)));
							 
								} else {
									chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(false);
									cf.ShowToast("Already Exists! Please select a different Image");
								}
						}
					else
					{
						chkboxchild[Integer.parseInt(repidofselbtn1)].setChecked(false);
						cf.ShowToast("You are allowed to select only "+Total_Maximumcount+" Images per Elevation");
					
					}}
					

				}else {
					chkboxchild[chk].setChecked(false);
					cf.ShowToast("Your file size exceeds 2MB");

				}
				String value = titleheader.getText().toString();
				String[] bits = value.split("/");
				String picname = bits[bits.length - 1];
				
				titleheader.setText(value.replace(picname, ""));
				dialogfullimage.setCancelable(true);
				dialogfullimage.cancel();
			}
		});
		dialogfullimage.setCancelable(false);
		dialogfullimage.show();
	}
	
	private int check_availability(String string) {
		// TODO Auto-generated method stub
		if(array_indb!=null)
		{
		for(int i=0;i<array_indb.length;i++)
		{
			if(array_indb[i].equals(string))
			{
				return 1;
			}
		}
		
	}
		return 0;
		
	}
		
	
	
	
	
        public void show_imaege() {

	
        	
		if(Currentfile_list.length<=20)
		{
			Lin_Pre_nex.setVisibility(View.GONE);
			Rec_count_lin.setVisibility(View.VISIBLE);
			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("");
			
		}
		else
		{
			Lin_Pre_nex.setVisibility(View.VISIBLE);
			Rec_count_lin.setVisibility(View.VISIBLE);
			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("Records Displayed from 1 to 20");
		}
		
		prev.setOnClickListener(new OnClickListener() {
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int end =limit_start;
			
			limit_start-=20;
			
			if(limit_start>=0)
			{
			int start =limit_start;
			showing_rec_cnt.setText("Records Displayed from "+(start+1) +" to "+end);
			if((limit_start)<=0)
			{
				prev.setVisibility(View.GONE);
			}
			else
			{
				prev.setVisibility(View.VISIBLE);
			}
			if((limit_start+20)<=Currentfile_list.length)
			{
				next.setVisibility(View.VISIBLE);
			}
			else
			{
				next.setVisibility(View.GONE);
			}	
			if (Currentfile_list != null)
			   {
				  int j=0; 
				   
				
				 
				  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
				  if(limit_start>=0)
				  {
					  pieces3=new String[20];
				   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
					   
					 
		    		
		    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
		            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
		    	            	 
		    			 		 pieces3[j]=Currentfile_list[i].getName();
		    	            
		            		 }
		    		 
			   }
				  }  
			 }
			
			 dynamicimagesparent();					
			 dynamicimageschild();
		}
			else
			{
				limit_start+=20;
				end=limit_start+20;
			}
		}
	});
		
		next.setOnClickListener(new OnClickListener() {
		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			limit_start+=20;
			
			if(Currentfile_list.length>=limit_start)
			{
				
			int start =limit_start;
			int end=0;
			
			
			
			if((limit_start+20)<=Currentfile_list.length)
			{
				next.setVisibility(View.VISIBLE);
			
			}
			else
			{
				next.setVisibility(View.GONE);
			}
			if((limit_start)<=0)
			{
				prev.setVisibility(View.GONE);
			}
			else
			{
				prev.setVisibility(View.VISIBLE);
			}
				
			if (Currentfile_list != null)
			   {
				  int j=0; 
				   
				
				 if(Currentfile_list.length>(limit_start+20))
				 {
					 pieces3=new String[20];
					 end =limit_start+20;
				 }
				 else
				 {
					 pieces3=new String[(Currentfile_list.length-limit_start)];
					 end =limit_start+( Currentfile_list.length-limit_start);
				 }
				  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
				  if(limit_start>=0)
				  {
					 
				   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
					   
					 
		    		
		    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
		            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".gif"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
		    	            	 
		    			 		 pieces3[j]=Currentfile_list[i].getName();
		    	            
		            		 }
		    		 
			   }
				  }  
			 }
			
			showing_rec_cnt.setText("Records Displayed from "+(start+1) +" to "+end);
			
			dynamicimagesparent();
		
			 dynamicimageschild();
			}
			else
			{
				limit_start=limit_start-20;
			}
		}
	});
		
	
		
	         dynamicimagesparent();
	        
	    	 dynamicimageschild();
	    	
	    	}

	private void walkdir(File images2) {
	File[] current_fold_list =images2.listFiles(directoryFilter);
	
	if(current_fold_list!=null)
	{
		pieces1=new String[current_fold_list.length];
		for (int i = 0; i < current_fold_list.length; i++) {
			   
			   if (current_fold_list[i].isDirectory()) {
            
            
            pieces1[i]=current_fold_list[i].getName();
         }
			   else
			 	 {
			 		
			 	 }
		}
		
	}
	/** Get only the folders form the File array list Ends   **/
	/** Get only the Images  form the File array list Starts   **/
	if(pdf.equals("true"))
	{
	
		Currentfile_list=images2.listFiles(imageandPDFFilter);
		   if (Currentfile_list != null)
		   {
			  int j=0; 
			  
			  if(Currentfile_list.length<=20)
				  pieces3=new String[Currentfile_list.length];
			  else
			  {
				  pieces3=new String[20];
			  }
			  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
			 
			   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
				   
				 
	    		
	    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
	            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".GIF"))||(Currentfile_list[i].getName().endsWith(".bmp"))||(Currentfile_list[i].getName().endsWith(".pdf"))){
	    	            	 
	    			 		 pieces3[j]=Currentfile_list[i].getName();
	    	            
	            		 }
	    			 	 else
	    			 	 {
	    			 		
	    			 	 }
	    		 
		   }
			  
		 }
	}
	else
	{
	Currentfile_list=images2.listFiles(imageFilter);
	   if (Currentfile_list != null)
	   {
		  int j=0; 
		  
		  if(Currentfile_list.length<=20)
			 
		  pieces3=new String[Currentfile_list.length];
		  else
		  {
			  pieces3=new String[20];
		  }
		  /*****limit_start used for showing the next twenty photos every time we click the next button we will increase the 20 ****/  
		 
		   for (int i = limit_start; i < Currentfile_list.length && j < 20 ; i++,j++) {
			   
			 
    		
    			 	 if ((Currentfile_list[i].getName().endsWith(".jpg"))||(Currentfile_list[i].getName().endsWith(".jpeg"))||(Currentfile_list[i].getName().endsWith(".png"))||(Currentfile_list[i].getName().endsWith(".gif")||(Currentfile_list[i].getName().endsWith(".JPG"))
            				 ||(Currentfile_list[i].getName().endsWith(".JPEG"))||(Currentfile_list[i].getName().endsWith(".PNG")))||(Currentfile_list[i].getName().endsWith(".GIF"))||(Currentfile_list[i].getName().endsWith(".bmp"))){
    	            	 
    			 		 pieces3[j]=Currentfile_list[i].getName();
    	            
            		 }
    			 	 else
    			 	 {
    			 		
    			 	 }
    		 
	   }
		  
	 }
	} 
	   /** Get only the Images  form the File array list Ends   **/
	   /** Set the visisbolity to  the Prev and next button layout Starts ***/
	   if(Lin_Pre_nex !=null && Currentfile_list.length>20 )
	{
		
		Lin_Pre_nex.setVisibility(View.VISIBLE);
		Rec_count_lin.setVisibility(View.VISIBLE);
			Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("Records Displayed from 1 to 20");
			prev.setVisibility(View.GONE);
			next.setVisibility(View.VISIBLE);
		
	}
	else if(Lin_Pre_nex !=null)
	{

		Lin_Pre_nex.setVisibility(View.GONE);
		Rec_count_lin.setVisibility(View.VISIBLE);
		Total_Rec_cnt.setText("Total Records : "+Currentfile_list.length+" ");
			showing_rec_cnt.setText("");
		
	}
	
}

	/** File filter to get only the directories from the folder **/
	FileFilter directoryFilter = new FileFilter() {
		public boolean accept(File file) {
			return file.isDirectory();
		}
	};
	/** File filter to get only the directories from the folder **/
	/** File filter to get only the directories from the folder **/
	FileFilter imageFilter = new FileFilter() {
		 
		public boolean accept(File file) {
			
			 if ((file.getName().endsWith(".jpg"))||(file.getName().endsWith(".jpeg"))||(file.getName().endsWith(".png"))||(file.getName().endsWith(".gif")||(file.getName().endsWith(".JPG"))
	            				 ||(file.getName().endsWith(".JPEG"))||(file.getName().endsWith(".PNG")))||(file.getName().endsWith(".gif"))||(file.getName().endsWith(".bmp"))){
				 
				 
				 
				 return true;
			 }
			 else
			 {
				 return false;
			 }
				 
		}
	};
	FileFilter imageandPDFFilter = new FileFilter() {
		 
		public boolean accept(File file) {
			
			 if ((file.getName().endsWith(".jpg"))||(file.getName().endsWith(".jpeg"))||(file.getName().endsWith(".png"))||(file.getName().endsWith(".gif")||(file.getName().endsWith(".JPG"))
	            				 ||(file.getName().endsWith(".JPEG"))||(file.getName().endsWith(".PNG")))||(file.getName().endsWith(".gif"))||(file.getName().endsWith(".bmp"))||(file.getName().endsWith(".pdf")) ){
				 
				 
				 
				 return true;
			 }
			 else
			 {
				 return false;
			 }
				 
		}
	};
	/** File filter to get only the directories from the folder **/
	Bitmap ShrinkBitmap(String file, int width, int height) {
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			return null;
		}

	}
	protected String[] Delete_from_array(String[] selectedtestcaption2,
			String txt) {
		// TODO Auto-generated method stub
		String tmp[]=new String[selectedtestcaption2.length-1];
		int j=0;
		if(selectedtestcaption2!=null)
		{
			for(int i=0;i<selectedtestcaption2.length;i++)
			{
				if(selectedtestcaption2[i]!=null)
				{
					if(!selectedtestcaption2[i].equals(txt))
					{
						tmp[j]=selectedtestcaption2[i];
						j++;
					}
					else
					{
						
					}
				}
			}
			
		}
		return tmp;
	}
	private String[] dynamicarraysetting(String ErrorMsg,String[] pieces3) {
		// TODO Auto-generated method stub
		 try
		 {
		if(pieces3==null)
		{
			pieces3=new String[1];
			
			pieces3[0]=ErrorMsg;
		}
		else
		{
			
			String tmp[]=new String[pieces3.length+1];
			int i;
			for(i =0;i<pieces3.length;i++)
			{
				
				tmp[i]=pieces3[i];
			}
		
			tmp[tmp.length-1]=ErrorMsg;
			pieces3=null;
			pieces3=tmp.clone();
		}
		 }
		 catch(Exception e)
		 {
			 System.out.println("Exception "+e.getMessage());
			
		 }
		return pieces3;
	}
	public boolean common(String path) {
		File f = new File(path);
		if (f.length() < 2097152) {
			return true;
		} else {
			return false;
		}

	}
	public void Search_by_folder()
	{/*
		 final Dialog dialog1 = new Dialog(Select_phots.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alert);
			
			((LinearLayout) dialog1.findViewById(R.id.Search_folder)).setVisibility(View.VISIBLE);
			dialog1.setCancelable(true);
			final Button selectBtn = (Button) dialog1.findViewById(R.id.SF_Search);
			Button cancelBtn = (Button) dialog1.findViewById(R.id.SF_cancel);
			Button Close = (Button) dialog1.findViewById(R.id.SF_close);
			final AutoCompleteTextView name_txt =(AutoCompleteTextView) dialog1.findViewById(R.id.SF_Name);
			final TextView finale_txt = (TextView) dialog1.findViewById(R.id.finale_txt);
			name_txt.setFocusable(false); 
			name_txt.setText((titleheader.getText().toString().substring(12,titleheader.getText().toString().length())));
			name_txt.setOnTouchListener(new OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					setFocus(name_txt);
					name_txt.setSelection(name_txt.getText().length());
					return false;
				}
			});
			
			cancelBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					//pieces1=null;pieces2=null;pieces3=null;
					
					dialog1.setCancelable(true);
					dialog1.cancel();
				}
			});
			Close.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					//pieces1=null;pieces2=null;pieces3=null;
					
					dialog1.setCancelable(true);
					dialog1.cancel();
				}
			});
			selectBtn.setOnClickListener(new OnClickListener() {
				public void onClick(View v) { 
					selectBtn.setVisibility(View.GONE);
					String Src_Txt =name_txt.getText().toString();
					//System.out.println("The selected folder name was "+name_txt.getText().toString()+"Sub string "+Src_Txt.substring(0, 11));
					System.out.println("The txt "+(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""));
					if(Src_Txt.equals(""))
					{
					titleheader.setText("/mnt/sdcard/");
						finaltext="/mnt/sdcard/";
						File images = new File("/mnt/sdcard/");
						//Currentfile_list=f.listFiles();
						childtitleheader.setText("sdcard"); // set the chaild title
						walkdir(images);
						dynamicimagesparent();
						dynamicimageschild();
					}
					else if((Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())).equals(""))
					{
						cf.show_toast("Please enter the valid Folder Name", 1);
					}
					else 
					{	Src_Txt="/mnt/sdcard/"+Src_Txt;
						File f = new File(Src_Txt);
						if(f.exists())
						{File images;
							if((Src_Txt.substring(Src_Txt.lastIndexOf("/"), Src_Txt.length())).equals(""))
							{
								
								titleheader.setText(Src_Txt);
			 					finaltext=Src_Txt;
			 					 images = new File(Src_Txt);
			 					//Currentfile_list=f.listFiles();
			 					Src_Txt=Src_Txt.substring(0, Src_Txt.length()-1);
			 					childtitleheader.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
							}
							else
							{
								titleheader.setText(Src_Txt+"/");
			 					finaltext=Src_Txt+"/";
			 					 images = new File(Src_Txt+"/");
			 					//Currentfile_list=f.listFiles();
			 					
			 					childtitleheader.setText(Src_Txt.substring(Src_Txt.lastIndexOf("/")+1,Src_Txt.length())); // set the chaild title
							}
							
		 					walkdir(images);
		 					dynamicimagesparent();
		 					dynamicimageschild();
							
						}
						else
						{
							cf.show_toast("Sorry! No files found.", 1);
						}
						
					}
					dialog1.setCancelable(true);
					dialog1.cancel();
				}
			});
			name_txt.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					try
					{
						String s1=s.toString();
						System.out.println("The issues not in the set "+s1);
						String file_name,file_txt,Requested_name;
						 if((s1.substring(0,1)).equals("/") )
							{
								name_txt.setText("");
								cf.show_toast("Please enter the valid text", 1);
								System.out.println("The issues not in the set ");
							}
						 else if((s1.length()>1) && (s1.substring(s.length()-1,s.length())).equals("/") && (s1.substring(s.length()-2,s.length()-1)).equals("/"))
						{
							name_txt.setText(s1.substring(0,s1.length()-1));
							name_txt.setSelection(name_txt.getText().length());
							cf.show_toast("Please enter the valid text", 1);
						}
						
						else if(name_txt.getText().toString().equals(""))
						{
							file_name="/mnt/sdcard/";
							 file_txt= "/mnt/sdcard";
						}
						else
						{
							file_name="/mnt/sdcard/"+name_txt.getText().toString();
							
							 file_txt= file_name.substring(0,file_name.lastIndexOf("/"));
//							 if(file_txt.equals("/mnt/sdcard"))
//							 {
//								 file_txt+="/"; 
//							 }
							 Requested_name=file_name.substring(file_name.lastIndexOf("/")+1,file_name.length());
							 System.out.println("The file name and the texdt was file_name "+file_name+" file_txt ="+file_txt+"Requested_name ="+Requested_name);
							File f = new File(file_txt);
							if(f.exists())
							{
								System.out.println("File xeists");
								File[] file_list= f.listFiles(directoryFilter);

								String[] autousername = new String[file_list.length];
								String[] filename=null;
								System.out.println("File xeists 1"+file_list.length);
									if(file_list.length>0)
									{
										System.out.println("comes in to if");
									int k=0;
											for(int i=0;i<file_list.length;i++)
											{
												String Tmp=file_list[i].getAbsolutePath();
												
												Tmp=Tmp.substring(Tmp.lastIndexOf("/")+1,Tmp.length());
												
												
												System.out.println(" Tmp = "+Tmp+" Requested_name = "+Requested_name+" (Tmp.substring(0,Requested_name.length())) = "+(Tmp.substring(0,Requested_name.length())));
												if((Tmp.substring(0,Requested_name.length())).toLowerCase().equals(Requested_name.toLowerCase()))
												{
													
													String temp= name_txt.getText().toString();
													if(temp.contains("/"))
													{
														temp= temp.substring(0,name_txt.getText().toString().lastIndexOf("/"));
														autousername[k]=temp+"/"+Tmp;
													}
													else
													{
														autousername[k] = Tmp;	
													}
													
													System.out.println("text for the file was autousername[k]= "+autousername[k]);
													k++;
													
												}
												
												
												
												
											}
											filename=new String[k];
											
											for(int m=0;m<k;m++)
											{
												
												filename[m]=autousername[m];
												System.out.println("text for the file was filename[i]= "+filename[m]);
												
											}
											
											
									}
									 ArrayAdapter<String> adapter = new ArrayAdapter<String>(Select_phots.this,R.layout.loginnamelist,filename);
									 name_txt.setThreshold(1);
									 name_txt.setAdapter(adapter);
						}
					
					
					
							 
					}
					
					}catch (Exception e) {
						// TODO: handle exception
						System.out.println("issues happen in cathce "+e.getMessage());
					}
				}
			});
			dialog1.setCancelable(false);
			dialog1.show();
			

	*/}
	public void setFocus(EditText etcomments_obs1_opt12) {
		// TODO Auto-generated method stub
		etcomments_obs1_opt12.setFocusableInTouchMode(true);
		etcomments_obs1_opt12.requestFocus();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {	
			
			if(titleheader.getText().toString().equals(Environment.getExternalStorageDirectory()+"/"))
			{
			final AlertDialog.Builder ab =new AlertDialog.Builder(Select_phots.this);
			ab.setTitle("Confirmation");
			ab.setMessage(Html.fromHtml("Do you want to close the file selection"));
			ab.setIcon(R.drawable.alertmsg);
			ab.setCancelable(false);
			ab.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Intent n= new Intent();
					n.putExtra("Result", -1);
					setResult(RESULT_CANCELED,n);
					try {
						//cf.show_toast("Not allowed to access ", 1);
						finish();
					} catch (Throwable e) {
						// TODO Auto-generated catch block
						System.out.println("issues in the final"+e.getMessage());
						e.printStackTrace();
					}
				}

				
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

					
					
				}
			});
			AlertDialog al=ab.create();
			al.show();
			}
			else
			{
				backclick=1;
 				pieces1=null;pieces3=null;limit_start=0;
 				
 	 				String value = titleheader.getText().toString();
 					String[] bits = value.split("/");	 		 					
 					String picname = bits[bits.length - 1];
 					if(value.equals(Environment.getExternalStorageDirectory()+"/"))
 					{
 						finaltext="";
 						
 						pathofimage  = value.replace(picname+"/", "");
 					
 					}
 					else
 					{
 						String tempstr = value.substring(0,value.length()-1);
	 					int bits1 = tempstr.lastIndexOf("/");
	 					String pathofimage1=tempstr.substring(0,bits1+1);
 						pathofimage  = pathofimage1;
 					//	pathofimage = value.replace(picname+"/", "");
 					}
 					
			titleheader.setText(pathofimage);
			finaltext=pathofimage;
			File f = new File(pathofimage);
			
			
			String[] bits1 = pathofimage.split("/");
		String picname1 = bits1[bits1.length - 1];
		childtitleheader.setText(picname1); // set the chaild title
		
			walkdir(f);
			dynamicimagesparent();
			dynamicimageschild();
					
				return true;
			}
		
		}
		
		return super.onKeyDown(keyCode, event);
	}	
}
