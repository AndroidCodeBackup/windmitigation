package idsoft.inspectiondepot.windmitinspection;


import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import java.util.Locale;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class HomwOwnerListing extends Activity {
	ScrollView sv;Button deleteallinspections;
	String alerttitle="";
	LinearLayout inspectionlist;
	String countarr[],data[];
	int i = 1, cnt, rws = 0,show_handler;
	Cursor cur;
	String statusofdata, InspectionType, status = "", keyword, statuschk,
			query,  strtit,dta = " ",s = "";
	private Button homepage, search, search_clear_txt;
	EditText search_text;
	TextView welcome, title,t;
	RelativeLayout rlheader;
	Button deletebtn[];
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	public int[] txtid = {R.id.txta,R.id.txtb,R.id.txtc,R.id.txtd,R.id.txte,R.id.txtf,R.id.txtg,R.id.txth,R.id.txti,R.id.txtj,
			R.id.txtk,R.id.txtl,R.id.txtm,R.id.txtn,R.id.txto,R.id.txtp,R.id.txtq,R.id.txtr,R.id.txts,R.id.txtt,R.id.txtu,
			R.id.txtv,R.id.txtw,R.id.txtx,R.id.txty,R.id.txtz,R.id.txtall};
	public TextView[] txt = new TextView[27];
	View v;
	
	   public void onCreate(Bundle savedInstanceState) 
	   {
		   super.onCreate(savedInstanceState);
		   cf=new CommonFunctions(this);			
		   db=new DatabaseFunctions(this);
		   wb=new WebserviceFunctions(this);
		   Bundle extras = getIntent().getExtras();
		   if (extras != null) {
			   status = extras.getString("status");	
		   }
		setContentView(R.layout.inspectionlist);
		
		db.getInspectorId();
		declaration();
		dbquery();
		
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside");
				Intent insp_info = new Intent(HomwOwnerListing.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);System.out.println("ends");
				
			}
		});
	
	}

	private void declaration() {
		// TODO Auto-generated method stub
		
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		search_text = ((EditText)findViewById(R.id.search_text));
		rlheader = (RelativeLayout) findViewById(R.id.emailreport_rlheader);
		
		t = (TextView) this.findViewById(R.id.textView1);
		title = (TextView) this.findViewById(R.id.rws);
		
		if(status.equals("Scheduled"))
		{
			statusofdata = "40";
		}
		if(status.equals("Inspection Completed"))
		{
			statusofdata = "1";
		}
		if(status.equals("Cancelled"))
		{
			statusofdata = "90";
		}
		
		if(status.equals("Scheduled") || status.equals("Cancelled"))
		{
			t.setText(status +" Inspection");
		}
		else
		{
			t.setText(status);
		}
		
		for(int i=0;i<txt.length;i++)
		{
			txt[i] = (TextView)findViewById(txtid[i]);
			txt[i].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					search_text.setText("");
					switch(v.getId())
					{
					   case R.id.txta: 
						   txtsearch("A");changetextbgcolor();
						   ((TextView)findViewById(R.id.txta)).setBackgroundColor(Color.parseColor("#519BC2")); 
						break;
					   case R.id.txtb: 
						   txtsearch("B");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtb)).setBackgroundColor(Color.parseColor("#519BC2")); 
						break;
					   case R.id.txtc: 
						   txtsearch("C");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtc)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtd: 
						   txtsearch("D");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtd)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txte: 
						   txtsearch("E");changetextbgcolor();
						   ((TextView)findViewById(R.id.txte)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtf: 
						   txtsearch("F");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtf)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtg: 
						   txtsearch("G");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtg)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txth: 
						   txtsearch("H");changetextbgcolor();
						   ((TextView)findViewById(R.id.txth)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txti: 
						   txtsearch("I");changetextbgcolor();
						   ((TextView)findViewById(R.id.txti)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtj: 
						   txtsearch("J");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtj)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtk: 
						   txtsearch("K");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtk)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtl: 
						   txtsearch("L");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtl)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtm: 
						   txtsearch("M");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtm)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtn: 
						   txtsearch("N");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtn)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txto: 
						   txtsearch("O");changetextbgcolor();
						   ((TextView)findViewById(R.id.txto)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtp: 
						   txtsearch("P");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtp)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtq: 
						   txtsearch("Q");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtq)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtr: 
						   txtsearch("R");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtr)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txts: 
						   txtsearch("S");changetextbgcolor();
						   ((TextView)findViewById(R.id.txts)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtt: 
						   txtsearch("T");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtt)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtu: 
						   txtsearch("U");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtu)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtv: 
						   txtsearch("V");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtv)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtw: 
						   txtsearch("W");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtw)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtx: 
						   txtsearch("X");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtx)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txty: 
						   txtsearch("Y");changetextbgcolor();
						   ((TextView)findViewById(R.id.txty)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtz: 
						   txtsearch("Z");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtz)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtall: 
						   txtsearch("");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtall)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					}
					
				}
			});
		}
		
		
	}
	private void changetextbgcolor()
	{
		for(int i=0;i<txt.length;i++)
		{
			txt[i].setBackgroundColor(Color.parseColor("#2682B3"));
		}
	}

	protected void txtsearch(String string) {
		// TODO Auto-generated method stub
		s = string;
		dbquery();
	}

	private void dbquery() {
		data = null;
		dta = "";
		countarr = null;
		rws = 0;
		query = "select * from " + db.policyholder+ " where PH_Status='" + statusofdata + "' and PH_InspectorId = '"+ db.Insp_id + "'";
		System.out.println("s="+s);
		
		if (!s.trim().equals("")) {
			query += " and (PH_FirstName like '" + s
					+ "%' or PH_LastName like '" + s
					+ "%' or PH_Policyno like '" + s + "%' ) ";
			if (status.equals("Inspection Completed")) {
				query += " and PH_Status=1 and  PH_SubStatus=0";
			}
		} 		
		else if (status.equals("Inspection Completed")) {
			query += " and  PH_Status=1";
		}
		else if (status.equals("Cancelled")) {
				query += " and  PH_Status=90 and  PH_SubStatus=0";
		}
		else if(s.trim().equals(""))
		{
			query += " and  PH_Status=40 and  PH_SubStatus=0";
		}
		query += " order by InspectionDate desc";System.out.println("Query="+query);
		cur = db.wind_db.rawQuery(query, null);
		rws = cur.getCount();System.out.println("rws="+rws);
		title.setText("TOTAL # OF RECORDS : "+rws);
		data = new String[rws];
		countarr = new String[rws];
		int j = 0;
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			rlheader.setVisibility(View.VISIBLE);
			do {
				String dbinspid = cur.getString(cur.getColumnIndex("PH_InspectorId"));
				String scheduleddate = cur.getString(cur.getColumnIndex("InspectionDate"));
				if (db.Insp_id.equals(dbinspid)) 
				{
					System.out.println("both equals");
					if (cur.getString(cur.getColumnIndex("PH_Status")).equals(statusofdata)) 
					{
						
						dta += " "+db.decode(cur.getString(cur.getColumnIndex("PH_FirstName")))+ "  ";
						dta += db.decode(cur.getString(cur.getColumnIndex("PH_LastName"))) + " | ";
						dta += db.decode(cur.getString(cur.getColumnIndex("PH_Address1"))) + " | ";
						dta += db.decode(cur.getString(cur.getColumnIndex("PH_City"))) + " | ";
						dta += db.decode(cur.getString(cur.getColumnIndex("PH_State"))) + " | ";
						dta += db.decode(cur.getString(cur.getColumnIndex("PH_County"))) + " | ";
						dta += cur.getString(cur.getColumnIndex("PH_Zip"))+ " | ";
						if(db.decode(cur.getString(cur.getColumnIndex("PH_Policyno"))).equals(""))
						{
							dta += "N/A"+" | ";
						}
						else
						{
						 dta += db.decode(cur.getString(cur.getColumnIndex("PH_Policyno")))+ " | ";
						}
						dta += db.decode(cur.getString(cur.getColumnIndex("InspectionDate"))) + " | ";
						dta += db.decode(cur.getString(cur.getColumnIndex("InspectionTime"))) + "&#126;";
						System.out.println("dta="+dta);
						countarr[j] = cur.getString(cur.getColumnIndex("PH_SRID"));
						j++;

					}
				}
			} while (cur.moveToNext());
			search_text.setText("");
			display();
		} else {
			
			inspectionlist.removeAllViews();
			rlheader.setVisibility(View.GONE);
			if(s.equals(""))
			{
				//cf.gohome();
			}
			else
			{
				cf.ShowToast("Sorry, No results found");
				cf.hidekeyboard(search_text);
			}
		}
	}
   
	private void display() {
		cf.hidekeyboard(((EditText)findViewById(R.id.search_text)));
		inspectionlist.removeAllViews();
		sv = new ScrollView(this);
		inspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		LinearLayout[] rl = new LinearLayout[rws];
		l1.removeAllViews();
		TextView[] tvstatus = new TextView[rws];
		Button[] cancel = new Button[rws];
		Button[] activate = new Button[rws];
		if (!dta.equals(null) && !dta.equals("null") && !dta.equals("")) {
			this.data = dta.split("&#126;");
			for (int i = 0; i < data.length; i++) {
				
				LinearLayout.LayoutParams llparams;
				LinearLayout.LayoutParams lltxtparams;

				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				llparams.setMargins(0, 2, 0, 0);

				rl[i] = new LinearLayout(this);
				l1.addView(rl[i], llparams);

				lltxtparams = new LinearLayout.LayoutParams(
						750,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(10, 20, 10, 20);
				
				
				tvstatus[i] = new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setTag("textbtn" + i);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextSize(14);
				rl[i].addView(tvstatus[i]);

			   LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
					   ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				downloadparams1.setMargins(0, 0, 20, 0);
				downloadparams1.gravity = Gravity.CENTER_VERTICAL;

				LinearLayout.LayoutParams sendmailparams = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				sendmailparams.setMargins(110, 0, 20, 0);
				sendmailparams.gravity = Gravity.CENTER_VERTICAL;

				//System.out.println("sta="+cf.status + cf.Homeid);
				if(!status.equals("Cancelled"))
				{
					System.out.println("inside");
					cancel[i] = new Button(this, null);
					cancel[i].setLayoutParams(downloadparams1);
					cancel[i].setId(2);
					cancel[i].setText("Cancel Inspection");
					cancel[i].setBackgroundResource(R.drawable.buttonstyle);
					cancel[i].setTextSize(14);
					cancel[i].setTextColor(Color.WHITE);
				//	cancel[i].setTypeface(null, Typeface.BOLD);
					cancel[i].setTag("cnclbtn" + i);
					rl[i].addView(cancel[i]);
					
					cancel[i].setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String getcnclbtn = v.getTag().toString();
							getcnclbtn = getcnclbtn.replace("cnclbtn", "");
							final int s = Integer.parseInt(getcnclbtn);
							String gt = countarr[s];
							final String srid = gt.toString();
							
							System.out.println("idcan= "+srid);
							final Dialog add_dialog = new Dialog(HomwOwnerListing.this,android.R.style.Theme_Translucent_NoTitleBar);
							add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							add_dialog.setCancelable(false);
							add_dialog.getWindow().setContentView(R.layout.cancelalert);
							 
							((RelativeLayout)add_dialog.findViewById(R.id.cancel)).setVisibility(v.VISIBLE);
					        ((Button)add_dialog.findViewById(R.id.yes)).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									add_dialog.dismiss();
									
									updatestatus(srid,90);
									
								}
							});
	                       ((Button)add_dialog.findViewById(R.id.no)).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									add_dialog.dismiss();
									
								}
							});
							
							add_dialog.setCancelable(false);
							add_dialog.show();
						}
					});
				}
				
				if(status.equals("Cancelled"))
				{
					System.out.println("inside" + cf.Homeid);
					activate[i] = new Button(this, null);
					activate[i].setLayoutParams(downloadparams1);
					activate[i].setId(2);
					activate[i].setText("Activate");
					activate[i].setBackgroundResource(R.drawable.buttonstyle);
					activate[i].setTextSize(14);
					activate[i].setTextColor(Color.WHITE);
				//	cancel[i].setTypeface(null, Typeface.BOLD);
					activate[i].setTag("actbtn" + i);
					rl[i].addView(activate[i]);
					
					activate[i].setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String getactbtn = v.getTag().toString();
							getactbtn = getactbtn.replace("actbtn", "");
							final int s = Integer.parseInt(getactbtn);
							String gt = countarr[s];
							final String srid = gt.toString();
							
							System.out.println("idact= "+srid);
							final Dialog add_dialog = new Dialog(HomwOwnerListing.this,android.R.style.Theme_Translucent_NoTitleBar);
							add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
							add_dialog.setCancelable(false);
							add_dialog.getWindow().setContentView(R.layout.cancelalert);
							 
							((TextView)add_dialog.findViewById(R.id.cancelid)).setText("Do you want to activate the inspection selected?");
					        ((Button)add_dialog.findViewById(R.id.yes)).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									add_dialog.dismiss();
									updatestatus(srid,40);
									
								}
							});
	                       ((Button)add_dialog.findViewById(R.id.no)).setOnClickListener(new OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									add_dialog.dismiss();
									
								}
							});
							
							add_dialog.setCancelable(false);
							add_dialog.show();
						}
					});
				}
				
				
				tvstatus[i].setOnClickListener(new View.OnClickListener() {

					public void onClick(final View v) {
						String getidofselbtn = v.getTag().toString();
						final String repidofselbtn = getidofselbtn.replace("textbtn", "");
						final int s = Integer.parseInt(repidofselbtn);
						String select = countarr[s];
						String keyword = select.toString();
						
						if(!status.equals("Cancelled"))
						{
							Intent intimg = new Intent(HomwOwnerListing.this,PolicyholderInfo.class);
							intimg.putExtra("homeid", keyword);
							intimg.putExtra("status", status);
							startActivity(intimg);
							finish();
						}
							
					}
				});

				 if (i % 2 == 0) {
					 rl[i].setBackgroundColor(Color.parseColor("#1871A0"));
								/*"#6E6E6E"));*/
					} else {
						rl[i].setBackgroundColor(Color.parseColor("#106896"));
								/*"#2E2E2E"));*/
					}
			}
		}

	}
	public void updatestatus(final String srid, final int j)
	{
		if (wb.isInternetOn() == true) {
			if(j==90)
			{
				cf.show_ProgressDialog("Cancelling Inspection..");
			}else if(j==40)
			{
				cf.show_ProgressDialog("Activating Inspection..");
			}
			
			new Thread() {
				public void run() {
					Looper.prepare();
					try
					{
						SoapObject request = new SoapObject(wb.NAMESPACE,"UPDATESTATUS");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorID",db.Insp_id);
						request.addProperty("SRID",srid);
						request.addProperty("Statusid",j);
						envelope.setOutputSoapObject(request);
						System.out.println("UPDATESTATUS request is " + request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);						
						androidHttpTransport.call(wb.NAMESPACE+"UPDATESTATUS", envelope);
						//String result = String.valueOf((SoapObject) envelope.getResponse());
						Object result= (Object)envelope.getResponse();
						
						System.out.println("UPDATESTATUS result is"+ result);
						
					//	String status = String.valueOf(result.getProperty("UPDATESTATUSResult"));
						System.out.println("status="+result);
						if(result.toString().equals("true"))
						{
							show_handler=1;
							handler.sendEmptyMessage(0);
						}
						else
						{
							show_handler=2;
							handler.sendEmptyMessage(0);
						}
						
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}
				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 2) {
							show_handler = 0;
							if(j==90){
							    cf.ShowToast("Inspection not cancelled");
							}
							else if(j==40)
							{
								cf.ShowToast("Inspection not activated");
							}
						}else if (show_handler == 1) {
							show_handler = 0;
							if(j==90)
							{
								try
								{  
									
									db.wind_db.execSQL("UPDATE " + db.policyholder
											+ " SET PH_Status='90' WHERE PH_SRID ='" + srid+ "'");
									cf.ShowToast("Inspection cancelled successfully");
								  dbquery();

								}catch (Exception e) {
									// TODO: handle exception
								}
							}
							else if(j==40)
							{
								try
								{
									db.wind_db.execSQL("UPDATE " + db.policyholder
											+ " SET PH_Status='40' WHERE PH_SRID ='" + srid+ "'");
									cf.ShowToast("Inspection activated sucessfully");
									
									dbquery();

								}catch (Exception e) {
									// TODO: handle exception
								}
							}
						}
					}
				};
			}.start();
		}
		else
		{
			cf.ShowToast("Internet connection not available");
		}
	}

	public void clicker(View v) {
		switch (v.getId()) 
		{
		case R.id.starthome:
			s = "";
			cf.gohome();
			break;
		case R.id.search:
			changetextbgcolor();
			String s1 = db.decode(search_text.getText().toString());
			if (s1.equals("")) 
			{
				cf.ShowToast("Please enter the Name or Policy Number to search");
				search_text.requestFocus();
			} 
			else 
			{
				s = s1;
				try 
				{
					dbquery();
				} 
				catch (Exception e) 
				{
					
				}
			}
			break;
		case R.id.search_clear_txt:
			changetextbgcolor();((TextView)findViewById(R.id.txtall)).setBackgroundColor(Color.parseColor("#519BC2"));
			    cf.hidekeyboard(((EditText)findViewById(R.id.search_text)));
				search_text.setText("");
				s = "";
				dbquery();
			break;		
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			s = "";
			Intent Star = new Intent(HomwOwnerListing.this,Dashboard.class);
			Star.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(Star);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}