package idsoft.inspectiondepot.windmitinspection;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Signature extends Activity
{
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	private static final int SELECT_PICTURE = 0;
	int s=0, t,u,select_count=0,nBitmapWidth = 400, nBitmapHeight = 170;
	private Bitmap mBitmap;
	private Canvas mCanvas;
	private Path mPath;
	Paint mPaint = new Paint();
	Uri mCapturedImageURI;
	private Paint mBitmapPaint;
	byte[] byteArray;
	ImageView hmesignimg,papersignimg;
	int value, Count;
	Dialog dialog;
	private static final int CAPTURE_PICTURE_INTENT = 0;
	UploadRestriction upr = new UploadRestriction();
	String homeid,homesigndesc="",InspectionType, status, paperworkdesc="",picname1="",filepath = "empty",picname="",selectedImagePath = "empty",selectedImagePathforpaper = "empty",capturedImageFilePath,extStorageDirectory;
	public void onCreate(Bundle SavedInstanceState) 
	{
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		db= new DatabaseFunctions(this);
		wb= new WebserviceFunctions(this);
		
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}
		setContentView(R.layout.homeownersignature); 		
		cf.getDeviceDimensions();				        
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(Signature.this, 3, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(Signature.this, 17,cf, 1));
		
		hmesignimg = (ImageView) findViewById(R.id.homedispmg);
		papersignimg = (ImageView) findViewById(R.id.paperimgdisp);
		setValue();
		
	}
	private void setValue() {
		// TODO Auto-generated method stub
		try
		{
			Cursor c1 = db.wind_db.rawQuery("SELECT * FROM " + db.policyholder
					+ " WHERE PH_SRID='" + cf.Homeid+"'", null);
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				selectedImagePath = db.decode(c1.getString(c1.getColumnIndex("fld_homeownersign")));
				homesigndesc = db.decode(c1.getString(c1.getColumnIndex("fld_homewonercaption")));
				selectedImagePathforpaper = db.decode(c1.getString(c1.getColumnIndex("fld_paperworksign")));
				paperworkdesc = db.decode(c1.getString(c1.getColumnIndex("fld_paperworkcaption")));
				
				if(!selectedImagePath.equals("empty") || !selectedImagePath.equals(""))
				{
					homephotodisplay();
				}
				if(!selectedImagePathforpaper.equals("empty") || !selectedImagePathforpaper.equals(""))
				{
					paperworkimgdisplay();
				}
				
				if(homesigndesc.equals(""))
				{
					((EditText)findViewById(R.id.edthome)).setText("Homeowner Signature");
				}
				else
				{
					((EditText)findViewById(R.id.edthome)).setText(homesigndesc);
				}
				
				if(paperworkdesc.equals(""))
				{
					((EditText)findViewById(R.id.edtpaper)).setText("Photograph of Field Paperwork Signature");
				}
				else
				{
					((EditText)findViewById(R.id.edtpaper)).setText(paperworkdesc);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println("home owner sign ="+e.getMessage());
		}
	}
	private void homephotodisplay() {
		// TODO Auto-generated method stub
			try {
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				String k = "0";
				try {
				BitmapFactory.decodeStream(new FileInputStream(selectedImagePath),
						null, o);
				k = "1";
				}
				catch (FileNotFoundException e) {
				k = "2";
				}
				if (k.equals("1")) {
					final int REQUIRED_SIZE = 200;
					int width_tmp = o.outWidth, height_tmp = o.outHeight;
					int scale = 1;
					while (true) {
						if (width_tmp / 2 < REQUIRED_SIZE
								|| height_tmp / 2 < REQUIRED_SIZE)
							break;
						width_tmp /= 2;
						height_tmp /= 2;
						scale *= 2;
					}
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inSampleSize = scale;
					System.out.println("sss good"+selectedImagePath);
					if(!"".equals(selectedImagePath))
					{
						Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
								selectedImagePath), null, o2);
						if(bitmap==null)
						{
							
							selectedImagePath="";
							cf.ShowToast("This image is not a supported format. You cannot upload");
							hmesignimg.setImageResource(R.drawable.noimage);
						}
						else
						{
						BitmapDrawable bmd = new BitmapDrawable(bitmap);
						hmesignimg.setImageDrawable(bmd);
						
						}
					}
				}
				else
				{
					Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
					hmesignimg.setMinimumWidth(200);
					hmesignimg.setMaxWidth(200);
					hmesignimg.setMinimumHeight(200);
					hmesignimg.setMaxHeight(200);
					hmesignimg.setImageBitmap(bmp);	
				}		
	
			} catch (FileNotFoundException e) {
				
			}
	}


	private void paperworkimgdisplay() {
		// TODO Auto-generated method stub
		try {
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			String j = "0";
			try {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o);
				j = "1";
			} catch (FileNotFoundException e) {
				j = "2";
			}
			if (j.equals("1")) {
			
			final int REQUIRED_SIZE = 200;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			if(!"".equals(selectedImagePathforpaper))
			{
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o2);
				if(bitmap==null)
				{
					
					selectedImagePathforpaper="";
					cf.ShowToast("This image is not a supported format. You cannot upload");
					papersignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				papersignimg.setImageDrawable(bmd);
				
				}
			}
			}
			else
			{
				Bitmap bmp= BitmapFactory.decodeResource(getResources(),R.drawable.photonotavail);
				papersignimg.setMinimumWidth(200);
				papersignimg.setMaxWidth(200);
				papersignimg.setMinimumHeight(200);
				papersignimg.setMaxHeight(200);
				papersignimg.setImageBitmap(bmp);
			}

		} catch (FileNotFoundException e)
		{
		}
	}
	public void clicker(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.save:
			homesigndesc = ((EditText)findViewById(R.id.edthome)).getText().toString();
			paperworkdesc = ((EditText)findViewById(R.id.edtpaper)).getText().toString();

			if (selectedImagePath.equals("empty") && selectedImagePathforpaper.equals("empty")) 
			{
				
			} 
			else 
			{
				try
				{
					db.wind_db.execSQL("UPDATE " + db.policyholder
							+ " SET fld_homeownersign='"+ db.encode(selectedImagePath)
							+ "',fld_homewonercaption='"+ db.encode(homesigndesc)
							+ "',fld_paperworksign='" + db.encode(selectedImagePathforpaper)
							+ "',fld_paperworkcaption='" + db.encode(paperworkdesc) + "' WHERE PH_SRID ='"+ cf.Homeid+ "'");
					cf.ShowToast("Signature saved successfully");
					Intent intimg = new Intent(Signature.this, Photos.class);
					intimg.putExtra("homeid", cf.Homeid);
					intimg.putExtra("status", cf.status);
					startActivity(intimg);
					//cf.goclass(7);
				}
				catch(Exception e)
				{
					System.out.println("e photo ="+e.getMessage());
				}	
			}
			break;
		case R.id.homegal:
			t = 0;
			u = 1;				
			pickfromgallery();
		break;
		case R.id.papergal:
			t = 0;
			u = 4;
			pickfromgallery();
		break;
		case R.id.papercamera:
			t = 1;
			u = 3;
			selectedImagePathforpaper = capturedImageFilePath;
			startCameraActivity();
		break;
		case R.id.homesign:
			t = 0;
			u = 1;
			final Dialog dialog = new Dialog(Signature.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog.getWindow().setContentView(R.layout.signature);
			
			Button btn_save = (Button) dialog.findViewById(R.id.save);
			Button btn_cancel = (Button) dialog.findViewById(R.id.cancel);
			ImageView btn_close = (ImageView) dialog.findViewById(R.id.helpclose);
			final GraphicsView gv = (GraphicsView) dialog.findViewById(R.id.sigview);
			gv.setDrawingCacheEnabled(true);
			gv.buildDrawingCache();
			
			
			btn_close.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
			btn_save.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
				
					System.out.println("GraphicsView.s "+GraphicsView.s);
					if (GraphicsView.s == 1) 
					{
						gv.setBackgroundColor(Color.TRANSPARENT);
						Bitmap bm = gv.getDrawingCache();								
						if(bm==null)
						{
							cf.ShowToast("Please add Signature");
						}
						else
						{
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
						byteArray = stream.toByteArray();
						
						OutputStream outStream;
						
						Date dat = new Date();
						dat.getDate();
						dat.getYear();
						dat.getMonth();
						dat.getHours();
						dat.getMinutes();
						dat.getSeconds();
						String _fileName = dat.getMonth() + "-" + dat.getDate()
								+ "-" + dat.getYear() + " " + dat.getHours()
								+ "-" + dat.getMinutes() + "-"
								+ dat.getSeconds();
						filepath = "/sdcard/DCIM/Camera/"
								+ _fileName.toString() + ".jpg";
						
						File file = new File(extStorageDirectory, String
								.valueOf(filepath));
						
						System.out.println("_file="+file);
						
						try {
							outStream = new FileOutputStream(file);
							outStream.write(byteArray);
							outStream.flush();
							outStream.close();
							selectedImagePath = filepath.toString();
							String[] bits = selectedImagePath.split("/");
							picname = bits[bits.length - 1];
							dialog.dismiss();
							displayphoto();
						} catch (FileNotFoundException e) {
							System.out.println("FileNotFoundException"+e.getMessage());
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							System.out.println("IOException"+e.getMessage());
							e.printStackTrace();
						} catch (Exception e) {
							System.out.println("ee"+e.getMessage());
							e.printStackTrace();
						}
						}
					} else {
						cf.ShowToast("Please enter Signature");

					}
				}					
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					GraphicsView.s=0;
					gv.clear();
					
				}					
			});
			dialog.setCancelable(false);
			dialog.show();		

		break;

	case R.id.home:
		cf.gohome();
		break;
		}
	}
	protected void pickfromgallery() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);
	}
	

	protected void startCameraActivity() {
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		mCapturedImageURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
		startActivityForResult(intent, CAPTURE_PICTURE_INTENT);

	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (t == 0) {
			if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();

					boolean bu = upr.common(getPath(selectedImageUri));

					if (bu) {

						if (u == 1) {
							selectedImagePath = getPath(selectedImageUri);
							String[] bits = selectedImagePath.split("/");
							picname = bits[bits.length - 1];
						} else {
							selectedImagePathforpaper = getPath(selectedImageUri);
							String[] bits = selectedImagePathforpaper
									.split("/");
							picname1 = bits[bits.length - 1];
						}

						displayphoto();
					} else {
						cf.ShowToast("Your file size exceeds 2MB");

					}

				}
			}
		} else if (t == 1) { // Camera activity 
			switch (resultCode) {
			case 0:
				break;
			case -1:
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(mCapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				capturedImageFilePath = cursor.getString(column_index_data);
				selectedImagePathforpaper = capturedImageFilePath;
				String[] bits = selectedImagePathforpaper.split("/");
				picname1 = bits[bits.length - 1];
				u = 4;
				displayphoto();

				break;

			}
		}

	}
	private void displayphoto() {
		// TODO Auto-generated method stub
		try {System.out.println("inside displ=");
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			if (u == 1) {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o);
			} else if (u == 4) {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o);
			} else if (u == 5) {
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o);
				BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePathforpaper), null, o);
			}
			final int REQUIRED_SIZE = 200;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			System.out.println("inside du"+u);
			if (u == 1) {
				System.out.println("selectedImagePath "+selectedImagePath);
				if(!"".equals(selectedImagePath))
				{
					Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(selectedImagePath), null, o2);				
					if(bitmap==null)
					{
						selectedImagePath="";
						cf.ShowToast("This image is not a supported format. You cannot upolad");
						hmesignimg.setImageResource(R.drawable.noimage);
					}
					else
					{
						BitmapDrawable bmd = new BitmapDrawable(bitmap);
						hmesignimg.setImageDrawable(bmd);
					}
				}
			} 
			else if (u == 4) {
				System.out.println("selectedImagePathforpaper"+selectedImagePathforpaper);
				if(!"".equals(selectedImagePathforpaper))
				{
				Bitmap bitmap2 = BitmapFactory.decodeStream(new FileInputStream(selectedImagePathforpaper), null,o2);
				System.out.println("bitmap2"+bitmap2);
				if(bitmap2==null)
				{
					selectedImagePathforpaper="";
					cf.ShowToast("This image is not a supported format. You cannot upload");
					papersignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
					papersignimg.setImageDrawable(bmd2);
				}
				}
			} else if (u == 5) {
				if(!"".equals(selectedImagePath))
				{
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						selectedImagePath), null, o2);
				
				if(bitmap==null)
				{
					selectedImagePath="";
					cf.ShowToast("This image is not a supported format. You cannot upload");
					hmesignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd = new BitmapDrawable(bitmap);
					hmesignimg.setImageDrawable(bmd);
				}
				}            
				if(!"".equals(selectedImagePathforpaper))
				{
				Bitmap bitmap2 = BitmapFactory.decodeStream(
						new FileInputStream(selectedImagePathforpaper), null,
						o2);
				
				if(bitmap2==null)
				{
					selectedImagePathforpaper="";
					cf.ShowToast("This image is not a supported format. You cannot upload");
					papersignimg.setImageResource(R.drawable.noimage);
				}
				else
				{
					BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
					papersignimg.setImageDrawable(bmd2);
				}
				}
			}

		} catch (FileNotFoundException e) {
		}

	}

	public class DrawView extends View {

		public DrawView(Context c) {
			super(c);

			mPaint.setAntiAlias(true);
			mPaint.setDither(true);
			mPaint.setColor(0xFF000000);
			mPaint.setStyle(Paint.Style.STROKE);
			mPaint.setStrokeJoin(Paint.Join.ROUND);
			mPaint.setStrokeCap(Paint.Cap.ROUND);
			mPaint.setStrokeWidth(4);

			mBitmap = Bitmap.createBitmap(nBitmapWidth, nBitmapHeight,
					Bitmap.Config.ARGB_8888);
			mBitmap.eraseColor(0xFFFFFFFF);
			mCanvas = new Canvas(mBitmap);

			mPath = new Path();
			mBitmapPaint = new Paint(Paint.DITHER_FLAG);

		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh) {
			super.onSizeChanged(w, h, oldw, oldh);
		}

		@Override
		protected void onDraw(Canvas canvas) {

			canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
			canvas.drawPath(mPath, mPaint);

		}

		private float mX, mY;
		private static final float TOUCH_TOLERANCE = 4;

		private void touch_start(float x, float y) {
			mPath.reset();
			mPath.moveTo(x, y);
			mX = x;
			mY = y;
		}

		private void touch_move(float x, float y) {
			float dx = Math.abs(x - mX);
			float dy = Math.abs(y - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
				mX = x;
				mY = y;
			}
		}

		private void touch_up() {
			s = 1;
			mPath.lineTo(mX, mY);
			// commit the path to our offscreen
			mCanvas.drawPath(mPath, mPaint);
			// kill this so we don't double draw
			mPath.reset();
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float x = event.getX();
			float y = event.getY();

			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touch_start(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_MOVE:
				touch_move(x, y);
				invalidate();
				break;
			case MotionEvent.ACTION_UP:
				touch_up();
				invalidate();
				break;
			}
			return true;
		}

		public void ClearPath() {
			mPath.reset();
			invalidate();
		}
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(Signature.this, QuesBuildCode.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}