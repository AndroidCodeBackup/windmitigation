package idsoft.inspectiondepot.windmitinspection.imagezoom;

public interface IDisposable {

	void dispose();
}
