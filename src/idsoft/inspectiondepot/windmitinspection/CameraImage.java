package idsoft.inspectiondepot.windmitinspection;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

public class CameraImage extends Activity {

	private int currnet_rotated;
	EditText ed;
	Button rl,rr;
	ImageView ph_im,close_im;
	Spinner elev,caption_sp;
	Bundle b;
	String path="",caption,saved_val[];
	private Bitmap rotated_b;
	CommonFunctions cf;
	String elev_name="",elevname="";
	DatabaseFunctions db;
	int image_order=1,spinelevval;
	private Uri CapturedImageURI;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.showtaken_img);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		b=getIntent().getExtras();
		if(b!=null)
		{
			cf.selectedhomeid=b.getString("homeid");
			
			
		}
		declaration();
		db.CreateTable(13);
		db.CreateTable(14);
	}
	
	private void declaration() {
		// TODO Auto-generated method stub
		elev=(Spinner) findViewById(R.id.ph_elev);
		ArrayAdapter ad =new ArrayAdapter(this, android.R.layout.simple_spinner_item,cf.elevations);
		ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		elev.setAdapter(ad);
		elev.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(elev.getSelectedItemPosition()!=0)
				{
					show_caption();
				}
				else
				{
					caption_sp.setEnabled(false);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		rl=(Button) findViewById(R.id.rotateleft);
		rr=(Button) findViewById(R.id.rotateright);
		ph_im=(ImageView) findViewById(R.id.ph_img);
		caption_sp=(Spinner) findViewById(R.id.ph_caption);
		caption_sp.setEnabled(false);
		//set_image();
		
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		CapturedImageURI = this.getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
		this.startActivityForResult(intent, 111);
	}

	protected void show_caption() {
		// TODO Auto-generated method stub
		if(elev.getSelectedItemPosition()==7 || elev.getSelectedItemPosition()==8 || elev.getSelectedItemPosition()==9)
		{
			int elevpos=0;
			if(elev.getSelectedItemPosition()==7)
			{
				elevpos = 1;
			}
			else if(elev.getSelectedItemPosition()==8)
			{
				elevpos = 2;
			}
			else if(elev.getSelectedItemPosition()==9)
			{
				elevpos = 3;
			}
			Cursor c1=db.wind_db.rawQuery("SELECT max(GCH_ImageOrder) as GCH_ImageOrder  from "+db.GeneralHazDoc+" WHERE GCH_SRID='"+cf.selectedhomeid+"' and GCH_Elevation='"+elevpos+"'",null);
			image_order=0;
			if(c1.getCount()>0)
			{
				c1.moveToFirst();
				if((c1.getString(c1.getColumnIndex("GCH_ImageOrder")))!=null)
				{
					image_order=(c1.getInt(c1.getColumnIndex("GCH_ImageOrder"))+1);
				}
				else
				{
					image_order =1;
				}
				if(image_order>8)
				{
					cf.ShowToast("Sorry you have already added maximum no of images for "+elev.getSelectedItem());
					caption_sp.setEnabled(false);
					elev.setSelection(0);
					
				}else
				{
					caption_sp.setEnabled(true);
				}
				
			}
			else
			{
				caption_sp.setEnabled(true);
			}
		}
		else
		{
			Cursor c=db.wind_db.rawQuery("SELECT max(IM_ImageOrder) as IM_ImageOrder  from "+db.Photos+" WHERE IM_SRID='"+cf.selectedhomeid+"' and IM_Elevation='"+elev.getSelectedItemPosition()+"'",null);
			image_order=0;
			if(c.getCount()>0)
			{
				c.moveToFirst();
				
				if((c.getString(c.getColumnIndex("IM_ImageOrder")))!=null)
				{
					image_order=(c.getInt(c.getColumnIndex("IM_ImageOrder"))+1);
				}
				else
				{
					image_order =1;
				}
				if(image_order>8)
				{
					cf.ShowToast("Sorry you have already added maximum no of images for "+elev.getSelectedItem());
					caption_sp.setEnabled(false);
					elev.setSelection(0);
					
				}else
				{
					caption_sp.setEnabled(true);
				}
				
			}
			else
			{
				caption_sp.setEnabled(true);
			}
		}
		
		cf.getElevationname(elev.getSelectedItemPosition());
		
		Cursor cap=db.SelectTablefunction(db.PhotoCaption, " WHERE IM_C_InspectorId='"+db.Insp_id+"' " +
				"AND IM_C_Elevation='"+image_order+"'  and IM_C_Elevation_Name='"+cf.elevname+"' ");
		System.out.println("cap="+cap.getCount()+"WHERE IM_C_InspectorId='"+db.Insp_id+"' " +
				"AND IM_C_Elevation='"+image_order+"'  and IM_C_Elevation_Name='"+cf.elevname+"'");
		String caption[];
		if(cap.getCount()>0)
		{
			cap.moveToFirst();
			if(elev.getSelectedItemPosition()==7 || elev.getSelectedItemPosition()==8 || elev.getSelectedItemPosition()==9)
			{
				caption=new String[cap.getCount()+2];
				caption[0]="--Select--";
				caption[1]="ADD PHOTO CAPTION";
				for(int j=2;j<cap.getCount()+2;j++,cap.moveToNext())
				{
					caption[j]=db.decode(cap.getString(cap.getColumnIndex("IM_C_caption")));
				}
				
			}
			else
			{
				caption=new String[cap.getCount()];
				for(int j=0;j<cap.getCount();j++,cap.moveToNext())
				{
					caption[j]=db.decode(cap.getString(cap.getColumnIndex("IM_C_caption")));
				}
			}
			
		}
		else
		{
			caption=new String[2];
			caption[0]="--Select--";
			caption[1]="ADD PHOTO CAPTION";
		}
		ArrayAdapter ad1 =new ArrayAdapter(this, android.R.layout.simple_spinner_item, caption);
		ad1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		caption_sp.setAdapter(ad1);
		caption_sp.setOnItemSelectedListener(new sp_onclicker());
		
	}

	class sp_onclicker implements OnItemSelectedListener
	{
		
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(caption_sp.getSelectedItem().toString().trim().equals("ADD PHOTO CAPTION"))
				{
					final Dialog dialog1 = new Dialog(CameraImage.this,android.R.style.Theme_Translucent_NoTitleBar);
					dialog1.getWindow().setContentView(R.layout.alert);
					((LinearLayout)dialog1.findViewById(R.id.camera)).setVisibility(arg1.VISIBLE);
					ed=((EditText)dialog1.findViewById(R.id.ed_values));
					Button save=((Button)dialog1.findViewById(R.id.save));
					Button clear=((Button)dialog1.findViewById(R.id.clear));
					Button close=((Button)dialog1.findViewById(R.id.helpclose));
					((TextView)dialog1.findViewById(R.id.txthelp)).setText("Add caption");
					((TextView)dialog1.findViewById(R.id.txtquestio)).setText("Enter your caption");
					close.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							caption_sp.setSelection(0);
						}
					});
					clear.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ed.setText("");
						}
					});
					save.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							if(!ed.getText().toString().trim().equals(""))
							{
								if(elev.getSelectedItemPosition()==1)
								{
									elevname = "FE";
								}
								else if(elev.getSelectedItemPosition()==2)
								{
									elevname = "RE";
								}
								else if(elev.getSelectedItemPosition()==3)
								{
									elevname = "BE";
								}
								else if(elev.getSelectedItemPosition()==4)
								{
									elevname = "LE";
								}
								else if(elev.getSelectedItemPosition()==5)
								{
									elevname = "AE";
								}
								else if(elev.getSelectedItemPosition()==6)
								{
									elevname = "Additional Photo";
								}
								else if(elev.getSelectedItemPosition()==7)
								{
									elevname = "EXT";
								}
								else if(elev.getSelectedItemPosition()==8)
								{
									elevname = "INT";
								}
								else if(elev.getSelectedItemPosition()==9)
								{
									elevname = "AI";
								}
								db.wind_db.execSQL(" INSERT INTO "+db.PhotoCaption+" (IM_C_InspectorId,IM_C_Elevation,IM_C_caption,IM_C_Elevation_Name) " +
										"VALUES ('"+db.Insp_id+"','"+image_order+"','"+db.encode(ed.getText().toString().trim())+"'," +
												"'"+elevname+"')");
								//add_caption(ed.getText().toString().trim(),img_order);
								show_caption();
								dialog1.dismiss();
							}
							else
							{
								cf.ShowToast("Please enter caption ");
							}
						}

						
					});
					dialog1.show();				
					}
				
			}
		
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		}
	private void set_image() {
		// TODO Auto-generated method stub
		rotated_b=cf.ShrinkBitmap(path, 400, 400);
		
		if(rotated_b ==null)
		{
			ph_im.setImageDrawable(getResources().getDrawable(R.drawable.noimage));
		}
		ph_im.setImageBitmap(rotated_b);
		
	}

	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.clear:
			elev.setSelection(0);
			caption_sp.setSelection(0);
		break;
		case R.id.rotateleft:
			if(rotated_b!=null)
			{
			System.gc();
			currnet_rotated-=90;
			if(currnet_rotated<0)
			{
				currnet_rotated=270;
			}

			
			Bitmap myImg;
			try {
				
					myImg = BitmapFactory.decodeStream(new FileInputStream(path));
				Matrix matrix =new Matrix();
				matrix.reset();
				//matrix.setRotate(currnet_rotated);
				
				matrix.postRotate(currnet_rotated);
				
				 rotated_b  = Bitmap.createBitmap(myImg, 0, 0,  myImg.getWidth(),myImg.getHeight(),
				        matrix, true);
				 
				 ph_im.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) { }
			catch (Exception e) { }
			catch (OutOfMemoryError e) {}
			}
		break;

		case R.id.rotateright:
			if(rotated_b!=null)
			{
			currnet_rotated+=90;
			if(currnet_rotated>=360)
			{
				currnet_rotated=0;
			}
			
			Bitmap myImg1;
			try {
				
					myImg1 = BitmapFactory.decodeStream(new FileInputStream(path));
				
				
				Matrix matrix =new Matrix();
				matrix.reset();
				//matrix.setRotate(currnet_rotated);
				matrix.postRotate(currnet_rotated);
				
				 rotated_b  = Bitmap.createBitmap(myImg1, 0, 0,  myImg1.getWidth(),myImg1.getHeight(),
				        matrix, true);
				 System.gc();
				 ph_im.setImageBitmap(rotated_b);

			} catch (FileNotFoundException e) { }
			catch (Exception e) { }
			catch (OutOfMemoryError e) {}
			}
			break;
		case R.id.ph_img:
			String fileName = "temp.jpg";
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, fileName);
			CapturedImageURI = getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
			startActivityForResult(intent, 111);
		break;
		case R.id.close:
			Intent in = getIntent();
			in.putExtras(b);
			setResult(RESULT_CANCELED, in);
			finish();
		break;
		case R.id.save:
			System.out.println("path="+path);
			if(!path.equals(""))
			{
			if(elev.getSelectedItemPosition()!=0)
			{
				if(caption_sp.getSelectedItemPosition()!=0 && caption_sp.getSelectedItemPosition()!=0)
				{
				if(currnet_rotated>0)
				{ 

					try
					{
					
						String current=MediaStore.Images.Media.insertImage(getContentResolver(), rotated_b, "My bitmap", "My rotated bitmap");
						  ContentValues values1 = new ContentValues();
						  values1.put(MediaStore.Images.Media.ORIENTATION, 0);
						  CameraImage.this.getContentResolver().update(Uri.parse(current), values1, MediaStore.Images.Media.DATA+ "=?", new String[] { current } );
						
						
						if(current!=null)
						{
						String path=cf.getPath(Uri.parse(current));
						
							
							File fout = new File(this.path);
							fout.delete();
							
							
							File fin = new File(path);
						
							fin.renameTo(new File(this.path));
						}
					} catch(Exception e)
					{
						System.out.println("Error occure while rotate the image "+e.getMessage());
					}
			}
				if(elev.getSelectedItemPosition()==7 || elev.getSelectedItemPosition()==8 || elev.getSelectedItemPosition()==9)
				{
					int elevpos=0;
					if(elev.getSelectedItemPosition()==7)
					{
						elevpos = 1;
					}
					else if(elev.getSelectedItemPosition()==8)
					{
						elevpos = 2;
					}
					else if(elev.getSelectedItemPosition()==9)
					{
						elevpos = 3;
					}
				
				db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazDoc
							+ " (GCH_InspectorId,GCH_SRID,GCH_Elevation,GCH_path,GCH_Description,GCH_ImageOrder)"
							+ " VALUES ('"+db.Insp_id+"','" + cf.selectedhomeid + "','"+elevpos+ "','"+ db.encode(path) + "','"
							+ db.encode(caption_sp.getSelectedItem().toString().trim())+ "','" + (image_order) + "')");
				
				}
				else
				{
				db.wind_db.execSQL("INSERT INTO "
							+ db.Photos
							+ " (IM_InspectorId,IM_SRID,IM_Elevation,IM_path,IM_Description,IM_ImageOrder)"
							+ " VALUES ('"+db.Insp_id+"','" + cf.selectedhomeid + "','"+elev.getSelectedItemPosition()+ "','"+ db.encode(path) + "','"
							+ db.encode(caption_sp.getSelectedItem().toString().trim())+ "','" + (image_order) + "')");
 					
				}
				cf.ShowToast("Image saved successfully");

				Intent in2 = getIntent();
				Bundle b1 =in2.getExtras();
				b1.putBoolean("Delete_data", false);
				//b1.putString("Caption", ed.getText().toString().trim());
				b1.putString("Path", path);
				in2.putExtras(b1);
				setResult(RESULT_OK, in2);
				finish();
				}
				else
				{
					cf.ShowToast(" Please select caption");
				}
			}
			else
			{
				cf.ShowToast(" Please select elevation");
			}
			}
			else
			{
				cf.ShowToast(" Please take image then try again");
			}
		break;
		default:
		break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);	
		if(resultCode==RESULT_OK)
		{
			if(requestCode==122)
			{
				String[] value=	data.getExtras().getStringArray("Selected_array");
				path=value[0];
				set_image();
			}
			else if (requestCode == 111) {
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(CapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String capturedImageFilePath = cursor
						.getString(column_index_data);
				String selectedImagePath = capturedImageFilePath;
				path=selectedImagePath;
				set_image();
			}
		}
		else
		{
			System.out.println("comes in the correct place");
		}
	
		
	}
	
	
}
