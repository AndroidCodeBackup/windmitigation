package idsoft.inspectiondepot.windmitinspection;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class View_pdf extends Activity {
    RelativeLayout rlqainspection,rlexport;
    TextView tvnoofrecords,tvnorecord;
    LinearLayout inspectionlist;
    private int show_handler;
	private int total;
	private String[] data,srid;
	private String[] datasend;
	private String[] pdfpath;
	private String path;
	private String status;
	private String classidentifier;
	private int no_of_record=0;
	private int current=1,rows;
	TextView tvrows,t;
	Cursor cur;
	RelativeLayout rlheader;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	int rec_per_page=10;
	int numbeofpage=1;
	AutoCompleteTextView actsearch;
	View v1;
	PowerManager.WakeLock wl=null;
	public int[] txtid = {R.id.txta,R.id.txtb,R.id.txtc,R.id.txtd,R.id.txte,R.id.txtf,R.id.txtg,R.id.txth,R.id.txti,R.id.txtj,
			R.id.txtk,R.id.txtl,R.id.txtm,R.id.txtn,R.id.txto,R.id.txtp,R.id.txtq,R.id.txtr,R.id.txts,R.id.txtt,R.id.txtu,
			R.id.txtv,R.id.txtw,R.id.txtx,R.id.txty,R.id.txtz,R.id.txtall};
	public TextView[] txt = new TextView[27];
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewlist);
		cf=new CommonFunctions(this);
		db= new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		Bundle b =getIntent().getExtras();
		if(b!=null)
		{
			status=b.getString("type");
			classidentifier=b.getString("classidentifier");
			no_of_record=b.getInt("total_record");
			current=b.getInt("current");
	
		}
		declaration();
		RRInspectionListing();
				
		
		
	}
	private void declaration() {
		// TODO Auto-generated method stub
		tvrows  = (TextView) findViewById(R.id.rws);
		t = (TextView)findViewById(R.id.textView1);
		t.setText("Download Reports");
		
		inspectionlist = (LinearLayout) findViewById(R.id.linlayoutdyn);
		rlheader = (RelativeLayout) findViewById(R.id.emailreport_rlheader);
		actsearch = (AutoCompleteTextView) findViewById(R.id.emailreport_actsearch);
		
		TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1,int i2) {
				if (actsearch.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	actsearch.setText("");
		        }
			}

			@Override
			public void afterTextChanged(Editable editable) {
				// here, after we introduced something in the EditText we get
				// the string from it
				try {
					db.CreateTable(26);
					Cursor cur1 = db.wind_db.rawQuery("select * from " + db.ReportsReady
									+ " where policyno like '"
									+ db.encode(actsearch.getText().toString())
									+ "%'", null);
					String[] autopolicyno = new String[cur1.getCount()];
					cur1.moveToFirst();
					if (cur1.getCount() != 0) {
						if (cur1 != null) {
							int i = 0;
							do {
								autopolicyno[i] = db.decode(cur1.getString(cur1
										.getColumnIndex("policyno")));

								if (autopolicyno[i].contains("null")) {
									autopolicyno[i] = autopolicyno[i].replace(
											"null", "");
								}

								i++;
							} while (cur1.moveToNext());
						}
						cur1.close();
					}

					Cursor cur2 = db.wind_db.rawQuery("select * from "
							+ db.policyholder + " where firstname like '"
							+ db.encode(actsearch.getText().toString()) + "%' and PH_InspectorId='"+db.Insp_id+"'",
							null);
					String[] autoownersname = new String[cur2.getCount()];
					cur2.moveToFirst();
					if (cur2.getCount() != 0) {
						if (cur2 != null) {
							int i = 0;
							do {
								autoownersname[i] = db.decode(cur2
										.getString(cur2
												.getColumnIndex("firstname")));

								if (autoownersname[i].contains("null")) {
									autoownersname[i] = autoownersname[i]
											.replace("null", "");
								}

								i++;
							} while (cur2.moveToNext());
						}
						cur2.close();
					}

					List<String> images = new ArrayList<String>();
					images.addAll(Arrays.asList(autopolicyno));
					images.addAll(Arrays.asList(autoownersname));

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(View_pdf.this, R.layout.autocompletelist, images);
					actsearch.setThreshold(1);
					actsearch.setAdapter(adapter);

				} catch (Exception e) {

				}

			}
		};

		// third, we must add the textWatcher to our EditText
		actsearch.addTextChangedListener(textWatcher);
		
		for(int i=0;i<txt.length;i++)
		{
			txt[i] = (TextView)findViewById(txtid[i]);
			txt[i].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					actsearch.setText("");
					switch(v.getId())
					{

					   case R.id.txta: 
						   txtsearch("A");changetextbgcolor();
						   ((TextView)findViewById(R.id.txta)).setBackgroundColor(Color.parseColor("#519BC2")); 
						break;
					   case R.id.txtb: 
						   txtsearch("B");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtb)).setBackgroundColor(Color.parseColor("#519BC2")); 
						break;
					   case R.id.txtc: 
						   txtsearch("C");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtc)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtd: 
						   txtsearch("D");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtd)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txte: 
						   txtsearch("E");changetextbgcolor();
						   ((TextView)findViewById(R.id.txte)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtf: 
						   txtsearch("F");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtf)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtg: 
						   txtsearch("G");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtg)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txth: 
						   txtsearch("H");changetextbgcolor();
						   ((TextView)findViewById(R.id.txth)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txti: 
						   txtsearch("I");changetextbgcolor();
						   ((TextView)findViewById(R.id.txti)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtj: 
						   txtsearch("J");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtj)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtk: 
						   txtsearch("K");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtk)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtl: 
						   txtsearch("L");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtl)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtm: 
						   txtsearch("M");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtm)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtn: 
						   txtsearch("N");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtn)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txto: 
						   txtsearch("O");changetextbgcolor();
						   ((TextView)findViewById(R.id.txto)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtp: 
						   txtsearch("P");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtp)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtq: 
						   txtsearch("Q");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtq)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtr: 
						   txtsearch("R");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtr)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txts: 
						   txtsearch("S");changetextbgcolor();
						   ((TextView)findViewById(R.id.txts)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtt: 
						   txtsearch("T");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtt)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtu: 
						   txtsearch("U");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtu)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtv: 
						   txtsearch("V");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtv)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtw: 
						   txtsearch("W");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtw)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtx: 
						   txtsearch("X");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtx)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txty: 
						   txtsearch("Y");changetextbgcolor();
						   ((TextView)findViewById(R.id.txty)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtz: 
						   txtsearch("Z");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtz)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtall: 
						   txtsearch("");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtall)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					
					}
					
				}
			});
		}
		((ImageView) findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("inside");
				Intent insp_info = new Intent(View_pdf.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);
			}
		});
		
	}
	private void changetextbgcolor()
	{
		for(int i=0;i<txt.length;i++)
		{
			txt[i].setBackgroundColor(Color.parseColor("#2682B3"));
		}
	}
	protected void txtsearch(String string) {
		// TODO Auto-generated method stub
		DynamicList(string);
		
	}
	private void RRInspectionListing()
	{
		if (wb.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing... Please wait..."
					+ " </font></b>";
			final ProgressDialog pd = ProgressDialog.show(View_pdf.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				

				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(wb.NAMESPACE,"ExportReportsready");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("InspectorId",Integer.parseInt(db.Insp_id));
						envelope.setOutputSoapObject(request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
						androidHttpTransport.call(wb.NAMESPACE+"ExportReportsready",envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						LoadExportReportsready(result);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							Intent in = new Intent(View_pdf.this,Dashboard.class);
							startActivity(in);
							finish();

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							Intent in = new Intent(View_pdf.this,Dashboard.class);
							startActivity(in);
							finish();
						} else if (show_handler == 5) {
							show_handler = 0;
							
							
							Cursor cur=db.wind_db.rawQuery("select * from "+db.ReportsReady, null);//+"  order by Schedule_ScheduledDate desc"
							int count=cur.getCount();
							tvrows.setText("TOTAL # OF RECORDS : "+count);
							if(count>=1)
							{
								inspectionlist.setVisibility(View.VISIBLE);
								rlheader.setVisibility(View.VISIBLE);
								DynamicList("");
								
							}
							else
							{
								inspectionlist.setVisibility(View.GONE);
								rlheader.setVisibility(View.GONE);
							}
							
							
						}
					}
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
			Intent in = new Intent(this,Dashboard.class);
			startActivity(in);
			finish();
			
		}
	}
	
	private void LoadExportReportsready(SoapObject result)
	{
		db.CreateTable(26);
		System.out.println(" the values "+result);
	 	db.wind_db.execSQL("delete from "+db.ReportsReady);
		int count;
		if(String.valueOf(result).equals("null") || String.valueOf(result).equals(null) || String.valueOf(result).equals("anytype{}"))
		 {
				count=0;
				total = 100;
		  }
		 else
		 {
			 count = result.getPropertyCount();
			 System.out.println("count "+count);
			 no_of_record=count;
			 int min=0,max=500;
			 int j=0;
			
			 for (int i=0; i<count; i++) {
				 
					SoapObject obj = (SoapObject) result.getProperty(i);
					System.out.println("comes corretc"+obj);
					String srid = String.valueOf(obj.getProperty("SRID"));
					String firstname = String.valueOf(obj.getProperty("FirstName"));
					String lastname = String.valueOf(obj.getProperty("LastName"));
					String address1 = String.valueOf(obj.getProperty("Address1"));
					String address2 = String.valueOf(obj.getProperty("Address2"));
					String state = String.valueOf(obj.getProperty("State"));
					String county = String.valueOf(obj.getProperty("Country"));
					String city = String.valueOf(obj.getProperty("City"));
					String zip = String.valueOf(obj.getProperty("Zip"));
					String email = String.valueOf(obj.getProperty("Email"));
					String status = String.valueOf(obj.getProperty("Status"));
					String policynumber = String.valueOf(obj.getProperty("OwnerPolicyNo"));
					String pdfpath = String.valueOf(obj.getProperty("Pdfpath"));
					String create_date =String.valueOf(obj.getProperty("AcceptedDate"));
					String Inspecteddate = String.valueOf(obj.getProperty("Inspecteddate"));
					System.out.println("reate"+create_date);

					db.wind_db.execSQL("insert into "
							+ db.ReportsReady
							+ " (srid,firstname,lastname,address1,address2,state,county,city,zip,policyno,email,status,pdfpath,inspectiondate,created_Date) values('"
							+ db.encode(srid) + "','"
							+ db.encode(firstname) + "','"
							+ db.encode(lastname) + "','"
							+ db.encode(address1) + "','"
							+ db.encode(address2) + "','"
							+ db.encode(state) + "','"
							+ db.encode(county) + "','"
							+ db.encode(city) + "','"
							+ db.encode(zip) + "','"
							+ db.encode(policynumber) + "','"
							+ db.encode(email) + "','"
							+ db.encode(status) + "','" + db.encode(pdfpath)
							+ "','"+db.encode(Inspecteddate)+"','"+create_date+"');");
					
					 j++;		
			 }
			
		 }
			
	}
	
	private void DynamicList(String string) {
		//System.out.println(" comes the sql= ORDER BY created_Date DESC LIMIT "+(start-1)+","+(end-1));
		db.CreateTable(26);
		if(string.equals(""))
		{
			cur=db.wind_db.rawQuery("Select * from "+db.ReportsReady + " Order by created_Date desc", null);
		}
		else
		{
			cur = db.wind_db.rawQuery("select * from " + db.ReportsReady
					+ " where firstname like '"+ db.encode(string)+ "%' or (policyno like '"+ db.encode(string) + "%' AND policyno!='N%2FA') order by created_Date desc", null);
		}
		tvrows.setText("TOTAL # OF RECORDS : "+cur.getCount());
				
		inspectionlist.removeAllViews();
		ScrollView sv = new ScrollView(this);
		inspectionlist.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		rows = cur.getCount();
		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] pter = new Button[rows];
		
		//final Button[] sendmail = new Button[rows];
		LinearLayout[] rl = new LinearLayout[rows];
		final String[] ownersname = new String[rows];
		data = new String[rows];
		datasend = new String[rows];
		pdfpath = new String[rows];
		srid = new String[rows];
		l1.removeAllViews();
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			rlheader.setVisibility(View.VISIBLE);
			int i = 0;
			do {

				String SRID = db.decode(cur.getString(cur
						.getColumnIndex("srid")));
				srid[i]=SRID;
				
				String FirstName = db.decode(cur.getString(cur
						.getColumnIndex("firstname")));
				data[i] = " " + FirstName + " | ";
				ownersname[i] = FirstName + " ";
				String LastName = db.decode(cur.getString(cur
						.getColumnIndex("lastname")));
				data[i] += LastName + " | ";
				ownersname[i] += " " + FirstName;
				String Address1 = db.decode(cur.getString(cur
						.getColumnIndex("address1")));
				data[i] += Address1 + " | ";
			/*	String Address2 = db.decode(cur.getString(cur
						.getColumnIndex("address2")));
				data[i] += Address2 + " | ";*/
				String City = db.decode(cur.getString(cur
						.getColumnIndex("city")));
				data[i] += City + " | ";
				String State = db.decode(cur.getString(cur
						.getColumnIndex("state")));
				data[i] += State + " | ";
				String Country = db.decode(cur.getString(cur
						.getColumnIndex("county")));
				data[i] += Country + " | ";
				String Zip = db
						.decode(cur.getString(cur.getColumnIndex("zip")));
				data[i] += Zip + " | ";
				
				String inspectiondate = db
						.decode(cur.getString(cur.getColumnIndex("inspectiondate")));
				data[i] += inspectiondate + " | ";
				
				String OwnerPolicyNo = db.decode(cur.getString(cur
						.getColumnIndex("policyno")));
				data[i] += OwnerPolicyNo + " | ";
				datasend[i] = OwnerPolicyNo + ":";
				
				String Email = db.decode(cur.getString(cur
						.getColumnIndex("email")));
				data[i] += Email;
				
				String Status = db.decode(cur.getString(cur
						.getColumnIndex("status")));
				datasend[i] += Status;
				String COMMpdf = db.decode(cur.getString(cur
						.getColumnIndex("pdfpath")));
				pdfpath[i] = COMMpdf;

//				System.out.println("The COMMpdf path is " + COMMpdf);
//
//				System.out.println("The Datas is " + data[i]);

				LinearLayout.LayoutParams llparams;
				LinearLayout.LayoutParams lltxtparams;

				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				llparams.setMargins(0, 2, 0, 0);

				rl[i] = new LinearLayout(this);
				l1.addView(rl[i], llparams);

				lltxtparams = new LinearLayout.LayoutParams(
						620,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(10, 20, 10, 20);

				tvstatus[i] = new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setId(1);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTextSize(14);
				rl[i].addView(tvstatus[i]);

				LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				downloadparams1.setMargins(0, 0, 20, 0);
				downloadparams1.gravity = Gravity.CENTER_VERTICAL;

				LinearLayout.LayoutParams sendmailparams = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				sendmailparams.setMargins(110, 0, 20, 0);
				sendmailparams.gravity = Gravity.CENTER_VERTICAL;

				view[i] = new Button(this, null);
				view[i].setLayoutParams(downloadparams1);
				view[i].setId(2);
				view[i].setText("View PDF");
				view[i].setBackgroundResource(R.drawable.buttonstyle);
				view[i].setTextSize(14);
				//view[i].setTypeface(null, Typeface.BOLD);
				view[i].setTag(i);
				rl[i].addView(view[i]);
				

				pter[i] = new Button(this);
				pter[i].setLayoutParams(downloadparams1);
				pter[i].setId(2);
				pter[i].setText("Email Report");
				pter[i].setBackgroundResource(R.drawable.buttonstyle);
				pter[i].setTextSize(14);
				//pter[i].setTypeface(null, Typeface.BOLD);
				pter[i].setTag(i + ":" + Email + ":" + ownersname[i] + ":" + srid[i]);
				rl[i].addView(pter[i]);
				
			
				if (COMMpdf.equals("N/A")) {
				//	sendmail[i].setVisibility(View.VISIBLE);
					view[i].setVisibility(View.GONE);
					pter[i].setVisibility(View.GONE);
				} else {
				//	sendmail[i].setVisibility(View.GONE);
					view[i].setVisibility(View.VISIBLE);
					pter[i].setVisibility(View.VISIBLE);

				}

				view[i].setOnClickListener(new OnClickListener() {

					

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						String buttonvalue = v.getTag().toString();
						
						int s = Integer.parseInt(buttonvalue);
						path = pdfpath[s];
						String[] filenamesplit = path.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						File sdDir = new File(Environment
								.getExternalStorageDirectory().getPath());
						File file = new File(sdDir.getPath()
								+ "/DownloadedPdfFile/" + filename);

						if (file.exists()) {
							View_Pdf_File(filename);
						} else {
							if (wb.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading... ");
								 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
								 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
								 wl.acquire();
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {

											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

											} else if (show_handler == 2) {
												show_handler = 0;

												View_Pdf_File(filename);

											}
											if(wl!=null)
											{
											wl.release();
											wl=null;
											}
										}
									};
								}.start();
							} else {
								cf.ShowToast("Internet connection not available");

							}
						}
					}
				});

				
				pter[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String buttonvalue = v.getTag().toString();
						String[] arrayvalue = buttonvalue.split(":");
						String value = arrayvalue[0];
						String emailaddress = arrayvalue[1];
						String name = arrayvalue[2];
						String homeid = arrayvalue[3];
						
						String ivalue = value;
						int ival = Integer.parseInt(ivalue);
						String ivalsplit = datasend[ival];
						String[] arrayivalsplit = ivalsplit.split(":");
						String pn = arrayivalsplit[0];
						String status = arrayivalsplit[1];

						int s = Integer.parseInt(value);
						path = pdfpath[s];
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];

						Intent intent = new Intent(View_pdf.this,
								EmailReport2.class);
						intent.putExtra("policynumber", homeid);
						intent.putExtra("status", status);
						intent.putExtra("mailid", emailaddress);
						intent.putExtra("classidentifier", classidentifier);
						intent.putExtra("ownersname", ownersname[ival]);
						intent.putExtra("count", rows);
						startActivity(intent);
						finish();

					}
				});
				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#1871A0"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#106896"));
				}
				
				i++;
			} while (cur.moveToNext());

		}else
			
		{
			rlheader.setVisibility(View.GONE);
			inspectionlist.removeAllViews();
			cf.ShowToast("Sorry, No results available");
		}

	}
	private void View_Pdf_File(String filename)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(View_pdf.this,
					ViewPdfFile.class);
			intentview.putExtra("path", path);
			startActivity(intentview);
			finish();
        }
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.home:
			cf.gohome();
		break;
		case R.id.emailreport_search:
			changetextbgcolor();
			Search();
			cf.hidekeyboard((EditText)actsearch);
			break;

		case R.id.emailreport_cancel:
			changetextbgcolor();((TextView)findViewById(R.id.txtall)).setBackgroundColor(Color.parseColor("#519BC2"));
			actsearch.setText("");
			DynamicList("");
			cf.hidekeyboard((EditText)actsearch);
			break;
		
		default:
			break;
		}
	}
	private void Search() {
		// TODO Auto-generated method stub
		if (actsearch.getText().toString().trim().equals("")) {
			cf.ShowToast("Please enter the Name or Policy Number to search");
			actsearch.requestFocus();
			actsearch.setText("");
		} else {
			DynamicList(actsearch.getText().toString().trim());
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent in = new Intent(this,Dashboard.class);
			in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(in);
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	
}
