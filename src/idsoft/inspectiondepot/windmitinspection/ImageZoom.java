package idsoft.inspectiondepot.windmitinspection;

import idsoft.inspectiondepot.windmitinspection.imagezoom.DecodeUtils;
import idsoft.inspectiondepot.windmitinspection.imagezoom.ImageViewTouch;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class ImageZoom extends Activity {
	ImageViewTouch mImage;
	Button mButton;
	String arrpath;
	CommonFunctions cf;
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		requestWindowFeature( Window.FEATURE_NO_TITLE );
		cf= new CommonFunctions(this);
		setContentView(R.layout.imagezoom);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			arrpath = extras.getString("Path");
	 	}		
		mImage = (ImageViewTouch) findViewById( R.id.image );
		mButton = (Button) findViewById( R.id.button);		
		mButton.setOnClickListener( new OnClickListener() {			
			@Override
			public void onClick( View v ) {
				finish();
			}
		});
		Bitmap bitmap = BitmapFactory.decodeFile(arrpath);
		mImage.setImageBitmap( bitmap );
		
	}
}
