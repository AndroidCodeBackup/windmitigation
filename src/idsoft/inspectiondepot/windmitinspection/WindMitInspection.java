package idsoft.inspectiondepot.windmitinspection;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;



public class WindMitInspection extends Activity 
{	
	ArrayElevation arrelev = new ArrayElevation();
	EditText password;
	AutoCompleteTextView username;
	ProgressDialog progressDialog,pd1;
	int logincheck = 0,show_handler, usercheck = 0,cleartxt=0,ipAddress;
	String autousername[];
	String bulkinsertstring="",elevtype,elevid,uname, rempwd="0",pwd, dbuname, dbpwd, dbinsid, status = "false",
			deviceId,model,manuf,devversion,apiLevel,Ins_Flag="1",status_userid="",chkstatus="",forpassword="",forusername="";
	private static final String TAG = null;
	TableLayout tblshow;
	View v1;
	TextWatcher watcher;
	CheckBox rememberpwd;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	android.text.format.DateFormat df;
	CharSequence cd;
	SoapObject resultforemailphotoextn;
	ProgressDialog pd;
	int k=0;
	private Object Settings;
	static boolean animationflag=false;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Bundle bundle=getIntent().getExtras();
		if(bundle!=null)
		{
			if(bundle.getString("status")!=null)
			{
			  chkstatus=bundle.getString("status");
			  if(chkstatus.equals("forgot"))
			  {
				  forpassword=bundle.getString("retpass");
				  forusername=bundle.getString("retuser");
			  }
			}
		}
		
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		setContentView(R.layout.login);
		declaration();
		Parse_Code();
		System.out.println("chkstatus="+chkstatus);
		
		
		
		if(chkstatus.equals("register"))
		{
			System.out.println("inside");
			Cursor cur1 = db.wind_db.rawQuery("select * from " + db.inspectorlogin+" where Ins_Flag1=1", null);
			System.out.println("cur1"+cur1.getCount());
			if(cur1.getCount() >= 1)
			{
				cur1.moveToFirst();
				System.out.println("username="+cur1.getString(cur1.getColumnIndex("Ins_UserName")));
				username.setText(db.decode(cur1.getString(cur1.getColumnIndex("Ins_UserName"))));
				password.setText(db.decode(cur1.getString(cur1.getColumnIndex("Ins_Password"))));
			}
		}
		else if(chkstatus.equals("forgot"))
		{
			username.setText(db.decode(forusername));
			password.setText(db.newdecode(forpassword));
		}		
		else
		{
		       check_logintable();
		}
	}
	private void check_logintable() {
		// TODO Auto-generated method stub
		db.CreateTable(1);
		Cursor cur = db.wind_db.rawQuery("select * from " + db.inspectorlogin + " where Ins_Flag1=1", null);System.out.println("cur"+cur.getCount());
		if(cur.getCount() == 1)
		{
			startActivity(new Intent(this, HomeScreen.class));
			finish();
		}		
		cur.close();
	}
	private void declaration() {
		// TODO Auto-generated method stub
		password = (EditText) this.findViewById(R.id.epwd);
		username = (AutoCompleteTextView) this.findViewById(R.id.eusername);	
		
		username.addTextChangedListener(new EditCustomTextWatcher(username));
		password.addTextChangedListener(new EditCustomTextWatcher(password));
		
		TextView forgotpwd = (TextView) findViewById(R.id.forgotpassword);
		SpannableString content = new SpannableString("Forgot Password ?");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		forgotpwd.setText(content);
		db.CreateTable(1);System.out.println("inside parse code");
		
		
        ((TextView)findViewById(R.id.forgotpassword)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(WindMitInspection.this,ForgotPassword.class));
				finish();
			}
		});
	}
	public void clicker(View v)
	{
		switch (v.getId()) 
		{
			case R.id.login:
				if (username.getText().toString().trim().equals("") && password.getText().toString().trim().equals("")) {
					cf.ShowToast("Please enter Username and Password");
					username.requestFocus();
					username.setText("");
					password.setText("");
				} else {
					if (username.getText().toString().trim().equals("")) {
						cf.ShowToast("Please enter Username");
						username.requestFocus();
						username.setText("");
					} else {
						if (password.getText().toString().trim().equals("")) {
							cf.ShowToast("Please enter Password");
							password.requestFocus();
							password.setText("");
						} else {
							try
							{
								Cursor cur = db.wind_db.rawQuery("select * from " +db.inspectorlogin + " where Ins_UserName='"+ db.encode(username.getText().toString().toLowerCase())+ "'",null);
							System.out.println("cur.getCount()"+cur.getCount());
								int rws = cur.getCount();
								if(rws>0)
								{
									cur.moveToFirst();
									int Column1 = cur.getColumnIndex("Ins_UserName");
									int Column2 = cur.getColumnIndex("Ins_Password");
									int Column3 = cur.getColumnIndex("Ins_Id");
									
									dbuname = db.decode(cur.getString(Column1));
									dbpwd = db.decode(cur.getString(Column2));
									dbinsid = db.decode(cur.getString(Column3));
									System.out.println("dbuname="+dbuname+"dbpwd="+dbpwd);
									System.out.println("dbuname="+username.getText().toString().toLowerCase()+"dbpwd="+password.getText().toString());
									if (username.getText().toString().trim().toLowerCase().equals(dbuname)&& password.getText().toString().trim().equals(dbpwd)) {
										db.wind_db.execSQL("UPDATE "+ db.inspectorlogin+ " SET Ins_Flag1=1"+ " WHERE Ins_Id ='"+ dbinsid + "'");
										
										logincheck = 1;
										
									} else {
										logincheck = 2;
									}
								}
								else
								{
									logincheck = 0;
								}
							}
							catch (Exception e) {
								// TODO: handle exception
							}
							if(logincheck == 0)
							{
								login();
							}
							else if(logincheck == 1)
							{
								insertcaption();
							}
							else if(logincheck == 2)
							{
								cf.ShowToast("Invalid UserName or Password");
							}
						
						}
					}
				}

			break;
			case R.id.cancellogin:
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				break;
				
			case R.id.register:
				Intent in = new Intent(this,Registration.class);
				in.putExtra("inspid", 0);
				startActivity(in);
				finish();
				break;
				
			
		}
	}
	private void login() {
		// TODO Auto-generated method stub
		
		if (wb.isInternetOn() == true) {
			cf.show_ProgressDialog("Checking user authentication..");
			new Thread() {
				String chkauth;
				public void run() {
					Looper.prepare();
					try
					{
						SoapObject request = new SoapObject(wb.NAMESPACE,"USERAUTHENTICATION");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("UserName",username.getText().toString());
						request.addProperty("Password",password.getText().toString());
						envelope.setOutputSoapObject(request);
						System.out.println("USERAUTHENTICATION request is " + request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);						
						androidHttpTransport.call(wb.NAMESPACE+ "USERAUTHENTICATION", envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						
						System.out.println("USERAUTHENTICATION result is"+ result);
						String status = String.valueOf(result.getProperty("Status"));
						System.out.println("status="+status);
						if(status.toLowerCase().equals("true"))
						{
							 db.CreateTable(1);
							 db.wind_db.execSQL("delete from " + db.inspectorlogin);
							 String Inspector_id = String.valueOf(result.getProperty("Inspectorid"));
								String Insp_fname = String.valueOf(result.getProperty("Inspectorfirstname"));
								String Insp_mname= (String.valueOf(result.getProperty("Inspectormiddlename")).equals("anyType{}"))?"":(String.valueOf(result.getProperty("Inspectormiddlename")).equals("NA"))?"":(String.valueOf(result.getProperty("Inspectormiddlename")).equals("N/A"))?"":String.valueOf(result.getProperty("Inspectormiddlename"));
								String Insp_lname = String.valueOf(result.getProperty("Inspectorlastname"));
								String Insp_address1 = String.valueOf(result.getProperty("Inspectoraddress1"));
								String Insp_address2= (String.valueOf(result.getProperty("Inspectoraddress2")).equals("anyType{}"))?"":(String.valueOf(result.getProperty("Inspectoraddress2")).equals("NA"))?"":(String.valueOf(result.getProperty("Inspectoraddress2")).equals("N/A"))?"":String.valueOf(result.getProperty("Inspectoraddress2"));
								String Insp_city = String.valueOf(result.getProperty("City"));
								String Insp_state = String.valueOf(result.getProperty("State"));						
								String Insp_county = String.valueOf(result.getProperty("County"));
								String Insp_zip = String.valueOf(result.getProperty("Zipcode"));
								String Insp_contphone = String.valueOf(result.getProperty("Inspectorphoneno"));
								String Insp_email = String.valueOf(result.getProperty("InspectorEmail"));
								String Insp_primlictype = String.valueOf(result.getProperty("PrimaryLicenseType"));
								String Insp_primlicno = String.valueOf(result.getProperty("PrimaryLicenseNumber"));						
								String Insp_licexpirydtae = String.valueOf(result.getProperty("PrimaryLicenseExpirydt"));
								String Insp_headshot = String.valueOf(result.getProperty("InspectorHeadshot"));
								String Insp_signature= String.valueOf(result.getProperty("InspectorSign"));
								String Insp_initials= String.valueOf(result.getProperty("InspectorInitial"));
								String Insp_username= String.valueOf(result.getProperty("Inspectorusername"));
								String Insp_password = String.valueOf(result.getProperty("Inspectorpassword"));
								String Insp_companyid = String.valueOf(result.getProperty("InspectorcompanyId"));	
								String Insp_companyname= String.valueOf(result.getProperty("Inspectorcompanyname"));
								String CompanyLogo= String.valueOf(result.getProperty("Companylogo"));
								
								String androidstatus = String.valueOf(result.getProperty("AndroidStatus"));
								String LoginStatus= String.valueOf(result.getProperty("LoginStatus"));
								
								String CompanyPhone = String.valueOf(result.getProperty("Companyphoneno"));
								String CompanyEmail= String.valueOf(result.getProperty("Companyemail"));
								 
								
								try 
								{
									URL ulrn1 = new URL(Insp_headshot.replace(" ", "%20"));
							        URLConnection urlConn1 = ulrn1.openConnection();
							        InputStream in1 = urlConn1.getInputStream();
							        int c1;
							        byte[] b1 = new byte[1024];							        
							        String FILENAME1 = "Headshot"+Inspector_id+".jpg";System.out.println("FILENAME1 "+FILENAME1);
									FileOutputStream fos1 = openFileOutput(FILENAME1, Context.MODE_WORLD_READABLE);
							        
							        while ((c1 = in1.read(b1)) != -1)
							        {
							        	fos1.write(b1, 0, c1);
							        }							        
							    }
								catch (Exception e) {
									// TODO: handle exception
									System.out.println("Headshot="+e.getMessage());
								}
								
								
								try 
								{
									URL ulrn2 = new URL(Insp_signature.replace(" ", "%20"));
							        URLConnection urlConn2 = ulrn2.openConnection();
							        InputStream in2 = urlConn2.getInputStream();
							        int c2;
							        byte[] b2 = new byte[1024];							        
							        String FILENAME2 = "Signature"+Inspector_id+".jpg";System.out.println("FILENAME2 "+FILENAME2);
									FileOutputStream fos2 = openFileOutput(FILENAME2, Context.MODE_WORLD_READABLE);
							        
							        while ((c2 = in2.read(b2)) != -1)
							        {
							        	fos2.write(b2, 0, c2);
							        }							        
							    }
								catch (Exception e) {
									// TODO: handle exception
									System.out.println("Signature="+e.getMessage());
								}
								
								try 
								{
									URL ulrn3 = new URL(Insp_initials.replace(" ", "%20"));
							        URLConnection urlConn3 = ulrn3.openConnection();
							        InputStream in3 = urlConn3.getInputStream();
							        int c3;
							        byte[] b3 = new byte[1024];							        
							        String FILENAME3 = "Initials"+Inspector_id+".jpg";System.out.println("FILENAME3"+FILENAME3);
									FileOutputStream fos3 = openFileOutput(FILENAME3, Context.MODE_WORLD_READABLE);
							        
							        while ((c3 = in3.read(b3)) != -1)
							        {
							        	fos3.write(b3, 0, c3);
							        }
							    }
								catch (Exception e) {
									// TODO: handle exception
									System.out.println("Initials Logo"+e.getMessage());
								}								
								
								try 
								{
									URL ulrn4 = new URL(CompanyLogo.replace(" ", "%20"));
							        URLConnection urlConn4 = ulrn4.openConnection();
							        InputStream in4 = urlConn4.getInputStream();
							        int c4;
							        byte[] b4 = new byte[1024];							        
							        String FILENAME4 = "CompanyLogo"+Inspector_id+".jpg";System.out.println("FILENAME4 "+FILENAME4);
									FileOutputStream fos4 = openFileOutput(FILENAME4, Context.MODE_WORLD_READABLE);
							        
							        while ((c4 = in4.read(b4)) != -1)
							        {
							        	fos4.write(b4, 0, c4);
							        }							        
							    }
								catch (Exception e) {
									// TODO: handle exception
									System.out.println("Company Logo"+e.getMessage());
								}
								
								
							try
							{
								Cursor cur = db.SelectTablefunction(db.inspectorlogin," where Ins_Id = '" + Inspector_id+ "'");
								System.out.println("Inspector_id"+Inspector_id+"count="+cur.getCount());
								if(cur.getCount()==0)
								{
								
									
								db.wind_db.execSQL("INSERT INTO "
										+ db.inspectorlogin
										+ " (Ins_Id,Ins_FirstName,Ins_MiddleName,Ins_LastName,Ins_Address1,Ins_Address2,Ins_City,Ins_State,Ins_County," +
										"Ins_Zip,Ins_Phone,Ins_Email,Ins_CompanyName,Ins_CompanyId,Ins_Company_desc,Ins_Company_Logo,Ins_CompanyPhone,Ins_CompanyEmail,Ins_UserName,Ins_Password," +
										"Ins_Flag1,Android_status,Ins_Headshot,Ins_Signature,Ins_Initials," +
										"Ins_rememberpwd,LoginStatus,Fld_licencetype,Fld_licenceno,Fld_licenceexpirydate)"
										+ " VALUES ('" + Inspector_id + "'," + 
										"'"+db.encode(Insp_fname) + "','"+db.encode(Insp_mname)+"','"+db.encode(Insp_lname)+"',"+
										"'"+db.encode(Insp_address1) + "','"+ db.encode(Insp_address2) + "','"+ db.encode(Insp_city) + "',"+
										"'"+db.encode(Insp_state) + "','"+ db.encode(Insp_county) + "','"+Insp_zip+ "',"+
										"'"+db.encode(Insp_contphone) + "','"+db.encode(Insp_email)+"','"+db.encode(Insp_companyname)+"','"+Insp_companyid+"'," +
										"'','"+db.encode(CompanyLogo)+"','"+db.encode(CompanyPhone)+"','"+db.encode(CompanyEmail)+"','"+db.encode(Insp_username)+"','"+db.encode(Insp_password)+"'," +
										"'"+Ins_Flag+"','"+androidstatus+"','"+Insp_headshot+"','" +db.encode(Insp_signature) + "',"+
										"'"+db.encode(Insp_initials)+"','"+0+"','"+LoginStatus+"',"+
										"'"+db.encode(Insp_primlictype)+"','"+db.encode(Insp_primlicno)+"','"+db.encode(Insp_licexpirydtae)+"')");
								}
								else
								{
									System.out.println(" UPDATE "+db.inspectorlogin+" SET Ins_FirstName='"+db.encode(Insp_fname) + "'," +
											"Ins_MiddleName='"+db.encode(Insp_mname)+"',Ins_LastName='"+db.encode(Insp_lname)+"',Ins_Address1='"+db.encode(Insp_address1)+"'," +
											"Ins_Address2='"+db.encode(Insp_address2)+"',Ins_City='"+db.encode(Insp_city)+"',Ins_State='"+db.encode(Insp_state)+"'," +
											"Ins_County='"+db.encode(Insp_county)+"',Ins_Zip='"+Insp_zip+"',Ins_Phone='"+db.encode(Insp_contphone)+"'," +
											"Ins_Email='"+db.encode(Insp_email)+"',Ins_CompanyName='"+db.encode(Insp_companyname)+"',Ins_CompanyPhone='"+db.encode(CompanyPhone)+"'," +
											"Ins_Company_Logo='"+db.encode(CompanyLogo)+"',Ins_CompanyEmail='"+db.encode(CompanyEmail)+"',Ins_Flag1='"+Ins_Flag+"'," +
											"Ins_Headshot='"+Insp_headshot+"',Ins_Signature='"+db.encode(Insp_signature)+"',Ins_Initials='"+db.encode(Insp_initials)+"'," +
											"Fld_licencetype='"+db.encode(Insp_primlictype)+"',Fld_licenceno='"+db.encode(Insp_primlicno)+"',Fld_licenceexpirydate='"+db.encode(Insp_licexpirydtae)+"' where Ins_Id='"+Inspector_id+"'");
									db.wind_db.execSQL(" UPDATE "+db.inspectorlogin+" SET Ins_FirstName='"+db.encode(Insp_fname) + "'," +
											"Ins_MiddleName='"+db.encode(Insp_mname)+"',Ins_LastName='"+db.encode(Insp_lname)+"',Ins_Address1='"+db.encode(Insp_address1)+"'," +
											"Ins_Address2='"+db.encode(Insp_address2)+"',Ins_City='"+db.encode(Insp_city)+"',Ins_State='"+db.encode(Insp_state)+"'," +
											"Ins_County='"+db.encode(Insp_county)+"',Ins_Zip='"+Insp_zip+"',Ins_Phone='"+db.encode(Insp_contphone)+"'," +
											"Ins_Email='"+db.encode(Insp_email)+"',Ins_CompanyName='"+db.encode(Insp_companyname)+"',Ins_CompanyPhone='"+db.encode(CompanyPhone)+"'," +
											"Ins_Company_Logo='"+db.encode(CompanyLogo)+"',Ins_CompanyEmail='"+db.encode(CompanyEmail)+"',Ins_Flag1='"+Ins_Flag+"'," +
											"Ins_Headshot='"+Insp_headshot+"',Ins_Signature='"+db.encode(Insp_signature)+"',Ins_Initials='"+db.encode(Insp_initials)+"'," +
											"Fld_licencetype='"+db.encode(Insp_primlictype)+"',Fld_licenceno='"+db.encode(Insp_primlicno)+"',Fld_licenceexpirydate='"+db.encode(Insp_licexpirydtae)+"' where Ins_Id='"+Inspector_id+"'");
								}
								
								
								show_handler = 1;
								handler.sendEmptyMessage(0);
								
							}catch (Exception e) {
								// TODO: handle exception
								System.out.println("eeee"+e.getMessage());
							}
							
						}
						else
						{  
							status_userid = String.valueOf(result.getProperty("Errormsg"));
							show_handler = 2;
							handler.sendEmptyMessage(0);
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						System.out.println("show_handler"+show_handler);
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 2) {
							show_handler = 0;
							cf.ShowToast(status_userid);
						}else if (show_handler == 1) {
							show_handler = 0;
							insertcaption();
							
						}
					}
				};

			}.start();
		} else {
			cf.ShowToast("Internet connection not available");
	
		}

	}

	private void insertcaption() {
		// TODO Auto-generated method stub
			try
			{
				//cf.db.execSQL("Delete from "+cf.tblphotocaption);
				db.CreateTable(25);
				db.getInspectorId();
				Cursor c1 = db.wind_db.rawQuery("SELECT * FROM " + db.PhotoCaption +" Where IM_C_InspectorId='"+db.Insp_id+"'", null);System.out.println("PhotoCaption"+c1.getCount());
				if(c1.getCount()==0)			
				{
					String source = "<b><font color=#00FF33>Processing...Please wait...</font></b>";
					pd1 = ProgressDialog.show(WindMitInspection.this, "", Html.fromHtml(source), true);
			        new Thread(new Runnable() {
			                public void run() {
			                	System.out.println("rrrun");		       
			                	insertelevation();
			                	finishedHandler.sendEmptyMessage(0);		                
			                }
			            }).start();
				       
				}
				else
				{
					cf.gohome();
				}
			}
			catch(Exception e)
			{
				System.out.println(" EEEE "+e.getMessage());
			}		                          
	}
	private Handler finishedHandler = new Handler() {
	    @Override 
	    public void handleMessage(Message msg) {
	        pd1.dismiss();
	        cf.gohome();
	    }
	};
	public void insertelevation()
	{
		// TODO Auto-generated method stub
		db.getInspectorId();
		System.out.println("Insp_id"+db.Insp_id);
		int m=2,f=0,l=0;String photocaption ="";
		try
		{	
			Cursor c2 = db.wind_db.rawQuery("SELECT * FROM " + db.PhotoCaption , null);
			if(c2.getCount()==0)
			{
				for(int i=0;i<499;i++) // To insert first 500 records using compound select method .
				{
					String substr=arrelev.frontelevation1[i].substring(5,arrelev.frontelevation1[i].length());
					if(substr.equals("--Select--") || substr.equals("ADD PHOTO CAPTION") || substr.equals("FRONT ELEVATION") || substr.equals("FE-1 FRONT ELEVATION")|| substr.equals("BACK ELEVATION") || substr.equals("BE-1 BACK ELEVATION")
								|| substr.equals("LEFT ELEVATION") || substr.equals("LE-1 LEFT ELEVATION") || substr.equals("RIGHT ELEVATION") || substr.equals("RE-1 RIGHT ELEVATION") || substr.equals("ATTIC ELEVATION") || substr.equals("AE-1 ATTIC ELEVATION"))
					{					
						photocaption = db.encode(substr);
					}
					else
					{
						photocaption =  db.encode(arrelev.frontelevation1[i]);
					}
					elevtype = arrelev.frontelevation1[i].substring(0, 2);
					elevid = arrelev.frontelevation1[i].substring(3,4);	
					
					bulkinsertstring += " UNION SELECT '"+m+"','"+db.Insp_id+"', '"+photocaption+"','" + elevid + "','"+elevtype+"'";
					m++;
				}	
				db.wind_db.execSQL("INSERT INTO "+db.PhotoCaption
					+ " SELECT '1' as IM_C_Id,  '"+db.Insp_id+"' as IM_C_InspectorId,'' as  IM_C_caption,'' as IM_C_Elevation,'' as IM_C_Elevation_Name" +
					bulkinsertstring); 
				bulkinsertstring="";
				for(int j=500;j<arrelev.frontelevation1.length;j++) // You can use only 500 SELECT statements in Compound select method. so we have splitted it.
				{
					f=j+2;
					String substr1=arrelev.frontelevation1[j].substring(5,arrelev.frontelevation1[j].length());
					if(substr1.equals("--Select--") || substr1.equals("ADD PHOTO CAPTION") || substr1.equals("ATTIC ELEVATION"))
					{
						photocaption = db.encode(substr1);
					}
					else
					{
						photocaption = db.encode(arrelev.frontelevation1[j]);
					}
					
					elevtype = arrelev.frontelevation1[j].substring(0, 2);
					elevid = arrelev.frontelevation1[j].substring(3,4);
					bulkinsertstring += " UNION SELECT '"+f+"', '"+db.Insp_id+"','"+photocaption+"','" + elevid + "','"+elevtype+"'";					
				}
				db.wind_db.execSQL("INSERT INTO "+db.PhotoCaption
						+ " SELECT '501' as IM_C_Id, '"+db.Insp_id+"' as IM_C_InspectorId ,'--Select--' as  IM_C_caption,'2' as IM_C_Elevation, 'AE' as IM_C_Elevation_Name" +	bulkinsertstring);	
				bulkinsertstring="";
				
				              
				for(int p=0;p<20;p++) // For all Additional elevation we have the same caption to be inserted. So we have kept in seperate array and inserted it.
				{
					int h =p+1;
					for(int k=0;k<arrelev.addielev.length;k++)
					{
						l=f+2;
						elevtype = "Additional Photo";
						bulkinsertstring += " UNION SELECT '"+l+"', '"+db.Insp_id+"','"+db.decode(arrelev.addielev[k])+"','" + h + "','"+elevtype+"'";
						f++;
					}
					f=l;
				}
			
				int len = arrelev.frontelevation1.length+2;
				db.wind_db.execSQL("INSERT INTO "+db.PhotoCaption
					+ " SELECT '"+len+"' as IM_C_Id,  '"+db.Insp_id+"' as IM_C_InspectorId, '' as  IM_C_caption,'' as IM_C_Elevation,'' as IM_C_Elevation_Name" +
					bulkinsertstring);	
			}
			else
			{
				int t=0;
				c2.moveToFirst();
				Cursor rs = db.wind_db.rawQuery("SELECT MAX(IM_C_Id) as ID FROM "+db.PhotoCaption,null);
				rs.moveToFirst();
				String lastid = rs.getString(rs.getColumnIndex("ID"));
				
				
				for(int i=0;i<499;i++) // To insert first 500 records using compound select method .
				{
					int id =i+2+Integer.parseInt(lastid);
					String substr=arrelev.frontelevation1[i].substring(5,arrelev.frontelevation1[i].length());
					if(substr.equals("--Select--") || substr.equals("ADD PHOTO CAPTION") || substr.equals("FRONT ELEVATION") || substr.equals("FE-1 FRONT ELEVATION")|| substr.equals("BACK ELEVATION") || substr.equals("BE-1 BACK ELEVATION")
								|| substr.equals("LEFT ELEVATION") || substr.equals("LE-1 LEFT ELEVATION") || substr.equals("RIGHT ELEVATION") || substr.equals("RE-1 RIGHT ELEVATION") || substr.equals("ATTIC ELEVATION") || substr.equals("AE-1 ATTIC ELEVATION"))
					{					
						photocaption = db.encode(substr);
					}
					else
					{
						photocaption =  db.encode(arrelev.frontelevation1[i]);
					}
					elevtype = arrelev.frontelevation1[i].substring(0, 2);
					elevid = arrelev.frontelevation1[i].substring(3,4);	
					
					bulkinsertstring += " UNION SELECT '"+id+"','"+db.Insp_id+"', '"+photocaption+"','" + elevid + "','"+elevtype+"'";
					m++;
				}	

				int n = Integer.parseInt(lastid)+1;
				db.wind_db.execSQL("INSERT INTO "+db.PhotoCaption
					+ " SELECT '"+n+"' as IM_C_Id,  '"+db.Insp_id+"' as IM_C_InspectorId,'' as  IM_C_caption,'' as IM_C_Elevation,'' as IM_C_Elevation_Name" +
					bulkinsertstring); 
				bulkinsertstring="";
				
				Cursor rs1 = db.wind_db.rawQuery("SELECT MAX(IM_C_Id) as ID FROM "+db.PhotoCaption,null);
				rs1.moveToFirst();
				String lastid1 = rs1.getString(rs1.getColumnIndex("ID"));

				for(int j=500;j<arrelev.frontelevation1.length;j++) // You can use only 500 SELECT statements in Compound select method. so we have splitted it.
				{
					int id1 =t+2+Integer.parseInt(lastid1);
					String substr1=arrelev.frontelevation1[j].substring(5,arrelev.frontelevation1[j].length());
					if(substr1.equals("--Select--") || substr1.equals("ADD PHOTO CAPTION") || substr1.equals("ATTIC ELEVATION"))
					{
						photocaption = db.encode(substr1);
					}
					else
					{
						photocaption = db.encode(arrelev.frontelevation1[j]);
					}
					
					elevtype = arrelev.frontelevation1[j].substring(0, 2);
					elevid = arrelev.frontelevation1[j].substring(3,4);
					bulkinsertstring += " UNION SELECT '"+id1+"', '"+db.Insp_id+"','"+photocaption+"','" + elevid + "','"+elevtype+"'";		
					t++;
				}
				
				int lid = Integer.parseInt(lastid1)+1;System.out.println("lid="+lid);
				db.wind_db.execSQL("INSERT INTO "+db.PhotoCaption
						+ " SELECT '"+lid+"' as IM_C_Id, '"+db.Insp_id+"' as IM_C_InspectorId ,'--Select--' as  IM_C_caption,'2' as IM_C_Elevation, 'AE' as IM_C_Elevation_Name" +	bulkinsertstring);	
				bulkinsertstring="";
				
				Cursor rs2 = db.wind_db.rawQuery("SELECT MAX(IM_C_Id) as ID FROM "+db.PhotoCaption,null);
				rs2.moveToFirst();
				String lastid2 = rs2.getString(rs2.getColumnIndex("ID"));         
				for(int p=0;p<20;p++) // For all Additional elevation we have the same caption to be inserted. So we have kept in seperate array and inserted it.
				{
					int h =p+1;
					for(int k=0;k<arrelev.addielev.length;k++)
					{
						
						l =f+2+Integer.parseInt(lastid2);
						elevtype = "Additional Photo";
						bulkinsertstring += " UNION SELECT '"+l+"', '"+db.Insp_id+"','"+db.decode(arrelev.addielev[k])+"','" + h + "','"+elevtype+"'";
						f++;
					}
					f=l;
				}
			
				int lid1 = Integer.parseInt(lastid2)+1;				
				db.wind_db.execSQL("INSERT INTO "+db.PhotoCaption
					+ " SELECT '"+lid1+"' as IM_C_Id,  '"+db.Insp_id+"' as IM_C_InspectorId, '' as  IM_C_caption,'' as IM_C_Elevation,'' as IM_C_Elevation_Name" +
					bulkinsertstring);				
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception "+e.getMessage());
		}

		}
	private void Parse_Code()
	{
		//Parse code for receiving notifications
		
				Parse.initialize(this, "UCyqXofiVblmwpd40e3UaaADPju8iGgdbxrjKAmh", "NOCrdUbBFs12j7VG06wGmk7GOfYfcWZsKTrnzVqa");

				ParseAnalytics.trackAppOpened(getIntent());

				// inform the Parse Cloud that it is ready for notifications
				db.CreateTable(1);
				Cursor cur = db.wind_db.rawQuery("select * from " + db.inspectorlogin + " WHERE Ins_Flag1 ='1'", null);
				System.out.println("curparse"+cur.getCount());
				if (cur.getCount() >= 1) {
					PushService.setDefaultPushCallback(this, HomeScreen.class);
					//animationflag=true;
				}
				else
				{

					PushService.setDefaultPushCallback(this, WindMitInspection.class);System.out.println("finish");
				}
//				PushService.setDefaultPushCallback(this, HomeScreen.class);
				ParseInstallation.getCurrentInstallation().saveInBackground();
				
				//Parse code for receiving notifications
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}