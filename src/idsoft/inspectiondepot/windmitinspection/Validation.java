package idsoft.inspectiondepot.windmitinspection;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.R.integer;
import android.webkit.URLUtil;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class Validation {
CommonFunctions cf;
	public Validation(CommonFunctions cf) {
	// TODO Auto-generated constructor stub
		this.cf=cf;
	}
	
	public boolean validate(EditText ed,String title)
	{
		
		if(ed.getTag()!=null)
		{
			String validationstring=ed.getTag().toString().trim();
			if(validationstring.contains("required"))
			{
				if(ed.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the text for "+title);
					ed.requestFocus();
					return false;
				}
			
			}
			if(validationstring.contains("email"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  // TODO Auto-generated method stub
					  final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
					  Pattern pattern = Pattern.compile(EMAIL_PATTERN);
					  Matcher matcher = pattern.matcher(ed.getText().toString().trim());
					  if(!matcher.matches())
					  {
						  cf.ShowToast("Please enter valid "+title);
						  ed.requestFocus();
					    return false;
					  }
					
				}
			
			}
			if(validationstring.contains("number"))
			{
				try
				{
				if(!ed.getText().toString().trim().equals(""))
				{
					if(validationstring.contains("number_"))
					{
						String[] val=validationstring.split("number_");
						if(val[1]!=null)
						{
							String length[]=val[1].split(",");
							if(!length[0].equals(""))
							{
								int len= Integer.parseInt(length[0]);
								if(ed.length()!=len)
								{
									cf.ShowToast("Please enter valid "+title);
									ed.requestFocus();
									return false;	
								}
							}
						}
							
					}
					
					
					
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			if(validationstring.contains("phone_no"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(ed.getText().toString().length()!=13)
					  {
						  cf.ShowToast("Please enter valid  "+title);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			if(validationstring.contains("website"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(!URLUtil.isValidUrl(ed.getText().toString()))
					  {
						  cf.ShowToast("Please enter valid "+title);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			if(validationstring.contains("Date_past"))
			{
				if(checkfortodaysdate(ed.getText().toString().trim()))
				{
					return true;
				}
				else
				{
					 cf.ShowToast("Date should not be greater than todays date "+title);
					 ed.requestFocus();
					  return false;
				}
				
			}
			
			
		}
		return true;
	}
	public boolean validate_other(EditText ed,String title,String option_other)
	{
		
		if(ed.getTag()!=null)
		{
			String validationstring=ed.getTag().toString().trim();
			if(validationstring.contains("required"))
			{
				if(ed.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the Other text for "+title);
					ed.requestFocus();
					return false;
				}
			
			}
			if(validationstring.contains("email"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  // TODO Auto-generated method stub
					  final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
					  Pattern pattern = Pattern.compile(EMAIL_PATTERN);
					  Matcher matcher = pattern.matcher(ed.getText().toString().trim());
					  if(!matcher.matches())
					  {
						  cf.ShowToast("Please enter valid "+title);
						  ed.requestFocus();
					    return false;
					  }
					
				}
			
			}
			if(validationstring.contains("number"))
			{
				try
				{
				if(!ed.getText().toString().trim().equals(""))
				{
					if(validationstring.contains("number_"))
					{
						String[] val=validationstring.split("number_");
						if(val[1]!=null)
						{
							String length[]=val[1].split(",");
							if(!length[0].equals(""))
							{
								int len= Integer.parseInt(length[0]);
								if(ed.length()!=len)
								{
									cf.ShowToast("Please enter valid "+title);
									ed.requestFocus();
									return false;	
								}
							}
						}
							
					}
					
					
					
				}
				}catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			if(validationstring.contains("phone_no"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(ed.getText().toString().length()!=13)
					  {
						  cf.ShowToast("Please enter valid  "+title);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			if(validationstring.contains("website"))
			{
				
				if(!ed.getText().toString().trim().equals(""))
				{
					  if(!URLUtil.isValidUrl(ed.getText().toString()))
					  {
						  cf.ShowToast("Please enter valid "+title);
						  ed.requestFocus();
						  return false;
					  }
					
				}
			
			}
			
			
		}
		return true;
	}

	public boolean validate(Spinner sp,String title,String defaults) {
		// TODO Auto-generated method stub
		if(sp.getTag()!=null)
		{
			String validationstring=sp.getTag().toString().trim();
			System.out.println("validation string "+validationstring);
			if(validationstring.contains("required"))
			{
				if(sp.getSelectedItem().toString().equals(defaults))
				{
					 cf.ShowToast("Please select value for "+title);
					return false;
				}
			}
		}
		
		return true;
	}

	public boolean validate(RadioButton[] call_info_rd, String title) {
		// TODO Auto-generated method stub
		
		for (int i=0;i<call_info_rd.length;i++)
		{
			if(call_info_rd[i].isChecked()){
				
				return true;
			}
		}
		 cf.ShowToast("Please select value for "+title);
		return false;
	}

	public boolean validate(CheckBox[] check, String title) {
		// TODO Auto-generated method stub
		for (int i=0;i<check.length;i++)
		{
			if(check[i].isChecked()){
				
				return true;
			}
		}
		 cf.ShowToast("Please select value for "+title);
		 return false;
	}

	public boolean validate(RadioGroup radioGroup, String title) {
		// TODO Auto-generated method stub
		for (int i=0;i<radioGroup.getChildCount();i++)
		{
			if(((RadioButton)radioGroup.getChildAt(i)).isChecked()){
				
				return true;
			}
		}
		 cf.ShowToast("Please select value for "+title);
		 return false;
		
	}

	public boolean validate(TextView wdos_tv, String title, String defaults) {
		// TODO Auto-generated method stub
		if(wdos_tv.getTag().toString().contains("required"))
		{
			String s=wdos_tv.getText().toString().trim();
			s=s.replace(defaults, "");
			if(s.equals(""))
			{
				 cf.ShowToast("Please select value for "+title);
				 return false;
			}
		}
		return true;
	}

	public boolean checkfortodaysdate(String string) {
		// TODO Auto-generated method stub
		int i1 = string.indexOf("/");
		String result = string.substring(0, i1);
		int i2 = string.lastIndexOf("/");
		String result1 = string.substring(i1 + 1, i2);
		String result2 = string.substring(i2 + 1);
		result2 = result2.trim();
		int smonth= Integer.parseInt(result);
		int sdate = Integer.parseInt(result1);
		int syear = Integer.parseInt(result2);
	    cf.getCalender();
	    cf.mMonth = cf.mMonth + 1;
	    if ( syear  < cf.mYear || (smonth < cf.mMonth && syear  <= cf.mYear) || (smonth <= cf.mMonth && syear <=cf.mYear && sdate <= cf.mDay )) {
	    	return true;
		} else {
			return false;
		}
		
	}

}