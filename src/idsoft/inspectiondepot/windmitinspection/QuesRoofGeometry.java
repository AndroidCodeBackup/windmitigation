package idsoft.inspectiondepot.windmitinspection;

import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class QuesRoofGeometry extends Activity {
	ListView list;
	String inspectortypeid, othertext = "", helpcontent;
	private static final int visibility = 0;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	String commentsfill, roofgeovalueprev, roofgeolengthprev,
			roofgeototalareaprev, roofgeoothertextprev,totallength,totalroofarea;
	RadioButton rdioA, rdioB, rdioC;
	boolean load_comment = true;
	int viewimage = 1;
	String InspectionType, status, comm, homeId, rdiochk = "0", yearbuilt = "",
			permitdate = "", updatecnt, identity, roofgeometrycomment,
			commdescrip = "",tmp="";
	Intent iInspectionList;
	AlertDialog alertDialog;
	TextView prevmitidata, helptxt,roofgeometry_type;
	TextView  txroofgeometryheading;
	EditText txtother, comments, yrbuilt1, yrbuilt2, permitdate1, permitdate2,
			txtothersquarefeet, txtotherlength;
	int commentsch, optionid;
	CheckBox[] cb;
	TextWatcher watcher;
	Button saveclose;
	int value, Count;
	CheckBox temp_st;
	View v1;
	String conchkbox, descrip, comm2;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);

		db.CreateTable(7);
		db.CreateTable(8);
		db.CreateTable(9);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}
		setContentView(R.layout.roofgeometry);
		db.getInspectorId();
		db.getPHinformation(cf.Homeid);
		db.changeimage(cf.Homeid);
		cf.getDeviceDimensions();
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);
		layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(QuesRoofGeometry.this, 2, cf, 0));
		/** Questions submenu **/
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(QuesRoofGeometry.this, 25,cf, 1));
		
		df = new android.text.format.DateFormat();
		cd = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		md = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		
		txroofgeometryheading = (TextView) findViewById(R.id.txtroofgeometryheading);
		txroofgeometryheading
				.setText(Html
						.fromHtml("<font color=red> * "
								+ "</font>What is the roof shape? (Do not consider roofs of porches or carports that are attached only to the fascia or wall of the host structure over unenclosed space in the determination of roof perimeter or roof area for roof geometry classification). "));

		prevmitidata = (TextView) findViewById(R.id.txtOriginalData);
		
		helptxt = (TextView) findViewById(R.id.help);
		helptxt.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (rdiochk==null)
				 {
					cf.alertcontent = "To see help comments, please select roof geometry options.";
				}
				else if (rdiochk == "1" || rdioA.isChecked() == true) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection A, Hip Roof of the OIR B1 -1802 Question 5 Roof Shape";
				} else if (rdiochk == "2" || rdioB.isChecked() == true) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection B, Flat Roof of the OIR B1 -1802 Question 5 Roof Shape.";
				} else if (rdiochk == "3" || rdioC.isChecked() == true) {
					cf.alertcontent = "This home was verified as meeting the requirements of selection C, Non Hip, of the OIR B1 -1802 Question 5 Roof Shape.";
				} else {
					cf.alertcontent = "To see help comments, please select roof geometry options.";
				}

				
				cf.showhelp("HELP",cf.alertcontent);
			}
		});

		
		db.getQuesOriginal(cf.Homeid);
		if(!db.rgoriddata.equals(""))
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>" + db.rgoriddata+ "</font>"));
		}
		else
		{
			prevmitidata.setText(Html.fromHtml("<font color=blue>Original Value : "+ "</font>" + "<font color=red>Not Available</font>"));
		}

		this.rdioA = (RadioButton) this.findViewById(R.id.rdobtn1);
		this.rdioA.setOnClickListener(OnClickListener);
		this.rdioB = (RadioButton) this.findViewById(R.id.rdobtn2);
		this.rdioB.setOnClickListener(OnClickListener);
		this.rdioC = (RadioButton) this.findViewById(R.id.rdobtn3);
		this.rdioC.setOnClickListener(OnClickListener);
		
		yrbuilt1 = (EditText) this.findViewById(R.id.txtyrbuilt1);
		yrbuilt1.addTextChangedListener(new EditCustomTextWatcher(yrbuilt1));
		yrbuilt2 = (EditText) this.findViewById(R.id.txtpermitDate1);
		yrbuilt2.addTextChangedListener(new EditCustomTextWatcher(yrbuilt2));
		
		permitdate1 = (EditText) this.findViewById(R.id.txtyrbuilt2);
		permitdate1.addTextChangedListener(new EditCustomTextWatcher(permitdate1));
		permitdate2 = (EditText) this.findViewById(R.id.txtpermitDate2);
		permitdate2.addTextChangedListener(new EditCustomTextWatcher(permitdate2));
		
		this.txtother = (EditText) this.findViewById(R.id.txtOther);
		txtotherlength = (EditText) this.findViewById(R.id.txtotherLen);
		txtotherlength.addTextChangedListener(new EditCustomTextWatcher(txtotherlength));
		txtothersquarefeet = (EditText) this.findViewById(R.id.txtotherSqfeet);
		txtothersquarefeet.addTextChangedListener(new EditCustomTextWatcher(txtothersquarefeet));
	
		comments = (EditText) this.findViewById(R.id.txtcomments);
		roofgeometry_type=(TextView) findViewById(R.id.SH_TV_ED);
		comments.addTextChangedListener(new TextWatchLimit(comments,500,roofgeometry_type));
		
        setdata();
      
	}
	private void setdata() {
		// TODO Auto-generated method stub
		try {
			Cursor cur = db.wind_db.rawQuery("select * from "
					+ db.Questions + " where SRID='" + cf.Homeid
					+ "'", null);
			if(cur.getCount()>0){
			cur.moveToFirst();
			if (cur != null) {
				roofgeovalueprev = cur.getString(cur
						.getColumnIndex("RoofGeoValue"));
				roofgeolengthprev = cur.getString(cur
						.getColumnIndex("RoofGeoLength"));
				roofgeototalareaprev = cur.getString(cur
						.getColumnIndex("RoofGeoTotalArea"));
				roofgeoothertextprev = db.decode(cur.getString(cur
						.getColumnIndex("RoofGeoOtherText")));

				if (roofgeovalueprev.equals("1")) {
					rdioA.setChecked(true);
					yrbuilt1.setEnabled(true);
					yrbuilt2.setEnabled(true);
					permitdate1.setEnabled(false);
					permitdate2.setEnabled(false);
					txtother.setEnabled(false);
					txtotherlength.setEnabled(false);
					txtothersquarefeet.setEnabled(false);

					yrbuilt1.setText(roofgeolengthprev);
					yrbuilt2.setText(roofgeototalareaprev);
					rdiochk = "1";
				} else if (roofgeovalueprev.equals("2")) {
					rdioB.setChecked(true);
					yrbuilt1.setEnabled(false);
					yrbuilt2.setEnabled(false);
					permitdate1.setEnabled(true);
					permitdate2.setEnabled(true);
					txtother.setEnabled(false);
					txtotherlength.setEnabled(false);
					txtothersquarefeet.setEnabled(false);
					permitdate1.setText(roofgeolengthprev);
					permitdate2.setText(roofgeototalareaprev);
					rdiochk = "2";
				} else if (roofgeovalueprev.equals("3")) {
					rdioC.setChecked(true);
					yrbuilt1.setEnabled(false);
					yrbuilt2.setEnabled(false);
					permitdate1.setEnabled(false);
					permitdate2.setEnabled(false);
					txtother.setEnabled(true);
					txtotherlength.setEnabled(true);
					txtothersquarefeet.setEnabled(true);
					txtother.setText(roofgeoothertextprev);
					txtotherlength.setText(roofgeolengthprev);
					txtothersquarefeet.setText(roofgeototalareaprev);
					rdiochk = "3";
				} else {
				}

			}
			}
		} catch (Exception e) {
	
		}

		try {
			Cursor cur = db.wind_db.rawQuery("select * from " + db.QuestionsComments
					+ " where SRID='" + cf.Homeid + "'", null);
			if(cur.getCount()>0){
			cur.moveToFirst();
				if (cur != null) {
					roofgeometrycomment = db.decode(cur.getString(cur
							.getColumnIndex("RoofGeometryComment")));
					comments.setText(roofgeometrycomment);
				}
			}
		} catch (Exception e) {
	
		}
	}
	
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.loadcomments:
			/***Call for the comments***/
			RG_suboption();System.out.println("test"+tmp);
			int len=((EditText)findViewById(R.id.txtcomments)).getText().toString().length();System.out.println("len="+len);			
			if(!tmp.equals(""))
			{
				if(load_comment)
				{
					load_comment=false;
					int loc1[] = new int[2];
					v.getLocationOnScreen(loc1);
					Intent in = cf.loadComments(tmp,loc1);
					in.putExtra("insp_ques", "5");
					in.putExtra("length", len);
					in.putExtra("max_length", 500);
					startActivityForResult(in, cf.loadcomment_code);
					
				}
			}
			else
			{
				cf.ShowToast("Please select the option for Roof Geometry");	
			}			
			break;
		case R.id.txthelpcontentoptionA:
			cf.alerttitle="A - Hip Roof";
		    cf.alertcontent="Hip roof: hip roof with no other roof shapes greater than 10 percent of the total roof system perimeter Total length of non-hip features: ____________ feet; total roof system perimeter: ____________ feet";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionB:
			cf.alerttitle="B - Flat Roof";
		    cf.alertcontent="Flat roof: roof on a building with five or more units where at least 90 percent of the main roof area has a roof slope of less than 2:12. Roof area with slope less than 2:12 ____________ sq. ft.; total roof area ____________ sq. ft.";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		case R.id.txthelpcontentoptionC:
			cf.alerttitle="C - Other Roof Type";
		    cf.alertcontent="Other roof: any roof that does not qualify as either A or B above";
		    cf.showhelp(cf.alerttitle,cf.alertcontent);
			break;
		
		
		
		case R.id.hme:
			cf.gohome();
			break;

	
		case R.id.savenext:
			int optionval=0;
			boolean chkstatus=true;			
			if(rdioA.isChecked() || rdioB.isChecked() || rdioC.isChecked())
			{
				if (rdiochk.equals("1") || rdioA.isChecked()) 
				{
					totallength = ((EditText) findViewById(R.id.txtyrbuilt1)).getText().toString();
					totalroofarea =((EditText) findViewById(R.id.txtpermitDate1)).getText().toString();
					optionval = 1;
				} 
				else if (rdiochk.equals("2")  || rdioB.isChecked()) 
				{
					totallength = ((EditText) findViewById(R.id.txtyrbuilt2)).getText().toString();
					totalroofarea =((EditText) findViewById(R.id.txtpermitDate2)).getText().toString();
					optionval = 2;
				}
				else if (rdiochk.equals("3")  || rdioC.isChecked()) 
				{
					totallength = ((EditText) findViewById(R.id.txtotherLen)).getText().toString();
					totalroofarea =((EditText) findViewById(R.id.txtotherSqfeet)).getText().toString();
					optionval = 3;			
				}						
				
				if (rdiochk.equals("1") || rdioA.isChecked()) 
				{
					if (totallength.trim().equals(""))
					{
						cf.ShowToast("Please enter the Total Length of non-hip features.\n"+"Condition : Total non-hip should not be greater than 10% of Total roof system perimeter");						
						cf.setFocus(((EditText) findViewById(R.id.txtyrbuilt1)));
					}
					else if (validation(totallength, totalroofarea, optionval) != "true") 
					{
							cf.ShowToast("Total non-hip should not be greater than 10% of Total roof system perimeter");
							cf.setFocus(((EditText) findViewById(R.id.txtyrbuilt1)));						 
					}
					else
					{
						insertroofgeometry();
					}					
				}
				else if (rdiochk.equals("2") || rdioB.isChecked()) 
				{
					if (totallength.trim().equals(""))
					{
						cf.ShowToast("Please enter Roof Area for Flat Roof. \n"+"Please enter roof area with slope 2:12 or should be less than 90% of Total sq.ft");		
						cf.setFocus(((EditText) findViewById(R.id.txtyrbuilt2)));	
					}
					else if (totalroofarea.trim().equals(""))
					{
						cf.ShowToast("Please enter Total roof area for Flat Roof");		
						cf.setFocus(((EditText) findViewById(R.id.txtpermitDate2)));	
					}
					else if (validation(totallength, totalroofarea, optionval) != "true") 
					{
						cf.ShowToast("Please enter roof area with slope 2:12 or should be less than 90% of Total sq.ft");
						cf.setFocus(((EditText) findViewById(R.id.txtyrbuilt2)));
					}
					else
					{
						insertroofgeometry();
					}
				}
				else if (rdiochk.equals("3") || rdioC.isChecked()) 
				{
					/*if(((EditText) findViewById(R.id.edoptioCothertext)).getText().toString().trim().equals(""))
					{
						cf.ShowToast("Please enter the other text for Other Roof.",0);
						((EditText)findViewById(R.id.edoptioCothertext)).setEnabled(true);
						cf.setFocus(((EditText) findViewById(R.id.edoptioCothertext)));	
					}
					else
					{*/
						insertroofgeometry();
				//	}
				}
			}
			else
			{
				cf.ShowToast("Please select the Roof Geometry Option");
			}				
			break;

		}
	}
	private void insertroofgeometry()
	{
		if (comments.getText().toString().trim().equals("")) 
		{
			cf.ShowToast("Please enter the Comments for Roof Geometry");
			
		}		
		else
		{
			try {
				Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
						+ db.Questions + " WHERE SRID='"
						+ cf.Homeid + "'", null);
				if (c2.getCount() == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.Questions
							+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
							+ "VALUES ('" + cf.Homeid + "','','','" + 0
							+ "','" + 0 + "','" + 0 + "','','','','','" + 0
							+ "','" + 0 + "','','" + 0 + "','" + 0 + "','"
							+ 0 + "','" + 0 + "','" + 0 + "','','"
							+ rdiochk + "','" + totallength + "','"
							+ totalroofarea + "','"
							+ db.encode(((EditText) findViewById(R.id.txtOther)).getText().toString()) + "','" + 0
							+ "','','" + 0 + "','" + 0
							+ "','','','','','','','','" + 0 + "','" + 0
							+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
							+ "','" + 0 + "','" + 0 + "','','" + 0 + "','"
							+ 0 + "','','" + 0 + "','','','" + cd + "','"
							+ 0 + "')");

				} else {
					System.out.println("emter");
					db.wind_db.execSQL("UPDATE " + db.Questions
							+ " SET RoofGeoValue='" + rdiochk
							+ "',RoofGeoLength ='" + totallength
							+ "',RoofGeoTotalArea='" + totalroofarea
							+ "',RoofGeoOtherText='"
							+ db.encode(((EditText) findViewById(R.id.txtOther)).getText().toString()) + "'"
							+ " WHERE SRID ='" + cf.Homeid.toString() + "'");
				}
			

			Cursor c3 = db.wind_db.rawQuery("SELECT * FROM " + db.QuestionsComments
					+ " WHERE SRID='" + cf.Homeid + "'", null);
				if (c3.getCount() == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.QuestionsComments
							+ " (SRID,i_InspectionTypeID,BuildingCodeComment,RoofCoverComment,RoofDeckComment,RoofWallComment,RoofGeometryComment,SecondaryWaterComment,OpeningProtectionComment,WallConstructionComment,InsOverAllComments,CreatedOn)"
							+ " VALUES ('"+ cf.Homeid+ "','"+ cf.InspectionType+ "','','','','','"+ db.encode(comments.getText().toString())+ "','','','','','"+ cf.datewithtime + "')");
			
					

				} else {
					db.wind_db.execSQL("UPDATE " + db.QuestionsComments
							+ " SET RoofGeometryComment='"
							+ db.encode(comments.getText().toString())
							+ "',CreatedOn ='"
							+ md + "'" + " WHERE SRID ='"
							+ cf.Homeid.toString() + "'");
					
				}
			

			
				Cursor c4 = db.wind_db.rawQuery("SELECT * FROM "
						+ db.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
						+ "'", null);
				int subchkrws = c4.getCount();
				if (subchkrws == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.SubmitCheckTable
							+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
							+ "VALUES ('" + db.Insp_id + "','"
							+ cf.Homeid
							+ "',0,0,0,0,0,1,0,0,0,0,0,0,0,0,0)");
				} else {
					db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
							+ " SET fld_roofgeo='1' WHERE Srid ='"
							+ cf.Homeid + "' and InspectorId='"
							+ db.Insp_id + "'");
				

				cf.ShowToast("Roof Geometry details has been saved successfully");
				iInspectionList = new Intent(QuesRoofGeometry.this, QuesSWR.class);
				iInspectionList.putExtra("homeid", cf.Homeid);
				iInspectionList.putExtra("status", cf.status);
				startActivity(iInspectionList);
				finish();
			}
			}
			catch (Exception e) {
				// TODO: handle exception
				cf.ShowToast("Problem in saving Roof geonetry values");
				
			}
		}
	}


	private void RG_suboption() {
		// TODO Auto-generated method stub
		if(rdioA.isChecked())	{		tmp="1";}
		else if(rdioB.isChecked()) {	tmp="2";}
		else if(rdioC.isChecked()) {	tmp="3";}
		else{tmp="";}
	}
	private String validation(String yearbuilt2, String permitdate3, int option1) {
		// TODO Auto-generated method stub
		String chk = "false";
		try {
			if (!permitdate3.equals("")) {
				if (option1 == 1) {
					int perimeter = Integer.parseInt(permitdate3);
					int yrbt = Integer.parseInt(yearbuilt2);
					perimeter = perimeter / 10;
					if (yrbt <= perimeter) {
						chk = "true";

					} else {
						chk = "false";

					}
				} else if (option1 == 2) {
					float perimeter = Float.parseFloat(permitdate3);
					float yrbt = Float.parseFloat(yearbuilt2);
					Double temp = ((89.00 / 100.00) * perimeter);

					if (temp >= yrbt) {
						chk = "true";
					} else {
						chk = "false";
					}
				} else {
					chk = "true";
				}

			} else {
				chk = "true";
			}
		} catch (Exception e) {
			//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ RoofGeometry.this +" problem in datatype conversion types on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);

			chk = "false";
		}
		return chk;
	}

	RadioButton.OnClickListener OnClickListener = new RadioButton.OnClickListener() {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.rdobtn1:
				yrbuilt1.setEnabled(true);
				yrbuilt2.setEnabled(true);
				permitdate1.setEnabled(false);
				permitdate2.setEnabled(false);
				txtother.setEnabled(false);
				txtotherlength.setEnabled(false);
				txtothersquarefeet.setEnabled(false);
				commentsfill = "This home was verified as meeting the requirements of selection A, �Hip Roof� of the OIR B1 -1802 Question 5 Roof Shape.";
				comments.setText(commentsfill);
				rdiochk = "1";
				permitdate1.setText(null);
				yrbuilt1.requestFocus();
				permitdate2.setText(null);
				txtothersquarefeet.setText(null);
				txtotherlength.setText(null);
				txtother.setText(null);
				rdioA.setChecked(true);
				rdioB.setChecked(false);
				rdioC.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdobtn2:
				permitdate1.setEnabled(true);
				permitdate2.setEnabled(true);
				yrbuilt1.setEnabled(false);
				yrbuilt2.setEnabled(false);
				txtother.setEnabled(false);
				txtotherlength.setEnabled(false);
				txtothersquarefeet.setEnabled(false);
				commentsfill = "This home was verified as meeting the requirements of selection B, �Flat Roof� of the OIR B1 -1802 Question 5 Roof Shape.";
				comments.setText(commentsfill);
				rdiochk = "2";
				yrbuilt2.setText(null);
				permitdate1.requestFocus();
				yrbuilt1.setText(null);
				txtothersquarefeet.setText(null);
				txtotherlength.setText(null);
				txtother.setText(null);
				rdioA.setChecked(false);
				rdioB.setChecked(true);
				rdioC.setChecked(false);
				viewimage = 1;
				break;

			case R.id.rdobtn3:
				txtother.setEnabled(true);
				txtotherlength.setEnabled(true);
				txtothersquarefeet.setEnabled(true);
				permitdate1.setEnabled(false);
				permitdate2.setEnabled(false);
				yrbuilt1.setEnabled(false);
				yrbuilt2.setEnabled(false);
				commentsfill = "This home was verified as meeting the requirements of selection C, Non Hip, of the OIR B1 -1802 Question 5 Roof Shape.";
				comments.setText(commentsfill);
				permitdate1.setText(null);
				txtotherlength.requestFocus();
				permitdate2.setText(null);
				yrbuilt2.setText(null);
				yrbuilt1.setText(null);
				rdiochk = "3";
				rdioA.setChecked(false);
				rdioB.setChecked(false);
				rdioC.setChecked(true);
				viewimage = 1;
				break;

			}
		}
	};

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(QuesRoofGeometry.this, QuesRoofWall.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*switch (resultCode) {
	
			case 0:
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();System.out.println("case -DATA ");
					String capturedImageFilePath = cursor.getString(column_index_data);
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
					
				
				}
				
				break;

		}
		*/
		if(requestCode==cf.loadcomment_code)
		{
			load_comment=true;
			if(resultCode==RESULT_OK)
			{
				String bccomments = ((EditText)findViewById(R.id.txtcomments)).getText().toString();
				((EditText)findViewById(R.id.txtcomments)).setText(bccomments +" "+data.getExtras().getString("Comments"));	 
			}
		}

	}
	
}
