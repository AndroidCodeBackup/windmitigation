package idsoft.inspectiondepot.windmitinspection;


import android.app.Activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WeblinkOpen extends Activity {
	private static final String TAG = null;
	WebView webview1;
	int value, Count;
	Intent intimg;
	String InspectionType, status, homeId, identity, weburl, Page;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			weburl = bunhomeId.getString("weburl");
			homeId = bunhomeId.getString("homeid");
			status = bunhomeId.getString("status");
			Page = bunhomeId.getString("page");

		}

		setContentView(R.layout.weblink);

		webview1 = (WebView) findViewById(R.id.webview);
		webview1.getSettings().setJavaScriptEnabled(true);
		webview1.loadUrl(weburl);

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {;
System.out.println("Page= "+Page);
			if (Page.equals("roof")) {
				intimg = new Intent(WeblinkOpen.this, QuesRoofCover.class);
			} else if (Page.equals("open")) {
				intimg = new Intent(WeblinkOpen.this, QuesOpenProt.class);
			}
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", homeId);
			intimg.putExtra("status", status);
			startActivity(intimg);System.out.println("Pageends= ");
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
