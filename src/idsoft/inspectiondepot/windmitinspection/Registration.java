package idsoft.inspectiondepot.windmitinspection;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.Html;
import android.text.InputFilter;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;


public class Registration extends Activity
{
	private static final int SELECT_PICTURE = 0;
	int t,show_handler,iden,u;
	int registerflag,k;
	String arr[];
	Dialog dialog; 
	int zip=0;
	byte[] raw1,raw2,raw3,signsignature,initialsignature;
	boolean countysetselection=false,signbool=false,initialsbool=false,compphotoavail=true,headshotphotoavail=true,initphotoavail=true,signphotoavail=true;
	CommonFunctions cf;
	DatabaseFunctions db;
	int Insp_Id;
	RadioButton rg1[];
	WebserviceFunctions wb;
	String citystateval="";
	static ArrayList<String> str_arrlst = new ArrayList<String>();
	 RadioButton[] rd;RadioButton rg[];
	EditText reg_et_firstname,reg_et_lastname,reg_et_middlename,reg_et_address1,reg_et_address2,reg_et_licenseno,
	reg_et_licenseother;
	Spinner primlicentype,spinnerstate,spinnercounty;
	EditText edcounty,edstate;
	String strzip="";
	DataBaseHelper1 dbh1;
	String primarylictype[] = {"--Select--", 
			"Licensed Roofing Contractor","Licensed Building Contractor","Licensed General Contractor","Licensed Residential Contractor","Licensed Electrician","Licensed Plumber","Licensed Architect","Professional Engineer","Certified ASHI Inspector","ASHI Candidate","Certified NACHI Inspector","CREIA","FABI RPI","HVAC License","State Licensed Home Inspector","Building Code Official License","Licensed Loss Adjuster","Chartered Building Surveyor","Other"};
	String strlicid,state,city,stateid,countyid,strcountyid,strcounty,zipidentifier,county,strstate,
	       strstateid,statevalue = "false",loadlicensevalue="false",Ins_Flag="1",androidstatus="0",
	       primlicvalue="",selectedImagePath1="",selectedImagePath2="",selectedImagePath3="",
	       picname1="",picname2="",picname3="",imageidentifier,selectedImagePath,selectedImagePathcomplogo,status_userid;
	ArrayAdapter primlicentypeadap,stateadapter,countyadapter;
	String [] primarylicid,arraystateid, arraystatename,arraycountyid, arraycountyname;
	GraphicsView signview,initialsview;
	ScrollView parentscr;
	public Uri CapturedImageURI;
	

	public void onCreate(Bundle SavedInstanceState) 
	{
		super.onCreate(SavedInstanceState);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			Insp_Id =  bunhomeId.getInt("inspid");
		}
		
		
		setContentView(R.layout.register);
		parentscr = (ScrollView)findViewById(R.id.parent);
		
		if(Insp_Id==0)
		{
			((Button) findViewById(R.id.home)).setVisibility(cf.v1.GONE);
		}
		else
		{
			((Button) findViewById(R.id.home)).setVisibility(cf.v1.VISIBLE);
		}
				
		signview=(GraphicsView) findViewById(R.id.signaturetxt);		
		signview.setDrawingCacheEnabled(true);
		
        signview.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				parentscr.requestDisallowInterceptTouchEvent(true);
				signbool = true;
				return false;
			}
		});
        
        initialsview=(GraphicsView) findViewById(R.id.initialstxt);		
        initialsview.setDrawingCacheEnabled(true);
		
        initialsview.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				parentscr.requestDisallowInterceptTouchEvent(true);
				initialsbool = true;
				return false;
			}
		});
		
		//statevalue = LoadState();
		/*spinnerstate = ((Spinner)findViewById(R.id.inspstate));
		spinnercounty = ((Spinner)findViewById(R.id.inspcounty));
		if (statevalue == "true") 
		{
			stateadapter = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_item, arraystatename);
			stateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);
		}
		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				int stateid = spinnerstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(strstateid);
					spinnercounty.setEnabled(true);
					
				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							Registration.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				int countyid = spinnercounty.getSelectedItemPosition();
				strcountyid = arraycountyid[countyid];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});*/
		
		((EditText)findViewById(R.id.inspphone)).addTextChangedListener(new CustomTextWatcher(((EditText)findViewById(R.id.inspphone))));
		((EditText)findViewById(R.id.compphone)).addTextChangedListener(new CustomTextWatcher(((EditText)findViewById(R.id.compphone))));
		/*((EditText)findViewById(R.id.inspzip)).addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(((EditText)findViewById(R.id.inspzip)).getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					zipidentifier="zip1";
					Load_State_County_City(((EditText)findViewById(R.id.inspzip)));
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});*/
		// ((EditText)findViewById(R.id.inspzip)).addTextChangedListener(new ECustomTextWatcher(((EditText)findViewById(R.id.inspzip))));
		
		reg_et_firstname = ((EditText)findViewById(R.id.inspfirstname));
		reg_et_firstname.addTextChangedListener(new EditCustomTextWatcher(reg_et_firstname));
		reg_et_lastname = ((EditText)findViewById(R.id.insplastname));
		reg_et_lastname.addTextChangedListener(new EditCustomTextWatcher(reg_et_lastname));
		reg_et_middlename = ((EditText)findViewById(R.id.inspmiddlename));
		reg_et_middlename.addTextChangedListener(new EditCustomTextWatcher(reg_et_middlename));
		reg_et_address1 = ((EditText)findViewById(R.id.inspadd1));
		reg_et_address1.addTextChangedListener(new EditCustomTextWatcher(reg_et_address1));
		reg_et_address2 = ((EditText)findViewById(R.id.inspadd2));
		reg_et_address2.addTextChangedListener(new EditCustomTextWatcher(reg_et_address2));
		reg_et_licenseno = ((EditText)findViewById(R.id.licenseno));
		reg_et_licenseno.addTextChangedListener(new EditCustomTextWatcher(reg_et_licenseno));
		reg_et_licenseother = ((EditText)findViewById(R.id.licenseother));
		reg_et_licenseother.addTextChangedListener(new EditCustomTextWatcher(reg_et_licenseother));
		primlicentype=(Spinner) findViewById(R.id.licensetype);
		//LoadLicensetype();
		
		primlicentypeadap = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_item,primarylictype);
		primlicentypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		primlicentype.setAdapter(primlicentypeadap);
		
		
		primlicentype.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				primlicvalue = primlicentype.getSelectedItem().toString();
				
				if(primlicvalue.equals("Other"))
				{
					((EditText)findViewById(R.id.licenseother)).setVisibility(View.VISIBLE);
				}
				else
				{
					((EditText)findViewById(R.id.licenseother)).setText("");
					((EditText)findViewById(R.id.licenseother)).setVisibility(View.GONE);
				}
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub				
			}
		});		
		setvalue();
	}
	protected void setvalue() {
		// TODO Auto-generated method stub
		db.getInspectorId();
		Cursor cur = db.SelectTablefunction(db.inspectorlogin," where Ins_Id = '" + db.Insp_id+ "'");
		if(cur.getCount()>0)
		{
			cur.moveToFirst();
			 ((EditText)findViewById(R.id.inspfirstname)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_FirstName"))));
			((EditText)findViewById(R.id.insplastname)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_LastName"))));
			((EditText)findViewById(R.id.inspmiddlename)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_MiddleName"))));	
			((EditText)findViewById(R.id.inspadd1)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_Address1"))));
			((EditText)findViewById(R.id.inspadd2)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_Address2"))));
			((EditText)findViewById(R.id.inspcity)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_City"))));			
			((EditText)findViewById(R.id.inspzip)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_Zip"))));
			((EditText)findViewById(R.id.inspstate)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_State"))));
			((EditText)findViewById(R.id.inspcounty)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_County"))));

		/*	if(!((EditText)findViewById(R.id.inspcity)).getText().toString().trim().equals("") ||
					!((EditText)findViewById(R.id.inspstate)).getText().toString().trim().equals("") ||
					!((EditText)findViewById(R.id.inspzip)).getText().toString().trim().equals("") ||
					!((EditText)findViewById(R.id.inspcounty)).getText().toString().trim().equals(""))
			{
				((Button)findViewById(R.id.search)).setVisibility(cf.v1.GONE);
			}
			else
			{
				((Button)findViewById(R.id.search)).setVisibility(cf.v1.VISIBLE);
			}
			*/

			String licencetype = db.decode(cur.getString(cur.getColumnIndex("Fld_licencetype")));
			System.out.println("licent="+licencetype);
			
			
			if(licencetype.contains("Other"))
			{
				String licencetypesplit[] = licencetype.split("&#98;");
				int spinnerPosition3 = primlicentypeadap.getPosition(licencetypesplit[0]);
				primlicentype.setSelection(spinnerPosition3);
				
				((EditText)findViewById(R.id.licenseother)).setVisibility(cf.v1.VISIBLE);
				((EditText)findViewById(R.id.licenseother)).setText(licencetypesplit[1]);								
			}
			else
			{
				int spinnerPosition3 = primlicentypeadap.getPosition(licencetype);
				primlicentype.setSelection(spinnerPosition3);
			    ((EditText)findViewById(R.id.licenseother)).setVisibility(cf.v1.GONE);
			}
			
			
			
			((EditText)findViewById(R.id.inspphone)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_Phone"))));
			((EditText)findViewById(R.id.inspemail)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_Email"))));	
			((EditText)findViewById(R.id.licenseno)).setText(db.decode(cur.getString(cur.getColumnIndex("Fld_licenceno"))));			
			((EditText)findViewById(R.id.licenseexpirydate)).setText(db.decode(cur.getString(cur.getColumnIndex("Fld_licenceexpirydate"))));
			
			((EditText)findViewById(R.id.companyname)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyName"))));
			((EditText)findViewById(R.id.compemail)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyEmail"))));	
			((EditText)findViewById(R.id.compphone)).setText(db.decode(cur.getString(cur.getColumnIndex("Ins_CompanyPhone"))));
			((Button)findViewById(R.id.register)).setText("Update");
			
			((EditText)findViewById(R.id.companylogotxt)).setVisibility(cf.v1.GONE);
			((ImageView) findViewById(R.id.comp_photo)).setVisibility(cf.v1.VISIBLE);
			((Button) findViewById(R.id.editbtn)).setVisibility(cf.v1.VISIBLE);
			
			((EditText)findViewById(R.id.headshottxt)).setVisibility(cf.v1.GONE);
			((ImageView) findViewById(R.id.headshot_photo)).setVisibility(cf.v1.VISIBLE);
			((Button) findViewById(R.id.headshotedit)).setVisibility(cf.v1.VISIBLE);
			
			try {
				System.out.println("d"+this.getFilesDir());
				File outputFile1 = new File(this.getFilesDir()+"/"+"CompanyLogo"+db.Insp_id.toString()+".jpg");
				if(outputFile1.exists())
				{
					System.out.println("outputFile1="+outputFile1);
					BitmapFactory.Options o1 = new BitmapFactory.Options();
					o1.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(new FileInputStream(outputFile1),
							null, o1);
					final int REQUIRED_SIZE = 200;
					int width_tmp1 = o1.outWidth, height_tmp1 = o1.outHeight;
					int scale1 = 1;
					while (true) {
						if (width_tmp1 / 2 < REQUIRED_SIZE
								|| height_tmp1 / 2 < REQUIRED_SIZE)
							break;
						width_tmp1 /= 2;
						height_tmp1 /= 2;
						scale1 *= 2;
					}
					BitmapFactory.Options o3 = new BitmapFactory.Options();
					o3.inSampleSize = scale1;
					Bitmap bitmap1 = BitmapFactory.decodeStream(new FileInputStream(
							outputFile1), null, o3);
					System.out.println("bitmap1 = "+bitmap1);
					BitmapDrawable bmd1 = new BitmapDrawable(bitmap1);
					((ImageView) findViewById(R.id.comp_photo)).setImageDrawable(bmd1);
					selectedImagePathcomplogo= outputFile1.toString();
				
				}
				else
				{
					compphotoavail  = false;
				}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the ssue in company logi ff"+e.getMessage());
			}
			
			try {
				System.out.println("d"+this.getFilesDir());
				File outputFile2 = new File(this.getFilesDir()+"/"+"Initials"+db.Insp_id.toString()+".jpg");
				if(outputFile2.exists())
				{
					System.out.println("outputFile2="+outputFile2);
					BitmapFactory.Options o2 = new BitmapFactory.Options();
					o2.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(new FileInputStream(outputFile2),
							null, o2);
					final int REQUIRED_SIZE = 200;
					int width_tmp1 = o2.outWidth, height_tmp1 = o2.outHeight;
					int scale1 = 1;
					while (true) {
						if (width_tmp1 / 2 < REQUIRED_SIZE
								|| height_tmp1 / 2 < REQUIRED_SIZE)
							break;
						width_tmp1 /= 2;
						height_tmp1 /= 2;
						scale1 *= 2;
					}
					BitmapFactory.Options o4 = new BitmapFactory.Options();
					o4.inSampleSize = scale1;
					Bitmap bitmap2 = BitmapFactory.decodeStream(new FileInputStream(
							outputFile2), null, o4);
					System.out.println("bitmap1 = "+bitmap2);
					BitmapDrawable bmd2 = new BitmapDrawable(bitmap2);
					
					initialsview.setBackgroundDrawable(bmd2);
					initialsbool=true;
				}
				else
				{
					initphotoavail=false;
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the ssue in company logi ff"+e.getMessage());
			}
			
			try {
				System.out.println("d"+this.getFilesDir());
				File outputFile3 = new File(this.getFilesDir()+"/"+"Signature"+db.Insp_id.toString()+".jpg");
				if(outputFile3.exists())
				{
				System.out.println("outputFile3="+outputFile3);
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile3),
						null, o2);
				final int REQUIRED_SIZE = 200;
				int width_tmp1 = o2.outWidth, height_tmp1 = o2.outHeight;
				int scale1 = 1;
				while (true) {
					if (width_tmp1 / 2 < REQUIRED_SIZE
							|| height_tmp1 / 2 < REQUIRED_SIZE)
						break;
					width_tmp1 /= 2;
					height_tmp1 /= 2;
					scale1 *= 2;
				}
				BitmapFactory.Options o4 = new BitmapFactory.Options();
				o4.inSampleSize = scale1;
				Bitmap bitmap3 = BitmapFactory.decodeStream(new FileInputStream(
						outputFile3), null, o4);
				System.out.println("bitmap1 = "+bitmap3);
				BitmapDrawable bmd3 = new BitmapDrawable(bitmap3);
				signview.setBackgroundDrawable(bmd3);
				signbool=true;
				}
				else
				{
					signphotoavail=false;
				}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the ssue in company logi ff"+e.getMessage());
			}
			signview.setDrawingCacheEnabled(true);
			
			try {
				System.out.println("d"+this.getFilesDir());
				File outputFile4 = new File(this.getFilesDir()+"/"+"Headshot"+db.Insp_id.toString()+".jpg");
				if(outputFile4.exists())
				{
					System.out.println("outputFile4="+outputFile4);
					BitmapFactory.Options o7 = new BitmapFactory.Options();
					o7.inJustDecodeBounds = true;
					BitmapFactory.decodeStream(new FileInputStream(outputFile4),
							null, o7);
					final int REQUIRED_SIZE = 200;
					int width_tmp1 = o7.outWidth, height_tmp1 = o7.outHeight;
					int scale1 = 1;
					while (true) {
						if (width_tmp1 / 2 < REQUIRED_SIZE
								|| height_tmp1 / 2 < REQUIRED_SIZE)
							break;
						width_tmp1 /= 2;
						height_tmp1 /= 2;
						scale1 *= 2;
					}
					BitmapFactory.Options o8 = new BitmapFactory.Options();
					o8.inSampleSize = scale1;
					Bitmap bitmap4 = BitmapFactory.decodeStream(new FileInputStream(
							outputFile4), null, o8);
					System.out.println("bitmap1 = "+bitmap4);
					BitmapDrawable bmd4 = new BitmapDrawable(bitmap4);
					((ImageView) findViewById(R.id.headshot_photo)).setImageDrawable(bmd4);
					selectedImagePath = outputFile4.toString();
				}
				else
				{
					headshotphotoavail=false;
				}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("the ssue in company logi ff"+e.getMessage());
			}
		}
		else
		{
			((EditText)findViewById(R.id.companylogotxt)).setVisibility(cf.v1.VISIBLE);
			((ImageView) findViewById(R.id.comp_photo)).setVisibility(cf.v1.GONE);
			((Button) findViewById(R.id.editbtn)).setVisibility(cf.v1.GONE);
			((Button) findViewById(R.id.companylogo)).setVisibility(cf.v1.VISIBLE);		
			
			
			((EditText)findViewById(R.id.headshottxt)).setVisibility(cf.v1.VISIBLE);
			((ImageView) findViewById(R.id.headshot_photo)).setVisibility(cf.v1.GONE);
			((Button) findViewById(R.id.headshotedit)).setVisibility(cf.v1.GONE);
			((Button) findViewById(R.id.headshoticon)).setVisibility(cf.v1.VISIBLE);
			((Button)findViewById(R.id.register)).setText("Save");
		}
	}
	private void Load_State_County_City(final EditText et)
	{

		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing... "
					+ "Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(Registration.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						System.out.println("et.getTe"+et.getText().toString());
						chklogin1 = wb
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							
						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								cf.ShowToast("Please enter a valid Zip");
								cf.hidekeyboard(((EditText)findViewById(R.id.inspzip)));
								
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
					
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
		}
	
	}
	private void Load_State_County_City(SoapObject objInsert)
	{
		cf.hidekeyboard(((EditText)findViewById(R.id.inspzip)));
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
			spinnerstate.setSelection(stateadapter.getPosition(state));
			countysetselection=true;
			((EditText)findViewById(R.id.inspcity)).setText(city);
	}
	public void LoadCounty(final String stateid) {
		try {
			dbh1 = new DataBaseHelper1(Registration.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + db.encode(stateid) + "' order by countyname", null);System.out.println("LoadCounty"+cur.getCount());
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = db.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = db.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					System.out.println("Name= "+Name);
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}
	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(Registration.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		System.out.println("countyse"+countysetselection);
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}
	public String LoadState() {
		try {
			dbh1 = new DataBaseHelper1(Registration.this);

			dbh1.createDataBase();
			SQLiteDatabase newDB = dbh1.openDataBase();
			dbh1.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);System.out.println("statevaluecur="+cur.getCount());
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = db.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = db.decode(cur.getString(cur
							.getColumnIndex("statename")));
					System.out.println("Category "+Category);
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}
	private void LoadLicenseinsSpinner() {
				// TODO Auto-generated method stub
				try
				{
				db.CreateTable(16);
				Cursor c =db.SelectTablefunction(db.PrimaryLicensetype, "");System.out.println("cc="+c.getCount());		
				if(c.getCount()>0)
			    {
					primarylictype=new String[c.getCount()+1];
					primarylicid=new String[c.getCount()+1];
					primarylictype[0] = "--Select--";
					c.moveToFirst();
					for(int i=1;i<c.getCount()+1;i++)
			   		{
			   			primarylictype[i]= db.decode(c.getString(c.getColumnIndex("LicenseName")));
			   			primarylicid[i]= db.decode(c.getString(c.getColumnIndex("Licenseid")));
			   			c.moveToNext();
			   		}
			    }
				}catch(Exception e)
				{
					System.out.println("dfdsf"+e.getMessage());
				}
				System.out.println("LoadLicenseinsSpinner ");
	}
	private void LoadLicensetype() {
		// TODO Auto-generated method stub
					cf.show_ProgressDialog("Processing... ");
	    	new Thread() {
				public void run(){
					Looper.prepare();
					try
					{
						SoapObject request = new SoapObject(wb.NAMESPACE,"LICENCETYPE");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						envelope.setOutputSoapObject(request);System.out.println("LICENCETYPE"+request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
						androidHttpTransport.call(wb.NAMESPACE+"LICENCETYPE",envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						System.out.println("LICENCETYPE="+result);
						InsertLicense(result);							
						
						show_handler=1;
						handler1.sendEmptyMessage(0);
					}
					catch (SocketException e) {
						System.out.println("socket"+e.getMessage());
						// TODO Auto-generated catch block
						show_handler=3;
						handler1.sendEmptyMessage(0);
						
					} catch (IOException e) {
						System.out.println("IO="+e.getMessage());
						// TODO Auto-generated catch block
						show_handler=3;
						handler1.sendEmptyMessage(0);
						
					} catch (XmlPullParserException e) {
						System.out.println("xml="+e.getMessage());
						// TODO Auto-generated catch block
						show_handler=3;
						handler1.sendEmptyMessage(0);
						
					}
					catch (Exception e) {
						// TODO: handle exception
						System.out.println("Eee"+e.getMessage());
						
						show_handler=4;
						handler1.sendEmptyMessage(0);
					}
				}
	    	}.start();
	}
	private Handler handler1 = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			cf.pd.dismiss();
			if(show_handler == 1)
			{				
				primlicentypeadap = new ArrayAdapter<String>(Registration.this,android.R.layout.simple_spinner_item,primarylictype);
				primlicentypeadap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				primlicentype.setAdapter(primlicentypeadap);
				show_handler=0;			
				System.out.println("final");
				setvalue();
			}
			else if(show_handler == 3)
			{
				cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
				show_handler=0;
			}
			else if(show_handler == 4)
			{
				cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
				show_handler=0;
			}
			
			
		}
	};

private void InsertLicense(SoapObject result) {
		// TODO Auto-generated method stub
		int Cnt = result.getPropertyCount();System.out.println("Cc"+Cnt);
		 db.wind_db.execSQL("delete from " + db.PrimaryLicensetype);
		 db.CreateTable(16);
		for(int i=0;i<Cnt;i++)
		{
			SoapObject temp_result=(SoapObject) result.getProperty(i);
			String LicenseID = String.valueOf(temp_result.getProperty("LicenseType_flag"));
			String LicenseName = String.valueOf(temp_result.getProperty("DocumentTitle"));
		
			db.wind_db.execSQL("Insert into "+db.PrimaryLicensetype+" (LicenseName,Licenseid) values" +
					"('"+db.encode(LicenseName)+"','"+LicenseID+"') ");
		}	
		System.out.println("insert licences");
		LoadLicenseinsSpinner();
	}
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.search:
		if(!((EditText)findViewById(R.id.inspcity)).getText().toString().trim().equals(""))
		{
			if(((EditText)findViewById(R.id.inspzip)).getText().toString().trim().equals(""))
			{
				zip=0;
			}
			else
			{
				zip  = Integer.parseInt(((EditText)findViewById(R.id.inspzip)).getText().toString());
			}
			
			citysearch(((EditText)findViewById(R.id.inspcity)).getText().toString().trim(),((EditText)findViewById(R.id.inspstate)).getText().toString().trim(),
					((EditText)findViewById(R.id.inspcounty)).getText().toString().trim(),zip);
		}
		else
		{
			cf.ShowToast("Please enter the city");
		}
		
		break;
		case R.id.home:
			cf.gohome();
			break;
		case R.id.issuedate:
			showDialogDate(((EditText)findViewById(R.id.licenseexpirydate)));
		   break;
		case R.id.editbtn:
			AlertDialog.Builder builder = new AlertDialog.Builder(Registration.this);
			builder.setMessage("Are you sure, Do you want to update the Company Logo?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int id) {
									
									((EditText)findViewById(R.id.companylogotxt)).setVisibility(cf.v1.VISIBLE);
									((ImageView) findViewById(R.id.comp_photo)).setVisibility(cf.v1.GONE);
									((Button) findViewById(R.id.editbtn)).setVisibility(cf.v1.GONE);
									((Button)findViewById(R.id.companylogo)).setVisibility(cf.v1.VISIBLE);
									
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});
			AlertDialog al=builder.create();
			al.setTitle("Confirmation");
			al.setIcon(R.drawable.alertmsg);
			al.setCancelable(false);
			al.show();
			break;
			
		case R.id.headshotedit:
			
			AlertDialog.Builder builder1 = new AlertDialog.Builder(Registration.this);
			builder1.setMessage("Are you sure, Do you want to update the Headshot?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int id) {
									
									((EditText)findViewById(R.id.headshottxt)).setVisibility(cf.v1.VISIBLE);
									((ImageView) findViewById(R.id.headshot_photo)).setVisibility(cf.v1.GONE);
									((Button) findViewById(R.id.headshotedit)).setVisibility(cf.v1.GONE);
									((Button)findViewById(R.id.headshoticon)).setVisibility(cf.v1.VISIBLE);									
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									dialog.cancel();
								}
							});
			AlertDialog a2= builder1.create();
			a2.setTitle("Confirmation");
			a2.setIcon(R.drawable.alertmsg);
			a2.setCancelable(false);
			a2.show();				
			break;
		case R.id.companylogo:
			u = 0;
			Gallery_Camera_Dialog();
			break;
		case R.id.headshoticon:
			u = 1;
			Gallery_Camera_Dialog();
			break;
		case R.id.signature_clearimage:
			sign_clear();
			break;
		case R.id.initials_clearimage:
			initialsclear();
			break;
		case R.id.clear:
			clearall();			
			break;
	    case R.id.register:
		 	strzip = ((EditText)findViewById(R.id.inspzip)).getText().toString();
	    	if(!((EditText)findViewById(R.id.inspfirstname)).getText().toString().trim().equals(""))
			{
				if(!((EditText)findViewById(R.id.insplastname)).getText().toString().trim().equals(""))
				{
					if(!((EditText)findViewById(R.id.inspadd1)).getText().toString().trim().equals(""))
					{
						if(!((EditText)findViewById(R.id.inspcity)).getText().toString().trim().equals(""))
						{
						
						if(!((EditText)findViewById(R.id.inspzip)).getText().toString().trim().equals(""))
						{
						 if(strzip.length()==5)
						  {
					
								if(!((EditText)findViewById(R.id.inspstate)).getText().toString().trim().equals(""))
								{
									if(!((EditText)findViewById(R.id.inspcounty)).getText().toString().trim().equals(""))
									{
										if(!((EditText)findViewById(R.id.inspphone)).getText().toString().trim().equals(""))
										{
											if(cf.PhoneNo_validation(((EditText)findViewById(R.id.inspphone)).getText().toString().trim()).equals("Yes"))
											{
											if(!((EditText)findViewById(R.id.inspemail)).getText().toString().trim().equals(""))
											{
												if(cf.Email_Validation(((EditText)findViewById(R.id.inspemail)).getText().toString()))
												{
												if(!((EditText)findViewById(R.id.licenseno)).getText().toString().trim().equals(""))
												{
													if(!((EditText)findViewById(R.id.licenseexpirydate)).getText().toString().trim().equals(""))
													{
														/*if(cf.checkfortodaysdate(((EditText)findViewById(R.id.licenseexpirydate)).getText().toString())==true)
														{*/
														//if(!((EditText)findViewById(R.id.licenseissuedate)).getText().toString().trim().equals(""))
														//{
														
														boolean b=((primlicentype.getSelectedItem().toString().equals("Other")) && ((EditText)findViewById(R.id.licenseother)).getText().toString().trim().equals("")) ? false:true;
														if(!primlicentype.getSelectedItem().toString().equals("--Select--") &&  b==true)
															
															{
																/*if(cf.checkfortodaysdate(((EditText)findViewById(R.id.licenseissuedate)).getText().toString())==true)
																{*/
																if(!((EditText)findViewById(R.id.companyname)).getText().toString().trim().equals(""))
																{
																	if(!((EditText)findViewById(R.id.companylogotxt)).getText().toString().trim().equals("")
																			||   (((ImageView)findViewById(R.id.comp_photo)).getVisibility()==View.VISIBLE && compphotoavail==true))
																	{
																	
																		if(!((EditText)findViewById(R.id.compphone)).getText().toString().trim().equals(""))
																		{
																			if(cf.PhoneNo_validation(((EditText)findViewById(R.id.compphone)).getText().toString().trim()).equals("Yes"))
																			{
																			
																			if(!((EditText)findViewById(R.id.compemail)).getText().toString().trim().equals(""))
																			{
																				if(cf.Email_Validation(((EditText)findViewById(R.id.compemail)).getText().toString()))
																				{																		
																					if(!((EditText)findViewById(R.id.headshottxt)).getText().toString().trim().equals("")
																							||   (((ImageView)findViewById(R.id.headshot_photo)).getVisibility()==View.VISIBLE && headshotphotoavail==true))
																					{
																						if(signbool)
																						{
																							signview.setDrawingCacheEnabled(true);
																							Bitmap bmsignature=Bitmap.createBitmap(signview.getDrawingCache());
																							
																							ByteArrayOutputStream bos=new ByteArrayOutputStream();
																							bmsignature.compress(Bitmap.CompressFormat.PNG, 0, bos);
																							signsignature=bos.toByteArray();
																							signview.setDrawingCacheEnabled(false);
																							if(initialsbool)
																							{
																								initialsview.setDrawingCacheEnabled(true);
																								Bitmap insignature=Bitmap.createBitmap(initialsview.getDrawingCache());
																								
																								ByteArrayOutputStream is=new ByteArrayOutputStream();
																								insignature.compress(Bitmap.CompressFormat.PNG, 0, is);
																								initialsignature=is.toByteArray();
																								initialsview.setDrawingCacheEnabled(false);
																								
																								InsertData();
																							}
																							else
																							{
																								cf.ShowToast("Please add Initials");
																							}
																						}
																						else
																						{
																							cf.ShowToast("Please add Signature");
																						}
																					}
																					else
																					{
																						cf.ShowToast("Please upload Headshot");
																					}
																				}
																				else
																				{
																					cf.ShowToast("Please enter the valid Company Email");
																					cf.setFocus(((EditText)findViewById(R.id.compemail)));
									
																				}
																			}
																			else
																			{	
																				cf.ShowToast("Please enter the Company Email");
																					cf.setFocus(((EditText)findViewById(R.id.compemail)));
																			
																			}
																			}
																			else
																			{
																				cf.ShowToast("Please enter the Company Contact Phone in 10 digits");
																				cf.setFocus(((EditText)findViewById(R.id.compphone)));
																			}
																		}
																		else
																		{
																			cf.ShowToast("Please enter the Company Contact Phone");
																			cf.setFocus(((EditText)findViewById(R.id.compphone)));
																		}
																		
																      }
																	  else
																	  {
																		cf.ShowToast("Please upload Company Logo");
																	  }	
																	}
																	else
																	{
																		cf.ShowToast("Please enter Company Name");
																	}
														
																/*}
																else
																{
																	cf.ShowToast("Please enter Inspector License Issue Date as previous or current date ", 0);
																}*/
															}
															else
															{
																if(primlicentype.getSelectedItem().toString().equals("--Select--"))
																{
																	cf.ShowToast("Please select Primary License Type");
																}
																else if(b==false)
																{
																	cf.ShowToast("Please enter the other text for Primary License Type");	
																	cf.setFocus(((EditText)findViewById(R.id.licenseother)));
																}																
															}
														/*}
														else
														{
															cf.ShowToast("Please select Future date for Inspector License Expiry Date", 0);
														}*/
													}
													else
													{
														cf.ShowToast("Please select License Expiry Date");
													}
												}
												else
												{
													cf.ShowToast("Please enter the Primary License Number");
													cf.setFocus(((EditText)findViewById(R.id.licenseno)));
												}
											}
												else
												{
													cf.ShowToast("Please enter the valid Email");
													cf.setFocus(((EditText)findViewById(R.id.inspemail)));
	
												}
											}
											else
											{	
												cf.ShowToast("Please enter the Email");
													cf.setFocus(((EditText)findViewById(R.id.inspemail)));
											
											}
											}
											else
											{	
												cf.ShowToast("Please enter the Contact Phone in 10 digits");
												cf.setFocus(((EditText)findViewById(R.id.inspphone)));											
											}
										}
										else
										{
											cf.ShowToast("Please enter the Contact Phone");
											cf.setFocus(((EditText)findViewById(R.id.inspphone)));
										}
									}
									else
									{
										cf.ShowToast("Please enter the County");
										
									}
								}
								else
								{
									cf.ShowToast("Please enter the State");
									
								}
						  }
							else
							{
								cf.ShowToast("Please enter the Valid Zip");
								cf.setFocus(((EditText)findViewById(R.id.inspzip)));
							}
						}
						else
						{
							cf.ShowToast("Please enter the Zipcode");
							cf.setFocus(((EditText)findViewById(R.id.inspzip)));
						}
						}
						else
						{
							cf.ShowToast("Please enter the City");
							cf.setFocus(((EditText)findViewById(R.id.inspcity)));
						}
					}
					else
					{
						cf.ShowToast("Please enter the Address1");
						cf.setFocus(((EditText)findViewById(R.id.inspadd1)));
					}
				}
				else
				{
					cf.ShowToast("Please enter the Last Name");
					cf.setFocus(((EditText)findViewById(R.id.insplastname)));
				}
			}
			else
			{
				cf.ShowToast("Please enter the First Name");
				cf.setFocus(((EditText)findViewById(R.id.inspfirstname)));
			}
			
			break;
		}
	}
	private void citysearch(final String city, final String state, final String county,final int zip) {
		// TODO Auto-generated method stub
		if (wb.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing... "
					+ "Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(Registration.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject citysearch;
				public void run() {
					Looper.prepare();
					try {
						citysearch = wb.callwebserviceforcitysearch(city,state,county,zip,"LOADOVERALLZIPVALIDATION");
						System.out.println("response LOADOVERALLZIPVALIDATION" + citysearch);
					
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
							
						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
							
						} else if (show_handler == 5) {
							show_handler = 0;
							showpoupup(citysearch);
						
						}
					}
					
				};
			}.start();

		} else {
			cf.ShowToast("Internet connection not available");
		}
	}
	private void showpoupup(SoapObject citysearch) {
		
	
		 	dialog = new Dialog(Registration.this,android.R.style.Theme_Translucent_NoTitleBar);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setCancelable(false);
			dialog.getWindow().setContentView(R.layout.customlayout);
			
			System.out.println("fdsfdf");
			LinearLayout lin = (LinearLayout) dialog.findViewById(R.id.chk);
			lin.setOrientation(LinearLayout.VERTICAL);
			
			Button close = (Button) dialog.findViewById(R.id.close);
			
			close.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			 lin.removeAllViews();
			LinearLayout h1= (LinearLayout) getLayoutInflater().inflate(R.layout.lisview_header, null);
			LinearLayout th= (LinearLayout)h1.findViewById(R.id.register);th.setVisibility(cf.v1.VISIBLE);
			TableLayout.LayoutParams lp = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
		 	//lp.setMargins(2, 0, 2, 2); 
		 	h1.removeAllViews(); 	 	
		 	lin.addView(th,lp);	
		 	lin.setVisibility(View.VISIBLE);	
		 	
		 	System.out.println("fdsfdfasddasasdasdadasd");
		 	
		 	int Cnt = citysearch.getPropertyCount();System.out.println("fdCnt="+Cnt);
		 	if(Cnt==0)
		 	{
		 		cf.ShowToast("Please enter the valid city.");
		 	}
		 	else
		 	{
		 	
			 	for (int i = 0; i < Cnt+1; i++) 
				{	
					TextView txtcity,txtcounty,txtstate,txtzip;
					RadioButton rd1;
					ImageView edit,delete;
					
				
					
					LinearLayout t1= (LinearLayout) getLayoutInflater().inflate(R.layout.listviewaddr, null);	
					LinearLayout t = (LinearLayout)t1.findViewById(R.id.registerlist);t.setVisibility(cf.v1.VISIBLE);
					t.setId(44444+i);/// Set some id for further use
							
					txtcity= (TextView) t.findViewWithTag("regcity");			
					txtcounty= (TextView) t.findViewWithTag("regcounty");
					txtstate= (TextView) t.findViewWithTag("regstate");	
					txtzip= (TextView) t.findViewWithTag("regzip");	
					
					rd1= (RadioButton) t.findViewWithTag("rdio1");		
					TableLayout.LayoutParams lp1 = new TableLayout.LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
				 	lp1.setMargins(2, 0, 2, 2); 
					
					if(i==0)
					{
						txtcity.setText("None");
						txtcounty.setText("None");
						txtstate.setText("None");
						txtzip.setText("None");
	
						rd1.setOnClickListener(new clicker1(i,"None","None","None",""));
					}
					else
					{
						SoapObject obj = (SoapObject) citysearch.getProperty(i-1);
						String zip = (String.valueOf(obj.getProperty("zip")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("zip")).equals("NA"))?"":(String.valueOf(obj.getProperty("zip")).equals("N/A"))?"":String.valueOf(obj.getProperty("zip"));
						String city = (String.valueOf(obj.getProperty("city")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("city")).equals("NA"))?"":(String.valueOf(obj.getProperty("city")).equals("N/A"))?"":String.valueOf(obj.getProperty("city"));
						String state = (String.valueOf(obj.getProperty("state")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("state")).equals("NA"))?"":(String.valueOf(obj.getProperty("state")).equals("N/A"))?"":String.valueOf(obj.getProperty("state"));
						String country = (String.valueOf(obj.getProperty("country")).equals("anyType{}"))?"":(String.valueOf(obj.getProperty("country")).equals("NA"))?"":(String.valueOf(obj.getProperty("country")).equals("N/A"))?"":String.valueOf(obj.getProperty("country"));
						System.out.println("zip = "+zip +" city = "+city +" state = "+state +" country = "+country);
					
						txtcity.setText(city);
						txtcounty.setText(country);
						txtstate.setText(state);
						txtzip.setText(zip);
						rd1.setOnClickListener(new clicker1(i,city,state,country,zip));
					
					}
					 	t1.removeAllViews();
						lin.addView(t,lp1);
				}
			 	
			 	dialog.show();
		 	}
	}
	class clicker1 implements OnClickListener
	{
		int i=0;
		String city1="",state1="",county1="",zip1="";
		public clicker1(int k,String city,String state,String county,String zip) {
		// TODO Auto-generated constructor stub
			i=k;
			state1=state;
			city1=city;
			county1= county;
			zip1= zip;
	    }
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			  dialog.dismiss(); 
			if(i==0)
			{

				((EditText)findViewById(R.id.inspstate)).setEnabled(true);
				((EditText)findViewById(R.id.inspcounty)).setEnabled(true);
				((EditText)findViewById(R.id.inspzip)).setEnabled(true);
				
				((EditText)findViewById(R.id.inspstate)).setText("");
				((EditText)findViewById(R.id.inspcounty)).setText("");
				((EditText)findViewById(R.id.inspzip)).setText("");	
				
			}
			else
			{			 
				((EditText)findViewById(R.id.inspcity)).setText(city1);
				((EditText)findViewById(R.id.inspstate)).setText(state1);
				((EditText)findViewById(R.id.inspcounty)).setText(county1);
				((EditText)findViewById(R.id.inspzip)).setText(zip1);	
				
				((EditText)findViewById(R.id.inspstate)).setEnabled(false);
				((EditText)findViewById(R.id.inspcounty)).setEnabled(false);
				((EditText)findViewById(R.id.inspzip)).setEnabled(false);
				
				//((Button)findViewById(R.id.search)).setVisibility(cf.v1.GONE);
				
			}
			    
			
		}
	}
	
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(Registration.this,
				new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}

	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				final Calendar c = Calendar.getInstance();
				final int year1 = c.get(Calendar.YEAR);
				int month1 = c.get(Calendar.MONTH);
				int day1 = c.get(Calendar.DAY_OF_MONTH);
				String currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
				
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				System.out.println("current date " + date1);
				System.out.println("selected date " + date2);
				if (date2.compareTo(date1) < 0) {
					System.out.println("inside date if");
					cf.ShowToast("Please select Current and Future Date");
					v.setText("");
				} else {
					System.out.println("inside date else");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
	private void Gallery_Camera_Dialog() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(Registration.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.galleryalert);
		dialog.setCancelable(false);
		
		Button btnchoosefromgallery = (Button) dialog
				.findViewById(R.id.alert_choosefromgallery);
		Button btntakeapicturefromcamera = (Button) dialog
				.findViewById(R.id.alert_takeapicturefromcamera);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.alert_helpclose);
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btnchoosefromgallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				i.setType("image/*");
				startActivityForResult(i, 0);

			}
		});
		btntakeapicturefromcamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				String fileName = "temp.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				CapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
				startActivityForResult(intent, 1);
			}
		});
		dialog.show();
	}
	private void initialsclear() {
		// TODO Auto-generated method stub
		initialsview.setDrawingCacheEnabled(true);
		initialsview.clear();
		initialsview.setDrawingCacheEnabled(false);
		initialsview.setBackgroundColor(Color.WHITE);
		initialsbool=false;
	}
	private void sign_clear() {
		// TODO Auto-generated method stub
		signview.setDrawingCacheEnabled(true);
		signview.clear();
		signview.setDrawingCacheEnabled(false);
		signview.setBackgroundColor(Color.WHITE);
		signbool=false;
	}
	private void clearall() {
		// TODO Auto-generated method stub
		((EditText)findViewById(R.id.inspfirstname)).setText("");
		((EditText)findViewById(R.id.insplastname)).setText("");
		((EditText)findViewById(R.id.inspmiddlename)).setText("");
		((EditText)findViewById(R.id.inspadd1)).setText("");
		((EditText)findViewById(R.id.inspadd2)).setText("");
		((EditText)findViewById(R.id.inspcity)).setText("");
		((EditText)findViewById(R.id.inspstate)).setText("");
		((EditText)findViewById(R.id.inspcounty)).setText("");
		 ((EditText)findViewById(R.id.inspzip)).setText("");
		((EditText)findViewById(R.id.inspphone)).setText("");
		((EditText)findViewById(R.id.inspemail)).setText("");			
		sign_clear();
		initialsclear();
		((EditText)findViewById(R.id.headshottxt)).setText("");
		((EditText)findViewById(R.id.compemail)).setText("");
		((EditText)findViewById(R.id.compphone)).setText("");
		
		
		((EditText)findViewById(R.id.licenseexpirydate)).setText("");
		primlicentype.setSelection(0);
		((EditText)findViewById(R.id.licenseno)).setText("");
		((EditText)findViewById(R.id.inspfirstname)).requestFocus();
		((EditText)findViewById(R.id.companylogotxt)).setText("");
		((EditText)findViewById(R.id.companyname)).setText("");
	}
	protected void pickfromgallery() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_PICTURE);
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);
			
			
			File f = new File(filePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			
			if (mb >= 2) {
				cf.ShowToast("File size exceeds!! Too large to attach");
			} else {
				Bitmap bm = decodeFile(filePath);
				
				System.out.println("The bitmap is "+bm);
				if(bm==null)
				{
					cf.ShowToast("File corrupted!! Cant able to attach");
				}
				else
				{
					if(u==1)
					{	
						selectedImagePath = filePath;
						String[] bits = selectedImagePath.split("/");
						picname1 = bits[bits.length - 1];
						((EditText)findViewById(R.id.headshottxt)).setText(selectedImagePath);
					}
					else 
					{
						selectedImagePathcomplogo = filePath;
						String[] bits1 = selectedImagePathcomplogo.split("/");
						picname2 = bits1[bits1.length - 1];
						((EditText)findViewById(R.id.companylogotxt)).setText(selectedImagePathcomplogo);
					}
					
					
					
					
				}
			}
		}
		
		else if (requestCode == 1 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(CapturedImageURI, projection, null,
					null, null);
			int column_index_data = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			String capturedImageFilePath = cursor.getString(column_index_data);
			
			File f = new File(capturedImageFilePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			

			if (mb >= 2) {
				cf.ShowToast("File size exceeds!! Too large to attach");
			} else {
				try
				{
					bitmapdb = decodeFile(capturedImageFilePath);
					if(bitmapdb==null)
					{
						cf.ShowToast("File corrupted!! Cant able to attach");
					}
					else
					{
						
						if(u==1)
						{	
							selectedImagePath = capturedImageFilePath;
							String[] bits = selectedImagePath.split("/");
							picname1 = bits[bits.length - 1];
							((EditText)findViewById(R.id.headshottxt)).setText(selectedImagePath);
						}
						else 
						{
							selectedImagePathcomplogo = capturedImageFilePath;
							String[] bits1 = selectedImagePathcomplogo.split("/");
							picname2 = bits1[bits1.length - 1];
							((EditText)findViewById(R.id.companylogotxt)).setText(selectedImagePathcomplogo);
						}
						
					}
				}
				catch (OutOfMemoryError e) {
					// TODO: handle exception
					cf.ShowToast("File size exceeds!! Too large to attach");
				}
			}
		}
			/*if (resultCode == RESULT_OK) {
				if (requestCode == SELECT_PICTURE) {
					Uri selectedImageUri = data.getData();
					if(t==1)
					{
						selectedImagePath1 = getPath(selectedImageUri);
						String[] bits = selectedImagePath1.split("/");
						picname1 = bits[bits.length - 1];
						((EditText)findViewById(R.id.headshottxt)).setText(selectedImagePath1);
					}
					else if(t==2)
					{
						selectedImagePath2 = getPath(selectedImageUri);
						String[] bits = selectedImagePath2.split("/");
						picname2 = bits[bits.length - 1];
						((EditText)findViewById(R.id.signaturetxt)).setText(selectedImagePath2);
					}
					else if(t==3)
					{
						selectedImagePath3 = getPath(selectedImageUri);
						String[] bits = selectedImagePath3.split("/");
						picname3 = bits[bits.length - 1];
						((EditText)findViewById(R.id.initialstxt)).setText(selectedImagePath3);
					}
				}
			}*/
	}

	private static Bitmap decodeFile(String file) {
	    try {
	    	
	    	File f=new File(file);
	    	
	        // Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 150;

	        // Find the correct scale value. It should be the power of 2.
	        int scale = 1;
	        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
	                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;

	        // Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        o.inJustDecodeBounds = false;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
	    }

	    return null;
	}
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private boolean comparetwodates(String string, String string2) {
		// TODO Auto-generated method stub
		
		return false;
	}
	private void InsertData() {
		// TODO Auto-generated method stub
	if(Insp_Id==0)
	{
		cf.show_ProgressDialog("Registering... ");
	}
	else
	{
		cf.show_ProgressDialog("Updating... ");
	}
    	new Thread() {
			public void run(){
				Looper.prepare();
				try
				{
					SoapObject request = new SoapObject(wb.NAMESPACE,"INSPECTORREGISTRATION");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					
					request.addProperty("Firstname",((EditText)findViewById(R.id.inspfirstname)).getText().toString());
					request.addProperty("Middlename",((EditText)findViewById(R.id.inspmiddlename)).getText().toString());
					request.addProperty("Lastname",((EditText)findViewById(R.id.insplastname)).getText().toString());
					request.addProperty("Address1",((EditText)findViewById(R.id.inspadd1)).getText().toString());
					request.addProperty("Address2",((EditText)findViewById(R.id.inspadd2)).getText().toString());
					
					request.addProperty("City",((EditText)findViewById(R.id.inspcity)).getText().toString());
					request.addProperty("State",((EditText)findViewById(R.id.inspstate)).getText().toString());System.out.println("request3"+request);
					request.addProperty("County",((EditText)findViewById(R.id.inspcounty)).getText().toString());
					if(((EditText)findViewById(R.id.inspzip)).getText().toString().trim().equals(""))
					{
						request.addProperty("Zipcode",0);
					}
					else
					{
						request.addProperty("Zipcode",((EditText)findViewById(R.id.inspzip)).getText().toString());	
					}
					
					request.addProperty("Contactphone",((EditText)findViewById(R.id.inspphone)).getText().toString());
					request.addProperty("Contactemail",((EditText)findViewById(R.id.inspemail)).getText().toString());
					System.out.println("test"+primlicentype.getSelectedItemPosition());
					
					

					if(primlicentype.getSelectedItem().toString().equals("Other"))
					{
						request.addProperty("Primarylicencetype",primlicentype.getSelectedItem().toString()+"&#98;"+((EditText)findViewById(R.id.licenseother)).getText().toString().trim());
					}
					else
					{
						request.addProperty("Primarylicencetype",primlicentype.getSelectedItem().toString());
					}
					
					request.addProperty("Primarylicenseno",((EditText)findViewById(R.id.licenseno)).getText().toString());
					request.addProperty("Licenseexpirydate",((EditText)findViewById(R.id.licenseexpirydate)).getText().toString());
					request.addProperty("InsID ",Insp_Id);
					
					
					System.out.println("selectedImagePath="+selectedImagePath);
					Bitmap bitmap1 = cf.ShrinkBitmap(selectedImagePath, 400, 400);
					MarshalBase64 marshal = new MarshalBase64();
					ByteArrayOutputStream out1 = new ByteArrayOutputStream();
					bitmap1.compress(CompressFormat.PNG, 100, out1);
					raw1 = out1.toByteArray();					
					
					request.addProperty("Headshot",raw1);					
					request.addProperty("Headshotimagename",picname1);
					marshal.register(envelope);
					System.out.println("came here");
					
					Bitmap bitmap2 = cf.ShrinkBitmap(selectedImagePathcomplogo, 400, 400);
					MarshalBase64 marshal1 = new MarshalBase64();
					ByteArrayOutputStream out2 = new ByteArrayOutputStream();
					bitmap2.compress(CompressFormat.PNG, 100, out2);
					raw2 = out2.toByteArray();					
					
					request.addProperty("Companylogo",raw2);
					marshal1.register(envelope);
					
					request.addProperty("Companyname",((EditText)findViewById(R.id.companyname)).getText().toString());
					
					request.addProperty("Companyphoneno",((EditText)findViewById(R.id.compphone)).getText().toString());
					request.addProperty("Companyemail",((EditText)findViewById(R.id.compemail)).getText().toString());
					
					request.addProperty("Signature",signsignature);
					request.addProperty("Initials",initialsignature);
					System.out.println("request"+request);
					
					envelope.setOutputSoapObject(request);					
					
					HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
					androidHttpTransport.call(wb.NAMESPACE+"INSPECTORREGISTRATION",envelope);
					SoapObject inspreg =(SoapObject) envelope.getResponse(); 
					System.out.println("inspreg="+inspreg);
					System.out.println("inssps="+String.valueOf(inspreg.getProperty("Status")));
					if(String.valueOf(inspreg.getProperty("Status")).equals("true"))
					{
						String Inspector_id = String.valueOf(inspreg.getProperty("Inspectorid"));
						String Insp_fname = String.valueOf(inspreg.getProperty("Inspectorfirstname"));
						
						String Insp_mname= (String.valueOf(inspreg.getProperty("Inspectormiddlename")).equals("anyType{}"))?"":(String.valueOf(inspreg.getProperty("Inspectormiddlename")).equals("NA"))?"":(String.valueOf(inspreg.getProperty("Inspectormiddlename")).equals("N/A"))?"":String.valueOf(inspreg.getProperty("Inspectormiddlename"));
						String Insp_lname = String.valueOf(inspreg.getProperty("Inspectorlastname"));
						String Insp_address1 = String.valueOf(inspreg.getProperty("Inspectoraddress1"));
						String Insp_address2= (String.valueOf(inspreg.getProperty("Inspectoraddress2")).equals("anyType{}"))?"":(String.valueOf(inspreg.getProperty("Inspectoraddress2")).equals("NA"))?"":(String.valueOf(inspreg.getProperty("Inspectoraddress2")).equals("N/A"))?"":String.valueOf(inspreg.getProperty("Inspectoraddress2"));
						String Insp_city = String.valueOf(inspreg.getProperty("City"));
						String Insp_state = String.valueOf(inspreg.getProperty("State"));						
						String Insp_county = String.valueOf(inspreg.getProperty("County"));
						String Insp_zip = String.valueOf(inspreg.getProperty("Zipcode"));
						String Insp_contphone = String.valueOf(inspreg.getProperty("Inspectorphoneno"));
						String Insp_email = String.valueOf(inspreg.getProperty("InspectorEmail"));
						
						String Insp_primlictype = String.valueOf(inspreg.getProperty("PrimaryLicenseType"));
						String Insp_primlicno = String.valueOf(inspreg.getProperty("PrimaryLicenseNumber"));						
						String Insp_licexpirydtae = String.valueOf(inspreg.getProperty("PrimaryLicenseExpirydt"));
					
						String Insp_headshot = String.valueOf(inspreg.getProperty("InspectorHeadshot"));
						String Insp_signature= String.valueOf(inspreg.getProperty("InspectorSign"));
						String Insp_initials= String.valueOf(inspreg.getProperty("InspectorInitial"));
						
						String Insp_username= String.valueOf(inspreg.getProperty("Inspectorusername"));
						String Insp_password = String.valueOf(inspreg.getProperty("Inspectorpassword"));
						String Insp_companyid = String.valueOf(inspreg.getProperty("InspectorcompanyId"));	
						String Insp_companyname= String.valueOf(inspreg.getProperty("Inspectorcompanyname"));	
						
						String LoginStatus= String.valueOf(inspreg.getProperty("LoginStatus"));
						String androidstatus= String.valueOf(inspreg.getProperty("AndroidStatus"));
						
						String CompanyName = String.valueOf(inspreg.getProperty("Companyname"));
						String CompanyLogo= String.valueOf(inspreg.getProperty("Companylogo"));
						
						String CompanyPhone = String.valueOf(inspreg.getProperty("Companyphoneno"));
						String CompanyEmail= String.valueOf(inspreg.getProperty("Companyemail"));						
						
						try
						{
							URL ulrn = new URL(Insp_headshot);
						    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						    InputStream is = con.getInputStream();
						    Bitmap bmp = BitmapFactory.decodeStream(is);
						   
						    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
						    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
						    byte[] b = baos.toByteArray();
						    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
						    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

						    String FILENAME = "Headshot"+Inspector_id +".jpg";System.out.println("FILENAME "+FILENAME);
							FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
							fos.write(decode);
							fos.close();	
						    
						}
						catch (Exception e1){
						
							System.out.println("Imagge headshot disp = "+e1.getMessage());
						}
						
						try
						{
							URL ulrn1 = new URL(Insp_signature);
						    HttpURLConnection con1 = (HttpURLConnection)ulrn1.openConnection();
						    InputStream is1 = con1.getInputStream();
						    Bitmap bmp1 = BitmapFactory.decodeStream(is1);
						   
						    ByteArrayOutputStream baos1 = new ByteArrayOutputStream();  
						    bmp1.compress(Bitmap.CompressFormat.PNG, 100, baos1); //bm is the bitmap object   
						    byte[] b1 = baos1.toByteArray();
						    String signature_insp = Base64.encodeToString(b1, Base64.DEFAULT);
						    byte[] decode1 = Base64.decode(signature_insp.toString(), 0);

						    String FILENAME1 = "Signature"+Inspector_id +".jpg";System.out.println("FILENAME "+FILENAME1);
							FileOutputStream fos1 = openFileOutput(FILENAME1, Context.MODE_WORLD_READABLE);
							fos1.write(decode1);
							fos1.close();	
						    
						}
						catch (Exception e1){
						
							System.out.println("Imagge Signature disp = "+e1.getMessage());
						}
						
						try
						{
							URL ulrn2 = new URL(Insp_initials);
						    HttpURLConnection con2 = (HttpURLConnection)ulrn2.openConnection();
						    InputStream is2 = con2.getInputStream();
						    Bitmap bmp2 = BitmapFactory.decodeStream(is2);
						   
						    ByteArrayOutputStream baos2 = new ByteArrayOutputStream();  
						    bmp2.compress(Bitmap.CompressFormat.PNG, 100, baos2); //bm is the bitmap object   
						    byte[] b2 = baos2.toByteArray();
						    String initials_insp = Base64.encodeToString(b2, Base64.DEFAULT);
						    byte[] decode2 = Base64.decode(initials_insp.toString(), 0);

						    String FILENAME2 = "Initials"+Inspector_id+".jpg";System.out.println("FILENAME2 "+FILENAME2);
							FileOutputStream fos2 = openFileOutput(FILENAME2, Context.MODE_WORLD_READABLE);
							fos2.write(decode2);
							fos2.close();	
						    
						}
						catch (Exception e1){
						
							System.out.println("Imagge Initials disp = "+e1.getMessage());
						}
						
						try
						{
							URL ulrn3 = new URL(CompanyLogo);
						    HttpURLConnection con3 = (HttpURLConnection)ulrn3.openConnection();
						    InputStream is3 = con3.getInputStream();
						    Bitmap bmp3 = BitmapFactory.decodeStream(is3);
						   
						    ByteArrayOutputStream baos3 = new ByteArrayOutputStream();  
						    bmp3.compress(Bitmap.CompressFormat.PNG, 100, baos3); //bm is the bitmap object   
						    byte[] b3 = baos3.toByteArray();
						    String companylogo = Base64.encodeToString(b3, Base64.DEFAULT);
						    byte[] decode3 = Base64.decode(companylogo.toString(), 0);

						    String FILENAME3 = "CompanyLogo"+Inspector_id+".jpg";System.out.println("FILENAME3 "+FILENAME3);
							FileOutputStream fos3 = openFileOutput(FILENAME3, Context.MODE_WORLD_READABLE);
							fos3.write(decode3);
							fos3.close();	
						    
						}
						catch (Exception e1){
						
							System.out.println("Company Logo disp = "+e1.getMessage());
						}
						
						
						
						
						Cursor cur = db.SelectTablefunction(db.inspectorlogin," where Ins_Id = '" + Inspector_id+ "'");
						if(cur.getCount()==0)
						{
							System.out.println("INSERT INTO "
									+ db.inspectorlogin
									+ " (Ins_Id,Ins_FirstName,Ins_MiddleName,Ins_LastName,Ins_Address1,Ins_Address2,Ins_City,Ins_State,Ins_County," +
									"Ins_Zip,Ins_Phone,Ins_Email,Ins_CompanyName,Ins_CompanyId,Ins_Company_desc,Ins_Company_Logo,Ins_CompanyPhone,Ins_CompanyEmail,Ins_UserName,Ins_Password," +
									"Ins_Flag1,Android_status,Ins_Headshot,Ins_Signature,Ins_Initials," +
									"Ins_rememberpwd,LoginStatus,Fld_licencetype,Fld_licenceno,Fld_licenceexpirydate)"
									+ " VALUES ('" + Inspector_id + "'," + 
									"'"+db.encode(Insp_fname) + "','"+db.encode(Insp_mname)+"','"+db.encode(Insp_lname)+"',"+
									"'"+db.encode(Insp_address1) + "','"+ db.encode(Insp_address2) + "','"+ db.encode(Insp_city) + "',"+
									"'"+db.encode(Insp_state) + "','"+ db.encode(Insp_county) + "','"+Insp_zip+ "',"+
									"'"+db.encode(Insp_contphone) + "','"+db.encode(Insp_email)+"','"+db.encode(CompanyName)+"','"+Insp_companyid+"'," +
									"'','"+db.encode(CompanyPhone)+"','"+db.encode(CompanyEmail)+"','"+db.encode(Insp_username)+"','"+db.encode(Insp_username)+"','"+db.encode(Insp_password)+"'," +
									"'"+Ins_Flag+"','"+androidstatus+"','"+Insp_headshot+"','" +db.encode(Insp_signature) + "',"+
									"'"+db.encode(Insp_initials)+"','"+0+"','"+LoginStatus+"',"+
									"'"+db.encode(Insp_primlictype)+"','"+db.encode(Insp_primlicno)+"','"+db.encode(Insp_licexpirydtae)+"'");
							
							db.wind_db.execSQL("INSERT INTO "
									+ db.inspectorlogin
									+ " (Ins_Id,Ins_FirstName,Ins_MiddleName,Ins_LastName,Ins_Address1,Ins_Address2,Ins_City,Ins_State,Ins_County," +
									"Ins_Zip,Ins_Phone,Ins_Email,Ins_CompanyName,Ins_CompanyId,Ins_Company_desc,Ins_Company_Logo,Ins_CompanyPhone,Ins_CompanyEmail,Ins_UserName,Ins_Password," +
									"Ins_Flag1,Android_status,Ins_Headshot,Ins_Signature,Ins_Initials," +
									"Ins_rememberpwd,LoginStatus,Fld_licencetype,Fld_licenceno,Fld_licenceexpirydate)"
									+ " VALUES ('" + Inspector_id + "'," + 
									"'"+db.encode(Insp_fname) + "','"+db.encode(Insp_mname)+"','"+db.encode(Insp_lname)+"',"+
									"'"+db.encode(Insp_address1) + "','"+ db.encode(Insp_address2) + "','"+ db.encode(Insp_city) + "',"+
									"'"+db.encode(Insp_state) + "','"+ db.encode(Insp_county) + "','"+Insp_zip+ "',"+
									"'"+db.encode(Insp_contphone) + "','"+db.encode(Insp_email)+"','"+db.encode(CompanyName)+"','"+Insp_companyid+"'," +
									"'','"+db.encode(CompanyLogo)+"','"+db.encode(CompanyPhone)+"','"+db.encode(CompanyEmail)+"','"+db.encode(Insp_username)+"','"+db.encode(Insp_password)+"'," +
									"'"+Ins_Flag+"','"+androidstatus+"','"+Insp_headshot+"','" +db.encode(Insp_signature) + "',"+
									"'"+db.encode(Insp_initials)+"','"+0+"','"+LoginStatus+"',"+
									"'"+db.encode(Insp_primlictype)+"','"+db.encode(Insp_primlicno)+"','"+db.encode(Insp_licexpirydtae)+"')");
							show_handler=1;
						    handler.sendEmptyMessage(0); 
						}
						else
						{
							System.out.println(" UPDATE "+db.inspectorlogin+" SET Ins_FirstName='"+db.encode(Insp_fname) + "'," +
									"Ins_MiddleName='"+db.encode(Insp_mname)+"',Ins_LastName='"+db.encode(Insp_lname)+"',Ins_Address1='"+db.encode(Insp_address1)+"'," +
									"Ins_Address2='"+db.encode(Insp_address2)+"',Ins_City='"+db.encode(Insp_city)+"',Ins_State='"+db.encode(Insp_state)+"'," +
									"Ins_County='"+db.encode(Insp_county)+"',Ins_Zip='"+Insp_zip+"',Ins_Phone='"+db.encode(Insp_contphone)+"'," +
									"Ins_Email='"+db.encode(Insp_email)+"',Ins_CompanyName='"+db.encode(CompanyName)+"',Ins_CompanyPhone='"+db.encode(CompanyPhone)+"'," +
									"Ins_Company_Logo='"+db.encode(CompanyLogo)+"',Ins_CompanyEmail='"+db.encode(CompanyEmail)+"',Ins_Flag1='"+Ins_Flag+"'," +
									"Ins_Headshot='"+Insp_headshot+"',Ins_Signature='"+db.encode(Insp_signature)+"',Ins_Initials='"+db.encode(Insp_initials)+"'," +
									"Fld_licencetype='"+db.encode(Insp_primlictype)+"',Fld_licenceno='"+db.encode(Insp_primlicno)+"',Fld_licenceexpirydate='"+db.encode(Insp_licexpirydtae)+"' where Ins_Id='"+Inspector_id+"'");
							
							db.wind_db.execSQL(" UPDATE "+db.inspectorlogin+" SET Ins_FirstName='"+db.encode(Insp_fname) + "'," +
									"Ins_MiddleName='"+db.encode(Insp_mname)+"',Ins_LastName='"+db.encode(Insp_lname)+"',Ins_Address1='"+db.encode(Insp_address1)+"'," +
									"Ins_Address2='"+db.encode(Insp_address2)+"',Ins_City='"+db.encode(Insp_city)+"',Ins_State='"+db.encode(Insp_state)+"'," +
									"Ins_County='"+db.encode(Insp_county)+"',Ins_Zip='"+Insp_zip+"',Ins_Phone='"+db.encode(Insp_contphone)+"'," +
									"Ins_Email='"+db.encode(Insp_email)+"',Ins_CompanyName='"+db.encode(CompanyName)+"',Ins_CompanyPhone='"+db.encode(CompanyPhone)+"'," +
									"Ins_Company_Logo='"+db.encode(CompanyLogo)+"',Ins_CompanyEmail='"+db.encode(CompanyEmail)+"',Ins_Flag1='"+Ins_Flag+"'," +
									"Ins_Headshot='"+Insp_headshot+"',Ins_Signature='"+db.encode(Insp_signature)+"',Ins_Initials='"+db.encode(Insp_initials)+"'," +
									"Fld_licencetype='"+db.encode(Insp_primlictype)+"',Fld_licenceno='"+db.encode(Insp_primlicno)+"',Fld_licenceexpirydate='"+db.encode(Insp_licexpirydtae)+"' where Ins_Id='"+Inspector_id+"'");
						}
						
						show_handler = 1;
						handler.sendEmptyMessage(0);
					}
					else
					{
						System.out.println("its inside false");
						status_userid = String.valueOf(inspreg.getProperty("Errormsg"));
						show_handler=2;
						handler.sendEmptyMessage(0);
					}
					
				}
				catch (SocketException e) {
					// TODO Auto-generated catch block
					System.out.println("SocketException"+e.getMessage());
					show_handler=3;
					handler.sendEmptyMessage(0);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("IOException"+e.getMessage());
					show_handler=3;
					handler.sendEmptyMessage(0);
					
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					System.out.println("XmlPullParserException"+e.getMessage());
					show_handler=3;
					handler.sendEmptyMessage(0);
					
				}
				catch (Exception e) {
					// TODO: handle exception
					System.out.println("Eee"+e.getMessage());
					show_handler=4;
					handler.sendEmptyMessage(0);
				}
			}
    	}.start();
	}
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {


			cf.pd.dismiss();
			if(show_handler == 1)
			{
				show_handler=0;	
				if(Insp_Id==0)
				{
					show_success_popup();
				}
				else
				{
					cf.ShowToast("Inspector Profile Updated Successfully");
					cf.gohome();
				}
			}
			else if(show_handler == 2)
			{
				cf.ShowToast(status_userid);
				show_handler=0;
			}
			else if(show_handler == 3)
			{
				cf.ShowToast("There is a problem on your Network. Please try again later with better Network");
				show_handler=0;
			}
			else if(show_handler == 4)
			{
				cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");
				show_handler=0;
			}
			
		}
    };


	protected void show_success_popup() {
		// TODO Auto-generated method stub
		final Dialog add_dialog = new Dialog(Registration.this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//add_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.LTGRAY));
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.registrationcustom);
		 
		((TextView)add_dialog.findViewById(R.id.redirect)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(Registration.this,WindMitInspection.class);
				intent.putExtra("status", "register");
				//intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
		
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(Insp_Id==0)
			{
				Intent intimg = new Intent(Registration.this,WindMitInspection.class);
				startActivity(intimg);
				finish();
			}
			else
			{
				Intent intimg = new Intent(Registration.this,HomeScreen.class);
				startActivity(intimg);
				finish();
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}
