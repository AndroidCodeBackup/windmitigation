package idsoft.inspectiondepot.windmitinspection;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class GeneralHazInfo extends Activity {
	private static final android.view.View.OnClickListener OnClickListener = null;
	EditText s7;
	Button additionalbtn,gendatabtn;
	private static final int visibility = 0;
	LinearLayout lin081,general_parrant,general_type1,general_lin;
	private static final String TAG = null;
	EditText etinsuredother,etobservother,etoccupancyother;
	String insuredothertxt="",observothertxt="",occupancyothertxt="";
	int rws,focus=0;
	String homeid, InspectionType, status, strloc1, strloc2, strloc3, strloc4,
			strpf1, strpf2, strpf3, strpf4, strob1, strob2, strob3, strob4,
			strob5, strpp1, strpp2, strpp3, strpp4, strpp5, strpp6, strpp7,
			strhz1, strhz2, strhz3, strhz4, strhz5, strhz6, strhz7, strhz8,
			strhz9, strhz10, strhz11, strhz12, strhz13, strhz14, strhz15,
			strhz16, strhz17, strhz18, strhz19, strhz20, strhz21, strhz22,
			strhz23, strhz24, strhz25, strdata1, strdata2, strdata3, strdata4,
			strdata5, strdata6, stroccup, strvacant, strbuildsz, strother,
			selvac, selvacflg, selnd, strcomm,InsuredIs="",Location1="",Observation1="",PerimeterPoolFence="",Vicious="",PoolPresent="";
	ImageView gentick, loctick, plfntick, pptick, obstick, haztick;
	int value, Count, c, b = 0, nd, ii, inc;
	String[] array_spinner, array_insured, array_obser, array_occ;
	int commentlength,data1flg, data2flg, data3flg, data4flg, data5flg, data6flg, qrws,
			loc1flg, loc2flg, loc3flg, loc4flg, obs1flg, obs2flg, obs3flg,
			obs4flg, obs5flg, pf1flg, pf2flg, pf3flg, pf4flg, pp1flg, pp2flg,
			pp3flg, pp4flg, pp5flg, pp6flg, pp7flg, hz1flg, hz2flg, hz3flg,
			hz4flg, hz5flg, hz6flg, hz7flg, hz8flg, hz9flg, hz10flg, hz11flg,
			hz12flg, hz13flg, hz14flg, hz15flg, hz16flg, hz17flg, hz18flg,
			hz19flg, hz20flg, hz21flg, hz22flg, hz23flg, hz24flg, hz25flg,
			occflg, vacflg;
	String seldata1, seldata2, seldata3, seldata4, seldata5, seldata6,
			seloccup, selbuildsz, selother, selflg, selloc1, selloc2, selloc3,
			selloc4;
	Cursor c2;
	View v1;
	int o, va;
	TextView policyholderinfo,general_TV_type1;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	RelativeLayout rellayt;
	CheckBox chkinclude;
	EditText etcomments;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	TextWatcher watcher;
	TableLayout exceedslimit1;
	Button home;
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf = new CommonFunctions(this);
		db = new DatabaseFunctions(this);
		wb = new WebserviceFunctions(this);
		/** Creating Genral conditon table **/
				
		db.CreateTable(7);
		db.CreateTable(9);
		db.CreateTable(15);
		
		Bundle bunhomeId = getIntent().getExtras();
		if (bunhomeId != null) {
			cf.getExtras(bunhomeId);
		}

		setContentView(R.layout.general);
		db.getInspectorId();
		cf.getDeviceDimensions();
		
		/** menu **/
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(GeneralHazInfo.this, 6, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		LinearLayout sublayout = (LinearLayout) findViewById(R.id.relativeLayout4);
		sublayout.addView(new MyOnclickListener(GeneralHazInfo.this, 29,cf, 1));
	
		Declaration();		
		
		setValue();

	}
	private void setValue() {
		// TODO Auto-generated method stub		
		try {
			db.CreateTable(7);
			Cursor c2 = db.SelectTablefunction(db.Questions, " where SRID='" + cf.Homeid + "'");
			if (c2.getCount() > 0) 
			{
				c2.moveToFirst();
				System.out.println("INcluc="+c2.getString(c2.getColumnIndex("GeneralHazardInclude")));
				if (c2.getString(c2.getColumnIndex("GeneralHazardInclude")).equals("1")) 
				{
					((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.VISIBLE);
					//((Button) findViewById(R.id.save)).setVisibility(v1.VISIBLE);
					inc = 1;
					chkinclude.setChecked(true);
					
					show();
				} 
				else
				{
					((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.INVISIBLE);
					//((Button) findViewById(R.id.save)).setVisibility(v1.GONE);
					inc = 2;
					chkinclude.setChecked(false);
					home.setVisibility(visibility);
				}
			}
		}
		catch(Exception e)
		{
			
		}
	}
	private void Declaration() {
		// TODO Auto-generated method stub
		
		array_spinner = new String[5];
		array_spinner[0] = "Not Applicable";
		array_spinner[1] = "No Access";
		array_spinner[2] = "Not Determined";
		array_spinner[3] = "Yes";
		array_spinner[4] = "No";

		array_insured = new String[6];
		array_insured[0] = "Owner";
		array_insured[1] = "Corporation";
		array_insured[2] = "Individual";
		array_insured[3] = "Tenant";
		array_insured[4] = "Partnership";
		array_insured[5] = "Other";

		array_obser = new String[5];
		array_obser[0] = "Interior Only";
		array_obser[1] = "Exterior Only";
		array_obser[2] = "Interior & Exterior";
		array_obser[3] = "Common Areas Only";
		array_obser[4] = "Other";

		array_occ = new String[8];
		array_occ[0] = "Single Family Home";
		array_occ[1] = "Condominium";
		array_occ[2] = "Town Home";
		array_occ[3] = "Duplex";
		array_occ[4] = "Triples";
		array_occ[5] = "Quadplex";
		array_occ[6] = "Apartment";
		array_occ[7] = "Other";
		
		etcomments = (EditText) findViewById(R.id.etcomment);
		etcomments.setOnTouchListener(new TouchFoucs(etcomments));
		etcomments.addTextChangedListener(new TextWatchLimit(etcomments, 500, ((TextView) findViewById(R.id.TVcommentslimit))));
		chkinclude = (CheckBox) findViewById(R.id.include);
		chkinclude.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					inc = 1;
					chkinclude.setChecked(true);
					((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.VISIBLE);
					//((Button) findViewById(R.id.save)).setVisibility(v1.VISIBLE);
					show();
				} else {
					inc = 2;
					chkhzdrws();
					if(rws==0)
					{
						((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.INVISIBLE);
						//((Button) findViewById(R.id.save)).setVisibility(v1.GONE);
						chkinclude.setChecked(false);
					}
					else
					{
						AlertDialog.Builder bl = new Builder(GeneralHazInfo.this);
						bl.setTitle("Confirmation");
						bl.setIcon(R.drawable.alertmsg);
						bl.setMessage(Html.fromHtml("Do you want to clear the saved data?"));
						bl.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,int id) {
									
										db.wind_db.execSQL(" DELETE FROM "+db.GeneralHazInfo+" WHERE Srid='"+cf.Homeid+"'");
										updategenhzinclue(0);
										((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.INVISIBLE);
										//((Button) findViewById(R.id.save)).setVisibility(v1.GONE);
										chkinclude.setChecked(false);
										cf.ShowToast("Deleted Successfully");
									 }									
						});
						bl.setNegativeButton("No",
								new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {
										chkinclude.setChecked(true);
										 }
				        });
						AlertDialog al=bl.create();
						al.setIcon(R.drawable.alertmsg);
						al.setCancelable(false);
						al.show();
					}
					
				}
			}
		});
		if (inc == 1) {
			((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.VISIBLE);
			//((Button) findViewById(R.id.save)).setVisibility(v1.VISIBLE);
			show();
		} else {
			((LinearLayout) findViewById(R.id.genhazardlin)).setVisibility(v1.INVISIBLE);
			//((Button) findViewById(R.id.save)).setVisibility(v1.GONE);
			//updategenhzinclue(inc);
		}
		
		gentick = (ImageView) findViewById(R.id.genovr);
		loctick = (ImageView) findViewById(R.id.locovr);
		obstick = (ImageView) findViewById(R.id.obsovr);
		plfntick = (ImageView) findViewById(R.id.polfnovr);		
		pptick = (ImageView) findViewById(R.id.ppovr);
		haztick = (ImageView) findViewById(R.id.hazovr);
	}
	
	protected void updategenhzinclue(int inc1) {
		// TODO Auto-generated method stub
		try {
			Cursor c2 = db.SelectTablefunction(db.Questions, " where SRID='" + cf.Homeid + "'");
			if (c2.getCount() == 0) 
			{
				db.wind_db.execSQL("INSERT INTO "
						+ db.Questions
						+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
						+ "VALUES ('" + cf.Homeid + "','','','" + 0 + "','" + 0 + "','" + 0
						+ "','','','','','" + 0 + "','" + 0 + "','','" + 0
						+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
						+ "','','" + 0 + "','" + 0 + "','','','','','','','','"
						+ 0 + "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
						+ "','" + 0 + "','" + 0 + "','" + 0 + "','','" + 0
						+ "','" + 0 + "','','" + 0 + "','','','" + cf.datewithtime + "','"
						+ inc1 + "')");
			} 
			else 
			{
				db.wind_db.execSQL("UPDATE " + db.Questions + " SET GeneralHazardInclude='" + inc1
						+ "',ModifyDate ='" + cf.datewithtime + "' WHERE SRID ='" + cf.Homeid + "'");
				System.out.println("UPDATE " + db.Questions + " SET GeneralHazardInclude='" + inc1
						+ "',ModifyDate ='" + cf.datewithtime + "' WHERE SRID ='" + cf.Homeid + "'");
			}
		}
		catch(Exception e)
		{
			System.out.println("ee"+e.getMessage());
		}
	}
	
	private void show() 
	{
		// TODO Auto-generated method stub
		//updategenhzinclue(inc);System.out.println("inside show");
		chkhzdrws();
		System.out.println("rws = "+rws);
		if (rws != 0) 
		{
			retrievedata();
			b = 1;
			etcomments.setText(strcomm);

		}
		
		changeimage(cf.Homeid);
	}
	
	private void chkhzdrws() {
		// TODO Auto-generated method stub
		
		try {
			Cursor c1 = db.wind_db.rawQuery("SELECT * FROM " + db.GeneralHazInfo + " WHERE Srid='" + cf.Homeid + "'", null);
			rws = c1.getCount();
			if(rws>0)
			{
				c1.moveToFirst();
				InsuredIs = db.decode(c1.getString(c1.getColumnIndex("InsuredIs")));
				Location1 = db.decode(c1.getString(c1.getColumnIndex("Location1")));
				Observation1 = db.decode(c1.getString(c1.getColumnIndex("Observation1")));
				PerimeterPoolFence = db.decode(c1.getString(c1.getColumnIndex("PerimeterPoolFence")));
				PoolPresent = db.decode(c1.getString(c1.getColumnIndex("PoolPresent")));
				Vicious = db.decode(c1.getString(c1.getColumnIndex("Vicious")));
			}
		} catch (Exception e) {
			Log.i(TAG, "error= " + e.getMessage());
		}
	}
	private void data() {
		// TODO Auto-generated method stub
		System.out.println("inside data");
		final Dialog dialog = new Dialog(GeneralHazInfo.this);
		dialog.setContentView(R.layout.generaldata);
		dialog.setTitle("GENERAL DATA");
		dialog.setCancelable(true);
		
		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		etinsuredother = (EditText) dialog.findViewById(R.id.etinsuredother);
		
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_insured);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		s.setOnItemSelectedListener(new MyOnItemSelectedListenerdata());

		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		etobservother = (EditText) dialog.findViewById(R.id.etobservother);
		ArrayAdapter adapter1 = new ArrayAdapter(this,android.R.layout.simple_spinner_item, array_obser);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerdata1());

		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		etoccupancyother = (EditText) dialog.findViewById(R.id.etoccupancyother);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_occ);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerdata2());

		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner07);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner08);
		Spinner s5 = (Spinner) dialog.findViewById(R.id.Spinner09);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerdata3());
		ArrayAdapter adapter4 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		s4.setAdapter(adapter4);
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerdata4());
		ArrayAdapter adapter5 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		s5.setAdapter(adapter5);
		s5.setOnItemSelectedListener(new MyOnItemSelectedListenerdata5());

		final CheckBox chkbx1 = (CheckBox) dialog.findViewById(R.id.chk1);
		final CheckBox chkbx2 = (CheckBox) dialog.findViewById(R.id.chk2);
		final CheckBox chkbx3 = (CheckBox) dialog.findViewById(R.id.chk3);

		final EditText et1 = (EditText) dialog.findViewById(R.id.ed1);
		final EditText et2 = (EditText) dialog.findViewById(R.id.ed2);
		final EditText et3 = (EditText) dialog.findViewById(R.id.ed3);
		
		
		et3.addTextChangedListener(new TextWatcher1(et3));
		et3.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cf.setFocus(et3);
				return false;
			}
		});
		
		
		
		final EditText et4 = (EditText) dialog.findViewById(R.id.ed4);
		et4.setOnTouchListener(new OnTouchListener() {
			
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				cf.setFocus(et4);
				return false;
			}
		});
		et1.setEnabled(false);
		et2.setEnabled(false);
		chkhzdrws();
		if (rws > 0) {
			retrievedata();
			if(strdata1.equals("Other")){
				etinsuredother.setText(db.decode(insuredothertxt));
			}
			if(strdata2.equals("Other")){
				etobservother.setText(db.decode(observothertxt));
			}
			if(strdata3.equals("Other")){
				etoccupancyother.setText(db.decode(occupancyothertxt));
			}
			int spinnerPosition = adapter.getPosition(strdata1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strdata2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strdata3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strdata4);
			s3.setSelection(spinnerPosition3);
			int spinnerPosition4 = adapter4.getPosition(strdata5);
			s4.setSelection(spinnerPosition4);
			int spinnerPosition5 = adapter5.getPosition(strdata6);
			s5.setSelection(spinnerPosition5);
			if (selflg.equals("1")) {
				if (!seloccup.equals("")) {
					chkbx1.setChecked(true);
				} else {
					chkbx1.setChecked(false);
				}
				et1.setText(seloccup);
				et1.setEnabled(true);
				chkbx3.setChecked(false);
			} else {

			}
			if (selvac.equals("1")) {
				if (!selvacflg.equals("")) {
					chkbx2.setChecked(true);
				} else {
					chkbx2.setChecked(false);
				}
				et2.setText(selvacflg);
				et2.setEnabled(true);
				chkbx3.setChecked(false);
			} else {

			}
			if (selnd.equals("1")) {
				System.out.println("chkbox 3 is checked");
				chkbx3.setChecked(true);
				et2.setText("");
				et1.setText("");
				chkbx1.setChecked(false);
				chkbx2.setChecked(false);
			} else {
				System.out.println("chkbox 3 is unchecked");
			}
			
			db.getPHinformation(cf.Homeid);
			if(selbuildsz.equals(""))
			{
				et3.setText(db.bsize);				
			}
			else
			{
				et3.setText(selbuildsz);
			}
			et4.setText(db.decode(selother));

		}
		else
		{
			et3.setText(db.bsize);		
		}

		chkbx1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					c = 1;
					chkbx1.setChecked(true);
					nd = 0;
					cf.setFocus(et1);
					chkbx3.setChecked(false);
					et1.setEnabled(true);
					et1.setCursorVisible(true);

				} else {
					c = 0;
					et1.setEnabled(false);
					et1.setText("");
				}
			}
		});
		chkbx2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					c = 2;
					chkbx2.setChecked(true);
					nd = 0;
					chkbx3.setChecked(false);
					cf.setFocus(et2);
					et2.setEnabled(true);
					et2.setCursorVisible(true);

				} else {
					c = 0;
					et2.setEnabled(false);
					et2.setText("");
				}
			}
		});
		chkbx3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// Perform action on clicks, depending on whether it's now
				// checked
				if (((CheckBox) v).isChecked()) {
					c = 3;
					chkbx1.setChecked(false);
					nd = 1;
					chkbx2.setChecked(false);
					chkbx3.setChecked(true);
					et1.setEnabled(false);
					et2.setEnabled(false);
					et1.setText("");
					et2.setText("");
				} else {
					c = 0;
				}
			}
		});
		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
System.out.println("creer "+c + " nd = "+nd);
				if ((c == 3) || chkbx3.isChecked()==true)  {
					System.out.println("ccc "+c);
					nd = 1;
					occflg = 0;
					vacflg = 0;
					stroccup = "";
					strvacant = "";
				} else {
					System.out.println("this is");
					if (chkbx1.isChecked() == true) {
						occflg = 1;
						stroccup = et1.getText().toString();
					} else {
						occflg = 0;
						stroccup = "";
					}
					if (chkbx2.isChecked() == true) {

						vacflg = 1;
						strvacant = et2.getText().toString();
					} else {
						System.out.println("n else "+nd);
						vacflg = 0;
						strvacant = "";
					}
				}
				strbuildsz = et3.getText().toString();
				strother = et4.getText().toString();
				
				insuredothertxt=etinsuredother.getText().toString();
				observothertxt=etobservother.getText().toString();
				occupancyothertxt=etoccupancyother.getText().toString();
				
				boolean[] othertext =new boolean[3];
				othertext[0]=(strdata1.equals("Other"))? ((insuredothertxt.trim().equals(""))? false:true):true;
				othertext[1]=(strdata2.equals("Other"))? ((observothertxt.trim().equals(""))? false:true):true;
				othertext[2]=(strdata3.equals("Other"))? ((occupancyothertxt.trim().equals(""))? false:true):true;
				
				if (rws == 0) {
					if(othertext[0]==true){
						if(othertext[1]==true){
							if(othertext[2]==true){
					if (chkbx1.isChecked() == true
							|| chkbx2.isChecked() == true
							|| chkbx3.isChecked() == true) {
						if (chkbx1.isChecked() == true
								|| chkbx2.isChecked() == true) {
							try {
								if (!et1.getText().toString().equals("")) {
									try
									{
										o = Integer.parseInt(stroccup);									
									}
									catch(Exception e)
									{
										o=0;											
									}									
								}
								if (!et2.getText().toString().equals("")) {
									try
									{
										va = Integer.parseInt(strvacant);							
									}
									catch(Exception e)
									{
										va=0;											
									}		
									
								}
							} catch (Exception e) {

							}
							if (stroccup.equals("") && strvacant.equals("")) {
								cf.ShowToast("Building Occupancy Percentage should not be empty.");
							} else {
								if ((o + va) == 100) {
									if (o == 100) {
										chkbx2.setChecked(false);
									} else if (va == 100) {
										chkbx1.setChecked(false);
									}
										db.wind_db.execSQL("INSERT INTO "
											+ db.GeneralHazInfo
											+ " (Srid,InsuredIs,InsuredOther,OccupancyType,OcccupOther,ObservationType,ObservOther,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
											+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
											+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
											+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
											+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
											+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
											+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
											+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
											+ " VALUES ('"
											+ cf.Homeid
											+ "','"
											+ strdata1
											+ "','"+db.encode(insuredothertxt)+"','"
											+ strdata3
											+ "','"+db.encode(occupancyothertxt)+"','"
											+ strdata2
											+ "','"+db.encode(observothertxt)+"','"
											+ stroccup
											+ "','"
											+ occflg
											+ "','"
											+ vacflg
											+ "','"
											+ strvacant
											+ "','"
											+ nd
											+ "','"
											+ strdata4
											+ "','"
											+ strbuildsz
											+ "','"
											+ strdata5
											+ "','"
											+ strdata6
											+ "','"
											+ db.encode(strother)
											+ "',"
											+ "'','','','',"
											+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
									
											Insertsubmitcheck();
										
										
									cf.ShowToast("General data saved successfully.");
									dialog.dismiss();
									gentick.setVisibility(v1.VISIBLE);
								} else if ((o + va) < 100) {
									cf.ShowToast("Building Occupancy Percentage should be equal to 100%.");
								} else {
									cf.ShowToast("Building Occupancy Percentage should not be greater than 100%.");
								}

							}
						} else {
							db.wind_db.execSQL("INSERT INTO "
									+ db.GeneralHazInfo
									+ " (Srid,InsuredIs,InsuredOther,OccupancyType,OcccupOther,ObservationType,ObservOther,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
									+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
									+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
									+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
									+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
									+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
									+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
									+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
									+ " VALUES ('"
									+ cf.Homeid
									+ "','"
									+ strdata1
									+ "','"+db.encode(insuredothertxt)+"','"
									+ strdata3
									+ "','"+db.encode(occupancyothertxt)+"','"
									+ strdata2
									+ "','"+db.encode(observothertxt)+"','"
									+ stroccup
									+ "','"
									+ occflg
									+ "','"
									+ vacflg
									+ "','"
									+ strvacant
									+ "','"
									+ nd
									+ "','"
									+ strdata4
									+ "','"
									+ strbuildsz
									+ "','"
									+ strdata5
									+ "','"
									+ strdata6
									+ "','"
									+ db.encode(strother)
									+ "',"
									+ "'','','','',"
									+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
							
							
							cf.ShowToast("General data saved successfully.");
							dialog.dismiss();
						}
						gentick.setVisibility(v1.VISIBLE);
					} else {
						cf.ShowToast("Please select Building Occupancy.");

					}
					} else {
						cf.ShowToast("Please enter the other text for occupancy type.");
					}
  				   } else {
						cf.ShowToast("Please enter the other text for observation type.");

  				   }
					}else{
						cf.ShowToast("Please enter the other text for insured is.");
					}
					
				} else {
					if(othertext[0]==true){
						if(othertext[1]==true){
							if(othertext[2]==true){
					if (chkbx1.isChecked() == true
							|| chkbx2.isChecked() == true
							|| chkbx3.isChecked() == true) {
						if (chkbx1.isChecked() == true
								|| chkbx2.isChecked() == true) {
							o = 0;
							va = 0;
							try {
								if (!et1.getText().toString().equals("")) {
									try
									{
										o = Integer.parseInt(et1.getText()
												.toString());								
									}
									catch(Exception e)
									{
										o=0;											
									}	
									
									
								}
								if (!et2.getText().toString().equals("")) {
									try
									{
										va = Integer.parseInt(et2.getText()
												.toString());							
									}
									catch(Exception e)
									{
										va=0;											
									}	
									
								}
							} catch (Exception e) {

							}

							if (et1.getText().toString().equals("")
									&& et2.getText().toString().equals("")) {
								cf.ShowToast("Building Occupancy Percentage should not be empty.");
							} else {
								if ((o + va) == 100) {
									if (o == 100) {
										chkbx2.setChecked(false);
									} else if (va == 100) {
										chkbx1.setChecked(false);
									}
									System.out.println("its 111");
									db.wind_db.execSQL("UPDATE "
											+ db.GeneralHazInfo
											+ " SET InsuredIs='" + strdata1
											+ "',InsuredOther='" + db.encode(insuredothertxt)
											+ "',OccupancyType='" + strdata3
											+ "',OcccupOther='" + db.encode(occupancyothertxt)
											+ "',ObservationType='" + strdata2
											+ "',ObservOther='" + db.encode(observothertxt)
											+ "'," + "OccupiedPercent='"
											+ stroccup + "',OccupiedFlag='"
											+ occflg + "',Vacant='" + vacflg
											+ "',VacantPercent='" + strvacant
											+ "',NotDetermined='" + nd
											+ "',PermitConfirmed='" + strdata4
											+ "',BuildingSize='" + strbuildsz
											+ "'," + "BalconyPresent='"
											+ strdata5
											+ "',AdditionalStructure='"
											+ strdata6 + "',OtherLocation='"
											+ db.encode(strother)
											+ "' WHERE Srid ='" + cf.Homeid
											+ "'");
									cf.ShowToast("General data saved successfully.");
									dialog.dismiss();
								} else if ((o + va) < 100) {
									cf.ShowToast("Building Occupancy Percentage should be equal to 100%.");
								} else {
									cf.ShowToast("Building Occupancy Percentage should not be greater than 100%.");
								}

							}

						} else {System.out.println("its 2222");
							db.wind_db.execSQL("UPDATE " + db.GeneralHazInfo
									+ " SET InsuredIs='" + strdata1
									+ "',InsuredOther='" + db.encode(insuredothertxt)
									+ "',OccupancyType='" + strdata3
									+ "',OcccupOther='" + db.encode(occupancyothertxt)
									+ "',ObservationType='" + strdata2 
									+ "',ObservOther='" + db.encode(observothertxt)
									+ "',OccupiedPercent='" + stroccup
									+ "',OccupiedFlag='" + occflg
									+ "',Vacant='" + vacflg
									+ "',VacantPercent='" + strvacant
									+ "',NotDetermined='" + nd
									+ "',PermitConfirmed='" + strdata4
									+ "',BuildingSize='" + strbuildsz + "',"
									+ "BalconyPresent='" + strdata5
									+ "',AdditionalStructure='" + strdata6
									+ "',OtherLocation='"
									+ db.encode(strother)
									+ "' WHERE Srid ='" + cf.Homeid + "'");
							cf.ShowToast("General data saved successfully.");
							dialog.dismiss();
						}
					} else {
						cf.ShowToast("Please select Building Occupancy.");

					}
					} else {
						cf.ShowToast("Please enter the other text for occupancy type.");
					}
  				   } else {
						cf.ShowToast("Please enter the other text for observation type.");

  				   }
					}else{
						cf.ShowToast("Please enter the other text for insured is.");
					}
				}

			}

		});

		dialog.show();
		dialog.setCancelable(false);
	}
	protected void Insertsubmitcheck() {
		// TODO Auto-generated method stub
		try {
			Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "+ db.SubmitCheckTable+ " WHERE Srid='"+ cf.Homeid + "'", null);
			if (c2.getCount() == 0) {
				db.wind_db.execSQL("INSERT INTO "
						+ db.SubmitCheckTable
						+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
						+ "VALUES ('"
						+ db.Insp_id
						+ "','"
						+ cf.Homeid
						+ "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1)");
			} else {
				db.wind_db.execSQL("UPDATE "
						+ db.SubmitCheckTable
						+ " SET fld_hazarddata='1' WHERE Srid ='"
						+ cf.Homeid
						+ "' and InspectorId='"
						+ db.Insp_id + "'");
			}
		} catch (Exception e) {

		}
	}
	public void location() {
		final Dialog dialog = new Dialog(GeneralHazInfo.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("LOCATION");
		dialog.setCancelable(true);

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		s.setOnItemSelectedListener(new MyOnItemSelectedListener());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListener1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListener2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListener3());
		chkhzdrws();
		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazInfo
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','"
							+ strloc1
							+ "','"
							+ strloc2
							+ "','"
							+ strloc3
							+ "','"
							+ strloc4
							+ "',"
							+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					
				} else {
					db.wind_db.execSQL("UPDATE " + db.GeneralHazInfo
							+ " SET Location1='" + strloc1 + "',Location2='"
							+ strloc2 + "',Location3='" + strloc3
							+ "',Location4='" + strloc4 + "'WHERE Srid ='"
							+ cf.Homeid + "'");

				}
				cf.ShowToast("Location saved successfully.");
				changeimage(cf.Homeid);
				dialog.dismiss();
			}

		});

		if (rws > 0) {
			retrievedata();

			int spinnerPosition = adapter.getPosition(strloc1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strloc2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strloc3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strloc4);
			s3.setSelection(spinnerPosition3);

		}
		dialog.show();
		dialog.setCancelable(false);
	}
	
	public void poolfence() {
		final Dialog dialog = new Dialog(GeneralHazInfo.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("PERIMETER POOL FENCE");
		dialog.setCancelable(true);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Perimeter Pool Fence");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Self Latches / locks to Gate");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Professionally Installed");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Pool Fence in Disrepair");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);

		s.setOnItemSelectedListener(new MyOnItemSelectedListenerpf());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerpf1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerpf2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerpf3());
		chkhzdrws();
		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazInfo
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','','','','','','','','','',"
							+ "'"
							+ strpf1
							+ "','"
							+ strpf4
							+ "','"
							+ strpf2
							+ "','"
							+ strpf3
							+ "','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					
				} else {
					db.wind_db.execSQL("UPDATE " + db.GeneralHazInfo
							+ " SET PerimeterPoolFence='" + strpf1
							+ "',PoolFenceDisrepair='" + strpf4 + "',"
							+ "SelfLatch='" + strpf2 + "',ProfesInstall='"
							+ strpf3 + "' WHERE Srid ='" + cf.Homeid + "'");

				}
				cf.ShowToast("Perimeter Pool Fence saved successfully.");
				changeimage(cf.Homeid);
				dialog.dismiss();
			}

		});
		if (rws > 0) {
			retrievedata();
			int spinnerPosition = adapter.getPosition(strpf1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strpf2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strpf3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strpf4);
			s3.setSelection(spinnerPosition3);

		}
		dialog.show();
		dialog.setCancelable(false);
	}
	public void observation() {
		final Dialog dialog = new Dialog(GeneralHazInfo.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("OBSERVATION");
		dialog.setCancelable(true);
		LinearLayout lin1 = (LinearLayout) dialog.findViewById(R.id.lin05);
		lin1.setVisibility(visibility);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Construction originally intended for current use");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Alterations / Remodeling noted since original construction");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Description");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Visible non-standard / home made / do it yourself alterations");

		TextView txt5 = (TextView) dialog.findViewById(R.id.text5);
		txt5.setText("Permit information for alterations confirmed");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner05);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter4 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s4.setAdapter(adapter4);

		s.setOnItemSelectedListener(new MyOnItemSelectedListenerob());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerob1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerob2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerob3());
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerob4());
		chkhzdrws();

		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazInfo
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','','','','',"
							+ "'"
							+ strob1
							+ "','"
							+ strob2
							+ "','"
							+ strob3
							+ "','"
							+ strob4
							+ "','"
							+ strob5
							+ "','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','')");
					
				} else {
					db.wind_db.execSQL("UPDATE " + db.GeneralHazInfo
							+ " SET Observation1='" + strob1
							+ "',Observation2='" + strob2 + "',"
							+ "Observation3='" + strob3 + "',Observation4='"
							+ strob4 + "',Observation5='" + strob5
							+ "' WHERE Srid ='" + cf.Homeid + "'");

				}
				cf.ShowToast("Observation saved successfully.");
				changeimage(cf.Homeid);
				dialog.dismiss();
			}

		});

		if (rws > 0) {
			retrievedata();
			int spinnerPosition = adapter.getPosition(strob1);
			s.setSelection(spinnerPosition);
			int spinnerPosition1 = adapter1.getPosition(strob2);
			s1.setSelection(spinnerPosition1);
			int spinnerPosition2 = adapter2.getPosition(strob3);
			s2.setSelection(spinnerPosition2);
			int spinnerPosition3 = adapter3.getPosition(strob4);
			s3.setSelection(spinnerPosition3);
			int spinnerPosition4 = adapter4.getPosition(strob5);
			s4.setSelection(spinnerPosition4);

		}
		dialog.show();
		dialog.setCancelable(false);
	}
	public void poolpresent() {
		final Dialog dialog = new Dialog(GeneralHazInfo.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("POOL PRESENT");
		dialog.setCancelable(true);
		LinearLayout lin1 = (LinearLayout) dialog.findViewById(R.id.lin05);
		lin1.setVisibility(visibility);

		LinearLayout lin2 = (LinearLayout) dialog.findViewById(R.id.lin06);
		lin2.setVisibility(visibility);

		LinearLayout lin3 = (LinearLayout) dialog.findViewById(R.id.lin07);
		lin3.setVisibility(visibility);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Pool Present");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Hot tub Present");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Empty in Ground Pool Present");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Pool Slide present");

		TextView txt5 = (TextView) dialog.findViewById(R.id.text5);
		txt5.setText("Diving board present");

		TextView txt6 = (TextView) dialog.findViewById(R.id.text6);
		txt6.setText("Perimeter Pool Enclosure");

		TextView txt7 = (TextView) dialog.findViewById(R.id.text7);
		txt7.setText("Pool enclosure in disrepair");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner05);
		Spinner s5 = (Spinner) dialog.findViewById(R.id.Spinner06);
		Spinner s6 = (Spinner) dialog.findViewById(R.id.Spinner07);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter1 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter2 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter3 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter4 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter5 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		ArrayAdapter adapter6 = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s1.setAdapter(adapter1);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s2.setAdapter(adapter2);
		adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s3.setAdapter(adapter3);
		adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s4.setAdapter(adapter4);
		adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s5.setAdapter(adapter5);
		adapter6.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s6.setAdapter(adapter6);

		s.setOnItemSelectedListener(new MyOnItemSelectedListenerpp());
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerpp1());
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerpp2());
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerpp3());
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerpp4());
		s5.setOnItemSelectedListener(new MyOnItemSelectedListenerpp5());
		s6.setOnItemSelectedListener(new MyOnItemSelectedListenerpp6());
		chkhzdrws();
		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazInfo
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('"
							+ cf.Homeid
							+ "','','','','','"
							+ 0
							+ "','"
							+ 0
							+ "','','"
							+ 0
							+ "','','"
							+ 0
							+ "','','','','','','','','','','','','',"
							+ "'','','','','"
							+ strpp1
							+ "','"
							+ strpp2
							+ "','"
							+ strpp3
							+ "','"
							+ strpp4
							+ "','"
							+ strpp5
							+ "','"
							+ strpp6
							+ "','"
							+ strpp7
							+ "','','','','','','','','','','','','','','','','','','','','','','','','','','')");
									} else {
					db.wind_db.execSQL("UPDATE " + db.GeneralHazInfo
							+ " SET PoolPresent='" + strpp1
							+ "',HotTubPresnt='" + strpp2
							+ "',EmptyGroundPool='" + strpp3 + "',"
							+ "PoolSlide='" + strpp4
							+ "',DivingBoardPoolSlide='" + strpp5
							+ "',PerimeterPoolEncl='" + strpp6
							+ "',PoolEnclosure='" + strpp7 + "' WHERE Srid ='"
							+ cf.Homeid + "'");

				}
				cf.ShowToast("Pool Present saved successfully.");
				changeimage(cf.Homeid);
				dialog.dismiss();
			}

		});

		if (rws > 0) {
			retrievedata();
			int spinnerPosition = adapter.getPosition(strpp1);
			int spinnerPosition1 = adapter1.getPosition(strpp2);
			int spinnerPosition2 = adapter2.getPosition(strpp3);
			int spinnerPosition3 = adapter3.getPosition(strpp4);
			int spinnerPosition4 = adapter4.getPosition(strpp5);
			int spinnerPosition5 = adapter5.getPosition(strpp6);
			int spinnerPosition6 = adapter6.getPosition(strpp7);

			s.setSelection(spinnerPosition);
			s1.setSelection(spinnerPosition1);
			s2.setSelection(spinnerPosition2);
			s3.setSelection(spinnerPosition3);
			s4.setSelection(spinnerPosition4);
			s5.setSelection(spinnerPosition5);
			s6.setSelection(spinnerPosition6);

		}
		dialog.show();
		dialog.setCancelable(false);
	}
	
	public void hazard() {

		final Dialog dialog = new Dialog(GeneralHazInfo.this);
		dialog.setContentView(R.layout.location);
		dialog.setTitle("SUMMARY OF HAZARDS");
		dialog.setCancelable(true);
		LinearLayout lin1 = (LinearLayout) dialog.findViewById(R.id.lin05);
		lin1.setVisibility(visibility);

		LinearLayout lin2 = (LinearLayout) dialog.findViewById(R.id.lin06);
		lin2.setVisibility(visibility);

		LinearLayout lin3 = (LinearLayout) dialog.findViewById(R.id.lin07);
		lin3.setVisibility(visibility);

		LinearLayout lin4 = (LinearLayout) dialog.findViewById(R.id.lin08);

		LinearLayout lin5 = (LinearLayout) dialog.findViewById(R.id.lin09);
		lin5.setVisibility(visibility);

		LinearLayout lin6 = (LinearLayout) dialog.findViewById(R.id.lin010);
		lin6.setVisibility(visibility);

		LinearLayout lin7 = (LinearLayout) dialog.findViewById(R.id.lin011);
		lin7.setVisibility(visibility);

		LinearLayout lin8 = (LinearLayout) dialog.findViewById(R.id.lin012);
		lin8.setVisibility(visibility);

		LinearLayout lin9 = (LinearLayout) dialog.findViewById(R.id.lin013);
		lin9.setVisibility(visibility);

		LinearLayout lin10 = (LinearLayout) dialog.findViewById(R.id.lin014);
		lin10.setVisibility(visibility);

		LinearLayout lin11 = (LinearLayout) dialog.findViewById(R.id.lin015);
		lin11.setVisibility(visibility);

		LinearLayout lin13 = (LinearLayout) dialog.findViewById(R.id.lin017);
		lin13.setVisibility(visibility);

		LinearLayout lin14 = (LinearLayout) dialog.findViewById(R.id.lin018);
		lin14.setVisibility(visibility);

		LinearLayout lin15 = (LinearLayout) dialog.findViewById(R.id.lin019);
		lin15.setVisibility(visibility);

		LinearLayout lin16 = (LinearLayout) dialog.findViewById(R.id.lin020);
		lin16.setVisibility(visibility);

		LinearLayout lin17 = (LinearLayout) dialog.findViewById(R.id.lin021);
		lin17.setVisibility(visibility);

		LinearLayout lin18 = (LinearLayout) dialog.findViewById(R.id.lin022);
		lin18.setVisibility(visibility);

		LinearLayout lin19 = (LinearLayout) dialog.findViewById(R.id.lin023);
		lin19.setVisibility(visibility);

		LinearLayout lin20 = (LinearLayout) dialog.findViewById(R.id.lin024);
		lin20.setVisibility(visibility);

		LinearLayout lin21 = (LinearLayout) dialog.findViewById(R.id.lin025);
		lin21.setVisibility(visibility);

		LinearLayout lin12 = (LinearLayout) dialog.findViewById(R.id.lin0116);
		lin12.setVisibility(visibility);

		lin081 = (LinearLayout) dialog.findViewById(R.id.lin081);

		TextView txt1 = (TextView) dialog.findViewById(R.id.text1);
		txt1.setText("Vicious / Exotic Pets");

		TextView txt2 = (TextView) dialog.findViewById(R.id.text2);
		txt2.setText("Horses/Livestock for Business On premises");

		TextView txt3 = (TextView) dialog.findViewById(R.id.text3);
		txt3.setText("Over hanging Limbs to Roof");

		TextView txt4 = (TextView) dialog.findViewById(R.id.text4);
		txt4.setText("Trampoline Present");

		TextView txt5 = (TextView) dialog.findViewById(R.id.text5);
		txt5.setText("Skateboard Ramp present");

		TextView txt6 = (TextView) dialog.findViewById(R.id.text6);
		txt6.setText("Bicycle Ramp present");

		TextView txt7 = (TextView) dialog.findViewById(R.id.text7);
		txt7.setText("Trip Hazards Noted");

		TextView txt8 = (TextView) dialog.findViewById(R.id.text81);
		txt8.setText("Trip Hazards Description");

		TextView txt9 = (TextView) dialog.findViewById(R.id.text9);
		txt9.setText("Unsafe Stairway Noted");

		TextView txt10 = (TextView) dialog.findViewById(R.id.text10);
		txt10.setText("Porch/deck >2� off grade (or 3 steps) no hand railings");

		TextView txt11 = (TextView) dialog.findViewById(R.id.text11);
		txt11.setText("Non-Standard construction Noted");

		TextView txt12 = (TextView) dialog.findViewById(R.id.text12);
		txt12.setText("Outdoor Appliances Noted");

		TextView txt13 = (TextView) dialog.findViewById(R.id.text13);
		txt13.setText("Open foundation present");

		TextView txt14 = (TextView) dialog.findViewById(R.id.text14);
		txt14.setText("Wood shingled roofs");

		TextView txt15 = (TextView) dialog.findViewById(R.id.text15);
		txt15.setText("Excess debris / Garbage /Trash");

		TextView txt17 = (TextView) dialog.findViewById(R.id.text17);
		txt17.setText("Building in general Disrepair");

		TextView txt18 = (TextView) dialog.findViewById(R.id.text18);
		txt18.setText("Property Damage Noted");

		TextView txt19 = (TextView) dialog.findViewById(R.id.text19);
		txt19.setText("Structure partially / Entirely under water");

		TextView txt20 = (TextView) dialog.findViewById(R.id.text20);
		txt20.setText("Inoperative / Non-Starting motor vehicle(s) Noted");

		TextView txt21 = (TextView) dialog.findViewById(R.id.text21);
		txt21.setText("Recent drywall repair");

		TextView txt22 = (TextView) dialog.findViewById(R.id.text22);
		txt22.setText("Possible Chinese drywall product");

		TextView txt23 = (TextView) dialog.findViewById(R.id.text23);
		txt23.setText("Owner Confirmed Chinese drywall product present");

		TextView txt24 = (TextView) dialog.findViewById(R.id.text24);
		txt24.setText("Non working security system noted");

		TextView txt25 = (TextView) dialog.findViewById(R.id.text25);
		txt25.setText("Non working smoke dedicator noted");

		TextView txt16 = (TextView) dialog.findViewById(R.id.text16);
		txt16.setText("Business on premises");

		Button btncls = (Button) dialog.findViewById(R.id.btnclose);
		btncls.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}

		});

		Spinner s = (Spinner) dialog.findViewById(R.id.Spinner01);
		Spinner s1 = (Spinner) dialog.findViewById(R.id.Spinner02);
		Spinner s2 = (Spinner) dialog.findViewById(R.id.Spinner03);
		Spinner s3 = (Spinner) dialog.findViewById(R.id.Spinner04);
		Spinner s4 = (Spinner) dialog.findViewById(R.id.Spinner05);
		Spinner s5 = (Spinner) dialog.findViewById(R.id.Spinner06);
		Spinner s6 = (Spinner) dialog.findViewById(R.id.Spinner07);
		s7 = (EditText) dialog.findViewById(R.id.edit08);
		Spinner s8 = (Spinner) dialog.findViewById(R.id.Spinner09);
		Spinner s9 = (Spinner) dialog.findViewById(R.id.Spinner010);
		Spinner s10 = (Spinner) dialog.findViewById(R.id.Spinner011);
		Spinner s11 = (Spinner) dialog.findViewById(R.id.Spinner012);
		Spinner s12 = (Spinner) dialog.findViewById(R.id.Spinner013);
		Spinner s13 = (Spinner) dialog.findViewById(R.id.Spinner014);
		Spinner s14 = (Spinner) dialog.findViewById(R.id.Spinner015);
		Spinner s16 = (Spinner) dialog.findViewById(R.id.Spinner017);
		Spinner s17 = (Spinner) dialog.findViewById(R.id.Spinner018);
		Spinner s18 = (Spinner) dialog.findViewById(R.id.Spinner019);
		Spinner s19 = (Spinner) dialog.findViewById(R.id.Spinner020);
		Spinner s20 = (Spinner) dialog.findViewById(R.id.Spinner021);
		Spinner s21 = (Spinner) dialog.findViewById(R.id.Spinner022);
		Spinner s22 = (Spinner) dialog.findViewById(R.id.Spinner023);
		Spinner s23 = (Spinner) dialog.findViewById(R.id.Spinner024);
		Spinner s24 = (Spinner) dialog.findViewById(R.id.Spinner025);
		Spinner s15 = (Spinner) dialog.findViewById(R.id.Spinner016);
		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, array_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		s.setAdapter(adapter);
		s.setOnItemSelectedListener(new MyOnItemSelectedListenerhz());
		s1.setAdapter(adapter);
		s1.setOnItemSelectedListener(new MyOnItemSelectedListenerhz1());
		s2.setAdapter(adapter);
		s2.setOnItemSelectedListener(new MyOnItemSelectedListenerhz2());
		s3.setAdapter(adapter);
		s3.setOnItemSelectedListener(new MyOnItemSelectedListenerhz3());
		s4.setAdapter(adapter);
		s4.setOnItemSelectedListener(new MyOnItemSelectedListenerhz4());
		s5.setAdapter(adapter);
		s5.setOnItemSelectedListener(new MyOnItemSelectedListenerhz5());
		s6.setAdapter(adapter);
		s6.setOnItemSelectedListener(new MyOnItemSelectedListenerhz6());
		s8.setAdapter(adapter);
		s8.setOnItemSelectedListener(new MyOnItemSelectedListenerhz8());
		s9.setAdapter(adapter);
		s9.setOnItemSelectedListener(new MyOnItemSelectedListenerhz9());
		s10.setAdapter(adapter);
		s10.setOnItemSelectedListener(new MyOnItemSelectedListenerhz10());
		s11.setAdapter(adapter);
		s11.setOnItemSelectedListener(new MyOnItemSelectedListenerhz11());
		s12.setAdapter(adapter);
		s12.setOnItemSelectedListener(new MyOnItemSelectedListenerhz12());
		s13.setAdapter(adapter);
		s13.setOnItemSelectedListener(new MyOnItemSelectedListenerhz13());
		s14.setAdapter(adapter);
		s14.setOnItemSelectedListener(new MyOnItemSelectedListenerhz14());
		s15.setAdapter(adapter);
		s15.setOnItemSelectedListener(new MyOnItemSelectedListenerhz15());
		s17.setAdapter(adapter);
		s17.setOnItemSelectedListener(new MyOnItemSelectedListenerhz17());
		s18.setAdapter(adapter);
		s18.setOnItemSelectedListener(new MyOnItemSelectedListenerhz18());
		s19.setAdapter(adapter);
		s19.setOnItemSelectedListener(new MyOnItemSelectedListenerhz19());
		s20.setAdapter(adapter);
		s20.setOnItemSelectedListener(new MyOnItemSelectedListenerhz20());
		s21.setAdapter(adapter);
		s21.setOnItemSelectedListener(new MyOnItemSelectedListenerhz21());
		s22.setAdapter(adapter);
		s22.setOnItemSelectedListener(new MyOnItemSelectedListenerhz22());
		s23.setAdapter(adapter);
		s23.setOnItemSelectedListener(new MyOnItemSelectedListenerhz23());
		s24.setAdapter(adapter);
		s24.setOnItemSelectedListener(new MyOnItemSelectedListenerhz24());
		s16.setAdapter(adapter);
		s16.setOnItemSelectedListener(new MyOnItemSelectedListenerhz16());

		chkhzdrws();

		Button btnsav = (Button) dialog.findViewById(R.id.btnsave);
		btnsav.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (rws == 0) {
					strhz8 = s7.getText().toString();
					db.wind_db.execSQL("INSERT INTO "
							+ db.GeneralHazInfo
							+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
							+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
							+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
							+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
							+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
							+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
							+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
							+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
							+ " VALUES ('" + cf.Homeid + "','','','','','" + 0
							+ "','" + 0 + "','','" + 0 + "','','" + 0
							+ "','','','','','','','','','','','','',"
							+ "'','','','','','','','','','',''," + "'"
							+ strhz1 + "','" + strhz2 + "','" + strhz3 + "','"
							+ strhz4 + "','" + strhz5 + "','" + strhz6 + "','"
							+ db.encode(strhz8) + "','" + strhz7
							+ "','" + strhz9 + "'," + "'" + strhz10 + "','"
							+ strhz11 + "','" + strhz12 + "','" + strhz13
							+ "','" + strhz14 + "','" + strhz15 + "','"
							+ strhz16 + "','" + strhz17 + "','" + strhz18
							+ "'," + "'" + strhz19 + "','" + strhz20 + "','"
							+ strhz21 + "','" + strhz22 + "','" + strhz23
							+ "','" + strhz24 + "','" + strhz25 + "','')");
					
				} else {
					strhz8 = s7.getText().toString();System.out.println("its 33333");
					db.wind_db.execSQL("UPDATE " + db.GeneralHazInfo
							+ " SET Vicious='" + strhz1 + "',LiveStock='"
							+ strhz2 + "',OverHanging='" + strhz3
							+ "',Trampoline='" + strhz4 + "',SkateBoard='"
							+ strhz5 + "'," + "bicycle='" + strhz6
							+ "',TripHazardDesc='"
							+ db.encode(strhz8)
							+ "',TripHazardNoted='" + strhz7
							+ "',UnsafeStairway='" + strhz9
							+ "',PorchAndDeck='" + strhz10 + "',"
							+ "NonStdConstruction='" + strhz11
							+ "',OutDoorAppliances='" + strhz12
							+ "',OpenFoundation='" + strhz13
							+ "',WoodShingled='" + strhz14 + "',"
							+ "ExcessDebris='" + strhz15
							+ "',BusinessPremises='" + strhz16
							+ "',GeneralDisrepair='" + strhz17
							+ "',PropertyDamage='" + strhz18 + "',"
							+ "StructurePartial='" + strhz19
							+ "',InOperative='" + strhz20 + "',RecentDrywall='"
							+ strhz21 + "',ChineseDrywall='" + strhz22 + "',"
							+ "ConfirmDrywall='" + strhz23 + "',NonSecurity='"
							+ strhz24 + "',NonSmoke='" + strhz25
							+ "' WHERE Srid ='" + cf.Homeid + "'");

				}
				cf.ShowToast("Summary of Hazards/Concerns saved successfully.");
				changeimage(cf.Homeid);
				dialog.dismiss();
			}

		});
		if (rws > 0) {
			retrievedata();

			int sp = adapter.getPosition(strhz1);
			int sp1 = adapter.getPosition(strhz2);
			int sp2 = adapter.getPosition(strhz3);
			int sp3 = adapter.getPosition(strhz4);
			int sp4 = adapter.getPosition(strhz5);
			int sp5 = adapter.getPosition(strhz6);
			int sp6 = adapter.getPosition(strhz7);
			int sp8 = adapter.getPosition(strhz9);
			int sp9 = adapter.getPosition(strhz10);
			int sp10 = adapter.getPosition(strhz11);
			int sp11 = adapter.getPosition(strhz12);
			int sp12 = adapter.getPosition(strhz13);
			int sp13 = adapter.getPosition(strhz14);
			int sp14 = adapter.getPosition(strhz15);
			int sp15 = adapter.getPosition(strhz16);
			int sp16 = adapter.getPosition(strhz17);
			int sp17 = adapter.getPosition(strhz18);
			int sp18 = adapter.getPosition(strhz19);
			int sp19 = adapter.getPosition(strhz20);
			int sp20 = adapter.getPosition(strhz21);
			int sp21 = adapter.getPosition(strhz22);
			int sp22 = adapter.getPosition(strhz23);
			int sp23 = adapter.getPosition(strhz24);
			int sp24 = adapter.getPosition(strhz25);
			s.setSelection(sp);
			s1.setSelection(sp1);
			s2.setSelection(sp2);
			s3.setSelection(sp3);
			s4.setSelection(sp4);
			s5.setSelection(sp5);
			s6.setSelection(sp6);
			if (strhz7.equals("Yes")) {
				lin081.setVisibility(visibility);
				s7.setText(db.decode(strhz8));
			}

			s8.setSelection(sp8);
			s9.setSelection(sp9);
			s10.setSelection(sp10);
			s11.setSelection(sp11);
			s12.setSelection(sp12);
			s13.setSelection(sp13);
			s14.setSelection(sp14);
			s15.setSelection(sp15);
			s16.setSelection(sp16);
			s17.setSelection(sp17);
			s18.setSelection(sp18);
			s19.setSelection(sp19);
			s20.setSelection(sp20);
			s21.setSelection(sp21);
			s22.setSelection(sp22);
			s23.setSelection(sp23);
			s24.setSelection(sp24);

		}
		dialog.show();
		dialog.setCancelable(false);
	}
	protected void changeimage(String homeid2) {
		// TODO Auto-generated method stub
			try {
				Cursor cur6 = db.wind_db.rawQuery("select * from " + db.GeneralHazInfo + " where Srid='" + cf.Homeid + "'", null);
				cur6.moveToFirst();
				if (cur6 != null) {
					String insured = cur6.getString(cur6
							.getColumnIndex("InsuredIs"));
					if (!insured.equals("")) {
						gentick.setVisibility(v1.VISIBLE);
					}
				}
				Cursor cur = db.wind_db.rawQuery("select * from " + db.GeneralHazInfo  + " where Srid='" + cf.Homeid + "'", null);
				cur.moveToFirst();
				if (cur != null) {
					String loc = cur.getString(cur.getColumnIndex("Location1"));
					if (!loc.equals("")) {
						loctick.setVisibility(v1.VISIBLE);
					}
				}
				Cursor cur1 = db.wind_db.rawQuery("select * from " + db.GeneralHazInfo  + " where Srid='" + cf.Homeid + "'", null);
				cur1.moveToFirst();
				if (cur1 != null) {
					String obs = cur1
							.getString(cur1.getColumnIndex("Observation1"));
					if (!obs.equals("")) {
						obstick.setVisibility(v1.VISIBLE);
					}
				}
				Cursor cur11 = db.wind_db.rawQuery("select * from " + db.GeneralHazInfo  + " where Srid='" + cf.Homeid + "'", null);
				cur11.moveToFirst();
				if (cur11 != null) {
					String plfn = cur11.getString(cur11
							.getColumnIndex("PerimeterPoolFence"));
					if (!plfn.equals("")) {
						plfntick.setVisibility(v1.VISIBLE);
					}
				}
				Cursor cur21 = db.wind_db.rawQuery("select * from " + db.GeneralHazInfo  + " where Srid='" + cf.Homeid + "'", null);
				cur21.moveToFirst();
				if (cur21 != null) {
					String pp = cur21
							.getString(cur21.getColumnIndex("PoolPresent"));
					if (!pp.equals("")) {
						pptick.setVisibility(v1.VISIBLE);
					}
				}
				Cursor cur211 = db.wind_db.rawQuery("select * from "
						+ db.GeneralHazInfo + " where Srid='" + cf.Homeid + "'",
						null);
				cur211.moveToFirst();
				if (cur211 != null) {
					String haz = cur211.getString(cur211.getColumnIndex("Vicious"));
					if (!haz.equals("")) {
						haztick.setVisibility(v1.VISIBLE);
					}
				}
			} catch (Exception e) {

			}
	}
	private void retrievedata()
	{
		try 
		{
			c2 = db.wind_db.rawQuery("SELECT * FROM  " + db.GeneralHazInfo + " WHERE Srid='" + cf.Homeid + "'", null);
			if(c2.getCount()>0)
			{
				c2.moveToFirst();
				
				strdata1 = c2.getString(c2.getColumnIndex("InsuredIs"));
				strdata2 = c2.getString(c2.getColumnIndex("ObservationType"));
				strdata3 = c2.getString(c2.getColumnIndex("OccupancyType"));
				insuredothertxt = c2.getString(c2.getColumnIndex("InsuredOther"));
				observothertxt = c2.getString(c2.getColumnIndex("ObservOther"));
				occupancyothertxt = c2.getString(c2.getColumnIndex("OcccupOther"));
				
				seloccup = c2.getString(c2.getColumnIndex("OccupiedPercent"));
				selflg = c2.getString(c2.getColumnIndex("OccupiedFlag"));
				selvac = c2.getString(c2.getColumnIndex("Vacant"));
				selvacflg = c2.getString(c2.getColumnIndex("VacantPercent"));				
				selnd= c2.getString(c2.getColumnIndex("NotDetermined"));
				
				strdata4 = c2.getString(c2.getColumnIndex("PermitConfirmed"));
				selbuildsz = c2.getString(c2.getColumnIndex("BuildingSize"));
				strdata5 = c2.getString(c2.getColumnIndex("BalconyPresent"));
				strdata6 = c2.getString(c2.getColumnIndex("AdditionalStructure"));				
				selother= c2.getString(c2.getColumnIndex("OtherLocation"));
				
				
				strloc1 = c2.getString(c2.getColumnIndex("Location1"));
				strloc2 = c2.getString(c2.getColumnIndex("Location2"));
				strloc3 = c2.getString(c2.getColumnIndex("Location3"));
				strloc4 = c2.getString(c2.getColumnIndex("Location4"));
				
				
				strob1 = c2.getString(c2.getColumnIndex("Observation1"));
				strob2 = c2.getString(c2.getColumnIndex("Observation2"));
				strob3 = c2.getString(c2.getColumnIndex("Observation3"));
				strob4 = c2.getString(c2.getColumnIndex("Observation4"));
				strob5 = c2.getString(c2.getColumnIndex("Observation5"));
				
				strpf1 = c2.getString(c2.getColumnIndex("PerimeterPoolFence"));
				strpf4 = c2.getString(c2.getColumnIndex("PoolFenceDisrepair"));
				strpf2 = c2.getString(c2.getColumnIndex("SelfLatch"));
				strpf3 = c2.getString(c2.getColumnIndex("ProfesInstall"));
				
				strpp1 = c2.getString(c2.getColumnIndex("PoolPresent"));
				strpp2 = c2.getString(c2.getColumnIndex("HotTubPresnt"));
				strpp3 = c2.getString(c2.getColumnIndex("EmptyGroundPool"));
				strpp4 = c2.getString(c2.getColumnIndex("PoolSlide"));
				strpp5 = c2.getString(c2.getColumnIndex("DivingBoardPoolSlide"));
				strpp6 = c2.getString(c2.getColumnIndex("PerimeterPoolEncl"));
				strpp7 = c2.getString(c2.getColumnIndex("PoolEnclosure"));
				
				strhz1 = c2.getString(c2.getColumnIndex("Vicious"));
				strhz2 = c2.getString(c2.getColumnIndex("LiveStock"));
				strhz3 = c2.getString(c2.getColumnIndex("OverHanging"));
				strhz4 = c2.getString(c2.getColumnIndex("Trampoline"));
				strhz5 = c2.getString(c2.getColumnIndex("SkateBoard"));
				strhz6 = c2.getString(c2.getColumnIndex("bicycle"));
				strhz8 = c2.getString(c2.getColumnIndex("TripHazardDesc"));
				strhz7 = c2.getString(c2.getColumnIndex("TripHazardNoted"));
				strhz9 = c2.getString(c2.getColumnIndex("UnsafeStairway"));
				strhz10 = c2.getString(c2.getColumnIndex("PorchAndDeck"));
				strhz11 = c2.getString(c2.getColumnIndex("NonStdConstruction"));
				strhz12 = c2.getString(c2.getColumnIndex("OutDoorAppliances"));
				strhz13 = c2.getString(c2.getColumnIndex("OpenFoundation"));
				strhz14 = c2.getString(c2.getColumnIndex("WoodShingled"));
				strhz15 = c2.getString(c2.getColumnIndex("ExcessDebris"));
				
				strhz16 = c2.getString(c2.getColumnIndex("BusinessPremises"));
				strhz17 = c2.getString(c2.getColumnIndex("GeneralDisrepair"));
				strhz18 = c2.getString(c2.getColumnIndex("PropertyDamage"));
				strhz19 = c2.getString(c2.getColumnIndex("StructurePartial"));
				strhz20 = c2.getString(c2.getColumnIndex("InOperative"));
				strhz21 = c2.getString(c2.getColumnIndex("RecentDrywall"));
				strhz22 = c2.getString(c2.getColumnIndex("ChineseDrywall"));
				strhz23 = c2.getString(c2.getColumnIndex("ConfirmDrywall"));
				strhz24 = c2.getString(c2.getColumnIndex("NonSecurity"));
				strhz25 = c2.getString(c2.getColumnIndex("NonSmoke"));
				strcomm = db.decode(c2.getString(c2.getColumnIndex("Comments")));
			}
		} 
		catch (Exception e) 
		{
			System.out.println("retrieve = "+e.getMessage());
		}
	}
	
	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.home:
			cf.gohome();
			break;
		case R.id.save:
			chkhzdrws();			
			if(chkinclude.isChecked())
			{
				if(!InsuredIs.equals(""))
				{
					if(!Location1.equals(""))
					{
						if(!Observation1.equals(""))
						{
							if(!PerimeterPoolFence.equals(""))
							{
								if(!PoolPresent.equals(""))
								{
									if(!Vicious.equals(""))
									{
										comentfill();
									}
									else
									{
										cf.ShowToast("Please save Summary of Hazards/Concerns");
									}
								}
								else
								{
									cf.ShowToast("Please save Pool Present");
								}
							}
							else
							{
								cf.ShowToast("Please save Perimeter Pool Fence");
							}
						}
						else
						{
							cf.ShowToast("Please save Observation");
						}
					}
					else
					{
						cf.ShowToast("Please save Location");
					}
				}
				else
				{
					cf.ShowToast("Please save General data");
				}	
			}
			else
			{
				updategenhzinclue(0);
				Intent int1 = new Intent(GeneralHazInfo.this, GeneralHazImage.class);
				int1.putExtra("homeid", cf.Homeid);
				int1.putExtra("status", cf.status);
				startActivity(int1);
			}
			break;
			
		case R.id.genvimg:
			b = 1;			
			data();
			break;
		case R.id.gendata:
			b = 1;
			data();
			break;
		case R.id.locvimg:
			b = 1;
			location();
			break;			
		case R.id.loca:
			b = 1;
			location();
			break;
		case R.id.plfvimg:
			b = 1;
			poolfence();
			break;
		case R.id.poolfenc:
			b = 1;
			poolfence();
			break;
		case R.id.obsvimg:
			b = 1;
			observation();
			break;
		case R.id.obs:
			b = 1;
			observation();
			break;
		case R.id.ppvimg:
			b = 1;
			poolpresent();
			break;
		case R.id.poolpres:
			b = 1;
			poolpresent();
			break;
		case R.id.hazvimg:
			b = 1;
			hazard();
			break;
		case R.id.haz:
			b = 1;
			hazard();
			break;
		
		}
	}
	private void comentfill() {
		// TODO Auto-generated method stub
		if (!etcomments.getText().toString().equals("")) {
			chkhzdrws();
			updategenhzinclue(1);
			if (rws == 0) {
				db.wind_db.execSQL("INSERT INTO "
						+ db.GeneralHazInfo
						+ " (Srid,InsuredIs,OccupancyType,ObservationType,OccupiedPercent,OccupiedFlag,Vacant,VacantPercent,NotDetermined,PermitConfirmed,BuildingSize,"
						+ "BalconyPresent,AdditionalStructure,OtherLocation,Location1,Location2,Location3,Location4,Observation1,"
						+ "Observation2,Observation3,Observation4,Observation5,PerimeterPoolFence,PoolFenceDisrepair,"
						+ "SelfLatch,ProfesInstall,PoolPresent,HotTubPresnt,EmptyGroundPool,PoolSlide,DivingBoardPoolSlide,"
						+ "PerimeterPoolEncl,PoolEnclosure,Vicious,LiveStock,OverHanging,Trampoline,SkateBoard,bicycle,"
						+ "TripHazardDesc,TripHazardNoted,UnsafeStairway,PorchAndDeck,NonStdConstruction,OutDoorAppliances,"
						+ "OpenFoundation,WoodShingled,ExcessDebris,BusinessPremises,GeneralDisrepair,PropertyDamage,"
						+ "StructurePartial,InOperative,RecentDrywall,ChineseDrywall,ConfirmDrywall,NonSecurity,NonSmoke,Comments)"
						+ " VALUES ('"
						+ cf.Homeid
						+ "','','','','','"
						+ 0
						+ "','"
						+ 0
						+ "','','"
						+ 0
						+ "','','"
						+ 0
						+ "','','','','','','','',"
						+ "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','"
						+ db.encode(etcomments.getText()
								.toString()) + "')");

			} else {
				db.wind_db.execSQL("UPDATE "
						+ db.GeneralHazInfo
						+ " SET Comments='"
						+ db.encode(etcomments.getText()
								.toString()) + "' WHERE Srid ='" + cf.Homeid
						+ "'");

			}
			Insertsubmitcheck();
			cf.ShowToast("General Hazard Data saved successfully.");
			Intent int1 = new Intent(GeneralHazInfo.this, GeneralHazImage.class);
			int1.putExtra("homeid", cf.Homeid);
			int1.putExtra("status", cf.status);
			startActivity(int1);
		}
		else
		{
			cf.ShowToast("Please enter the General data comments.");
		}
	}
	public class MyOnItemSelectedListenerdata implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata1 = parent.getItemAtPosition(pos).toString();
			data1flg = parent.getSelectedItemPosition();
			if(strdata1.equals("Other"))
			{
				etinsuredother.setVisibility(view.VISIBLE);
				insuredothertxt=etinsuredother.getText().toString();
			}
			else
			{
				etinsuredother.setVisibility(view.GONE);
				//etinsuredother.setText("");
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata1 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata2 = parent.getItemAtPosition(pos).toString();
			data2flg = parent.getSelectedItemPosition();
			if(strdata2.equals("Other"))
			{
				etobservother.setVisibility(view.VISIBLE);
				observothertxt=etobservother.getText().toString();
			}
			else
			{
				etobservother.setVisibility(view.GONE);
				//etobservother.setText("");
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata2 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata3 = parent.getItemAtPosition(pos).toString();
			data3flg = parent.getSelectedItemPosition();
			if(strdata3.equals("Other"))
			{
				etoccupancyother.setVisibility(view.VISIBLE);
				occupancyothertxt=etoccupancyother.getText().toString();
			}
			else
			{
				etoccupancyother.setVisibility(view.GONE);
				//etoccupancyother.setText("");
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata3 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata4 = parent.getItemAtPosition(pos).toString();
			data4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata4 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata5 = parent.getItemAtPosition(pos).toString();
			data5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerdata5 implements
			OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strdata6 = parent.getItemAtPosition(pos).toString();
			data6flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc1 = parent.getItemAtPosition(pos).toString();
			loc1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc2 = parent.getItemAtPosition(pos).toString();
			loc2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc3 = parent.getItemAtPosition(pos).toString();
			loc3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListener3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strloc4 = parent.getItemAtPosition(pos).toString();
			loc4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz1 = parent.getItemAtPosition(pos).toString();
			hz1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz2 = parent.getItemAtPosition(pos).toString();
			hz2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz3 = parent.getItemAtPosition(pos).toString();
			hz3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz4 = parent.getItemAtPosition(pos).toString();
			hz4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz5 = parent.getItemAtPosition(pos).toString();
			hz5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz5 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz6 = parent.getItemAtPosition(pos).toString();
			hz6flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz6 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz7 = parent.getItemAtPosition(pos).toString();
			hz7flg = parent.getSelectedItemPosition();
			if (strhz7.equals("Yes")) {
				lin081.setVisibility(visibility);
			} else {
				lin081.setVisibility(view.GONE);
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz8 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz9 = parent.getItemAtPosition(pos).toString();
			hz9flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz9 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz10 = parent.getItemAtPosition(pos).toString();
			hz10flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz10 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz11 = parent.getItemAtPosition(pos).toString();
			hz11flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz11 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz12 = parent.getItemAtPosition(pos).toString();
			hz12flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz12 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz13 = parent.getItemAtPosition(pos).toString();
			hz13flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz13 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz14 = parent.getItemAtPosition(pos).toString();
			hz14flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz14 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz15 = parent.getItemAtPosition(pos).toString();
			hz15flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz15 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz16 = parent.getItemAtPosition(pos).toString();
			hz16flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz16 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz17 = parent.getItemAtPosition(pos).toString();
			hz17flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz17 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz18 = parent.getItemAtPosition(pos).toString();
			hz18flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz18 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz19 = parent.getItemAtPosition(pos).toString();
			hz19flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz19 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz20 = parent.getItemAtPosition(pos).toString();
			hz20flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz20 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz21 = parent.getItemAtPosition(pos).toString();
			hz21flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz21 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz22 = parent.getItemAtPosition(pos).toString();
			hz22flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz22 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz23 = parent.getItemAtPosition(pos).toString();
			hz23flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz23 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz24 = parent.getItemAtPosition(pos).toString();
			hz24flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerhz24 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strhz25 = parent.getItemAtPosition(pos).toString();
			hz25flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	public class MyOnItemSelectedListenerob implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob1 = parent.getItemAtPosition(pos).toString();
			obs1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob2 = parent.getItemAtPosition(pos).toString();
			obs2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob3 = parent.getItemAtPosition(pos).toString();
			obs3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob4 = parent.getItemAtPosition(pos).toString();
			obs4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerob4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strob5 = parent.getItemAtPosition(pos).toString();
			obs5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	public class MyOnItemSelectedListenerpp implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp1 = parent.getItemAtPosition(pos).toString();
			pp1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp2 = parent.getItemAtPosition(pos).toString();
			pp2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp3 = parent.getItemAtPosition(pos).toString();
			pp3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp4 = parent.getItemAtPosition(pos).toString();
			pp4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp4 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp5 = parent.getItemAtPosition(pos).toString();
			pp5flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpp5 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp6 = parent.getItemAtPosition(pos).toString();
			pp6flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	public class MyOnItemSelectedListenerpp6 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpp7 = parent.getItemAtPosition(pos).toString();
			pp7flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	public class MyOnItemSelectedListenerpf implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf1 = parent.getItemAtPosition(pos).toString();
			pf1flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpf1 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf2 = parent.getItemAtPosition(pos).toString();
			pf2flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpf2 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf3 = parent.getItemAtPosition(pos).toString();
			pf3flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public class MyOnItemSelectedListenerpf3 implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {
			strpf4 = parent.getItemAtPosition(pos).toString();
			pf4flg = parent.getSelectedItemPosition();
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intimg = new Intent(GeneralHazInfo.this, OverallComments.class);
			intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("homeid", cf.Homeid);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}