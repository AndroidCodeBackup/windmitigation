package idsoft.inspectiondepot.windmitinspection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EmailList extends Activity{
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	RelativeLayout rlheader;
	LinearLayout lldynamic;
	String[] data,datasend;
	TextView txtrws;
	static String mailid,policyno,name,srid1;
	AutoCompleteTextView actsearch;
public int[] txtid = {R.id.txta,R.id.txtb,R.id.txtc,R.id.txtd,R.id.txte,R.id.txtf,R.id.txtg,R.id.txth,R.id.txti,R.id.txtj,
			R.id.txtk,R.id.txtl,R.id.txtm,R.id.txtn,R.id.txto,R.id.txtp,R.id.txtq,R.id.txtr,R.id.txts,R.id.txtt,R.id.txtu,
			R.id.txtv,R.id.txtw,R.id.txtx,R.id.txty,R.id.txtz,R.id.txtall};
	public TextView[] txt = new TextView[27];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		setContentView(R.layout.emaillist);
		db.getInspectorId();
		
        ((ImageView)findViewById(R.id.head_insp_info)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent insp_info = new Intent(EmailList.this,PolicyholdeInfoHead.class);
				insp_info.putExtra("Type", "Inspector");
				insp_info.putExtra("insp_id", db.Insp_id);
				startActivityForResult(insp_info,77);System.out.println("ends");
				
			}
		});
        
        lldynamic = (LinearLayout) findViewById(R.id.emailreport_lldynamic);
		rlheader = (RelativeLayout) findViewById(R.id.emailreport_rlheader);
		actsearch = (AutoCompleteTextView) findViewById(R.id.emailreport_actsearch);
		txtrws = (TextView)findViewById(R.id.rws);
		
        db.CreateTable(2);
        db.getInspectorId();
		Cursor cur = db.wind_db.rawQuery("select * from " + db.policyholder + " where PH_InspectorId='"+db.Insp_id+"' order by InspectionDate desc", null);
		display_list(cur);
		
		TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1,int i2) {
				if (actsearch.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	actsearch.setText("");
		        }
			}

			@Override
			public void afterTextChanged(Editable editable) {
				// here, after we introduced something in the EditText we get
				// the string from it
				try {
					db.CreateTable(2);
					Cursor cur1 = db.wind_db.rawQuery("select * from " + db.policyholder
									+ " where PH_Policyno like '"
									+ db.encode(actsearch.getText().toString())
									+ "%' and PH_InspectorId='"+db.Insp_id+"'" , null);
					String[] autopolicyno = new String[cur1.getCount()];
					cur1.moveToFirst();
					if (cur1.getCount() != 0) {
						if (cur1 != null) {
							int i = 0;
							do {
								autopolicyno[i] = db.decode(cur1.getString(cur1
										.getColumnIndex("PH_Policyno")));

								if (autopolicyno[i].contains("null")) {
									autopolicyno[i] = autopolicyno[i].replace(
											"null", "");
								}

								i++;
							} while (cur1.moveToNext());
						}
						cur1.close();
					}

					Cursor cur2 = db.wind_db.rawQuery("select * from "
							+ db.policyholder + " where PH_FirstName like '"
							+ db.encode(actsearch.getText().toString()) + "%' and PH_InspectorId='"+db.Insp_id+"'" ,
							null);
					String[] autoownersname = new String[cur2.getCount()];
					cur2.moveToFirst();
					if (cur2.getCount() != 0) {
						if (cur2 != null) {
							int i = 0;
							do {
								autoownersname[i] = db.decode(cur2
										.getString(cur2
												.getColumnIndex("PH_FirstName")));

								if (autoownersname[i].contains("null")) {
									autoownersname[i] = autoownersname[i]
											.replace("null", "");
								}

								i++;
							} while (cur2.moveToNext());
						}
						cur2.close();
					}

					List<String> images = new ArrayList<String>();
					images.addAll(Arrays.asList(autopolicyno));
					images.addAll(Arrays.asList(autoownersname));

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(EmailList.this, R.layout.autocompletelist, images);
					actsearch.setThreshold(1);
					actsearch.setAdapter(adapter);

				} catch (Exception e) {

				}

			}
		};

		// third, we must add the textWatcher to our EditText
		actsearch.addTextChangedListener(textWatcher);
		
		for(int i=0;i<txt.length;i++)
		{
			txt[i] = (TextView)findViewById(txtid[i]);
			txt[i].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					actsearch.setText("");
					switch(v.getId())
					{
					   case R.id.txta: 
						   txtsearch("A");changetextbgcolor();
						   ((TextView)findViewById(R.id.txta)).setBackgroundColor(Color.parseColor("#519BC2")); 
						break;
					   case R.id.txtb: 
						   txtsearch("B");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtb)).setBackgroundColor(Color.parseColor("#519BC2")); 
						break;
					   case R.id.txtc: 
						   txtsearch("C");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtc)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtd: 
						   txtsearch("D");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtd)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txte: 
						   txtsearch("E");changetextbgcolor();
						   ((TextView)findViewById(R.id.txte)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtf: 
						   txtsearch("F");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtf)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtg: 
						   txtsearch("G");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtg)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txth: 
						   txtsearch("H");changetextbgcolor();
						   ((TextView)findViewById(R.id.txth)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txti: 
						   txtsearch("I");changetextbgcolor();
						   ((TextView)findViewById(R.id.txti)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtj: 
						   txtsearch("J");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtj)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtk: 
						   txtsearch("K");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtk)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtl: 
						   txtsearch("L");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtl)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtm: 
						   txtsearch("M");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtm)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtn: 
						   txtsearch("N");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtn)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txto: 
						   txtsearch("O");changetextbgcolor();
						   ((TextView)findViewById(R.id.txto)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtp: 
						   txtsearch("P");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtp)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtq: 
						   txtsearch("Q");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtq)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtr: 
						   txtsearch("R");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtr)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txts: 
						   txtsearch("S");changetextbgcolor();
						   ((TextView)findViewById(R.id.txts)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtt: 
						   txtsearch("T");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtt)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtu: 
						   txtsearch("U");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtu)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtv: 
						   txtsearch("V");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtv)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtw: 
						   txtsearch("W");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtw)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtx: 
						   txtsearch("X");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtx)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txty: 
						   txtsearch("Y");changetextbgcolor();
						   ((TextView)findViewById(R.id.txty)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtz: 
						   txtsearch("Z");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtz)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					   case R.id.txtall: 
						   txtsearch("");changetextbgcolor();
						   ((TextView)findViewById(R.id.txtall)).setBackgroundColor(Color.parseColor("#519BC2"));
						break;
					}
					
				}
			});
		}
		
		
	}
	private void changetextbgcolor()
	{
		for(int i=0;i<txt.length;i++)
		{
			txt[i].setBackgroundColor(Color.parseColor("#2682B3"));
		}
	}
	protected void txtsearch(String string) {
		// TODO Auto-generated method stub
		try
		{
			db.CreateTable(2);
			Cursor cur = db.wind_db.rawQuery("select * from " + db.policyholder
					+ " where PH_FirstName like '"
					+ db.encode(string.toLowerCase().trim())
					+ "%' or (PH_Policyno like '"+ db.encode(string.toLowerCase().trim()) + "%' and PH_Policyno!='N%2FA') and PH_InspectorId='"+db.Insp_id+"'", null);
			System.out.println("cur d"+cur.getCount());
			display_list(cur);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	private void display_list(Cursor cur) {
		// TODO Auto-generated method stub
		cur.moveToFirst();
		txtrws.setText("TOTAL # OF RECORDS : "+cur.getCount());
		if (cur.getCount() >= 1)
		{
			lldynamic.removeAllViews();
			rlheader.setVisibility(View.VISIBLE);
			int cnt = cur.getCount();
			data=new String[cnt];
			datasend=new String[cnt];
			String[] ownersName=new String[cnt];

			TextView[] tv = new TextView[cnt];
			Button[] sendmail = new Button[cnt];
			LinearLayout.LayoutParams textparams,btnparams;
			
			int i = 0;
			do
			{
				String ownersname=db.decode(cur.getString(cur.getColumnIndex("PH_FirstName")))+" "+db.decode(cur.getString(cur.getColumnIndex("PH_LastName")));
				data[i]=ownersname+" | ";
				ownersName[i]=ownersname;
				String address=db.decode(cur.getString(cur.getColumnIndex("PH_Address1")));
				data[i] +=address+" | ";
				String city=db.decode(cur.getString(cur.getColumnIndex("PH_City")));
				data[i] +=city+" | ";
				String state=db.decode(cur.getString(cur.getColumnIndex("PH_State")));
				data[i] +=state+" | ";
				String county=db.decode(cur.getString(cur.getColumnIndex("PH_County")));
				data[i] +=county+" | ";
				String zip=db.decode(cur.getString(cur.getColumnIndex("PH_Zip")));
				data[i] +=zip+" | ";
				String inspectiondate=db.decode(cur.getString(cur.getColumnIndex("InspectionDate")));
				data[i] +=inspectiondate+" | ";
				String policynumber=db.decode(cur.getString(cur.getColumnIndex("PH_Policyno")));
				String srid=db.decode(cur.getString(cur.getColumnIndex("PH_SRID")));
				if(policynumber.equals(""))
				{
					policynumber="N/A";
				}
				data[i] +=policynumber+" | ";
				String email=db.decode(cur.getString(cur.getColumnIndex("PH_Email")));
				if(email.equals(""))
				{
					email="N/A";
				}
				data[i] +=email;
				
				
				//For sending policy number and status to email report3
				String pn=db.decode(cur.getString(cur.getColumnIndex("PH_Policyno")));
				datasend[i] =pn+":";
				String status=db.decode(cur.getString(cur.getColumnIndex("PH_Status")));
				datasend[i] +=status+":"+ownersname;
				
				LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);

				LinearLayout ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(llparams);
				lldynamic.addView(ll);
				
				textparams = new LinearLayout.LayoutParams(
						775, ViewGroup.LayoutParams.WRAP_CONTENT);
				textparams.setMargins(20, 20, 20, 20);


				btnparams = new LinearLayout.LayoutParams(
						100,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				btnparams.setMargins(20, 0, 20, 0);
				btnparams.gravity = Gravity.CENTER_VERTICAL;
			
				tv[i] = new TextView(this);
				tv[i].setLayoutParams(textparams);
				tv[i].setText(data[i]);
				tv[i].setTextColor(Color.WHITE);
				tv[i].setTextSize(14);
				ll.addView(tv[i]);
				
				sendmail[i] = new Button(this);
				sendmail[i].setLayoutParams(btnparams);
				sendmail[i].setText("Send Mail");
				sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
				sendmail[i].setTextColor(0xffffffff);
				sendmail[i].setTextSize(14);
				sendmail[i].setBackgroundResource(R.drawable.mybutton);
				sendmail[i].setTypeface(null, Typeface.BOLD);
				sendmail[i].setTag(email+":"+srid+":"+ownersName[i]+":"+i);
				ll.addView(sendmail[i]);
				
                sendmail[i].setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String value=v.getTag().toString();
						String[] splitvalue=value.split(":");
						mailid=splitvalue[0];
						policyno=splitvalue[1];
						name=splitvalue[2];
						
						String ivalue=splitvalue[3];
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						String[] arrayivalsplit=ivalsplit.split(":");
						//String pn=arrayivalsplit[0];
						String status=arrayivalsplit[1];
						String ownersname=arrayivalsplit[2];
						
						System.out.println("Mail id is :"+mailid);
						System.out.println("Policy no is :"+policyno);
						
						Intent intent=new Intent(EmailList.this,EmailReport2.class);
						intent.putExtra("policynumber", policyno);
						intent.putExtra("status", status);
						intent.putExtra("mailid", mailid);
						intent.putExtra("classidentifier", "EmailReport");
						intent.putExtra("ownersname", ownersname);
						startActivity(intent);
						finish();
						
					}
				});
                
                if (i % 2 == 0) {
					ll.setBackgroundColor(Color.parseColor("#1871A0"));
							/*"#6E6E6E"));*/
				} else {
					ll.setBackgroundColor(Color.parseColor("#106896"));
							/*"#2E2E2E"));*/
				}
				i++;
				
			}while (cur.moveToNext());
		}
		else
		{
			rlheader.setVisibility(View.GONE);
			lldynamic.removeAllViews();
			cf.ShowToast("Sorry, No results available");
		}
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.emailreport_home:
			cf.gohome();
			break;
		case R.id.emailreport_search:
			changetextbgcolor();
			Search();
			cf.hidekeyboard((EditText)actsearch);
			break;

		case R.id.emailreport_cancel:
			actsearch.setText("");
			db.CreateTable(2);
			changetextbgcolor();
			Cursor cur = db.wind_db.rawQuery("select * from " + db.policyholder + " where PH_InspectorId='"+db.Insp_id+"'", null);
			display_list(cur);
			cur.close();
			cf.hidekeyboard((EditText)actsearch);
			break;
		
		default:
			break;
		}
		
	}
	private void Search() {
		if (actsearch.getText().toString().trim().equals("")) {
			cf.ShowToast("Please enter the Name or Policy Number to search");
			actsearch.requestFocus();
			actsearch.setText("");
		} else {
			db.CreateTable(2);
			Cursor cur = db.wind_db.rawQuery("select * from " + db.policyholder
					+ " where PH_FirstName like '"
					+ db.encode(actsearch.getText().toString().trim())
					+ "%' or PH_Policyno like '"
					+ db.encode(actsearch.getText().toString().trim()) + "%' and PH_InspectorId='"+db.Insp_id+"'", null);
			
			display_list(cur);
		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(EmailList.this,HomeScreen.class));
			finish();
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU) {

		}
		return super.onKeyDown(keyCode, event);
	}
}
