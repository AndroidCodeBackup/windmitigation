package idsoft.inspectiondepot.windmitinspection;

import idsoft.inspectiondepot.windmitinspection.OrderInspection.mDateSetListener;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;



public class PolicyholderInfo extends Activity {
	String homeid, fname, lname, addr, cty, zpcde, statnam, cntry, yrhme,
			 policynum, storiesnum, compname,schedate;
	String chkstatus[] = { "true", "true", "true", "true", "true", "true","true" };
	int value, ml=0,Count, otheryear,mc = 0, f,updrws, yrchk,spinnerPosition,keyDelfee,keyDeldis;
	String inspstarttime, inspendtime, insurancedate, insname, schdat, schcmt,
			insdate, strfname, strlname, straddr1,straddr2, strcity, strzip, strstate,
			strcountry, stryrofhme, InspectionType, status, streffdat,updatecnt,
			strcontactperson, strhmephn, strwrkphn, strcellphn, strpolicy,anytype = "anyType{}", flag, homephn,
			strstories, stremail, strcompname,YearofBuilt;
	static final int DATE_DIALOG_ID = 0;
	private static final String TAG = null;
	private int mYear, mMonth, mDay;
	Button getdate, getinspdate;
	boolean ph[]={true,true,true};
	CheckBox mailchk;
	String strtime = "",currentdate;
	View v1;
	ArrayAdapter adapter;
	TextView viewqainspector;
	static final int TIME_DIALOG_ID = 999;
	String yearb[] = { "Select","Other","2013","2012","2011","2010","2009","2008","2007","2006","2005","2004","2003","2002","2001","2000","1999","1998",
			"1997","1996","1995","1994","1993","1992","1991","1990","1989","1988","1987","1986","1985","1984","1983","1982","1981","1980","1979",
			"1978","1977","1976","1975","1974","1973","1972","1971","1970","1969","1968","1967","1966","1965","1964"};
	EditText etdate,ettime,etinspfees,etinspdiscount;	
	Spinner yearofhome;	
	private static final int visibility = 0;
	TextView inspectiondate, txtinsdat, txtfirstname, txtlastname, txtaddress,
			txtcity, txtzip, txtstate, txtcounty, txtyearofhome, txthomephone,policyholderinfo,originaldatayear,
			txtnoofstories, txtemail, txtinsudate,scheduledate, dateview, scheddate, starttime, endtime;
	EditText firstname, effdat, schedulecomment, contactperson, lastname,otheryearhome,
			homephone, address1,address2, workphone, city, cellphone, zip, policy, state,
			country, stories, email,et_mailaddress1,et_mailaddress2,et_mailstate,et_mailcounty,et_mailcity,et_mailzip;
	android.text.format.DateFormat df;
	CharSequence cd, md;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	TimePickerDialog timepicker;
	
	public void onCreate(Bundle SavedInstanceState) {
		super.onCreate(SavedInstanceState);
		cf=new CommonFunctions(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		
		 Bundle extras = getIntent().getExtras();
	     if (extras != null) {
			cf.getExtras(extras);				
	     }
	     System.out.println("cf.sss="+cf.status);
		setContentView(R.layout.homeownerinfo);
		
		Declaration();
		db.getInspectorId();
		cf.getDeviceDimensions();
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.header);layout.setMinimumWidth(cf.wd);
		layout.addView(new MyOnclickListener(PolicyholderInfo.this, 1, cf, 0));		
		layout.setMinimumWidth(cf.wd);
		//LinearLayout sublayout = (LinearLayout) findViewById(R.id.general_menu);
		//sublayout.addView(new MyOnclickListener(getApplicationContext(), 11,cf, 1));	

		db.CreateTable(7);
		db.CreateTable(9);
		db.CreateTable(23);
		viewownerinfo();
	}

	private void Declaration() {
		// TODO Auto-generated method stub
		final Calendar c = Calendar.getInstance();
		final int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
		
		firstname = (EditText) this.findViewById(R.id.efirstname);
		firstname.addTextChangedListener(new ECustomTextWatcher(firstname));
		
		contactperson = (EditText) this.findViewById(R.id.econtactperson);
		contactperson.addTextChangedListener(new ECustomTextWatcher(contactperson));
		
		lastname = (EditText) this.findViewById(R.id.elastname);
		lastname.addTextChangedListener(new ECustomTextWatcher(lastname));
		
		homephone = (EditText) this.findViewById(R.id.ehomephone);homephone.addTextChangedListener(new CustomTextWatcher(homephone));
		address1 = (EditText) this.findViewById(R.id.eaddress1);
		address1.addTextChangedListener(new ECustomTextWatcher(address1));
		
		address2 = (EditText) this.findViewById(R.id.eaddress2);
		address2.addTextChangedListener(new ECustomTextWatcher(address2));
		
		workphone = (EditText) this.findViewById(R.id.eworkphone);workphone.addTextChangedListener(new CustomTextWatcher(workphone));
		city = (EditText) this.findViewById(R.id.ecity);
		city.addTextChangedListener(new ECustomTextWatcher(city));
		
		cellphone = (EditText) this.findViewById(R.id.ecellphone);cellphone.addTextChangedListener(new CustomTextWatcher(cellphone));
		zip = (EditText) this.findViewById(R.id.ezip);
		zip.addTextChangedListener(new ECustomTextWatcher(zip));
		
		policy = (EditText) this.findViewById(R.id.epolicy);	
		policy.addTextChangedListener(new ECustomTextWatcher(policy));
		
		state = (EditText) this.findViewById(R.id.estate);
		state.addTextChangedListener(new ECustomTextWatcher(state));
		
		stories = (EditText) this.findViewById(R.id.estories);stories.addTextChangedListener(new TextWatcher1(stories));
		country = (EditText) this.findViewById(R.id.ecounty);
		country.addTextChangedListener(new ECustomTextWatcher(country));
		
		email = (EditText) this.findViewById(R.id.email);
		email.addTextChangedListener(new ECustomTextWatcher(email));
		
		yearofhome = (Spinner) this.findViewById(R.id.eyearofhome);
		otheryearhome = (EditText) this.findViewById(R.id.otheryearhome);	
		otheryearhome.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
			   	    if (otheryearhome.getText().toString().startsWith(" "))
			        {
			            // Not allowed
			   	    	otheryearhome.setText("");
			        }
			   	    if(otheryearhome.getText().toString().startsWith("0"))
			   	    {
			   	    	otheryearhome.setText("");
			   	    }
			   	    
			   	   if (!otheryearhome.getText().toString().trim().equals("")) {
			   		   try{
						if (Integer.parseInt((otheryearhome.getText().toString()
								.trim())) > year1) {
							otheryearhome.setText("");
							cf.ShowToast("Please enter a valid year");
						}
			   		   }catch(Exception e)
			   		   {}
					}
					if (Arrays.asList(yearb).contains(
							otheryearhome.getText().toString().trim())) {
						otheryearhome.setText("");
						cf.ShowToast("Please enter a year that is not in the year list");
					}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		adapter = new ArrayAdapter(PolicyholderInfo.this,android.R.layout.simple_spinner_item, yearb);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		yearofhome.setAdapter(adapter);
		etdate = (EditText) findViewById(R.id.orderinsp_etinspdate);
		ettime = (EditText) findViewById(R.id.orderinsp_etinsptime);
		etinspfees = (EditText) findViewById(R.id.orderinsp_etinspfee);
		etinspdiscount = (EditText) findViewById(R.id.orderinsp_etinspdiscount);
		schedulecomment = (EditText) this.findViewById(R.id.eschcomment);
		schedulecomment.addTextChangedListener(new ECustomTextWatcher(schedulecomment));
		
		mailchk = (CheckBox) findViewById(R.id.valid);
	
		yearofhome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() 
		{
			public void onItemSelected(AdapterView<?> parentView,View selectedItemView, int position, long id) 
			{
				if (yearb[position].equals("Other")) 
				{
					otheryearhome.setVisibility(View.VISIBLE);
				} 
				else 
				{
					otheryearhome.setVisibility(View.GONE);
				}
			}
			public void onNothingSelected(AdapterView<?> parentView) {}
		});
		
		mailchk.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (((CheckBox) v).isChecked()) {
					mc = 1;
					email.setText("N/A");
					email.setEnabled(false);
				} else {
					mc = 0;
					email.setText("");
					email.setEnabled(true);
				}
			}
		});

		et_mailaddress1  = (EditText) findViewById(R.id.policy_mail_add1);et_mailaddress1.addTextChangedListener(new TextWatcher1(et_mailaddress1));
		et_mailstate  = (EditText) findViewById(R.id.policy_mail_state);et_mailstate.addTextChangedListener(new TextWatcher1(et_mailstate));
		et_mailaddress2  = (EditText) findViewById(R.id.policy_mail_add2);et_mailaddress2.addTextChangedListener(new TextWatcher1(et_mailaddress2));
		et_mailcounty  = (EditText) findViewById(R.id.policy_mail_county);et_mailcounty.addTextChangedListener(new TextWatcher1(et_mailcounty));
		et_mailcity  = (EditText) findViewById(R.id.policy_mail_city);et_mailcity.addTextChangedListener(new TextWatcher1(et_mailcity));
		et_mailzip  = (EditText) findViewById(R.id.policy_mail_zipcode);
		
		schedulecomment.addTextChangedListener(new TextWatchLimit(schedulecomment,500,(TextView) findViewById(R.id.SH_TV_ED)));
		
		  etinspfees.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (etinspfees.getText().toString().matches("0"))
			        {
			            // Not allowed
						etinspfees.setText("");
			        }
					if (etinspfees.getText().toString().startsWith(" "))
			        {
			            // Not allowed
						etinspfees.setText("");
			        }
					if (etinspfees.getText().toString().matches("^0"))
			        {
			            // Not allowed
						etinspfees.setText("");
			        }
					if(!etinspdiscount.getText().toString().trim().equals(""))
					{
						try
						{
							if(Integer.parseInt(etinspdiscount.getText().toString())>Integer.parseInt(etinspfees.getText().toString()))
							{
								cf.ShowToast("Please enter Discount less than the Inspection Fee");
								cf.hidekeyboard(etinspdiscount);
								
							}
						}
						catch(Exception e)
						{
							System.out.println("etinspfees"+e.getMessage());
						}
					}
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
	       
	       etinspdiscount.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if (etinspdiscount.getText().toString().matches("0"))
			        {
			            // Not allowed
						etinspdiscount.setText("");
			        }
					if (etinspdiscount.getText().toString().startsWith(" "))
			        {
			            // Not allowed
						etinspdiscount.setText("");
			        }
					if (etinspdiscount.getText().toString().matches("0"))
			        {
			            // Not allowed
						etinspdiscount.setText("");
			        }
					 if(etinspfees.getText().toString().trim().equals(""))
						{
							cf.ShowToast("Please enter Inspection Fee");
							etinspfees.requestFocus();
							cf.hidekeyboard(etinspdiscount);
						}
						else 
						{
							if(!etinspdiscount.getText().toString().trim().equals(""))
							{
								try
								{
									if(Integer.parseInt(etinspdiscount.getText().toString())>Integer.parseInt(etinspfees.getText().toString()))
									{
										cf.ShowToast("Please enter Discount less than the Inspection Fee");
										cf.hidekeyboard(etinspdiscount);
										
									}
								}
								catch(Exception e)
								{
									System.out.println("etinspdiscount"+e.getMessage());
								}
							}
							
						}
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.chkmailing:
	    	if(((CheckBox)findViewById(R.id.chkmailing)).isChecked())
			{	
	    		System.out.println("dsfsdf");
				 ml = 1;
				et_mailaddress1.setText(address1.getText().toString());
				et_mailaddress2.setText(address2.getText().toString());
				et_mailcity.setText(city.getText().toString());
				et_mailstate.setText(state.getText().toString());
				et_mailcounty.setText(country.getText().toString());
				et_mailzip.setText(zip.getText().toString());
				
  		    }
			else
			{	ml=0;
				et_mailaddress1.setText("");
				et_mailaddress2.setText("");
				et_mailcity.setText("");
				et_mailstate.setText("");
				et_mailcounty.setText("");
				et_mailzip.setText("");
			}
	   	break;
		case R.id.orderinsp_getdate:
			showDialogDate(etdate);
			break;
		case R.id.orderinsp_gettime:
			showDialog(TIME_DIALOG_ID);
			break;
		case R.id.hme:
			cf.gohome();
			break;
		case R.id.save:
			strfname = firstname.getText().toString();System.out.println("fnamne");
			strlname = lastname.getText().toString();
			straddr1 = address1.getText().toString();
			straddr2 = address2.getText().toString();
			strcity = city.getText().toString();
			strzip = zip.getText().toString();
			strstate = state.getText().toString();System.out.println("mob");
			strcountry = country.getText().toString();
			stryrofhme = yearofhome.getSelectedItem().toString();
			if (stryrofhme.equals("Other"))
				stryrofhme = otheryearhome.getText().toString();
			strcontactperson = contactperson.getText().toString();
			strhmephn = homephone.getText().toString();
			strwrkphn = workphone.getText().toString();
			strcellphn = cellphone.getText().toString();System.out.println("cell");
			strpolicy = policy.getText().toString();
			strstories = stories.getText().toString();
			stremail = email.getText().toString();
			
			schcmt = schedulecomment.getText().toString();
			System.out.println("fsdfdsf");
			try {
				otheryear = Integer.parseInt(stryrofhme);
			} catch (Exception e) {
				otheryear = 0;
				//cf.Error_LogFile_Creation(e.getMessage()+" "+" at "+ PersonalInfo.this +" problem in converting datatypes on "+cf.datewithtime+" "+"in apk"+" "+cf.rcstr);
			}
			if (!strwrkphn.equals("")) {
				if(!cf.PhoneNo_validation(strwrkphn).equals("Yes")){
					cf.ShowToast("Please enter the Work Phone in 10 digit Number");
					workphone.setText("");
					workphone.requestFocus();
					chkstatus[0] = "false";
				} else {
					chkstatus[0] = "true";
				}
			} else {
				chkstatus[0] = "true";
			}
			if (!strcellphn.equals("")) {
				if(!cf.PhoneNo_validation(strcellphn).equals("Yes")){
					cf.ShowToast("Please enter the Cell Phone in 10 digit Number");
					cellphone.setText("");
					cellphone.requestFocus();
					chkstatus[1] = "false";
				} else {
					chkstatus[1] = "true";

				}
			} else {
				chkstatus[1] = "true";
			}
		
			ph[0] =(!strwrkphn.equals(""))? ((cf.PhoneNo_validation(strwrkphn)!="Yes")? false:true):true;		/****CHECKING WORKPHONE VALIDATION**/
			ph[1] =(!strcellphn.equals(""))? ((cf.PhoneNo_validation(strcellphn)!="Yes")? false:true):true;	/***CHECKING CELLPHONE VALIDATION***/
			ph[2] =(!strhmephn.equals(""))? ((cf.PhoneNo_validation(strhmephn)!="Yes")? false:true):true;	/***CHECKING HOMEPHONE VALIDATION***/
			strtime = ettime.getText().toString();
			
			
			boolean booldiscount = true;
			if (!etinspdiscount.getText().toString().trim().equals("") && !etinspfees.getText().toString().trim().equals("")) 
			{
				try
				{
					if(Integer.parseInt(etinspdiscount.getText().toString())>Integer.parseInt(etinspfees.getText().toString()))
					{
						booldiscount=false;
					}
					else
					{
						booldiscount=true;
					}
				}
				catch(Exception e)
				{
					System.out.println("booldiscount"+e.getMessage());
				}
			} 
			else
			{
				booldiscount = true;
			}	
			
			if (!"".equals(strfname)) 
			{
				if (!"".equals(strlname)) 
				{
					//if (!"".equals(strhmephn)) 
					//{
						if(ph[2]==true)
						{
						 if(ph[0]==true) {/****CHECKING WORKPHONE IS AVAILABLE OR NOT*/
			               if(ph[1]==true) {/****CHECKING CELLPHONE IS AVAILABLE OR NOT*/
							if (strstories.length() != 0) 
							{
								if (!etdate.getText().toString().trim().equals("")) 
								{
									if (!strtime.equals("")) 
									{
										if (!etinspfees.getText().toString().trim().equals("")) 
										{
											if(booldiscount){
											/*if (!etinspdiscount.getText().toString().trim().equals("")) 
											{*/
												if (stremail.equals("") && (mc == 0)) 
												{
													cf.ShowToast("Please enter the Email");
													email.requestFocus();
													chkstatus[2] = "false";
												}
												else 
												{
													if (mc == 1) 
													{
														chkstatus[2] = "true";
														yearvalidation();
													} 
													else if (mc == 0) 
													{
														if (cf.Email_Validation(stremail) == false)
														{
															cf.ShowToast("Please enter the valid Email");
															email.setText("");
															email.requestFocus();
															chkstatus[2] = "false";
														} 
														else 
														{
															chkstatus[2] = "true";
															yearvalidation();
														}
													}											
												}
											}
											else
											{
												cf.ShowToast("Please enter Discount less than Inspection Fee");
												etinspdiscount.requestFocus();
											}
										}
										else
										{
											cf.ShowToast("Please enter Inspection Fee");
											etinspfees.requestFocus();
										}
											
											
									}
									else
									{
										cf.ShowToast("Please select Inspection Time");
									}
								}
								else
								{
									cf.ShowToast("Please select Inspection Date");
									etdate.requestFocus();
									etdate.setText("");
								}									
							}
							else
							{
								cf.ShowToast("Please enter the No. of Stories");
								stories.requestFocus();
							}
	                    	 }
	 						else
	 						{
	 							cf.ShowToast("Please enter the Cell Phone in 10 digit Number");
	 							cellphone.requestFocus();
	 						}
	 					
	 						}
	 						else
	 						{
	 							cf.ShowToast("Please enter the Work Phone in 10 digit Number");
	 							workphone.requestFocus();
	 						}							
                    	/* }
 						else
 						{
 							cf.ShowToast("Please enter the Home Phone in 10 digit Number");
 						}*/
			
					}
					else
					{
						cf.ShowToast("Please enter the Home Phone in 10 digit Number");
						homephone.requestFocus();
					}	
				} 
				else 
				{
					cf.ShowToast("Please enter the Last Name");
					lastname.requestFocus();
				}
			}
			else 
			{
				cf.ShowToast("Please enter the First Name");
				firstname.requestFocus();
			}

			break;
		}
	}

	
	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			int hour = selectedHour;
			int minute = selectedMinute;
			StringBuilder sb = new StringBuilder();
			String hr="",min="";
			final Calendar c = Calendar.getInstance();
			if (hour >= 12) {
				hour=hour-12;
				if(hour<10)
				{
				hr="0"+hour;
				}
				else
				{
				hr=String.valueOf(hour);
				}
				if(minute<10)
				{
				min="0"+minute;
				}
				else
				{
				min=String.valueOf(minute);
				}
				sb.append(hr).append(":").append(min).append(" PM");
				} else {
				if(hour<10)
				{
				hr="0"+hour;
				}
				else
				{
				hr=String.valueOf(hour);
				}
				if(minute<10)
				{
				min="0"+minute;
				}
				else
				{
				min=String.valueOf(minute);
				}
				sb.append(hr).append(":").append(min).append(" AM");
				}


			ettime.setText(sb);
			strtime = ettime.getText().toString();
			
			//updating time picker to current time

			
			timepicker.updateTime(c.get(Calendar.HOUR_OF_DAY), 
					 c.get(Calendar.MINUTE));
		}
	};
	@Override
	protected Dialog onCreateDialog(int id) {

	switch (id) {
	case TIME_DIALOG_ID:
		final Calendar c1 = Calendar.getInstance();
		int hour = c1.get(Calendar.HOUR_OF_DAY);
		int minute = c1.get(Calendar.MINUTE);
		timepicker= new TimePickerDialog(this, timePickerListener, hour, minute,
				false);
		return timepicker;
	}
	return null;
	}
	private void yearvalidation()
	{
		if (!yearofhome.getSelectedItem().toString().equals("Select")) 
		{
			if ((!yearofhome.getSelectedItem().toString().equals("Other"))  || (!otheryearhome.getText().toString().equals("") && 4 == otheryearhome.getText().length())) 
			{
				inspaddrvalidation();
			/*	if (otheryear >= 1900 && otheryear <= 2013) 
				{
					
				}
				else
				{
					cf.ShowToast("Please enter year of home greater than 1900 and less than 2013");
					otheryearhome.setText("");
					otheryearhome.requestFocus();
				}*/
			}
			else
			{
				if(otheryearhome.getText().toString().trim().equals(""))
				{
					cf.ShowToast("Please enter the other text for year of home");	
				}
				else if(otheryearhome.getText().length()<4)
				{
					cf.ShowToast("Please enter Year of home in four digits");	
				}
				otheryearhome.requestFocus();
			}
		}
		else
		{
			cf.ShowToast("Please select the Year of Home");
			yearofhome.requestFocus();
		}
	}
	
	private void inspaddrvalidation() {
		// TODO Auto-generated method stub
		if (!"".equals(straddr1))
		{
			if (strzip.length() == 5) 
			{
				if (!"".equals(strcity)) 
				{
					if (!"".equals(strstate)) 
					{
						if (!"".equals(strcountry)) 
						{
							Mailing_Validation();
						} 
						else 
						{
							cf.ShowToast("Please enter the Inspection County");
							country.requestFocus();
						}
					} 
					else 
					{
						cf.ShowToast("Please enter the Inspection State");						
						state.requestFocus();
					}
				} 
				else 
				{
					cf.ShowToast("Please enter the Inspection City");
					city.requestFocus();
				}
			} 
			else 
			{
				cf.ShowToast("Please enter the Inspection Zipcode");
				zip.setText("");
				zip.requestFocus();
			}
		}
		else 
		{
			cf.ShowToast("Please enter the Inspection Address #1");
			address1.requestFocus();
		}		
	}
	public void Mailing_Validation()
	{
		if (!"".equals(et_mailaddress1.getText().toString().trim())) {  /*CHECKING MAILING ADRESS1 IS AVAILABLE OR NOT*/
			 if (!"".equals(et_mailzip.getText().toString().trim())) {/*CHECKING ZIP IS AVAILABLE OR NOT*/
	                if (et_mailzip.getText().length() == 5) { /*CHECKING LENGTH OF ZIPCODE*/
	               	 if (!"".equals(et_mailcity.getText().toString().trim())) {  /*CHECKING MAILING CITY IS AVAILABLE OR NOT*/
                		 if (!"".equals(et_mailstate.getText().toString().trim())) {  /*CHECKING MAILING STATE IS AVAILABLE OR NOT*/
                			if (!"".equals(et_mailcounty.getText().toString().trim())){   /*CHECKING MAILING COUNTY IS AVAILABLE OR NOT*/
                				if(((EditText)findViewById(R.id.eschcomment)).getText().toString().trim().equals(""))
		                		{
		                			cf.ShowToast("Please enter the Scheduling Comments");
		                			cf.setFocus(((EditText)findViewById(R.id.eschcomment)));
		                		}
		                		else
		                		{
	                			  update();
		                		}
                			}
        	                else
        	                {
        	                	cf.ShowToast("Please enter the Mailing County");
    							cf.setFocus(et_mailcounty);
        	                }
                		 }
     	                 else
     	                 {
     	                	cf.ShowToast("Please enter the Mailing State");
 							cf.setFocus(et_mailstate);
     	                 }
	               	  }
 	                  else
 	                  {
 	                	  cf.ShowToast("Please enter the Mailing City");
						  cf.setFocus(et_mailcity);
 	                  }
	                }
	                else
	                {
	                	cf.ShowToast("Please enter the valid ZipCode");
						et_mailzip.setText("");cf.setFocus(et_mailzip);
	                }
			 }
			 else
			 {
				 cf.ShowToast("Please enter the Mailing ZipCode");
					cf.setFocus(et_mailzip);
			 }			
		}
        else 
        {
			cf.ShowToast("Please enter the Mailing Address #1");
			cf.setFocus(et_mailaddress1);
		}	
	}
	private void update() {

		// TODO Auto-generated method stub
		if(mailchk.isChecked())
		{
			stremail = "";
		}
		else
		{
			stremail = email.getText().toString();
		}
		if (chkstatus[0] == "true" && chkstatus[1] == "true"
				&& chkstatus[2] == "true" && chkstatus[3] == "true"
				&& chkstatus[5] == "true" && chkstatus[6] == "true") {
			try {
				
				Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "+ db.Questions + " WHERE SRID='" + cf.Homeid+ "'", null);
				int rws = c2.getCount();
				
				db.wind_db.execSQL("UPDATE " + db.policyholder
						+ " SET PH_FirstName='"+ db.encode(strfname)+ "',PH_LastName='"+ db.encode(strlname)+ "',PH_Address1='"+ db.encode(straddr1) + "'," +
								"PH_City='"+ db.encode(strcity) + "',PH_Zip='"+ strzip + "',PH_State='"+ db.encode(strstate) + "',PH_County='"+ db.encode(strcountry)+ "'," +
								"YearBuilt='" + stryrofhme+ "',PH_ContactPerson='"+ db.encode(strcontactperson)+ "',PH_HomePhone='" + strhmephn+ "',PH_WorkPhone='" + strwrkphn+ "'," +
								"PH_CellPhone='" + strcellphn+ "',PH_Policyno='"+ db.encode(strpolicy) + "',PH_NOOFSTORIES='"+ strstories + "',PH_Email='" + stremail+ "'," +
								"chkbx='" + mc + "',Fee='"+db.encode(etinspfees.getText().toString())+"',Discount='"+db.encode(etinspdiscount.getText().toString())+"',Schedule_Comments='"+db.encode(schedulecomment.getText().toString().trim())+"' WHERE PH_SRID ='" + cf.Homeid+ "'");
				System.out.println("UPDATE " + db.policyholder
						+ " SET PH_FirstName='"+ db.encode(strfname)+ "',PH_LastName='"+ db.encode(strlname)+ "',PH_Address1='"+ db.encode(straddr1) + "'," +
						"PH_City='"+ db.encode(strcity) + "',PH_Zip='"+ strzip + "',PH_State='"+ db.encode(strstate) + "',PH_County='"+ db.encode(strcountry)+ "'," +
						"YearBuilt='" + stryrofhme+ "',PH_ContactPerson='"+ db.encode(strcontactperson)+ "',PH_HomePhone='" + strhmephn+ "',PH_WorkPhone='" + strwrkphn+ "'," +
						"PH_CellPhone='" + strcellphn+ "',PH_Policyno='"+ db.encode(strpolicy) + "',PH_NOOFSTORIES='"+ strstories + "',PH_Email='" + stremail+ "'," +
						"chkbx='" + mc + "',Fee='"+db.encode(etinspfees.getText().toString())+"',Discount='"+db.encode(etinspdiscount.getText().toString())+"',Schedule_Comments='"+db.encode(schedulecomment.getText().toString().trim())+"' WHERE PH_SRID ='" + cf.Homeid+ "'");
				
				if (rws == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.Questions
							+ " (SRID,BuildingCodeYearBuilt,BuildingCodePerApplnDate,BuildingCodeValue,RoofCoverType,RoofCoverValue,RoofCoverOtherText,RoofCoverPerApplnDate,RoofCoverYearofInsDate,RoofCoverProdAppr,RoofCoverNoInfnProvide,RoofDeckValue,RoofDeckOtherText,RooftoWallValue,RooftoWallSubValue,RooftoWallClipsMinValue,RooftoWallSingleMinValue,RooftoWallDoubleMinValue,RooftoWallOtherText,RoofGeoValue,RoofGeoLength,RoofGeoTotalArea,RoofGeoOtherText,SWRValue,OpenProtectType,OpenProtectValue,OpenProtectSubValue,OpenProtectLevelChart,GOWindEntryDoorsValue,GOGarageDoorsValue,GOSkylightsValue,NGOGlassBlockValue,NGOEntryDoorsValue,NGOGarageDoorsValue,WoodFrameValue,WoodFramePer,ReinMasonryValue,ReinMasonryPer,UnReinMasonryValue,UnReinMasonryPer,PouredConcrete,PouredConcretePer,OtherWallTitle,OtherWal,OtherWallPer,EffectiveDate,Completed,OverallComments,ScheduleComments,ModifyDate,GeneralHazardInclude)"
							+ "VALUES ('" + cf.Homeid + "','','','" + 0 + "','"
							+ 0 + "','" + 0 + "','','','','','" + 0 + "','" + 0
							+ "','','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
							+ "','" + 0 + "','','" + 0 + "','" + 0 + "','" + 0
							+ "','','" + 0 + "','','" + 0 + "','" + 0
							+ "','','','','','','','','" + 0 + "','" + 0
							+ "','" + 0 + "','" + 0 + "','" + 0 + "','" + 0
							+ "','" + 0 + "','" + 0 + "','','" + 0 + "','" + 0
							+ "','','" + 0 + "','','"
							+ db.encode(schcmt) + "','" + cd
							+ "','" + 0 + "')");
				} else {
					db.wind_db.execSQL("UPDATE " + db.Questions
							+ " SET ScheduleComments='"
							+ db.encode(schcmt)
							+ "',ModifyDate ='" + md + "' WHERE SRID ='"
							+ cf.Homeid + "'");

				}

				Cursor c3 = db.wind_db.rawQuery("SELECT * FROM "
						+ db.SubmitCheckTable + " WHERE Srid='" + cf.Homeid
						+ "'", null);
				if ( c3.getCount() == 0) {
					db.wind_db.execSQL("INSERT INTO "
							+ db.SubmitCheckTable
							+ " (InspectorId,Srid,fld_policy,fld_builcode,fld_roofcover,fld_roofdeck,fld_roofwall,fld_roofgeo,fld_swr,fld_open,fld_wall,fld_signature,fld_front,fld_feedbackinfo,fld_feedbackdoc,fld_overall,fld_hazarddata)"
							+ "VALUES ('" + db.Insp_id + "','" + cf.Homeid
							+ "',1,0,0,0,0,0,0,0,0,0,0,0,0,0,0)");

				} else {
					db.wind_db.execSQL("UPDATE " + db.SubmitCheckTable
							+ " SET fld_policy='1' WHERE Srid ='" + cf.Homeid
							+ "' and InspectorId='" + db.Insp_id + "'");
				}
				
				/*CHECKING MAILING ADDRESS TABLE*/
				Cursor c = db.wind_db.rawQuery("SELECT * FROM "
						+ db.MailingPolicyHolder + " WHERE ML_PH_SRID='" + db.encode(cf.Homeid)
						+ "'", null);
				if (c.getCount() == 0) {
						db.wind_db.execSQL("INSERT INTO "
								+ db.MailingPolicyHolder
								+ " (ML_PH_SRID,ML_PH_InspectorId,ML,ML_PH_Address1,ML_PH_Address2,ML_PH_City,ML_PH_Zip,ML_PH_State,ML_PH_County)"
								+ "VALUES ('"+cf.Homeid+"','"+db.encode(db.Insp_id)+"','"+db.encode(Integer.toString(ml))+"','"+db.encode(et_mailaddress1.getText().toString())+"','"
							    + db.encode(et_mailaddress2.getText().toString())+"','"+db.encode(et_mailcity.getText().toString())+"','"
							    + db.encode(et_mailzip.getText().toString())+"','"+db.encode(et_mailstate.getText().toString())+"','"
							    + db.encode(et_mailcounty.getText().toString())+"')");
				}
				else
				{
					db.wind_db.execSQL("UPDATE " + db.MailingPolicyHolder
							+ " SET ML='"+db.encode(Integer.toString(ml))+"',ML_PH_Address1='" + db.encode(et_mailaddress1.getText().toString())
							+ "',ML_PH_Address2='"
							+ db.encode(et_mailaddress2.getText().toString())
							+ "',ML_PH_City='"
							+ db.encode(et_mailcity.getText().toString())
							+ "',ML_PH_Zip='"
							+ db.encode(et_mailzip.getText().toString()) + "',ML_PH_State='"
							+ db.encode(et_mailstate.getText().toString()) + "',ML_PH_County='"
							+ db.encode(et_mailcounty.getText().toString()) + "' WHERE ML_PH_SRID ='" +cf.Homeid
							+ "'");
				}
				cf.ShowToast("Policy Holder Information saved successfully");
				Intent intque = new Intent(PolicyholderInfo.this, QuesBuildCode.class);
				intque.putExtra("homeid", cf.Homeid);
				intque.putExtra("status", cf.status);
				intque.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intque);
				finish();
				
			} catch (Exception e) {

			}
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;

			if (f == 1) {
				if (cf.InspectionType.equals("28")) {
					effdat.setText(new StringBuilder()
							// Month is 0 based so add 1
							.append(mMonth + 1).append("/").append(mDay)
							.append("/").append(mYear).append(" "));
				} else {

				}

			} else {
				inspectiondate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(mMonth + 1).append("/").append(mDay)
						.append("/").append(mYear).append(" "));
			}

		}
	};


	public void viewownerinfo() {
		String schdat = "",schcmt = "",	insdate = "",inspstarttime = "",inspendtime = "",fname = "",lname = "",addr = "",cty = "",zpcde = "",
				statnam = "",cntry = "",yrhme = "",wrkphn = "",cellphn = "",policynum = "",	storiesnum = "",compname = "",insname="",contperson="";
		try {
			Cursor c2 = db.wind_db.rawQuery("SELECT * from " + db.policyholder +" WHERE PH_SRID='"+ cf.Homeid + "'", null);
			System.out.println("c2="+c2.getCount());
			if(c2.getCount()>0)
			{
				c2.moveToFirst();
				do 
				{
					schcmt = db.decode(c2.getString(c2.getColumnIndex("Schedule_Comments")));System.out.println("schcmt"+schcmt);
					System.out.println(db.decode(c2.getString(c2.getColumnIndex("PH_ContactPerson"))));
					contactperson.setText(db.decode(c2.getString(c2.getColumnIndex("PH_ContactPerson"))));System.out.println("dsfdf");
					schedulecomment.setText(db.decode(c2.getString(c2.getColumnIndex("Schedule_Comments"))));System.out.println("dsfdstest");
					
					homephn =  db.decode(c2.getString(c2.getColumnIndex("PH_HomePhone")));System.out.println("homephn");
					wrkphn = db.decode(c2.getString(c2.getColumnIndex("PH_WorkPhone")));System.out.println("wrkphn");
					cellphn = db.decode(c2.getString(c2.getColumnIndex("PH_CellPhone")));System.out.println("cell");
					
					System.out.println("homephn"+homephn);
					if (homephn.equals(anytype) || homephn.equals("Null") || homephn.equals("Not Available") || homephn.equals("") || homephn.equals("N/A")) 
					{
						System.out.println("inside");
						homephone.setText("");
					} else 
					{
						System.out.println("else");
						StringBuilder sVowelBuilder1 = new StringBuilder(homephn);
						sVowelBuilder1.deleteCharAt(0);
						sVowelBuilder1.deleteCharAt(3);
						sVowelBuilder1.deleteCharAt(6);
						homephn = sVowelBuilder1.toString();
						homephone.setText(homephn);
					}
					System.out.println("homephnends");
					if (wrkphn.equals(anytype) || wrkphn.equals("Null") || wrkphn.equals("Not Available") || wrkphn.equals("") || wrkphn.equals("N/A"))
					{
						workphone.setText("");
					} 
					else 
					{
						StringBuilder sVowelBuilder1 = new StringBuilder(wrkphn);
						sVowelBuilder1.deleteCharAt(0);
						sVowelBuilder1.deleteCharAt(3);
						sVowelBuilder1.deleteCharAt(6);
						wrkphn = sVowelBuilder1.toString();
						workphone.setText(wrkphn);
					}
					
					System.out.println("wrkphnends");
					if (cellphn.equals(anytype) || cellphn.equals("Null") || cellphn.equals("Not Available") || cellphn.equals("") || cellphn.equals("N/A")) 
					{
						cellphone.setText("");
					} 
					else 
					{
						StringBuilder sVowelBuilder1 = new StringBuilder(cellphn);
						sVowelBuilder1.deleteCharAt(0);
						sVowelBuilder1.deleteCharAt(3);
						sVowelBuilder1.deleteCharAt(6);
						cellphn = sVowelBuilder1.toString();
						cellphone.setText(cellphn);
					}
					System.out.println("cellphnends");
					
					firstname.setText(db.decode(c2.getString(c2.getColumnIndex("PH_FirstName"))));
					lastname.setText(db.decode(c2.getString(c2.getColumnIndex("PH_LastName"))));
					address1.setText(db.decode(c2.getString(c2.getColumnIndex("PH_Address1"))));
					address2.setText(db.decode(c2.getString(c2.getColumnIndex("PH_Address2"))));
					city.setText(db.decode(c2.getString(c2.getColumnIndex("PH_City"))));
					zip.setText(db.decode(c2.getString(c2.getColumnIndex("PH_Zip"))));
					state.setText(db.decode(c2.getString(c2.getColumnIndex("PH_State"))));
					country.setText(db.decode(c2.getString(c2.getColumnIndex("PH_County"))));
					policy.setText(db.decode(c2.getString(c2.getColumnIndex("PH_Policyno"))));
					stories.setText(db.decode(c2.getString(c2.getColumnIndex("PH_NOOFSTORIES"))));					
					schedulecomment.setText(db.decode(c2.getString(c2.getColumnIndex("Schedule_Comments"))));
					
					if(c2.getString(c2.getColumnIndex("PH_Email")).equals("N/A"))
					{
						email.setText("");
					}
					else
					{
					   email.setText(db.decode(c2.getString(c2.getColumnIndex("PH_Email"))));
					}
					System.out.println("yrbuilt");
					
					yrhme = c2.getString(c2.getColumnIndex("YearBuilt"));
					if (yrhme.equals(anytype) || yrhme.equals("Null") || yrhme.equals("Not Available") || yrhme.equals("") || yrhme.equals("N/A")) 
					{
						yearofhome.setSelection(0);
					} 
					else
					{
						int y1;
						try {
							y1 = Integer.parseInt(yrhme);
						} catch (Exception e) {
							yrhme = "0";
							y1 = 0;
						}

						if (y1 > 2013 || y1 < 1964) {
							yearofhome.setSelection(1);
							otheryearhome.setText(yrhme.toString());

							otheryearhome.setVisibility(View.VISIBLE);

						} else {
							spinnerPosition = adapter.getPosition(yrhme);
							yearofhome.setSelection(spinnerPosition);
						}
					}
						
						if (c2.getString(c2.getColumnIndex("chkbx")).equals("1")) {
							mc=1;
							mailchk.setChecked(true);
							email.setEnabled(false);
						} else {
						   mc=0;
						   mailchk.setChecked(false);
							email.setEnabled(true);
						}
						System.out.println("maillad");
					
					etdate.setText(db.decode(c2.getString(c2.getColumnIndex("InspectionDate"))));
					ettime.setText(db.decode(c2.getString(c2.getColumnIndex("InspectionTime"))));
					etinspfees.setText(db.decode(c2.getString(c2.getColumnIndex("Fee"))));
					etinspdiscount.setText(db.decode(c2.getString(c2.getColumnIndex("Discount"))));
					
					System.out.println("ends");
				} while (c2.moveToNext());
			}
			c2.close();

		} catch (Exception e) {
			System.out.println("viewowner"+e.getMessage());	
		}
		/****Show the saved mailling address **/
	    try {
			Cursor c2 = db.wind_db.rawQuery("SELECT * FROM "
					+ db.MailingPolicyHolder + " WHERE ML_PH_SRID='" + db.encode(cf.Homeid)
					+ "' and ML_PH_InspectorId='" + db.encode(db.Insp_id) + "'", null);
			int rws = c2.getCount();
			c2.moveToFirst();
			if (c2 != null) {
				System.out.println("CCCC");
					et_mailaddress1.setText(db.decode(c2.getString(c2.getColumnIndex("ML_PH_Address1"))));
					et_mailaddress2.setText(db.decode(c2.getString(c2.getColumnIndex("ML_PH_Address2"))));
					et_mailcity.setText(db.decode(c2.getString(c2.getColumnIndex("ML_PH_City"))));
					et_mailzip.setText(db.decode(c2.getString(c2.getColumnIndex("ML_PH_Zip"))));
					et_mailstate.setText(db.decode(c2.getString(c2.getColumnIndex("ML_PH_State"))));
					et_mailcounty.setText(db.decode(c2.getString(c2.getColumnIndex("ML_PH_County"))));
					if (db.decode(c2.getString(c2.getColumnIndex("ML"))).equals("1")) {
						ml=1;
						((CheckBox)findViewById(R.id.chkmailing)).setChecked(true);
					} else {
						ml=0;
						((CheckBox)findViewById(R.id.chkmailing)).setChecked(false);
					}
				
			}
			
	  } catch (Exception e) {
  }
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			System.out.println("cf.status"+cf.status);
			Intent intimg = new Intent(PolicyholderInfo.this, HomwOwnerListing.class);
			//intimg.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intimg.putExtra("status", cf.status);
			startActivity(intimg);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*switch (resultCode) {
	
			case 0:
			
				break;
			case -1:
				try {
					String[] projection = { MediaStore.Images.Media.DATA };//System.out.println("DATA");
					System.out.println("cf.mCapturedImageURI "+cf.mCapturedImageURI);
					Cursor cursor = managedQuery(cf.mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor.getString(column_index_data);
					
					cf.showselectedimage(capturedImageFilePath);
				} catch (Exception e) {
					System.out.println("catch -1 "+e.getMessage());
				}
				
				break;

		}*/

	}
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(PolicyholderInfo.this,
				new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}
	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				System.out.println("current date " + date1);
				System.out.println("selected date " + date2);
				if (date2.compareTo(date1) > 0) {
					System.out.println("inside date if");
					cf.ShowToast("Please select Current and Past Date");
					v.setText("");
				} else {
					System.out.println("inside date else");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}
