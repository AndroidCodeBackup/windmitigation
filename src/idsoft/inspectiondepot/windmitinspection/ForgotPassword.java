package idsoft.inspectiondepot.windmitinspection;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;


public class ForgotPassword extends Activity{
	EditText forgot_et_username,forgot_et_email;
	CommonFunctions cf;
	WebserviceFunctions wb;
	String retpassword="",errormsg="",retusername="";
	int show_handler;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgotpassword);
		cf = new CommonFunctions(this);
		wb = new WebserviceFunctions(this);
		
		forgot_et_username = (EditText)findViewById(R.id.forgot_username);
		forgot_et_email = (EditText)findViewById(R.id.forgot_email);
		
		forgot_et_username.addTextChangedListener(new TextWatcher1(forgot_et_username));
		forgot_et_email.addTextChangedListener(new TextWatcher1(forgot_et_email));
		
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.submit:
			if(forgot_et_username.getText().toString().trim().equals("") && forgot_et_email.getText().toString().trim().equals(""))
			{
				cf.ShowToast("Please enter atleast any one option");
			}
			else
			{
				
				if(forgot_et_username.getText().toString().trim().equals(""))
				{
					if(!forgot_et_email.getText().toString().trim().equals(""))
					{
						boolean chkmail = cf.Email_Validation(forgot_et_email.getText().toString());
						if(chkmail)
						{
							forgot_password();
						}
						else
						{
							cf.ShowToast("Please enter valid Email");
							forgot_et_email.requestFocus();
						}
					}
			
				}
				else
				{
					if(forgot_et_email.getText().toString().trim().equals(""))
					{
						forgot_password();
					}
					else
					{
						boolean chkmail = cf.Email_Validation(forgot_et_email.getText().toString());
						if(chkmail)
						{
							forgot_password();
						}
						else
						{
							cf.ShowToast("Please enter valid Email");
							forgot_et_email.requestFocus();
						}
					}
					
				}
				
			}
			break;
		case R.id.clear:
			forgot_et_username.setText("");
			forgot_et_email.setText("");
			forgot_et_username.requestFocus();
			break;

		default:
			break;
		}
	}
	private void forgot_password() {
		// TODO Auto-generated method stub
		if (wb.isInternetOn() == true) {
			cf.show_ProgressDialog("Retrieving password..");
			new Thread() {
				public void run() {
					Looper.prepare();
					
					try
					{
						SoapObject request = new SoapObject(wb.NAMESPACE,"FORGOTPASSWORD");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("Username",forgot_et_username.getText().toString());
						request.addProperty("Emailid",forgot_et_email.getText().toString());
						envelope.setOutputSoapObject(request);
						System.out.println("FORGOTPASSWORD request is " + request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);						
						androidHttpTransport.call(wb.NAMESPACE+"FORGOTPASSWORD", envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						
						System.out.println("FORGOTPASSWORD result is"+ result);
						
						String status = String.valueOf(result.getProperty("Status"));
						System.out.println("status="+status);
						if(status.toLowerCase().equals("true"))
						{
							retpassword = String.valueOf(result.getProperty("Inspectorpassword"));
							retusername = String.valueOf(result.getProperty("Inspectorusername"));
							show_handler=1;
							handler.sendEmptyMessage(0);
						}
						else
						{
							errormsg = String.valueOf(result.getProperty("Errormsg"));
							show_handler=2;
							handler.sendEmptyMessage(0);
						}
						
						
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}
				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your Network. Please try again later with better Network");

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.ShowToast("There is a problem on your application. Please contact Paperless administrator");

						} else if (show_handler == 2) {
							show_handler = 0;
							cf.ShowToast(errormsg);
						}else if (show_handler == 1) {
							show_handler = 0;
							show_popup();
						}
					}
				};
			}.start();
		}
		else
		{
			cf.ShowToast("Internet connection not available");
		}
	}
	private void show_popup() {
		// TODO Auto-generated method stub
		final Dialog add_dialog = new Dialog(ForgotPassword.this,android.R.style.Theme_Translucent_NoTitleBar);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.forgotpasswordcustom);
		 
        ((TextView)add_dialog.findViewById(R.id.redirect)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(ForgotPassword.this,WindMitInspection.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra("status", "forgot");
				intent.putExtra("retpass", retpassword);
				intent.putExtra("retuser", retusername);
				startActivity(intent);
				finish();
		
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(ForgotPassword.this,WindMitInspection.class));
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
