package idsoft.inspectiondepot.windmitinspection;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ExportData extends Activity
{
	boolean ex_function[] ={false,false,false,false,false,false};
	boolean ex_status[] ={false,false,false,false,false,false};
	private int vcode,usercheck,mState,RUNNING=1;
	int delay = 40; // Milliseconds of delay in the update loop
	ProgressDialog pd1;
	double total; // Determines type progress bar: 0 = spinner, 1 = horizontal
	private ProgressDialog progDialog;
	PowerManager.WakeLock wl=null;
	private String  Error_tracker="",elevationname="";
	int Current_insp=0,stateid,countyid,val_handler,show_handler;
	public ProgressThread progThread;
	int handler_verif=0,maxBarValue=0;
	int typeBar = 1;
	String Exportbartext="",feed_missingfiles;
	CommonFunctions cf;
	DatabaseFunctions db;
	WebserviceFunctions wb;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.import_data);
		//System.out.println("comes ocrrectly");
		
		cf=new CommonFunctions(this);
		Bundle extras = getIntent().getExtras();
		if (extras != null) 
		{
			cf.Homeid =  extras.getString("homeid");
		}
		 progDialog = new ProgressDialog(this);
		db=new DatabaseFunctions(this);
		wb=new WebserviceFunctions(this);
		start_export();
		
	}
	private void start_export() {
		// TODO Auto-generated method stub
		if (wb.isInternetOn() == true) {
			
			 PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
			 wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
			 wl.acquire();
			 total=0.0;
            /**We need to delete the preivious progress bar then we start new one code starts herer**/
				try
				{
				removeDialog(1);
				}
				catch (Exception e)
				{
					
				}
				/**We need to delete the preivious progress bar then we start new one code Ends herer**/
             
				showDialog(1);
				new Thread() {

					private String elevvetiondes;
					private int usercheck;

					public void run() {
						Looper.prepare();

						try {
							
						Error_tracker="";	
						String	selectedItem = cf.Homeid;
							total = 1.0;
							Exportbartext="Check the status of inspection ";
							total=5.0;
							String Chk_Inspector=wb.IsCurrentInspector(db.Insp_id,cf.Homeid,"IsCurrentInspector");
							if(Chk_Inspector.equals("true"))
							{  
								SoapObject add_property =wb.export_header("GetInspectionStatus");
								
								add_property.addProperty("InspectorID",db.Insp_id);
								add_property.addProperty("SRID",cf.Homeid);
							
								String result=wb.export_footer(wb.envelope, add_property, "GetInspectionStatus");
								System.out.println("he status"+result);
								if(result.trim().equals("40")||result.trim().equals("2") ||result.trim().equals("30"))
								{
									System.out.println("cam final");
									Current_insp=1;
									total=10.0;
									Exportbartext="Policy holder information";
									if(!ex_function[0])
									{
										ex_status[0]=updatepolicy();
									}
									else
									{
										ex_status[0]=true;
										Error_tracker+=" You have problem exporting policy holder information";
									}
									total=15.0;
									Current_insp=2;
									if(!ex_function[1])
									{
										Exportbartext="Questions";
										ex_status[1]=uploadquestions();
									}
									else
									{
										ex_status[1]=true;
										Error_tracker+=" You have problem exporting "+Exportbartext+" @";
									}
									
									total=40.0;
									Current_insp=3;
									
									if(!ex_function[2])
									{
										Exportbartext="Photos ";
										ex_status[2]=Expot_photos();
									}
									else
									{
										ex_status[2]=true;	
									}
									
									total=60.0;
									Current_insp=4;
									if(!ex_function[3])
									{
										Exportbartext="Feed back ";
										ex_status[3]=Expot_feedback();
									}
									else
									{
										ex_status[3]=true;	
									}
									
									total=80.0;
									Current_insp=5;
									
									if(!ex_function[4])
									{
										Exportbartext="Export General Information";
										ex_status[4]=Export_GCH();
									}
									else
									{
										ex_status[4]=true;	
									}
									
									total=90.0;
									Current_insp=6;
									
									if(!ex_function[5])
									{
										Exportbartext="Update Inspection Status";
										ex_status[5]=Update_Inspection_Status();
									}
									else
									{
										ex_status[5]=true;	
									}
									total=95.0;
									
									/***/
									System.out.println("ex_status[0]"+ex_status[0]+"  "+ex_status[1]+ " "+ex_status[2]);
									System.out.println("ex_status[3]"+ex_status[3]+"  "+ex_status[4]+ " "+ex_status[5]);
									
									
									if(ex_status[0] && ex_status[1] && ex_status[2] && ex_status[3] && ex_status[4] && ex_status[5])
									{
										/*db.wind_db.execSQL("UPDATE "
												+ db.policyholder
												+ " SET PH_Status=2 WHERE PH_SRID ='"
												+ cf.Homeid
												+ "'");	*/
										handler_verif=3;									
									}
									else
									{
										handler_verif=4;	
									}
									
									
									total=100.0;
									handler_export.sendEmptyMessage(0);
									Current_insp=6;
									
								}
								else
								{
									handler_verif=2;
									total=100.0;// Inspection not in the schedule or assign
									handler_export.sendEmptyMessage(0);
								}
								
							}
							else
							{
								handler_verif=1;
								total=100.0;// Inspection allocated to some other inspector
								handler_export.sendEmptyMessage(0);
							}
						}
						catch(SocketTimeoutException e)
						{
						handler_verif=4;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(SocketException e)
						{
						handler_verif=4;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(IOException e)
						{
						handler_verif=4;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(NetworkErrorException e)
						{
						handler_verif=4;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(TimeoutException e)
						{
						handler_verif=4;
						total=100.00;
						handler_export.sendEmptyMessage(0);
						Error_tracker+=" You have problem exporting "+Exportbartext+" @";
						}
						catch(Exception e)
							{
							handler_verif=4;
							total=100.00;
							handler_export.sendEmptyMessage(0);
							Error_tracker+=" You have problem exporting "+Exportbartext+" @";
							}
						
						}
					}.start();
			 }
			 else
			 {
				 cf.ShowToast("Internet connection is not available");
			 }
	}
	protected boolean Update_Inspection_Status() throws SocketException,IOException, FileNotFoundException, NetworkErrorException,TimeoutException, SocketTimeoutException {
		SoapObject request = new SoapObject(wb.NAMESPACE, "GetInspectionStatus");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;

		request.addProperty("InspectorID", db.Insp_id);
		request.addProperty("SRID", cf.Homeid);
		envelope.setOutputSoapObject(request);System.out.println("request="+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
		try {
			androidHttpTransport.call(wb.NAMESPACE + "GetInspectionStatus",
					envelope);
		} catch (XmlPullParserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String result = envelope.getResponse().toString();
		System.out.println("result=Update_Inspection_Status" + result);
		return wb.check_result(result);
	
}


	 private boolean updatepolicy() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException
		{
			System.out.println("inside policyhodlder");
			SoapObject request = new SoapObject(wb.NAMESPACE,"ExportData");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			Bitmap bitmap,bitmap1;
			
			
			Cursor c2= db.SelectTablefunction(db.policyholder, " where PH_SRID='" + cf.Homeid + "' and PH_InspectorId='"+ db.Insp_id + "'");
			if(c2.getCount()==1)
			{
				c2.moveToFirst();
				request.addProperty("InspectorID",db.Insp_id);
				request.addProperty("SRID",cf.Homeid);
				request.addProperty("OwnerFirstName",db.decode(c2.getString(c2.getColumnIndex("PH_FirstName"))));
				request.addProperty("OwnerLastName",db.decode(c2.getString(c2.getColumnIndex("PH_LastName"))));
				request.addProperty("Address",db.decode(c2.getString(c2.getColumnIndex("PH_Address1"))));
				request.addProperty("Address2",db.decode(c2.getString(c2.getColumnIndex("PH_Address2"))));
				request.addProperty("Policynumber",db.decode(c2.getString(c2.getColumnIndex("PH_Policyno"))));
				request.addProperty("City",db.decode(c2.getString(c2.getColumnIndex("PH_City"))));
				request.addProperty("Zip",c2.getString(c2.getColumnIndex("PH_Zip")));
				getStateID(db.decode(c2.getString(c2.getColumnIndex("PH_State"))));
				System.out.println("statecountyid="+stateid);
				request.addProperty("State",stateid);
				request.addProperty("County",db.decode(c2.getString(c2.getColumnIndex("PH_County"))));
				request.addProperty("YearBuilt",c2.getString(c2.getColumnIndex("YearBuilt")));
				request.addProperty("InspectionDate",db.decode(c2.getString(c2.getColumnIndex("InspectionDate"))));
				request.addProperty("ContactPerson",db.decode(c2.getString(c2.getColumnIndex("PH_ContactPerson"))));			
				request.addProperty("HomePhone",db.decode(c2.getString(c2.getColumnIndex("PH_HomePhone"))));
				request.addProperty("WorkPhone",db.decode(c2.getString(c2.getColumnIndex("PH_WorkPhone"))));
				request.addProperty("CellPhone",db.decode(c2.getString(c2.getColumnIndex("PH_CellPhone"))));
				request.addProperty("Nstories",c2.getString(c2.getColumnIndex("PH_NOOFSTORIES")));
				request.addProperty("Email",db.decode(c2.getString(c2.getColumnIndex("PH_Email"))));				
				request.addProperty("SchedulingComments",db.decode(c2.getString(c2.getColumnIndex("Schedule_Comments"))));
				request.addProperty("Description1",db.decode(c2.getString(c2.getColumnIndex("fld_homewonercaption"))));	
				request.addProperty("Description2",db.decode(c2.getString(c2.getColumnIndex("fld_paperworkcaption"))));	
				request.addProperty("Inspectionfee", db.decode(c2.getString(c2.getColumnIndex("Fee"))));
				
				if(db.decode(c2.getString(c2.getColumnIndex("Discount"))).equals(""))
				{
					request.addProperty("Discount", "0");
				}
				else
				{
					request.addProperty("Discount", db.decode(c2.getString(c2.getColumnIndex("Discount"))));	
				}
				
				String homesignpath = db.decode(c2.getString(c2.getColumnIndex("fld_homeownersign")));System.out.println("homesi"+homesignpath);
				if(!homesignpath.equals(""))
				{
					String homesignname = homesignpath.substring(homesignpath.lastIndexOf("/") + 1);System.out.println("homesignname"+homesignname);
					File f = new File(homesignpath);System.out.println("ff="+f);
					if (f.exists()) {
						try {
							System.out.println("ff="+f);
							System.out.println("homesignname");
							bitmap = cf.ShrinkBitmap(homesignpath, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																	// 800, 800);
							System.out.println("comes correc1"+bitmap);
			
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							byte[] raw = out.toByteArray();
							request.addProperty("Signatureimg1", raw);
						} catch (Exception e) {
							// TODO: handle exception
			System.out.println("testca");
							Error_tracker += "You have problem in exporting Home Signature Image";
						}
					}
					else
					{
						request.addProperty("Signatureimg1", "");
					}
				}
				else
				{
					request.addProperty("Signatureimg1", "");
				}
				
				
				String papersignpath = db.decode(c2.getString(c2.getColumnIndex("fld_paperworksign")));System.out.println("papersignpath"+papersignpath);
				if(!papersignpath.equals(""))
				{
					String papersignname = papersignpath.substring(homesignpath.lastIndexOf("/") + 1);System.out.println("papersignname"+papersignname);
					File f1 = new File(papersignpath);
					if (f1.exists()) {
						try {
							System.out.println("ssfd");
							bitmap1 = cf.ShrinkBitmap(papersignpath, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																	// 800, 800);
							System.out.println("comes correc1");
			
							ByteArrayOutputStream out1 = new ByteArrayOutputStream();
							bitmap1.compress(CompressFormat.PNG, 100, out1);
							byte[] raw1 = out1.toByteArray();
							request.addProperty("Signatureimg2", raw1);
						} catch (Exception e) {
							// TODO: handle exception
			
							Error_tracker += "You have problem in exporting Paper Signature Image";
						}
					}
					else
					{
						request.addProperty("Signatureimg2", "");
					}
				}
				else
				{
					request.addProperty("Signatureimg2", "");
				}

				
				Cursor C_policymail = db.SelectTablefunction(db.MailingPolicyHolder, " where ML_PH_SRID='"+ cf.Homeid+ "'");			
				if (C_policymail.getCount() > 0) {
					C_policymail.moveToFirst();
					getStateID(db.decode(C_policymail.getString(C_policymail.getColumnIndex("ML_PH_State"))));
					
					request.addProperty("Mailingaddress1", db.decode(C_policymail.getString(C_policymail.getColumnIndex("ML_PH_Address1"))));
					request.addProperty("Mailingaddress2", db.decode(C_policymail.getString(C_policymail.getColumnIndex("ML_PH_Address2"))));
					request.addProperty("Mailingcity", db.decode(C_policymail.getString(C_policymail.getColumnIndex("ML_PH_City"))));
					request.addProperty("Mailingzip", db.decode(C_policymail.getString(C_policymail.getColumnIndex("ML_PH_Zip"))));
					request.addProperty("Mailingstate", stateid);
					request.addProperty("Mailingcounty", db.decode(C_policymail.getString(C_policymail.getColumnIndex("ML_PH_County"))));
				} else {
					request.addProperty("Mailingaddress1", "");
					request.addProperty("Mailingaddress2", "");
					request.addProperty("Mailingcity", "");
					request.addProperty("Mailingzip", "");
					request.addProperty("Mailingstate", "");
					request.addProperty("Mailingcounty", "");
				}

						/** set the value get from the policy holder mailing  table Ends **/
				envelope.setOutputSoapObject(request);
				MarshalBase64 marshal = new MarshalBase64();
				marshal.register(envelope);
				
				System.out.println("requestpolicy="+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);System.out.println("came still");
				try {
					androidHttpTransport.call(wb.NAMESPACE+"ExportData",envelope);System.out.println("androidHttpTransport still");
				} catch (XmlPullParserException e) {
					System.out.println("test"+e.getMessage());
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result =  envelope.getResponse().toString();System.out.println("result still");
				System.out.println("result="+result);
				return wb.check_result(result);
			}
			
			return true;
		}
		
		private void getStateID(String statename){
			// TODO Auto-generated method stub
			try
			{
				DataBaseHelper1 dbh1 = new DataBaseHelper1(ExportData.this);
				dbh1.createDataBase();
				SQLiteDatabase newDB = dbh1.openDataBase();
				dbh1.getReadableDatabase();
				System.out.println("State_Table"+db.encode(statename)+" =="+db.decode(statename));
				Cursor cur = newDB.rawQuery("select * from State_Table where statename='"+db.encode(statename)+"'",null);System.out.println("select * from State_Table where statename='"+db.encode(statename)+"'"+cur.getCount());
				cur.moveToFirst();
				stateid  = cur.getInt(0);System.out.println("stat"+stateid);
				
			}
			catch (Exception e) {
				// TODO: handle exception
				System.out.println("excep="+e.getMessage());
			}
			
		}
		private boolean uploadquestions() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException
		{
				if(Questionsthrows())
				{
					if(updateroofcover())
					{
						if(quescomm())
						{
							
						}
					}
				}
				return true;
		}
		
		private boolean Questionsthrows() throws NetworkErrorException,IOException, XmlPullParserException,SocketTimeoutException 
		{
			
			SoapObject request = new SoapObject(wb.NAMESPACE,"ExportWSI1802Questions");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;

			Cursor c2= db.SelectTablefunction(db.Questions, " where SRID='" + cf.Homeid + "'");
			if(c2.getCount()==1)
			{
				c2.moveToFirst();
				request.addProperty("InspectorID",db.Insp_id);
				request.addProperty("SRID",cf.Homeid);
				request.addProperty("BuildingCodeYearBuilt",c2.getString(2));
				request.addProperty("BuildingCodePerApplnDate",c2.getString(4));
				request.addProperty("BuildingCodeValue",c2.getString(3));
				
				Cursor c1 = db.SelectTablefunction(db.QuestionsRoofCover, " where SRID='" + cf.Homeid + "'");
				if(c1.getCount()==1)
				{
					c1.moveToFirst();
					request.addProperty("RoofCoverValue",c1.getInt(3));
				}
				
				request.addProperty("RoofDeckValue", c2.getString(12));
				request.addProperty("RoofDeckOtherText",db.decode(c2.getString(13)));
				request.addProperty("RooftoWallValue",c2.getString(14));			
				request.addProperty("RooftoWallToeNailMinValue", 0);
				
				request.addProperty("RooftoWallClipsMinValue",c2.getString(16));
				request.addProperty("RooftoWallSingleMinValue",c2.getString(17));
				request.addProperty("RooftoWallDoubleMinValue",c2.getString(18));
				request.addProperty("RooftoWallSubValue",c2.getString(15));
				request.addProperty("RooftoWallOtherText",db.decode(c2.getString(19)));
				request.addProperty("RoofGeoValue",c2.getString(20));
				request.addProperty("RoofGeoOtherText",db.decode(c2.getString(23)));
				
				
				if(c2.getString(21).equals(""))
				{
					request.addProperty("RoofGeoLength",0);
				}
				else
				{
					request.addProperty("RoofGeoLength",c2.getString(21));
				}
				
				if(c2.getString(22).equals(""))
				{
					request.addProperty("RoofGeoTotalArea",0);
				}
				else
				{
					request.addProperty("RoofGeoTotalArea",c2.getString(22));
				}
				request.addProperty("SWRValue", c2.getString(24));
				request.addProperty("OpenProtectType",c2.getString(25));
				
				request.addProperty("OpenProtectValue",c2.getString(26));
				request.addProperty("OpenProtectSubValue",c2.getString(27));
				request.addProperty("OpenProtectLevelChart",c2.getString(28));
				
				request.addProperty("GOWindEntryDoorsValue",c2.getString(29));
				request.addProperty("GOGarageDoorsValue",c2.getString(30));
				request.addProperty("GOSkylightsValue",c2.getString(31));
				request.addProperty("NGOGlassBlockValue",c2.getString(32));
				request.addProperty("NGOGarageDoorsValue",c2.getString(34));
				request.addProperty("NGOEntryDoorsValue",c2.getString(33));
				request.addProperty("WoodFrameValue",c2.getString(35));
				request.addProperty("WoodFramePer",c2.getInt(36));
				request.addProperty("ReinMasonryValue",c2.getString(37));
				request.addProperty("ReinMasonryPer",c2.getInt(38));
				request.addProperty("UnReinMasonryValue",c2.getString(39));
				request.addProperty("UnReinMasonryPer",c2.getInt(40));
				request.addProperty("PouredConcrete",c2.getString(41));
				request.addProperty("PouredConcretePer",c2.getInt(42));
				request.addProperty("OtherWallTitle",db.decode(c2.getString(43)));
				request.addProperty("OtherWal", c2.getString(44));
				request.addProperty("OtherWallPer",c2.getInt(45));
				request.addProperty("GeneralHazardsInclude",c2.getString(51));

				
						/** set the value get from the policy holder mailing  table Ends **/
				envelope.setOutputSoapObject(request);
				System.out.println("request="+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				try {
					androidHttpTransport.call(wb.NAMESPACE+"ExportWSI1802Questions",envelope);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result =  envelope.getResponse().toString();
				System.out.println("resultquestion="+result);
				return wb.check_result(result);
			}
			
			return true;
			
		}
		private boolean updateroofcover() throws NetworkErrorException,IOException, XmlPullParserException,SocketTimeoutException 
		{
			SoapObject request = new SoapObject(wb.NAMESPACE,"ExportWSI1802RoofCoverType");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;

			Cursor c= db.SelectTablefunction(db.QuestionsRoofCover, " where SRID='" + cf.Homeid + "'");System.out.println("teer"+c.getCount());
			if(c.getCount()==1)
			{
				c.moveToFirst();
				request.addProperty("InspectorID",db.Insp_id);
				request.addProperty("SRID",cf.Homeid);
				request.addProperty("RoofCoverType", db.decode(c.getString(2)));
				request.addProperty("PermitApplnDate1",db.decode(c.getString(4)));
				request.addProperty("PermitApplnDate2",db.decode(c.getString(5)));
				request.addProperty("PermitApplnDate3",db.decode(c.getString(6)));
				request.addProperty("PermitApplnDate4",db.decode(c.getString(7)));
				request.addProperty("PermitApplnDate5",db.decode(c.getString(8)));
				request.addProperty("PermitApplnDate6",db.decode(c.getString(9)));
				request.addProperty("RoofCoverTypeOther",db.decode(c.getString(10)));
				request.addProperty("ProdApproval1",db.decode(c.getString(11)));
				request.addProperty("ProdApproval2",db.decode(c.getString(12)));
				request.addProperty("ProdApproval3",db.decode(c.getString(13)));
				request.addProperty("ProdApproval4",db.decode(c.getString(14)));
				request.addProperty("ProdApproval5",db.decode(c.getString(15)));
				request.addProperty("ProdApproval6",db.decode(c.getString(16)));		
				request.addProperty("InstallYear1", c.getString(17));
				request.addProperty("InstallYear2", c.getString(18));
				request.addProperty("InstallYear3", c.getString(19));
				request.addProperty("InstallYear4", c.getString(20));
				request.addProperty("InstallYear5", c.getString(21));
				request.addProperty("InstallYear6", c.getString(22));
				request.addProperty("NoInfo1", c.getString(23));
				request.addProperty("NoInfo2", c.getString(24));
				request.addProperty("NoInfo3", c.getString(25));
				request.addProperty("NoInfo4", c.getString(26));
				request.addProperty("NoInfo5", c.getString(27));
				request.addProperty("NoInfo6", c.getString(28));
				request.addProperty("RoofPreDominant",c.getString(29));
	System.out.println("reqroofcover="+request);
				
						/** set the value get from the policy holder mailing  table Ends **/
				envelope.setOutputSoapObject(request);
				System.out.println("request="+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				try {
					androidHttpTransport.call(wb.NAMESPACE+"ExportWSI1802RoofCoverType",envelope);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result =  envelope.getResponse().toString();
				System.out.println("result="+result);
				return wb.check_result(result);
			}
			
			return true;
		}
		protected boolean quescomm() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
			
			SoapObject request = new SoapObject(wb.NAMESPACE,"ExportWSIInsComments");
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;

			Cursor c= db.SelectTablefunction(db.QuestionsComments, " where SRID='" + cf.Homeid + "'");
			if(c.getCount()==1)
			{
				c.moveToFirst();
				request.addProperty("InspectorID",db.Insp_id);
				request.addProperty("Srid",cf.Homeid);
				request.addProperty("BuildingCodeComment", db.decode(c.getString(3)));
				request.addProperty("RoofCoverComment",db.decode(c.getString(4)));
				request.addProperty("RoofDeckComment",db.decode(c.getString(5)));
				request.addProperty("RoofWallComment",db.decode(c.getString(6)));
				request.addProperty("RoofGeometryComment",db.decode(c.getString(7)));
				request.addProperty("SecondaryWaterComment",db.decode(c.getString(8)));
				request.addProperty("OpeningProtectionComment",db.decode(c.getString(9)));
				request.addProperty("WallConstructionComment",db.decode(c.getString(10)));
				request.addProperty("InsOverAllComments",db.decode(c.getString(11)));
				request.addProperty("CreatedOn",db.decode(c.getString(12)));
				
						/** set the value get from the policy holder mailing  table Ends **/
				envelope.setOutputSoapObject(request);
				System.out.println("request="+request);
				HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
				try {
					androidHttpTransport.call(wb.NAMESPACE+"ExportWSIInsComments",envelope);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result =  envelope.getResponse().toString();
				System.out.println("result="+result);
				return wb.check_result(result);
			}
			
			return true;
			
		}
		private boolean Expot_photos() throws SocketException,NetworkErrorException, TimeoutException, IOException {
			System.gc();
			
			
			 Bitmap bitmap=null;
			String sql = " Select * from " + db.Photos + " Where IM_SRID='"	+ cf.Homeid + "' ";System.out.println("sql="+sql);
			Cursor C_photos = db.wind_db.rawQuery(sql, null);System.out.println("C_photos="+C_photos.getCount());
			C_photos.moveToFirst();
			String result_val = "";
			if (C_photos.getCount() >= 1) {
				double sub_count=(double)(20.00/C_photos.getCount());
				System.out.println("the value"+sub_count);
				C_photos.moveToFirst();
				String  path = "", name = "";
				boolean b = false;
				for (int i = 0; i < C_photos.getCount(); i++, C_photos.moveToNext()) {
				
					System.out.println("test="+C_photos.getCount());
					b = false;
					try {
						bitmap.recycle();// to release the memory
					} catch (Exception e) {
						// TODO: handle exception
					}
			
					
					path = db.decode(C_photos.getString(C_photos.getColumnIndex("IM_path")));
					name = path.substring(path.lastIndexOf("/") + 1);
					File f = new File(path);
					if (f.exists()) {
						SoapObject request = wb.export_header("ExportWSI1802ElevationPicture");
						try {
							
							bitmap = cf.ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																	// 800, 800);
							System.out.println("comes correc1");
			
							System.out.println("out =");
							ByteArrayOutputStream out = new ByteArrayOutputStream();
							bitmap.compress(CompressFormat.PNG, 100, out);
							byte[] raw = out.toByteArray();System.out.println("raaw =");
							request.addProperty("imgByte", raw);System.out.println("raaw="+raw);
						} catch (Exception e) {
							// TODO: handle exception
			System.out.println("catchinside="+e.getMessage());
							Error_tracker += "You have problem in exporting Image in the Elevation= "+ cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]	+ " ,image order = "+ C_photos.getString(C_photos.getColumnIndex("IM_ImageOrder"))+ " Path=" + path + " @";
						}
			
						System.out.println("inside thsis"+cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]);
						Exportbartext="Photos in "+cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]+" Image "+C_photos.getString(C_photos.getColumnIndex("IM_ImageOrder"));
						request.addProperty("InspectorID", db.Insp_id);
						request.addProperty("SRID", cf.Homeid);					
						if(C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))==6)
						{
							request.addProperty("Elevation",8);
						}
						else
						{
							request.addProperty("Elevation",C_photos.getInt(C_photos.getColumnIndex("IM_Elevation")));
						}
						
						request.addProperty("ImageNameWithExtension", name);
						request.addProperty("Description", db.decode(C_photos.getString(C_photos.getColumnIndex("IM_Description"))));
						request.addProperty("ImageOrder", C_photos.getString(C_photos.getColumnIndex("IM_ImageOrder")));
						request.addProperty("CreatedOn", C_photos.getString(C_photos.getColumnIndex("IM_CreatedOn")));
						
						System.out.println("createtime="+request);
						//request.addProperty("ROWID", C_photos.getString(C_photos.getColumnIndex("IM_ID")));
						wb.envelope.setOutputSoapObject(request);
						MarshalBase64 marshal = new MarshalBase64();
						marshal.register(wb.envelope);
						wb.envelope.setOutputSoapObject(request);
						System.out.println("Photos=" + request);
						b = true;
			
						if (b) {
							HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
							try {
								androidHttpTransport.call(wb.NAMESPACE+ "ExportWSI1802ElevationPicture", wb.envelope);
							} catch (XmlPullParserException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							String val= wb.envelope.getResponse().toString();System.out.println("photos response"+val);
							result_val +=val+ "~";System.out.println("resul="+result_val);
							if(val.equals("false"))
							{
								Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
							}
							System.out.println("the photos reult =" + result_val);
						} else {
							result_val += "false~";
						}
						// EX_CHKGCH[3]=
						// cf.SoapResponse("ExportGCHSummaryHazardsConcerns",cf.request);
			
					} else {
						Error_tracker += "Please check the following Image is available in"	+ ", Elevation ="+ cf.elev[C_photos.getInt(C_photos.getColumnIndex("IM_Elevation"))]
								+ ", image order = "
								+ C_photos.getString(C_photos
										.getColumnIndex("IM_ImageOrder"))
								+ " Path=" + path + " @";
						// photos_missingfiles+=db.decode(C_photos.getString(C_photos.getColumnIndex("ARR_IM_Description")))+"&#94;";
						result_val += "false~";
						// System.out.println("File missing");
					}
			
					
					// }
					total+=sub_count;
				}
				
				
				if (result_val.contains("false"))
					return false;
				else
					return true;
			} else {
				return false;
			}

		}
		private boolean Expot_feedback() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException,TimeoutException
		{
			int ishosat = 0,csc=0;
			SoapObject request;
			Bitmap bitmap=null;
			String result_val="";
			System.out.println("inside feedack");
			if(Upload_Feedbackinfo())
			{
				System.out.println("Upload_Feedbackinfo");
				/** Start export feedback document ***/
				String sql1 = " Select * from " + db.FeedBackDocument
						+ " Where FD_D_SRID='" + cf.Homeid
						+ "' ";
		
				Cursor C_feed_d = db.wind_db.rawQuery(sql1, null);
				if(C_feed_d.getCount()>0){
				
				double subcount=(double) 20.00/C_feed_d.getCount();
				C_feed_d.moveToFirst();
				String result_val_d = "";
				String[] array_doc = new String[12];
				array_doc[0] = "--Select--";
				array_doc[1] = "Acknowledgement Form";
				array_doc[2] = "CSE Form";
				array_doc[3] = "OIR 1802 Form";
				array_doc[4] = "Paper Signup Sheet";
				array_doc[5] = "Other Information";
				array_doc[6] = "Roof Permit ";
				array_doc[7] = "Sketch";
				array_doc[8] = "Building Permit";
				array_doc[9] = "Property Appraisal Information";
				array_doc[10] = "Field Inspection Report";
				array_doc[11] = "Field Notes";
				boolean com = false;
				int feed_inc = 50 / C_feed_d.getCount();
				
		
				for (int i = 0; i < C_feed_d.getCount(); i++, C_feed_d
						.moveToNext()) {
				
					com = false;
					try {
						bitmap.recycle();// to release the memory
					} catch (Exception e) {
						// TODO: handle exception
					}
					String tit = db.decode(C_feed_d.getString(C_feed_d
							.getColumnIndex("FD_D_doctit")));
					Boolean titb = false;
					for (int n = 0; n < array_doc.length; n++) {
						if (array_doc[n].equals(tit)) {
							titb = true;
						}
					}
					String path = db.decode(C_feed_d.getString(C_feed_d
							.getColumnIndex("FD_D_path")));System.out.println("path="+path);
					String name = path.substring(path.lastIndexOf("/") + 1);System.out.println("name="+name);
					Boolean b = (C_feed_d.getString(C_feed_d
							.getColumnIndex("FD_D_type"))
							.equals("1")) ? true : false;
					
					Exportbartext="Feed back Document, Title("+tit+"), Document "+(i+1);
						request =wb.export_header("ExportWSI1802FeedbackDocument");
					
					request.addProperty("InspectorID", db.Insp_id);						
					request.addProperty("SRID", cf.Homeid);
				
					//if (titb) {
						request.addProperty("DocumentTitle", tit);
						
					//}
					System.out.println("wat is titl="+tit);
					
					request.addProperty("ImageNameWithExtension", name);
					//request.addProperty("FD_D_type", b);
					request.addProperty("ImageOrder",C_feed_d.getString(C_feed_d.getColumnIndex("FD_D_Id")));
					request.addProperty("IsOfficeUse",b);
					request.addProperty("CreatedDate",  db.decode(C_feed_d.getString(C_feed_d.getColumnIndex("FD_D_CreatedOn"))));
					//request.addProperty("AndroidID", i+1);
					
					
				//	System.out.println("wat is name="+name);
					if (name.endsWith(".pdf")) {
		
						File dir = Environment.getExternalStorageDirectory();
						if (path.startsWith("file:///")) {
							path = path.substring(11);
						}
		
						File assist = new File(path);
						if (assist.exists()) {
							try {
								String mypath = path;
								String temppath[] = mypath.split("/");
								int ss = temppath[temppath.length - 1]
										.lastIndexOf(".");
								String tests = temppath[temppath.length - 1]
										.substring(ss);
								String namedocument;
								InputStream fis = new FileInputStream(assist);
								long length = assist.length();
								byte[] bytes = new byte[(int) length];
								int offset = 0;
								int numRead = 0;
								while (offset < bytes.length
										&& (numRead = fis.read(bytes, offset,
												bytes.length - offset)) >= 0) {
									offset += numRead;
								}
								Object strBase64 = Base64.encode(bytes);
								request.addProperty("imgByte", strBase64);
								//System.out.println("propery feed "+request);
								
								com = true;
							} catch (Exception e) {
								// TODO: handle exception.
								Error_tracker += "You have problem uploadin Feed back information @";
							}System.out.println("ocom"+com);
							if (com) {
								
								String val= wb.export_footer(wb.envelope, request, "ExportFeedbackDocument");
								//System.out.println("Fvalt"+ request);
								result_val +=val+ "~";
								System.out.println("Feed doc rsult"+ result_val);
								if(val.equals("false"))
								{
									Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
								}
							} else {
								result_val += "false~";
							}
		
						} else {
							Error_tracker += "Please check the following Image/Document is available "
									+  " Path=" + path + " @";
							 feed_missingfiles+=tit+"&#94;"; 
							result_val += "false~";
							 System.out.println("file missing"); 
						}
		
					} else {
						bitmap = null;
						MarshalBase64 marshal = new MarshalBase64();
						File f = new File(path);System.out.println("its jpg="+path);
						if (f.exists()) {
							System.out.println("file exists");
							/***
							 * Check the file size compress the based on the
							 * size
							 **/
		
							/***
							 * Check the file size compress the based on the
							 * size ends
							 **/
							try {
								bitmap = cf.ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																		// 800,
																		// 800);
								ByteArrayOutputStream out = null;
								out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								byte[] raw = out.toByteArray();System.out.println("file rwaw");
		
								request.addProperty("imgByte", raw);System.out.println("req="+request);
								marshal.register(wb.envelope);
								com = true;
							} catch (Exception e) {
								// TODO: handle exception
								Error_tracker += "You have problem uploadin Feed back information @";
							}
							System.out.println("com="+com);
							if (com) {
								String val= wb.export_footer(wb.envelope, request, "ExportWSI1802FeedbackDocument");
								result_val +=val+ "~";
								if(val.equals("false"))
								{
									Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
								}
							} else {
								result_val += "false";
							}
							System.out.println("Feed info rsult" + result_val);
						} else {
							// feedback_fileexits=true;
							Error_tracker += "Please check the following Image/Document is available Path=" + path + " @";
							result_val += "false~";
		
						}
		
					}
		
					// System.out.println("feed info request val="+request);
		
				total+=subcount;
				}
		
				/** export feedback document ends ***/
				// }
				System.out.println("Feedback doc result_val"+result_val);
				if (result_val.contains("false"))
					return false;
				else if (result_val.equals(""))
					return false;
				else
					return true;
			}	
			else
			{
				return true;
			}
			}
			return false;
		}
		private boolean Upload_Feedbackinfo() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException,TimeoutException 
		{
			// TODO Auto-generated method stub
			int insupaperwork=0,manuinfo=0,csc=0;
			SoapObject request;
			String sql = " Select * from " + db.FeedBackInfo+ " Where FD_SRID='" + cf.Homeid+ "' ";
			Cursor C_feed_i = db.wind_db.rawQuery(sql, null);
			C_feed_i.moveToFirst();
			String result_val = "";
			
			if (C_feed_i.getCount() >= 1) {
				
				C_feed_i.moveToFirst();
				for (int i = 0; i < C_feed_i.getCount(); i++, C_feed_i.moveToNext()) {
			
					
					request = wb.export_header("ExportWSI1802FeedbackInformation");
					request.addProperty("InspectorID", db.Insp_id);
					request.addProperty("SRID", cf.Homeid);
					System.out.println("HO"+db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_insupaperwork")))+"FD_manuinfo"+C_feed_i.getString(C_feed_i.getColumnIndex("FD_manuinfo")));
					
					if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_insupaperwork"))).equals("Yes"))
					{
						insupaperwork=1;
					}
					else if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_insupaperwork"))).equals("No"))
					{
						insupaperwork=2;
					}
					else if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_insupaperwork"))).equals("Not Applicable"))
					{
						insupaperwork=3;
					}
					
					
					if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_manuinfo"))).equals("Yes"))
					{
						manuinfo=1;
					}
					else if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_manuinfo"))).equals("No"))
					{
						manuinfo=2;
					}
					else if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_manuinfo"))).equals("Not Applicable"))
					{
						manuinfo=3;
					}
					
					System.out.println("FD_CSC"+db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CSC"))));
					if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CSC"))).equals("Yes"))
					{
						csc=1;
					}
					else if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CSC"))).equals("No"))
					{
						csc=2;
					}
					else if(db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CSC"))).equals("Not Applicable"))
					{
						csc=3;
					}
					
					request.addProperty("IsCusServiceCompltd",csc);
					request.addProperty("IsInspectionPaperAvbl", insupaperwork);				
					request.addProperty("IsManufacturerInfo", manuinfo);
					request.addProperty("PresentatInspection",db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_whowas"))));
					request.addProperty("OtherPresent", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_whowas_other"))));
					request.addProperty("FeedbackComments", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_comments"))));
					request.addProperty("CreatedOn", db.decode(C_feed_i.getString(C_feed_i.getColumnIndex("FD_CreatedOn"))));
					System.out.println("feed info request val=" + request);
					String result= wb.export_footer(wb.envelope, request, "ExportWSI1802FeedbackInformation")+ "~";
					return wb.check_result(result);	
				}
					
				}
			System.out.println("complete feedbac");
			return true;
		}
		protected boolean Export_GCHData() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException,TimeoutException 
		{
			db.CreateTable(15);
			String sql = " Select * from " + db.GeneralHazInfo+ " Where Srid='" + cf.Homeid+ "' ";
			Cursor c = db.wind_db.rawQuery(sql, null);
			c.moveToFirst();
			String result_val = "";
			
			if (c.getCount() >= 1) {
				
				c.moveToFirst();
				
					SoapObject request = new SoapObject(wb.NAMESPACE,"ExportWSI1802GenHzdData");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
				
					request.addProperty("InspectorID",db.Insp_id);
					request.addProperty("Srid", cf.Homeid);
					request.addProperty("InsuredIs", c.getString(2));
					request.addProperty("OtherInsured",  db.decode(c.getString(c.getColumnIndex("InsuredOther"))));
					request.addProperty("OccupancyType", c.getString(c.getColumnIndex("OccupancyType")));
					request.addProperty("OtherOccupancy",  db.decode(c.getString(c.getColumnIndex("OcccupOther"))));
					request.addProperty("ObservationType",c.getString(c.getColumnIndex("ObservationType")));
					request.addProperty("OtherObservation", db.decode(c.getString(c.getColumnIndex("ObservOther"))));
					request.addProperty("Occupied", c.getString(c.getColumnIndex("OccupiedFlag")));
					request.addProperty("OccupiedPercent",c.getString(c.getColumnIndex("OccupiedPercent")));
					request.addProperty("Vacant", c.getString(c.getColumnIndex("Vacant")));
					request.addProperty("VacantPercent",c.getString(c.getColumnIndex("VacantPercent")));			
					request.addProperty("NotDetermined", c.getString(c.getColumnIndex("NotDetermined")));
					request.addProperty("PermitConfirmed",c.getString(c.getColumnIndex("PermitConfirmed")));
					if(c.getString(14).equals(""))
					{
						request.addProperty("BuildingSize", 0);
					}
					else
					{
						request.addProperty("BuildingSize", c.getString(14));
					}
					
					request.addProperty("BalconyPresent",c.getString(15));
					request.addProperty("AdditionalStructure",c.getString(16));
					request.addProperty("OtherLocation",db.decode(c.getString(17)));		
					request.addProperty("Location1", c.getString(18));
					request.addProperty("Location2", c.getString(19));
					request.addProperty("Location3", c.getString(20));
					request.addProperty("Location4", c.getString(21));
					request.addProperty("Observation1", c.getString(22));
					request.addProperty("Observation2", c.getString(23));
					request.addProperty("Observation3", c.getString(24));
					request.addProperty("Observation4", c.getString(25));
					request.addProperty("Observation5", c.getString(26));
					request.addProperty("PerimeterPoolFence",c.getString(27));
					request.addProperty("PoolFenceDisrepair",c.getString(28));
					request.addProperty("SelfLatch", c.getString(29));
					request.addProperty("ProfesInstall", c.getString(30));
					request.addProperty("PoolPresent", c.getString(31));
					request.addProperty("HotTubPresnt", c.getString(32));
					request.addProperty("EmptyGroundPool", c.getString(33));
					request.addProperty("PoolSlide", c.getString(34));
					request.addProperty("DivingBoard", c.getString(35));
					request.addProperty("PerimeterPoolEncl",c.getString(36));
					request.addProperty("PoolEnclosure", c.getString(37));
					request.addProperty("Vicious", c.getString(38));
					request.addProperty("LiveStock", c.getString(39));
					request.addProperty("OverHanging", c.getString(40));
					request.addProperty("Trampoline", c.getString(41));
					request.addProperty("SkateBoard", c.getString(42));
					request.addProperty("bicycle", c.getString(43));
					request.addProperty("TripHazardDesc",db.decode(c.getString(44)));
					request.addProperty("TripHazardNoted", c.getString(45));
					request.addProperty("UnsafeStairway", c.getString(46));
					request.addProperty("PorchAndDeck", c.getString(47));
					request.addProperty("NonStdConstruction",c.getString(48));
					request.addProperty("OutDoorAppliances",c.getString(49));
					request.addProperty("OpenFoundation", c.getString(50));
					request.addProperty("WoodShingled", c.getString(51));
					request.addProperty("ExcessDebris", c.getString(52));
					request.addProperty("BusinessPremises", c.getString(53));
					request.addProperty("GeneralDisrepair", c.getString(54));
					request.addProperty("PropertyDamage", c.getString(55));
					request.addProperty("StructurePartial", c.getString(56));
					request.addProperty("InOperative", c.getString(57));
					request.addProperty("RecentDrywall", c.getString(58));
					request.addProperty("ChineseDrywall", c.getString(59));
					request.addProperty("ConfirmDrywall", c.getString(60));
					request.addProperty("NonSecurity", c.getString(61));
					request.addProperty("NonSmoke", c.getString(62));
					request.addProperty("Comments",db.decode(c.getString(63)));
					System.out.println("feed info request val=" + request);
					
					
					envelope.setOutputSoapObject(request);
					System.out.println("request="+request);
					HttpTransportSE androidHttpTransport = new HttpTransportSE(wb.URL);
					try {
						androidHttpTransport.call(wb.NAMESPACE+"ExportWSI1802GenHzdData",envelope);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String result =  envelope.getResponse().toString();
					System.out.println("resultgch="+result);
					return wb.check_result(result);
								
			}	
			return true;
		}
		protected boolean Export_GCH() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException,TimeoutException 
		{
			Bitmap bitmap=null;
			String result_val = "";
			String  path = "", name = "";
			SoapObject request;
			System.out.println("Camer");
			if(Export_GCHData()) 
			{
				System.out.println("inside tGHCH dat");
					db.CreateTable(14);
					/** Start export feedback document ***/
					String sql1 = " Select * from " + db.GeneralHazDoc
							+ " Where GCH_SRID='" + cf.Homeid
							+ "' ";
			
					Cursor C_Gen = db.wind_db.rawQuery(sql1, null);
					if(C_Gen.getCount()>=1)
					{
					double subcount=(double) 20.00/C_Gen.getCount();
					C_Gen.moveToFirst();
					String result_val_d = "";


					boolean com = false;
					int gen_inc = 50 / C_Gen.getCount();
					
			
					for (int i = 0; i < C_Gen.getCount(); i++, C_Gen.moveToNext()) {
					
						com = false;
						try {
							bitmap.recycle();// to release the memory
						} catch (Exception e) {
							// TODO: handle exception
						}
									
						
						path = db.decode(C_Gen.getString(C_Gen.getColumnIndex("GCH_path")));
						name = path.substring(path.lastIndexOf("/") + 1);

						
						if(C_Gen.getString(C_Gen.getColumnIndex("GCH_Elevation")).equals("1"))
						{
							elevationname = "Exterior and Grounds";
						}
						else if(C_Gen.getString(C_Gen.getColumnIndex("GCH_Elevation")).equals("2"))
						{
							elevationname="Interior";
						}
						else if(C_Gen.getString(C_Gen.getColumnIndex("GCH_Elevation")).equals("3"))
						{
							elevationname="Additional";
						}
						
						
						Exportbartext="General Hazard Images in "+ elevationname +" Image "+C_Gen.getString(C_Gen.getColumnIndex("GCH_ImageOrder"));
						
						request =wb.export_header("ExportWSI1802GenHzdImages");
						
						request.addProperty("InspectorID", db.Insp_id);						
						request.addProperty("SRID", cf.Homeid);
						request.addProperty("Elevation", C_Gen.getString(C_Gen.getColumnIndex("GCH_Elevation")));
						request.addProperty("ImageNameWithExtension", name);
						request.addProperty("ImageOrder",C_Gen.getString(C_Gen.getColumnIndex("GCH_ImageOrder")));
						request.addProperty("Description",db.decode(C_Gen.getString(C_Gen.getColumnIndex("GCH_Description"))));
						request.addProperty("CreatedDate",  db.decode(C_Gen.getString(C_Gen.getColumnIndex("GCH_CreatedOn"))));
						System.out.println("propery feed "+request);
						
						if (name.endsWith(".pdf")) {
			
							File dir = Environment.getExternalStorageDirectory();
							if (path.startsWith("file:///")) {
								path = path.substring(11);
							}
			
							File assist = new File(path);
							if (assist.exists()) {
								try {
									String mypath = path;
									String temppath[] = mypath.split("/");
									int ss = temppath[temppath.length - 1]
											.lastIndexOf(".");
									String tests = temppath[temppath.length - 1]
											.substring(ss);
									String namedocument;
									InputStream fis = new FileInputStream(assist);
									long length = assist.length();
									byte[] bytes = new byte[(int) length];
									int offset = 0;
									int numRead = 0;
									while (offset < bytes.length
											&& (numRead = fis.read(bytes, offset,
													bytes.length - offset)) >= 0) {
										offset += numRead;
									}
									Object strBase64 = Base64.encode(bytes);
									request.addProperty("imgByte", strBase64);
			
									System.out.println("General Daat result" + result_val);
									com = true;
								} catch (Exception e) {
									// TODO: handle exception.
									Error_tracker += "You have problem uploadin Feed back information @";
								}
								if (com) {
									String val= wb.export_footer(wb.envelope, request, "ExportFeedbackDocument");
									result_val +=val+ "~";
									if(val.equals("false"))
									{
										Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
									}
								} else {
									result_val += "false~";
								}
			
							} else {
								Error_tracker += "Please check the following Image/Document is available "
										+  " Path=" + path + " @";
								result_val += "false~";
								 System.out.println("file missing"); 
							}
			
						} else {
							bitmap = null;
							MarshalBase64 marshal = new MarshalBase64();
							File f = new File(path);
							if (f.exists()) {
								/***
								 * Check the file size compress the based on the
								 * size
								 **/
			
								/***
								 * Check the file size compress the based on the
								 * size ends
								 **/
								try {
									bitmap = cf.ShrinkBitmap(path, 400, 400);// BitmapFactory.decodeFile(path);//ShrinkBitmap(path,
																			// 800,
																			// 800);
									ByteArrayOutputStream out = null;
									out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									byte[] raw = out.toByteArray();
			
									request.addProperty("imgByte", raw);
									marshal.register(wb.envelope);
									com = true;
								} catch (Exception e) {
									// TODO: handle exception
									Error_tracker += "You have problem uploadin General Data information @";
								}
								if (com) {
									String val= wb.export_footer(wb.envelope, request, "ExportWSI1802GenHzdImages");
									result_val +=val+ "~";
									if(val.equals("false"))
									{
										Error_tracker+=" You have problem in exporting "+Exportbartext+"@";
									}
								} else {
									result_val += "false";
								}
								System.out.println("general info rsult" + result_val);
							} else {
								// feedback_fileexits=true;
								Error_tracker += "Please check the following Image/Document is available Path=" + path + " @";
								result_val += "false~";
			
							}
			
						}
			
						// System.out.println("feed info request val="+request);
			
					total+=subcount;
					}
			
					/** export feedback document ends ***/
				
					if (result_val.contains("false"))
						return false;
					else if (result_val.equals(""))
						return false;
					else
						return true;
				}
				else
				{
					System.out.println("eelelelelle");
					return true;
				}		
			}
			else
			{
				return false;
			}
		}
		final Handler handler_export = new Handler() {

			public void handleMessage(Message msg) {
				System.out.println("handler_verify="+handler_verif);
				total=0.0;
				progDialog.dismiss();
				if(handler_verif==1)
				{
					cf.ShowToast("Sorry inspection reallocate to some other inspector");
					finish();
				}
				else if(handler_verif==2)
					{
						cf.ShowToast("You cannot export this inspection please check the satus in www.paperlessinspectors.com");
						finish();
					}
				
				else if(handler_verif==4)
				{
					cf.ShowToast("Sorry you have problem in exporting data ");
					finish();
				}
				
				else if(handler_verif==3)
				{
					GeneratePDF();				
				}
				if(wl!=null)
				{
				wl.release();
				wl=null;
				}
				System.out.println("export erro "+Error_tracker);
				
			
			}

		};
		protected void GeneratePDF() {
			// TODO Auto-generated method stub
			if(wb.isInternetOn())
			{
			String source = "<font color=#FFFFFF>Generating PDF... Please wait..."+ "</font>";
			pd1 = ProgressDialog.show(ExportData.this,"", Html.fromHtml(source), true);
		        new Thread(new Runnable() {
		                public void run() {
		                	
		                	try {
		            				if(PDFGen())
			            			{
		            					db.wind_db.execSQL("UPDATE "
		            							+ db.policyholder
		    									+ " SET PH_Status=2 WHERE PH_SRID ='"+ cf.Homeid+ "'"); 
		            					show_handler =0;
			            				handler.sendEmptyMessage(0);
			            			}
			            			else
			            			{
			            				show_handler=1;
			            				handler.sendEmptyMessage(0);
			            			}
		            			
		            			
		            		} catch (SocketTimeoutException e) {
		            			show_handler=2;
		            			handler.sendEmptyMessage(0);
		            			// TODO Auto-generated catch block
		            			e.printStackTrace();
		            		} catch (NetworkErrorException e) {
		            			show_handler=2;
		            			handler.sendEmptyMessage(0);
		            			// TODO Auto-generated catch block
		            			e.printStackTrace();
		            		} catch (IOException e) {
		            			show_handler=2;
		            			// TODO Auto-generated catch block
		            			handler.sendEmptyMessage(0);
		            			
		            		} catch (XmlPullParserException e) {
		            			// TODO Auto-generated catch block
		            			System.out.println("error occuers exmess"+e.getMessage());
		            			show_handler=2;
		            			handler.sendEmptyMessage(0);
		            			
		            		}                
		                }
		            }).start();	
			}
			else
			{
				cf.ShowToast("Internet Connection not available");
			}
		}
		Handler handler = new Handler() {
			public void handleMessage(Message msg) {
				pd1.dismiss();
				if(show_handler==0)
				{
					cf.ShowToast("PDF Generated successfully");
					Intent submitint = new Intent(getApplicationContext(),Export.class);
					submitint.putExtra("type", "export");
					submitint.putExtra("classidentifier", "HomeScreen");
					startActivity(submitint);
					finish();
				}
				else if(show_handler==1)
				{
					cf.ShowToast("Problem in generating PDF");finish();
				}
				else if(show_handler==2)
				{
					cf.ShowToast("There is some problem in network. Please try again later");finish();
				}			
			}
		};
		protected boolean PDFGen() throws IOException, XmlPullParserException ,NetworkErrorException,SocketTimeoutException {
			// TODO Auto-generated method stub
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			SoapObject ad_property = new SoapObject(wb.NAMESPACE,"Generatemar1802Pdf");
		    ad_property.addProperty("InspectorID", db.Insp_id);
			ad_property.addProperty("SRID", cf.Homeid);
			ad_property.addProperty("Flag", "No");
			envelope.setOutputSoapObject(ad_property);System.out.println("ad_pr"+ad_property);
			HttpTransportSE androidHttpTransport1 = new HttpTransportSE(wb.URL_PDF);
			androidHttpTransport1.call(wb.NAMESPACE+"Generatemar1802Pdf", envelope);
			String result = String.valueOf(envelope.getResponse());
			System.out.println("PDFGen result "+result);
			return wb.check_result(result);
		}
		protected Dialog onCreateDialog(int id) {
			switch (id) {
			case 1:			
				
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setMax(100);
				progDialog.setMessage(Html.fromHtml("Please be patient as we package and prepare your Wind Mitigation Inspection report. <br/>"+Exportbartext));
				progDialog.setCancelable(false);
				progThread = new ProgressThread(handler1);
				progThread.start();
				return progDialog;
			default:
				return null;
			}
		}
		class ProgressThread extends Thread {


			// Class constants defining state of the thread
			final static int DONE = 0;

			Handler mHandler;

			ProgressThread(Handler h) {
				mHandler = h;
			}

			@Override
			public void run() {
				mState = RUNNING;
				
				while (mState == RUNNING) {
					try {
						
						// Control speed of update (but precision of delay not
						// guaranteed)
						Thread.sleep(delay);
					} catch (InterruptedException e) {
						//Log.e("ERROR", "Thread was Interrupted");
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putInt("total", (int)total);
					msg.setData(b);
					mHandler.sendMessage(msg);

				}
			}

			public void setState(int state) {
				mState = state;
			}

		}
		/** Upload policy holder information Starts **/
		final Handler handler1 = new Handler() {

			public void handleMessage(Message msg) {
				// Get the current value of the variable total from the message data
				// and update the progress bar.
				
				int total = msg.getData().getInt("total");
				
				progDialog.setProgress(total);
				progDialog.setMessage(Html.fromHtml("Please be patient as we package and prepare your Wind Mitigation Inspection report. <br/>"+Exportbartext));
				//Exportbartext="";
				if (total == 100) {
					progDialog.setCancelable(true);
					dismissDialog(typeBar);
					progThread.setState(ProgressThread.DONE);
					

				}

			}
		};
}
