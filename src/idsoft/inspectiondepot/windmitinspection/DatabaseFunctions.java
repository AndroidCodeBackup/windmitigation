package idsoft.inspectiondepot.windmitinspection;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class DatabaseFunctions {
	public String MY_DATABASE_NAME = "WindMitDatabase.db";	
	public CommonFunctions cf;
	
	public Cursor curcomm;
	
	public static final String inspectorlogin = "tbl_inspectorlogin", policyholder = "tbl_policyholder" , 
			StandardComments = "tbl_standard_comm",  Questions = "tbl_questions",  OriginalMit = "tbl_original_mitinfo",
			QuestionsComments = "tbl_ques_comments",SubmitCheckTable = "tbl_chkforsubmit", FeedBackInfo = "tbl_feedbackinfo", 
			FeedBackDocument="tbl_feedbackdoc", QuestionsRoofCover = "tbl_ques_roofcover", PhotoCaption = "tbl_photo_caption", 
			Photos = "tbl_photos", GeneralHazInfo = "tbl_generalhaz", GeneralHazDoc = "tbl_generalhazdoc", 
			B1802version = "tbl_Version", MailingPolicyHolder= "tbl_mailingpolicyholder",Registration="tbl_registration",
			PrimaryLicensetype="tbl_primlicensetype",PaymentDetails="tbl_payment";;
	public int chkroofcover;
	public static final String ReportsReady = "ReportsReady";
	public String  Insp_id,Insp_firstname="",Insp_lastname="",Insp_address,Insp_companyname,Insp_email,Insp_ext,inspectiondate,
	buildcodevalue = "0", roofdeckvalueprev = "0",alerttitle,alertcontent,
			rooftowallvalueprev = "0", roofgeovalue = "0", roofswrvalue = "0",
			openprovalue = "0", woodframeper = "0", reper = "0", unreper = "0",
			pcnper = "0", otrper = "0";
	public static SQLiteDatabase wind_db;
    Context con;
    public DateFormat df;
    
    public String bcoriddata="",rcoriddata="",rdoriddata="",rworiddata="",rgoriddata="",swroriddata="",oporiddata="",wcoriddata="",
    		bccomments="",rccomments="",rdcomments="",rwcomments="",rgcomments="",swrcomments="",opcomments="",wccomments="",overallcomments="",
    		yearbuilt="",ph_county="",bsize="",ph_firstname="",ph_lastname="",orderid="";
	
    
    public DatabaseFunctions(Context con) 
    {
    	cf = new CommonFunctions(this.con);
    	wind_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		this.con = con;
		CreateTable(16);
        getInspectorId();
    }

	public void CreateTable(int i) {
		// TODO Auto-generated method stub
	switch (i) 
	{
		case 0:
		/** CREATING ARR VERSION TABLE **/
			try {
				
				wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ B1802version
						+ "(VId INTEGER PRIMARY KEY Not null,VersionCode varchar(50),VersionName varchar(50),ModifiedDate varchar(50));");
	
			} catch (Exception e) {}
		break;
		case 1:
			/*INSPECTOR LOGIN*/
			try
			{
				wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
					+ inspectorlogin
					+ " (Id INTEGER PRIMARY KEY AUTOINCREMENT,Ins_Id varchar(100),Ins_FirstName varchar(100),Ins_MiddleName varchar(100)," +
					"Ins_LastName varchar(100),Ins_Address1 varchar(100),Ins_Address2 varchar(100),Ins_City varchar(100),Ins_State varchar(100)," +
					"Ins_County varchar(100),Ins_Zip varchar(100),Ins_Phone varchar(100),Ins_Email varchar(100)," +
					"Ins_CompanyName varchar(100),Ins_CompanyId varchar(100),Ins_Company_desc varchar(100),Ins_Company_Logo varchar(100)," +
					"Ins_CompanyPhone varchar(50),Ins_CompanyEmail varchar(50),Ins_UserName varchar(100),Ins_Password varchar(100),Ins_Flag1 varchar(100),Android_status varchar(100)," +
					"Ins_Headshot VARCHAR(100),Ins_Signature VARCHAR(100)," +
					"Ins_Initials VARCHAR(100),Ins_rememberpwd VARCHAR(100),LoginStatus Varchar(50)," +
					"Fld_licencetype varchar(100),Fld_licenceno varchar(50),Fld_licenceexpirydate varchar(50));");
			} catch (Exception e){}
			
		break;
		case 2:
			/* POLICYHOLDER */
			try {
				
				wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ policyholder
						+ " (Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,PH_SRID varchar(50),PH_FirstName varchar(100),PH_LastName varchar(100),PH_MiddleName varchar(100)," +
						"PH_Address1 varchar(100),PH_Address2 varchar(100),PH_City varchar(50),PH_State varchar(50),PH_County varchar(50)," +
						"PH_Zip	varchar(25),YearBuilt varchar(100),PH_HomePhone varchar(100),PH_WorkPhone varchar(100)," +
						"PH_CellPhone varchar(100),PH_ContactPerson varchar(100),PH_Policyno varchar(100)," +
						"PH_Email varchar(100),PH_NOOFSTORIES varchar(100)," +
						"PH_Status varchar(100),PH_SubStatus varchar(100) Default '0',PH_InspectorId varchar(100)," +
						"Schedule_Comments varchar(500),PH_InspectionTypeId varchar(25)," +
						"InspectionDate varchar(100),InspectionTime varchar(100),InsuranceCompanyname varchar(100),chkbx int," +
						"BuildingSize VARCHAR(20),fld_homeownersign varchar(250) DEFAULT '',fld_homewonercaption varchar(150) DEFAULT '',fld_paperworksign varchar(250) DEFAULT ''," +
						"fld_paperworkcaption varchar(150) DEFAULT '',Fee varchar(150) DEFAULT '',Discount varchar(150) DEFAULT '',Orderid varchar(10));");

			} catch (Exception e) {}
		break;
			
		case 5:
			/** StandardComments Table **/
			try {
				wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ StandardComments
						+ " (SID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,SC_InspectorId varchar(100),SC_QuesID INT NOT NULL DEFAULT ' '," +
						"SC_OptionID VARCHAR (100) NOT NULL DEFAULT ' '," +
						"SC_Description VARCHAR (50) NOT NULL DEFAULT ' ',SC_Status VARCHAR (50) NOT NULL DEFAULT ' ');");

			} catch (Exception e) {
				
			}
			break;
			
		case 6:
			/** Original data for questions **/
			try {
				wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ OriginalMit
						+ " (OQID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,ORI_SRID varchar(100),ORI_BUILDCODE varchar(100)," +
						"ORI_ROOFCOVER varchar(100),ORI_ROOFDECK varchar(100),ORI_ROOFWALL varchar(100),ORI_ROOFGEO varchar(100)," +
						"ORI_SWR varchar(100),ORI_OPENPROT varchar(100),ORI_WALLCONS Varchar(100));");

			} catch (Exception e) {
				
			}
			break;
			
		case 7:
			/** Questions **/
			try {

				wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
						+ Questions
						+ " (QID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,SRID VARCHAR (50) NOT NULL DEFAULT ' '," +
						"BuildingCodeYearBuilt VARCHAR (50) NOT NULL DEFAULT ' ',BuildingCodeValue int NOT NULL DEFAULT '0'," +
						"BuildingCodePerApplnDate VARCHAR (50) NOT NULL DEFAULT ' ',RoofCoverType int NOT NULL DEFAULT ' '," +
						"RoofCoverValue int NOT NULL DEFAULT '0',RoofCoverOtherText VARCHAR (100) DEFAULT ''," +
						"RoofCoverPerApplnDate VARCHAR (50) NOT NULL DEFAULT '',RoofCoverYearofInsDate VARCHAR (50) NOT NULL DEFAULT ' '," +
						"RoofCoverProdAppr VARCHAR (50) NOT NULL DEFAULT ' ',RoofCoverNoInfnProvide int  DEFAULT ' '," +
						"RoofDeckValue int NOT NULL DEFAULT '0',RoofDeckOtherText VARCHAR (100) NOT NULL DEFAULT '0'," +
						"RooftoWallValue int DEFAULT ' ',RooftoWallSubValue int NOT NULL DEFAULT '0'," +
						"RooftoWallClipsMinValue int NOT NULL DEFAULT '0',RooftoWallSingleMinValue int NOT NULL DEFAULT '0'," +
						"RooftoWallDoubleMinValue int NOT NULL DEFAULT '0',RooftoWallOtherText VARCHAR (100) NOT NULL DEFAULT '0'," +
						"RoofGeoValue int NOT NULL DEFAULT '',RoofGeoLength int NOT NULL DEFAULT '0',RoofGeoTotalArea int NOT NULL DEFAULT '0'," +
						"RoofGeoOtherText VARCHAR (100) DEFAULT '',SWRValue int NOT NULL DEFAULT '',OpenProtectType VARCHAR (100) NOT NULL DEFAULT ''," +
						"OpenProtectValue int NOT NULL DEFAULT '',OpenProtectSubValue int,OpenProtectLevelChart varchar(100)," +
						"GOWindEntryDoorsValue VARCHAR (100),GOGarageDoorsValue VARCHAR (100),GOSkylightsValue VARCHAR (100)," +
						"NGOGlassBlockValue VARCHAR (100),NGOEntryDoorsValue VARCHAR (100),NGOGarageDoorsValue VARCHAR (100)," +
						"WoodFrameValue int,WoodFramePer int,ReinMasonryValue int,ReinMasonryPer int,UnReinMasonryValue int," +
						"UnReinMasonryPer int,PouredConcrete int,PouredConcretePer int,OtherWallTitle varchar(50),OtherWal int," +
						"OtherWallPer int,EffectiveDate varchar(50),OverallComments varchar(100),Completed int,ScheduleComments varchar(500)," +
						"ModifyDate datetime,GeneralHazardInclude Integer);");
			} catch (Exception e) {
				
			}
			break;
			case 8:
			/** QuestionsComments **/
			try {

				wind_db.execSQL("CREATE TABLE IF NOT EXISTS " + QuestionsComments
						+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
						+ "SRID VARCHAR (50) NOT NULL DEFAULT ' ',i_InspectionTypeID int NOT NULL DEFAULT '0',"
						+ "BuildingCodeComment varchar(510),RoofCoverComment varchar(510),"
						+ "RoofDeckComment varchar(510),RoofWallComment varchar(510),"
						+ "RoofGeometryComment varchar(510),SecondaryWaterComment varchar(510)," 
						+ "OpeningProtectionComment varchar(510),WallConstructionComment varchar(510)," +
						"InsOverAllComments VARCHAR (510),CreatedOn datetime);");
			}
			catch (Exception e) {			
			}
			break;
			
			case 9:
				/** SUBMIT CHECK TABLE **/
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ SubmitCheckTable
							+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,InspectorId varchar(100),Srid varchar(100),fld_policy int," +
							"fld_builcode int,fld_roofcover int,fld_roofdeck int,fld_roofwall int,fld_roofgeo int,fld_swr int,fld_open int," +
							"fld_wall int,fld_signature int,fld_front int,fld_feedbackinfo int,fld_feedbackdoc int,fld_overall int,fld_hazarddata int);");
				} catch (Exception e) {}
			break;
			case 10:
				/** Feedback info table **/
				try {				
					
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "+FeedBackInfo+" (FD_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
							"FD_InspectorId varchar(50) NOT NULL,FD_SRID varchar(50) NOT NULL,FD_CSC varchar(15) NOT NULL," +
							"FD_whowas varchar(15) NOT NULL,FD_whowas_other varchar(50),FD_insupaperwork varchar(15) NOT NULL,FD_manuinfo varchar(15) NOT NULL," +
							"FD_comments varchar(500) NOT NULL,FD_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
					CreateTable(131);
				} 
				catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
			break;	
			case 11:
				/** FEEDBACK DOCUMENT **/
				try {
					// db("DROP TABLE IF  EXISTS "
					// + FeedBackDocumentTable + " ");
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "+FeedBackDocument+" (FD_D_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
							"FD_D_InspectorId varchar(50) NOT NULL,FD_D_SRID varchar(50) NOT NULL,FD_D_doctit varchar(15) NOT NULL," +
							"FD_D_doctit_other varchar(50), FD_D_path varchar(100) NOT NULL,FD_D_type varchar(50)," +
							"FD_D_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				} catch (Exception e) {}
				break;
			case 12:
				/** ROOF COVER **/
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS " + QuestionsRoofCover
							+ " (WSID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
							+ "SRID VARCHAR (50) NOT NULL DEFAULT '',"
							+ "RoofCoverType VARCHAR (50) NOT NULL DEFAULT '0',"
							+ "RoofCoverValue int NOT NULL DEFAULT '0',"
							+ "PermitApplnDate1 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "PermitApplnDate2 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "PermitApplnDate3 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "PermitApplnDate4 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "PermitApplnDate5 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "PermitApplnDate6 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "RoofCoverTypeOther VARCHAR (100) DEFAULT '',"
							+ "ProdApproval1 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "ProdApproval2 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "ProdApproval3 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "ProdApproval4 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "ProdApproval5 VARCHAR (50) NOT NULL DEFAULT '',"
							+ "ProdApproval6 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "InstallYear1 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "InstallYear2 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "InstallYear3 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "InstallYear4 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "InstallYear5 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "InstallYear6 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "NoInfo1  VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "NoInfo2 VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "NoInfo3  VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "NoInfo4  VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "NoInfo5  VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "NoInfo6  VARCHAR (50) NOT NULL DEFAULT ' ',"
							+ "RoofPreDominant int NOT NULL DEFAULT '0');");
				}
				catch (Exception e) {
					
				}
				break;
			case 13:
				/** PHOTOS **/
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "+Photos+" (WSID INTEGER PRIMARY KEY AUTOINCREMENT,IM_InspectorId varchar(50) NOT NULL," +
							"IM_SRID varchar(50) NOT NULL,IM_Elevation varchar(5) NOT NULL,IM_path varchar(250) NOT NULL," +
							"IM_Description varchar(150) NOT NULL,IM_ImageOrder varchar(5) NOT NULL,IM_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
					
				} catch (Exception e) {
					
				}
				break;
			case 14:
				/** GENERAL HAZARDS DOCUMENT **/
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "+GeneralHazDoc+" (GID INTEGER PRIMARY KEY AUTOINCREMENT,GCH_InspectorId varchar(50) NOT NULL," +
							"GCH_SRID varchar(50) NOT NULL,GCH_Elevation varchar(5) NOT NULL,GCH_path varchar(250) NOT NULL," +
							"GCH_Description varchar(150) NOT NULL,GCH_ImageOrder varchar(5) NOT NULL,GCH_CreatedOn DATETIME DEFAULT(CURRENT_TIMESTAMP))");
					
				} catch (Exception e) {
					
				}
				break;
			case 15:
				/** GENERAL HAZARDS INFO **/
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ GeneralHazInfo
							+ " (GBId INTEGER PRIMARY KEY,Srid varchar(50),InsuredIs varchar(50),InsuredOther Varchar(50),OccupancyType varchar(50)," +
							"OcccupOther Varchar(50),ObservationType varchar(50),ObservOther Varchar(50),OccupiedPercent varchar(50),OccupiedFlag Int," +
							"Vacant Int,VacantPercent varchar(50),NotDetermined Int,PermitConfirmed varchar(50),BuildingSize Int Not null Default(0)," +
							"BalconyPresent varchar(50),AdditionalStructure varchar(50),OtherLocation varchar(500),Location1 varchar(50)," +
							"Location2 varchar(50),Location3 varchar(50),Location4 varchar(50),Observation1 varchar(50),Observation2 varchar(50)," +
							"Observation3 varchar(50),Observation4 varchar(50),Observation5 varchar(50),PerimeterPoolFence varchar(50)," +
							"PoolFenceDisrepair varchar(50),SelfLatch varchar(50),ProfesInstall varchar(50),PoolPresent varchar(50)," +
							"HotTubPresnt varchar(50),EmptyGroundPool varchar(50),PoolSlide varchar(50),DivingBoardPoolSlide varchar(50)," +
							"PerimeterPoolEncl varchar(50),PoolEnclosure varchar(50),Vicious varchar(50),LiveStock varchar(50),OverHanging varchar(50)," +
							"Trampoline varchar(50),SkateBoard varchar(50),bicycle varchar(50),TripHazardDesc varchar(50),TripHazardNoted varchar(50)," +
							"UnsafeStairway varchar(50),PorchAndDeck varchar(50),NonStdConstruction varchar(50),OutDoorAppliances varchar(50)," +
							"OpenFoundation varchar(50),WoodShingled varchar(50),ExcessDebris varchar(50),BusinessPremises varchar(50)," +
							"GeneralDisrepair varchar(50),PropertyDamage varchar(50),StructurePartial varchar(50),InOperative varchar(50)," +
							"RecentDrywall varchar(50),ChineseDrywall varchar(50),ConfirmDrywall varchar(50),NonSecurity varchar(50)," +
							"NonSmoke varchar(50),Comments varchar(500));");
				} catch (Exception e) {
					
				}
				break;
			case 16:
				/** LICENSE TYPE **/
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ PrimaryLicensetype
							+ " (PID INTEGER PRIMARY KEY Not null,LicenseName Varchar(300) ,Licenseid INTEGER Default(0));");
				} catch (Exception e) {
					
				}
				break;
			
			case 23:
				/* MAILING DETAILS OF POLICYHOLDER */
				try {
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "
							+ MailingPolicyHolder
							+ " (MA_Ph_Id INTEGER PRIMARY KEY AUTOINCREMENT,ML_PH_InspectorId varchar(50) NOT NULL,ML_PH_SRID varchar(50) NOT NULL," +
							"ML Integer,ML_PH_Address1 varchar(100),ML_PH_Address2 varchar(100),ML_PH_City varchar(100),ML_PH_Zip varchar(100)," +
							"ML_PH_State varchar(100),ML_PH_County varchar(100));");		
				} catch (Exception e) {
					
				}
				break;
				
		case 25:
				try {
					
					wind_db.execSQL("CREATE TABLE IF NOT EXISTS "+PhotoCaption+" (IM_C_Id INTEGER PRIMARY KEY AUTOINCREMENT," +
							"IM_C_InspectorId varchar(50) NOT NULL,IM_C_caption varchar(250) NOT NULL,IM_C_Elevation varchar(10) NOT NULL," +
							"IM_C_Elevation_Name varchar(250))");
				} catch (Exception e) { System.out.println("the erro msg in mo access "+e.getMessage());}
			break;
			
			case 26:
				try {
					wind_db.execSQL("create table if not exists "
							+ ReportsReady
							+ " (id integer primary key autoincrement,srid varchar2,firstname varchar2,lastname varchar2,address1 varchar2," +
							"address2 varchar2,state varchar2,county varchar2,city varchar2,zip varchar2,policyno varchar2,email varchar2," +
							"status varchar2,pdfpath varchar2,inspectiondate varchar,created_Date DATETIME DEFAULT(CURRENT_TIMESTAMP))");
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				break;
			case 27:
				try {
					wind_db.execSQL("create table if not exists "
							+ PaymentDetails
							+ " (pid integer primary key autoincrement,inspectorid varchar,srid varchar2,OrderId integer,name varchar2,cardtype varchar2,creditcardnumber varchar2," +
							"expirationmonth varchar2,expirationyear varchar2,expirationotheryear varchar2,amount varchar,address varchar,city varchar,state integer,county varchar,"+
							"zipcode varchar,email varchar,cardid integer,automode varchar)");
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				break;
	}
		
	}
	public Cursor SelectTablefunction(String inspectorlogin2, String string) {
		// TODO Auto-generated method stub
		Cursor cur = wind_db.rawQuery("select * from " + inspectorlogin2 + " " + string, null);
		return cur;
	}
	public String encode(String oldstring) 
	{
		if(oldstring==null)
		{
			oldstring="";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return oldstring;
		/*try {
			   byte[] bytes = oldstring.getBytes("ISO-8859-1");
			   if (!validUTF8(bytes))
			    return oldstring;   
			   return new String(bytes, "UTF-8");  
			  } catch (UnsupportedEncodingException e) {
			   // Impossible, throw unchecked
			   throw new IllegalStateException("No Latin1 or UTF-8: " + e.getMessage());
			  }*/

	}
	public String newencode(String oldstring) 
	{
		try {
			   byte[] bytes = oldstring.getBytes("ISO-8859-1");
			   if (!validUTF8(bytes))
			    return oldstring;   
			   return new String(bytes, "UTF-8");  
			  } catch (UnsupportedEncodingException e) {
			   // Impossible, throw unchecked
			   throw new IllegalStateException("No Latin1 or UTF-8: " + e.getMessage());
			  }
	}
	public String decode(String newstring) 
	{
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;
	}
	public String newdecode(String newstring) 
	{
		try {
			   byte[] bytes = newstring.getBytes("ISO-8859-1");
			   if (!validUTF8(bytes))
			    return newstring;   
			   return new String(bytes, "UTF-8");  
			  } catch (UnsupportedEncodingException e) {
			   // Impossible, throw unchecked
			   throw new IllegalStateException("No Latin1 or UTF-8: " + e.getMessage());
			  }
	}
	public static boolean validUTF8(byte[] input) {
		  int i = 0;
		  // Check for BOM
		  if (input.length >= 3 && (input[0] & 0xFF) == 0xEF
		    && (input[1] & 0xFF) == 0xBB & (input[2] & 0xFF) == 0xBF) {
		   i = 3;
		  }

		  int end;
		  for (int j = input.length; i < j; ++i) {
		   int octet = input[i];
		   if ((octet & 0x80) == 0) {
		    continue; // ASCII
		   }

		   // Check for UTF-8 leading byte
		   if ((octet & 0xE0) == 0xC0) {
		    end = i + 1;
		   } else if ((octet & 0xF0) == 0xE0) {
		    end = i + 2;
		   } else if ((octet & 0xF8) == 0xF0) {
		    end = i + 3;
		   } else {
		    // Java only supports BMP so 3 is max
		    return false;
		   }

		   while (i < end) {
		    i++;
		    octet = input[i];
		    if ((octet & 0xC0) != 0x80) {
		     // Not a valid trailing byte
		     return false;
		    }
		   }
		  }
		  return true;
		}
	public String convert_null(String newstring)
	{
		return (newstring==null)?"N/A":(newstring.trim().equals(""))? "N/A":newstring;
		
	}
	public boolean check_value_inTable(String tbl_name,String Where)
	{
		Cursor c =SelectTablefunction(tbl_name, Where);
		if(c.getCount()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	/*public void delete_all(final String where,String atitle) {
		// TODO Auto-generated method stub
		    final Dialog dialog1 = new Dialog(con,android.R.style.Theme_Translucent_NoTitleBar);
			dialog1.getWindow().setContentView(R.layout.alertsync);
			TextView txttitle = (TextView) dialog1.findViewById(R.id.txthelp);
			txttitle.setText("Delete");
			TextView txt = (TextView) dialog1.findViewById(R.id.txtid);
			txt.setText(Html.fromHtml(atitle));
			Button btn_yes = (Button) dialog1.findViewById(R.id.yes);
			Button btn_cancel = (Button) dialog1.findViewById(R.id.cancel);
			btn_yes.setOnClickListener(new OnClickListener()
			{
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.dismiss();
					CreateTable(2);CreateTable(7);CreateTable(8);CreateTable(10);
					CreateTable(11);CreateTable(13);CreateTable(14);CreateTable(15);
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where s_SRID in "+where;
											
						wind_db.execSQL("DELETE FROM " + policyholder
								+ " " + temp + " ");
					} catch (Exception e) {

					}
					
					try {
						
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SRID in "+where;
											
						wind_db.execSQL("DELETE FROM " + Questions 
								+ " " + temp + "");
					} catch (Exception e) {
					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SrID in "+where;
						
						wind_db.execSQL("DELETE FROM " + Photos
								+ " " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where Srid in "+where;
						
						wind_db.execSQL("DELETE FROM " + FeedBackInfo
								+ " " + temp + "");
					} catch (Exception e) {

					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SRID in "+where;
						
						wind_db.execSQL("DELETE FROM " + FeedBackDocument
								+ " " + temp + "");
					} catch (Exception e) {

					}
					
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SRID in "+where;
						
						wind_db.execSQL("DELETE FROM " + QuestionsComments 
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where Srid in "+where;
						
						wind_db.execSQL("DELETE FROM " + GeneralHazInfo 
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					try {
						String temp="";
						if(where.equals(""))
						 temp="";
						else
						 temp="Where SrID in "+where;
						
						wind_db.execSQL("DELETE FROM " + GeneralHazDoc 
								+ "  " + temp + "");
					} catch (Exception e) {

					}
					cf.ShowToast("Deleted sucessfully.",1);
					con.startActivity(new Intent(con,HomeScreen.class));
					
				}
				
			});
			btn_cancel.setOnClickListener(new OnClickListener()
			{

				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog1.setCancelable(true);
					dialog1.dismiss();
					
				}
				
			});
			dialog1.setCancelable(false);
			dialog1.show();
	 
		
	}*/
	public void getInspectorId() {
		// TODO Auto-generated method stub
		try {
			CreateTable(1);
			Cursor cur = this.SelectTablefunction(this.inspectorlogin," where Ins_Flag1='1'");
			System.out.println(" the quesry ");
			cur.moveToFirst();
			if (cur.getCount()>0) {
				do {
					Insp_id = decode(cur.getString(cur.getColumnIndex("Ins_Id")));System.out.println("inspe"+Insp_id);
					
					Insp_firstname = decode(cur.getString(cur.getColumnIndex("Ins_FirstName")));
					Insp_lastname = decode(cur.getString(cur.getColumnIndex("Ins_LastName")));
					Insp_address = decode(cur.getString(cur.getColumnIndex("Ins_Address1")));
					Insp_companyname = decode(cur.getString(cur.getColumnIndex("Ins_CompanyName")));
					Insp_email = decode(cur.getString(cur.getColumnIndex("Ins_Email")));
				} while (cur.moveToNext());
				CreateTable(15);
			}
			cur.close();
		} catch (Exception e) {System.out.println("erro"+e.getMessage());
		}
	}
	public void getPHinformation(String  homeid) {
		// TODO Auto-generated method stub
		try
		{
			CreateTable(2);
			Cursor c2 = this.SelectTablefunction(this.policyholder," where PH_SRID='"+homeid+"'");
			if (c2.getCount()>0) 
			{
					c2.moveToFirst();
					ph_firstname = decode(c2.getString(c2.getColumnIndex("PH_FirstName")));
					ph_lastname = decode(c2.getString(c2.getColumnIndex("PH_LastName")));
					yearbuilt = decode(c2.getString(c2.getColumnIndex("YearBuilt")));
					ph_county= decode(c2.getString(c2.getColumnIndex("PH_County")));
					bsize= c2.getString(c2.getColumnIndex("BuildingSize"));
					orderid= c2.getString(c2.getColumnIndex("Orderid"));
					
					
			}
		} 
		catch (Exception e)
		{
			System.out.println("erro"+e.getMessage());
		}
	}
	public void getQuesOriginal(String  homeid) {
		// TODO Auto-generated method stub
		try 
		{
			CreateTable(6);
			Cursor c2 = SelectTablefunction(OriginalMit," where ORI_SRID='" + homeid + "'");		
			if (c2.getCount()>0) 
			{
				c2.moveToFirst();
					bcoriddata = decode(c2.getString(c2.getColumnIndex("ORI_BUILDCODE")));
					rcoriddata = decode(c2.getString(c2.getColumnIndex("ORI_ROOFCOVER")));
					rdoriddata = decode(c2.getString(c2.getColumnIndex("ORI_ROOFDECK")));
					rworiddata = decode(c2.getString(c2.getColumnIndex("ORI_ROOFWALL")));
					rgoriddata = decode(c2.getString(c2.getColumnIndex("ORI_ROOFGEO")));
					swroriddata = decode(c2.getString(c2.getColumnIndex("ORI_SWR")));
					oporiddata = decode(c2.getString(c2.getColumnIndex("ORI_OPENPROT")));
					wcoriddata = decode(c2.getString(c2.getColumnIndex("ORI_WALLCONS")));
			}
		}
		catch (Exception e) 
		{
			
		}
	}
	public void getQuesComments(String  homeid) {
		// TODO Auto-generated method stub
		try 
		{
			CreateTable(8);
			Cursor curcomm = SelectTablefunction(QuestionsComments," where SRID ='" + homeid + "'");		
			if (curcomm.getCount()>0) 
			{
				curcomm.moveToFirst();
					bccomments = decode(curcomm.getString(curcomm.getColumnIndex("BuildingCodeComment")));
					rccomments = decode(curcomm.getString(curcomm.getColumnIndex("RoofCoverComment")));
					rdcomments = decode(curcomm.getString(curcomm.getColumnIndex("RoofDeckComment")));
					rwcomments = decode(curcomm.getString(curcomm.getColumnIndex("RoofWallComment")));
					rgcomments = decode(curcomm.getString(curcomm.getColumnIndex("RoofGeometryComment")));
					swrcomments = decode(curcomm.getString(curcomm.getColumnIndex("SecondaryWaterComment")));
					opcomments = decode(curcomm.getString(curcomm.getColumnIndex("OpeningProtectionComment")));
					wccomments = decode(curcomm.getString(curcomm.getColumnIndex("WallConstructionComment")));
					overallcomments = decode(curcomm.getString(curcomm.getColumnIndex("InsOverAllComments")));
					
			}
		}
		catch (Exception e) 
		{
			
		}
	}
	public void fn_delete(String dt) {
		// TODO Auto-generated method stub
		CreateTable(2);CreateTable(7);CreateTable(8);CreateTable(10);
		CreateTable(11);CreateTable(13);CreateTable(14);CreateTable(15);
		wind_db.execSQL("DELETE FROM "+ policyholder+ " WHERE s_SRID ='" + dt+ "'");
		wind_db.execSQL("DELETE FROM "+ Questions+ " WHERE SRID ='" + dt+ "'");										
		wind_db.execSQL("DELETE FROM "+ QuestionsComments+ " WHERE SRID ='" + dt+ "'");
		wind_db.execSQL("DELETE FROM "+ Photos + " WHERE SrID ='" + dt+ "'");
		wind_db.execSQL("DELETE FROM "+ FeedBackInfo+ " WHERE Srid ='" + dt+ "'");
		wind_db.execSQL("DELETE FROM "+ FeedBackDocument+ " WHERE SRID ='" + dt+ "'");
		wind_db.execSQL("DELETE FROM "+ GeneralHazInfo+ " WHERE Srid ='" + dt+ "'");
		wind_db.execSQL("DELETE FROM "+ GeneralHazDoc+ " WHERE SrID ='" + dt+ "'");
	}

	public void dropTable(int select) {
		switch (select) {
		case 16:
			/** drop OnlineTable **/
			try {
				wind_db.execSQL("DROP TABLE IF EXISTS "+ PrimaryLicensetype );
				
			} catch (Exception e) {
			}
			break;
		}
    }
	public void changeimage(String Homeid) {
		// TODO Auto-generated method stub
		try {
			Cursor c1 = this.SelectTablefunction(this.Questions,"where SRID='" +Homeid + "'");System.out.println("CCC"+c1.getCount());
			if (c1.getCount() > 0) 
			{
				c1.moveToFirst();
				buildcodevalue = c1.getString(c1.getColumnIndex("BuildingCodeValue"));System.out.println("buildcodevalue"+buildcodevalue);
				roofdeckvalueprev = c1.getString(c1.getColumnIndex("RoofDeckValue"));
				rooftowallvalueprev = c1.getString(c1.getColumnIndex("RooftoWallValue"));
				roofgeovalue = c1.getString(c1.getColumnIndex("RoofGeoValue"));
				openprovalue = c1.getString(c1.getColumnIndex("OpenProtectValue"));
				roofswrvalue = c1.getString(c1.getColumnIndex("SWRValue"));
				woodframeper = c1.getString(c1.getColumnIndex("WoodFramePer"));
				reper = c1.getString(c1.getColumnIndex("ReinMasonryPer"));
				unreper = c1.getString(c1.getColumnIndex("UnReinMasonryPer"));
				pcnper = c1.getString(c1.getColumnIndex("PouredConcretePer"));
				otrper = c1.getString(c1.getColumnIndex("OtherWallPer"));				
			}System.out.println("Dfds");
			Cursor c3 = this.SelectTablefunction(this.QuestionsRoofCover,"where SRID='" + Homeid + "'");
			chkroofcover = c3.getCount();System.out.println("CCCroof"+chkroofcover);

		} catch (Exception e) {
			System.out.println("Exceptionintry= " + e);
		}
	}
}